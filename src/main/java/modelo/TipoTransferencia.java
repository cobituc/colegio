package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TipoTransferencia {
    
    private int idTransferencia;
    private String nombre;

    public TipoTransferencia(int idTransferencia, String nombre) {
        this.idTransferencia = idTransferencia;
        this.nombre = nombre;
    }

    public int getIdTransferencia() {
        return idTransferencia;
    }

    public void setIdTransferencia(int idTransferencia) {
        this.idTransferencia = idTransferencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
    public static void cargarTipoTransferencia(Connection connection, ArrayList<TipoTransferencia> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT idTipoTransferencia,TransferenciaNombre FROM tipo_transferencia ORDER BY idTipoTransferencia");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new TipoTransferencia(rs.getInt("idTipoTransferencia"), rs.getString("transferenciaNombre")));               
            }
        } catch (SQLException e) {
        }
    }
    
    public static TipoTransferencia buscarTipoTransferencia(String nombre, ArrayList<TipoTransferencia> lista) {
        TipoTransferencia resultado = null;
        
        for (TipoTransferencia tipo : lista) {
            if (tipo.getNombre().equals(nombre)) {
                resultado = tipo;
                break;
            }
        }
        return resultado;
    }
    
    public static void insertarTipoTransferencia(Connection connection, TipoTransferencia tipoTransferencia) {

        String sSQL;
        int i = 0;

        sSQL = "INSERT INTO tipo_transferencia(TransferenciaNombre) VALUES(?)";

        try {

            PreparedStatement pst = connection.prepareStatement(sSQL);

            pst.setString(1, tipoTransferencia.getNombre());

            i = pst.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static boolean actualizarTipoTransferencia(Connection connection, TipoTransferencia tipo){
         
         int update = 0;         
            
            String sSQL;
            PreparedStatement pst;            
            
            sSQL = "UPDATE tipo_transferencia SET TransferenciaNombre=? WHERE idCuenta = "+tipo.getIdTransferencia();
        try {
            pst = connection.prepareStatement(sSQL);
       
            pst.setString(1, tipo.getNombre());            
            update = pst.executeUpdate();     

        } catch (SQLException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return update>0;
     }
    
    @Override
    public String toString() {
        return nombre;
    }
}