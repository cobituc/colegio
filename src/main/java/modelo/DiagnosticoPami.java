
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class DiagnosticoPami {
    
    private int id;
    private String codigo;
    private String diagnostico;

    public DiagnosticoPami(int id, String codigo, String diagnostico) {
        this.id = id;
        this.codigo = codigo;
        this.diagnostico = diagnostico;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    
    
    public static void cargarDiagnosticos(Connection connection, ArrayList<DiagnosticoPami> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT * FROM diagnosticos_pami");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new DiagnosticoPami(rs.getInt(1), rs.getString(2), rs.getString(3)));               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }   
    
    
    public static DiagnosticoPami buscarDiagnostico(String codigo, ArrayList<DiagnosticoPami> lista) {
        DiagnosticoPami resultado = null;
        for (DiagnosticoPami diagnosticos : lista) {
            if (diagnosticos.getCodigo().equals(codigo)) {
                resultado = diagnosticos;
                break;
            }
        }
        return resultado;
    }
    
    public static boolean buscarDiagnosticoBool(String codigo, ArrayList<DiagnosticoPami> lista) {
        boolean resultado=false;
        for (DiagnosticoPami diagnosticos : lista) {
            if (diagnosticos.getCodigo().equals(codigo)) {
                resultado = true;
                break;
            }
        }
        return resultado;
    }
    
}