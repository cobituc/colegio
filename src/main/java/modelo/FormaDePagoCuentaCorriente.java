package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class FormaDePagoCuentaCorriente {
    
    private int idFormaDePago;
    private String nombre;
    private double porcentaje;
    

    public FormaDePagoCuentaCorriente(int idFormaDePago,String nombre,double porcentaje) {
        this.nombre = nombre;
        this.idFormaDePago=idFormaDePago;
        this.porcentaje=porcentaje;
    }
    
    public double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }

//    }
    public int getIdFormaDePago() {
        return idFormaDePago;
    }

    public void setIdFormaDePago(int idFormaDePago) {
        this.idFormaDePago = idFormaDePago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
    public static void cargarDetalleDePago(Connection connection, ArrayList<FormaDePagoCuentaCorriente> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT id_forma_cuenta_corriente,descripcion,porcentaje FROM forma_cuenta_corriente ORDER BY id_forma_cuenta_corriente");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new FormaDePagoCuentaCorriente(rs.getInt("id_forma_cuenta_corriente"),rs.getString("descripcion"),rs.getDouble("porcentaje")));               
            }
        } catch (SQLException e) {
             e.printStackTrace();
        }
    }
    
    public static FormaDePagoCuentaCorriente buscarDetalleFormaDePago(String nombre, ArrayList<FormaDePagoCuentaCorriente> lista) {
        FormaDePagoCuentaCorriente resultado = null;
        
        for (FormaDePagoCuentaCorriente forma : lista) {
            if (forma.getNombre().equals(nombre)) {
                resultado = forma;
                break;
            }
        }
        return resultado;
    }
    
    @Override
    public String toString() {
        return nombre;
    }
}