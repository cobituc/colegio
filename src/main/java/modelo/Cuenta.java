package modelo;

import controlador.UtilToDate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cuenta {

    private int id;
    private String propiedad;
    private long numero;
    private String nombre;
    private String bancoNombre;
    private String tipo;
    private Date fecha;
    private String transferencia;
    private long cuit;
    private String cbu;
    private int estado;
    private int idUsuario;

    public Cuenta() {
    }

    public Cuenta(int id, String propiedad, long numero, String nombre, String bancoNombre, String tipo, Date fecha, String transferencia, long cuit, String cbu, int estado, int idUsuario) {
        this.id = id;
        this.propiedad = propiedad;
        this.numero = numero;
        this.nombre = nombre;
        this.bancoNombre = bancoNombre;
        this.tipo = tipo;
        this.fecha = fecha;
        this.transferencia = transferencia;
        this.cuit = cuit;
        this.cbu = cbu;
        this.estado = estado;
        this.idUsuario = idUsuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPropiedad() {
        return propiedad;
    }

    public void setPropiedad(String propiedad) {
        this.propiedad = propiedad;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBancoNombre() {
        return bancoNombre;
    }

    public void setBancoNombre(String bancoNombre) {
        this.bancoNombre = bancoNombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(String transferencia) {
        this.transferencia = transferencia;
    }

    public long getCuit() {
        return cuit;
    }

    public void setCuit(long cuit) {
        this.cuit = cuit;
    }

    public String getCbu() {
        return cbu;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return numero + " - " + nombre;
    }

    public static void cargarCuenta(Connection connection, ArrayList<Cuenta> lista) {

        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT id_cuenta, propiedad, numero, nombre, bancoNombre, tipo, fecha_creacion, transferencia, cuit, cbu, estado, usuario FROM cuentas");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new Cuenta(rs.getInt("id_cuenta"), rs.getString("propiedad"), rs.getInt("numero"), rs.getString("nombre"), rs.getString("bancoNombre"), rs.getString("tipo"), rs.getDate("fecha_creacion"), rs.getString("transferencia"), rs.getLong("cuit"), rs.getString("cbu"), rs.getInt("estado"), rs.getInt("usuario")));
            }
        } catch (SQLException e) {

        }
    }

    public static Cuenta buscarCuenta(String nombre, ArrayList<Cuenta> lista) {

        Cuenta resultado = null;
        for (Cuenta cuenta : lista) {
            if ((cuenta.getNumero() + " - " + cuenta.getNombre()).equals(nombre)) {
                resultado = cuenta;
                break;
            }
        }
        return resultado;
    }

    public static int insertarCuenta(Connection connection, Cuenta cuenta) {

        String sSQL;
        int i = 0;
        int inserta = 0;

        sSQL = "INSERT INTO cuentas(propiedad, numero, nombre, bancoNombre, tipo, fecha_creacion, transferencia, cuit, cbu, estado, usuario) VALUES(?,?,?,?,?,?,?,?,?,?,?)";

        try {
            Statement instruccion = connection.createStatement();
            
            PreparedStatement pst = connection.prepareStatement(sSQL);

            pst.setString(1, cuenta.getPropiedad());
            pst.setLong(2, cuenta.getNumero());
            pst.setString(3, cuenta.getNombre());
            pst.setString(4, cuenta.getBancoNombre());
            pst.setString(5, cuenta.getTipo());
            pst.setDate(6, UtilToDate.convert(cuenta.getFecha()));
            pst.setString(7, cuenta.getTransferencia());
            pst.setLong(8, cuenta.getCuit());
            pst.setString(9, cuenta.getCbu());
            pst.setInt(10, cuenta.getEstado());
            pst.setInt(11, cuenta.getIdUsuario());

            i = pst.executeUpdate();
            
            ResultSet rs = instruccion.executeQuery("SELECT LAST_INSERT_ID() FROM cuentas");
            rs.next();            
            inserta = rs.getInt(1);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return inserta;
    }

    public static boolean actualizarCuenta(Connection connection, Cuenta cuenta) {

        int update = 0;

        String sSQL;
        PreparedStatement pst;

        sSQL = "UPDATE cuentas SET propiedad=?, numero=?, nombre=?, bancoNombre=?, tipo=?, fecha_creacion=?, transferencia=?, cuit=?, cbu=?, estado=?, usuario=? WHERE id_cuenta = " + cuenta.getId();
        try {

            pst = connection.prepareStatement(sSQL);

            pst.setString(1, cuenta.getPropiedad());
            pst.setLong(2, cuenta.getNumero());
            pst.setString(3, cuenta.getNombre());
            pst.setString(4, cuenta.getBancoNombre());
            pst.setString(5, cuenta.getTipo());
            pst.setDate(6, UtilToDate.convert(cuenta.getFecha()));
            pst.setString(7, cuenta.getTransferencia());
            pst.setLong(8, cuenta.getCuit());
            pst.setString(9, cuenta.getCbu());
            pst.setInt(10, cuenta.getEstado());
            pst.setInt(11, cuenta.getIdUsuario());

            update = pst.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }

        return update > 0;
    }

    public static int insertarSubCuenta(Connection connection, int cuentaPrincipal, int subCuenta) {

        String sSQL;
        int i = 0;

        sSQL = "INSERT INTO subcuentas(id_cuenta, id_cuenta_padre) VALUES(?,?)";

        try {
            
            PreparedStatement pst = connection.prepareStatement(sSQL);
            pst.setInt(1, subCuenta);
            pst.setInt(2, cuentaPrincipal);
            i = pst.executeUpdate();
        
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return i;
    }
}
