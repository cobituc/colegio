package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class FormaDePagoCheque {
    
    private int idFormaDePago;    
    private int numero;
    private int idBanco;
    private double importe;
    private String fechaEmision;
    private String fechaVencimiento;

    public FormaDePagoCheque(int numero, int idBanco, double importe, String fechaEmision, String fechaVencimiento) {
        this.numero = numero;
        this.idBanco = idBanco;
        this.importe = importe;
        this.fechaEmision = fechaEmision;
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setIdFormaDePago(int idFormaDePago) {
        this.idFormaDePago = idFormaDePago;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setIdBanco(int idBanco) {
        this.idBanco = idBanco;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public int getIdFormaDePago() {
        return idFormaDePago;
    }

    public int getNumero() {
        return numero;
    }

    public int getIdBanco() {
        return idBanco;
    }

    public double getImporte() {
        return importe;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }
    
    public static int insertarCheque(Connection connection, FormaDePagoCheque cheque) {
        String sSQL;
        PreparedStatement InsertMovimineto = null;

        int IdMovimiento = 0;
        sSQL = "INSERT INTO detalle_movimiento_forma_cheque(numero,id_banco,importe,fecha_emision,fecha_vencimiento) VALUES(?,?,?,?,?)";
        try {

            InsertMovimineto = connection.prepareStatement(sSQL);

            connection.setAutoCommit(false); //transaction block start

            InsertMovimineto.setInt(1, cheque.getNumero());
            InsertMovimineto.setInt(2, cheque.getIdBanco());
            InsertMovimineto.setDouble(3, cheque.getImporte());
            InsertMovimineto.setString(4, cheque.getFechaEmision());
            InsertMovimineto.setString(5, cheque.getFechaVencimiento());
            InsertMovimineto.executeUpdate();
            ////////////////////////////////////////////////////////////
            
            connection.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            try {
                connection.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        } finally {
            try {                
                if (InsertMovimineto != null) {
                    InsertMovimineto.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return IdMovimiento;
    }
    
    
}