package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ObraSocial {

    private int idObraSocial;
    private int codigo;
    private String nombre;
    private double importeUnidadDeArancel;
    private int estado;

    public ObraSocial(int idObraSocial, int codigo, String nombre, double importeUnidadDeArancel, int estado) {
        this.idObraSocial = idObraSocial;
        this.codigo = codigo;
        this.nombre = nombre;
        this.importeUnidadDeArancel = importeUnidadDeArancel;
        this.estado = estado;
    }
    
    
    

    public int getIdObraSocial() {
        return idObraSocial;
    }

    public void setIdObraSocial(int idObraSocial) {
        this.idObraSocial = idObraSocial;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getImporteUnidadDeArancel() {
        return importeUnidadDeArancel;
    }

    public void setImporteUnidadDeArancel(double importeUnidadDeArancel) {
        this.importeUnidadDeArancel = importeUnidadDeArancel;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

        

    public static void cargarOS(Connection connection, ArrayList<ObraSocial> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT id_obrasocial,razonsocial_obrasocial,importeunidaddearancel_obrasocial,codigo_obrasocial, estado_obrasocial FROM obrasocial ORDER BY int_codigo_obrasocial");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new ObraSocial(rs.getInt("id_obrasocial"), rs.getInt("codigo_obrasocial"), rs.getString("razonsocial_obrasocial"), rs.getDouble("importeunidaddearancel_obrasocial"), rs.getInt("estado_obrasocial")));               
            }
        } catch (SQLException e) {
        }
    }
    
    public static ObraSocial buscarOS(String nombre, ArrayList<ObraSocial> lista) {
        ObraSocial resultado = null;
        for (ObraSocial obras : lista) {
            if ((obras.getCodigo()+" - "+obras.getNombre()).equals(nombre)) {
                resultado = obras;
                break;
            }
        }
        return resultado;
    }

    @Override
    public String toString() {
        return codigo + " - " + nombre;
    }

}
