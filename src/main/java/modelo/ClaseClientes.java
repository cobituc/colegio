package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ClaseClientes {

    public ClaseClientes() {
    }

    public int AgregarNoColegiado(int idResponsableIVA, int idTipodoc, String documento, String nombre, String domicilio, String localidad, String provincia, int margen, String persona, String telefonocel, String telefonofijo, String mail, String obs) {
        int cliente = 0;
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        Statement SelectIvaDocumento = null;
        PreparedStatement AgregarClientes = null;
        PreparedStatement AgregarNoColegiado = null;
        Statement UltimoIdCliente = null;
        String SQL = "";
        //Veo si Ivadocumento no esta cargado en p_cliente
        String sqldoc = "SELECT IvaDocumento FROM nocolegiado WHERE IvaDocumento='" + documento + "'";
        try {
            SelectIvaDocumento = cn.createStatement();
            ResultSet rsdoc = SelectIvaDocumento.executeQuery(sqldoc);
            if (!rsdoc.next()) {
                cn.setAutoCommit(false); //transaction block start
                
                SQL = "INSERT INTO clientes (tipo, idTiporesponsable, idTipodoc) VALUES(?,?,?)";
                AgregarClientes = cn.prepareStatement(SQL);
                AgregarClientes.setString(1, "nocolegiado");
                AgregarClientes.setInt(2, idResponsableIVA);
                AgregarClientes.setInt(3, idTipodoc);
                AgregarClientes.executeUpdate();
                
                //Busco Ultimo cliente
                UltimoIdCliente = cn.createStatement();
                ResultSet rs = UltimoIdCliente.executeQuery("SELECT MAX(idClientes) AS idClientes FROM clientes");
                rs.next();
                cliente = rs.getInt("idClientes");

                //Inserto el cliente
                String sSQL = "INSERT INTO nocolegiado("
                        + " IvaDocumento, nombre, domicilio, localidad,"
                        + " provincia, margen, persona, telcel, telfijo, mail, observacion, idClientes) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
                AgregarNoColegiado = cn.prepareStatement(sSQL);
                AgregarNoColegiado.setString(1, documento);
                AgregarNoColegiado.setString(2, nombre);
                AgregarNoColegiado.setString(3, domicilio);
                AgregarNoColegiado.setString(4, localidad);
                AgregarNoColegiado.setString(5, provincia);
                AgregarNoColegiado.setInt(6, margen);
                AgregarNoColegiado.setString(7, persona);
                AgregarNoColegiado.setString(8, telefonocel);
                AgregarNoColegiado.setString(9, telefonofijo);
                AgregarNoColegiado.setString(10, mail);
                AgregarNoColegiado.setString(11, obs);
                AgregarNoColegiado.setInt(12, cliente);
                int n = AgregarNoColegiado.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                }
                cn.commit(); //transaction block end
                cliente = 1;
            } else {
                JOptionPane.showMessageDialog(null, "El No Colegiado ya esta en la base de datos");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
            try {
                cn.rollback();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } finally {
            try {
                if (SelectIvaDocumento != null) {
                    SelectIvaDocumento.close();
                }
                if (AgregarClientes != null) {
                    AgregarClientes.close();
                }
                if (AgregarNoColegiado != null) {
                    AgregarNoColegiado.close();
                }
                if (UltimoIdCliente != null) {
                    UltimoIdCliente.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return cliente;
    }

    public void ModificarNoColegiado(int idNoColegiado, int idResponsableIVA, int idTipodoc, String documento, String nombre, String domicilio, String localidad, String provincia, int margen, String persona, String telefonocel, String telefonofijo, String mail, String obs, int idClientes) {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarClientes = null;
        PreparedStatement ModificarNoColegiado = null;
        
        String sSQL = "UPDATE nocolegiado SET"
                + " IvaDocumento=?,"
                + " nombre=?, domicilio=?, localidad=?,"
                + " provincia=?, margen=?, persona=?,"
                + " telcel=?, telfijo=?, mail=?,"
                + " observacion=?"
                + " WHERE idNoColegiado=" + idNoColegiado;
        
        String sSQL2 = "UPDATE clientes SET"
                + " idTiporesponsable=?, idTipodoc=?"
                + " WHERE idClientes=" + idClientes;
        
        try {
            ModificarNoColegiado = cn.prepareStatement(sSQL);
            ModificarNoColegiado.setString(1, documento);
            ModificarNoColegiado.setString(2, nombre);
            ModificarNoColegiado.setString(3, domicilio);
            ModificarNoColegiado.setString(4, localidad);
            ModificarNoColegiado.setString(5, provincia);
            ModificarNoColegiado.setInt(6, margen);
            ModificarNoColegiado.setString(7, persona);
            ModificarNoColegiado.setString(8, telefonocel);
            ModificarNoColegiado.setString(9, telefonofijo);
            ModificarNoColegiado.setString(10, mail);
            ModificarNoColegiado.setString(11, obs);
            int n = ModificarNoColegiado.executeUpdate();
            
            ModificarClientes = cn.prepareStatement(sSQL2);
            ModificarClientes.setInt(1, idResponsableIVA);
            ModificarClientes.setInt(2, idTipodoc);
            ModificarClientes.executeUpdate();
            
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de Datos");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (ModificarNoColegiado != null) {
                    ModificarNoColegiado.close();
                }
                if (ModificarClientes != null) {
                    ModificarClientes.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

}