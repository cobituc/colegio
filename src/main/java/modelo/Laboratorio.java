package modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Laboratorio {

    private int idLaboratorios;
    private String nombre;
    private long cuit;
    private String direccion;
    private String numero;
    private String observaciones;
    private long celular;
    private long telefono;
    private int idLocalidades;
    private String mail;
    private String paginaWeb;
    private String horario;
    private String especialidad;
    private Date fechaAlta;
    private Date fechaBaja;
    private String SSSRP;
    private Date SSSRPvenc;
    private Date altaSIPROSA;
    private Date vencimientoSIPROSA;
    private String nHabilitacionSiprosa;

    public Laboratorio(int idLaboratorios, String nombre, long cuit, String direccion, String numero, String observaciones, long celular, long telefono, int idLocalidades, String mail, String paginaWeb, String horario, String especialidad, Date fechaAlta, Date fechaBaja, String SSSRP, Date SSSRPvenc, Date altaSIPROSA, Date vencimientoSIPROSA, String nHabilitacionSiprosa) {
        this.idLaboratorios = idLaboratorios;
        this.nombre = nombre;
        this.cuit = cuit;
        this.direccion = direccion;
        this.numero = numero;
        this.observaciones = observaciones;
        this.celular = celular;
        this.telefono = telefono;
        this.idLocalidades = idLocalidades;
        this.mail = mail;
        this.paginaWeb = paginaWeb;
        this.horario = horario;
        this.especialidad = especialidad;
        this.fechaAlta = fechaAlta;
        this.fechaBaja = fechaBaja;
        this.SSSRP = SSSRP;
        this.SSSRPvenc = SSSRPvenc;
        this.altaSIPROSA = altaSIPROSA;
        this.vencimientoSIPROSA = vencimientoSIPROSA;
        this.nHabilitacionSiprosa = nHabilitacionSiprosa;
    }

      
    
    public Laboratorio(){};
    
    public int getIdLaboratorios() {
        return idLaboratorios;
    }

    public void setIdLaboratorios(int idLaboratorios) {
        this.idLaboratorios = idLaboratorios;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getCuit() {
        return cuit;
    }

    public void setCuit(long cuit) {
        this.cuit = cuit;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public long getCelular() {
        return celular;
    }

    public void setCelular(long celular) {
        this.celular = celular;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public int getIdLocalidades() {
        return idLocalidades;
    }

    public void setIdLocalidades(int idLocalidades) {
        this.idLocalidades = idLocalidades;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPaginaWeb() {
        return paginaWeb;
    }

    public void setPaginaWeb(String paginaWeb) {
        this.paginaWeb = paginaWeb;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }   

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getSSSRP() {
        return SSSRP;
    }

    public void setSSSRP(String SSSRP) {
        this.SSSRP = SSSRP;
    }

    public Date getSSSRPvenc() {
        return SSSRPvenc;
    }

    public void setSSSRPvenc(Date SSSRPvenc) {
        this.SSSRPvenc = SSSRPvenc;
    }

    public Date getAltaSIPROSA() {
        return altaSIPROSA;
    }

    public void setAltaSIPROSA(Date altaSIPROSA) {
        this.altaSIPROSA = altaSIPROSA;
    }

    public Date getVencimientoSIPROSA() {
        return vencimientoSIPROSA;
    }

    public void setVencimientoSIPROSA(Date vencimientoSIPROSA) {
        this.vencimientoSIPROSA = vencimientoSIPROSA;
    }

    public String getnHabilitacionSiprosa() {
        return nHabilitacionSiprosa;
    }

    public void setnHabilitacionSiprosa(String nHabilitacionSiprosa) {
        this.nHabilitacionSiprosa = nHabilitacionSiprosa;
    }
    
    

    public static void cargarLaboratorio(Connection connection, ArrayList<Laboratorio> lista) {

        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs;
            int i=0;
            rs = instruccion.executeQuery("SELECT * FROM laboratorios");
            while (rs.next()) {
                
                lista.add(new Laboratorio(rs.getInt("idLaboratorios"), rs.getString("nombre_fantasia"), rs.getLong("cuit"), 
                        rs.getString("direccion"), rs.getString("numero_direccion"), rs.getString("observaciones"), rs.getLong("celular"), rs.getLong("telefono"),
                        rs.getInt("idLocalidades"), rs.getString("mail"), rs.getString("paginaWeb"), 
                        rs.getString("horario"), rs.getString("especialidad"), rs.getDate("alta"),
                        rs.getDate("baja"), rs.getString("SSSRP"), rs.getDate("SSSRPvenc"), 
                        rs.getDate("altaSIPROSA"), rs.getDate("vencimientoSIPROSA"), rs.getString("nHabilitacionSIPROSA")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }

    
    public static Laboratorio buscarLaboratorio(String nombre, ArrayList<Laboratorio> lista) {
        
        Laboratorio resultado = null;
      
        for (Laboratorio laboratorio : lista) {

            if (laboratorio.getNombre().equals(nombre)) {

                resultado = laboratorio;
                break;
            }
        }
        return resultado;
    }
    
    public static Laboratorio buscarLaboratorio(int id, ArrayList<Laboratorio> lista) {
        
        Laboratorio resultado = null;
      
        for (Laboratorio laboratorio : lista) {

            if (laboratorio.getIdLaboratorios()==id) {

                resultado = laboratorio;
                break;
            }
        }
        return resultado;
    }

    public static int actualizarLaboratorio(Connection connection, Laboratorio laboratorio) {

        String sSQL;
        int i = 0;

        sSQL = "UPDATE laboratorios SET nombre_fantasia=?, cuit=?, direccion=?, numero_direccion=?, observaciones=?, "
                + "celular=?, telefono=?, idLocalidades=?, mail=?, paginaWeb=?, "
                + "horario=?, especialidad=?, alta=?, baja=?, SSSRP=?, SSSRPvenc=?, "
                + "altaSIPROSA=?, vencimientoSIPROSA=?, nHabilitacionSIPROSA=? WHERE idLaboratorios =? ";
       

            PreparedStatement pst;
        try {
            pst = connection.prepareStatement(sSQL);
       

            pst.setString(1, laboratorio.getNombre());
            pst.setLong(2, laboratorio.getCuit());
            pst.setString(3, laboratorio.getDireccion());
            pst.setString(4, laboratorio.getNumero());
            pst.setString(5, laboratorio.getObservaciones());
            pst.setLong(6, laboratorio.getCelular());
            pst.setLong(7, laboratorio.getTelefono());
            pst.setInt(8, laboratorio.getIdLocalidades());
            pst.setString(9, laboratorio.getMail());
            pst.setString(10, laboratorio.getPaginaWeb());
            pst.setString(11, laboratorio.getHorario());
            pst.setString(12, laboratorio.getEspecialidad());
            pst.setDate(13, laboratorio.getFechaAlta());
            pst.setDate(14, laboratorio.getFechaBaja());
            pst.setString(15, laboratorio.getSSSRP());
            pst.setDate(16, laboratorio.getSSSRPvenc());
            pst.setDate(17, laboratorio.getAltaSIPROSA());
            pst.setDate(18, laboratorio.getVencimientoSIPROSA());
            pst.setString(19, laboratorio.getnHabilitacionSiprosa());
            pst.setInt(20, laboratorio.getIdLaboratorios());            

            i = pst.executeUpdate();

         } catch (SQLException ex) {
             i=0;
            Logger.getLogger(Laboratorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }

    public static int insertarLaboratorioSP(Connection connection, Laboratorio laboratorio) {

        String sSQL;
        int i = 0;

        sSQL = "CALL insertar_laboratorio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        CallableStatement sp_insertar_lab = null;

       
        try {
            sp_insertar_lab = connection.prepareCall(sSQL);        

            sp_insertar_lab.setString(1, laboratorio.getNombre());
            System.out.println(laboratorio.getNombre());
            sp_insertar_lab.setLong(2, laboratorio.getCuit());
            System.out.println(laboratorio.getCuit());            
            sp_insertar_lab.setString(3, laboratorio.getDireccion());
            System.out.println(laboratorio.getDireccion());            
            sp_insertar_lab.setString(4, laboratorio.getNumero());
            System.out.println(laboratorio.getNumero());            
            sp_insertar_lab.setString(5, laboratorio.getObservaciones());
            System.out.println(laboratorio.getObservaciones()); 
            sp_insertar_lab.setLong(6, laboratorio.getCelular());
            System.out.println(laboratorio.getCelular());
            sp_insertar_lab.setLong(7, laboratorio.getTelefono());
            System.out.println(laboratorio.getTelefono());
            sp_insertar_lab.setInt(8, laboratorio.getIdLocalidades());
            System.out.println(laboratorio.getIdLocalidades());
            sp_insertar_lab.setString(9, laboratorio.getMail());
            System.out.println(laboratorio.getMail());
            sp_insertar_lab.setString(10, laboratorio.getPaginaWeb());
            System.out.println(laboratorio.getPaginaWeb());
            sp_insertar_lab.setString(11, laboratorio.getHorario());
            System.out.println(laboratorio.getHorario());
            sp_insertar_lab.setString(12, laboratorio.getEspecialidad());
            System.out.println(laboratorio.getEspecialidad());
            sp_insertar_lab.setDate(13, laboratorio.getFechaAlta());
            System.out.println(laboratorio.getFechaAlta());
            sp_insertar_lab.setDate(14, laboratorio.getFechaBaja());
            System.out.println(laboratorio.getFechaBaja());
            sp_insertar_lab.setString(15, laboratorio.getSSSRP());
            System.out.println(laboratorio.getSSSRP());
            sp_insertar_lab.setDate(16, laboratorio.getSSSRPvenc());
            System.out.println(laboratorio.getSSSRPvenc());
            sp_insertar_lab.setDate(17, laboratorio.getAltaSIPROSA());
            System.out.println(laboratorio.getAltaSIPROSA());
            sp_insertar_lab.setDate(18, laboratorio.getVencimientoSIPROSA());
            sp_insertar_lab.setString(19, laboratorio.getnHabilitacionSiprosa());
            sp_insertar_lab.registerOutParameter(20, java.sql.Types.INTEGER);
            sp_insertar_lab.execute();
            i=sp_insertar_lab.getInt(20);           
} catch (SQLException ex) {
            Logger.getLogger(Laboratorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
        @Override
    public String toString() {
        return nombre;

    }

}
