package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Optional;
import javax.swing.JOptionPane;

public class ObraSocialAfip {

    private int idObraSocial;
    private String nombre;
    private String direccion;
    private long cuit;
    private String descripcion;
    private String nombre_provincia;

    public ObraSocialAfip(int idObraSocial, String nombre, String direccion, long cuit, String descripcion, String nombre_provincia) {
        this.idObraSocial = idObraSocial;
        this.nombre = nombre;
        this.direccion = direccion;
        this.cuit = cuit;
        this.descripcion = descripcion;
        this.nombre_provincia = nombre_provincia;
    }

    public int getIdObraSocial() {
        return idObraSocial;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public long getCuit() {
        return cuit;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getNombre_provincia() {
        return nombre_provincia;
    }

    public void setIdObraSocial(int idObraSocial) {
        this.idObraSocial = idObraSocial;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setCuit(long cuit) {
        this.cuit = cuit;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setNombre_provincia(String nombre_provincia) {
        this.nombre_provincia = nombre_provincia;
    }

    public static void cargarOSAfip(Connection connection, ArrayList<ObraSocialAfip> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT idObrasocialDato,razonSocial,direccion,cuit,descripcion,nombre_provincia FROM vista_obrasociales_datos ORDER BY razonSocial");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new ObraSocialAfip(rs.getInt("idObrasocialDato"), rs.getString("razonSocial"), rs.getString("direccion"), rs.getLong("cuit"), rs.getString("descripcion"), rs.getString("nombre_provincia")));
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public static ObraSocialAfip buscarOS(String nombre, ArrayList<ObraSocialAfip> lista) {
        ObraSocialAfip resultado = null;
        for (ObraSocialAfip obras : lista) {
            if ((obras.getNombre()).equals(nombre)) {
                resultado = obras;
                break;
            }
        }
        return resultado;
    }

     public static Optional<ObraSocialAfip> buscarOSAfip(String stringOrden, ArrayList<ObraSocialAfip> listaOS) {
        

        for (ObraSocialAfip os : listaOS) {
            if (os.getNombre().equals(stringOrden)) {
                
                return Optional.of(os);
                
             }
        }
        return Optional.empty();
    }
    
    @Override
    public String toString() {
        return nombre;

    }

}
