
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class ColegiadoLaboratorio {

    
    private int idColegiado;
    private int director;
    private int facturante;

    public ColegiadoLaboratorio(int idColegiado, int director, int facturante) {
        this.idColegiado = idColegiado;
        this.director = director;
        this.facturante = facturante;
    }

    public int getIdColegiado() {
        return idColegiado;
    }

    public void setIdColegiado(int idColegiado) {
        this.idColegiado = idColegiado;
    }

    public int getDirector() {
        return director;
    }

    public void setDirector(int director) {
        this.director = director;
    }

    public int getFacturante() {
        return facturante;
    }

    public void setFacturante(int facturante) {
        this.facturante = facturante;
    }
    
    public static ColegiadoLaboratorio buscarColab(int idColegiado, ArrayList<ColegiadoLaboratorio> lista) {
        
        ColegiadoLaboratorio resultado = null;

        for (ColegiadoLaboratorio colab : lista) {

            if (colab.getIdColegiado() == idColegiado) {
                
                resultado = colab;
                break;

            }
        }

        return resultado;
    }
    
    public static int agregarColegiadoLab (Connection connection, ColegiadoLaboratorio colab, int idLaboratorio){
        
                  
        String consulta = "INSERT INTO laboratorios_tienen_colegiados (idLaboratorios, idColegiados, director, factura) VALUES(?,?,?,?)";
        int i=0;
         try {

            PreparedStatement pst = connection.prepareStatement(consulta);

            pst.setInt(1, idLaboratorio);
            pst.setInt(2, colab.getIdColegiado());
            pst.setInt(3, colab.getDirector());
            pst.setInt(4, colab.getFacturante());            

            i = pst.executeUpdate();
            return 1;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }

    public static void cargarColegiadoLab(Connection connection, ArrayList<ColegiadoLaboratorio> lista, Laboratorio lab) {

        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs;
            rs = instruccion.executeQuery("SELECT * FROM laboratorios_tienen_colegiados where idLaboratorios="+lab.getIdLaboratorios());
            while (rs.next()) {
                lista.add(new ColegiadoLaboratorio(rs.getInt("idColegiados"), rs.getInt("director"), rs.getInt("factura")));

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void borrarColegiadosLab(Connection connection, Laboratorio lab) {

        try {
            PreparedStatement pst = connection.prepareStatement("DELETE FROM laboratorios_tienen_colegiados where idLaboratorios="+lab.getIdLaboratorios());
            
            int i = pst.executeUpdate();
            System.out.println(i);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return idColegiado+"";
    }
    
    
}
