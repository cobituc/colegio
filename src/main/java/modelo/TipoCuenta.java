
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TipoCuenta {
    
    private int idCuenta;
    private String nombre;

    public TipoCuenta(int idCuenta, String nombre) {
        this.idCuenta = idCuenta;
        this.nombre = nombre;
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
    public static void cargarTipoCuenta(Connection connection, ArrayList<TipoCuenta> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT idCuenta,cuentaNombre FROM tipo_cuenta ORDER BY idCuenta");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new TipoCuenta(rs.getInt("idCuenta"), rs.getString("cuentaNombre")));               
            }
        } catch (SQLException e) {
        }
    }
    
    public static TipoCuenta buscarTipoCuenta(String nombre, ArrayList<TipoCuenta> lista) {
        TipoCuenta resultado = null;
        for (TipoCuenta tipo : lista) {
            if (tipo.getNombre().equals(nombre)) {
                resultado = tipo;
                break;
            }
        }
        return resultado;
    }
    
    public static void insertarTipoCuenta(Connection connection, TipoCuenta tipoCuenta) {

        String sSQL;
        int i = 0;

        sSQL = "INSERT INTO tipo_cuenta(CuentaNombre) VALUES(?)";

        try {

            PreparedStatement pst = connection.prepareStatement(sSQL);

            pst.setString(1, tipoCuenta.getNombre());

            i = pst.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static boolean actualizarTipoCuenta(Connection connection, TipoCuenta tipo){
         
         int update = 0;         
            
            String sSQL;
            PreparedStatement pst;            
            
            sSQL = "UPDATE tipo_cuenta SET CuentaNombre=? WHERE idCuenta = "+tipo.getIdCuenta();
        try {
            pst = connection.prepareStatement(sSQL);
       
            pst.setString(1, tipo.getNombre());            
            update = pst.executeUpdate();     

        } catch (SQLException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return update>0;
     }
    
    @Override
    public String toString() {
        return nombre;
    }
}
