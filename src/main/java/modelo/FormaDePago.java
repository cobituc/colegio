package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class FormaDePago {
    
    private int idFormaDePago;
    private String nombre;

    public FormaDePago(String nombre,int idFormaDePago) {
        this.nombre = nombre;
        this.idFormaDePago=idFormaDePago;
    }
    
//    public String cboFormaDePago (String nombre){
//        this.nombre = nombre;
//    }

    public int getIdFormaDePago() {
        return idFormaDePago;
    }

    public void setIdFormaDePago(int idFormaDePago) {
        this.idFormaDePago = idFormaDePago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
    public static void cargarFormaDePago(Connection connection, ArrayList<FormaDePago> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT idFormasDePago,nombre FROM formasdepago ORDER BY idFormasDePago");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new FormaDePago(rs.getString("nombre"),rs.getInt("idFormasDePago")));               
            }
        } catch (SQLException e) {
             e.printStackTrace();
        }
    }
    
    public static FormaDePago buscarFormaDePago(String nombre, ArrayList<FormaDePago> lista) {
        FormaDePago resultado = null;
        
        for (FormaDePago forma : lista) {
            if (forma.getNombre().equals(nombre)) {
                resultado = forma;
                break;
            }
        }
        return resultado;
    }
    
    @Override
    public String toString() {
        return nombre;
    }
}