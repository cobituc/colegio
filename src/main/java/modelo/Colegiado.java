
package modelo;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class Colegiado {
    
    private int idColegiados;
    private long cuil;
    private String matricula;
    private String nombre;
    private int nDeActa;
    private int nDeFolio;
    private int nDeLibro;
    private long celular;
    private int cPostal;
    private String direccion;
    private String especialidad;
    private Date fechaDoctorado;
    private Date fechaEgreso;
    private Date fechaNacimiento;
    private String localidad;
    private String mail;
    private long documento;
    private int numeroTitulo;
    private long telefono;
    private String tipoDoc;
    private Date fechaAlta;
    private String tipoProfesional;
    private String sexo;
    private String localidadNacimiento;
    private String universidad;
    private String facultad;
    private Blob blob;
    private String nacionalidad;
    private int numeroDireccion;
    private String pisoDireccion;
    private String deptoDireccion;
    private String estadoBioquimico;
    private int estadoPeec;

    
    public Colegiado(){}
    
    public Colegiado(int idColegiados, long cuil, String matricula, String nombre, int nDeActa, int nDeFolio, int nDeLibro, long celular, int cPostal, String direccion, String especialidad, Date fechaDoctorado, Date fechaEgreso, Date fechaNacimiento, String localidad, String mail, long documento, int numeroTitulo, long telefono, String tipoDoc, Date fechaAlta, String tipoProfesional, String sexo, String localidadNacimiento, String universidad, String facultad, Blob blob, String nacionalidad, int numeroDireccion, String pisoDireccion, String deptoDireccion, String estadoBioquimico) {
        this.idColegiados = idColegiados;
        this.cuil = cuil;
        this.matricula = matricula;
        this.nombre = nombre;
        this.nDeActa = nDeActa;
        this.nDeFolio = nDeFolio;
        this.nDeLibro = nDeLibro;
        this.celular = celular;
        this.cPostal = cPostal;
        this.direccion = direccion;
        this.especialidad = especialidad;
        this.fechaDoctorado = fechaDoctorado;
        this.fechaEgreso = fechaEgreso;
        this.fechaNacimiento = fechaNacimiento;
        this.localidad = localidad;
        this.mail = mail;
        this.documento = documento;
        this.numeroTitulo = numeroTitulo;
        this.telefono = telefono;
        this.tipoDoc = tipoDoc;
        this.fechaAlta = fechaAlta;
        this.tipoProfesional = tipoProfesional;
        this.sexo = sexo;
        this.localidadNacimiento = localidadNacimiento;
        this.universidad = universidad;
        this.facultad = facultad;
        this.blob = blob;
        this.nacionalidad = nacionalidad;
        this.numeroDireccion = numeroDireccion;
        this.pisoDireccion = pisoDireccion;
        this.deptoDireccion = deptoDireccion;
        this.estadoBioquimico = estadoBioquimico;
    }

    public Colegiado(long cuil, String matricula, String nombre, int nDeActa, int nDeFolio, int nDeLibro, long celular, int cPostal, String direccion, String especialidad, Date fechaDoctorado, Date fechaEgreso, Date fechaNacimiento, String localidad, String mail, long documento, int numeroTitulo, long telefono, String tipoDoc, Date fechaAlta, String tipoProfesional, String sexo, String localidadNacimiento, String universidad, String facultad, Blob blob, String nacionalidad) {
        this.cuil = cuil;
        this.matricula = matricula;
        this.nombre = nombre;
        this.nDeActa = nDeActa;
        this.nDeFolio = nDeFolio;
        this.nDeLibro = nDeLibro;
        this.celular = celular;
        this.cPostal = cPostal;
        this.direccion = direccion;
        this.especialidad = especialidad;
        this.fechaDoctorado = fechaDoctorado;
        this.fechaEgreso = fechaEgreso;
        this.fechaNacimiento = fechaNacimiento;
        this.localidad = localidad;
        this.mail = mail;
        this.documento = documento;
        this.numeroTitulo = numeroTitulo;
        this.telefono = telefono;
        this.tipoDoc = tipoDoc;
        this.fechaAlta = fechaAlta;
        this.tipoProfesional = tipoProfesional;
        this.sexo = sexo;
        this.localidadNacimiento = localidadNacimiento;
        this.universidad = universidad;
        this.facultad = facultad;
        this.blob = blob;
        this.nacionalidad = nacionalidad;
    }

    public Colegiado(long cuil, String matricula, String nombre, int nDeActa, int nDeFolio, int nDeLibro, long celular, int cPostal, String direccion, String especialidad, Date fechaDoctorado, Date fechaEgreso, Date fechaNacimiento, String localidad, String mail, long documento, int numeroTitulo, long telefono, String tipoDoc, Date fechaAlta, String tipoProfesional, String sexo, String localidadNacimiento, String universidad, String facultad, String nacionalidad) {
        this.cuil = cuil;
        this.matricula = matricula;
        this.nombre = nombre;
        this.nDeActa = nDeActa;
        this.nDeFolio = nDeFolio;
        this.nDeLibro = nDeLibro;
        this.celular = celular;
        this.cPostal = cPostal;
        this.direccion = direccion;
        this.especialidad = especialidad;
        this.fechaDoctorado = fechaDoctorado;
        this.fechaEgreso = fechaEgreso;
        this.fechaNacimiento = fechaNacimiento;
        this.localidad = localidad;
        this.mail = mail;
        this.documento = documento;
        this.numeroTitulo = numeroTitulo;
        this.telefono = telefono;
        this.tipoDoc = tipoDoc;
        this.fechaAlta = fechaAlta;
        this.tipoProfesional = tipoProfesional;
        this.sexo = sexo;
        this.localidadNacimiento = localidadNacimiento;
        this.universidad = universidad;
        this.facultad = facultad;
        this.nacionalidad = nacionalidad;
    }

    public Colegiado(int idColegiados, long cuil, String matricula, String nombre, int nDeActa, int nDeFolio, int nDeLibro, long celular, int cPostal, String direccion, String especialidad, Date fechaDoctorado, Date fechaEgreso, Date fechaNacimiento, String localidad, String mail, long documento, int numeroTitulo, long telefono, String tipoDoc, Date fechaAlta, String tipoProfesional, String sexo, String localidadNacimiento, String universidad, String facultad, Blob blob, String nacionalidad, int numeroDireccion, String pisoDireccion, String deptoDireccion, String estadoBioquimico, int estadoPeec) {
        this.idColegiados = idColegiados;
        this.cuil = cuil;
        this.matricula = matricula;
        this.nombre = nombre;
        this.nDeActa = nDeActa;
        this.nDeFolio = nDeFolio;
        this.nDeLibro = nDeLibro;
        this.celular = celular;
        this.cPostal = cPostal;
        this.direccion = direccion;
        this.especialidad = especialidad;
        this.fechaDoctorado = fechaDoctorado;
        this.fechaEgreso = fechaEgreso;
        this.fechaNacimiento = fechaNacimiento;
        this.localidad = localidad;
        this.mail = mail;
        this.documento = documento;
        this.numeroTitulo = numeroTitulo;
        this.telefono = telefono;
        this.tipoDoc = tipoDoc;
        this.fechaAlta = fechaAlta;
        this.tipoProfesional = tipoProfesional;
        this.sexo = sexo;
        this.localidadNacimiento = localidadNacimiento;
        this.universidad = universidad;
        this.facultad = facultad;
        this.blob = blob;
        this.nacionalidad = nacionalidad;
        this.numeroDireccion = numeroDireccion;
        this.pisoDireccion = pisoDireccion;
        this.deptoDireccion = deptoDireccion;
        this.estadoBioquimico = estadoBioquimico;
        this.estadoPeec = estadoPeec;
    }

    public int getEstadoPeec() {
        return estadoPeec;
    }

    public void setEstadoPeec(int estadoPeec) {
        this.estadoPeec = estadoPeec;
    }
    
    

    
    
    
    public int getIdColegiados() {
        return idColegiados;
    }

    public void setIdColegiados(int idColegiados) {
        this.idColegiados = idColegiados;
    }

    public long getCuil() {
        return cuil;
    }

    public void setCuil(long cuil) {
        this.cuil = cuil;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getnDeActa() {
        return nDeActa;
    }

    public void setnDeActa(int nDeActa) {
        this.nDeActa = nDeActa;
    }

    public int getnDeFolio() {
        return nDeFolio;
    }

    public void setnDeFolio(int nDeFolio) {
        this.nDeFolio = nDeFolio;
    }

    public int getnDeLibro() {
        return nDeLibro;
    }

    public void setnDeLibro(int nDeLibro) {
        this.nDeLibro = nDeLibro;
    }

    public long getCelular() {
        return celular;
    }

    public void setCelular(long celular) {
        this.celular = celular;
    }

    public int getcPostal() {
        return cPostal;
    }

    public void setcPostal(int cPostal) {
        this.cPostal = cPostal;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public Date getFechaDoctorado() {
        return fechaDoctorado;
    }

    public void setFechaDoctorado(Date fechaDoctorado) {
        this.fechaDoctorado = fechaDoctorado;
    }

    public Date getFechaEgreso() {
        return fechaEgreso;
    }

    public void setFechaEgreso(Date fechaEgreso) {
        this.fechaEgreso = fechaEgreso;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public long getDocumento() {
        return documento;
    }

    public void setDocumento(long documento) {
        this.documento = documento;
    }

    public int getNumeroTitulo() {
        return numeroTitulo;
    }

    public void setNumeroTitulo(int numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getTipoProfesional() {
        return tipoProfesional;
    }

    public void setTipoProfesional(String tipoProfesional) {
        this.tipoProfesional = tipoProfesional;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getLocalidadNacimiento() {
        return localidadNacimiento;
    }

    public void setLocalidadNacimiento(String localidadNacimiento) {
        this.localidadNacimiento = localidadNacimiento;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public Blob getImagen() {
        return blob;
    }

    public void setBlob(Blob blob) {
        this.blob = blob;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public int getNumeroDireccion() {
        return numeroDireccion;
    }

    public void setNumeroDireccion(int numeroDireccion) {
        this.numeroDireccion = numeroDireccion;
    }

    public String getPisoDireccion() {
        return pisoDireccion;
    }

    public void setPisoDireccion(String pisoDireccion) {
        this.pisoDireccion = pisoDireccion;
    }

    public String getDeptoDireccion() {
        return deptoDireccion;
    }

    public void setDeptoDireccion(String deptoDireccion) {
        this.deptoDireccion = deptoDireccion;
    }

    public String getEstadoBioquimico() {
        return estadoBioquimico;
    }

    public void setEstadoBioquimico(String estadoBioquimico) {
        this.estadoBioquimico = estadoBioquimico;
    }   
    
     public static void cargarColegiado(Connection connection, ArrayList<Colegiado> lista) {
        
         try {
            Statement instruccion = connection.createStatement();
            ResultSet rs;
            rs = instruccion.executeQuery("SELECT id_colegiados, cuil_colegiado, matricula_colegiado,"
                    + " nombre_colegiado, ndeacta_colegiado, ndefolio_colegiado, ndelibro_colegiado,"
                    + " celular_particular, codigopostal_particular, direccion_particular, especialidad_particular, "
                    + "fechadedoctorado_particular, fechadeegreso_particular, fechadenacimiento_particular, "
                    + "localidad_particular, mail_particular, numerodedocumento_particular, numerodetitulo_particular, telefono_particular, "
                    + "tipodedocumento_particular, fecha_alta_colegiado, tipo_profesional, sexo, lugar_de_nacimiento, "
                    + "universidad, facultad, foto_particular, nacionalidad, numero_direccion, piso_direccion, departamento_direccion, estado_bioquimico,"
                    + " estadoPEEC FROM colegiados");
            while (rs.next()) {
                lista.add(new Colegiado(rs.getInt("id_colegiados"), rs.getLong("cuil_colegiado"), 
                        rs.getString("matricula_colegiado"), rs.getString("nombre_colegiado"), rs.getInt("ndeacta_colegiado"), 
                        rs.getInt("ndefolio_colegiado"), rs.getInt("ndelibro_colegiado"), rs.getLong("celular_particular"), 
                        rs.getInt("codigopostal_particular"), rs.getString("direccion_particular"), 
                        rs.getString("especialidad_particular"), rs.getDate("fechadedoctorado_particular"), 
                        rs.getDate("fechadeegreso_particular"), rs.getDate("fechadenacimiento_particular"), 
                        rs.getString("localidad_particular"), rs.getString("mail_particular"), 
                        rs.getInt("numerodedocumento_particular"), rs.getInt("numerodetitulo_particular"), 
                        rs.getInt("telefono_particular"), rs.getString("tipodedocumento_particular"), 
                        rs.getDate("fecha_alta_colegiado"), rs.getString("tipo_profesional"), 
                        rs.getString("sexo"), rs.getString("lugar_de_nacimiento"), rs.getString("universidad"), 
                        rs.getString("facultad"), rs.getBlob("foto_particular"), rs.getString("nacionalidad"), 
                        rs.getInt("numero_direccion"), rs.getString("piso_direccion"), rs.getString("departamento_direccion"), 
                        rs.getString("estado_bioquimico"), rs.getInt("estadoPEEC")));
                
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
     
     public static Colegiado buscarColegiado(String matricula, ArrayList<Colegiado> lista) {
        Colegiado resultado = null;

        for (Colegiado colegiado : lista) {

            if (colegiado.getMatricula().equals(matricula) ) {

                resultado = colegiado;
                break;

            }
        }

        return resultado;
    }
     
     public static Colegiado buscarColegiadoCompleto(String completo, ArrayList<Colegiado> lista) {
        Colegiado resultado = null;

        for (Colegiado colegiado : lista) {

            if (colegiado.toString().equals(completo) ) {

                resultado = colegiado;
                break;

            }
        }

        return resultado;
    }
     
     public static Colegiado buscarColegiado(int id, ArrayList<Colegiado> lista) {
        Colegiado resultado = null;

        for (Colegiado colegiado : lista) {

            if (colegiado.getIdColegiados()==id ) {

                resultado = colegiado;
                break;

            }
        }

        return resultado;
    }
     
     public static boolean ingresarColegiado(Connection connection, Colegiado colegiado){
         
         String sSQL;
         int i = 0;
                        
        try {
            
            sSQL = "INSERT INTO colegiados(cuil_colegiado, matricula_colegiado, nombre_colegiado,"
                    + " ndeacta_colegiado, ndefolio_colegiado, ndelibro_colegiado, celular_particular, "
                    + "codigopostal_particular, direccion_particular, especialidad_particular, fechadedoctorado_particular,"
                    + " fechadeegreso_particular,"
                    + " fechadenacimiento_particular, localidad_particular, mail_particular, numerodedocumento_particular, "
                    + "numerodetitulo_particular, telefono_particular, tipodedocumento_particular, fecha_alta_colegiado, "
                    + "tipo_profesional, sexo, lugar_de_nacimiento, universidad, facultad, foto_particular, nacionalidad,estado_colegiado, "
                    + "numero_direccion, piso_direccion, departamento_direccion, estado_bioquimico, estadoPEEC) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pst = connection.prepareStatement(sSQL);
            
            pst.setLong(1, colegiado.getCuil());
            pst.setString(2, colegiado.getMatricula());
            pst.setString(3, colegiado.getNombre());
            pst.setInt(4, colegiado.getnDeActa());
            pst.setInt(5, colegiado.getnDeFolio());
            pst.setInt(6, colegiado.getnDeLibro());
            pst.setLong(7, colegiado.getCelular());
            pst.setInt(8, colegiado.getcPostal());
            pst.setString(9, colegiado.getDireccion());
            pst.setString(10, colegiado.getEspecialidad());
            pst.setDate(11, colegiado.getFechaDoctorado());
            pst.setDate(12, colegiado.getFechaEgreso());
            pst.setDate(13, colegiado.getFechaNacimiento());
            pst.setString(14, colegiado.getLocalidad());
            pst.setString(15, colegiado.getMail());
            pst.setLong(16, colegiado.getDocumento());
            pst.setInt(17, colegiado.getNumeroTitulo());
            pst.setLong(18, colegiado.getTelefono());
            pst.setString(19, colegiado.getTipoDoc());
            pst.setDate(20, colegiado.getFechaAlta());                       
            pst.setString(21, colegiado.getTipoProfesional());
            pst.setString(22, colegiado.getSexo());
            pst.setString(23, colegiado.getLocalidadNacimiento());
            pst.setString(24, colegiado.getUniversidad());
            pst.setString(25, colegiado.getFacultad());
            pst.setBlob(26, colegiado.getImagen());            
            pst.setString(27, colegiado.getNacionalidad());
            pst.setString(28, "ACTIVO");
            pst.setInt(29, colegiado.getNumeroDireccion());
            pst.setString(30, colegiado.getPisoDireccion());
            pst.setString(31, colegiado.getDeptoDireccion());
            pst.setString(32, colegiado.getEstadoBioquimico());
            pst.setInt(33, colegiado.getEstadoPeec());
            i = pst.executeUpdate();            

         } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        if(i>0){
            return true;
        }else{
            return false;
        }
     }
     
     public static boolean actualizarColegiado(Connection connection, Colegiado colegiado){
         
         int update = 0;
         try {
            
            String sSQL;
            PreparedStatement pst;            
            
            sSQL = "UPDATE colegiados SET cuil_colegiado=?, matricula_colegiado=?, nombre_colegiado=?,"
                    + " ndeacta_colegiado=?, ndefolio_colegiado=?, ndelibro_colegiado=?, celular_particular=?, "
                    + "codigopostal_particular=?, direccion_particular=?, especialidad_particular=?, fechadedoctorado_particular=?,"
                    + " fechadeegreso_particular=?, fechadenacimiento_particular=?, "
                    + "localidad_particular=?, mail_particular=?, numerodedocumento_particular=?, "
                    + "numerodetitulo_particular=?, telefono_particular=?, "
                    + "tipodedocumento_particular=?, fecha_alta_colegiado=?, tipo_profesional=?,"
                    + " sexo=?, lugar_de_nacimiento=?, universidad=?, facultad=?, foto_particular=?,"
                    + " nacionalidad=?,estado_colegiado=?, numero_direccion=?, piso_direccion=?, departamento_direccion=?, estado_bioquimico=?, estadoPEEC=?"
                    + " WHERE id_colegiados="+colegiado.getIdColegiados();
            pst = connection.prepareStatement(sSQL);
            pst.setLong(1, colegiado.getCuil());
            pst.setString(2, colegiado.getMatricula());
            pst.setString(3, colegiado.getNombre());
            pst.setInt(4, colegiado.getnDeActa());
            pst.setInt(5, colegiado.getnDeFolio());
            pst.setInt(6, colegiado.getnDeFolio());
            pst.setLong(7,colegiado.getCelular());
            pst.setInt(8, colegiado.getcPostal());
            pst.setString(9, colegiado.getDireccion());
            pst.setString(10, colegiado.getEspecialidad());
            pst.setDate(11, colegiado.getFechaDoctorado());
            pst.setDate(12, colegiado.getFechaEgreso());
            pst.setDate(13, colegiado.getFechaNacimiento());
            pst.setString(14, colegiado.getLocalidad());
            pst.setString(15, colegiado.getMail());
            pst.setLong(16, colegiado.getDocumento());
            pst.setInt(17, colegiado.getNumeroTitulo());
            pst.setLong(18, colegiado.getTelefono());
            pst.setString(19, colegiado.getTipoDoc());
            pst.setDate(20, colegiado.getFechaAlta());            
            pst.setString(21, colegiado.getTipoProfesional());           
            pst.setString(22, colegiado.getSexo());            
            pst.setString(23, colegiado.getLocalidadNacimiento());
            pst.setString(24, colegiado.getUniversidad());
            pst.setString(25, colegiado.getFacultad());
            pst.setBlob(26, colegiado.getImagen());            
            pst.setString(27, colegiado.getNacionalidad());
            pst.setString(28, "ACTIVO");
            if(colegiado.getNumeroDireccion()>0){
            pst.setInt(29, colegiado.getNumeroDireccion());
            }else{
                pst.setString(29, null);
            }
            pst.setString(30, colegiado.getPisoDireccion());
            pst.setString(31, colegiado.getDeptoDireccion());
            pst.setString(32, colegiado.getEstadoBioquimico());
            pst.setInt(33, colegiado.getEstadoPeec());
            update = pst.executeUpdate();
           

        } catch (SQLException e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
         
        return update>0;
     }
     
    @Override
    public String toString() {
        return matricula + " - " + nombre;
        
    }

}
