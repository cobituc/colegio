
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Banco {
    
    private int idBanco;
    private String nombre;

    public Banco(int idBanco, String nombre) {
        this.idBanco = idBanco;
        this.nombre = nombre;
    }
        
    public int getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(int idBanco) {
        this.idBanco = idBanco;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    } 
    
    
    
    public static void cargarBanco(Connection connection, ArrayList<Banco> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT idBanco,BancoNombre FROM banco ORDER BY idBanco");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new Banco(rs.getInt("idBanco"), rs.getString("BancoNombre")));               
            }
        } catch (SQLException e) {
        }
    }
    
    public static Banco buscarBanco(String nombre, ArrayList<Banco> lista) {
        Banco resultado = null;
        for (Banco tipo : lista) {
            if (tipo.getNombre().equals(nombre)) {
                resultado = tipo;
                break;
            }
        }
        return resultado;
    }
    
    public static void insertarBanco(Connection connection, Banco banco) {

        String sSQL;
        int i = 0;

        sSQL = "INSERT INTO banco(BancoNombre) VALUES(?)";

        try {

            PreparedStatement pst = connection.prepareStatement(sSQL);

            pst.setString(1, banco.getNombre());

            i = pst.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
        
    public static boolean actualizarBanco(Connection connection, Banco banco){
         
         int update = 0;         
            
            String sSQL;
            PreparedStatement pst;            
            
            sSQL = "UPDATE banco SET NombreBanco=? WHERE idBanco = "+banco.getIdBanco();
        try {
            pst = connection.prepareStatement(sSQL);
       
            pst.setString(1, banco.getNombre());            
            update = pst.executeUpdate();     

        } catch (SQLException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return update>0;
     }
    
    @Override
    public String toString() {
        return nombre;
    }
}
