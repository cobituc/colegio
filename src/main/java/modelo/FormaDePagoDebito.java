package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class FormaDePagoDebito {
    
    private int idFormaDePago;
    private String nombre;
    private double porcentaje;
    

    public FormaDePagoDebito(int idFormaDePago,String nombre,double porcentaje) {
        this.nombre = nombre;
        this.idFormaDePago=idFormaDePago;
        this.porcentaje=porcentaje;
    }
    
    public double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }

//    }
    public int getIdFormaDePago() {
        return idFormaDePago;
    }

    public void setIdFormaDePago(int idFormaDePago) {
        this.idFormaDePago = idFormaDePago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
    public static void cargarDetalleDePago(Connection connection, ArrayList<FormaDePagoDebito> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT id_tarjeta,descripcion,porcentaje FROM forma_tarjeta_debito ORDER BY id_tarjeta");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new FormaDePagoDebito(rs.getInt("id_tarjeta"),rs.getString("descripcion"),rs.getDouble("porcentaje")));               
            }
        } catch (SQLException e) {
             e.printStackTrace();
        }
    }
    
    public static FormaDePagoDebito buscarDetalleFormaDePago(String nombre, ArrayList<FormaDePagoDebito> lista) {
        FormaDePagoDebito resultado = null;
        
        for (FormaDePagoDebito forma : lista) {
            if (forma.getNombre().equals(nombre)) {
                resultado = forma;
                break;
            }
        }
        return resultado;
    }
    
    @Override
    public String toString() {
        return nombre;
    }
}