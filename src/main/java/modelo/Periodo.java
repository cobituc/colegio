
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Periodo {
    
    private int periodo;

    public Periodo(int periodo) {
        this.periodo = periodo;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }
    
    @Override
    public String toString() {
        return periodo+"";
    }

   
    public static void cargarPeriodos(Connection connection, ArrayList<Periodo> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT DISTINCT(periodo) FROM liquidacion_fecha");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new Periodo(rs.getInt(1)));               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static Periodo buscarPeriodo(int periodo, ArrayList<Periodo> lista) {
        Periodo resultado = null;
        for (Periodo periodos : lista) {
            if (periodos.getPeriodo()==periodo) {
                resultado = periodos;
                break;
            }
        }
        return resultado;
    }
}