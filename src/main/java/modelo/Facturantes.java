
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Facturantes {
    
    private String matricula;
    private String nombre;    
    private String laboratorio;
    private int factura;
    private int director;
    private String usuario;
    private String contraseña;
    private String contraseñaFac;

    
    public Facturantes(){}   

    public Facturantes(String matricula, String nombre, String laboratorio, int factura, int director, String usuario, String contraseña, String contraseñaFac) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.laboratorio = laboratorio;
        this.factura = factura;
        this.director = director;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.contraseñaFac = contraseñaFac;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio = laboratorio;
    }

    public int getFactura() {
        return factura;
    }

    public void setFactura(int factura) {
        this.factura = factura;
    }

    public int getDirector() {
        return director;
    }

    public void setDirector(int director) {
        this.director = director;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getContraseñaFac() {
        return contraseñaFac;
    }

    public void setContraseñaFac(String contraseñaFac) {
        this.contraseñaFac = contraseñaFac;
    }
    
    
    
    
     public static void cargarFacturante(Connection connection, ArrayList<Facturantes> lista) {
        
         try {
            Statement instruccion = connection.createStatement();
            ResultSet rs;
            rs = instruccion.executeQuery("SELECT * FROM vista_facturante");
            while (rs.next()) {
                lista.add(new Facturantes(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8)));
                
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
     
     public static Facturantes buscarFacturante(String matricula, String laboratorio, ArrayList<Facturantes> lista) {
        Facturantes resultado = null;

        for (Facturantes facturante : lista) {

            if (facturante.getMatricula().equals(matricula) && facturante.getLaboratorio().equals(laboratorio)) {
                
                resultado = facturante;
                break;

            }
        }

        return resultado;
    }
     
     public static void actualizarFacturante(Connection connection, int idColegiados, String usuario, String contraseña, String contraseñaFac, int idLaboratorio){
        
                  
        String consulta = "UPDATE laboratorios_tienen_colegiados SET usuario=?, contraseña=?, contraseña_facturacion=? WHERE idLaboratorios = '"+idLaboratorio+"' AND idColegiados= '"+idColegiados+"'";
        int i=0;
         try {

            PreparedStatement pst = connection.prepareStatement(consulta);

            pst.setString(1, usuario);
            pst.setString(2, contraseña);
            pst.setString(3, contraseñaFac);

            i = pst.executeUpdate();

        } catch (SQLException e) {
            
             System.out.println(e.getMessage());
                        
        }
    }
         
     
    @Override
    public String toString() {
        return matricula + " - " + nombre;
        
    }

}
