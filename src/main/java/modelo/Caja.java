/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author CBT-COMPUTOS-05
 */
public class Caja {

    private int idCliente;
    private int tipoMovimiento;
    private int tipoCliente;
    private String[][] datosMovimientos;
    private String[][] datosMovimientosPagos;

    Caja() {
        this.datosMovimientos = null;
        this.datosMovimientosPagos = null;
    }

    public static Caja crearCaja() {
        Caja c1 = new Caja();
        return c1;
    }

    public Caja(int idCliente, int tipoMovimiento, int tipoCliente, String[][] datos) {
        this.idCliente = idCliente;
        this.tipoMovimiento = tipoMovimiento;
        this.tipoCliente = tipoCliente;
    }

    public String[][] getDatosMovimientos() {
        return datosMovimientos;
    }

    public void setDatosMovimientos(String[][] datosMovimientos) {
        this.datosMovimientos = datosMovimientos;
    }

    public String[][] getDatosMovimientosPagos() {
        return datosMovimientosPagos;
    }

    public void setDatosMovimientosPagos(String[][] datosMovimientosPagos) {
        this.datosMovimientosPagos = datosMovimientosPagos;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public void setTipoMovimiento(int tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public void setTipoCliente(int tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public int getTipoMovimiento() {
        return tipoMovimiento;
    }

    public int getTipoCliente() {
        return tipoCliente;
    }

    public static int insertarIngresoCaja(Connection connection, Caja caja) {
        String sSQL;
        Statement UltimoIdMovimiento = null;
        PreparedStatement InsertDetalleMovimineto = null;
        PreparedStatement InsertDetalleMoviminetoPago = null;
        PreparedStatement InsertMovimineto = null;

        int IdMovimiento = 0;
        sSQL = "INSERT INTO movimientos(id_cliente,tipoMovimiento,tipoCliente,fecha) VALUES(?,?,?,CURDATE())";
        try {

            InsertMovimineto = connection.prepareStatement(sSQL);

            connection.setAutoCommit(false); //transaction block start

            InsertMovimineto.setInt(1, caja.getIdCliente());
            InsertMovimineto.setInt(2, caja.getTipoMovimiento());
            InsertMovimineto.setInt(3, caja.getTipoCliente());

            InsertMovimineto.executeUpdate();
            //////////////////////////////////////////////////////////////////////////
            UltimoIdMovimiento = connection.createStatement();
            ResultSet rs = UltimoIdMovimiento.executeQuery("SELECT MAX(id_movimientos) AS id_movimientos FROM movimientos");
            rs.next();
            IdMovimiento = rs.getInt("id_movimientos");
            ///////////////////////////////////////////////////////////////////////////
            String SQLInsertDetalleMovimiento = "INSERT INTO detalle_movimiento (id_movimiento, importe,descripcion,id_cuenta) "
                    + "VALUES (?,?,?,?)";
            InsertDetalleMovimineto = connection.prepareStatement(SQLInsertDetalleMovimiento);
            for (int i = 0; i < caja.getDatosMovimientos().length; i++) {
                InsertDetalleMovimineto.setInt(1, IdMovimiento);
                InsertDetalleMovimineto.setDouble(2, Double.valueOf(caja.getDatosMovimientos()[i][0]));//importe
                InsertDetalleMovimineto.setString(3, caja.getDatosMovimientos()[i][1]);//descripcion
                InsertDetalleMovimineto.setInt(4, Integer.valueOf(caja.getDatosMovimientos()[i][2]));//id_cuenta
                InsertDetalleMovimineto.executeUpdate();
            }
            ///////////////////////////////////////////////////////////////////////////
            String SQLInsertDetalleMovimientoPago = "INSERT INTO movimiento_detalle_pago (id_movimiento, id_forma_de_pago, id_cheque, id_tarjeta, id_banco, importe) "
                    + "VALUES (?,?,?,?,?,?)";
            InsertDetalleMoviminetoPago = connection.prepareStatement(SQLInsertDetalleMovimientoPago);
            for (int i = 0; i < caja.getDatosMovimientosPagos().length; i++) {
                InsertDetalleMoviminetoPago.setInt(1, IdMovimiento);
                InsertDetalleMoviminetoPago.setInt(2, Integer.valueOf(caja.getDatosMovimientosPagos()[i][0]));   //  id_forma_de_pago           
                if (Integer.valueOf(caja.getDatosMovimientosPagos()[i][1]) == 0) {//idCheque
                    InsertDetalleMoviminetoPago.setObject(3, null);
                } else {
                    InsertDetalleMoviminetoPago.setInt(3, Integer.valueOf(caja.getDatosMovimientosPagos()[i][1]));
                }
                if (Integer.valueOf(caja.getDatosMovimientosPagos()[i][2]) == 0) {//id_tarjeta
                    InsertDetalleMoviminetoPago.setObject(4, null);
                } else {
                    InsertDetalleMoviminetoPago.setInt(4, Integer.valueOf(caja.getDatosMovimientosPagos()[i][2]));
                }
                if (Integer.valueOf(caja.getDatosMovimientosPagos()[i][3]) == 0) {//id_banco
                    InsertDetalleMoviminetoPago.setObject(5, null);
                } else {
                    InsertDetalleMoviminetoPago.setInt(5, Integer.valueOf(caja.getDatosMovimientosPagos()[i][3]));
                }
                InsertDetalleMoviminetoPago.setDouble(6, Double.valueOf(caja.getDatosMovimientosPagos()[i][4]));

                InsertDetalleMoviminetoPago.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            try {
                connection.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        } finally {
            try {
                if (UltimoIdMovimiento != null) {
                    UltimoIdMovimiento.close();
                }
                if (InsertDetalleMovimineto != null) {
                    InsertDetalleMovimineto.close();
                }
                if (InsertDetalleMoviminetoPago != null) {
                    InsertDetalleMoviminetoPago.close();
                }
                if (InsertMovimineto != null) {
                    InsertMovimineto.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return IdMovimiento;
    }
}
