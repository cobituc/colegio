package modelo;

import java.awt.Cursor;
import vista.Login;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ClaseFacturacion {

    public ClaseFacturacion() {
    }

    public int FacturarObraSocial(int[] datos, String Fecha, String Hora, String Fecha_Vencieminto, String Periodo, Double Total, int IdObraSocial,int idRecibo, int PuntosdeVentas) {
        int idFacturacion = 0;
        //Saco el tipo de Facturacion
        //////////////////////////////
        int idTiporesponsable = 0;
        String AfipTipo = "";
        String RazonSocial = "";
        String Documento = "";
        int tipodocWSFE = 0;
        int tipodocHASAR = 0;
        int tiporesponsableWSFE = 0;
        int tiporesponsableHASAR = 0;
        String Domicilio = "";

        String sql = "SELECT * FROM vista_tipo_facturacion_obras_sociales WHERE idObrasocialDato=" + IdObraSocial;

        String sqlPedidos = "SELECT * FROM vista_recibos_obras_sociales WHERE idRecibo=" + idRecibo;
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        Statement SelectCliente = null;
        Statement SelectHasar = null;
        Statement SelectPedidos = null;
        PreparedStatement Insertfacturacion = null;
        PreparedStatement Updatepedidos = null;

        PreparedStatement AgregarFacturacion = null;
        PreparedStatement AgregarFacturacion_iva = null;
        Statement UltimoIdFacturacion = null;
        try {
            SelectCliente = cn.createStatement();
            ResultSet rs = SelectCliente.executeQuery(sql);
            while (rs.next()) {
                idTiporesponsable = rs.getInt("idTiporesponsable");
                AfipTipo = rs.getString("AfipTipo");
                RazonSocial = rs.getString("RazonSocial");
                Documento = rs.getString("Documento");
                tipodocWSFE = rs.getInt("tipodocWSFE");
                tipodocHASAR = rs.getInt("tipodocHASAR");
                tiporesponsableWSFE = rs.getInt("tiporesponsableWSFE");
                tiporesponsableHASAR = rs.getInt("tiporesponsableHASAR");
                Domicilio = rs.getString("Domicilio");
            }

            //////////////////
            //SI EL TIPO DE FACTURACION ES WEB SERVISES FACTURA ELECTRONICA
            if (AfipTipo.equals("WSFE")) {

                //lote de comprobantes de ingreso
                int CantReg = 1;

                int CbteTipo;
                if (tiporesponsableWSFE == 1) {
                    //Ticket Factura A
                    CbteTipo = 1;
                } else {
                    //Ticket Factura B
                    CbteTipo = 6;
                }
                fnCargarFecha fecha = new fnCargarFecha();
                String CbteFech = fecha.cargarfechaafip();

                Double ImpTotal = 0.0;
                Double ImpNeto = 0.0;
                Double ImpNetobase = 0.0;
                Double ImpIVA = 0.0;

                Double ImpTotConc = 0.0;
                Double ImpOpEx = 0.0;
                Double ImpTrib = 0.0;

                Double Iva270base = 0.0;
                Double Iva210base = 0.0;
                Double Iva105base = 0.0;
                Double Iva50base = 0.0;
                Double Iva25base = 0.0;
                Double Iva0base = 0.0;

                SelectPedidos = cn.createStatement();
                ResultSet rsPedidos = SelectPedidos.executeQuery(sqlPedidos);
                while (rsPedidos.next()) {

                    double precio = rsPedidos.getDouble(9);
                    double cant = rsPedidos.getDouble(8);
                    double bonif = rsPedidos.getDouble(5);
                    double desc = rsPedidos.getDouble(21);

                    fnRedondear redondear = new fnRedondear();

                    //IVAbase= PrecioV * (1-bon/100) * Cant * (1+Descuento/100);
                    if (rsPedidos.getDouble(7) == 27) {
                        Iva270base = Iva270base + redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 - (desc / 100)) * cant);
                    }
                    if (rsPedidos.getDouble(7) == 21) {
                        Iva210base = Iva210base + redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 - (desc / 100)) * cant);
                    }
                    if (rsPedidos.getDouble(7) == 10.5) {
                        Iva105base = Iva105base + redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 - (desc / 100)) * cant);
                    }
                    if (rsPedidos.getDouble(7) == 5) {
                        Iva50base = Iva50base + redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 - (desc / 100)) * cant);
                    }
                    if (rsPedidos.getDouble(7) == 2.5) {
                        Iva25base = Iva25base + redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 - (desc / 100)) * cant);
                    }
                    if (rsPedidos.getDouble(7) == 0) {
                        Iva0base = Iva0base + redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 - (desc / 100)) * cant);
                    }
                }

                int idIVA = 0;
                if (Iva270base != 0.0) {
                    idIVA = idIVA + 1;
                    ImpNetobase = Iva270base + ImpNetobase;
                }
                if (Iva210base != 0.0) {
                    idIVA = idIVA + 1;
                    ImpNetobase = Iva210base + ImpNetobase;
                }
                if (Iva105base != 0.0) {
                    idIVA = idIVA + 1;
                    ImpNetobase = Iva105base + ImpNetobase;
                }
                if (Iva50base != 0.0) {
                    idIVA = idIVA + 1;
                    ImpNetobase = Iva50base + ImpNetobase;
                }
                if (Iva25base != 0.0) {
                    idIVA = idIVA + 1;
                    ImpNetobase = Iva25base + ImpNetobase;
                }
                if (Iva0base != 0.0) {
                    idIVA = idIVA + 1;
                    ImpNetobase = Iva0base + ImpNetobase;
                }

                String[][] datosiva = new String[idIVA][3];

                int ban270 = 0, ban210 = 0, ban105 = 0, ban50 = 0, ban25 = 0, ban0 = 0;
                for (int i = 0; i < idIVA; i++) {
                    fnRedondear redondear = new fnRedondear();
                    if (Iva270base != 0.0 && ban270 == 0) {
                        datosiva[i][0] = "6";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva270base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva270base * 27 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva270base * 27 / 100);
                        ban270 = 1;
                    } else if (Iva210base != 0.0 && ban210 == 0) {
                        datosiva[i][0] = "5";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva210base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva210base * 21 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva210base * 21 / 100);
                        ban210 = 1;
                    } else if (Iva105base != 0.0 && ban105 == 0) {
                        datosiva[i][0] = "4";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva105base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva105base * 10.5 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva105base * 10.5 / 100);
                        ban105 = 1;
                    } else if (Iva50base != 0.0 && ban50 == 0) {
                        datosiva[i][0] = "8";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva50base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva50base * 5 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva50base * 5 / 100);
                        ban50 = 1;
                    } else if (Iva25base != 0.0 && ban25 == 0) {
                        datosiva[i][0] = "9";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva25base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva25base * 2.5 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva25base * 2.5 / 100);
                        ban25 = 1;
                    } else if (Iva0base != 0.0 && ban0 == 0) {
                        datosiva[i][0] = "3";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva0base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva0base * 0 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva0base * 0 / 100);
                        ban0 = 1;
                    }
                }

                fnRedondear redondear = new fnRedondear();
                ImpNeto = redondear.dosDigitos(ImpNetobase);
                ImpTotal = redondear.dosDigitos(ImpNeto + ImpIVA);

                ClaseAfipWSCompUltimoAutorizado ComprobanteNro = new ClaseAfipWSCompUltimoAutorizado();
                int CompNro = Integer.valueOf(ComprobanteNro.CompUltimoAutorizado(PuntosdeVentas, CbteTipo)) + 1;
                String CbteDesde = String.valueOf(CompNro);
                String CbteHasta = CbteDesde;

                ClaseAfipWSCae cae = new ClaseAfipWSCae();
                String[] CAE = cae.ObtenerCAE(CantReg, CbteTipo, PuntosdeVentas, tipodocWSFE, Documento, CbteDesde, CbteHasta,
                        CbteFech, ImpTotal, ImpTotConc, ImpNeto, ImpOpEx, ImpIVA, ImpTrib, datosiva);
                //TENGO CAE
                if (!CAE[0].equals("RECHAZADO")) {

                    String SQL = "";

                    cn.setAutoCommit(false); //transaction block start

                    SQL = "INSERT INTO facturacion (idPedidos, cantReg, idTipoCbte, puntoVenta, idTiporesponsable, "
                            + "documento, cbteDesde, cbteHasta, cbteFech, impTotal, impTotConc, impNeto, impOpEx, "
                            + "impIVA, impTrib, AfipTipo, CAE, fechaCAE, id_usuario) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    AgregarFacturacion = cn.prepareStatement(SQL);

                    AgregarFacturacion.setInt(1, idRecibo);
                    AgregarFacturacion.setInt(2, CantReg);
                    AgregarFacturacion.setInt(3, CbteTipo);
                    AgregarFacturacion.setInt(4, PuntosdeVentas);
                    AgregarFacturacion.setInt(5, idTiporesponsable);
                    AgregarFacturacion.setString(6, Documento);
                    AgregarFacturacion.setString(7, CbteDesde);
                    AgregarFacturacion.setString(8, CbteHasta);
                    AgregarFacturacion.setString(9, CbteFech);
                    AgregarFacturacion.setDouble(10, ImpTotal);
                    AgregarFacturacion.setDouble(11, ImpTotConc);
                    AgregarFacturacion.setDouble(12, ImpNeto);
                    AgregarFacturacion.setDouble(13, ImpOpEx);
                    AgregarFacturacion.setDouble(14, ImpIVA);
                    AgregarFacturacion.setDouble(15, ImpTrib);
                    AgregarFacturacion.setString(16, AfipTipo);
                    AgregarFacturacion.setString(17, CAE[0]);
                    AgregarFacturacion.setString(18, CAE[1]);
                    AgregarFacturacion.setInt(19, Login.idusuario);
                    AgregarFacturacion.executeUpdate();

                    //Busco Ultimo Factura
                    UltimoIdFacturacion = cn.createStatement();
                    ResultSet rsFac = UltimoIdFacturacion.executeQuery("SELECT MAX(idFacturacion) AS idFacturacion FROM facturacion");
                    rsFac.next();
                    idFacturacion = rsFac.getInt("idFacturacion");

                    for (int i = 0; i < datosiva.length; i++) {
                        //Inserto el cliente
                        String sSQL = "INSERT INTO facturacion_iva(idFacturacion, idTipoiva, baseImp, importe) VALUES(?,?,?,?)";
                        AgregarFacturacion_iva = cn.prepareStatement(sSQL);
                        AgregarFacturacion_iva.setInt(1, idFacturacion);
                        AgregarFacturacion_iva.setInt(2, Integer.valueOf(datosiva[i][0]));
                        AgregarFacturacion_iva.setDouble(3, Double.valueOf(datosiva[i][1]));
                        AgregarFacturacion_iva.setDouble(4, Double.valueOf(datosiva[i][2]));

                        AgregarFacturacion_iva.executeUpdate();
                    }

                    String SQLUpdatePedidos = "UPDATE pedidos SET estado=? WHERE idPedidos=" + idRecibo;
                    Updatepedidos = cn.prepareStatement(SQLUpdatePedidos);
                    Updatepedidos.setString(1, "FACTURADO");
                    Updatepedidos.executeUpdate();

                    cn.commit(); //transaction block end
                    JOptionPane.showMessageDialog(null, "La Facturacion se realizo con exito...");

                } else {
                    JOptionPane.showMessageDialog(null, "El Pedido fue Rechazado por Afip");
                }

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCliente != null) {
                    SelectCliente.close();
                }
                if (SelectHasar != null) {
                    SelectHasar.close();
                }
                if (SelectPedidos != null) {
                    SelectPedidos.close();
                }
                if (Insertfacturacion != null) {
                    Insertfacturacion.close();
                }
                if (Updatepedidos != null) {
                    Updatepedidos.close();
                }
                if (AgregarFacturacion != null) {
                    AgregarFacturacion.close();
                }
                if (AgregarFacturacion_iva != null) {
                    AgregarFacturacion_iva.close();
                }
                if (UltimoIdFacturacion != null) {
                    UltimoIdFacturacion.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "El pedido no se pudo facturar");
            }
        }
        return idFacturacion;
    }

    public int RecibosObraSocial(int IdCliente, int IdPedidos, int PuntosdeVentas) {
        int idrecibo = 0;
        return idrecibo;
    }

    public void ImprimirListadoMovimientos(String forma_pago, String estado, String fecha_inicio, String fecha_fin) {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        JasperReport reporte;
        try {
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Listado de Movimientos", true);
            viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
            viewer.setSize(1024, 720);
            viewer.setLocationRelativeTo(null);

            reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Reportes_Pedidos.jasper"));
            Map parametros = new HashMap();
            parametros.put("forma_pago", forma_pago);
            parametros.put("estado", estado);
            parametros.put("fecha_inicio", fecha_inicio);
            parametros.put("fecha_fin", fecha_fin);

            JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
            JasperViewer visualizador = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(visualizador.getContentPane());
            viewer.setVisible(true);

        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "El pedido no se pudo imprimir");
        }
    }

    String ArmaPeriodo(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        //202010
        /////Año///
        for (i = 0; i <= 3; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 4; i <= 5; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ////Dia////
        salida = salida + "01";
        return salida;
    }

    public int AgregarRecibo(int[] datos, String Fecha, String Hora, String Fecha_Vencieminto, String Periodo, Double Total, int IdObraSocial) {
        int IdRecibo = 0;
        /////   Conexion Mysql
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        PreparedStatement InsertRecibo = null;
        Statement UltimoIdRecibo = null;
        Statement ResumenDdJj = null;
        PreparedStatement InsertDetalleRecibo = null;
        //PreparedStatement Insertformadepago = null;
        System.out.println("llega");
        try {
            String SQLInsertPedido = "INSERT INTO os_recibos (fecha, hora,fecha_vencimiento,periodo, total, id_obrasocial, id_usuario) "
                    + "VALUES (?,?,?,?,?,?,?)";

            cn.setAutoCommit(false); //transaction block start
            //Inserto Recibo
            InsertRecibo = cn.prepareStatement(SQLInsertPedido);
            InsertRecibo.setString(1, Fecha);
            InsertRecibo.setString(2, Hora);
            InsertRecibo.setString(3, Fecha_Vencieminto);
            InsertRecibo.setString(4, ArmaPeriodo(Periodo));
            InsertRecibo.setDouble(5, Total);
            InsertRecibo.setInt(6, IdObraSocial);
            InsertRecibo.setInt(7, Login.idusuario);
            InsertRecibo.executeUpdate();
            //Busco Ultimo ID Recibo
            UltimoIdRecibo = cn.createStatement();
            ResultSet rs = UltimoIdRecibo.executeQuery("SELECT MAX(id_recibo) AS id_recibo FROM os_recibos");
            rs.next();
            IdRecibo = rs.getInt("id_recibo");
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            String SQLInsertDetalleRecibo = "INSERT INTO os_detalle_recibos (id_recibo, id_declaracion_jurada) "
                    + "VALUES (?,?)";
            ResumenDdJj = cn.createStatement();
            InsertDetalleRecibo = cn.prepareStatement(SQLInsertDetalleRecibo);
            ResultSet Result_set_DDJJJ = null;
            for (int i = 0; i < datos.length; i++) {
                //Cargo declaracion jurada y Detalle Pedido
                //Busco ddjj
                Result_set_DDJJJ = ResumenDdJj.executeQuery("SELECT id_declaracion_jurada FROM declaracion_jurada WHERE cod_obra_social=" + datos[i] + " and periodo_declaracion=" + Periodo + " and estado=1");
                while (Result_set_DDJJJ.next()) {
                    //Insero Detalle Pedido
                    InsertDetalleRecibo.setInt(1, IdRecibo);
                    InsertDetalleRecibo.setInt(2, Result_set_DDJJJ.getInt(1));
                    InsertDetalleRecibo.executeUpdate();
                }
            }
            cn.commit(); //transaction block end

        } catch (SQLException e) {
            System.out.println("modelo.ClaseFacturacion.AgregarRecibo()" + " e 538 " + e);
            IdRecibo = 0;
            try {
                cn.rollback();
            } catch (SQLException ex) {
                System.out.println("modelo.ClaseFacturacion.AgregarRecibo()" + " e 542 " + ex);
                IdRecibo = 0;
            }
        } finally {
            try {
                if (InsertRecibo != null) {
                    InsertRecibo.close();
                }
                if (InsertRecibo != null) {
                    InsertRecibo.close();
                }
                if (InsertDetalleRecibo != null) {
                    InsertDetalleRecibo.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                System.out.println("modelo.ClaseFacturacion.AgregarRecibo()" + " e 559 " + ex);
                IdRecibo = 0;
            }
        }
        return IdRecibo;
    }

    public void ImprimirRecibo(int IdRecibo) {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        JasperReport reporte;
        try {
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Recibo", true);
//            viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
            viewer.setSize(1024, 720);
            viewer.setLocationRelativeTo(null);

            reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Recibo_OS.jasper"));
            Map parametros = new HashMap();
            parametros.put("id_recibo", IdRecibo);

            JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
            JasperViewer visualizador = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(visualizador.getContentPane());
            viewer.setVisible(true);

        } catch (JRException ex) {
            System.out.println("modelo.ClaseFacturacion.ImprimirRecibo() error 588 " + ex);;
        }
    }
    
     public String numeroVerificador(String codigo) {
        int par = 0;
        int impar = 0;
        int i = 1;
        int suma = 0;

        while (i <= codigo.length()) {
            if (i % 2 == 0) {

                par = par + Integer.valueOf(String.valueOf((codigo.charAt(i - 1))));

            } else {

                impar = impar + Integer.valueOf(String.valueOf((codigo.charAt(i - 1))));

            }
            i++;
        }
        suma = par + (impar * 3);

        int verify_number;

        if (10 - (suma % 10) == 10) {
            verify_number = 0;
        } else {
            verify_number = 10 - (suma % 10);
        }
        return String.valueOf(verify_number);
    }

    
     public void ImprimirFactura(int idFacturacion) {
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        String NumVerif = "", codigo_barra = "";
        String sql = "SELECT *, LPAD(puntoventa,5,0) as pventa, LPAD(idTipoCbte,3,0) as tipocbe  FROM facturacion WHERE idFacturacion=" + idFacturacion;
//        String sql = "SELECT * FROM facturacion WHERE idFacturacion=" + idFacturacion;

        Statement SelectFacturacion = null;
        try {
            SelectFacturacion = cn.createStatement();
            ResultSet rs = SelectFacturacion.executeQuery(sql);
            rs.next();
            codigo_barra = "30522483881" + rs.getString("tipocbe") + rs.getString("pventa") + rs.getString("cae") + rs.getString("fechacae");

            codigo_barra = codigo_barra + numeroVerificador(codigo_barra);
            ///FACTURA A
            if (rs.getInt("idTipoCbte") == 1) {
                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Factura A", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);
                    //String imagen = "/Imagenes/Logoparafactura.png";
                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/FacturaA.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);

                    //reportparametermap1.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);

                    //reportparametermap2.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);
                    //reportparametermap3.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);

                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);
                        }
                    }
                    //JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);

                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
            ////FACTURA B
            if (rs.getInt("idTipoCbte") == 6) {

                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Factura B", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);

                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/FacturaB.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);

                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);

                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);

                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);
                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);

                        }

                    }
///                    JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);

                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
            ///NOTAS DE CREDITO A
            if (rs.getInt("idTipoCbte") == 3) {
                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Nota Credito A", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);
                    //String imagen = "/Imagenes/Logoparafactura.png";
                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/NotaCreditoA.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);
                    //reportparametermap1.put("Imagen", this.getClass().getResourceAsStream(imagen));

                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);
                    //reportparametermap2.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);
                    //reportparametermap3.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);
                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);
                        }
                    }
                    ///JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);

                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
            ////NOTAS DE CREDITO B
            if (rs.getInt("idTipoCbte") == 8) {

                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Nota de Credito B", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);

                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/NotaCreditoB.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);

                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);

                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);

                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);
                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);
                        }
                    }
                    ///  JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);

                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectFacturacion != null) {
                    SelectFacturacion.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "El pedido no se pudo facturar");
            }
        }
    }

}
