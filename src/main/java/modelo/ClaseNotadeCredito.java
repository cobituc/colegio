package modelo;

import vista.Login;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ClaseNotadeCredito {

    public ClaseNotadeCredito() {
    }

    public int Facturar(int idfactura, int PuntosdeVentas) {
        int idNotadeCredito = 0;
        //Saco el tipo de Facturacion
        //////////////////////////////
        //lote de comprobantes de ingreso
        int idPedido = 0;
        int CantReg = 0;
        int CbteTipo = 0;
        int tipodocWSFE = 0;
        int tipoRespWSFE = 0;
        int idCuentaCorriente = 0;
        String Documento = null;
        double ImpTotal = 0.0, ImpTotConc = 0.0, ImpNeto = 0.0, ImpOpEx = 0.0, ImpIVA = 0.0, ImpTrib = 0.0;
        String AfipTipo = null;
        String cbteAsociado = null;

        String sql = "SELECT * FROM vista_facturacion_notadecredito WHERE idFacturacion=" + idfactura;
        String sqlIVA = "SELECT * FROM facturacion_iva WHERE idFacturacion=" + idfactura;
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        Statement SelectFactura = null;
        Statement SelectIVA = null;
        Statement UltimoIdFacturacion = null;
        PreparedStatement AgregarFacturacion = null;
        PreparedStatement AgregarFacturacion_iva = null;
        PreparedStatement Updatepedidos = null;
        PreparedStatement InsertCtaCte = null;

        Statement CountIVA = null;
        try {
            SelectFactura = cn.createStatement();
            ResultSet rs = SelectFactura.executeQuery(sql);
            while (rs.next()) {
                idPedido = rs.getInt("idPedidos");
                CantReg = rs.getInt("cantReg");
                CbteTipo = rs.getInt("idTipoCbte");
                tipodocWSFE = rs.getInt("tipodocWSFE");
                tipoRespWSFE = rs.getInt("idTiporesponsable");
                Documento = rs.getString("documento");
                ImpTotal = rs.getDouble("impTotal");
                ImpTotConc = rs.getDouble("impTotConc");
                ImpNeto = rs.getDouble("impNeto");
                ImpOpEx = rs.getDouble("impOpEx");
                ImpIVA = rs.getDouble("impIVA");
                ImpTrib = rs.getDouble("impTrib");
                AfipTipo = rs.getString("AfipTipo");
                cbteAsociado = rs.getString("ptoVentaCbte");
                if (rs.getObject("idCuentaCorriente") != null) {
                    idCuentaCorriente = rs.getInt("idCuentaCorriente");
                }
            }

            if (CbteTipo == 1) {
                //Nota de Credito A
                CbteTipo = 3;
            } else {
                //Nota de Credito B
                CbteTipo = 8;
            }

            ClaseAfipWSCompUltimoAutorizado ComprobanteNro = new ClaseAfipWSCompUltimoAutorizado();
            int CompNro = Integer.valueOf(ComprobanteNro.CompUltimoAutorizado(PuntosdeVentas, CbteTipo)) + 1;
            String CbteDesde = String.valueOf(CompNro);
            String CbteHasta = CbteDesde;
            //////////////////

            fnCargarFecha fecha = new fnCargarFecha();
            String CbteFech = fecha.cargarfechaafip();

            CountIVA = cn.createStatement();
            ResultSet rsCountIVA = CountIVA.executeQuery("SELECT COUNT(idFacturacion) AS cantIVA FROM facturacion_iva WHERE idFacturacion = " + idfactura);
            rsCountIVA.next();
            int cantIVA = rsCountIVA.getInt("cantIVA");

            String[][] datosiva = new String[cantIVA][3];

            SelectIVA = cn.createStatement();
            ResultSet rsIVA = SelectIVA.executeQuery(sqlIVA);

            int i = 0;
            while (rsIVA.next()) {
                datosiva[i][0] = rsIVA.getString("idTipoiva");
                datosiva[i][1] = rsIVA.getString("baseImp");
                datosiva[i][2] = rsIVA.getString("importe");
                i++;
            }

            ClaseAfipWSCae cae = new ClaseAfipWSCae();
            String[] CAE = cae.ObtenerCAE(CantReg, CbteTipo, PuntosdeVentas, tipodocWSFE, Documento, CbteDesde, CbteHasta,
                    CbteFech, ImpTotal, ImpTotConc, ImpNeto, ImpOpEx, ImpIVA, ImpTrib, datosiva);

            //TENGO CAE
            if (!CAE[0].equals("RECHAZADO")) {

                String SQL = "";

                cn.setAutoCommit(false); //transaction block start

                SQL = "INSERT INTO facturacion (cantReg, idTipoCbte, puntoVenta, idTiporesponsable, "
                        + "documento, cbteDesde, cbteHasta, cbteFech, impTotal, impTotConc, impNeto, impOpEx, "
                        + "impIVA, impTrib, AfipTipo, CAE, fechaCAE, cbteAsociado, idPedidos, id_usuario) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                AgregarFacturacion = cn.prepareStatement(SQL);

                AgregarFacturacion.setInt(1, CantReg);
                AgregarFacturacion.setInt(2, CbteTipo);
                AgregarFacturacion.setInt(3, PuntosdeVentas);
                AgregarFacturacion.setInt(4, tipoRespWSFE);
                AgregarFacturacion.setString(5, Documento);
                AgregarFacturacion.setString(6, CbteDesde);
                AgregarFacturacion.setString(7, CbteHasta);
                AgregarFacturacion.setString(8, CbteFech);
                AgregarFacturacion.setDouble(9, ImpTotal);
                AgregarFacturacion.setDouble(10, ImpTotConc);
                AgregarFacturacion.setDouble(11, ImpNeto);
                AgregarFacturacion.setDouble(12, ImpOpEx);
                AgregarFacturacion.setDouble(13, ImpIVA);
                AgregarFacturacion.setDouble(14, ImpTrib);

                AgregarFacturacion.setString(15, AfipTipo);
                AgregarFacturacion.setString(16, CAE[0]);
                AgregarFacturacion.setString(17, CAE[1]);
                AgregarFacturacion.setString(18, cbteAsociado);
                AgregarFacturacion.setInt(19, idPedido);
                AgregarFacturacion.setInt(20, Login.idusuario);

                AgregarFacturacion.executeUpdate();

                //Busco Ultimo Factura
                UltimoIdFacturacion = cn.createStatement();
                ResultSet rsFac = UltimoIdFacturacion.executeQuery("SELECT MAX(idFacturacion) AS idFacturacion FROM facturacion");
                rsFac.next();
                idNotadeCredito = rsFac.getInt("idFacturacion");

                for (int j = 0; j < datosiva.length; j++) {
                    //Inserto el cliente
                    String sSQL = "INSERT INTO facturacion_iva(idFacturacion, idTipoiva, baseImp, importe) VALUES(?,?,?,?)";
                    AgregarFacturacion_iva = cn.prepareStatement(sSQL);
                    AgregarFacturacion_iva.setInt(1, idNotadeCredito);
                    AgregarFacturacion_iva.setInt(2, Integer.valueOf(datosiva[j][0]));
                    AgregarFacturacion_iva.setDouble(3, Double.valueOf(datosiva[j][1]));
                    AgregarFacturacion_iva.setDouble(4, Double.valueOf(datosiva[j][2]));

                    AgregarFacturacion_iva.executeUpdate();
                }

                String SQLUpdatePedidos = "UPDATE pedidos SET estado=? WHERE idPedidos=" + idPedido;
                Updatepedidos = cn.prepareStatement(SQLUpdatePedidos);
                Updatepedidos.setString(1, null);
                Updatepedidos.executeUpdate();
                String InsertStockSQL = "INSERT INTO detalledecuentacorriente (idCuentaCorriente, idPedidos, descripcion, haber, fecha) "
                        + "VALUES (?,?,?,?,?)";

                if (idCuentaCorriente > 0) {
                    System.out.println(idCuentaCorriente);
                    InsertCtaCte = cn.prepareStatement(InsertStockSQL);
                    InsertCtaCte.setInt(1, idCuentaCorriente);
                    InsertCtaCte.setInt(2, idPedido);
                    InsertCtaCte.setString(3, "NOTA DE CREDITO CTA CORRIENTE");
                    InsertCtaCte.setDouble(4, ImpTotal);
                    InsertCtaCte.setString(5, fecha.cargarfecha());
                    InsertCtaCte.executeUpdate();
                }

                cn.commit(); //transaction block end
                JOptionPane.showMessageDialog(null, "La nota de crédito se realizo con exito...");

            } else {
                JOptionPane.showMessageDialog(null, "El Pedido fue Rechazado por Afip");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {

                if (AgregarFacturacion != null) {
                    AgregarFacturacion.close();
                }
                if (AgregarFacturacion_iva != null) {
                    AgregarFacturacion_iva.close();
                }
                if (UltimoIdFacturacion != null) {
                    UltimoIdFacturacion.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "El pedido no se pudo facturar");
            }
        }
        return idNotadeCredito;
    }

}
