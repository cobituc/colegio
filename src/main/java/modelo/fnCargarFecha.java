package modelo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class fnCargarFecha {

    public fnCargarFecha() {
    }

    public String cargarfecha() {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        return fecha;
    }

    public String cargarHora() {
        SimpleDateFormat formato = new SimpleDateFormat("HH:mm:ss");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String hora = formato.format(currentDate);
        return hora;
    }

    public String cargarfechaafip() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        return fecha;
    }

    public String cargarfechayhoraSQL() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        return fecha;
    }

    public java.sql.Date cargarfechaSQL() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);

        //formateo la la fecha de expiración
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try {
            myDate = formatter.parse(fecha);
        } catch (ParseException ex) {
            Logger.getLogger(fnCargarFecha.class.getName()).log(Level.SEVERE, null, ex);
        }
        java.sql.Date sqlfecha = new java.sql.Date(myDate.getTime());
        return sqlfecha;
    }

}
