package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DeclaracionJurada {

    private int id;
    private int matricula;
    private String colegiado;
    private int codigoOS;
    private String obraSocial;
    private double importe;
    private int practicas;
    private int ordenes;
    private int estado;
    private String observacion;
    private int idColegiados;

    public DeclaracionJurada(int id, int matricula, String colegiado, int codigoOS, String obraSocial, double importe, int practicas, int ordenes, int estado, String observacion, int idColegiados) {
        this.id = id;
        this.matricula = matricula;
        this.colegiado = colegiado;
        this.codigoOS = codigoOS;
        this.obraSocial = obraSocial;
        this.importe = importe;
        this.practicas = practicas;
        this.ordenes = ordenes;
        this.estado = estado;
        this.observacion = observacion;
        this.idColegiados = idColegiados;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getColegiado() {
        return colegiado;
    }

    public void setColegiado(String colegiado) {
        this.colegiado = colegiado;
    }

    public int getCodigoOS() {
        return codigoOS;
    }

    public void setCodigoOS(int codigoOS) {
        this.codigoOS = codigoOS;
    }

    public String getObraSocial() {
        return obraSocial;
    }

    public void setObraSocial(String obraSocial) {
        this.obraSocial = obraSocial;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public int getPracticas() {
        return practicas;
    }

    public void setPracticas(int practicas) {
        this.practicas = practicas;
    }

    public int getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(int ordenes) {
        this.ordenes = ordenes;
    }
    
    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getIdColegiados() {
        return idColegiados;
    }

    public void setIdColegiados(int idColegiados) {
        this.idColegiados = idColegiados;
    }
        
    @Override
    public String toString() {
        return "DeclaracionJurada{" + "id=" + id + '}';
    }

       
    public static void cargarDeclaracion(Connection connection, ArrayList<DeclaracionJurada> lista, int matricula, int periodo) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT id_declaracion_jurada, matricula_colegiado, nombre_colegiado, cod_obra_social, obra_social, importe, practicas, ordenes, estado, observacion, id_colegiados FROM declaracion_jurada WHERE matricula_colegiado =" + matricula + " and periodo_declaracion=" + periodo);
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new DeclaracionJurada(rs.getInt("id_declaracion_jurada"), rs.getInt("matricula_colegiado"), rs.getString("nombre_colegiado"), rs.getInt("cod_obra_social"), rs.getString("obra_social"), rs.getDouble("importe"), rs.getInt("practicas"), rs.getInt("ordenes"), rs.getInt("estado"), rs.getString("observacion"), rs.getInt("id_colegiados")));               
            }
        } catch (SQLException e) {
        }
    }
    
}
