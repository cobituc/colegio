
package modelo;


public class Debito {
    
    private int idDebito;
    private String nombre;

    public Debito(int idDebito, String nombre) {
        this.idDebito = idDebito;
        this.nombre = nombre;
    }    
    
    public int getIdDebito() {
        return idDebito;
    }

    public void setIdDebito(int idDebito) {
        this.idDebito = idDebito;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
}
