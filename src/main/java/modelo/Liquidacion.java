
package modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Liquidacion {
    
    private int liquidacion;
    private Date fecha;
    private int recibo;
    private int periodo;

    public Liquidacion(int liquidacion, Date fecha, int recibo, int periodo) {
        this.liquidacion = liquidacion;
        this.fecha = fecha;
        this.recibo = recibo;
        this.periodo = periodo;
    }

    public int getLiquidacion() {
        return liquidacion;
    }

    public void setLiquidacion(int liquidacion) {
        this.liquidacion = liquidacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getRecibo() {
        return recibo;
    }

    public void setRecibo(int recibo) {
        this.recibo = recibo;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    @Override
    public String toString() {
        return liquidacion+"";
    }    
    
    
    public static void cargarLiquidaciones(Connection connection, ArrayList<Liquidacion> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT id_liquidacion, fecha, numero_recibo, periodo FROM liquidacion_fecha ORDER BY id_liquidacion DESC");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new Liquidacion(rs.getInt(1), rs.getDate(2), rs.getInt(3),rs.getInt(4)));               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void cargarLiquidaciones(Connection connection, ArrayList<Liquidacion> lista, int desde, int hasta) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT id_liquidacion, fecha, numero_recibo, periodo FROM liquidacion_fecha WHERE periodo BETWEEN "+desde+" AND "+hasta);
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new Liquidacion(rs.getInt(1), rs.getDate(2), rs.getInt(3),rs.getInt(4)));               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
       
}