package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CuentaBancaria {
    
                private String bancoNombre;
                private int numeroCuenta;
                private int cbu;
                private int cuitDestino;
                private String cuentaNombre;
                private String transferenciaNombre;
                private int estado;

    public CuentaBancaria(String bancoNombre, int numeroCuenta, int cbu, int cuitDestino, String cuentaNombre, String transferenciaNombre, int estado) {
        this.bancoNombre = bancoNombre;
        this.numeroCuenta = numeroCuenta;
        this.cbu = cbu;
        this.cuitDestino = cuitDestino;
        this.cuentaNombre = cuentaNombre;
        this.transferenciaNombre = transferenciaNombre;
        this.estado = estado;
    }
                
                
    public String getBancoNombre() {
        return bancoNombre;
    }

    public void setBancoNombre(String bancoNombre) {
        this.bancoNombre = bancoNombre;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public int getCbu() {
        return cbu;
    }

    public void setCbu(int cbu) {
        this.cbu = cbu;
    }

    public int getCuitDestino() {
        return cuitDestino;
    }

    public void setCuitDestino(int cuitDestino) {
        this.cuitDestino = cuitDestino;
    }

    public String getCuentaNombre() {
        return cuentaNombre;
    }

    public void setCuentaNombre(String cuentaNombre) {
        this.cuentaNombre = cuentaNombre;
    }

    public String getTransferenciaNombre() {
        return transferenciaNombre;
    }

    public void setTransferenciaNombre(String transferenciaNombre) {
        this.transferenciaNombre = transferenciaNombre;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
       
     public static void cargarCuentaBancaria(Connection connection, ArrayList<CuentaBancaria> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("Select * from  vista_banco_tipo");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new CuentaBancaria(rs.getString("BancoNombre"), rs.getInt("NumeroCuenta"), rs.getInt("CBU"), rs.getInt("cuitDestino"), rs.getString("CuentaNombre"), rs.getString("TransferenciaNombre"), rs.getInt("Estado")));               
            }
        } catch (SQLException e) {
        }
    }
     
     public static CuentaBancaria buscarCuentaBancaria(int numero, ArrayList<CuentaBancaria> lista) {
        CuentaBancaria resultado = null;
        for (CuentaBancaria cuenta : lista) {
            if (cuenta.getNumeroCuenta() == numero) {
                resultado = cuenta;
                break;
            }
        }
        return resultado;
    }
    
}
