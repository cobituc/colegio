package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConexionMariaDB {
    
    private Connection connection;
    //private String url = "jdbc:mariadb://localhost:3306/colegiobioquimicos";
   // private String url = "jdbc:mariadb://db.cobituc.info:3306/colegiobioquimicos_test";
    private String url = "jdbc:mariadb://db.cobituc.info:3306/colegiobioquimicos";
       public String user = "usColegio";   
    public String pass = "3I%%We2AQoD2hA$";
   
    public Connection getConnection() {
		return connection;
	}
    
    public void setConnection(Connection connection) {
		this.connection = connection;
	}

    public void EstablecerConexion() {
       
        try {
			Class.forName("org.mariadb.jdbc.Driver");
			connection = DriverManager.getConnection(url, user, pass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
}
    public void cerrarConexion(){
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
    
    public Connection Conectar() {
        Connection link = null;
        try {
            //Cargamos el Driver MySQL
            Class.forName("org.mariadb.jdbc.Driver");
            //Creamos un enlace hacia la base de datos
            link = DriverManager.getConnection(this.url, this.user, this.pass);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConexionMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ConexionMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return link;
    }
}
