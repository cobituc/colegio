
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Localidad {

    private int idLocalidades;
    private int cp;
    private String nombre;

    public Localidad(int idLocalidades, int cp, String nombre) {
        this.idLocalidades = idLocalidades;
        this.cp = cp;
        this.nombre = nombre;
    }

    public int getIdLocalidades() {
        return idLocalidades;
    }

    public void setIdLocalidades(int idLocalidades) {
        this.idLocalidades = idLocalidades;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }     
    
    
    public static void cargarLocalidades(Connection connection, ArrayList<Localidad> lista) {
        
         try {
            Statement instruccion = connection.createStatement();
            ResultSet rs;
            rs = instruccion.executeQuery("SELECT * FROM localidades");
            while (rs.next()) {
                lista.add(new Localidad(rs.getInt("id_localidad"), rs.getInt("codigo_postal"), rs.getString("nombre_localidad")));
                
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
     
     public static Localidad buscarLocalidad(String nombre, ArrayList<Localidad> lista) {
        Localidad resultado = null;

        for (Localidad localidad : lista) {

            if (localidad.getNombre().equals(nombre) ) {

                resultado = localidad;
                break;

            }
        }

        return resultado;
    }
    public static Localidad buscarLocalidad(int id, ArrayList<Localidad> lista) {
        Localidad resultado = null;

        for (Localidad localidad : lista) {

            if (localidad.getIdLocalidades() == id ) {

                resultado = localidad;
                break;

            }
        }

        return resultado;
    }
    
}
