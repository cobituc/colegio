
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Matricula {
    
    private String matricula; 

    public Matricula(String matricula) {
        this.matricula = matricula;
    }
    
    
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    } 

    

    @Override
    public String toString() {
        return matricula;
    }
    
    public static void cargarMatriculas(Connection connection, ArrayList<Matricula> lista, int liquidacion) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT DISTINCT(matricula_colegiado) FROM liquidacion WHERE id_liquidacion = "+liquidacion);
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new Matricula(rs.getString(1)));               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public static void cargarMatriculas(Connection connection, ArrayList<Matricula> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT DISTINCT(matricula_colegiado)  FROM liquidacion");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new Matricula(rs.getString(1)));               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static Matricula buscarMatricula(String matricula, ArrayList<Matricula> lista) {
        Matricula resultado = null;
        for (Matricula matriculas : lista) {
            if (matriculas.getMatricula().equals(matricula)) {
                resultado = matriculas;
                break;
            }
        }
        return resultado;
    }
}