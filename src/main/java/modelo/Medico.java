
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Medico {
    
    private int id;
    private String apellido;
    private String nombre;
    private String mail;
    private long telefono;
    private int matricula;
    private String observaciones;
    private int estado;

    public Medico(int id, String apellido, String nombre, String mail, long telefono, int matricula, String observaciones, int estado) {
        this.id = id;
        this.apellido = apellido;
        this.nombre = nombre;
        this.mail = mail;
        this.telefono = telefono;
        this.matricula = matricula;
        this.observaciones = observaciones;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return matricula+" - "+apellido+", "+nombre;
    }
    
    public static void cargarMedicos(Connection connection, ArrayList<Medico> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT * FROM medicos");
            lista.removeAll(lista);
            while (rs.next()) {
                lista.add(new Medico(rs.getInt("id_medicos"), rs.getString("apellido"), 
                        rs.getString("nombre"), rs.getString("mail"), 
                        rs.getLong("telefono"), rs.getInt("matricula"), rs.getString("observaciones"), 
                        rs.getInt("estado")));               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static Medico buscarMedico(int matricula, String apellido, String nombre, ArrayList<Medico> lista) {
        Medico resultado = null;
        for (Medico medico : lista) {
            if ((medico.toString()).equals(matricula+" - "+apellido+", "+nombre)) {
                resultado = medico;
                break;
            }
        }
        return resultado;
    }
    
    public static String agregarMedicos (Connection connection, Medico medico){
        String mensaje="";                
        String sSQL;        
        try {
            
            
            
            sSQL = "INSERT INTO medicos (nombre, apellido, mail, telefono, matricula, observaciones, estado) "
                    + "VALUES(?,?,?,?,?,?,?)";
            PreparedStatement pst = connection.prepareStatement(sSQL);

            pst.setString(1, medico.getNombre());
            pst.setString(2, medico.getApellido());
            pst.setString(3, medico.getMail());
            pst.setLong(4, medico.getTelefono());
            pst.setInt(5, medico.getMatricula());
            pst.setString(6, medico.getObservaciones());
            pst.setInt(7, medico.getEstado());            
            pst.executeUpdate();
            mensaje="Exito al agregar el médico";
        } catch (SQLException e) {
            mensaje="No se pudo agregar el médico";
            System.out.println(e.getMessage());
        }                
        return mensaje;
    }
    
    
    public static String modificarMedico (Connection connection, Medico medico){
        
        String mensaje;                
        String sSQL;        
        try {
            
            sSQL = "UPDATE medicos SET  nombre=?, apellido=?, mail=?, telefono=?, "
                    + "matricula=?, observaciones=?, estado=? WHERE id_medicos= "+medico.getId();
            
            PreparedStatement pst = connection.prepareStatement(sSQL);

            pst.setString(1, medico.getNombre());
            pst.setString(2, medico.getApellido());
            pst.setString(3, medico.getMail());
            pst.setLong(4, medico.getTelefono());
            pst.setInt(5, medico.getMatricula());
            pst.setString(6, medico.getObservaciones());
            pst.setInt(7, medico.getEstado());            
            pst.executeUpdate();
            mensaje="Exito al modificar el médico";
        } catch (SQLException e) {
            mensaje="No se pudo modificar el médico";
            System.out.println(e.getMessage());
        }
        
        return mensaje;
        
    }
    
}
