package vista;

import controlador.ConexionMariaDB;
import static controlador.HiloInicio.contadorafiliado;
import static controlador.HiloInicio.listaOS;
import controlador.solomayusculas;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Modificar_Orden extends javax.swing.JDialog {

    DefaultTableModel model1;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    public static String documento_afiliado, nombre_afiliado, numero_afiliado, fecha, fecha2, id_obra2 = "", localidad_lab, domicilio_lab, total_pesos_letras, total_centavos_letras, periodo, periododjj;
    TextAutoCompleter textAutoAcompleter;
    int agrega = 1, banderaelimina = 0, bandera_pami2 = 0, bandera_pami = 0, banderamodifica = 0, id_obra_social, contadorj = 0, id_orden, pacientes = 0, practicas = 0, total = 0, contadorobra = 0;
    String hora = "", ip = "", fechaosde = "", cuit = "", Codigo_afiliado = "", pasaporte = "", mensaje = "";
    TextAutoCompleter textAutoAcompleter2;    
    public static int[] idobrasocial = new int[500];
    public static int periodo_colegiado;
    public static String CodObra, ObraSocial;
    public static int idobraimprime = 0, pami = 0;
    String[] practica = new String[2000];
    String[] preciopractica = new String[2000];
    String[] codfacpractica = new String[2000];
    public static int estado_observacion = 1;
    String obra = "";
    public static boolean validacion_pami;
    public static String codverificador;
    //public static String matriculamodifica,per
    public Modificar_Orden(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
//        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logocbt.png")).getImage());
        this.setLocationRelativeTo(null);
        textAutoAcompleter = new TextAutoCompleter(txtpractica);
        textAutoAcompleter2 = new TextAutoCompleter(txtobrasocial);
        cargarperiodo();
        cargarorden();
        cargarfechaOsde();
        tablapracticas.getColumnModel().getColumn(4).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(4).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(4).setPreferredWidth(0);
        cargarip();
        deshabilitarpanel1();
        txtfecha.setEnabled(false);
        jLabel16.setEnabled(true);
        txtobrasocial.setEnabled(false);
        jLabel5.setEnabled(false);
        tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(700);
        txtnombreafiliado.setDocument(new solomayusculas());
    }

    void cargarip() {
        try {
            String thisIp = InetAddress.getLocalHost().getHostAddress();
            String thisname = InetAddress.getLocalHost().getHostName();
            ip = thisIp + "-" + thisname;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    void cargarfechaOsde() {

        //SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat formatoTiempo = new SimpleDateFormat("HHmmss");
        java.util.Date currentDate1 = new java.util.Date();
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(currentDate1);
        hora = formatoTiempo.format(currentDate1);
        /////////////////////////////////////
        //SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
        java.util.Date currentDate = new java.util.Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fechaosde = formato.format(currentDate);
    }
    
    void cargarorden() {
        String sSQL = "";
        String numero = "";
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        sSQL = "SELECT MAX(id_orden) AS id_orden FROM ordenes";

        //“SELECT MAX(id) AS id FROM tabla”
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            if (rs.getInt("id_orden") != 0) {
                id_orden = rs.getInt("id_orden") + 1;
            } else {
                id_orden = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    void cargarobrasocial() {
/*
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT id_obrasocial,razonsocial_obrasocial,importeunidaddearancel_obrasocial,codigo_obrasocial FROM obrasocial ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idobrasocial[contadorobrasocial] = rs.getInt("id_obrasocial");
                nombreobrasocial[contadorobrasocial] = (rs.getString("razonsocial_obrasocial"));
                arancel[contadorobrasocial] = (rs.getDouble("importeunidaddearancel_obrasocial"));
                obrasocial[contadorobrasocial] = (rs.getString("codigo_obrasocial") + " - " + rs.getString("razonsocial_obrasocial"));
                contadorobrasocial++;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar al servidor... Verifique su conexión a internet");
        }*/
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel7 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txtlaboratorio = new javax.swing.JTextField();
        txtcolegiado = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtmonth = new javax.swing.JTextField();
        txtyear = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablapracticas = new javax.swing.JTable();
        jSeparator2 = new javax.swing.JSeparator();
        txtobrasocial = new javax.swing.JTextField();
        txtnombreafiliado = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtpractica = new javax.swing.JTextField();
        txtnumorden = new javax.swing.JFormattedTextField();
        txtfecha = new javax.swing.JFormattedTextField();
        txtdocumento = new javax.swing.JFormattedTextField();
        txtnumafiliado = new javax.swing.JFormattedTextField();
        jLabel18 = new javax.swing.JLabel();
        txtmatricula = new javax.swing.JTextField();
        btnobservacion = new javax.swing.JButton();
        btnimprimir = new javax.swing.JButton();
        btnaceptar = new javax.swing.JButton();
        btnimprimir2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Período de Facturación", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(51, 51, 51));
        jLabel16.setText("Matricula:");

        txtlaboratorio.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtlaboratorio.setForeground(new java.awt.Color(0, 102, 204));
        txtlaboratorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtlaboratorioActionPerformed(evt);
            }
        });

        txtcolegiado.setForeground(new java.awt.Color(0, 102, 204));
        txtcolegiado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        txtcolegiado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcolegiado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcolegiadoActionPerformed(evt);
            }
        });
        txtcolegiado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcolegiadoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcolegiadoKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtcolegiado, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtlaboratorio)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtlaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcolegiado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Período de Facturación", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Mes:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Año:");

        txtmonth.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmonth.setForeground(new java.awt.Color(0, 102, 204));
        txtmonth.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmonthKeyPressed(evt);
            }
        });

        txtyear.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtyear.setForeground(new java.awt.Color(0, 102, 204));
        txtyear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtyearActionPerformed(evt);
            }
        });
        txtyear.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtyearKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(38, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtmonth, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtyear, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(txtmonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtyear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ingreso", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        jPanel1.setEnabled(false);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Obra Social:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Nombre del Afiliado:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("N° de Afiliado:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("Matricula Prescripcion:");

        tablapracticas.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablapracticas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Número", "Código", "Descripción", "Precio", "Cod fac"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablapracticas.setEnabled(false);
        tablapracticas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapracticasKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablapracticas);

        txtobrasocial.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtobrasocial.setForeground(new java.awt.Color(0, 102, 204));
        txtobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialActionPerformed(evt);
            }
        });
        txtobrasocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtobrasocialKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtobrasocialKeyReleased(evt);
            }
        });

        txtnombreafiliado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnombreafiliado.setForeground(new java.awt.Color(0, 102, 204));
        txtnombreafiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreafiliadoActionPerformed(evt);
            }
        });
        txtnombreafiliado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreafiliadoKeyPressed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel11.setText("Numero de Orden:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("Fecha de Orden:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("Buscar Práctica:");

        txtpractica.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtpractica.setForeground(new java.awt.Color(0, 102, 204));
        txtpractica.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtpracticaFocusGained(evt);
            }
        });
        txtpractica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpracticaActionPerformed(evt);
            }
        });
        txtpractica.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtpracticaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpracticaKeyReleased(evt);
            }
        });

        txtnumorden.setForeground(new java.awt.Color(0, 102, 204));
        txtnumorden.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnumorden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnumordenActionPerformed(evt);
            }
        });
        txtnumorden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnumordenKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnumordenKeyReleased(evt);
            }
        });

        txtfecha.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtfecha.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfecha.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtfecha.setDropMode(javax.swing.DropMode.INSERT);
        txtfecha.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtfecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfechaActionPerformed(evt);
            }
        });
        txtfecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfechaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfechaKeyReleased(evt);
            }
        });

        txtdocumento.setForeground(new java.awt.Color(0, 102, 204));
        txtdocumento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        txtdocumento.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdocumentoActionPerformed(evt);
            }
        });
        txtdocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdocumentoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdocumentoKeyReleased(evt);
            }
        });

        txtnumafiliado.setForeground(new java.awt.Color(0, 102, 204));
        txtnumafiliado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnumafiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnumafiliadoActionPerformed(evt);
            }
        });
        txtnumafiliado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnumafiliadoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnumafiliadoKeyReleased(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(51, 51, 51));
        jLabel18.setText("Documento:");

        txtmatricula.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmatricula.setForeground(new java.awt.Color(0, 102, 204));
        txtmatricula.setToolTipText("Buscar por dni paciente o num paciente o nom paciente o num orden");
        txtmatricula.setDropMode(javax.swing.DropMode.INSERT);
        txtmatricula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmatriculaActionPerformed(evt);
            }
        });
        txtmatricula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyReleased(evt);
            }
        });

        btnobservacion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobservacion.setMnemonic('b');
        btnobservacion.setText("Observación");
        btnobservacion.setToolTipText("[Alt + b]");
        btnobservacion.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobservacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobservacionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2)
            .addComponent(jSeparator3)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 658, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnumorden)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtpractica, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnobservacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtobrasocial))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdocumento)
                                .addGap(16, 16, 16)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnumafiliado)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel9)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtmatricula)
                            .addComponent(txtnombreafiliado, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtobrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtnombreafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtdocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(txtnumafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel10)
                    .addComponent(txtnumorden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtpractica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnobservacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        btnimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        btnimprimir.setMnemonic('i');
        btnimprimir.setText("Imprimir");
        btnimprimir.setToolTipText("[Alt + i]");

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnaceptar.setMnemonic('i');
        btnaceptar.setText("Modificar");
        btnaceptar.setToolTipText("[Alt + i]");

        btnimprimir2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728939 - flag yellow.png"))); // NOI18N
        btnimprimir2.setMnemonic('i');
        btnimprimir2.setText("Volver");
        btnimprimir2.setToolTipText("[Alt + i]");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnimprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnaceptar)
                        .addGap(18, 18, 18)
                        .addComponent(btnimprimir2, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnimprimir)
                    .addComponent(btnaceptar)
                    .addComponent(btnimprimir2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtlaboratorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtlaboratorioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtlaboratorioActionPerformed

    private void txtcolegiadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcolegiadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcolegiadoActionPerformed

    boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    void cargarobrasocialpami() {
/*
        int i = 0;

        // Recorro y cargo las obras sociales
        while (i < contadorobrasocial) {

            if (obrasocial[i].equals("13800 - INSSJYP - PAMI  -AUGL 1 (AMB)") || obrasocial[i].equals("13700 - INSSJYP -PAMI- EN TRANSITO")) {

                textAutoAcompleter2.addItem(obrasocial[i]);
            }
            i++;
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter2.setMode(0); // infijo     

        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter2.setCaseSensitive(false); //No sensible a mayúsculas        
*/
    }

    void cargarobrasocial2() {

       

        // Recorro y cargo las obras sociales        
            textAutoAcompleter2.addItems(listaOS.toArray());

        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter2.setMode(0); // infijo     

        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter2.setCaseSensitive(false); //No sensible a mayúsculas        

    }
    
    private void txtcolegiadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcolegiadoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ////////////////////////////////////////////////////////////////////
            int opcion = JOptionPane.showConfirmDialog(this, "Desea realizar ordenes de Pami?", "Mensaje", JOptionPane.YES_NO_OPTION);
            if (opcion == 0) {
                if (!txtcolegiado.getText().equals("") && isNumeric(txtcolegiado.getText())) {
                    ConexionMariaDB mysql = new ConexionMariaDB();
                    Connection cn = mysql.Conectar();
                    periodo = txtyear.getText() + txtmonth.getText();
                    int i = 0;
                    pami = 1;
                    try {
                        String sSQL = "SELECT id_colegiados,matricula_colegiado,nombre_colegiado,validador,periodo_pami FROM colegiados where matricula_colegiado=" + Integer.valueOf(txtcolegiado.getText());
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sSQL);
                        while (rs.next()) {
                            if (rs.getInt("matricula_colegiado") == (Integer.valueOf(txtcolegiado.getText()))) {
//                                Login.matricula_colegiado = rs.getString("matricula_colegiado");
//                                Login.nombre_colegiado = rs.getString("nombre_colegiado");
//                                txtlaboratorio.setText(Login.nombre_colegiado);
                                txtlaboratorio.setEditable(false);
//                                Login.periodo_colegiado = rs.getInt("periodo_pami");
//                                Login.id_Usuario_colegiado = rs.getInt("id_colegiados");
                                jLabel1.setEnabled(true);
                                txtmonth.setEnabled(true);
                                txtmonth.setEditable(true);
                                jLabel2.setEnabled(true);
                                txtyear.setEnabled(true);
                                txtobrasocial.setEnabled(false);
                                jLabel5.setEnabled(false);
                                txtcolegiado.transferFocus();
                                txtmonth.requestFocus();
                                i = 1;
                            }
                        }
                        borrarobras();
                        cargarobrasocialpami();
                        if (i == 0) {
                            JOptionPane.showMessageDialog(null, "Matricula erronea o el validador no puede realizar ordenes para esta matricula...");
                            txtcolegiado.setText("");
                        }
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                        JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    }

                }
            } else {
                if (!txtcolegiado.getText().equals("") && isNumeric(txtcolegiado.getText())) {
                    ConexionMariaDB mysql = new ConexionMariaDB();
                    Connection cn = mysql.Conectar();
                    periodo = txtyear.getText() + txtmonth.getText();
                    int i = 0;
                    pami = 0;
                    try {
                        String sSQL = "SELECT id_colegiados,matricula_colegiado,nombre_colegiado,validador,periodos FROM colegiados ";
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sSQL);
                        while (rs.next()) {
                            if (rs.getInt("matricula_colegiado") == (Integer.valueOf(txtcolegiado.getText()))) {
                              ///  if (rs.getInt("validador") == (Login.id_usuario_validador)) {
//                                    Login.matricula_colegiado = rs.getString("matricula_colegiado");
//                                    Login.nombre_colegiado = rs.getString("nombre_colegiado");
//                                    txtlaboratorio.setText(Login.nombre_colegiado);
//                                    Login.periodo_colegiado = rs.getInt("periodos");
//                                    Login.id_Usuario_colegiado = rs.getInt("id_colegiados");
                                    jLabel1.setEnabled(true);
                                    txtmonth.setEnabled(true);
                                    txtmonth.setEditable(true);
                                    jLabel2.setEnabled(true);
                                    txtyear.setEnabled(true);
                                    txtyear.setEditable(true);
                                    txtobrasocial.setEnabled(false);
                                    jLabel5.setEnabled(false);
                                    //txtcolegiado.setEditable(false);
                                    txtmonth.requestFocus();
                                    i = 1;
                                ///}
                            }
                        }
                        borrarobras();
                        cargarobrasocial();
                        if (i == 0) {
                            JOptionPane.showMessageDialog(null, "Matricula erronea o el validador no puede realizar ordenes para esta matricula...");
                            txtcolegiado.setText("");
                        }
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                        JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    }

                }
            }
            ////////////////////////////////////////////////////////////////////////////

        }

    }//GEN-LAST:event_txtcolegiadoKeyPressed

    void borrarpractica() {
        textAutoAcompleter.removeAllItems();
    }

    void borrarobras() {
        textAutoAcompleter2.removeAllItems();
    }

    void cargarpracticaconobra() {
        int i = 0;
        practica = new String[500000];
        preciopractica = new String[500000];
        contadorj = 0;
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            Statement St = cn.createStatement();
            ResultSet Rs = St.executeQuery("SELECT codigo_practica,preciototal,determinacion, codigo_fac_practicas_obrasocial FROM obrasocial_tiene_practicasnbu  WHERE id_obrasocial=" + id_obra_social);
            while (Rs.next()) {
                practica[contadorj] = (Rs.getString(1) + " - " + Rs.getString(3));
                textAutoAcompleter.addItem(practica[contadorj]);
                preciopractica[contadorj] = Rs.getString(2);
                codfacpractica[contadorj] = Rs.getString(4);
                contadorj++;
            }
            //practicas.requestFocus();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        textAutoAcompleter.setMode(0); // infijo
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter.setCaseSensitive(false);
    }
    

    void cargarperiodo() {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatoTiempo = new SimpleDateFormat("HHmmss");
        SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");

        java.util.Date currentDate1 = new java.util.Date();
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(currentDate1);

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        hora = formatoTiempo.format(currentDate1);
        fecha2 = formato2.format(currentDate);
        txtfecha.setText(fecha);
        /////////////////////////////////////////////////////////
        String año2 = "", mes2 = "", dia2 = "", salida = "";

        calendar.setTime(currentDate);

        int i = 0;
        ///Mes/// 21/06/1985
        for (i = 3; i <= 4; i++) {
            mes2 = mes2 + fecha.charAt(i);
        }

        /////Año/////
        for (i = 6; i <= 9; i++) {
            año2 = año2 + fecha.charAt(i);
        }

        ////Dia////
        for (i = 8; i <= 9; i++) {
            dia2 = dia2 + fecha.charAt(i);
        }
        periodo = año2 + mes2;
        txtfecha.setText(fecha);
        // txtmes.setText(mes2);
        //   txtmes1.setText(mes2);
        // txtaño.setText(año2);
        //  txtaño1.setText(año2);
    }

    void habilitarpanel1() {

        jLabel18.setEnabled(true);
        jLabel8.setEnabled(true);
        jLabel6.setEnabled(true);
        jLabel9.setEnabled(true);
        jLabel10.setEnabled(true);
        jLabel11.setEnabled(true);
        txtdocumento.setEnabled(true);
        txtfecha.setEnabled(true);

        txtmatricula.setEnabled(true);
        txtnumorden.setEnabled(true);
        jPanel1.setEnabled(true);
        tablapracticas.setEnabled(true);
    }

    void deshabilitarpanel1() {
        //jLabel1.setEnabled(false);
        txtmonth.setEditable(false);
        //jLabel2.setEnabled(false);
        txtyear.setEditable(false);
        //jLabel16.setEnabled(false);
        txtlaboratorio.setEditable(false);
        jLabel6.setEnabled(false);
        jLabel18.setEnabled(false);
        jLabel8.setEnabled(false);
        jLabel9.setEnabled(false);
        jLabel10.setEnabled(false);
        txtfecha.setEnabled(false);
        txtfecha.setText("");
        txtdocumento.setEnabled(false);
        txtdocumento.setText("");
        txtnombreafiliado.setEnabled(false);
        txtnombreafiliado.setText("");
        txtnumafiliado.setEnabled(false);
        txtnumafiliado.setText("");
        txtmatricula.setEnabled(false);
        txtmatricula.setText("");
        txtnumorden.setEnabled(false);
        txtnumorden.setText("");
        txtpractica.setEnabled(false);
        txtpractica.setText("");
        jLabel11.setEnabled(false);
        tablapracticas.setEnabled(false);
        //txtmes.setFocusable(true);
    }

    private void txtcolegiadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcolegiadoKeyReleased
        if (txtcolegiado.getText().equals("")) {
            txtobrasocial.setText("");
            txtobrasocial.setEnabled(false);
            txtcolegiado.requestFocus();
        }
    }//GEN-LAST:event_txtcolegiadoKeyReleased

    private void txtmonthKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmonthKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isNumeric(txtmonth.getText())) {
                txtyear.setEnabled(true);
                txtyear.setEditable(true);
                txtyear.requestFocus();
            } else {
                txtmonth.requestFocus();
            }
        }

    }//GEN-LAST:event_txtmonthKeyPressed

    private void txtyearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtyearActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtyearActionPerformed

    private void txtyearKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtyearKeyPressed

        if ((evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) && (txtyear.getText().equals(""))) {

            txtmonth.setEditable(true);
            txtmonth.requestFocus();

        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isNumeric(txtyear.getText())) {
                ConexionMariaDB mysql = new ConexionMariaDB();
                Connection cn = mysql.Conectar();
                periodo = txtyear.getText() + txtmonth.getText();
                int i = 0;
                if (pami == 1) {
                    try {
                        String sSQL = "SELECT estado_pami, periodo_pami FROM periodos ";
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sSQL);
                        rs.next();
                        if (rs.getBoolean("estado_pami") == true && rs.getInt("periodo_pami") <= Integer.valueOf(periodo)) {
                            i = 0;

                        } else {
                            JOptionPane.showMessageDialog(null, "El período esta Inhabilitado...");
                        }

                    } catch (Exception e) {

                        JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    }
                } else {
                    try {
                        String sSQL = "SELECT estado,periodo FROM periodos ";
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sSQL);
                        rs.next();
                        if (rs.getBoolean("estado") == true && rs.getInt("periodo") <= Integer.valueOf(periodo)) {
                            i = 0;

                        } else {
                            JOptionPane.showMessageDialog(null, "El período esta Inhabilitado...");
                        }

                    } catch (Exception e) {

                        JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    }
                }

                if (periodo_colegiado > Integer.valueOf(periodo)) {
                    i = 1;////periodo cerrado
                }
                if (periodo_colegiado < Integer.valueOf(periodo)) {
                    i = 2;////debe cerrar periodo vigente
                }
                if (i == 0) {

                    txtobrasocial.setEnabled(true);
                    jLabel5.setEnabled(true);
                    txtobrasocial.requestFocus();
                    /*txtmes.setEnabled(true);
                     txtaño.setEnabled(true);
                     txtmes.setEditable(true);
                     txtaño.setEditable(true);*/
                }
                if (i == 1) {
                    JOptionPane.showMessageDialog(null, "El periodo ya fue finalizado...");
                    /*txtmes.setEnabled(true);
                     txtaño.setEnabled(true);*/
                    txtobrasocial.setEnabled(false);
                    jLabel5.setEnabled(false);
                    txtmonth.requestFocus();
                }
                if (i == 2) {
                    JOptionPane.showMessageDialog(null, "Está por ingresar un período posterior al vigente...");
                    /*txtmes.setEditable(false);
                     txtaño.setEditable(false);*/
                    txtobrasocial.setEnabled(true);
                    //jLabel5.setEnabled(true);
                    txtobrasocial.requestFocus();
                }

            } else {
                txtyear.requestFocus();
            }

        }

    }//GEN-LAST:event_txtyearKeyPressed

    private void tablapracticasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapracticasKeyPressed
        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            tablapracticas.transferFocus();
            evt.consume();
        }

        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            if (tablapracticas.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
            } else {

                temp.removeRow(tablapracticas.getSelectedRow());

            }
        }
    }//GEN-LAST:event_tablapracticasKeyPressed

    private void txtobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialActionPerformed
        borrarpractica();
        int band = 0;
        obra = txtobrasocial.getText();
        if (!obra.equals("") || !txtmonth.equals("") || !txtyear.equals("")) {

            if (!obra.equals("13800 - INSSJYP - PAMI  -AUGL 1 (AMB)") && !obra.equals("13700 - INSSJYP -PAMI- EN TRANSITO")) {
                int i = 0;
                
                if (band == 0) {
                    JOptionPane.showMessageDialog(null, "La obra social no se encuentra en nuestra base de datos...");
                    txtobrasocial.requestFocus();
                } else {
                    /*    if (obra.equals("3100 - OSDE") || obra.equals("3101 - OSDE  ( RESPONSABLES INSCRIPTOS)")) {
                     txtobrasocial.setEnabled(false);
                     cargarpracticaconobra();
                     txtmatricula.requestFocus();
                     } else {*/
                    /*if ("1800 - SUBSIDIO DE SALUD - IPSSPT".equals(txtobrasocial.getText()) || "1801 - SUBSIDIO DE SALUD - MATERNO INFANTIL".equals(txtobrasocial.getText()) || "1802 - SUBSIDIO DE SALUD - SEGURO ESCOLAR".equals(txtobrasocial.getText()) || "1803 - SUBSIDIO DE SALUD - RECIPROCIDAD".equals(txtobrasocial.getText()) || "1804 - SUBSIDIO DE SALUD - INTERNADO".equals(txtobrasocial.getText()) || "1810 - SUBSIDIO DE SALUD - PRODIASS-PLAN PREVENCION".equals(txtobrasocial.getText()) || "1815 - SUBSIDIO DE SALUD - REFACTURACION".equals(txtobrasocial.getText())) {
                     try {
                     txtnumafiliado.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##-######-##")));
                     } catch (ParseException ex) {
                     Logger.getLogger(MainVa.class.getName()).log(Level.SEVERE, null, ex);
                     }
                     }else{
                     try {
                     txtnumafiliado.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("#0")));
                     } catch (ParseException ex) {
                     Logger.getLogger(MainVa.class.getName()).log(Level.SEVERE, null, ex);
                     }
                     }*/
                    if (banderamodifica == 0) {
                        txtobrasocial.setEnabled(false);
                        cargarpracticaconobra();
                        txtobrasocial.transferFocus();
                    } else {
                        txtobrasocial.setEnabled(false);
                        cargarpracticaconobra();
                        txtpractica.requestFocus();
                        borrartabla();
                    }

                    // }
                }
            } 
        }
    }//GEN-LAST:event_txtobrasocialActionPerformed

     void borrartabla() {
        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
        int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
        for (int i = a; i >= 0; i--) {
            temp.removeRow(i);
        }
    }
    
    private void txtobrasocialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialKeyReleased
        if (txtobrasocial.getText().equals("")) {
            if (banderamodifica == 0) {
                deshabilitarpanel1();
                cargarperiodo();
                txtobrasocial.requestFocus();
            }
        }
    }//GEN-LAST:event_txtobrasocialKeyReleased

 
    
    private void txtobrasocialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialKeyPressed
        if ((evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) && (txtobrasocial.getText().equals(""))) {

            txtmonth.setEnabled(true);
            txtyear.setEnabled(true);
            txtmonth.setEditable(true);
            txtyear.setEditable(true);
            txtobrasocial.setEnabled(false);
            jLabel5.setEnabled(true);
            txtyear.requestFocus();

        }

    }//GEN-LAST:event_txtobrasocialKeyPressed

    private void txtnombreafiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreafiliadoActionPerformed
        //  txtnombreafiliado.transferFocus();
    }//GEN-LAST:event_txtnombreafiliadoActionPerformed

    private void txtnombreafiliadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreafiliadoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtnombreafiliado.getText().equals("")) {
                txtnombreafiliado.transferFocus();
            }
        }

    }//GEN-LAST:event_txtnombreafiliadoKeyPressed

    private void txtpracticaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpracticaActionPerformed

        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
        String cod = "", nom = "", cadena = txtpractica.getText();
        int n = tablapracticas.getRowCount();
        int i = 0, j = 1;
        ///cod/// 0000 - aaaaaaaaa
        while (i < cadena.length()) {
            if (String.valueOf(cadena.charAt(i)).equals("-")) {
                j = i + 2;
                i = cadena.length();
            } else {
                cod = cod + cadena.charAt(i);
            }
            i++;
        }
        ///nom/// 0000 - aaaaaaaaa
        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }
        ////////////////////////////////////
        int band = 0;
        if (!cadena.equals("")) {
            i = 0;
            int fila = 0;

            while (i < contadorj) {
                if (cadena.equals(practica[i])) {
                    if (n != 0) {
                        fila = n;
                        Object nuevo[] = {
                            fila + 1, "", ""};
                        temp.addRow(nuevo);
                        tablapracticas.setValueAt(cod, fila, 1);
                        tablapracticas.setValueAt(nom, fila, 2);
                        tablapracticas.setValueAt(preciopractica[i], fila, 3);
                        tablapracticas.setValueAt(codfacpractica[i], fila, 4);
                    } else {
                        Object nuevo[] = {
                            "1", "", ""};
                        temp.addRow(nuevo);
                        tablapracticas.setValueAt(cod, 0, 1);
                        tablapracticas.setValueAt(nom, 0, 2);
                        tablapracticas.setValueAt(preciopractica[i], 0, 3);
                        tablapracticas.setValueAt(codfacpractica[i], fila, 4);
                    }
                    band = 1;
                }
                i++;
                /////////////////////////////////////////////////////////////
                ///////Ultima Fila///////
                tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(10);
                tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(20);
                tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(300);
                tablapracticas.getColumnModel().getColumn(3).setPreferredWidth(20);
                Rectangle r = tablapracticas.getCellRect(tablapracticas.getRowCount() - 1, 0, true);
                tablapracticas.scrollRectToVisible(r);
                tablapracticas.getSelectionModel().setSelectionInterval(tablapracticas.getRowCount() - 1, tablapracticas.getRowCount() - 1);
                //////////////////////////

            }
            if (band == 0) {
                JOptionPane.showMessageDialog(null, "La practica no es aceptada por la Obra Social...");
                txtpractica.requestFocus();
            }

        }
        txtpractica.setText("");
    }//GEN-LAST:event_txtpracticaActionPerformed

    private void txtpracticaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtpracticaFocusGained

    }//GEN-LAST:event_txtpracticaFocusGained

    private void txtpracticaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpracticaKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ADD) {

            txtpractica.setText("");
            btnaceptar.doClick();

        }

        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tablapracticas.requestFocus();
        }
    }//GEN-LAST:event_txtpracticaKeyReleased

    private void txtpracticaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpracticaKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tablapracticas.setRowSelectionInterval(0, 0);

        }

    }//GEN-LAST:event_txtpracticaKeyPressed

    private void txtnumordenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnumordenActionPerformed
        // txtnumorden.transferFocus();
    }//GEN-LAST:event_txtnumordenActionPerformed

    private void txtnumordenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumordenKeyPressed

        if ((evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) && (txtnumorden.getText().equals(""))) {
            txtmatricula.requestFocus();
        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtnumorden.getText().equals("")) {
                if (!txtnumorden.getText().equals("")) {
                    if ("1800 - SUBSIDIO DE SALUD - IPSSPT".equals(txtobrasocial.getText()) || "1801 - SUBSIDIO DE SALUD - MATERNO INFANTIL".equals(txtobrasocial.getText()) || "1802 - SUBSIDIO DE SALUD - SEGURO ESCOLAR".equals(txtobrasocial.getText()) || "1803 - SUBSIDIO DE SALUD - RECIPROCIDAD".equals(txtobrasocial.getText()) || "1804 - SUBSIDIO DE SALUD - INTERNADO".equals(txtobrasocial.getText()) || "1810 - SUBSIDIO DE SALUD - PRODIASS-PLAN PREVENCION".equals(txtobrasocial.getText()) || "1815 - SUBSIDIO DE SALUD - REFACTURACION".equals(txtobrasocial.getText())) {
                        long ordnumero = Long.valueOf(txtnumorden.getText());
                        long resto3 = 0, resto2, resto = ordnumero;
                        long total = 0, i = 0, d, digito;
                        long totalsobre11, totalmod11, orddigitoverificador;
                        if (txtnumorden.getText().length() != 16) {

                            resto3 = resto / 100;
                            digito = resto - (resto3 * 100);
                            resto = resto3;

                            do {
                                i++;
                                resto2 = resto / 10;
                                d = resto - (resto2 * 10);
                                total = total + (d * (i + 1));
                                resto = resto2;
                            } while (resto > 0);
                            totalsobre11 = total / 11;
                            totalmod11 = total - (totalsobre11 * 11);
                            orddigitoverificador = 11 - totalmod11;
                            if (orddigitoverificador == digito) {
                                txtfecha.setEnabled(true);
                                txtfecha.select(0, 0);
                                txtfecha.requestFocus();
                            } else {
                                if (orddigitoverificador == 11) {
                                    txtfecha.setEnabled(true);
                                    txtfecha.select(0, 0);
                                    txtfecha.requestFocus();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Numero de orden incorrecto...");
                                }
                            }
                        } else {
                            CodigoVerificador.numordencod = String.valueOf(ordnumero);
                            new CodigoVerificador(null, true).setVisible(true);
                            txtnumorden.setText(codverificador);
                            txtfecha.setEnabled(true);
                            txtfecha.select(0, 0);
                            txtfecha.requestFocus();
                        }
                    } else {
                        txtfecha.setEnabled(true);
                        txtfecha.select(0, 0);
                        txtfecha.requestFocus();
                    }
                }

            }
        }

    }//GEN-LAST:event_txtnumordenKeyPressed

    private void txtnumordenKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumordenKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnumordenKeyReleased

    private void txtfechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfechaActionPerformed
        /*  DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
         jLabel12.setEnabled(true);
         txtpractica.setEnabled(true);
         txtfecha.transferFocus();*/
        ///////////////////////////////////////////////
    }//GEN-LAST:event_txtfechaActionPerformed

    private void txtfechaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfechaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (!txtfecha.getText().equals("")) {
                if (!txtfecha.equals("  /  /    ")) {
                    if (Integer.valueOf(txtfecha.getText().substring(0, 2)) <= 31 && Integer.valueOf(txtfecha.getText().substring(0, 2)) >= 1) {
                        if (Integer.valueOf(txtfecha.getText().substring(3, 5)) <= 12 && Integer.valueOf(txtfecha.getText().substring(3, 5)) >= 1) {
                            if (Integer.valueOf(txtfecha.getText().substring(3, 5)) == 1 || Integer.valueOf(txtfecha.getText().substring(3, 5)) == 3 || Integer.valueOf(txtfecha.getText().substring(3, 5)) == 5 || Integer.valueOf(txtfecha.getText().substring(3, 5)) == 7 || Integer.valueOf(txtfecha.getText().substring(3, 5)) == 8 || Integer.valueOf(txtfecha.getText().substring(3, 5)) == 10 || Integer.valueOf(txtfecha.getText().substring(3, 5)) == 12) {
                                DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
                                jLabel12.setEnabled(true);
                                txtpractica.setEnabled(true);
                                txtpractica.requestFocus();
                                //txtfecha.setEditable(false);

                            } else {
                                if (Integer.valueOf(txtfecha.getText().substring(3, 5)) == 4 || Integer.valueOf(txtfecha.getText().substring(3, 5)) == 6 || Integer.valueOf(txtfecha.getText().substring(3, 5)) == 9 || Integer.valueOf(txtfecha.getText().substring(3, 5)) == 11) {
                                    if (Integer.valueOf(txtfecha.getText().substring(0, 2)) <= 30) {
                                        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
                                        jLabel12.setEnabled(true);
                                        txtpractica.setEnabled(true);
                                        txtpractica.requestFocus();
                                        /// txtfecha.setEditable(false);
                                    }
                                } else {

                                    if (Integer.valueOf(txtfecha.getText().substring(3, 5)) == 2) {

                                        if (Integer.valueOf(txtfecha.getText().substring(6, 10)) % 4 == 0) {

                                            if (Integer.valueOf(txtfecha.getText().substring(0, 2)) <= 29) {
                                                DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
                                                jLabel12.setEnabled(true);
                                                txtpractica.setEnabled(true);
                                                txtpractica.requestFocus();
                                                /// txtfecha.setEditable(false);

                                            }
                                        } else {

                                            if (Integer.valueOf(txtfecha.getText().substring(0, 2)) <= 28) {
                                                DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
                                                jLabel12.setEnabled(true);
                                                txtpractica.setEnabled(true);
                                                txtpractica.requestFocus();
                                                //   txtfecha.setEditable(false);

                                            }

                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

    }//GEN-LAST:event_txtfechaKeyPressed

    private void txtfechaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfechaKeyReleased

    }//GEN-LAST:event_txtfechaKeyReleased

    private void txtdocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdocumentoActionPerformed
        txtdocumento.transferFocus();
        int band = 0, band2 = 0;
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String dni = txtdocumento.getText();
        if (!dni.equals("")) {
            if (!dni.equals("11111111")) {
                int i = 0;
                try {
                    String sSQL = "SELECT nombre_afiliado,dni_afiliado,numero_afiliado FROM afiliados WHERE (dni_afiliado=" + dni + "  AND id_obra_social=" + id_obra_social + ") OR (numero_afiliado=" + dni + "  AND id_obra_social=" + id_obra_social + ") ";
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sSQL);
                    while (rs.next()) {
                        txtnombreafiliado.setText(rs.getString("nombre_afiliado"));
                        txtnumafiliado.setText(rs.getString("numero_afiliado"));
                        txtdocumento.setText(rs.getString("dni_afiliado"));
                        txtnombreafiliado.setEditable(false);
                        txtnumafiliado.setEnabled(true);
                        txtnumafiliado.requestFocus();
                        band = 1;
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
                if (band == 0) {
                    try {
                        String sSQL = "SELECT dni_persona,apellido_persona,nombre_persona FROM personas WHERE dni_persona=" + dni;
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sSQL);
                        while (rs.next()) {
                            documento_afiliado = dni;
                            nombre_afiliado = rs.getString("apellido_persona") + " " + rs.getString("nombre_persona");
                            numero_afiliado = "";
                            band2 = 1;
                            txtdocumento.setText(documento_afiliado);
                            txtnombreafiliado.setEnabled(false);
                            txtnombreafiliado.setEditable(false);
                            txtnombreafiliado.setText(nombre_afiliado);
                            txtnumafiliado.setText(numero_afiliado);
                            txtnumafiliado.setEnabled(true);
                            txtnumafiliado.requestFocus();
                        }
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                    if (band2 == 0) {
                        txtdocumento.setEnabled(true);
                        txtnombreafiliado.setEditable(true);
                        txtnombreafiliado.setEnabled(true);
                        txtnumafiliado.setEnabled(true);
                        jLabel18.setEnabled(true);
                        txtnombreafiliado.requestFocus();
                    }
                }
            } else {
                txtdocumento.setEnabled(true);
                txtnombreafiliado.setEditable(true);
                txtnombreafiliado.setEnabled(true);
                txtnumafiliado.setEnabled(true);
                jLabel18.setEnabled(true);
                txtnombreafiliado.requestFocus();
            }
        }
        /*txtnumafiliado.setFormatterFactory(new DefaultFormatterFactory());
         if ("1800 - SUBSIDIO DE SALUD - IPSSPT".equals(txtobrasocial.getText()) || "1801 - SUBSIDIO DE SALUD - MATERNO INFANTIL".equals(txtobrasocial.getText()) || "1802 - SUBSIDIO DE SALUD - SEGURO ESCOLAR".equals(txtobrasocial.getText()) || "1803 - SUBSIDIO DE SALUD - RECIPROCIDAD".equals(txtobrasocial.getText()) || "1804 - SUBSIDIO DE SALUD - INTERNADO".equals(txtobrasocial.getText()) || "1810 - SUBSIDIO DE SALUD - PRODIASS-PLAN PREVENCION".equals(txtobrasocial.getText()) || "1815 - SUBSIDIO DE SALUD - REFACTURACION".equals(txtobrasocial.getText())) {
         try {
         txtnumafiliado.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##-######-##")));
         } catch (ParseException ex) {
         Logger.getLogger(MainVa.class.getName()).log(Level.SEVERE, null, ex);
         }
         } else {

         txtnumafiliado.setFormatterFactory(new DefaultFormatterFactory());

         }*/
    }//GEN-LAST:event_txtdocumentoActionPerformed

    private void txtdocumentoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdocumentoKeyPressed

        if ((evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) && (txtdocumento.getText().equals(""))) {
            txtobrasocial.setEnabled(true);
            txtobrasocial.setText("");
            txtobrasocial.setEditable(true);
            txtobrasocial.requestFocus();
            txtdocumento.setText("");
            deshabilitarpanel1();
        }

    }//GEN-LAST:event_txtdocumentoKeyPressed

    private void txtdocumentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdocumentoKeyReleased
        if (txtdocumento.getText().equals("")) {
            txtnombreafiliado.setText("");
            txtnumafiliado.setText("");
            txtdocumento.requestFocus();
        }
    }//GEN-LAST:event_txtdocumentoKeyReleased

    private void txtnumafiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnumafiliadoActionPerformed
        // txtnumafiliado.transferFocus();
    }//GEN-LAST:event_txtnumafiliadoActionPerformed

    private void txtnumafiliadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumafiliadoKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int ban = 0;
            if (!txtnumafiliado.getText().equals("")) {
                if ("1800 - SUBSIDIO DE SALUD - IPSSPT".equals(txtobrasocial.getText()) || "1801 - SUBSIDIO DE SALUD - MATERNO INFANTIL".equals(txtobrasocial.getText()) || "1802 - SUBSIDIO DE SALUD - SEGURO ESCOLAR".equals(txtobrasocial.getText()) || "1803 - SUBSIDIO DE SALUD - RECIPROCIDAD".equals(txtobrasocial.getText()) || "1804 - SUBSIDIO DE SALUD - INTERNADO".equals(txtobrasocial.getText()) || "1810 - SUBSIDIO DE SALUD - PRODIASS-PLAN PREVENCION".equals(txtobrasocial.getText()) || "1815 - SUBSIDIO DE SALUD - REFACTURACION".equals(txtobrasocial.getText())) {
                    if (txtnumafiliado.getText().length() == 12) {
                        if (isNumeric(txtnumafiliado.getText().substring(0, 2))) {
                            if (txtnumafiliado.getText().substring(2, 3).equals("-")) {
                                if (isNumeric(txtnumafiliado.getText().substring(3, 9))) {
                                    if (txtnumafiliado.getText().substring(9, 10).equals("-")) {
                                        if (isNumeric(txtnumafiliado.getText().substring(10, 12))) {
                                            ban = 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    ban = 1;
                }

            }
            if (ban == 1) {

                txtmatricula.requestFocus();
            } else {
                txtnumafiliado.requestFocus();
            }
        }

        if ((evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) && (txtnumafiliado.getText().equals(""))) {
            txtnombreafiliado.setText("");
            txtdocumento.requestFocus();
            txtdocumento.selectAll();

        }

    }//GEN-LAST:event_txtnumafiliadoKeyPressed

    private void txtnumafiliadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumafiliadoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnumafiliadoKeyReleased

    private void txtmatriculaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmatriculaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmatriculaActionPerformed

    private void txtmatriculaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtmatricula.getText().equals("")) {
                txtnumorden.setEnabled(true);
                txtmatricula.transferFocus();
            }
        }
        if ((evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) && (txtmatricula.getText().equals(""))) {
            txtnumafiliado.requestFocus();
        }

    }//GEN-LAST:event_txtmatriculaKeyPressed

    private void txtmatriculaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyReleased
        
    }//GEN-LAST:event_txtmatriculaKeyReleased

    private void btnobservacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobservacionActionPerformed
        new Observacion(null, true).setVisible(true);
    }//GEN-LAST:event_btnobservacionActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btnimprimir;
    private javax.swing.JButton btnimprimir2;
    private javax.swing.JButton btnobservacion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTable tablapracticas;
    private javax.swing.JFormattedTextField txtcolegiado;
    private javax.swing.JFormattedTextField txtdocumento;
    private javax.swing.JFormattedTextField txtfecha;
    private javax.swing.JTextField txtlaboratorio;
    private javax.swing.JTextField txtmatricula;
    private javax.swing.JTextField txtmonth;
    private javax.swing.JTextField txtnombreafiliado;
    private javax.swing.JFormattedTextField txtnumafiliado;
    private javax.swing.JFormattedTextField txtnumorden;
    private javax.swing.JTextField txtobrasocial;
    private javax.swing.JTextField txtpractica;
    private javax.swing.JTextField txtyear;
    // End of variables declaration//GEN-END:variables
}
