package vista;

import controlador.ConexionMariaDB;
import controlador.ConexionMariaDataB;
import controlador.DatosGaficoValidadores;
import controlador.DatosGrafico;
import controlador.Funciones;
import controlador.Reportes;
import controlador.Validadores;
import controlador.reporteEntregaValidadores;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import modelo.Colegiado;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

public class Recepcion extends javax.swing.JDialog {

    DefaultTableModel modelo, modelo1, modelo2, modelo3, modeloDevolucion, modeloconsulta;
    private ConexionMariaDataB conexion;
    public static int totalOrdenes, total_validadores, id_validador;
    String primerVencimiento, segundoVencimiento, primerVencimientoPorsterior, segundoVencimientoPosterior, año, mes, fecha;
    int validadores = 0, banderaModifica = 0;
    DefaultCategoryDataset datosGrafico = new DefaultCategoryDataset();
    DefaultCategoryDataset datosGraficoValidadores = new DefaultCategoryDataset();
    JFreeChart grafico, graficoValidadores;
    ArrayList<DatosGaficoValidadores> vectorDatosValidadores = new ArrayList<>();
    ArrayList<DatosGrafico> vectorDatos = new ArrayList<>();
    TextAutoCompleter textAutoAcompleter, textAutoAcompleter2, mes_ac_recepcion, mes_ac_entrega, mes_ac_devolucion, mes_ac_consultas, mes_ac_graficos;
    ArrayList<Validadores> lista = new ArrayList<>();

    public Recepcion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        cargarDatosaño();
        txtMesRecepcion.requestFocus();
        txt_matricula.setEnabled(false);
        bt_cargar.setEnabled(false);
        txtAñoRecepcion.setEnabled(false);
        txt_cantidaddeordenes.setEnabled(false);
        cmb_entrega.setEnabled(false);
        cargarDatosRecepcion();

        ArrayList<String> meses = new ArrayList<>();
        meses.add("01");
        meses.add("02");
        meses.add("03");
        meses.add("04");
        meses.add("05");
        meses.add("06");
        meses.add("07");
        meses.add("08");
        meses.add("09");
        meses.add("10");
        meses.add("11");
        meses.add("12");
        textAutoAcompleter = new TextAutoCompleter(txtValidadorEntrega);
        textAutoAcompleter2 = new TextAutoCompleter(txt_validador_devolucion);
        mes_ac_recepcion = new TextAutoCompleter(txtMesRecepcion);
        mes_ac_entrega = new TextAutoCompleter(txtMesEntrega);
        mes_ac_devolucion = new TextAutoCompleter(txt_mes_devolucion);
        mes_ac_consultas = new TextAutoCompleter(txtMesConsulta);
        mes_ac_graficos = new TextAutoCompleter(txt_mes_grafico);

        mes_ac_recepcion.addItems(meses.toArray());
        mes_ac_entrega.addItems(meses.toArray());
        mes_ac_devolucion.addItems(meses.toArray());
        mes_ac_consultas.addItems(meses.toArray());
        mes_ac_graficos.addItems(meses.toArray());

        cargarValidador();
        conexion = new ConexionMariaDataB();
        conexion.EstablecerConexion();
        //creaGraficoValidadores();

        Validadores.cargarValidador(conexion.getConnection(), lista);
        textAutoAcompleter.removeAllItems();
        textAutoAcompleter2.removeAllItems();
        for (int i = 0; i < lista.size(); i++) {

            textAutoAcompleter.addItem(lista.get(i).toString());
            textAutoAcompleter2.addItem(lista.get(i).toString());

        }
        textAutoAcompleter.setMode(0);
        textAutoAcompleter.setCaseSensitive(false);

        textAutoAcompleter2.setMode(0);
        textAutoAcompleter2.setCaseSensitive(false);
    }

    void cargarValidador() {
        textAutoAcompleter.removeAllItems();
        int i = 0;
        ConexionMariaDB CN = new ConexionMariaDB();
        Connection conexion = CN.Conectar();
        try {
            String ConsultaMatricula = "SELECT count(id_usuario) FROM usuarios where validacion_pami=1";
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(ConsultaMatricula);

            while (rs.next()) {

                validadores = rs.getInt(1);
            }
            conexion.close();
        } catch (NumberFormatException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void Limpiar_jTexto() {

        txt_matricula.setText("");
        txtAñoRecepcion.setText("");
        txt_cantidaddeordenes.setText("");
        lbl_nombrecolegiado.setText("");
        txtMesRecepcion.setText("");
        txt_matricula.requestFocus();
        cmb_entrega.setSelectedIndex(0);

    }

    void cargarDatosGrafico() {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();
            String ConsultaMatricula = "SELECT * FROM vista_grafico_pami where id_colegiados=" + trae_idcolegiado(Integer.valueOf(txt_matricula.getText()));

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(ConsultaMatricula);
            while (rs.next()) {
                vectorDatos.add(new DatosGrafico(rs.getString(1), rs.getInt(3), rs.getInt(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void cargarDatosRecepcion() {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();
            String ConsultaMatricula = "SELECT * FROM periodos";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(ConsultaMatricula);
            while (rs.next()) {
                primerVencimiento = rs.getString("PrimerVencimientoActual");
                segundoVencimiento = rs.getString("SegundoVencimientoActual");
                primerVencimientoPorsterior = rs.getString("PrimerVencimientoProximo");
                segundoVencimientoPosterior = rs.getString("SegundoVencimientoProximo");

            }
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void cargarDatosaño() {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();

            String ConsultaPeriodo = "select YEAR(NOW()) as año, MONTH (NOW()) as mes, DATE_FORMAT(CURDATE(),'%d-%m-%Y') AS fecha";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(ConsultaPeriodo);

            if (rs.next()) {

                fecha = rs.getString("fecha");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void cargarTablaValidador(int periodo) {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();
            String[] titulo = {"MP", "Bioquimico", "Órdenes", "Entrega", "Paquete", "Fecha", "Impresion"};
            Object[] filas = new Object[7];
            int totalOrdenes_1 = 0;
            modelo1 = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    if (column == 6) {
                        return true;
                    } else {
                        return false;
                    }
                }

                public Class<?> getColumnClass(int i) {
                    if (i == 6) {
                        return java.lang.Boolean.class;
                    }
                    return super.getColumnClass(i);
                }
            };

            String ConsultaMatricula = "SELECT * FROM vista_validador WHERE  id_validador= " + Validadores.verificarValidador(lista, txtValidadorEntrega.getText()).getIdValidadores() + " AND periodo = " + periodo + " AND estado = 1";

            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaMatricula);

            while (RS.next()) {

                filas[0] = RS.getString("matricula_colegiado");
                filas[1] = RS.getString("nombre_colegiado");
                filas[2] = RS.getString("cantidad_ordenes");
                filas[3] = RS.getString("numero_entrega");
                filas[4] = RS.getString("paquete");
                filas[5] = RS.getString("fecha");
                filas[6] = false;
                modelo1.addRow(filas);

                totalOrdenes_1 = totalOrdenes_1 + RS.getInt("cantidad_ordenes");

            }
            lbtotalValidador1.setText(String.valueOf(totalOrdenes_1));
            totalOrdenes_1 = Integer.valueOf(lblPromedioOrdenes.getText()) - totalOrdenes_1;

            lbrestoValidador.setText(String.valueOf(totalOrdenes_1));

            tablaAsignados.setModel(modelo1);
            txtBuscar.requestFocus();
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void cargarTablaCargadas(int periodo) {

        ConexionMariaDB CN = new ConexionMariaDB();
        Connection con = CN.Conectar();
        String[] titulo = {"MP", "Bioquimico", "Órdenes OK", "Órdenes obs", "Órdenes anuladas", "Total"};

        String[] filas = new String[6];

        modelo3 = new DefaultTableModel(null, titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        try {
            String ConsultaMatricula = "SELECT * FROM vista_ordenes_cargadas_validador WHERE  id_validador= " + Validadores.verificarValidador(lista, txt_validador_devolucion.getText()).getIdValidadores() + " AND periodo = " + periodo;
            System.out.println("periodo: " + periodo);
            System.out.println("id: " + Validadores.verificarValidador(lista, txt_validador_devolucion.getText()).getIdValidadores());
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaMatricula);

            while (RS.next()) {

                filas[0] = RS.getString(8);
                filas[1] = RS.getString(9);
                filas[2] = RS.getString(3);
                filas[3] = RS.getString(4);
                filas[4] = RS.getString(5);
                filas[5] = RS.getString(6);
                modelo3.addRow(filas);
            }
            tablaOrdenesCargadas.setModel(modelo3);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarTablaValidadorDevolucion(int periodo) {

        try {
            ConexionMariaDB CNs = new ConexionMariaDB();
            Connection cons = CNs.Conectar();
            String[] titulo = {"MP", "Bioquimico", "Órdenes", "Entrega", "Paquete", "Fecha", "id recepcion"};
            Object[] registros = new Object[7];
            id_validador = Validadores.verificarValidador(lista, txt_validador_devolucion.getText()).getIdValidadores();
            int totalOrdenes_1 = 0;
            modeloDevolucion = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };

            String ConsultaMatricula = "SELECT * FROM vista_validador WHERE  id_validador= " + id_validador + " AND periodo = " + periodo + " AND estado = 1 AND estado_recepcion = 0";

            Statement STt = cons.createStatement();
            ResultSet RSs = STt.executeQuery(ConsultaMatricula);

            while (RSs.next()) {

                registros[0] = RSs.getString("matricula_colegiado");
                registros[1] = RSs.getString("nombre_colegiado");
                registros[2] = RSs.getString("cantidad_ordenes");
                registros[3] = RSs.getString("numero_entrega");
                registros[4] = RSs.getString("paquete");
                registros[5] = RSs.getString("fecha");
                registros[6] = RSs.getString("id_recepcion");
                System.out.println(RSs.getString("matricula_colegiado"));
                modeloDevolucion.addRow(registros);
                totalOrdenes_1 = totalOrdenes_1 + RSs.getInt("cantidad_ordenes");
            }
            tablaOrdenesAsignadas.setModel(modeloDevolucion);

        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void cargarTablaValidadorDevolucionDevuelta(int periodo) {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();
            String[] titulo = {"MP", "Bioquimico", "Órdenes", "Entrega", "Fecha", "id recepcion"};

            String[] filas = new String[6];

            int totalOrdenes_1 = 0;

            modelo2 = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            id_validador = Validadores.verificarValidador(lista, txt_validador_devolucion.getText()).getIdValidadores();

            String ConsultaMatricula = "SELECT * FROM vista_validador WHERE  id_validador= " + id_validador + " AND periodo = " + periodo + " AND estado = 2";

            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaMatricula);

            while (RS.next()) {

                filas[0] = RS.getString(2);
                filas[1] = RS.getString(8);
                filas[2] = RS.getString(3);
                filas[3] = RS.getString(4);
                filas[4] = RS.getString(6);
                filas[5] = RS.getString(1);
                modelo2.addRow(filas);
                totalOrdenes_1 = RS.getInt(2) + totalOrdenes_1;
            }
            tablaDevolucion.setModel(modelo2);
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void ConsultaPeriodo(String periodoIngresado) {

        try {
            int periodoPami;
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();

            String ConsultaPeriodo = "SELECT periodo_pami FROM periodos";
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaPeriodo);

            RS.first();

            periodoPami = RS.getInt(1);

            if (!periodoIngresado.equals(periodoPami)) {
                JOptionPane.showMessageDialog(null, "El periodo no esta vigente");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void ConsultaOrdenesPeriodo(int periodo) {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();
            
            String ConsultaPeriodo = "SELECT total, totalValidadores FROM vista_total_ordenes_pami WHERE periodo=" + periodo;
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaPeriodo);

            if (RS.next()) {
                totalOrdenes = RS.getInt(1);
                total_validadores = RS.getInt(2);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void CargarTablaRecepcion(int periodo, int matricula) {

        try {
            String[] titulo = {"Fecha", "Presentación", "Paquete", "Cant. Ordenes"};
            String[] filas = new String[4];
            modelo1 = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();

            String ConsultaRecepcionBono = "SELECT fecha,"
                    + " numero_entrega, paquete, cantidad_ordenes FROM recepcion_bioquimico "
                    + " INNER JOIN colegiados ON recepcion_bioquimico.id_colegiados = colegiados.id_colegiados"
                    + " WHERE colegiados.matricula_colegiado = " + matricula + " AND periodo=" + periodo;
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaRecepcionBono);
            while (RS.next()) {
                filas[0] = RS.getString(1);
                filas[1] = RS.getString(2);
                filas[2] = RS.getString(3);
                filas[3] = RS.getString(4);

                modelo1.addRow(filas);
            }
            tb_recepcion.setModel(modelo1);
            cargarDatosGrafico();
            creaGrafico();
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void CargarTablaConsulta(int periodo, int matricula) {

        try {
            String[] titulo = {"Fecha", "Presentación", "Cant. Ordenes"};
            String[] filas = new String[3];
            modelo1 = new DefaultTableModel(null, titulo);
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();

            String ConsultaRecepcionBono = "SELECT fecha,"
                    + " numero_entrega, cantidad_ordenes FROM recepcion_bioquimico "
                    + " INNER JOIN colegiados ON recepcion_bioquimico.id_colegiados = colegiados.id_colegiados"
                    + " WHERE periodo = " + periodo + " and colegiados.matricula_colegiado = " + matricula;
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaRecepcionBono);
            while (RS.next()) {
                filas[0] = RS.getString(1);
                filas[1] = RS.getString(2);
                filas[2] = RS.getString(3);

                modelo1.addRow(filas);
            }
            tb_consulta.setModel(modelo1);
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void CargarTablaDisponible(int periodo) {

        try {
            String[] titulo = {"MP", "Colegiado", "Cant. Ordenes", "Entrega", "Paquete", "idrecep"};
            String[] filas = new String[6];
            modelo = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();

            String CargarTablaDisponible = "SELECT *\n"
                    + "FROM vista_left_outer_join\n"
                    + "where periodo=" + periodo + " and estado_recepcion=1 \n"
                    + "order by CAST(cantidad_ordenes as UNSIGNED) DESC, CAST(matricula_colegiado as UNSIGNED) ";
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(CargarTablaDisponible);
            while (RS.next()) {
                filas[0] = RS.getString(9);
                filas[1] = RS.getString(10);
                filas[2] = RS.getString(2);
                filas[3] = RS.getString("numero_entrega");
                filas[4] = RS.getString("paquete");
                filas[5] = RS.getString("id_recepcion");
                modelo.addRow(filas);
            }
            tablaDisponibles.setModel(modelo);
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private int trae_idcolegiado(int matricula) {

        int idcolegiado = 0;
        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();

            String ConsultaMatricula = "SELECT colegiados.id_colegiados"
                    + " FROM colegiados WHERE colegiados.matricula_colegiado = " + matricula;
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaMatricula);
            while (RS.next()) {

                idcolegiado = Integer.parseInt(RS.getString(1));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return idcolegiado;
    }

    void creaGrafico() {
        lblGrafico.setIcon(null);
        datosGrafico.clear();
        for (int i = 0; i < vectorDatos.size(); i++) {
            datosGrafico.setValue(vectorDatos.get(i).getCantidad(), vectorDatos.get(i).getPeriodo(), "");
        }
        grafico = ChartFactory.createBarChart3D("", "Histórico", "", datosGrafico);
        BufferedImage primeraImagen = grafico.createBufferedImage(271, 168);
        lblGrafico.setIcon(new ImageIcon(primeraImagen));
    }

    void creaGraficoValidadores(int periodoValidadores) {
        lblGrafico1.setIcon(null);
        datosGraficoValidadores.clear();
        DatosGaficoValidadores.cargarDatos(conexion.getConnection(), vectorDatosValidadores, periodoValidadores);
        for (int i = 0; i < vectorDatosValidadores.size(); i++) {
            datosGraficoValidadores.setValue(vectorDatosValidadores.get(i).getOrdenes(), vectorDatosValidadores.get(i).getValidador(), "");
        }
        graficoValidadores = ChartFactory.createBarChart3D("", "Histórico", "", datosGraficoValidadores);
        BufferedImage primeraImagen = graficoValidadores.createBufferedImage(800, 500);
        lblGrafico1.setIcon(new ImageIcon(primeraImagen));
    }

    void aplicarCambios() {
        if (!txt_matricula.getText().equals("")) {
            try {
                
                int idcolegiado = trae_idcolegiado(Integer.parseInt(txt_matricula.getText()));
                String PeriodoString = txtAñoRecepcion.getText() + txtMesRecepcion.getText();                
                int periodoString = Integer.parseInt(PeriodoString);
                ConexionMariaDB CN = new ConexionMariaDB();
                Connection con = CN.Conectar();
                
                String ConsultaMatricula = "SELECT COUNT(numero_entrega) FROM vista_recepcion_con_matricula WHERE matricula_colegiado = '" + txt_matricula.getText() + "' AND numero_entrega = " + Integer.valueOf(cmb_entrega.getSelectedItem().toString()) + " AND paquete= " + Integer.valueOf(cboPaquete.getSelectedItem().toString()) + " AND periodo = " + periodoString;
                Statement ST = con.createStatement();
                ResultSet RS = ST.executeQuery(ConsultaMatricula);
                RS.next();
                if (RS.getInt(1) > 0) {
                    JOptionPane.showMessageDialog(null, "El bioquímico ya realizó esa entrega ");
                } else {
                    try {
                        String InsertarRecepcion = "INSERT INTO recepcion_bioquimico(Fecha, id_colegiados, numero_entrega,"
                                + " cantidad_ordenes, estado_recepcion, periodo, paquete) VALUES (CURDATE()," + idcolegiado + ","
                                + cmb_entrega.getSelectedItem().toString() + ","
                                + txt_cantidaddeordenes.getText() + ",1," + periodoString + ", " + cboPaquete.getSelectedItem().toString() + ")";

                        Statement ST1 = con.createStatement();
                        ST1.executeUpdate(InsertarRecepcion);
                        CargarTablaRecepcion(periodoString, Integer.valueOf(txt_matricula.getText()));
                    } catch (SQLException e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        txt_cantidaddeordenes.setText(null);
        cmb_entrega.setSelectedIndex(0);
        cmb_entrega.setEnabled(false);
        txt_cantidaddeordenes.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        deshacer = new javax.swing.JMenuItem();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelrecepcion = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtMesRecepcion = new javax.swing.JTextField();
        txtAñoRecepcion = new javax.swing.JTextField();
        txt_matricula = new javax.swing.JTextField();
        txt_cantidaddeordenes = new javax.swing.JTextField();
        cmb_entrega = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        bt_cargar = new javax.swing.JButton();
        bt_volver = new javax.swing.JButton();
        lbl_nombrecolegiado = new javax.swing.JLabel();
        btnBorrar = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lblGrafico = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        cboPaquete = new javax.swing.JComboBox();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tb_recepcion = new javax.swing.JTable();
        jPanelentrega = new javax.swing.JPanel();
        btnSalir = new javax.swing.JButton();
        btnImprimirEntrega = new javax.swing.JButton();
        lbl_mes_entrega = new javax.swing.JLabel();
        txtMesEntrega = new javax.swing.JTextField();
        lbl_ano_entrega = new javax.swing.JLabel();
        txtAñoEntrega = new javax.swing.JTextField();
        lbl_validadorentrega = new javax.swing.JLabel();
        txtValidadorEntrega = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaDisponibles = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaAsignados = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        lbrestoValidador = new javax.swing.JLabel();
        lbl_validadorentrega1 = new javax.swing.JLabel();
        lblTotalOrdenes = new javax.swing.JLabel();
        lbl_validadorentrega2 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        lblPromedioOrdenes = new javax.swing.JLabel();
        lbl_validadorentrega4 = new javax.swing.JLabel();
        lbtotalValidador1 = new javax.swing.JLabel();
        lbl_validadorentrega5 = new javax.swing.JLabel();
        lblodenesRestantes = new javax.swing.JLabel();
        btnImprimirEntrega1 = new javax.swing.JButton();
        jPaneldevolucion = new javax.swing.JPanel();
        lbl_mes_entrega1 = new javax.swing.JLabel();
        txt_mes_devolucion = new javax.swing.JTextField();
        lbl_ano_entrega1 = new javax.swing.JLabel();
        txt_año_devolucion = new javax.swing.JTextField();
        lbl_validadorentrega3 = new javax.swing.JLabel();
        txt_validador_devolucion = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tablaOrdenesAsignadas = new javax.swing.JTable();
        lblTotalCargadasDevolucionç = new javax.swing.JLabel();
        lblTotalOrdenesDevolucion = new javax.swing.JLabel();
        lblTotalDevueltas = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablaDevolucion = new javax.swing.JTable();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tablaOrdenesCargadas = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jPanelconsulta = new javax.swing.JPanel();
        txtAñoConsulta = new javax.swing.JTextField();
        txtMesConsulta = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        bt_consultarc = new javax.swing.JButton();
        bt_consulH = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        txt_nombrecolegiadoc = new javax.swing.JLabel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_matriculac = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tb_consulta = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        lblTotalOrdenesBusqueda = new javax.swing.JLabel();
        bt_volver1 = new javax.swing.JButton();
        panelGraficos = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        lblGrafico1 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txt_mes_grafico = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txt_año_grafico = new javax.swing.JTextField();

        deshacer.setText("Deshacer entrega");
        deshacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deshacerActionPerformed(evt);
            }
        });
        jPopupMenu1.add(deshacer);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        jPanelrecepcion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jPanelrecepcion.setName(""); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Matricula:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Cantidad de ordenes:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Entrega:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Mes:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("Año:");

        txtMesRecepcion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtMesRecepcion.setForeground(new java.awt.Color(0, 102, 204));
        txtMesRecepcion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMesRecepcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMesRecepcionActionPerformed(evt);
            }
        });

        txtAñoRecepcion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtAñoRecepcion.setForeground(new java.awt.Color(0, 102, 204));
        txtAñoRecepcion.setEnabled(false);
        txtAñoRecepcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAñoRecepcionActionPerformed(evt);
            }
        });

        txt_matricula.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_matricula.setForeground(new java.awt.Color(0, 102, 204));
        txt_matricula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_matriculaActionPerformed(evt);
            }
        });

        txt_cantidaddeordenes.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_cantidaddeordenes.setForeground(new java.awt.Color(0, 102, 204));
        txt_cantidaddeordenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_cantidaddeordenesActionPerformed(evt);
            }
        });

        cmb_entrega.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cmb_entrega.setForeground(new java.awt.Color(0, 102, 204));
        cmb_entrega.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4" }));
        cmb_entrega.setEnabled(false);
        cmb_entrega.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmb_entregaKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setText("Historico de Entregas:");
        jLabel5.setMaximumSize(new java.awt.Dimension(150, 14));
        jLabel5.setPreferredSize(new java.awt.Dimension(150, 20));

        bt_cargar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        bt_cargar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/azul down.png"))); // NOI18N
        bt_cargar.setText("Cargar");
        bt_cargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_cargarActionPerformed(evt);
            }
        });
        bt_cargar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bt_cargarKeyPressed(evt);
            }
        });

        bt_volver.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        bt_volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        bt_volver.setText("Salir");
        bt_volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_volverActionPerformed(evt);
            }
        });

        lbl_nombrecolegiado.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_nombrecolegiado.setForeground(new java.awt.Color(0, 102, 204));
        lbl_nombrecolegiado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btnBorrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728996 - bin.png"))); // NOI18N
        btnBorrar.setText("Borrar todo");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });

        btnImprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        btnImprimir.setText("Imprimir");
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblGrafico.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblGrafico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblGrafico, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel14.setText("Gráfico:");
        jLabel14.setMaximumSize(new java.awt.Dimension(150, 14));
        jLabel14.setPreferredSize(new java.awt.Dimension(150, 20));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel17.setText("Paquete:");

        cboPaquete.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cboPaquete.setForeground(new java.awt.Color(0, 102, 204));
        cboPaquete.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" }));
        cboPaquete.setEnabled(false);
        cboPaquete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cboPaqueteKeyPressed(evt);
            }
        });

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tb_recepcion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tb_recepcion.setModel(new javax.swing.table.DefaultTableModel(
        ));
        tb_recepcion.setToolTipText("");
        tb_recepcion.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tb_recepcion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tb_recepcionMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tb_recepcion);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 401, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelrecepcionLayout = new javax.swing.GroupLayout(jPanelrecepcion);
        jPanelrecepcion.setLayout(jPanelrecepcionLayout);
        jPanelrecepcionLayout.setHorizontalGroup(
            jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelrecepcionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_nombrecolegiado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelrecepcionLayout.createSequentialGroup()
                        .addGroup(jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelrecepcionLayout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMesRecepcion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAñoRecepcion, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(53, 53, 53)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_matricula, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addGroup(jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelrecepcionLayout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 143, Short.MAX_VALUE))
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanelrecepcionLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cantidaddeordenes, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_entrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboPaquete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanelrecepcionLayout.createSequentialGroup()
                        .addComponent(bt_cargar)
                        .addGap(18, 18, 18)
                        .addComponent(btnBorrar)
                        .addGap(18, 18, 18)
                        .addComponent(btnImprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bt_volver)))
                .addContainerGap())
        );
        jPanelrecepcionLayout.setVerticalGroup(
            jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelrecepcionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_matricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtMesRecepcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtAñoRecepcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lbl_nombrecolegiado, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmb_entrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txt_cantidaddeordenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(cboPaquete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(56, 56, 56)
                .addGroup(jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                .addGroup(jPanelrecepcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bt_cargar)
                    .addComponent(bt_volver)
                    .addComponent(btnImprimir)
                    .addComponent(btnBorrar))
                .addContainerGap())
        );

        jTabbedPane1.addTab("RECEPCIÓN", jPanelrecepcion);

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnImprimirEntrega.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnImprimirEntrega.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        btnImprimirEntrega.setText("Imprimir");
        btnImprimirEntrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirEntregaActionPerformed(evt);
            }
        });

        lbl_mes_entrega.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_mes_entrega.setText("Mes:");

        txtMesEntrega.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtMesEntrega.setForeground(new java.awt.Color(0, 102, 204));
        txtMesEntrega.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMesEntrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMesEntregaActionPerformed(evt);
            }
        });

        lbl_ano_entrega.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_ano_entrega.setText("Año:");

        txtAñoEntrega.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtAñoEntrega.setForeground(new java.awt.Color(0, 102, 204));
        txtAñoEntrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAñoEntregaActionPerformed(evt);
            }
        });

        lbl_validadorentrega.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_validadorentrega.setText("Validador:");

        txtValidadorEntrega.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtValidadorEntrega.setForeground(new java.awt.Color(0, 102, 204));
        txtValidadorEntrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtValidadorEntregaActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Paquetes disponibles"));

        tablaDisponibles.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaDisponibles.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaDisponibles.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaDisponiblesMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tablaDisponibles);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 342, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Paquetes asignados"));

        tablaAsignados.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        tablaAsignados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane5.setViewportView(tablaAsignados);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728952 - magnifier zoom.png"))); // NOI18N

        txtBuscar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtBuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        lbrestoValidador.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbrestoValidador.setForeground(new java.awt.Color(0, 102, 204));
        lbrestoValidador.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lbl_validadorentrega1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_validadorentrega1.setText("Total:");

        lblTotalOrdenes.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTotalOrdenes.setForeground(new java.awt.Color(0, 102, 204));
        lblTotalOrdenes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lbl_validadorentrega2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_validadorentrega2.setText("Total órdenes:");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setText("Promedio de órdenes:");

        lblPromedioOrdenes.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblPromedioOrdenes.setForeground(new java.awt.Color(0, 102, 204));
        lblPromedioOrdenes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lbl_validadorentrega4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_validadorentrega4.setText("Resto:");

        lbtotalValidador1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbtotalValidador1.setForeground(new java.awt.Color(0, 102, 204));
        lbtotalValidador1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lbl_validadorentrega5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_validadorentrega5.setText("Ordenes Restantes:");

        lblodenesRestantes.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblodenesRestantes.setForeground(new java.awt.Color(0, 102, 204));
        lblodenesRestantes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE))
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(lbl_validadorentrega2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTotalOrdenes, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblPromedioOrdenes, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(76, 76, 76)
                                .addComponent(lbl_validadorentrega5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblodenesRestantes, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(34, 34, 34)
                        .addComponent(lbl_validadorentrega1)
                        .addGap(18, 18, 18)
                        .addComponent(lbtotalValidador1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                        .addComponent(lbl_validadorentrega4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbrestoValidador, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtBuscar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbrestoValidador, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_validadorentrega4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbtotalValidador1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_validadorentrega2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblPromedioOrdenes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lbl_validadorentrega1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblTotalOrdenes, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblodenesRestantes, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl_validadorentrega5, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );

        btnImprimirEntrega1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnImprimirEntrega1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        btnImprimirEntrega1.setText("Comprobante de entrega");
        btnImprimirEntrega1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirEntrega1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelentregaLayout = new javax.swing.GroupLayout(jPanelentrega);
        jPanelentrega.setLayout(jPanelentregaLayout);
        jPanelentregaLayout.setHorizontalGroup(
            jPanelentregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelentregaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelentregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelentregaLayout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanelentregaLayout.createSequentialGroup()
                        .addComponent(lbl_mes_entrega)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMesEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lbl_ano_entrega)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAñoEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbl_validadorentrega)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtValidadorEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(56, 56, 56))
                    .addGroup(jPanelentregaLayout.createSequentialGroup()
                        .addComponent(btnImprimirEntrega)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnImprimirEntrega1)
                        .addGap(185, 185, 185)
                        .addComponent(btnSalir)
                        .addContainerGap())))
        );
        jPanelentregaLayout.setVerticalGroup(
            jPanelentregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelentregaLayout.createSequentialGroup()
                .addGroup(jPanelentregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelentregaLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelentregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtMesEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelentregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtValidadorEntrega)
                                .addComponent(lbl_validadorentrega, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelentregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtAñoEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lbl_ano_entrega, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jPanelentregaLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(lbl_mes_entrega, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(jPanelentregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnImprimirEntrega)
                    .addComponent(btnSalir)
                    .addComponent(btnImprimirEntrega1))
                .addContainerGap())
        );

        jTabbedPane1.addTab("ENTREGA", jPanelentrega);

        lbl_mes_entrega1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_mes_entrega1.setText("Mes:");

        txt_mes_devolucion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_mes_devolucion.setForeground(new java.awt.Color(0, 102, 204));
        txt_mes_devolucion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_mes_devolucion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_mes_devolucionActionPerformed(evt);
            }
        });

        lbl_ano_entrega1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_ano_entrega1.setText("Año:");

        txt_año_devolucion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_año_devolucion.setForeground(new java.awt.Color(0, 102, 204));
        txt_año_devolucion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_año_devolucionActionPerformed(evt);
            }
        });

        lbl_validadorentrega3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_validadorentrega3.setText("Validador:");

        txt_validador_devolucion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_validador_devolucion.setForeground(new java.awt.Color(0, 102, 204));
        txt_validador_devolucion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_validador_devolucionActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel11.setText("Total órdenes: ");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel12.setText("Total cargadas: ");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setText("Total devueltas: ");

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablaOrdenesAsignadas.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaOrdenesAsignadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaOrdenesAsignadas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaOrdenesAsignadasMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(tablaOrdenesAsignadas);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 379, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblTotalCargadasDevolucionç.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotalCargadasDevolucionç.setForeground(new java.awt.Color(0, 102, 204));

        lblTotalOrdenesDevolucion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotalOrdenesDevolucion.setForeground(new java.awt.Color(0, 102, 204));

        lblTotalDevueltas.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotalDevueltas.setForeground(new java.awt.Color(0, 102, 204));

        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablaDevolucion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jScrollPane6.setViewportView(tablaDevolucion);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablaOrdenesCargadas.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaOrdenesCargadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane7.setViewportView(tablaOrdenesCargadas);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                .addContainerGap())
        );

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        jButton1.setText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPaneldevolucionLayout = new javax.swing.GroupLayout(jPaneldevolucion);
        jPaneldevolucion.setLayout(jPaneldevolucionLayout);
        jPaneldevolucionLayout.setHorizontalGroup(
            jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPaneldevolucionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPaneldevolucionLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPaneldevolucionLayout.createSequentialGroup()
                        .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPaneldevolucionLayout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTotalOrdenesDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPaneldevolucionLayout.createSequentialGroup()
                                .addComponent(lbl_mes_entrega1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_mes_devolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lbl_ano_entrega1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_año_devolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lbl_validadorentrega3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_validador_devolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPaneldevolucionLayout.createSequentialGroup()
                        .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPaneldevolucionLayout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTotalCargadasDevolucionç, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPaneldevolucionLayout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTotalDevueltas, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPaneldevolucionLayout.setVerticalGroup(
            jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPaneldevolucionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_mes_entrega1)
                        .addComponent(txt_mes_devolucion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_año_devolucion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbl_validadorentrega3)
                        .addComponent(txt_validador_devolucion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbl_ano_entrega1)))
                .addGap(18, 18, 18)
                .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPaneldevolucionLayout.createSequentialGroup()
                        .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTotalOrdenesDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(lblTotalCargadasDevolucionç, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPaneldevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblTotalDevueltas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel13)))
                    .addComponent(jButton1))
                .addContainerGap())
        );

        jTabbedPane1.addTab("DEVOLUCIÓN", jPaneldevolucion);

        txtAñoConsulta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtAñoConsulta.setForeground(new java.awt.Color(0, 102, 204));
        txtAñoConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAñoConsultaActionPerformed(evt);
            }
        });

        txtMesConsulta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtMesConsulta.setForeground(new java.awt.Color(0, 102, 204));
        txtMesConsulta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMesConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMesConsultaActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setText("Mes:");

        bt_consultarc.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        bt_consultarc.setText("Consultar");
        bt_consultarc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_consultarcActionPerformed(evt);
            }
        });

        bt_consulH.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        bt_consulH.setText("Consultar Histórico");
        bt_consulH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_consulHActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel10.setText("Año:");

        txt_nombrecolegiadoc.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_nombrecolegiadoc.setForeground(new java.awt.Color(0, 102, 204));

        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Cantidad de presentaciones:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setText("Matricula:");

        txt_matriculac.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txt_matriculac.setForeground(new java.awt.Color(0, 102, 204));
        txt_matriculac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_matriculacActionPerformed(evt);
            }
        });

        tb_consulta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha", "Presentación", "Cant. ordenes"
            }
        ));
        tb_consulta.setComponentPopupMenu(jPopupMenu1);
        jScrollPane2.setViewportView(tb_consulta);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(txt_matriculac, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 576, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(218, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txt_matriculac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Bioquímico", jPanel1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 804, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 368, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Validador", jPanel2);

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel20.setText("Total órdenes: ");

        lblTotalOrdenesBusqueda.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotalOrdenesBusqueda.setForeground(new java.awt.Color(0, 102, 204));

        bt_volver1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        bt_volver1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        bt_volver1.setText("Salir");
        bt_volver1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_volver1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelconsultaLayout = new javax.swing.GroupLayout(jPanelconsulta);
        jPanelconsulta.setLayout(jPanelconsultaLayout);
        jPanelconsultaLayout.setHorizontalGroup(
            jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelconsultaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane2)
                    .addGroup(jPanelconsultaLayout.createSequentialGroup()
                        .addGroup(jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelconsultaLayout.createSequentialGroup()
                                .addComponent(bt_consultarc)
                                .addGap(18, 18, 18)
                                .addComponent(bt_consulH))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelconsultaLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMesConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtAñoConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_nombrecolegiadoc, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bt_volver1)))
                .addContainerGap())
            .addGroup(jPanelconsultaLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTotalOrdenesBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelconsultaLayout.setVerticalGroup(
            jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelconsultaLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txt_nombrecolegiadoc, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtAñoConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtMesConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10)
                        .addComponent(jLabel8)))
                .addGap(18, 18, 18)
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelconsultaLayout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addGroup(jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTotalOrdenesBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 100, Short.MAX_VALUE)
                        .addGroup(jPanelconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bt_consultarc)
                            .addComponent(bt_consulH)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelconsultaLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bt_volver1)))
                .addContainerGap())
        );

        jTabbedPane2.getAccessibleContext().setAccessibleName("Bioquímico");

        jTabbedPane1.addTab("CONSULTA", jPanelconsulta);

        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblGrafico1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblGrafico1, javax.swing.GroupLayout.DEFAULT_SIZE, 785, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblGrafico1, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel18.setText("Mes:");

        txt_mes_grafico.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_mes_grafico.setForeground(new java.awt.Color(0, 102, 204));
        txt_mes_grafico.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_mes_grafico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_mes_graficoActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel19.setText("Año:");

        txt_año_grafico.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_año_grafico.setForeground(new java.awt.Color(0, 102, 204));
        txt_año_grafico.setEnabled(false);
        txt_año_grafico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_año_graficoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelGraficosLayout = new javax.swing.GroupLayout(panelGraficos);
        panelGraficos.setLayout(panelGraficosLayout);
        panelGraficosLayout.setHorizontalGroup(
            panelGraficosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGraficosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelGraficosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelGraficosLayout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_mes_grafico, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_año_grafico, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelGraficosLayout.setVerticalGroup(
            panelGraficosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGraficosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelGraficosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txt_mes_grafico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(txt_año_grafico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(95, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("GRÁFICOS", panelGraficos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 698, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("RECEPCIÓN");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtAñoRecepcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAñoRecepcionActionPerformed

        txt_matricula.setText(null);
        lbl_nombrecolegiado.setText(null);

        if (Funciones.isNumeric(txtAñoRecepcion.getText())) {
            
            txt_matricula.setEnabled(true);
            txt_matricula.requestFocus();
            
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un año valido");
            txtAñoRecepcion.setText("");
        }

    }//GEN-LAST:event_txtAñoRecepcionActionPerformed

    private void txt_matriculaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_matriculaActionPerformed

        if (Funciones.isNumeric(txt_matricula.getText())) {

            try {
                ConexionMariaDB CN = new ConexionMariaDB();
                Connection con = CN.Conectar();
                
                String ConsultaMatricula = "SELECT colegiados.matricula_colegiado, colegiados.nombre_colegiado"
                        + " FROM colegiados WHERE colegiados.matricula_colegiado = " + txt_matricula.getText() + " and estado_colegiado='ACTIVOF'";
                Statement ST = con.createStatement();
                ResultSet RS = ST.executeQuery(ConsultaMatricula);
                if (RS.next()) {
                    int periodo = Integer.valueOf(txtAñoRecepcion.getText()+txtMesRecepcion.getText());
                    lbl_nombrecolegiado.setText(RS.getString(2));
                    txtAñoRecepcion.setEnabled(true);
                    txtMesRecepcion.setEnabled(true);
                    txt_cantidaddeordenes.setEnabled(true);
                    CargarTablaRecepcion(periodo, Integer.valueOf(txt_matricula.getText()));
                    txt_cantidaddeordenes.requestFocus();

                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese una matricula valida");
                    txt_matricula.setText("");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese una matricula valida");
            txt_matricula.setText("");
        }

    }//GEN-LAST:event_txt_matriculaActionPerformed

    private void txt_cantidaddeordenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_cantidaddeordenesActionPerformed
        if (Funciones.isNumeric(txt_cantidaddeordenes.getText())) {
            if (Integer.parseInt(txt_cantidaddeordenes.getText()) > 0) {
                cmb_entrega.setEnabled(true);
                cmb_entrega.requestFocus();
            } else {
                JOptionPane.showMessageDialog(null, "Inregese una cantidad valida");
                txt_cantidaddeordenes.setText("");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Inregese una cantidad valida");
            txt_cantidaddeordenes.setText("");
        }
    }//GEN-LAST:event_txt_cantidaddeordenesActionPerformed

    private void cmb_entregaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmb_entregaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cboPaquete.setEnabled(true);
            cboPaquete.requestFocus();
            bt_cargar.setEnabled(true);
        }
    }//GEN-LAST:event_cmb_entregaKeyPressed

    private void bt_cargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_cargarActionPerformed

        aplicarCambios();

    }//GEN-LAST:event_bt_cargarActionPerformed

    private void bt_cargarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bt_cargarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txt_matricula.getText().equals("")) {

                int idcolegiado = trae_idcolegiado(Integer.parseInt(txt_matricula.getText()));
                Date fechaLocal = new Date();
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String fechahoy = formato.format(fechaLocal);

                ConexionMariaDB CN = new ConexionMariaDB();
                Connection con = CN.Conectar();
                try {
                    int periodo = Integer.valueOf(txtAñoEntrega.getText()+txtMesRecepcion.getText());
                    
                            
                            String InsertarRecepcion = "INSERT INTO recepcion_bioquimico(Fecha, id_colegiados, numero_entrega,"
                            + " cantidad_ordenes, estado_recepcion, periodo) VALUES ('" + fechahoy + "'," + idcolegiado + ","
                            + cmb_entrega.getSelectedItem().toString() + ","
                            + txt_cantidaddeordenes.getText() + ",1," + periodo + ")";

                    Statement ST = con.createStatement();
                    ST.executeUpdate(InsertarRecepcion);

                    int matricula = Integer.parseInt(txt_matricula.getText());
                    CargarTablaRecepcion(periodo, matricula);

                    txtAñoRecepcion.setEnabled(false);
                    txtMesRecepcion.setEnabled(false);
                    txt_cantidaddeordenes.setEnabled(false);
                    cmb_entrega.setEnabled(false);

                } catch (NumberFormatException | SQLException e) {
                    JOptionPane.showMessageDialog(null, e);
                }

                Limpiar_jTexto();
                CargarTablaRecepcion(0, 0);

            }
        }
    }//GEN-LAST:event_bt_cargarKeyPressed

    private void bt_volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_volverActionPerformed
        dispose();
    }//GEN-LAST:event_bt_volverActionPerformed

    private void txtMesEntregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMesEntregaActionPerformed

        if (Funciones.isNumeric(txtMesEntrega.getText()) && txtMesEntrega.getText().length() == 2) {

            txtAñoEntrega.requestFocus();

        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
            txtMesEntrega.setText("");
        }
    }//GEN-LAST:event_txtMesEntregaActionPerformed

    private void txtAñoEntregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAñoEntregaActionPerformed

        if (Funciones.isNumeric(txtAñoEntrega.getText())) {

            try {
                ConexionMariaDB CN = new ConexionMariaDB();
                Connection con = CN.Conectar();
                
                int periodo = Integer.parseInt(txtAñoEntrega.getText()+txtMesEntrega.getText());
                
                String ConsultaMatricula = "SELECT * FROM vista_left_outer_join where periodo = " + periodo;
                Statement ST = con.createStatement();
                ResultSet RS = ST.executeQuery(ConsultaMatricula);

                if (RS.next()) {
                    CargarTablaDisponible(periodo);

                } else {
                    JOptionPane.showMessageDialog(null, "No hay ordenes para repartir");

                }
                
                
                txtValidadorEntrega.requestFocus();
                ConsultaOrdenesPeriodo(periodo);
                lblTotalOrdenes.setText(String.valueOf(totalOrdenes));
                lblPromedioOrdenes.setText(totalOrdenes / (validadores - 5) + "");
                lblodenesRestantes.setText(String.valueOf(totalOrdenes - total_validadores));
            } catch (SQLException ex) {
                Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un año valido");
            txtAñoEntrega.setText("");
        }


    }//GEN-LAST:event_txtAñoEntregaActionPerformed

    private void txtValidadorEntregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtValidadorEntregaActionPerformed

        if (Validadores.verificarValidador(lista, txtValidadorEntrega.getText()) != null) {
            
            if (tablaAsignados.getRowCount() != 0) {
                modelo1.setRowCount(0);
            }
            int periodo = Integer.parseInt(txtAñoEntrega.getText()+txtMesEntrega.getText());
            cargarTablaValidador(periodo);

        } else {
            txtValidadorEntrega.setText("");
        }

    }//GEN-LAST:event_txtValidadorEntregaActionPerformed

    private void txt_matriculacActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_matriculacActionPerformed
        if (Funciones.isNumeric(txt_matriculac.getText())) {
            try {
                ConexionMariaDB CN = new ConexionMariaDB();
                Connection con = CN.Conectar();
                String periodo_1 = txtAñoConsulta.getText() + txtMesConsulta.getText();
                String ConsultaMatricula = "SELECT colegiados.matricula_colegiado, colegiados.nombre_colegiado"
                        + " FROM colegiados WHERE colegiados.matricula_colegiado = " + txt_matriculac.getText();
                Statement ST = con.createStatement();
                ResultSet RS = ST.executeQuery(ConsultaMatricula);
                while (RS.next()) {
                    txt_nombrecolegiadoc.setText(RS.getString(2));
                    cargarTablaPeriodoMatricula(periodo_1);
                }
                
                
                txtMesRecepcion.requestFocus();
            } catch (SQLException ex) {
                Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese una matricula valida");
            txt_matriculac.setText("");
        }
    }//GEN-LAST:event_txt_matriculacActionPerformed

    void cargarTablaPeriodoMatricula(String periodo) {

        try {
            ConexionMariaDB CNs = new ConexionMariaDB();
            Connection cons = CNs.Conectar();
            String[] titulo = {"MP", "Bioquimico", "Órdenes", "Entrega", "Fecha", "Validador", "id_recepcion"};
            
            String[] registros = new String[8];
            int totalOrdenes_1 = 0;
            
            modeloconsulta = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            
            String ConsultaMatricula = "SELECT * FROM vista_pami_busqueda WHERE  matricula_colegiado= " + txt_matriculac.getText() + " AND periodo = " + periodo;

            Statement STt = cons.createStatement();
            ResultSet RSs = STt.executeQuery(ConsultaMatricula);

            while (RSs.next()) {

                registros[0] = RSs.getString("matricula_colegiado");
                registros[1] = RSs.getString("nombre_colegiado");
                registros[2] = RSs.getString("cantidad_ordenes");
                registros[3] = RSs.getString("numero_entrega");
                registros[4] = RSs.getString("fecha");
                registros[5] = RSs.getString("validador");
                registros[6] = RSs.getString("id_recepcion");
                modeloconsulta.addRow(registros);
                totalOrdenes_1 = totalOrdenes_1 + RSs.getInt("cantidad_ordenes");
            }
            lblTotalOrdenesBusqueda.setText(String.valueOf(totalOrdenes_1));
            tb_consulta.setModel(modeloconsulta);
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }


    private void txtAñoConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAñoConsultaActionPerformed

        int añohoy = Integer.parseInt(año);

        int año1 = Integer.parseInt(txtAñoConsulta.getText());

        if (Funciones.isNumeric(txtAñoConsulta.getText())) {
            if ((año1 <= añohoy)) {
                txt_matriculac.requestFocus();

            } else {
                JOptionPane.showMessageDialog(null, "Ingrese un año valido");
                txtAñoConsulta.setText("");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un año valido");
            txtAñoConsulta.setText("");
        }
    }//GEN-LAST:event_txtAñoConsultaActionPerformed

    private void txtMesConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMesConsultaActionPerformed

        if (Funciones.isNumeric(txtMesConsulta.getText()) && txtMesConsulta.getText().length()== 2) {
            if (1 <= Integer.parseInt(txtMesConsulta.getText()) && Integer.parseInt(txtMesConsulta.getText()) <= 12) {
                txtAñoConsulta.requestFocus();
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
                txtMesConsulta.setText("");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
            txtMesConsulta.setText("");
        }
    }//GEN-LAST:event_txtMesConsultaActionPerformed

    private void bt_consultarcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_consultarcActionPerformed

        if (!txt_matricula.getText().equals("")) {
            int matricula = Integer.parseInt(txt_matriculac.getText());
            int periodo = Integer.valueOf(txtAñoConsulta.getText()+txtMesConsulta.getText());
            CargarTablaConsulta(periodo, matricula);
        }
    }//GEN-LAST:event_bt_consultarcActionPerformed

    private void bt_consulHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_consulHActionPerformed

        if (!txt_matriculac.getText().equals("")) {
            try {
                int matricula = Integer.parseInt(txt_matriculac.getText());
                String[] titulo = {"Perido", "Fecha", "Presentación", "Cant. Ordenes"};
                String[] filas = new String[4];
                modelo1 = new DefaultTableModel(null, titulo);
                ConexionMariaDB CN = new ConexionMariaDB();
                Connection con = CN.Conectar();
                
                String ConsultaRecepcionBono = "SELECT periodo, fecha,"
                        + " numero_entrega, cantidad_ordenes FROM recepcion_bioquimico "
                        + " INNER JOIN colegiados ON recepcion_bioquimico.id_colegiados = colegiados.id_colegiados"
                        + " WHERE colegiados.matricula_colegiado = " + matricula;
                Statement ST = con.createStatement();
                ResultSet RS = ST.executeQuery(ConsultaRecepcionBono);
                while (RS.next()) {
                    filas[0] = RS.getString(1);
                    filas[1] = RS.getString(2);
                    filas[2] = RS.getString(3);
                    filas[3] = RS.getString(4);
                    modelo1.addRow(filas);
                }
                tb_consulta.setModel(modelo1);
            } catch (SQLException ex) {
                Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }//GEN-LAST:event_bt_consulHActionPerformed

    private void txt_mes_devolucionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_mes_devolucionActionPerformed

        if (Funciones.isNumeric(txt_mes_devolucion.getText()) && txt_mes_devolucion.getText().length() == 2) {
            txt_año_devolucion.requestFocus();
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
            txt_mes_devolucion.setText("");
        }

    }//GEN-LAST:event_txt_mes_devolucionActionPerformed

    private void txt_año_devolucionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_año_devolucionActionPerformed

        if (Funciones.isNumeric(txt_año_devolucion.getText())) {

            try {
                ConexionMariaDB CN = new ConexionMariaDB();
                Connection con = CN.Conectar();
                
                String ConsultaMatricula = "SELECT * FROM vista_left_outer_join";
                Statement ST = con.createStatement();
                ResultSet RS = ST.executeQuery(ConsultaMatricula);

                if (RS.next()) {

                } else {
                    JOptionPane.showMessageDialog(null, "No hay ordenes para repartir");

                }
                txt_validador_devolucion.requestFocus();
            } catch (SQLException ex) {
                Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un año valido");
            txt_año_devolucion.setText("");
        }

    }//GEN-LAST:event_txt_año_devolucionActionPerformed

    private void txt_validador_devolucionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_validador_devolucionActionPerformed

        if (Validadores.verificarValidador(lista, txt_validador_devolucion.getText()) != null) {
            int periodo = Integer.parseInt(txt_año_devolucion.getText() + txt_mes_devolucion.getText());
            cargarTablaValidadorDevolucion(periodo);
            cargarTablaValidadorDevolucionDevuelta(periodo);
            cargarTablaCargadas(periodo);

        } else {
            txt_validador_devolucion.setText("");
        }

    }//GEN-LAST:event_txt_validador_devolucionActionPerformed

    private void txtMesRecepcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMesRecepcionActionPerformed

        if (Funciones.isNumeric(txtMesRecepcion.getText()) && txtMesRecepcion.getText().length() == 2) {
          
            txtAñoRecepcion.setEnabled(true);
            txtAñoRecepcion.requestFocus();
            
        } else {

            JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
            txtMesRecepcion.setText("");
        }

    }//GEN-LAST:event_txtMesRecepcionActionPerformed

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed

        CargarTablaRecepcion(0, 0);
        txtMesRecepcion.requestFocus();
        lbl_nombrecolegiado.setText("");
        txt_matricula.setText("");
        txt_cantidaddeordenes.setText("");
        txt_matricula.setEnabled(false);
        bt_cargar.setEnabled(false);
        txtAñoRecepcion.setEnabled(false);
        txt_cantidaddeordenes.setEnabled(false);
        cmb_entrega.setEnabled(false);
        lblGrafico.setIcon(null);

    }//GEN-LAST:event_btnBorrarActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed

        if (tb_recepcion.getRowCount() > 0) {
            if (tb_recepcion.getSelectedRow() > -1) {
                Reportes r = new Reportes();
                int mesProximo, añoProximo;
                if (Integer.valueOf(txtMesRecepcion.getText()) == 12) {
                    mesProximo = 1;
                    añoProximo = Integer.valueOf(txtAñoRecepcion.getText()) + 1;
                } else {
                    añoProximo = Integer.valueOf(txtAñoRecepcion.getText());
                    mesProximo = Integer.valueOf(txtMesRecepcion.getText()) + 1;
                }
                r.ReporteRecepcion(Integer.valueOf(txt_matricula.getText()), lbl_nombrecolegiado.getText(), Integer.valueOf(tb_recepcion.getValueAt(tb_recepcion.getSelectedRow(), 1).toString()), Integer.valueOf(txtMesRecepcion.getText()), Integer.valueOf(tb_recepcion.getValueAt(tb_recepcion.getSelectedRow(), 3).toString()), Integer.valueOf(txtAñoRecepcion.getText()), mesProximo, añoProximo, primerVencimiento, segundoVencimiento, primerVencimientoPorsterior, segundoVencimientoPosterior);

            }
        }
        CargarTablaRecepcion(0, 0);
        txtMesRecepcion.requestFocus();
        lbl_nombrecolegiado.setText("");
        txt_matricula.setText("");
        txt_cantidaddeordenes.setText("");
        txt_matricula.setEnabled(false);
        bt_cargar.setEnabled(false);
        txtAñoRecepcion.setEnabled(false);
        txt_cantidaddeordenes.setEnabled(false);
        cmb_entrega.setEnabled(false);
        lblGrafico.setIcon(null);

    }//GEN-LAST:event_btnImprimirActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();

    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnImprimirEntregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirEntregaActionPerformed
        if (tablaAsignados.getRowCount() > 0) {
            if (tablaAsignados.getSelectedRow() > -1) {
                ArrayList<Colegiado> listaColegiados = new ArrayList<>();
                Colegiado c;
                Colegiado.cargarColegiado(conexion.getConnection(), listaColegiados);
                c = Colegiado.buscarColegiado(tablaAsignados.getValueAt(tablaAsignados.getSelectedRow(), 0).toString(), listaColegiados);
                Reportes rotulo = new Reportes();
                rotulo.ReporteRotulo(c.getNombre(),
                        Integer.valueOf(c.getMatricula()),
                        Integer.valueOf(tablaAsignados.getValueAt(tablaAsignados.getSelectedRow(), 2).toString()),
                        //Integer.valueOf(tablaAsignados.getValueAt(tablaAsignados.getSelectedRow(), 3).toString()),
                        Integer.valueOf(txtMesEntrega.getText()),
                        fecha, txtValidadorEntrega.getText());

            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un registro");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un registro");
        }
    }//GEN-LAST:event_btnImprimirEntregaActionPerformed

    private void tb_recepcionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tb_recepcionMouseClicked

        if (evt.getClickCount() == 2) {
            banderaModifica = 1;
            bt_cargar.setText("Modificar");
            txt_cantidaddeordenes.setText(tb_recepcion.getValueAt(tb_recepcion.getSelectedRow(), 2).toString());
        }

    }//GEN-LAST:event_tb_recepcionMouseClicked

    private void cboPaqueteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboPaqueteKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            aplicarCambios();
        }

    }//GEN-LAST:event_cboPaqueteKeyPressed

    private void tablaDisponiblesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaDisponiblesMouseClicked

        if (evt.getClickCount() == 2) {
            if (!txtValidadorEntrega.getText().equals("")) {
                if (Validadores.verificarValidador(lista, txtValidadorEntrega.getText()) != null) {
                    ConexionMariaDB cc = new ConexionMariaDB();
                    Connection cn = cc.Conectar();
                    try {
                        String update = "UPDATE recepcion_bioquimico SET estado_recepcion = 0 WHERE id_recepcion = " + Integer.valueOf(tablaDisponibles.getValueAt(tablaDisponibles.getSelectedRow(), 5).toString());
                        String insert = "INSERT INTO validador_pami (fecha, id_validador, estado, id_recepcion) VALUES(CURDATE(),?,?,?)";
                        PreparedStatement pst = cn.prepareStatement(insert);
                        pst.setInt(1, Validadores.verificarValidador(lista, txtValidadorEntrega.getText()).getIdValidadores());
                        pst.setInt(2, 1);
                        pst.setInt(3, Integer.valueOf(tablaDisponibles.getValueAt(tablaDisponibles.getSelectedRow(), 5).toString()));
                        pst.executeUpdate();
                        pst = cn.prepareStatement(update);
                        int n = pst.executeUpdate();
                        if (n <= 0) {
                            JOptionPane.showMessageDialog(null, "No se pudo asignar el el validador");
                        } else {
                            int periodo = Integer.parseInt(txtAñoEntrega.getText() + txtMesEntrega.getText());
                            CargarTablaDisponible(periodo);
                            cargarTablaValidador(periodo);
                            ConsultaOrdenesPeriodo(periodo);
                            lblTotalOrdenes.setText(String.valueOf(totalOrdenes));
                            lblPromedioOrdenes.setText(totalOrdenes / (validadores - 2) + "");
                            lblodenesRestantes.setText(String.valueOf(totalOrdenes - total_validadores));
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }//GEN-LAST:event_tablaDisponiblesMouseClicked

    private void txt_mes_graficoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_mes_graficoActionPerformed

        if (Funciones.isNumeric(txt_mes_grafico.getText()) && txt_mes_grafico.getText().length()==2 ) {

            if (1 <= Integer.valueOf(txt_mes_grafico.getText()) && Integer.valueOf(txt_mes_grafico.getText()) <= 12) {

                txt_año_grafico.setEnabled(true);
                txt_año_grafico.requestFocus();
            } else {

                JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
                txt_mes_grafico.setText("");
            }

        } else {

            JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
            txt_mes_grafico.setText("");
        }

    }//GEN-LAST:event_txt_mes_graficoActionPerformed

    private void txt_año_graficoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_año_graficoActionPerformed

        Date fecha1 = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy");
        SimpleDateFormat formato1 = new SimpleDateFormat("MM");
        String añolocal = formato.format(fecha1);
        String meslocal = formato1.format(fecha1);
        int añohoy = Integer.parseInt(añolocal);
        int meshoy = Integer.parseInt(meslocal);
        int año1 = Integer.parseInt(txt_año_grafico.getText());
        int mes1 = Integer.parseInt(txt_mes_grafico.getText());

        if (Funciones.isNumeric(txt_año_grafico.getText())) {

            if ((año1 == añohoy && mes1 == meshoy) || ((año1 < añohoy) && ((meshoy - 1) == 12))) {

                String Periodo = txt_año_grafico.getText() + txt_mes_grafico.getText();

                int periodoGrafico = Integer.parseInt(Periodo);

                creaGraficoValidadores(periodoGrafico);

            } else {

                String PeriodoString = txt_año_grafico.getText() + txt_mes_grafico.getText();

                int periodoGrafico = Integer.parseInt(PeriodoString);
                creaGraficoValidadores(periodoGrafico);

            }
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un año valido");
            txt_año_grafico.setText("");
        }

    }//GEN-LAST:event_txt_año_graficoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        dispose();

    }//GEN-LAST:event_jButton1ActionPerformed

    private void tablaOrdenesAsignadasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaOrdenesAsignadasMouseClicked

        if (evt.getClickCount() == 2) {
            if (!txt_validador_devolucion.getText().equals("")) {
                if (Validadores.verificarValidador(lista, txt_validador_devolucion.getText()) != null) {
                    ConexionMariaDB cc = new ConexionMariaDB();
                    Connection cn = cc.Conectar();
                    try {
                        String update = "UPDATE recepcion_bioquimico SET estado_recepcion = 2 WHERE id_recepcion = " + Integer.valueOf(tablaOrdenesAsignadas.getValueAt(tablaOrdenesAsignadas.getSelectedRow(), 6).toString());
                        String insert = "INSERT INTO validador_pami (fecha, id_validador, estado, id_recepcion) VALUES(CURDATE(),?,?,?)";
                        PreparedStatement pst = cn.prepareStatement(insert);
                        pst.setInt(1, Validadores.verificarValidador(lista, txt_validador_devolucion.getText()).getIdValidadores());
                        pst.setInt(2, 2);
                        pst.setInt(3, Integer.valueOf(tablaOrdenesAsignadas.getValueAt(tablaOrdenesAsignadas.getSelectedRow(), 6).toString()));
                        pst.executeUpdate();
                        pst = cn.prepareStatement(update);
                        int n = pst.executeUpdate();
                        if (n <= 0) {
                            JOptionPane.showMessageDialog(null, "No se pudo asignar el el validador");
                        } else {

                            if (Validadores.verificarValidador(lista, txt_validador_devolucion.getText()) != null) {
                                int periodo = Integer.parseInt(txt_año_devolucion.getText() + txt_mes_devolucion.getText());
                                cargarTablaValidadorDevolucion(periodo);
                                cargarTablaValidadorDevolucionDevuelta(periodo);
                                cargarTablaCargadas(periodo);

                            } else {
                                txt_validador_devolucion.setText("");
                            }
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }//GEN-LAST:event_tablaOrdenesAsignadasMouseClicked

    private void btnImprimirEntrega1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirEntrega1ActionPerformed
        LinkedList<reporteEntregaValidadores> resultadoEntrega = new LinkedList<>();
        resultadoEntrega.clear();
        reporteEntregaValidadores datos;
        for (int i = 0; i < tablaAsignados.getRowCount(); i++) {
            if (Boolean.valueOf(tablaAsignados.getValueAt(i, 6).toString()) == true) {
                System.out.println(tablaAsignados.getValueAt(i, 1).toString());
                /*datos = new reporteEntregaValidadores(
                        tablaAsignados.getValueAt(i, 0).toString(),
                        tablaAsignados.getValueAt(i, 1).toString(),
                        Integer.valueOf(tablaAsignados.getValueAt(i, 2).toString()),
                        tablaAsignados.getValueAt(i, 3).toString(),
                        tablaAsignados.getValueAt(i, 4).toString(),
                        tablaAsignados.getValueAt(i, 5).toString());
                resultadoEntrega.add(datos);*/
            }
        }
        int periodo = Integer.parseInt(txtAñoEntrega.getText() + txtMesEntrega.getText());
        cargarTablaValidador(periodo);
        JDialog viewer = new JDialog(new javax.swing.JFrame(), "Reporte", true);
        viewer.setSize(800, 600);
        viewer.setLocationRelativeTo(null);

        try {
            JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/reporte_validadores.jasper"));
            Map parametros = new HashMap();
            parametros.put("nombre", txtValidadorEntrega.getText());

            JasperPrint jPrintreporte = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(resultadoEntrega));
            JasperViewer jv = new JasperViewer(jPrintreporte, false);
            viewer.getContentPane().add(jv.getContentPane());
            viewer.setVisible(true);

        } catch (JRException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_btnImprimirEntrega1ActionPerformed

    private void deshacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deshacerActionPerformed
        String periodo_1 = txtAñoConsulta.getText() + txtMesConsulta.getText();

        if (tb_consulta.getRowCount() > 0) {
            if (tb_consulta.getSelectedRow() > -1) {
                ConexionMariaDB cc = new ConexionMariaDB();
                Connection cn = cc.Conectar();
                try {
                    String update = "UPDATE recepcion_bioquimico SET estado_recepcion = 1 WHERE id_recepcion = " + Integer.valueOf(tb_consulta.getValueAt(tb_consulta.getSelectedRow(), 6).toString());
                    String delete = "delete from validador_pami where id_recepcion=" + Integer.valueOf(tb_consulta.getValueAt(tb_consulta.getSelectedRow(), 6).toString());

                    PreparedStatement pst = cn.prepareStatement(update);
                    int n = pst.executeUpdate();
                    PreparedStatement pst1 = cn.prepareStatement(delete);
                    int n1 = pst1.executeUpdate();

                    if (n <= 0 && n1 <= 0) {
                        JOptionPane.showMessageDialog(null, "No se pudo devolver la entrega");
                    } else {
                        JOptionPane.showMessageDialog(null, "Las ordenes estan disponible para ser asignadas nuevamente");
                        cargarTablaPeriodoMatricula(periodo_1);
                    }

                } catch (SQLException ex) {
                    Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }//GEN-LAST:event_deshacerActionPerformed

    private void bt_volver1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_volver1ActionPerformed

        dispose();

    }//GEN-LAST:event_bt_volver1ActionPerformed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased

            TableRowSorter sorter = new TableRowSorter(modelo);
            sorter.setRowFilter(RowFilter.regexFilter(".*" + txtBuscar.getText() + ".*"));
            tablaDisponibles.setRowSorter(sorter);
    }//GEN-LAST:event_txtBuscarKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bt_cargar;
    private javax.swing.JButton bt_consulH;
    private javax.swing.JButton bt_consultarc;
    private javax.swing.JButton bt_volver;
    private javax.swing.JButton bt_volver1;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnImprimirEntrega;
    private javax.swing.JButton btnImprimirEntrega1;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboPaquete;
    private javax.swing.JComboBox cmb_entrega;
    private javax.swing.JMenuItem deshacer;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelconsulta;
    private javax.swing.JPanel jPaneldevolucion;
    private javax.swing.JPanel jPanelentrega;
    private javax.swing.JPanel jPanelrecepcion;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JLabel lblGrafico;
    private javax.swing.JLabel lblGrafico1;
    private javax.swing.JLabel lblPromedioOrdenes;
    private javax.swing.JLabel lblTotalCargadasDevolucionç;
    private javax.swing.JLabel lblTotalDevueltas;
    private javax.swing.JLabel lblTotalOrdenes;
    private javax.swing.JLabel lblTotalOrdenesBusqueda;
    private javax.swing.JLabel lblTotalOrdenesDevolucion;
    private javax.swing.JLabel lbl_ano_entrega;
    private javax.swing.JLabel lbl_ano_entrega1;
    private javax.swing.JLabel lbl_mes_entrega;
    private javax.swing.JLabel lbl_mes_entrega1;
    private javax.swing.JLabel lbl_nombrecolegiado;
    private javax.swing.JLabel lbl_validadorentrega;
    private javax.swing.JLabel lbl_validadorentrega1;
    private javax.swing.JLabel lbl_validadorentrega2;
    private javax.swing.JLabel lbl_validadorentrega3;
    private javax.swing.JLabel lbl_validadorentrega4;
    private javax.swing.JLabel lbl_validadorentrega5;
    private javax.swing.JLabel lblodenesRestantes;
    private javax.swing.JLabel lbrestoValidador;
    private javax.swing.JLabel lbtotalValidador1;
    private javax.swing.JPanel panelGraficos;
    private javax.swing.JTable tablaAsignados;
    private javax.swing.JTable tablaDevolucion;
    private javax.swing.JTable tablaDisponibles;
    private javax.swing.JTable tablaOrdenesAsignadas;
    private javax.swing.JTable tablaOrdenesCargadas;
    private javax.swing.JTable tb_consulta;
    private javax.swing.JTable tb_recepcion;
    private javax.swing.JTextField txtAñoConsulta;
    private javax.swing.JTextField txtAñoEntrega;
    private javax.swing.JTextField txtAñoRecepcion;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtMesConsulta;
    private javax.swing.JTextField txtMesEntrega;
    private javax.swing.JTextField txtMesRecepcion;
    private javax.swing.JTextField txtValidadorEntrega;
    private javax.swing.JTextField txt_año_devolucion;
    private javax.swing.JTextField txt_año_grafico;
    private javax.swing.JTextField txt_cantidaddeordenes;
    private javax.swing.JTextField txt_matricula;
    private javax.swing.JTextField txt_matriculac;
    private javax.swing.JTextField txt_mes_devolucion;
    private javax.swing.JTextField txt_mes_grafico;
    private javax.swing.JLabel txt_nombrecolegiadoc;
    private javax.swing.JTextField txt_validador_devolucion;
    // End of variables declaration//GEN-END:variables
}
