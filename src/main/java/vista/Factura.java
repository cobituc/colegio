package vista;

import controlador.ConexionMariaDB;
import controlador.NumberToLetterConverter;
import controlador.camposordenes_osde;
import controlador.camposordenes_ss;
import static vista.Login.idusuario;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Factura extends javax.swing.JFrame {

    int contadorj = 0, id_obra_social, idobraimprime, id_usuario_validador, periodo_colegiado, id_usuario, ddjj = 0, troquel = 0, valor = 0;
    String ObraSocial, CodObra, año, mes, periodo, colegiado, fecha2, periodo2;
    TextAutoCompleter textAutoAcompleter2;
    String[] obrasocial = new String[500];
    int[] idobrasocial = new int[500];
    String[] nombreobrasocial = new String[500];
    Double[] arancel = new Double[500];
    int[] idobra = new int[150000];
    int contadorobrasocial = 0;
    Hiloobrasocial hilo2;
    Hiloobrasocialtodas hilo3;
    Hiloobrasocialperiodo hilo4;
    Hiloobrasocialmatricula hilo5;
    Hiloobrasocialddjj hilo6;

    public Factura(java.awt.Frame parent, boolean modal) {
        //super(parent, modal);
        initComponents();
        //////////////////Grupo de Botones
        buttonGroup1.add(opcion1);
        buttonGroup1.add(opcion2);
        buttonGroup1.add(opcion3);
        buttonGroup1.add(opcion4);
        buttonGroup1.add(opcion5);
        /////////////////////
        progreso.setVisible(false);

        textAutoAcompleter2 = new TextAutoCompleter(txtobrasocial);
        traerobrasosial();
        cargarperiodo();
        cargarobrasocial();
        this.setLocationRelativeTo(null);
    }

    public class Hiloobrasocial extends Thread {

        JProgressBar progreso;

        public Hiloobrasocial(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            ///////////////////facturacion/////////////////////////////////////
            int cantidad = 100 / 10;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab, cuit;
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            LinkedList<camposordenes_osde> Resultados3 = new LinkedList<camposordenes_osde>();
            LinkedList<camposordenes_ss> Resultados_ss = new LinkedList<camposordenes_ss>();
            Resultados3.clear();
            Resultados_ss.clear();
            String nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0.00, pesos = 0, importetotal = 0, centavos = 0, coseguro = 0.00, totalconcoseguro = 0.00, coseguro_ss = 0.00;
            int pacientes = 0, flag_osde = 0;
            DecimalFormat df = new DecimalFormat("0.00");
            String nombre_obra = "", cod_obra = "", matricula = "";
            int bandera20012 = 0;
            try {
                Statement st3 = cn.createStatement();
                ResultSet rs3 = st3.executeQuery("SELECT id_colegiados,nombre_colegiado,matricula_colegiado,factura,direccion_laboratorio,localidad_laboratorio,periodos,estado_periodo,numerodireccion_laboratorio, periodos.periodo,estado_facturacion,cuil_colegiado \n"
                        + " FROM colegiados , periodos\n"
                        + " WHERE matricula_colegiado=" + txtcolegiado.getText() + " and tipo_profesional='B'");

                if (rs3.next()) {
                    colegiado = rs3.getInt("id_colegiados");
                    matricula = rs3.getString("matricula_colegiado");
                    nombre_colegiado = rs3.getString("nombre_colegiado");
                    domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                    localidad_lab = rs3.getString("localidad_laboratorio");
                    cuit = rs3.getString("cuil_colegiado");

                    String sql = "";

                    if (rs3.getInt("id_colegiados") == 1173 && (id_obra_social == 24 || id_obra_social == 94)) {
                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0  order by ip,ordenes.id_orden,detalle_ordenes.id_detalle";
                        bandera20012 = 1;
                        System.out.println("ingresa en 20012");

                    } else {
                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 ";

                        System.out.println("ingresa en otras os");
                    }

                    if ((id_obra_social == 24 || id_obra_social == 94) && valor == 3) {

                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + " and tipo_orden=" + valor + " AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 ";

                    } else if (bandera20012 == 0) {

                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + " and tipo_orden!=3 AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 ";
                    }

                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);

                    pesos = 0;
                    pacientes = 0;
                    practicas = 0;
                    coseguro = 0;
                    coseguro_ss = 0;
                    Resultados3.clear();
                    Resultados_ss.clear();
                    total = 0;
                    int id_ordenes = 0;
                    Statement st2 = cn.createStatement();
                    String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial WHERE id_obrasocial= " + id_obra_social;
                    ResultSet rs2 = st2.executeQuery(sql2);
                    rs2.next();
                    nombre_obra = rs2.getString("razonsocial_obrasocial");
                    cod_obra = rs2.getString("codigo_obrasocial");
                    uni_gastos = df.format(rs2.getDouble("importeunidaddegasto_obrasocial"));
                    uni_Honorarios = df.format(rs2.getDouble("importeunidaddearancel_obrasocial"));

                    //////////////////////////////////////////////////////////////////
                    while (rs.next()) {
                        if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {

                            camposordenes_ss tipo_ss;
                            if (id_ordenes == 0) {
                                tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {
                                    tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    tipo_ss = new camposordenes_ss("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''", df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                }
                            }
                            total = Redondeardosdigitos(total);
                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            coseguro = Redondeardosdigitos(coseguro);
                            coseguro = coseguro + Redondeardosdigitos(rs.getDouble("detalle_ordenes.coseguro"));
                            practicas = practicas + 1;
                            Resultados_ss.add(tipo_ss);
                            bandera = 1;

                        } else {
                            camposordenes_osde tipo;
                            if (id_ordenes == 0) {

                                tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));

                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {

                                    tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    //tipo = new camposordenes("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                    tipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                }
                            }
                            total = Redondeardosdigitos(total);
                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            practicas = practicas + 1;
                            Resultados3.add(tipo);
                            bandera = 1;
                        }

                    }
                    /////////////////////////////////////////////////////////////////////////////////////
                    if (id_obra_social == 89 || id_obra_social == 21 || id_obra_social == 108 || id_obra_social == 95) {

                        Statement st7 = cn.createStatement();
                        String sql7 = "SELECT  sum(coseguro)\n"
                                + "FROM ordenes\n"
                                + "WHERE periodo=" + txtaño.getText() + txtmes.getText() + " and estado_orden=1 and id_obrasocial=" + id_obra_social + " and id_colegiados=" + rs3.getInt("id_colegiados");
                        ResultSet rs7 = st7.executeQuery(sql7);
                        if (rs7.next()) {
                            coseguro = 0;
                            coseguro = Redondeardosdigitos(rs7.getDouble(1));
                        }
                    }

                    pesos = pesos + total;
                    /////////////////////////////////////////////////////
                    if (bandera == 1) {
                        System.out.println("Sale del while: " + cod_obra);
                        pesos = Redondeardosdigitos(pesos);
                        importetotal = pesos;
                        totalconcoseguro = importetotal - coseguro;
                        ///////////////////////////////////////////////////////////////////////////////////
                        String mes = "", año = txtaño.getText();
                        String cadena = txtmes.getText();
                        if (cadena.equals("01")) {
                            mes = "Enero";
                        }
                        if (cadena.equals("02")) {
                            mes = "Febrero";
                        }
                        if (cadena.equals("03")) {
                            mes = "Marzo";
                        }
                        if (cadena.equals("04")) {
                            mes = "Abril";
                        }
                        if (cadena.equals("05")) {
                            mes = "Mayo";
                        }
                        if (cadena.equals("06")) {
                            mes = "Junio";
                        }
                        if (cadena.equals("07")) {
                            mes = "Julio";
                        }
                        if (cadena.equals("08")) {
                            mes = "Agosto";
                        }
                        if (cadena.equals("09")) {
                            mes = "Septiembre";
                        }
                        if (cadena.equals("10")) {
                            mes = "Octubre";
                        }
                        if (cadena.equals("11")) {
                            mes = "Noviembre";
                        }
                        if (cadena.equals("12")) {
                            mes = "Diciembre";
                        }
                        String periodo2 = mes + " " + año;
                        centavos = Redondearcentavos(pesos);
                        pesos = Redondeardosdigitos(pesos - centavos / 100);
                        total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                        total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                        Map parametros = new HashMap();
                        parametros.put("matricula", matricula);
                        parametros.put("periodo", periodo2);
                        parametros.put("fecha", fecha2);
                        if ((cod_obra.equals("3100") || cod_obra.equals("3102")) && valor == 3) {
                            nombre_obra = nombre_obra + " INTERNADO";
                            parametros.put("obra_social", nombre_obra);
                        } else {
                            parametros.put("obra_social", nombre_obra);
                        }
                        parametros.put("num_obra_social", cod_obra);
                        parametros.put("laboratorio", nombre_colegiado);
                        parametros.put("domicilio_lab", domicilio_lab);
                        parametros.put("localidad", localidad_lab);
                        parametros.put("pacientes", String.valueOf(pacientes));
                        parametros.put("practicas", String.valueOf(practicas));
                        /////////////////////////////////////
                        JFrame viewer = new JFrame();
                        //viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logocbt.png")).getImage());
                        viewer.setSize(800, 600);
                        viewer.setLocationRelativeTo(null);
                        JasperViewer jv = null;
                        JasperPrint jPrint = null;
                        //////////////////////////////////////
                        if (cod_obra.equals("511")) { ////////comparo para saber que pdf realizar 511 - MEDIFE

                            try {
                                // String periodo2 = mes + " " + año;
                                pesos = pesos * 1.105;
                                centavos = Redondearcentavos(pesos);
                                pesos = Redondeardosdigitos(pesos - centavos / 100);
                                total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_IVA.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("Subtotal", df.format(importetotal));
                                parametros.put("IVA", df.format(importetotal * 0.21));
                                parametros.put("total", df.format(importetotal * 1.21));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }

                        if (cod_obra.equals("50015") || cod_obra.equals("2700") || cod_obra.equals("40813") || cod_obra.equals("50016")) {
                            try {

                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_coseguro.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                ///Coseguro
                                parametros.put("coseguro", df.format(coseguro));
                                parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));

                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                            try {
                                pesos = totalconcoseguro;
                                centavos = Redondearcentavos(pesos);
                                pesos = Redondeardosdigitos(pesos - centavos / 100);
                                total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_SS.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                ///Coseguro
                                parametros.put("cuit", cuit);
                                parametros.put("coseguro", df.format(coseguro));
                                parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados_ss));

                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("3100") || cod_obra.equals("3101") || cod_obra.equals("3102")) {
                            try {
                                //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_osde.jrxml");
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_OSDE.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");
                                //JasperPrintManager.printReport(jPrint, false);

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("50015") || cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("511") || cod_obra.equals("1807") || cod_obra.equals("2700")
                                || cod_obra.equals("40813") || cod_obra.equals("50016")) {
                        } else {

                            System.out.println(cod_obra);
                            try {
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_Fecha.jasper"));
                                ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }

                        }
                        ////////////////////////////////
                        jv = new JasperViewer(jPrint, false);
                        viewer.getContentPane().add(jv.getContentPane());
                        viewer.setVisible(true);
                        ////////////////////////////////////
                    }

                    indice = indice + cantidad;

                }
                btnimprimir.setEnabled(true);
                JOptionPane.showMessageDialog(null, "Los detalles fueron generados correctamente");
                //////////////////////////////////////////////////////////////////////////////
                cn.close();
            } catch (Exception e) {
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {

            }

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    public class Hiloobrasocialtodas extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialtodas(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {

            ///////////////////facturacion/////////////////////////////////////
            int cantidad = 100 / 10;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab, cuit;
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            Connection cn2 = mysql.Conectar();
            Connection cn3 = mysql.Conectar();
            LinkedList<camposordenes_osde> Resultados3 = new LinkedList<camposordenes_osde>();
            LinkedList<camposordenes_ss> Resultados_ss = new LinkedList<camposordenes_ss>();
            Resultados3.clear();
            Resultados_ss.clear();
            String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0.00, pesos = 0, importetotal = 0, centavos = 0, coseguro = 0.00, totalconcoseguro = 0.00, coseguro_ss = 0.00;
            int pacientes = 0;
            DecimalFormat df = new DecimalFormat("0.00");
            String nombre_obra = "", cod_obra = "", matricula = "";

            try {
                //////////////////////// Consulto todas las matriculas que realizaron la o.s.
                Statement st10 = cn3.createStatement();
                ResultSet rs10 = st10.executeQuery("SELECT DISTINCT(ordenes.id_obrasocial),colegiados.matricula_colegiado\n"
                        + "FROM ordenes \n"
                        + "INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados\n"
                        + "WHERE ordenes.id_obrasocial=" + id_obra_social + " and ordenes.periodo=" + periodo + " and estado_orden!=0\n"
                        + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED))");

                while (rs10.next()) {
                    Statement st3 = cn2.createStatement();
                    ResultSet rs3 = st3.executeQuery("SELECT id_colegiados,nombre_colegiado,matricula_colegiado,factura,direccion_laboratorio,localidad_laboratorio,periodos,estado_periodo,numerodireccion_laboratorio, periodos.periodo,estado_facturacion,cuil_colegiado \n"
                            + " FROM colegiados , periodos\n"
                            + " WHERE id_colegiado=" + rs10.getInt(2));
                    rs3.next();
                    int ban_caso = 0;
                    System.out.println(rs3.getInt("matricula_colegiado"));
                    colegiado = rs3.getInt("id_colegiados");
                    matricula = rs3.getString("matricula_colegiado");
                    nombre_colegiado = rs3.getString("nombre_colegiado");
                    domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                    localidad_lab = rs3.getString("localidad_laboratorio");
                    cuit = rs3.getString("cuil_colegiado");
                    /////////////////////////////////////////////////////
                    /// SELECCIONO LOS BIOQUIMICOS QUE FACTURARON ESA MATRICULA
                    Statement st6 = cn.createStatement();
                    String sql6 = "SELECT DISTINCT(ordenes.id_obrasocial), Int_codigo_obrasocial, colegiados.matricula_colegiado FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados "
                            + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial "
                            + "WHERE ordenes.periodo=" + periodo + " ordenes.id_obrasocial=" + id_obra_social + "  and estado_orden!=0  order by(CAST(colegiados.matricula_colegiado as UNSIGNED))";
                    ResultSet rs6 = st6.executeQuery(sql6);
                    /////////////////

                    String sql = "";
                    if (rs3.getInt("id_colegiados") == 1173) {
                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + periodo + " AND ordenes.id_obrasocial=" + id_obra_social + "  \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 order by obrasocial.Int_codigo_obrasocial,ordenes.id_orden,detalle_ordenes.id_detalle,ip";

                    } else {
                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + periodo + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 order by obrasocial.Int_codigo_obrasocial,ordenes.id_orden, detalle_ordenes.id_detalle";

                    }
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);

                    while (rs6.next()) {

                        System.out.println(rs6.getInt("Int_codigo_obrasocial"));
                        pesos = 0;
                        pacientes = 0;
                        practicas = 0;
                        coseguro = 0;
                        coseguro_ss = 0;
                        Resultados3.clear();
                        Resultados_ss.clear();
                        total = 0;
                        int id_ordenes = 0;
                        Statement st2 = cn.createStatement();
                        String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial WHERE id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial");
                        ResultSet rs2 = st2.executeQuery(sql2);
                        rs2.next();
                        nombre_obra = rs2.getString("razonsocial_obrasocial");
                        cod_obra = rs2.getString("codigo_obrasocial");
                        uni_gastos = df.format(rs2.getDouble("importeunidaddegasto_obrasocial"));
                        uni_Honorarios = df.format(rs2.getDouble("importeunidaddearancel_obrasocial"));

                        //////////////////////////////////////////////////////////////////
                        if (ban_caso == 1) {
                            if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                    || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                    || cod_obra.equals("1806") || cod_obra.equals("1810")) {

                                camposordenes_ss tipo_ss;
                                if (id_ordenes == 0) {
                                    tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    if (id_ordenes != rs.getInt("id_orden")) {
                                        tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                        band = 1;
                                        id_ordenes = rs.getInt("id_orden");
                                        pacientes = pacientes + 1;
                                    } else {
                                        tipo_ss = new camposordenes_ss("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''", df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                    }
                                }
                                //  System.out.println(rs6.getInt("Int_codigo_obrasocial"));
                                total = Redondeardosdigitos(total);
                                total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                coseguro = Redondeardosdigitos(coseguro);
                                coseguro = coseguro + Redondeardosdigitos(rs.getDouble("detalle_ordenes.coseguro"));
                                practicas = practicas + 1;
                                Resultados_ss.add(tipo_ss);
                                bandera = 1;

                            } else {
                                camposordenes_osde tipo;
                                if (id_ordenes == 0) {
                                    //tipo = new camposordenes(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                    tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                    //band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    if (id_ordenes != rs.getInt("id_orden")) {
                                        //tipo = new camposordenes(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                        tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                        band = 1;
                                        id_ordenes = rs.getInt("id_orden");
                                        pacientes = pacientes + 1;
                                    } else {
                                        //tipo = new camposordenes("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                        tipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                    }
                                }
                                ////////////////cargo la matriz para imprimir//////////                                   
                                //  if (band == 0) {
                                total = Redondeardosdigitos(total);
                                total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                practicas = practicas + 1;
                                Resultados3.add(tipo);
                                bandera = 1;
                                ban_caso = 0;
                            }

                        }
                        /////////////////////////////////////////////////////////////////////////////////////
                        if (rs6.getString(1).equals("89")) {
                            Statement st7 = cn.createStatement();
                            String sql7 = "SELECT  sum(coseguro)\n"
                                    + "FROM ordenes\n"
                                    + "WHERE periodo=" + periodo + " and estado_orden=1 and id_obrasocial=89 and id_colegiados=" + rs3.getInt("id_colegiados");
                            ResultSet rs7 = st7.executeQuery(sql7);
                            if (rs7.next()) {
                                coseguro = Redondeardosdigitos(rs7.getDouble(1));
                            }
                        }

                        while (rs.next() && rs6.getInt("id_obrasocial") == rs.getInt("obrasocial.id_obrasocial")) {

                            if (rs6.getInt("id_obrasocial") == rs.getInt("obrasocial.id_obrasocial")) {

                                if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                        || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                        || cod_obra.equals("1806") || cod_obra.equals("1810")) {

                                    camposordenes_ss tipo_ss;
                                    if (id_ordenes == 0) {

                                        tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                        id_ordenes = rs.getInt("id_orden");
                                        pacientes = pacientes + 1;
                                    } else {
                                        if (id_ordenes != rs.getInt("id_orden")) {
                                            tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                            band = 1;
                                            id_ordenes = rs.getInt("id_orden");
                                            pacientes = pacientes + 1;
                                        } else {
                                            tipo_ss = new camposordenes_ss("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''", df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                        }

                                    }
                                    //System.out.println(tipo_ss);
                                    total = Redondeardosdigitos(total);
                                    total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                    coseguro = Redondeardosdigitos(coseguro);
                                    coseguro = coseguro + Redondeardosdigitos(rs.getDouble("detalle_ordenes.coseguro"));
                                    practicas = practicas + 1;
                                    Resultados_ss.add(tipo_ss);
                                    bandera = 1;

                                } else {
                                    camposordenes_osde tipo;
                                    if (id_ordenes == 0) {
                                        //tipo = new camposordenes(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                        tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                        //band = 1;
                                        id_ordenes = rs.getInt("id_orden");
                                        pacientes = pacientes + 1;
                                    } else {
                                        if (id_ordenes != rs.getInt("id_orden")) {

                                            tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                            band = 1;
                                            id_ordenes = rs.getInt("id_orden");
                                            pacientes = pacientes + 1;
                                        } else {
                                            //tipo = new camposordenes("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                            tipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                        }

                                    }

                                    total = Redondeardosdigitos(total);
                                    total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                    practicas = practicas + 1;
                                    Resultados3.add(tipo);
                                    bandera = 1;
                                }
                            }
                        }

                        if (ban_caso == 0) {
                            ban_caso = 1;
                        }

                        pesos = pesos + total;
                        /////////////////////////////////////////////////////
                        if (bandera == 1) {
                            pesos = Redondeardosdigitos(pesos);
                            importetotal = pesos;
                            totalconcoseguro = importetotal - coseguro;
                            ///////////////////////////////////////////////////////////////////////////////////
                            String mes = "", año = periodo.substring(0, 4), cadena = periodo.substring(4, 6);
                            if (cadena.equals("01")) {
                                mes = "Enero";
                            }
                            if (cadena.equals("02")) {
                                mes = "Febrero";
                            }
                            if (cadena.equals("03")) {
                                mes = "Marzo";
                            }
                            if (cadena.equals("04")) {
                                mes = "Abril";
                            }
                            if (cadena.equals("05")) {
                                mes = "Mayo";
                            }
                            if (cadena.equals("06")) {
                                mes = "Junio";
                            }
                            if (cadena.equals("07")) {
                                mes = "Julio";
                            }
                            if (cadena.equals("08")) {
                                mes = "Agosto";
                            }
                            if (cadena.equals("09")) {
                                mes = "Septiembre";
                            }
                            if (cadena.equals("10")) {
                                mes = "Octubre";
                            }
                            if (cadena.equals("11")) {
                                mes = "Noviembre";
                            }
                            if (cadena.equals("12")) {
                                mes = "Diciembre";
                            }
                            String periodo2 = mes + " " + año;
                            centavos = Redondearcentavos(pesos);
                            pesos = Redondeardosdigitos(pesos - centavos / 100);
                            total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                            total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                            Map parametros = new HashMap();
                            parametros.put("matricula", matricula);
                            parametros.put("periodo", periodo2);
                            parametros.put("fecha", fecha2);
                            parametros.put("obra_social", nombre_obra);
                            parametros.put("num_obra_social", cod_obra);
                            parametros.put("laboratorio", nombre_colegiado);
                            parametros.put("domicilio_lab", domicilio_lab);
                            parametros.put("localidad", localidad_lab);
                            parametros.put("pacientes", String.valueOf(pacientes));
                            parametros.put("practicas", String.valueOf(practicas));
                            if (cod_obra.equals("511")) { ////////comparo para saber que pdf realizar 511 - MEDIFE

                                try {

                                    // String periodo2 = mes + " " + año;
                                    pesos = pesos * 1.21;
                                    centavos = Redondearcentavos(pesos);
                                    pesos = Redondeardosdigitos(pesos - centavos / 100);
                                    total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                    total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);

                                    JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_IVA.jasper"));

                                    parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                    parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                    parametros.put("Subtotal", df.format(importetotal));
                                    parametros.put("IVA", df.format(importetotal * 0.21));
                                    parametros.put("total", df.format(importetotal * 1.21));
                                    JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                    //JasperExportManager.exportReportToPdfFile(jPrint, "C:\\PDF-FACTURACION\\PDF-FACTURACION" + periodo + "\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                    JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                    // JasperViewer.viewReport(jPrint, true);
                                } catch (JRException ex) {
                                    System.err.println("Error iReport: " + ex.getMessage());
                                }
                            }

                            if (cod_obra.equals("50015")) {
                                try {
                                    JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_coseguro.jasper"));
                                    parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                    parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                    parametros.put("total", df.format(importetotal));
                                    ///Coseguro
                                    parametros.put("coseguro", df.format(coseguro));
                                    parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                    JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                    JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                    // JasperViewer.viewReport(jPrint, true);
                                    // JasperPrintManager.printReport(jPrint, false);
                                } catch (JRException ex) {
                                    System.err.println("Error iReport: " + ex.getMessage());
                                }
                            }
                            if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                    || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                    || cod_obra.equals("1806") || cod_obra.equals("1810")) {
                                try {
                                    pesos = totalconcoseguro;
                                    centavos = Redondearcentavos(pesos);
                                    pesos = Redondeardosdigitos(pesos - centavos / 100);
                                    total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                    total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);

                                    //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_SS.jrxml");
                                    JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_SS.jasper"));
                                    parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                    parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                    parametros.put("total", df.format(importetotal));
                                    ///Coseguro
                                    parametros.put("cuit", cuit);
                                    parametros.put("coseguro", df.format(coseguro));
                                    parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                    JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados_ss));
                                    JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                    //JasperViewer.viewReport(jPrint, true);
                                    //JasperPrintManager.printReport(jPrint, false);
                                } catch (JRException ex) {
                                    System.err.println("Error iReport: " + ex.getMessage());
                                }
                            }
                            if (cod_obra.equals("3100") || cod_obra.equals("3101") || cod_obra.equals("3102")) {
                                try {
                                    //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_osde.jrxml");
                                    JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_OSDE.jasper"));
                                    parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                    parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                    parametros.put("total", df.format(importetotal));
                                    JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                    JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                    JasperPrintManager.printReport(jPrint, false);
                                    //JasperViewer.viewReport(jPrint, true);

                                } catch (JRException ex) {
                                    System.err.println("Error iReport: " + ex.getMessage());
                                }
                            }
                            if (cod_obra.equals("50015") || cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                    || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                    || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("511")) {
                            } else {

                                System.out.println(cod_obra);
                                try {
                                    //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_Fecha.jrxml");
                                    JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_Fecha.jasper"));
                                    ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                    parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                    parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                    parametros.put("total", df.format(importetotal));
                                    JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                    JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                    //JasperExportManager.exportReportToPdfFile(jPrint, "C:\\PDF-FACTURACION\\PDF-FACTURACION" + periodo + "\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                    // JasperViewer.viewReport(jPrint, true);
                                    JasperPrintManager.printReport(jPrint, false);
                                } catch (JRException ex) {
                                    System.err.println("Error iReport: " + ex.getMessage());
                                }

                            }

                        }

                    }
                    indice = indice + cantidad;

                }
                btnimprimir.setEnabled(true);
                JOptionPane.showMessageDialog(null, "Los detalles fueron generados correctamente");
                //////////////////////////////////////////////////////////////////////////////
                cn.close();
            } catch (Exception e) {
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {

            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    public class Hiloobrasocialperiodo extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialperiodo(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
///////////////////facturacion/////////////////////////////////////
            int cantidad = 100 / 10;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab, cuit;
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            LinkedList<camposordenes_osde> Resultados3 = new LinkedList<camposordenes_osde>();
            LinkedList<camposordenes_ss> Resultados_ss = new LinkedList<camposordenes_ss>();
            Resultados3.clear();
            Resultados_ss.clear();
            String nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0.00, pesos = 0, importetotal = 0, centavos = 0, coseguro = 0.00, totalconcoseguro = 0.00, coseguro_ss = 0.00;
            int pacientes = 0, flag_osde = 0;
            DecimalFormat df = new DecimalFormat("0.00");
            String nombre_obra = "", cod_obra = "", matricula = "";
            int bandera20012 = 0;
            System.out.println("id_obra_social" + id_obra_social);
            try {
                Statement st3 = cn.createStatement();
                ResultSet rs3 = st3.executeQuery("SELECT DISTINCT (id_colegiados) ,nombre_colegiado,matricula_colegiado,factura,direccion_laboratorio,localidad_laboratorio,periodos,estado_periodo,numerodireccion_laboratorio,estado_facturacion,cuil_colegiado, LPAD(matricula_colegiado,5, '0') as mat \n"
                        + "FROM ordenes \n"
                        + "INNER JOIN colegiados  USING (id_colegiados) \n"
                        + " WHERE factura='SI' and tipo_profesional='B' and periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + " and id_colegiados!=1212 and id_colegiados!=1315"
                        + " and ordenes.estado_orden!=0 \n"
                        + " order by(CAST(colegiados.matricula_colegiado as UNSIGNED))");

                while (rs3.next()) {
                    System.out.println("matricula: " + rs3.getInt("matricula_colegiado"));
                    System.out.println("id_colegiado: " + rs3.getInt("id_colegiados"));
                    colegiado = rs3.getInt("id_colegiados");
                    matricula = rs3.getString("matricula_colegiado");
                    nombre_colegiado = rs3.getString("nombre_colegiado");
                    domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                    localidad_lab = rs3.getString("localidad_laboratorio");
                    cuit = rs3.getString("cuil_colegiado");

                    System.out.println("id_obrasocial: " + id_obra_social);
                    System.out.println(txtaño.getText() + txtmes.getText());
                    String sql = "";

                    if (rs3.getInt("id_colegiados") == 1173 && (id_obra_social == 24 || id_obra_social == 94)) {
//                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
//                                + "FROM ordenes \n"
//                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
//                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
//                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
//                                + "WHERE ordenes.periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
//                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0  order by ip,ordenes.id_orden,detalle_ordenes.id_detalle";
                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial \n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0  order by ip,ordenes.id_orden,detalle_ordenes.id_detalle";
                        bandera20012 = 1;
                        System.out.println("ingresa en 20012");

                    } else {
                        System.out.println("ingresa en Prensa 3");
                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 ";

                        System.out.println("ingresa en otras os");
                    }

                    if ((id_obra_social == 24 || id_obra_social == 94) && valor == 3) {

                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + " and tipo_orden=" + valor + " AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 ";

                    } else if (bandera20012 == 0) {

                        /*sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtaño.getText() + txtmes.getText() + " and ordenes.id_obrasocial=" + id_obra_social + " and tipo_orden!=3 AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 ";*/
                    }

                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);

                    pesos = 0;
                    pacientes = 0;
                    practicas = 0;
                    coseguro = 0;
                    coseguro_ss = 0;
                    Resultados3.clear();
                    Resultados_ss.clear();
                    total = 0;
                    int id_ordenes = 0;
                    Statement st2 = cn.createStatement();
                    String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial WHERE id_obrasocial= " + id_obra_social;
                    ResultSet rs2 = st2.executeQuery(sql2);
                    rs2.next();

                    System.out.println("Toma datos de obra social");

                    nombre_obra = rs2.getString("razonsocial_obrasocial");
                    cod_obra = rs2.getString("codigo_obrasocial");
                    uni_gastos = df.format(rs2.getDouble("importeunidaddegasto_obrasocial"));
                    uni_Honorarios = df.format(rs2.getDouble("importeunidaddearancel_obrasocial"));

                    //////////////////////////////////////////////////////////////////
                    while (rs.next()) {

                        System.out.println("ingresa al while- rs");

                        if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                            System.out.println("cod_obra=" + cod_obra);
                            camposordenes_ss tipo_ss;
                            if (id_ordenes == 0) {
                                tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {
                                    tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    tipo_ss = new camposordenes_ss("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''", df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                }
                            }
                            total = Redondeardosdigitos(total);
                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            coseguro = Redondeardosdigitos(coseguro);
                            coseguro = coseguro + Redondeardosdigitos(rs.getDouble("detalle_ordenes.coseguro"));
                            practicas = practicas + 1;
                            Resultados_ss.add(tipo_ss);
                            bandera = 1;

                        } else {
                            System.out.println("ingresa en otras camposordenes_osde");
                            camposordenes_osde tipo;
                            if (id_ordenes == 0) {

                                tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));

                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {

                                    tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    //tipo = new camposordenes("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                    tipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                }
                            }
                            total = Redondeardosdigitos(total);
                            System.out.println("antes de calcular el coseguro:" + total);
                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            practicas = practicas + 1;
                            Resultados3.add(tipo);
                            bandera = 1;
                        }
                    }

                    System.out.println("antes de calcular el coseguro");
                    /////////////////////////////////////////////////////////////////////////////////////
                    if (id_obra_social == 89 || id_obra_social == 21 || id_obra_social == 108 || id_obra_social == 95) {

                        Statement st7 = cn.createStatement();
                        String sql7 = "SELECT  sum(coseguro)\n"
                                + "FROM ordenes\n"
                                + "WHERE periodo=" + txtaño.getText() + txtmes.getText() + " and estado_orden=1 and id_obrasocial=" + id_obra_social + " and id_colegiados=" + rs3.getInt("id_colegiados");
                        ResultSet rs7 = st7.executeQuery(sql7);
                        if (rs7.next()) {
                            coseguro = 0;
                            coseguro = Redondeardosdigitos(rs7.getDouble(1));
                        }
                    }

                    pesos = pesos + total;

                    System.out.println("peso y total:" + pesos);
                    /////////////////////////////////////////////////////
                    if (bandera == 1) {
                        System.out.println("Sale del while: " + cod_obra);
                        pesos = Redondeardosdigitos(pesos);
                        importetotal = pesos;
                        totalconcoseguro = importetotal - coseguro;
                        ///////////////////////////////////////////////////////////////////////////////////

                        String periodo2 = devuelve_mes();

                        centavos = Redondearcentavos(pesos);
                        pesos = Redondeardosdigitos(pesos - centavos / 100);
                        total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                        total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                        Map parametros = new HashMap();
                        parametros.put("matricula", matricula);
                        parametros.put("periodo", periodo2);
                        parametros.put("fecha", fecha2);
                        if ((cod_obra.equals("3100") || cod_obra.equals("3102")) && valor == 3) {
                            nombre_obra = nombre_obra + " INTERNADO";
                            parametros.put("obra_social", nombre_obra);
                        } else {
                            parametros.put("obra_social", nombre_obra);
                        }
                        parametros.put("num_obra_social", cod_obra);
                        parametros.put("laboratorio", nombre_colegiado);
                        parametros.put("domicilio_lab", domicilio_lab);
                        parametros.put("localidad", localidad_lab);
                        parametros.put("pacientes", String.valueOf(pacientes));
                        parametros.put("practicas", String.valueOf(practicas));
                        /////////////////////////////////////
                        JFrame viewer = new JFrame();
                        //viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logocbt.png")).getImage());
                        viewer.setSize(800, 600);
                        viewer.setLocationRelativeTo(null);
                        JasperViewer jv = null;
                        JasperPrint jPrint = null;
                        //////////////////////////////////////
                        if (cod_obra.equals("511")) { ////////comparo para saber que pdf realizar 511 - MEDIFE

                            try {
                                // String periodo2 = mes + " " + año;
                                pesos = pesos * 1.21;
                                centavos = Redondearcentavos(pesos);
                                pesos = Redondeardosdigitos(pesos - centavos / 100);
                                total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_IVA.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("Subtotal", df.format(importetotal));
                                parametros.put("IVA", df.format(importetotal * 0.21));
                                parametros.put("total", df.format(importetotal * 1.21));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }

                        if (cod_obra.equals("50015") || cod_obra.equals("2700") || cod_obra.equals("40813") || cod_obra.equals("50016")) {
                            try {

                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_coseguro.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                ///Coseguro
                                parametros.put("coseguro", df.format(coseguro));
                                parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));

                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                            try {
                                pesos = totalconcoseguro;
                                centavos = Redondearcentavos(pesos);
                                pesos = Redondeardosdigitos(pesos - centavos / 100);
                                total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_SS.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                ///Coseguro
                                parametros.put("cuit", cuit);
                                parametros.put("coseguro", df.format(coseguro));
                                parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados_ss));

                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("3100") || cod_obra.equals("3101") || cod_obra.equals("3102")) {
                            try {
                                //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_osde.jrxml");
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_OSDE.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");
                                //JasperPrintManager.printReport(jPrint, false);

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("50015") || cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("511") || cod_obra.equals("1807") || cod_obra.equals("2700")
                                || cod_obra.equals("40813") || cod_obra.equals("50016")) {
                        } else {

                            System.out.println("Reporte ORDENES FECHA: " + cod_obra);
                            try {
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_Fecha.jasper"));
                                ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtaño.getText() + txtmes.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }

                        }
                        ////////////////////////////////
                        jv = new JasperViewer(jPrint, false);
                        viewer.getContentPane().add(jv.getContentPane());
                        viewer.setVisible(false);
                        ////////////////////////////////////
                    }

                    indice = indice + cantidad;

                }
                btnimprimir.setEnabled(true);
                JOptionPane.showMessageDialog(null, "Los detalles fueron generados correctamente");
                //////////////////////////////////////////////////////////////////////////////
                cn.close();
            } catch (Exception e) {
            }

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    public class Hiloobrasocialmatricula extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialmatricula(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            ///////////////////facturacion/////////////////////////////////////
            int cantidad = 100 / 10;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab, cuit;
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            Connection cn2 = mysql.Conectar();
            Connection cn_ddjj = mysql.Conectar();
            LinkedList<camposordenes_osde> Resultados3 = new LinkedList<camposordenes_osde>();
            LinkedList<camposordenes_ss> Resultados_ss = new LinkedList<camposordenes_ss>();
            Resultados3.clear();
            Resultados_ss.clear();
            String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0.00, pesos = 0, importetotal = 0, centavos = 0, coseguro = 0.00, totalconcoseguro = 0.00, coseguro_ss = 0.00;
            int pacientes = 0, bandera_periodo = 0;

            DecimalFormat df = new DecimalFormat("0.00");
            String nombre_obra = "", cod_obra = "", matricula = "";
            /////////////////////////////////Declaracion Jurada/////////////////////////////
            Statement st_periodo;
            try {
                st_periodo = cn_ddjj.createStatement();
                ResultSet rs_periodo = st_periodo.executeQuery("SELECT id_colegiados,nombre_colegiado,matricula_colegiado,factura,direccion_laboratorio,localidad_laboratorio,periodos,estado_periodo,numerodireccion_laboratorio, periodos.periodo,estado_facturacion,cuil_colegiado \n"
                        + " FROM colegiados , periodos\n"
                        + " WHERE matricula_colegiado=" + txtcolegiado.getText());
                rs_periodo.next();
                if (rs_periodo.getInt("periodos") > Integer.valueOf(periodo) && rs_periodo.getInt("estado_periodo") == 1) {
                    bandera_periodo = 1;
                }

            } catch (SQLException ex) {
                Logger.getLogger(Factura.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (bandera_periodo == 1) {

                if (ddjj == 1 || troquel == 1) {

                    //JOptionPane.showMessageDialog(null, "ingresa a ddjj");
                    total_pesos_letras = "";
                    total_centavos_letras = "";
                    int id_colegiado = 0;
                    try {
                        Statement st_ddjj = cn_ddjj.createStatement();
                        ResultSet rs_ddjj = st_ddjj.executeQuery("select sum(importe), id_colegiados \n"
                                + "from declaracion_jurada \n"
                                + "where periodo_declaracion=" + periodo + " and matricula_colegiado=" + txtcolegiado.getText());
                        if (rs_ddjj.next()) {
                            centavos = Redondearcentavos(rs_ddjj.getDouble(1));
                            pesos = Redondeardosdigitos(rs_ddjj.getDouble(1) - centavos / 100);
                            total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                            total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                            id_colegiado = rs_ddjj.getInt("id_colegiados");
                        }
                        try {
                            Map parametros = new HashMap();
                            parametros.put("id_colegiados", id_colegiado);
                            parametros.put("periodo_ddjj", periodo);
                            parametros.put("fecha", fecha2);
                            parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                            parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                            if (ddjj == 1) {
                                //  JOptionPane.showMessageDialog(null, "ddjj");
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ObraSocial.jasper"));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, cn);
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + txtcolegiado.getText() + ".pdf");
                                JasperPrintManager.printReport(jPrint, false);
                            }

                            if (troquel == 1) {
                                //      JOptionPane.showMessageDialog(null, "troquel");
                                JasperReport report_validacion = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ObraSocial_Validacion.jasper"));
                                JasperPrint jPrint_validacion = JasperFillManager.fillReport(report_validacion, parametros, cn);
                                JasperExportManager.exportReportToPdfFile(jPrint_validacion, "C:\\Descargas-CBT\\" + periodo + "-" + txtcolegiado.getText() + "-validacion-.pdf");
                                JasperPrintManager.printReport(jPrint_validacion, false);
                            }
                            //JOptionPane.showMessageDialog(null, ddjj);

                        } catch (JRException ex) {
                            System.err.println("Error iReport: " + ex.getMessage());
                        }
                        cn_ddjj.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(Factura.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                ////////////////////////////////////////////////////////////////////////////////////////
                total_pesos_letras = "";
                total_centavos_letras = "";
                try {
                    Statement st3 = cn2.createStatement();
                    ResultSet rs3 = st3.executeQuery("SELECT id_colegiados,nombre_colegiado,matricula_colegiado,factura,direccion_laboratorio,localidad_laboratorio,periodos,estado_periodo,numerodireccion_laboratorio, periodos.periodo,estado_facturacion,cuil_colegiado \n"
                            + " FROM colegiados , periodos\n"
                            + " WHERE matricula_colegiado=" + txtcolegiado.getText());

                    while (rs3.next()) {
                        int ban_caso = 0;
                        System.out.println(rs3.getInt("matricula_colegiado"));
                        colegiado = rs3.getInt("id_colegiados");
                        matricula = rs3.getString("matricula_colegiado");
                        nombre_colegiado = rs3.getString("nombre_colegiado");
                        domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                        localidad_lab = rs3.getString("localidad_laboratorio");
                        cuit = rs3.getString("cuil_colegiado");
                        /////////////////////////////////////////////////////
                        /// SELECCIONO LAS OBRAS SOCIALES FACTURADAS POR EL BIOQUIMICO EN UN PERIODO
                        Statement st6 = cn.createStatement();
                        String sql6 = "SELECT DISTINCT(ordenes.id_obrasocial), Int_codigo_obrasocial "
                                + "FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados "
                                + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial "
                                + "WHERE ordenes.periodo=" + periodo + " and colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " and estado_orden!=0  order by (obrasocial.Int_codigo_obrasocial)";
                        ResultSet rs6 = st6.executeQuery(sql6);
                        /////////////////SELECCIONO TODAS LAS 
                        String sql = "";
                        //////////BIOMED NOA S.R.L.
                        if (rs3.getInt("id_colegiados") == 1173) {
                            sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,round(detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial,2) as UB\n"
                                    + "FROM ordenes \n"
                                    + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                    + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                    + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                    + "WHERE ordenes.periodo=" + periodo + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                    + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 order by obrasocial.Int_codigo_obrasocial,ordenes.id_orden,detalle_ordenes.id_detalle,ip";

                        } else {
                            sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,round(detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial,2) as UB\n"
                                    + "FROM ordenes \n"
                                    + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                    + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                    + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                    + "WHERE ordenes.periodo=" + periodo + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                    + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 order by obrasocial.Int_codigo_obrasocial,ordenes.id_orden, detalle_ordenes.id_detalle";

                        }

                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sql);

                        while (rs6.next()) {

                            System.out.println(rs6.getInt("Int_codigo_obrasocial") + " - 1");
                            pesos = 0;
                            pacientes = 0;
                            practicas = 0;
                            coseguro = 0;
                            coseguro_ss = 0;
                            Resultados3.clear();
                            Resultados_ss.clear();
                            total = 0;
                            int id_ordenes = 0;

                            ////////////////////////DATOS OBRASOCIAL/////////////////////////////
                            Statement st2 = cn.createStatement();
                            String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial "
                                    + "FROM obrasocial WHERE id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial");
                            ResultSet rs2 = st2.executeQuery(sql2);
                            rs2.next();
                            nombre_obra = rs2.getString("razonsocial_obrasocial");
                            cod_obra = rs2.getString("codigo_obrasocial");
                            uni_gastos = df.format(rs2.getDouble("importeunidaddegasto_obrasocial"));
                            uni_Honorarios = df.format(rs2.getDouble("importeunidaddearancel_obrasocial"));
                            /////////////////////////////////////////////////////////////////////
                            if (ban_caso == 1) {
                                if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                        || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                        || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                                    System.out.println("caso base");
                                    camposordenes_ss tipo_ss;
                                    if (id_ordenes == 0) {
                                        System.out.println("orden iugual a 0");
                                        tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));

                                        System.out.println("pasa tipo_ss");
                                        id_ordenes = rs.getInt("id_orden");
                                        pacientes = pacientes + 1;
                                    } else {
                                        if (id_ordenes != rs.getInt("id_orden")) {
                                            System.out.println("orden distinto  a " + rs.getInt("id_orden"));
                                            tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                            band = 1;
                                            id_ordenes = rs.getInt("id_orden");
                                            pacientes = pacientes + 1;
                                        } else {
                                            System.out.println("orden igual  a " + rs.getInt("id_orden"));
                                            tipo_ss = new camposordenes_ss("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''", df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                        }
                                    }
                                    total = Redondeardosdigitos(total);
                                    total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                    coseguro = Redondeardosdigitos(coseguro);
                                    coseguro = coseguro + Redondeardosdigitos(rs.getDouble("detalle_ordenes.coseguro"));
                                    practicas = practicas + 1;
                                    Resultados_ss.add(tipo_ss);
                                    bandera = 1;

                                } else {
                                    camposordenes_osde tipo;
                                    if (id_ordenes == 0) {
                                        tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                        id_ordenes = rs.getInt("id_orden");
                                        pacientes = pacientes + 1;
                                    } else {
                                        if (id_ordenes != rs.getInt("id_orden")) {
                                            tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                            band = 1;
                                            id_ordenes = rs.getInt("id_orden");
                                            pacientes = pacientes + 1;
                                        } else {
                                            tipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                        }
                                    }
                                    ////////////////cargo la matriz para imprimir//////////                                   
                                    total = Redondeardosdigitos(total);
                                    total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                    practicas = practicas + 1;
                                    Resultados3.add(tipo);
                                    bandera = 1;
                                    ban_caso = 0;
                                }

                            }
                            /////////////////////////////////////////////////////////////////////////////////////
                            if (rs6.getString(1).equals("89") || rs6.getString(1).equals("95") || rs6.getString(1).equals("21") || rs6.getString(1).equals("108")) {
                                Statement st7 = cn.createStatement();
                                String sql7 = "SELECT  sum(coseguro)\n"
                                        + "FROM ordenes\n"
                                        + "WHERE periodo=" + periodo + " and estado_orden=1 and id_obrasocial=" + rs6.getString(1) + " and id_colegiados=" + rs3.getInt("id_colegiados");
                                ResultSet rs7 = st7.executeQuery(sql7);
                                if (rs7.next()) {
                                    coseguro = 0;
                                    coseguro = Redondeardosdigitos(rs7.getDouble(1));
                                }
                            }

                            while (rs.next() && rs6.getInt("id_obrasocial") == rs.getInt("obrasocial.id_obrasocial")) {

                                if (rs6.getInt("id_obrasocial") == rs.getInt("obrasocial.id_obrasocial")) {

                                    if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                            || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                            || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                                        System.out.println("pasa el caso base");
                                        camposordenes_ss tipo_ss;
                                        if (id_ordenes == 0) {

                                            tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                            id_ordenes = rs.getInt("id_orden");
                                            pacientes = pacientes + 1;
                                        } else {
                                            if (id_ordenes != rs.getInt("id_orden")) {
                                                tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                                band = 1;
                                                id_ordenes = rs.getInt("id_orden");
                                                pacientes = pacientes + 1;
                                            } else {
                                                tipo_ss = new camposordenes_ss("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''", df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                            }

                                        }
                                        //System.out.println(tipo_ss);
                                        total = Redondeardosdigitos(total);
                                        total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                        coseguro = Redondeardosdigitos(coseguro);
                                        coseguro = coseguro + Redondeardosdigitos(rs.getDouble("detalle_ordenes.coseguro"));
                                        practicas = practicas + 1;
                                        Resultados_ss.add(tipo_ss);
                                        bandera = 1;

                                    } else {
                                        camposordenes_osde tipo;
                                        if (id_ordenes == 0) {
                                            //tipo = new camposordenes(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                            tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                            //band = 1;
                                            id_ordenes = rs.getInt("id_orden");
                                            pacientes = pacientes + 1;
                                        } else {
                                            if (id_ordenes != rs.getInt("id_orden")) {
                                                //tipo = new camposordenes(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                                tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                                band = 1;
                                                id_ordenes = rs.getInt("id_orden");
                                                pacientes = pacientes + 1;
                                            } else {
                                                //tipo = new camposordenes("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                                tipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                            }

                                        }

                                        total = Redondeardosdigitos(total);
                                        total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                        practicas = practicas + 1;
                                        Resultados3.add(tipo);
                                        bandera = 1;
                                    }
                                }
                            }

                            if (ban_caso == 0) {
                                ban_caso = 1;
                            }

                            pesos = pesos + total;
                            /////////////////////////////////////////////////////
                            if (bandera == 1) {
                                System.out.println("llega antes de reporte");
                                pesos = Redondeardosdigitos(pesos);
                                importetotal = pesos;
                                totalconcoseguro = importetotal - coseguro;
                                ///////////////Mes y año en letras////////////
                                String periodo2 = devuelve_mes();
                                //////////////////////////////////////////////
                                centavos = Redondearcentavos(pesos);
                                pesos = Redondeardosdigitos(pesos - centavos / 100);
                                total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                                Map parametros = new HashMap();
                                parametros.put("matricula", matricula);
                                parametros.put("periodo", periodo2);
                                parametros.put("fecha", fecha2);
                                parametros.put("obra_social", nombre_obra);
                                parametros.put("num_obra_social", cod_obra);
                                parametros.put("laboratorio", nombre_colegiado);
                                parametros.put("domicilio_lab", domicilio_lab);
                                parametros.put("localidad", localidad_lab);
                                parametros.put("pacientes", String.valueOf(pacientes));
                                parametros.put("practicas", String.valueOf(practicas));

                                if (cod_obra.equals("511")) { ////////comparo para saber que pdf realizar 511 - MEDIFE

                                    try {
                                        // String periodo2 = mes + " " + año;
                                        pesos = pesos * 1.21;
                                        centavos = Redondearcentavos(pesos);
                                        pesos = Redondeardosdigitos(pesos - centavos / 100);
                                        total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                        total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);

                                        JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_IVA.jasper"));

                                        parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                        parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                        parametros.put("Subtotal", df.format(importetotal));
                                        parametros.put("IVA", df.format(importetotal * 0.21));
                                        parametros.put("total", df.format(importetotal * 1.21));
                                        JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                        JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                        JasperPrintManager.printReport(jPrint, false);
                                    } catch (JRException ex) {
                                        System.err.println("Error iReport: " + ex.getMessage());
                                        JOptionPane.showMessageDialog(null, ex.getMessage());
                                    }
                                }

                                if (cod_obra.equals("50015") || cod_obra.equals("50016") || cod_obra.equals("2700") || cod_obra.equals("40813")) {
                                    try {
                                        JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_coseguro.jasper"));
                                        parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                        parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                        parametros.put("total", df.format(importetotal));
                                        ///Coseguro
                                        parametros.put("coseguro", df.format(coseguro));
                                        parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                        JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                        JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");

                                        if (cod_obra.equals("2700")) {
//                                            JasperViewer.viewReport(jPrint, true);
                                            JasperPrintManager.printReport(jPrint, false);
                                        }

                                    } catch (JRException ex) {
                                        System.err.println("Error iReport: " + ex.getMessage());
                                        JOptionPane.showMessageDialog(null, ex.getMessage());
                                    }
                                }
                                if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                        || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                        || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                                    try {
                                        pesos = totalconcoseguro;
                                        centavos = Redondearcentavos(pesos);
                                        pesos = Redondeardosdigitos(pesos - centavos / 100);
                                        total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                        total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);

                                        JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_SS.jasper"));
                                        parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                        parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                        parametros.put("total", df.format(importetotal));
                                        ///Coseguro
                                        parametros.put("cuit", cuit);
                                        parametros.put("coseguro", df.format(coseguro));
                                        parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                        JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados_ss));
                                        JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                        //JasperViewer.viewReport(jPrint, true);
                                        //JasperPrintManager.printReport(jPrint, false);
                                    } catch (JRException ex) {
                                        System.err.println("Error iReport: " + ex.getMessage());
                                        JOptionPane.showMessageDialog(null, ex.getMessage());
                                    }
                                }
                                if (cod_obra.equals("3100") || cod_obra.equals("3101") || cod_obra.equals("3102")) {
                                    try {
                                        //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_osde.jrxml");
                                        JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_OSDE.jasper"));
                                        parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                        parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                        parametros.put("total", df.format(importetotal));
                                        JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                        JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                        JasperPrintManager.printReport(jPrint, false);
                                        //JasperViewer.viewReport(jPrint, true);

                                    } catch (JRException ex) {
                                        System.err.println("Error iReport: " + ex.getMessage());
                                        JOptionPane.showMessageDialog(null, ex.getMessage());
                                    }
                                }
                                if (cod_obra.equals("13800")) {
                                    try {
                                        //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_Fecha.jrxml");
                                        JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_Fecha.jasper"));
                                        ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                        parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                        parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                        parametros.put("total", df.format(importetotal));
                                        JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                        JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                        //JasperPrintManager.printReport(jPrint, false);
                                    } catch (JRException ex) {
                                        System.err.println("Error iReport: " + ex.getMessage());
                                        JOptionPane.showMessageDialog(null, ex.getMessage());
                                    }
                                }

                                if (cod_obra.equals("50015") || cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                        || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805") || cod_obra.equals("1807")
                                        || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("511")
                                        || cod_obra.equals("3100") || cod_obra.equals("3101") || cod_obra.equals("3102") || cod_obra.equals("13800")
                                        || cod_obra.equals("50016") || cod_obra.equals("2700") || cod_obra.equals("40813")) {
                                } else {

                                    System.out.println(cod_obra);
                                    try {
                                        //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_Fecha.jrxml");
                                        JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_Fecha.jasper"));
                                        ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                        parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                        parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                        parametros.put("total", df.format(importetotal));
                                        JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                        JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + matricula + "-" + cod_obra + ".pdf");
                                        JasperPrintManager.printReport(jPrint, false);
                                    } catch (JRException ex) {
                                        System.err.println("Error iReport: " + ex.getMessage());
                                        JOptionPane.showMessageDialog(null, ex.getMessage());
                                    }

                                }
                            }
                        }
                        indice = indice + cantidad;
                    }
                    btnimprimir.setEnabled(true);
                    JOptionPane.showMessageDialog(null, "Los detalles fueron generados correctamente");
                    //////////////////////////////////////////////////////////////////////////////

                } catch (Exception e) {
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                }
                ////////////////////////// Guarda los datos del usuario      
                String horaImpresion, fechaImpresion, sSQL = "";
                //SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm");
                java.util.Date currentDate1 = new java.util.Date();
                GregorianCalendar calendar1 = new GregorianCalendar();
                calendar1.setTime(currentDate1);
                horaImpresion = formatoTiempo.format(currentDate1);
                /////////////////////////////////////
                //SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                java.util.Date currentDate = new java.util.Date();
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(currentDate);
                fechaImpresion = formato.format(currentDate);

                try {
                    sSQL = "INSERT INTO impresion_facturantes(id_usuarios, id_colegiados, fecha, "
                            + "hora, impresion_ddjj, impresion_validacion, impresion_obrasocial)"
                            + "VALUES(?,?,?,?,?,?,?)";

                    PreparedStatement pst = cn.prepareStatement(sSQL);
                    pst.setInt(1, idusuario);
                    pst.setInt(2, colegiado);
                    pst.setString(3, fechaImpresion);
                    pst.setString(4, horaImpresion);
                    pst.setInt(5, ddjj);
                    pst.setInt(6, troquel);
                    pst.setInt(7, 1);
                    pst.executeUpdate();
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Factura.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                JOptionPane.showMessageDialog(null, "No se encuntra cerrado el Periodo. Cierre el periodo y vuelva a intentar");
                btnimprimir.setEnabled(true);
            }
            ddjj = 0;
            troquel = 0;
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    String devuelve_mes() {
        String mes = "", cadena = periodo.substring(4, 6);
        if (cadena.equals("01")) {
            mes = "Enero";
        }
        if (cadena.equals("02")) {
            mes = "Febrero";
        }
        if (cadena.equals("03")) {
            mes = "Marzo";
        }
        if (cadena.equals("04")) {
            mes = "Abril";
        }
        if (cadena.equals("05")) {
            mes = "Mayo";
        }
        if (cadena.equals("06")) {
            mes = "Junio";
        }
        if (cadena.equals("07")) {
            mes = "Julio";
        }
        if (cadena.equals("08")) {
            mes = "Agosto";
        }
        if (cadena.equals("09")) {
            mes = "Septiembre";
        }
        if (cadena.equals("10")) {
            mes = "Octubre";
        }
        if (cadena.equals("11")) {
            mes = "Noviembre";
        }
        if (cadena.equals("12")) {
            mes = "Diciembre";
        }
        String periodo2 = mes + " " + periodo.substring(0, 4);
        return periodo2;
    }

    public class Hiloobrasocialddjj extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialddjj(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            ///////////////////facturacion/////////////////////////////////////
            String total_pesos_letras, total_centavos_letras;
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            Connection cn_ddjj = mysql.Conectar();
            int colegiado = 0, bandera_periodo = 0, centavos = 0;
            double pesos = 0;

            /////////////////////////////////Declaracion Jurada/////////////////////////////
            Statement st_periodo, st_tabla_periodo;

            try {
                st_periodo = cn_ddjj.createStatement();
                ResultSet rs_periodo = st_periodo.executeQuery("SELECT "
                        + "id_colegiados,nombre_colegiado,matricula_colegiado,factura,"
                        + "direccion_laboratorio,localidad_laboratorio,periodos,estado_periodo,"
                        + "numerodireccion_laboratorio, periodos.periodo,estado_facturacion,cuil_colegiado \n"
                        + " FROM colegiados , periodos\n"
                        + " WHERE matricula_colegiado=" + txtcolegiado.getText());
                rs_periodo.next();

                st_tabla_periodo = cn_ddjj.createStatement();
                ResultSet rs_tabla_periodo = st_tabla_periodo.executeQuery("Select periodo from periodos");
                rs_tabla_periodo.next();

                colegiado = rs_periodo.getInt("id_colegiados");
                System.out.println("periodo2 " + periodo2);
                System.out.println("periodotabla " + rs_periodo.getInt("periodos"));

                if (rs_tabla_periodo.getInt("periodo") > Integer.valueOf(periodo2)) {
                    bandera_periodo = 1;

                }
                if (rs_tabla_periodo.getInt("periodo") == Integer.valueOf(periodo2) && rs_tabla_periodo.getInt("periodo") < rs_periodo.getInt("periodos")) {
                    bandera_periodo = 1;

                }

            } catch (SQLException ex) {
                Logger.getLogger(Factura.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (bandera_periodo == 1) {

                if (ddjj == 1 || troquel == 1) {

                    //JOptionPane.showMessageDialog(null, "ingresa a ddjj");
                    total_pesos_letras = "";
                    total_centavos_letras = "";
                    int id_colegiado = 0;
                    try {
                        Statement st_ddjj = cn_ddjj.createStatement();
                        ResultSet rs_ddjj = st_ddjj.executeQuery("select round(sum(importe),2), id_colegiados  \n"
                                + "from declaracion_jurada \n"
                                + "where periodo_declaracion=" + periodo2 + " and matricula_colegiado=" + txtcolegiado.getText());
                        if (rs_ddjj.next()) {
                            System.out.println("rs_ddjj.getDouble(1): " + rs_ddjj.getDouble(1));
                            centavos = Redondearcentavos(rs_ddjj.getDouble(1));
                            pesos = Redondeardosdigitos(rs_ddjj.getDouble(1) - centavos / 100);
                            total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                            total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                            id_colegiado = rs_ddjj.getInt("id_colegiados");
                        }
                        try {

                            JFrame viewer = new JFrame();
                            //viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logocbt.png")).getImage());
                            viewer.setSize(800, 600);
                            viewer.setLocationRelativeTo(null);
                            JasperViewer jv = null;
                            JasperPrint jPrint = null;
                            JasperPrint jPrint_validacion = null;
                            Map parametros = new HashMap();
                            parametros.put("id_colegiados", id_colegiado);
                            parametros.put("periodo_ddjj", periodo2);
                            parametros.put("fecha", fecha2);
                            parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                            parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                            if (troquel == 1) {
                                //      JOptionPane.showMessageDialog(null, "troquel");
                                JasperReport report_validacion = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ObraSocial_Validacion.jasper"));
                                jPrint_validacion = JasperFillManager.fillReport(report_validacion, parametros, cn);
                                JasperExportManager.exportReportToPdfFile(jPrint_validacion, "C:\\Descargas-CBT\\" + periodo + "-" + txtcolegiado.getText() + "-validacion-.pdf");
//                                JasperPrintManager.printReport(jPrint_validacion, false);

                                jv = new JasperViewer(jPrint_validacion, false);
                                viewer.getContentPane().add(jv.getContentPane());
                                viewer.setVisible(true);

                            }
                            //JOptionPane.showMessageDialog(null, ddjj);
                            if (ddjj == 1) {
                                //  JOptionPane.showMessageDialog(null, "ddjj");
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ObraSocial.jasper"));
                                jPrint = JasperFillManager.fillReport(report, parametros, cn);
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo2 + "-" + txtcolegiado.getText() + ".pdf");
//                                JasperPrintManager.printReport(jPrint, false);

                                jv = new JasperViewer(jPrint, false);
                                viewer.getContentPane().add(jv.getContentPane());
                                viewer.setVisible(true);
                            }

                        } catch (JRException ex) {
                            System.err.println("Error iReport: " + ex.getMessage());
                        }
                        cn_ddjj.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(Factura.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                /////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////// Guarda los datos del usuario      
                String horaImpresion, fechaImpresion, sSQL = "";
                //SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm");
                java.util.Date currentDate1 = new java.util.Date();
                GregorianCalendar calendar1 = new GregorianCalendar();
                calendar1.setTime(currentDate1);
                horaImpresion = formatoTiempo.format(currentDate1);
                /////////////////////////////////////
                //SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                java.util.Date currentDate = new java.util.Date();
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(currentDate);
                fechaImpresion = formato.format(currentDate);

                try {
                    sSQL = "INSERT INTO impresion_facturantes(id_usuarios, id_colegiados, fecha, "
                            + "hora, impresion_ddjj, impresion_validacion, impresion_obrasocial)"
                            + "VALUES(?,?,?,?,?,?,?)";

                    PreparedStatement pst = cn.prepareStatement(sSQL);
                    pst.setInt(1, idusuario);
                    pst.setInt(2, colegiado);
                    pst.setString(3, fechaImpresion);
                    pst.setString(4, horaImpresion);
                    pst.setInt(5, ddjj);
                    pst.setInt(6, troquel);
                    pst.setInt(7, 0);
                    pst.executeUpdate();
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Factura.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                JOptionPane.showMessageDialog(null, "No se encuntra cerrado el Periodo. Cierre el periodo y vuelva a intentar");
                btnimprimir.setEnabled(true);
            }
            ddjj = 0;
            troquel = 0;

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    void cargarperiodo() {
        ////////////////////////////////////////////////////////////////////
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT * FROM periodos ";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            periodo = rs.getString("periodo");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        //////////////////////////////////

        SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);

        fecha2 = formato2.format(currentDate);
        /////////////////////////////////////////////////////////       

        calendar.setTime(currentDate);

    }

    public int Redondearcentavos(double numero) {
       System.out.println("numero1: " + numero);
        double parteDecimal = numero % 1;
        //double parteEntera = numero - parteDecimal;
        numero= Math.round(parteDecimal*100);
        //numero = Math.rint((numero - Math.rint(numero)) * 100);
        System.out.println("numero2: " + numero);
        int i_val = (int) numero;
        return i_val;
//        return Math.rint(numero * 100.0) % 100.0;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    public double Redondeardosdigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    public int completatotal(double numero) {
        //int centavos=Math.rint(numero * 100) % 100;
        int n = (int) numero;
        return n;
    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtcolegiado = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtobrasocial = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtmes = new javax.swing.JFormattedTextField();
        txtaño = new javax.swing.JFormattedTextField();
        btnimprimir = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        progreso = new javax.swing.JProgressBar();
        chk_ddj = new javax.swing.JCheckBox();
        chk_troquel = new javax.swing.JCheckBox();
        opcion1 = new javax.swing.JRadioButton();
        opcion2 = new javax.swing.JRadioButton();
        opcion3 = new javax.swing.JRadioButton();
        opcion4 = new javax.swing.JRadioButton();
        opcion5 = new javax.swing.JRadioButton();

        jScrollPane1.setViewportView(jTree1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar Facturante", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        jPanel3.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Matricula:");

        txtcolegiado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcolegiado.setForeground(new java.awt.Color(0, 102, 204));
        txtcolegiado.setNextFocusableComponent(txtmes);
        txtcolegiado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcolegiadoActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Apellido y Nombre o Razon Social");

        txtapellido.setEditable(false);
        txtapellido.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtapellido.setForeground(new java.awt.Color(0, 102, 204));
        txtapellido.setNextFocusableComponent(txtmes);
        txtapellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtapellidoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtapellido)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(txtcolegiado, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtcolegiado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(133, 133, 133))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resumen de una Obra Social", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Obra Social:");

        txtobrasocial.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtobrasocial.setForeground(new java.awt.Color(0, 102, 204));
        txtobrasocial.setNextFocusableComponent(btnimprimir);
        txtobrasocial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtobrasocialMouseClicked(evt);
            }
        });
        txtobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtobrasocial)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtobrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Período a Imprimir", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Mes:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Año:");

        txtmes.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtmes.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtmes.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmesActionPerformed(evt);
            }
        });

        txtaño.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtaño.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtaño.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtaño.setNextFocusableComponent(txtobrasocial);
        txtaño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtañoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtmes, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtaño)
                .addGap(101, 101, 101))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtmes)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728924 - clock time.png"))); // NOI18N
        btnimprimir.setMnemonic('g');
        btnimprimir.setText("Generar");
        btnimprimir.setNextFocusableComponent(btnsalir);
        btnimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setMnemonic('v');
        btnsalir.setText("Volver");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        chk_ddj.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_ddj.setText("DDJJ");
        chk_ddj.setToolTipText("por matricula, periodo y obra social");
        chk_ddj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chk_ddjActionPerformed(evt);
            }
        });

        chk_troquel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_troquel.setText("Rotulos - Validacion");
        chk_troquel.setToolTipText("por matricula, periodo y obra social");
        chk_troquel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chk_troquelActionPerformed(evt);
            }
        });

        opcion1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        opcion1.setText("Opcion 1");
        opcion1.setToolTipText("Por matricula, periodo y obra social");
        opcion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion1ActionPerformed(evt);
            }
        });

        opcion2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        opcion2.setText("Opcion 2");
        opcion2.setToolTipText("Por periodo y obra social");
        opcion2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion2ActionPerformed(evt);
            }
        });

        opcion3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        opcion3.setText("Opcion 3");
        opcion3.setToolTipText("Por periodo");
        opcion3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion3ActionPerformed(evt);
            }
        });

        opcion4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        opcion4.setText("Opcion 4");
        opcion4.setToolTipText("Por periodo y matricula");
        opcion4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion4ActionPerformed(evt);
            }
        });

        opcion5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        opcion5.setText("Opcion 5");
        opcion5.setToolTipText("Imprimir solo DDJJ o Rotulos");
        opcion5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnimprimir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(106, 106, 106)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addGap(199, 199, 199)
                                                    .addComponent(opcion4))
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                    .addComponent(chk_ddj)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(chk_troquel)
                                                    .addGap(26, 26, 26)))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(opcion2)
                                                .addGap(34, 34, 34)
                                                .addComponent(opcion3))))
                                    .addComponent(opcion1))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(157, 157, 157)
                        .addComponent(opcion5)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(opcion1)
                    .addComponent(opcion2)
                    .addComponent(opcion3)
                    .addComponent(opcion4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(opcion5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chk_ddj)
                    .addComponent(chk_troquel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    void traerobrasosial() {

        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT id_obrasocial,razonsocial_obrasocial,importeunidaddearancel_obrasocial,codigo_obrasocial FROM obrasocial ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idobrasocial[contadorobrasocial] = rs.getInt("id_obrasocial");
                nombreobrasocial[contadorobrasocial] = (rs.getString("razonsocial_obrasocial"));
                arancel[contadorobrasocial] = (rs.getDouble("importeunidaddearancel_obrasocial"));
                obrasocial[contadorobrasocial] = (rs.getString("codigo_obrasocial") + " - " + rs.getString("razonsocial_obrasocial"));
                contadorobrasocial++;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarobrasocial() {
        textAutoAcompleter2.removeAllItems();
        int i = 0;
        // Recorro y cargo las obras sociales
        while (i < contadorobrasocial) {
            textAutoAcompleter2.addItem(obrasocial[i]);
            i++;
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter2.setMode(0); // infijo     

        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter2.setCaseSensitive(false); //No sensible a mayúsculas        

    }

    private void txtobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialActionPerformed
        String cod = "", nom = "", cadena = txtobrasocial.getText();
        int i = 0, k = 0, j = 1;
        ///cod/// 0000 - aaaaaaaaa
        while (k < cadena.length()) {
            if (String.valueOf(cadena.charAt(k)).equals("-")) {
                j = k + 2;
                k = cadena.length();
            } else {
                cod = cod + cadena.charAt(k);
            }
            k++;
        }
        ///nom/// 0000 - aaaaaaaaa
        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }
        while (i < contadorobrasocial) {
            if (cadena.equals(obrasocial[i])) {
                id_obra_social = idobrasocial[i];
                break;
            }
            i++;
        }
        ObraSocial = nom;
        CodObra = cod;
        idobraimprime = id_obra_social;
        txtobrasocial.transferFocus();
    }//GEN-LAST:event_txtobrasocialActionPerformed

    private void txtmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmesActionPerformed
        txtmes.transferFocus();
    }//GEN-LAST:event_txtmesActionPerformed

    private void txtañoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtañoActionPerformed
        año = txtaño.getText();
        mes = txtmes.getText();
        periodo2 = txtaño.getText() + txtmes.getText();
        txtaño.transferFocus();
    }//GEN-LAST:event_txtañoActionPerformed

    private void txtcolegiadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcolegiadoActionPerformed
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        int i = 0;

        try {
            String sSQL = "SELECT id_colegiados,matricula_colegiado,nombre_colegiado,validador,periodos FROM colegiados  where tipo_profesional='B' and factura='SI' and matricula_colegiado=" + txtcolegiado.getText();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            if (rs.next()) {
                txtapellido.setText(rs.getString("nombre_colegiado"));
                periodo_colegiado = rs.getInt("periodos");
                id_usuario = rs.getInt("id_colegiados");
                colegiado = txtcolegiado.getText();
                txtapellido.transferFocus();

            } else {
                JOptionPane.showMessageDialog(null, "Matricula erronea o el validador no puede realizar ordenes para esta matricula...");
                txtcolegiado.requestFocus();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Error en la base de datos");
        }
    }//GEN-LAST:event_txtcolegiadoActionPerformed

    private void txtapellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtapellidoActionPerformed
        if (!txtapellido.getText().equals("")) {
            txtapellido.transferFocus();
        }
    }//GEN-LAST:event_txtapellidoActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirActionPerformed

        if (opcion1.isSelected()) {
            if (id_obra_social == 24 || id_obra_social == 94) {
                //
                int opcion = JOptionPane.YES_NO_OPTION;
                valor = JOptionPane.showConfirmDialog(null, "Imprimir OSDE Internado?", "Warning", opcion);
                //SI =0
                // JOptionPane.showMessageDialog(null, "SI :" + valor);
                if (valor == 0) {
                    valor = 3;
                }

            }
            btnimprimir.setEnabled(false);
            iniciarSplash();
            hilo2 = new Hiloobrasocial(progreso);
            hilo2.start();
            hilo2 = null;
        }

        if (opcion2.isSelected()) {
            btnimprimir.setEnabled(false);
            iniciarSplash();
            hilo4 = new Hiloobrasocialperiodo(progreso);
            hilo4.start();
            hilo4 = null;
        }

        if (opcion3.isSelected()) {
            btnimprimir.setEnabled(false);
            iniciarSplash();
            hilo3 = new Hiloobrasocialtodas(progreso);
            hilo3.start();
            hilo3 = null;
        }

        if (opcion4.isSelected()) {
            btnimprimir.setEnabled(false);
            iniciarSplash();
            if (chk_ddj.isSelected()) {
                ddjj = 1;
            }
            if (chk_troquel.isSelected()) {
                troquel = 1;
            }
            hilo5 = new Hiloobrasocialmatricula(progreso);
            hilo5.start();
            hilo5 = null;
        }
        if ((chk_ddj.isSelected() || chk_troquel.isSelected()) && opcion5.isSelected()) {

            btnimprimir.setEnabled(false);
            iniciarSplash();
            if (chk_ddj.isSelected()) {
                ddjj = 1;
            }
            if (chk_troquel.isSelected()) {
                troquel = 1;
            }
            hilo6 = new Hiloobrasocialddjj(progreso);
            hilo6.start();
            hilo6 = null;
            btnimprimir.setEnabled(true);
        }


    }//GEN-LAST:event_btnimprimirActionPerformed

    private void txtobrasocialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtobrasocialMouseClicked
        String cod = "", nom = "", cadena = txtobrasocial.getText();
        int i = 0, k = 0, j = 1;
        ///cod/// 0000 - aaaaaaaaa
        while (k < cadena.length()) {
            if (String.valueOf(cadena.charAt(k)).equals("-")) {
                j = k + 2;
                k = cadena.length();
            } else {
                cod = cod + cadena.charAt(k);
            }
            k++;
        }
        ///nom/// 0000 - aaaaaaaaa
        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }
        while (i < contadorobrasocial) {
            if (cadena.equals(obrasocial[i])) {
                id_obra_social = idobrasocial[i];
                break;
            }
            i++;
        }
        ObraSocial = nom;
        CodObra = cod;
        idobraimprime = id_obra_social;
        txtobrasocial.transferFocus();
    }//GEN-LAST:event_txtobrasocialMouseClicked

    private void chk_ddjActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chk_ddjActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chk_ddjActionPerformed

    private void chk_troquelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chk_troquelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chk_troquelActionPerformed

    private void opcion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion1ActionPerformed
        txtcolegiado.setEnabled(true);

    }//GEN-LAST:event_opcion1ActionPerformed

    private void opcion2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion2ActionPerformed
        txtcolegiado.setEnabled(false);
    }//GEN-LAST:event_opcion2ActionPerformed

    private void opcion5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion5ActionPerformed
        txtcolegiado.setEnabled(true);
        txtobrasocial.setEnabled(false);
    }//GEN-LAST:event_opcion5ActionPerformed

    private void opcion3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion3ActionPerformed
        txtcolegiado.setEnabled(false);
    }//GEN-LAST:event_opcion3ActionPerformed

    private void opcion4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion4ActionPerformed
        txtcolegiado.setEnabled(true);
        txtobrasocial.setEnabled(false);
    }//GEN-LAST:event_opcion4ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnimprimir;
    private javax.swing.JButton btnsalir;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox chk_ddj;
    private javax.swing.JCheckBox chk_troquel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTree jTree1;
    private javax.swing.JRadioButton opcion1;
    private javax.swing.JRadioButton opcion2;
    private javax.swing.JRadioButton opcion3;
    private javax.swing.JRadioButton opcion4;
    private javax.swing.JRadioButton opcion5;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JFormattedTextField txtaño;
    private javax.swing.JTextField txtcolegiado;
    private javax.swing.JFormattedTextField txtmes;
    private javax.swing.JTextField txtobrasocial;
    // End of variables declaration//GEN-END:variables
}
