package vista;

import controlador.ConexionMariaDB;
import controlador.fnAlinear;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class ObraSocialElegir extends javax.swing.JDialog {

    DefaultTableModel model;
    static int id_obrasocial_cliente;
    static String nombre_obrasocial;
    static String cuit_obrasocial;
    static String direccion_obrasocial;
    static String localidad;
    static String provincia;
    static String wsfe;
    static String condicion_iva;
    static String cp;

    public ObraSocialElegir(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        dobleclick();
        cargartabla("");
        this.setResizable(false);
        this.setLocationRelativeTo(null);

        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

    }

    void dobleclick() {
        tablaobrasocial.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    btnaceptar.doClick();
                }
            }
        });

    }

    //////////////////////FUNCION CARGAR TABLA //////////////////////
    void cargartabla(String valor) {
        String[] Titulo = {"id", "Razon Social/Nombre", "CP", "CUIT", "Dirección", "Condición", "WSFE", "Localidad", "Provincia"};
        String[] Registros = new String[9];
        String sql = "SELECT\n"
                + "idObrasocialDato,\n"
                + "razonSocial,\n"
                + "CP,\n"
                + "cuit,\n"
                + "direccion,\n"
                + "descripcion,\n"
                + "wsfe,\n"
                + "nombre_localidad,\n"
                + "nombre_provincia\n"
                + "FROM\n"
                + "vista_obrasociales_datos";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMariaDB maria = new ConexionMariaDB();
        Connection cn = maria.Conectar();
        Statement SelectColegiado = null;
        try {
            SelectColegiado = cn.createStatement();
            ResultSet rs = SelectColegiado.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                model.addRow(Registros);
            }
            tablaobrasocial.getTableHeader().setFont(new Font("Thaoma", 1, 12));
            tablaobrasocial.setModel(model);
            tablaobrasocial.setAutoCreateRowSorter(true);
            fnAlinear alinear = new fnAlinear();
            tablaobrasocial.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
            tablaobrasocial.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablaobrasocial.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
            tablaobrasocial.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearIzquierda());
            tablaobrasocial.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
            tablaobrasocial.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaobrasocial.getColumnModel().getColumn(0).setMinWidth(0);
            tablaobrasocial.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablaobrasocial.getColumnModel().getColumn(6).setMaxWidth(0);
            tablaobrasocial.getColumnModel().getColumn(6).setMinWidth(0);
            tablaobrasocial.getColumnModel().getColumn(6).setPreferredWidth(0);

            tablaobrasocial.getColumnModel().getColumn(1).setPreferredWidth(200);
            tablaobrasocial.getColumnModel().getColumn(2).setPreferredWidth(50);
            tablaobrasocial.getColumnModel().getColumn(3).setPreferredWidth(100);
            tablaobrasocial.getColumnModel().getColumn(4).setPreferredWidth(120);
            tablaobrasocial.getColumnModel().getColumn(5).setPreferredWidth(150);
            tablaobrasocial.getColumnModel().getColumn(7).setPreferredWidth(150);
            tablaobrasocial.getColumnModel().getColumn(8).setPreferredWidth(150);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectColegiado != null) {
                    SelectColegiado.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaobrasocial = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        btnaceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Elegir colegiado");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaobrasocial.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaobrasocial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tablaobrasocial.setNextFocusableComponent(txtbuscar);
        tablaobrasocial.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tablaobrasocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaobrasocialKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaobrasocial);

        txtbuscar.setNextFocusableComponent(tablaobrasocial);
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728934 - enter login right.png"))); // NOI18N
        btnaceptar.setMnemonic('a');
        btnaceptar.setText("Aceptar");
        btnaceptar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnaceptar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnaceptar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        int filasel;
        filasel = tablaobrasocial.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            if (tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 5).toString().equals("") || tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 2).toString().equals("")) {
                JOptionPane.showMessageDialog(null, "A la obra social le faltan datos para facturar");
            } else {
                id_obrasocial_cliente = Integer.valueOf(tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 0).toString());
                nombre_obrasocial = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 1).toString();
                cuit_obrasocial = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 3).toString();
                direccion_obrasocial = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 4).toString();
                localidad = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 7).toString();
                provincia = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 8).toString();
                wsfe = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 6).toString();
                condicion_iva = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 5).toString();
                cp = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 2).toString();
                this.dispose();
            }
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed

    }//GEN-LAST:event_txtbuscarActionPerformed

    private void tablaobrasocialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaobrasocialKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int filasel;
            filasel = tablaobrasocial.getSelectedRow();
            if (filasel == -1) {
                JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
            } else {
                if (tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 5).toString().equals("") || tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 2).toString().equals("")) {
                    JOptionPane.showMessageDialog(null, "A la obra social le faltan datos para facturar");
                } else {
                    id_obrasocial_cliente = Integer.valueOf(tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 0).toString());
                    nombre_obrasocial = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 1).toString();
                    cuit_obrasocial = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 3).toString();
                    direccion_obrasocial = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 4).toString();
                    localidad = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 7).toString();
                    provincia = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 8).toString();
                    wsfe = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 6).toString();
                    condicion_iva = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 5).toString();
                    cp = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 2).toString();
                    this.dispose();
                }
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            tablaobrasocial.transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablaobrasocialKeyPressed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        if (!txtbuscar.getText().equals("")) {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
            tablaobrasocial.setRowSorter(sorter);
        } else {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*.*"));
            tablaobrasocial.setRowSorter(sorter);
        }
    }//GEN-LAST:event_txtbuscarKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaobrasocial;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
