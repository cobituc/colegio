package vista;


import controlador.ConexionMariaDB;
import java.sql.Connection;
import javax.swing.JOptionPane;

public class Detalle_nbu extends javax.swing.JDialog {

    public static String codigo_fac_practicas_obrasocial = "", unidaddebioquimica_practica = "", preciofijo = "", preciototal = "";

    public Detalle_nbu(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        txtcod_fac.setText(codigo_fac_practicas_obrasocial);
        txtunidad_bioquimica.setText(unidaddebioquimica_practica);
        txtprecio_fijo.setText(preciofijo);
        txtpreciototal.setText(preciototal);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtcod_fac = new javax.swing.JTextField();
        txtprecio_fijo = new javax.swing.JTextField();
        txtunidad_bioquimica = new javax.swing.JTextField();
        txtpreciototal = new javax.swing.JTextField();
        modificar = new javax.swing.JButton();
        cancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Codigo Facturacion de Practicas de Obra Social:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Unidad Bioquimica:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Precio Fijo:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Precio Total:");

        txtcod_fac.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcod_fac.setForeground(new java.awt.Color(0, 102, 204));

        txtprecio_fijo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtprecio_fijo.setForeground(new java.awt.Color(0, 102, 204));

        txtunidad_bioquimica.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtunidad_bioquimica.setForeground(new java.awt.Color(0, 102, 204));

        txtpreciototal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtpreciototal.setForeground(new java.awt.Color(0, 102, 204));

        modificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        modificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        modificar.setText("Modificar");
        modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarActionPerformed(evt);
            }
        });

        cancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        cancelar.setText("Cancelar");
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(modificar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cancelar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtpreciototal, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(txtprecio_fijo, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(txtunidad_bioquimica, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(txtcod_fac))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtcod_fac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtunidad_bioquimica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtprecio_fijo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtpreciototal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelar)
                    .addComponent(modificar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelarActionPerformed

    private void modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarActionPerformed
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String sSQL = "";
        if (!"".equals(txtcod_fac.getText()) && !"".equals(txtunidad_bioquimica.getText()) && !"".equals(txtprecio_fijo.getText()) && !"".equals(txtpreciototal.getText())) {
            
            JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            /*try {
             sSQL = "UPDATE usuarios SET apellido_usuario=?,"
             + " nombre_usuario=?, usuario_usuario=?,"
             + " contraseña_usuario=?, colegiado_usuario=?,"
             + " obrasocial_usuario=?, opciones_usuario=?,"
             + " nbu_usuario=?, validador_usuario=?"
             + " WHERE id_usuario=" + idusuarios;
             PreparedStatement pst = cn.prepareStatement(sSQL);
             pst.setString(1, txtcod_fac.getText());
             pst.setString(2, txtunidad_bioquimica.getText());
             pst.setString(3, txtprecio_fijo.getText());
             pst.setString(4, txtpreciototal.getText());
             int n = pst.executeUpdate();
             if (n > 0) {
             JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
             }
             //cargartabla("");
             } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null, "Error en la Base de Datos");
             }*/
        } else {
            JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios");
        }
    }//GEN-LAST:event_modificarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JButton modificar;
    private javax.swing.JTextField txtcod_fac;
    private javax.swing.JTextField txtprecio_fijo;
    private javax.swing.JTextField txtpreciototal;
    private javax.swing.JTextField txtunidad_bioquimica;
    // End of variables declaration//GEN-END:variables
}
