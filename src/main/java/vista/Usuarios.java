package vista;

import controlador.ConexionMariaDB;
import controlador.Funciones;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.table.DefaultTableModel;

public class Usuarios extends javax.swing.JDialog {

    DefaultTableModel model;
    int mod = 0, idusuarios;
    int elim = 0;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;

    public Usuarios(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        dobleclick();
        this.setLocationRelativeTo(null);
        cargartabla("");
        Funciones.funcionescape(this);
        this.setResizable(false);
    }

    void dobleclick() {
        tablausuarios.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel;
                    filasel = tablausuarios.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        mod = 1;
                        cargardatos(tablausuarios.getValueAt(tablausuarios.getSelectedRow(), 0).toString());
                        btncancelar.setText("Cancelar");
                    }
                }
            }
        });
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void cargartabla(String valor) {
        String[] Titulo = {"N°", "Apellido", "Nombre", "Usuario"};
        String[] Registros = new String[4];
        String sql = "SELECT id_usuario, apellido_usuario, nombre_usuario, usuario_usuario "
                + "FROM usuarios WHERE apellido_usuario LIKE '%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString("id_usuario");
                Registros[1] = rs.getString("apellido_usuario");
                Registros[2] = rs.getString("nombre_usuario");
                Registros[3] = rs.getString("usuario_usuario");
                model.addRow(Registros);
            }
            tablausuarios.setModel(model);
            tablausuarios.setAutoCreateRowSorter(true);
            /////////////////////////////////////////////////////////////
            tablausuarios.getColumnModel().getColumn(0).setMaxWidth(0);
            tablausuarios.getColumnModel().getColumn(0).setMinWidth(0);
            tablausuarios.getColumnModel().getColumn(0).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            alinear();
            tablausuarios.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
            tablausuarios.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablausuarios.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
            tablausuarios.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
            //////////////////////////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        }
    }

    void cargardatos(String id) {
        String sSQL = "";
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        sSQL = "SELECT * FROM usuarios";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                if (id.equals(rs.getString("id_usuario"))) {
                    idusuarios = rs.getInt("id_usuario");
                    txtapellido.setText(rs.getString("apellido_usuario"));
                    txtnombre.setText(rs.getString("nombre_usuario"));
                    txtusuario.setText(rs.getString("usuario_usuario"));
                    txtcontraseña.setText(rs.getString("contraseña_usuario"));
                    jCheckBoxcolegiado.setSelected(rs.getBoolean("colegiado_usuario"));
                    jCheckBoxobrasocial.setSelected(rs.getBoolean("obrasocial_usuario"));
                    jCheckBoxopciones.setSelected(rs.getBoolean("opciones_usuario"));
                    jCheckBoxnbu.setSelected(rs.getBoolean("nbu_usuario"));
                    jCheckBoxvalidador.setSelected(rs.getBoolean("validador_usuario"));
                }
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        }
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        Modificar = new javax.swing.JMenuItem();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        txtusuario = new javax.swing.JTextField();
        txtcontraseña = new javax.swing.JTextField();
        btnaplicar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablausuarios = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        btnaceptar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jCheckBoxcolegiado = new javax.swing.JCheckBox();
        jCheckBoxobrasocial = new javax.swing.JCheckBox();
        jCheckBoxnbu = new javax.swing.JCheckBox();
        jCheckBoxopciones = new javax.swing.JCheckBox();
        jCheckBoxvalidador = new javax.swing.JCheckBox();
        btncancelar = new javax.swing.JButton();

        Modificar.setText("Modificar");
        Modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Modificar);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Apellidos:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Nombres:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Nombre de Usuario:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Contraseña:");

        txtapellido.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtapellido.setForeground(new java.awt.Color(0, 102, 204));
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtapellidoKeyReleased(evt);
            }
        });

        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));

        txtusuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtusuario.setForeground(new java.awt.Color(0, 102, 204));

        txtcontraseña.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcontraseña.setForeground(new java.awt.Color(0, 102, 204));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtusuario)
                    .addComponent(txtcontraseña, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtapellido, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtnombre, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtcontraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 16, Short.MAX_VALUE))
        );

        btnaplicar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaplicar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728966 - edit pen pencil writing.png"))); // NOI18N
        btnaplicar.setMnemonic('p');
        btnaplicar.setText("Aplicar");
        btnaplicar.setMaximumSize(new java.awt.Dimension(75, 23));
        btnaplicar.setMinimumSize(new java.awt.Dimension(75, 23));
        btnaplicar.setPreferredSize(new java.awt.Dimension(75, 23));
        btnaplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaplicarActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablausuarios.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablausuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {}
            },
            new String [] {

            }
        ));
        tablausuarios.setComponentPopupMenu(jPopupMenu1);
        tablausuarios.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tablausuarios.setOpaque(false);
        tablausuarios.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablausuariosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablausuarios);

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(25, 25, 25))
        );

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728934 - enter login right.png"))); // NOI18N
        btnaceptar.setMnemonic('a');
        btnaceptar.setText("Aceptar");
        btnaceptar.setMaximumSize(new java.awt.Dimension(75, 23));
        btnaceptar.setMinimumSize(new java.awt.Dimension(75, 23));
        btnaceptar.setPreferredSize(new java.awt.Dimension(75, 23));
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccionar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jCheckBoxcolegiado.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBoxcolegiado.setText("Colegiados");

        jCheckBoxobrasocial.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBoxobrasocial.setText("Obra Social");

        jCheckBoxnbu.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBoxnbu.setText("NBU");

        jCheckBoxopciones.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBoxopciones.setText("Opciones");

        jCheckBoxvalidador.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBoxvalidador.setText("Validador");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jCheckBoxnbu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jCheckBoxcolegiado, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckBoxopciones, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCheckBoxobrasocial)))
                    .addComponent(jCheckBoxvalidador, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(75, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxcolegiado)
                    .addComponent(jCheckBoxobrasocial))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxnbu)
                    .addComponent(jCheckBoxopciones))
                .addGap(18, 18, 18)
                .addComponent(jCheckBoxvalidador)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btncancelar.setMnemonic('v');
        btncancelar.setText("Volver");
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                        .addComponent(btnaplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnaceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnaplicar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btncancelar, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtapellidoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyReleased
        cargartabla(txtapellido.getText());
    }//GEN-LAST:event_txtapellidoKeyReleased

    private void btnaplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaplicarActionPerformed
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String apellido, nombre, usuario, contraseña;
        boolean colegiado, obrasocial, opciones, nbu, validador;
        String sSQL = "";
        apellido = txtapellido.getText();
        nombre = txtnombre.getText();
        usuario = txtusuario.getText();
        contraseña = txtcontraseña.getText();
        colegiado = jCheckBoxcolegiado.isSelected();
        obrasocial = jCheckBoxobrasocial.isSelected();
        opciones = jCheckBoxnbu.isSelected();
        nbu = jCheckBoxnbu.isSelected();
        validador = jCheckBoxvalidador.isSelected();
        if (!"".equals(apellido) && !"".equals(nombre) && !"".equals(usuario) && !"".equals(contraseña)) {
            if (mod == 0) {
                ///////////////////agregar datos//////////////////////////////
                try {
                    String sql = "SELECT apellido_usuario, usuario_usuario FROM usuarios";
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);
                    int contador = 0; //inicio
                    while (rs.next()) {
                        contador++;
                    }
                    rs.beforeFirst();// fin
                    if (contador != 0) {
                        while (rs.next()) {

                            if (apellido.equals(rs.getString("apellido_usuario"))) {
                                if (usuario.equals(rs.getString("usuario_usuario"))) {
                                    JOptionPane.showMessageDialog(null, "El usuario ya esta en la base de datos");
                                    break;
                                }
                            }
                        }
                    }
                    rs.beforeFirst();
                    sSQL = "INSERT INTO usuarios (nombre_usuario, apellido_usuario, usuario_usuario, "
                            + "contraseña_usuario,  colegiado_usuario, obrasocial_usuario, opciones_usuario, "
                            + "nbu_usuario, validador_usuario) VALUES(?,?,?,?,?,?,?,?,?)";
                    PreparedStatement pst = cn.prepareStatement(sSQL);
                    pst.setString(1, nombre);
                    pst.setString(2, apellido);
                    pst.setString(3, usuario);
                    pst.setString(4, contraseña);
                    pst.setBoolean(5, colegiado);
                    pst.setBoolean(6, obrasocial);
                    pst.setBoolean(7, opciones);
                    pst.setBoolean(8, nbu);
                    pst.setBoolean(9, validador);
                    int n = pst.executeUpdate();
                    if (n > 0) {
                        JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                    }
                    txtapellido.setText("");
                    txtnombre.setText("");
                    txtusuario.setText("");
                    txtcontraseña.setText("");
                    jCheckBoxcolegiado.setSelected(false);
                    jCheckBoxobrasocial.setSelected(false);
                    jCheckBoxopciones.setSelected(false);
                    jCheckBoxnbu.setSelected(false);
                    jCheckBoxvalidador.setSelected(false);
                    cargartabla("");
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                }
            } ////////////////////////modifica datos/////////////////////////////////
            else {
                try {
                    sSQL = "UPDATE usuarios SET apellido_usuario=?,"
                            + " nombre_usuario=?, usuario_usuario=?,"
                            + " contraseña_usuario=?, colegiado_usuario=?,"
                            + " obrasocial_usuario=?, opciones_usuario=?,"
                            + " nbu_usuario=?, validador_usuario=?"
                            + " WHERE id_usuario=" + idusuarios;
                    PreparedStatement pst = cn.prepareStatement(sSQL);
                    pst.setString(1, apellido);
                    pst.setString(2, nombre);
                    pst.setString(3, usuario);
                    pst.setString(4, contraseña);
                    pst.setBoolean(5, colegiado);
                    pst.setBoolean(6, obrasocial);
                    pst.setBoolean(7, opciones);
                    pst.setBoolean(8, nbu);
                    pst.setBoolean(9, validador);
                    int n = pst.executeUpdate();
                    if (n > 0) {
                        JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                    }
                    txtapellido.setText("");
                    txtnombre.setText("");
                    txtusuario.setText("");
                    txtcontraseña.setText("");
                    jCheckBoxcolegiado.setSelected(false);
                    jCheckBoxobrasocial.setSelected(false);
                    jCheckBoxopciones.setSelected(false);
                    jCheckBoxnbu.setSelected(false);
                    jCheckBoxvalidador.setSelected(false);
                    mod = 0;
                    cargartabla("");
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la Base de Datos");
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios");
        }
    }//GEN-LAST:event_btnaplicarActionPerformed

    private void tablausuariosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablausuariosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            evt.consume();
            tablausuarios.transferFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int filasel;
            filasel = tablausuarios.getSelectedRow();
            if (filasel == -1) {
                JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
            } else {
                mod = 1;
                cargardatos(tablausuarios.getValueAt(tablausuarios.getSelectedRow(), 0).toString());
                btncancelar.setText("Cancelar");
            }
        }
    }//GEN-LAST:event_tablausuariosKeyPressed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        if (elim == 0) {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            String apellido, nombre, usuario, contraseña;
            boolean colegiado, obrasocial, opciones, nbu, validador;
            String sSQL = "";
            apellido = txtapellido.getText();
            nombre = txtnombre.getText();
            usuario = txtusuario.getText();
            contraseña = txtcontraseña.getText();
            colegiado = jCheckBoxcolegiado.isSelected();
            obrasocial = jCheckBoxobrasocial.isSelected();
            opciones = jCheckBoxnbu.isSelected();
            nbu = jCheckBoxnbu.isSelected();
            validador = jCheckBoxvalidador.isSelected();
            if (!"".equals(apellido) && !"".equals(nombre) && !"".equals(usuario) && !"".equals(contraseña)) {
                if (mod == 0) {
                    ///////////////////cargar datos//////////////////////////////
                    try {
                        String sql = "SELECT apellido_usuario, usuario_usuario FROM usuarios";
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sql);
                        int contador = 0; //inicio
                        while (rs.next()) {
                            contador++;
                        }
                        rs.beforeFirst();// fin
                        if (contador != 0) {
                            while (rs.next()) {

                                if (apellido.equals(rs.getString("apellido_usuario"))) {
                                    if (usuario.equals(rs.getString("usuario_usuario"))) {
                                        JOptionPane.showMessageDialog(null, "El usuario ya esta en la base de datos");
                                        break;
                                    }
                                }
                            }
                        }
                        rs.beforeFirst();
                        sSQL = "INSERT INTO usuarios (nombre_usuario, apellido_usuario, usuario_usuario, "
                                + "contraseña_usuario,  colegiado_usuario, obrasocial_usuario, opciones_usuario, "
                                + "nbu_usuario, validador_usuario) VALUES(?,?,?,?,?,?,?,?,?)";
                        PreparedStatement pst = cn.prepareStatement(sSQL);
                        pst.setString(1, nombre);
                        pst.setString(2, apellido);
                        pst.setString(3, usuario);
                        pst.setString(4, contraseña);
                        pst.setBoolean(5, colegiado);
                        pst.setBoolean(6, obrasocial);
                        pst.setBoolean(7, opciones);
                        pst.setBoolean(8, nbu);
                        pst.setBoolean(9, validador);
                        int n = pst.executeUpdate();
                        if (n > 0) {
                            JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                        }
                        txtapellido.setText("");
                        txtnombre.setText("");
                        txtusuario.setText("");
                        txtcontraseña.setText("");
                        jCheckBoxcolegiado.setSelected(false);
                        jCheckBoxobrasocial.setSelected(false);
                        jCheckBoxopciones.setSelected(false);
                        jCheckBoxnbu.setSelected(false);
                        jCheckBoxvalidador.setSelected(false);
                        this.dispose();
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                    }
                } ////////////////////////modifica datos/////////////////////////////////
                else {
                    try {
                        sSQL = "UPDATE usuarios SET apellido_usuario=?,"
                                + " nombre_usuario=?, usuario_usuario=?,"
                                + " contraseña_usuario=?, colegiado_usuario=?,"
                                + " obrasocial_usuario=?, opciones_usuario=?,"
                                + " nbu_usuario=?, validador_usuario=?"
                                + " WHERE id_usuario=" + idusuarios;
                        PreparedStatement pst = cn.prepareStatement(sSQL);
                        pst.setString(1, apellido);
                        pst.setString(2, nombre);
                        pst.setString(3, usuario);
                        pst.setString(4, contraseña);
                        pst.setBoolean(5, colegiado);
                        pst.setBoolean(6, obrasocial);
                        pst.setBoolean(7, opciones);
                        pst.setBoolean(8, nbu);
                        pst.setBoolean(9, validador);
                        int n = pst.executeUpdate();
                        if (n > 0) {
                            JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                        }
                        txtapellido.setText("");
                        txtnombre.setText("");
                        txtusuario.setText("");
                        txtcontraseña.setText("");
                        jCheckBoxcolegiado.setSelected(false);
                        jCheckBoxobrasocial.setSelected(false);
                        jCheckBoxopciones.setSelected(false);
                        jCheckBoxnbu.setSelected(false);
                        jCheckBoxvalidador.setSelected(false);
                        mod = 0;
                        this.dispose();
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Error en la Base de Datos");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios");
            }
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        if (mod == 1) {
            txtapellido.setText("");
            txtnombre.setText("");
            txtusuario.setText("");
            txtcontraseña.setText("");
            jCheckBoxcolegiado.setSelected(false);
            jCheckBoxobrasocial.setSelected(false);
            jCheckBoxnbu.setSelected(false);
            mod = 0;
            btncancelar.setText("Salir");
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_btncancelarActionPerformed

    private void ModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificarActionPerformed
        int filasel;
        filasel = tablausuarios.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            mod = 1;
            cargardatos(tablausuarios.getValueAt(tablausuarios.getSelectedRow(), 0).toString());
            btncancelar.setText("Cancelar");
        }
    }//GEN-LAST:event_ModificarActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Modificar;
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btnaplicar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JCheckBox jCheckBoxcolegiado;
    private javax.swing.JCheckBox jCheckBoxnbu;
    private javax.swing.JCheckBox jCheckBoxobrasocial;
    private javax.swing.JCheckBox jCheckBoxopciones;
    private javax.swing.JCheckBox jCheckBoxvalidador;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablausuarios;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtcontraseña;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txtusuario;
    // End of variables declaration//GEN-END:variables
}
