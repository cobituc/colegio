package vista;

import com.mxrck.autocompleter.TextAutoCompleter;
import javax.swing.JOptionPane;
import static controlador.HiloInicio.listaColegiados;
import static controlador.HiloInicio.listaFormaDePago;
import static controlador.HiloInicio.listaBancos;
import static controlador.HiloInicio.listaDetalleFormaDePagoCredito;
import static controlador.HiloInicio.listaDetalleFormaDePagoDebito;
import static controlador.HiloInicio.listaDetalleFormaDePagoCuentaCorriente;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import javax.swing.table.DefaultTableModel;
import modelo.Banco;
import modelo.FormaDePago;
import modelo.FormaDePagoCredito;
import modelo.FormaDePagoCuentaCorriente;
import modelo.FormaDePagoDebito;

public class IngresoValorizacion extends javax.swing.JDialog {

    TextAutoCompleter textAutoAcompleterCuenta;
    TextAutoCompleter textAutoAcompleterAsociado;
    int x, y, idBanco, idFormaDePago, idCuentaCorriente, idTarjeta = 0, idCheque = 0;
    public static String[][] datosPagos;
    String DetallePago;
    double importe;
    public static int tipo=0;
    public static double importeTotal = 0;

    public IngresoValorizacion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        txtTotal.setText(String.valueOf("Importe Total: $" + String.valueOf(importeTotal)));
        cargarFormaPago();
        tamañoTabla();
        if(tipo==1){
            btnAceptar.setText("Realizar Ingreso");
            btnAceptar.setForegroundIcon(new Color(15,157,88));
        }else{
            btnAceptar.setText("Realizar Egreso");        
            btnAceptar.setForegroundIcon(new Color(219,68,55));
        }
        
    }

    void tamañoTabla() {
        tablaIngreso.getColumnModel().getColumn(3).setMaxWidth(0);
        tablaIngreso.getColumnModel().getColumn(3).setMinWidth(0);
        tablaIngreso.getColumnModel().getColumn(3).setPreferredWidth(0);
        //////////////////////////////////////////////////////////////////
        tablaIngreso.getColumnModel().getColumn(4).setMaxWidth(0);
        tablaIngreso.getColumnModel().getColumn(4).setMinWidth(0);
        tablaIngreso.getColumnModel().getColumn(4).setPreferredWidth(0);
        //////////////////////////////////////////////////////////////////////
        tablaIngreso.getColumnModel().getColumn(5).setMaxWidth(0);
        tablaIngreso.getColumnModel().getColumn(5).setMinWidth(0);
        tablaIngreso.getColumnModel().getColumn(5).setPreferredWidth(0);
        //////////////////////////////////////////////////////////////////
        tablaIngreso.getColumnModel().getColumn(6).setMaxWidth(0);
        tablaIngreso.getColumnModel().getColumn(6).setMinWidth(0);
        tablaIngreso.getColumnModel().getColumn(6).setPreferredWidth(0);
        ////////////////////////////////////////////////////////////////
    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    void cargatotales() {
        double totalIngresos = 0.00, sumatoriaIngresos = 0.00;
        int contador = 0;
        DecimalFormat df = new DecimalFormat("0.00");
        int totalRowIngresos = tablaIngreso.getRowCount();

        totalRowIngresos -= 1;

        if (totalRowIngresos != -1) {
            for (int i = 0; i <= (totalRowIngresos); i++) {
                /////////////////////////////
                String x = tablaIngreso.getValueAt(i, 0).toString();
                sumatoriaIngresos = Double.valueOf(x);
                ////////////////////////////////////////////////////
                totalIngresos = Redondear(totalIngresos + sumatoriaIngresos);
                contador++;
            }
        }

        txtTotalIngresos.setText((String.valueOf((totalIngresos))));

    }

    void cargarFormaPago() {
        cboFormaPago.removeAllItems();
        try {
            for (int i = 0; i < listaFormaDePago.size(); i++) {
                cboFormaPago.addItem(listaFormaDePago.get(i).toString());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar forma de pago. e(92)");
        }
    }

    void cargarFormaPagoCredito() {
        cboDetallePago.removeAllItems();
        try {
            for (int i = 0; i < listaDetalleFormaDePagoCredito.size(); i++) {
                cboDetallePago.addItem(listaDetalleFormaDePagoCredito.get(i).toString());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar forma de pago. e(103)");
        }
    }

    void cargarFormaPagoDebito() {
        cboDetallePago.removeAllItems();
        try {
            for (int i = 0; i < listaDetalleFormaDePagoDebito.size(); i++) {
                cboDetallePago.addItem(listaDetalleFormaDePagoDebito.get(i).toString());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar forma de pago. e(114)");
        }
    }

    void cargarFormaPagoTransferencia() {
        cboDetallePago.removeAllItems();
        try {
            for (int i = 0; i < listaBancos.size(); i++) {
                cboDetallePago.addItem(listaBancos.get(i).toString());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar forma de pago. e(125)");
        }
    }

    void cargarFormaPagoCuentaCorriente() {
        cboDetallePago.removeAllItems();
        try {
            for (int i = 0; i < listaDetalleFormaDePagoCuentaCorriente.size(); i++) {
                cboDetallePago.addItem(listaDetalleFormaDePagoCuentaCorriente.get(i).toString());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar forma de pago. e(136)");
        }
    }

//    void cargarCuentas() {
//        try {
//            textAutoAcompleterCuenta.removeAllItems();
//            for (int i = 0; i < listaCuenta.size(); i++) {
//                textAutoAcompleterCuenta.addItem(listaCuenta.get(i).toString());
//            }
//            textAutoAcompleterCuenta.setMode(0);
//            textAutoAcompleterCuenta.setCaseSensitive(false);
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, "Error al cargar cuentas. e(155)");
//        }
//    }
    void cargarAsociadoColegiado() {
        try {
            textAutoAcompleterAsociado.removeAllItems();
            for (int i = 0; i < listaColegiados.size(); i++) {
                textAutoAcompleterAsociado.addItem(listaColegiados.get(i).toString());
            }
            textAutoAcompleterAsociado.setMode(0);
            textAutoAcompleterAsociado.setCaseSensitive(false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar colegiados. e(161)");
        }
    }

    void cargarDetalleFormaDePago() {

        if (cboFormaPago.getSelectedItem() != null) {
            if (cboFormaPago.getSelectedItem().toString().equals("CONTADO")) {
                cboDetallePago.removeAllItems();
                txtImporte.requestFocus();
            }
            if (cboFormaPago.getSelectedItem().toString().equals("CUENTA CORRIENTE")) {
                cargarFormaPagoCuentaCorriente();
                txtImporte.requestFocus();
            }
            if (cboFormaPago.getSelectedItem().toString().equals("TARJETA DE CREDITO")) {
                cargarFormaPagoCredito();
                txtImporte.requestFocus();
            }
            if (cboFormaPago.getSelectedItem().toString().equals("TARJETA DE DEBITO")) {
                cargarFormaPagoDebito();
                txtImporte.requestFocus();
            }
            if (cboFormaPago.getSelectedItem().toString().equals("CHEQUE")) {
                cboDetallePago.removeAllItems();
                new ChequesIngresar(null, true).setVisible(true);
                importe = ChequesIngresar.importe;
                idCheque = ChequesIngresar.idCheque;
                txtImporte.setText(String.valueOf(importe));
                DetallePago = String.valueOf(ChequesIngresar.numero);
                txtImporte.requestFocus();

            }
            if (cboFormaPago.getSelectedItem().toString().equals("TRANSFERENCIA")) {
                cargarFormaPagoTransferencia();
                txtImporte.requestFocus();
            }
        }
    }

    void blanquearDatos() {

        DetallePago = "";
        txtImporte.setText("");
        if (cboDetallePago.getItemCount() != 0) {
            cboDetallePago.setSelectedIndex(0);
        }
        cboFormaPago.setSelectedIndex(0);
        idBanco = 0;
        idFormaDePago = 0;
        idTarjeta = 0;
        idCuentaCorriente = 0;
        idCheque = 0;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        btnSalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnAceptar = new RSMaterialComponent.RSButtonMaterialIconUno();
        jLabel21 = new javax.swing.JLabel();
        cboFormaPago = new RSMaterialComponent.RSComboBox();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaIngreso = new RSMaterialComponent.RSTableMetroCustom();
        jLabel19 = new javax.swing.JLabel();
        txtTotalIngresos = new javax.swing.JLabel();
        cboDetallePago = new RSMaterialComponent.RSComboBox();
        jLabel18 = new javax.swing.JLabel();
        btnagregar = new RSMaterialComponent.RSButtonMaterialIconUno();
        txtImporte = new RSMaterialComponent.RSTextFieldOne();
        jLabel17 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel3MouseDragged(evt);
            }
        });
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel3MousePressed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(214, 45, 32));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar.setSizeIcon(20.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(285, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 7, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 383, Short.MAX_VALUE)
        );

        jPanel4.setBackground(new java.awt.Color(66, 133, 200));

        btnSalir.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir.setText("Salir");
        btnSalir.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnSalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnSalir.setRound(20);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnAceptar.setBackground(new java.awt.Color(255, 255, 255));
        btnAceptar.setForeground(new java.awt.Color(0, 0, 0));
        btnAceptar.setText("Realizar Ingreso");
        btnAceptar.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnAceptar.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnAceptar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnAceptar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnAceptar.setRound(20);
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel21.setText("Forma de Ingreso:");

        cboFormaPago.setForeground(new java.awt.Color(51, 51, 51));
        cboFormaPago.setColorArrow(new java.awt.Color(66, 133, 200));
        cboFormaPago.setColorBorde(new java.awt.Color(255, 255, 255));
        cboFormaPago.setColorBoton(new java.awt.Color(255, 255, 255));
        cboFormaPago.setColorFondo(new java.awt.Color(255, 255, 255));
        cboFormaPago.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboFormaPago.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboFormaPago.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboFormaPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFormaPagoActionPerformed(evt);
            }
        });
        cboFormaPago.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cboFormaPagoKeyPressed(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(66, 133, 200));

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tablaIngreso.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaIngreso.setForeground(new java.awt.Color(255, 255, 255));
        tablaIngreso.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Importe", "Forma de Pago", "Detalle", "idFormaDePago", "idCheque", "idTarjeta", "idBanco"
            }
        ));
        tablaIngreso.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaIngreso.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaIngreso.setBorderHead(null);
        tablaIngreso.setBorderRows(null);
        tablaIngreso.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaIngreso.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaIngreso.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaIngreso.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaIngreso.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaIngreso.setGridColor(new java.awt.Color(15, 157, 88));
        tablaIngreso.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaIngreso.setShowHorizontalLines(true);
        tablaIngreso.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaIngresoMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tablaIngreso);

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Importe:");

        txtTotalIngresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTotalIngresos.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalIngresos.setText("0.0");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTotalIngresos, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
            .addComponent(jScrollPane5)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtTotalIngresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        cboDetallePago.setForeground(new java.awt.Color(51, 51, 51));
        cboDetallePago.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "...", "Banco HSBC", "Banco Galicia", "Banco Santander", "Banco Santiago" }));
        cboDetallePago.setColorArrow(new java.awt.Color(66, 133, 200));
        cboDetallePago.setColorBorde(new java.awt.Color(255, 255, 255));
        cboDetallePago.setColorBoton(new java.awt.Color(255, 255, 255));
        cboDetallePago.setColorFondo(new java.awt.Color(255, 255, 255));
        cboDetallePago.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboDetallePago.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboDetallePago.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboDetallePago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboDetallePagoActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Detalle:");

        btnagregar.setBackground(new java.awt.Color(255, 255, 255));
        btnagregar.setForeground(new java.awt.Color(0, 0, 0));
        btnagregar.setText("Agregar");
        btnagregar.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnagregar.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnagregar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnagregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnagregar.setRound(20);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        txtImporte.setForeground(new java.awt.Color(51, 51, 51));
        txtImporte.setBorderColor(new java.awt.Color(255, 255, 255));
        txtImporte.setPhColor(new java.awt.Color(51, 51, 51));
        txtImporte.setPlaceholder("$0.00");
        txtImporte.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtImporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImporteActionPerformed(evt);
            }
        });
        txtImporte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtImporteKeyPressed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Importe:");

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtTotal.setForeground(new java.awt.Color(244, 180, 0));
        txtTotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txtTotal.setText("Forma de Ingreso:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel21)
                                .addGap(18, 18, 18)
                                .addComponent(cboFormaPago, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtImporte, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cboDetallePago, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txtTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(188, 188, 188))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtImporte, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cboFormaPago, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel17)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboDetallePago, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 494, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed

        dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void jPanel3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);
    }//GEN-LAST:event_jPanel3MouseDragged

    private void jPanel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MousePressed

        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jPanel3MousePressed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void tablaIngresoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaIngresoMouseClicked

    }//GEN-LAST:event_tablaIngresoMouseClicked

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        int cantidad = tablaIngreso.getRowCount(), i = 0;
        if (cantidad != -1) {
            datosPagos = new String[tablaIngreso.getRowCount()][5];
            for (i = 0; i < cantidad; i++) {
                datosPagos[i][0] = tablaIngreso.getValueAt(i, 3).toString();//idforma de pago
                datosPagos[i][1] = tablaIngreso.getValueAt(i, 4).toString();//id cheque
                datosPagos[i][2] = tablaIngreso.getValueAt(i, 5).toString();//id tarjeta
                datosPagos[i][3] = tablaIngreso.getValueAt(i, 6).toString();//id banco
                datosPagos[i][4] = tablaIngreso.getValueAt(i, 0).toString();//importe                
            }
            this.dispose();
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void txtImporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImporteActionPerformed
        if (!txtImporte.getText().equals("")) {
            importe = Double.valueOf(txtImporte.getText());
            btnagregar.requestFocus();
        }

    }//GEN-LAST:event_txtImporteActionPerformed

    private void cboDetallePagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboDetallePagoActionPerformed
        if (cboDetallePago.getItemCount() != 0) {
            if (cboFormaPago.getSelectedItem() != null) {
                if (cboFormaPago.getSelectedItem().toString().equals("CONTADO")) {
                    cboDetallePago.removeAllItems();
                    DetallePago = "";
                }
                if (cboFormaPago.getSelectedItem().toString().equals("CUENTA CORRIENTE")) {
                    FormaDePagoCuentaCorriente cc = FormaDePagoCuentaCorriente.buscarDetalleFormaDePago(cboDetallePago.getSelectedItem().toString(), listaDetalleFormaDePagoCuentaCorriente);
                    System.out.println("Detalle forma de pago " + cc);
                    if (cc != null) {
                        idCuentaCorriente = cc.getIdFormaDePago();
                        DetallePago = cboDetallePago.getSelectedItem().toString();
                        System.out.println("Detalle forma de pago idCuentaCorriente " + idCuentaCorriente);
                    }
                }
                if (cboFormaPago.getSelectedItem().toString().equals("TARJETA DE CREDITO")) {
                    FormaDePagoCredito credito = FormaDePagoCredito.buscarDetalleFormaDePago(cboDetallePago.getSelectedItem().toString(), listaDetalleFormaDePagoCredito);
                    System.out.println("Detalle forma de pago " + credito);
                    if (credito != null) {
                        idTarjeta = credito.getIdFormaDePago();
                        DetallePago = cboDetallePago.getSelectedItem().toString();
                        System.out.println("Detalle forma de pago idTarjeta " + idTarjeta);
                    }
                }
                if (cboFormaPago.getSelectedItem().toString().equals("TARJETA DE DEBITO")) {
                    FormaDePagoDebito debito = FormaDePagoDebito.buscarDetalleFormaDePago(cboDetallePago.getSelectedItem().toString(), listaDetalleFormaDePagoDebito);
                    System.out.println("Detalle forma de pago " + debito);
                    if (debito != null) {
                        idTarjeta = debito.getIdFormaDePago();
                        DetallePago = cboDetallePago.getSelectedItem().toString();
                        System.out.println("Detalle forma de pago idTarjeta " + idTarjeta);
                    }
                }
                if (cboFormaPago.getSelectedItem().toString().equals("CHEQUE")) {
                    cboDetallePago.removeAllItems();
                    DetallePago = "";
                }
                if (cboFormaPago.getSelectedItem().toString().equals("TRANSFERENCIA")) {
                    Banco banco = Banco.buscarBanco(cboDetallePago.getSelectedItem().toString(), listaBancos);
                    System.out.println("Detalle forma de pago transferecia por " + banco);
                    if (banco != null) {
                        idBanco = banco.getIdBanco();
                        DetallePago = cboDetallePago.getSelectedItem().toString();
                        System.out.println("Detalle forma de pago idBanco " + idBanco);
                    }
                }
            }

        }

        btnagregar.requestFocus();
    }//GEN-LAST:event_cboDetallePagoActionPerformed

    private void cboFormaPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboFormaPagoActionPerformed
        if (cboFormaPago.getItemCount() != 0) {
            FormaDePago forma = modelo.FormaDePago.buscarFormaDePago(cboFormaPago.getSelectedItem().toString(), listaFormaDePago);
            System.out.println("forma de pago " + forma);
            if (forma != null) {
                idFormaDePago = forma.getIdFormaDePago();
                cargarDetalleFormaDePago();
            }
        }
    }//GEN-LAST:event_cboFormaPagoActionPerformed


    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        DefaultTableModel temp = (DefaultTableModel) tablaIngreso.getModel();
        Object nuevo[] = {
            importe,
            cboFormaPago.getSelectedItem().toString(),
            DetallePago,
            idFormaDePago,
            idCheque,
            idTarjeta,
            idBanco
        };
        temp.addRow(nuevo);
        blanquearDatos();
        cargatotales();
    }//GEN-LAST:event_btnagregarActionPerformed

    private void cboFormaPagoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboFormaPagoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboFormaPagoKeyPressed

    private void txtImporteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtImporteKeyPressed
      
    }//GEN-LAST:event_txtImporteKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonMaterialIconUno btnAceptar;
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnSalir;
    private RSMaterialComponent.RSButtonMaterialIconUno btnagregar;
    private RSMaterialComponent.RSComboBox cboDetallePago;
    private RSMaterialComponent.RSComboBox cboFormaPago;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane5;
    private RSMaterialComponent.RSTableMetroCustom tablaIngreso;
    private RSMaterialComponent.RSTextFieldOne txtImporte;
    private javax.swing.JLabel txtTotal;
    private javax.swing.JLabel txtTotalIngresos;
    // End of variables declaration//GEN-END:variables
}
