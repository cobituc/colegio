package vista;

import controlador.ConexionMariaDB;
import controlador.Funciones;
import controlador.HiloInicio;
import static controlador.HiloInicio.listaColegiados;
import controlador.ImageTest;
import controlador.Numerico;
import controlador.UtilToDate;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.event.KeyEvent;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import modelo.Colegiado;
import modelo.Localidad;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.commons.io.IOUtils;

public class DatosColegiadoAgrega extends javax.swing.JDialog {

    String pic = "";
    int id_colegiado = 0;
    Funciones efechas = new Funciones();
    Image imagen2;
    private int banderaExaminar = 0;
    private modelo.ConexionMariaDB conexion;

    public DatosColegiadoAgrega(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);

        TextAutoCompleter textAutoAcompleter = new TextAutoCompleter(txtLocalidadNacimiento);
        textAutoAcompleter.addItems(HiloInicio.listaLocalidades.toArray());
        textAutoAcompleter.setMode(0);
        textAutoAcompleter.setCaseSensitive(false);

        TextAutoCompleter txtAlocalidad = new TextAutoCompleter(txtLocalidad);
        txtAlocalidad.addItems(HiloInicio.listaLocalidades.toArray());
        txtAlocalidad.setMode(0);
        txtAlocalidad.setCaseSensitive(false);

        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtLocalidadNacimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtNacionalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtLocalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtMail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toLowerCase(c));
                }
            }
        });

        txtUniversidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtFacultad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtEspecialidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGuardar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtnombre = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        fechanacimiento = new com.toedter.calendar.JDateChooser();
        jLabel55 = new javax.swing.JLabel();
        cbosexo = new javax.swing.JComboBox();
        jLabel53 = new javax.swing.JLabel();
        cbotipodoc = new javax.swing.JComboBox();
        jLabel54 = new javax.swing.JLabel();
        txtdocumento = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        txtCuil = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtTelCelular = new javax.swing.JTextField();
        txtMail = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtTelFijo = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtLocalidad = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtCodigoPostal = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtLocalidadNacimiento = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtNacionalidad = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txtNroDireccion = new javax.swing.JTextField();
        txtPisoDireccion = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtDeptoDireccion = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        cboEstado = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        cboTipoProf = new javax.swing.JComboBox();
        txtNumeroDeLibro = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtNumeroDeFolio = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtNumeroDeActa = new javax.swing.JTextField();
        txtMatricula = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        fechaAlta = new com.toedter.calendar.JDateChooser();
        fechaEgreso = new com.toedter.calendar.JDateChooser();
        fechaDoctorado = new com.toedter.calendar.JDateChooser();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtNumeroTitulo = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txtEspecialidad = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        btnExaminar = new javax.swing.JButton();
        btnCamara = new javax.swing.JButton();
        lblFoto = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtUniversidad = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtFacultad = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728991 - diskette save.png"))); // NOI18N
        btnGuardar.setMnemonic('s');
        btnGuardar.setText("Guardar datos");
        btnGuardar.setToolTipText("Alt + s");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnSalir.setMnemonic('v');
        btnSalir.setText("Salir");
        btnSalir.setToolTipText("Alt + v");
        btnSalir.setPreferredSize(new java.awt.Dimension(108, 23));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Datos personales"));

        txtnombre.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Apellido y Nombre:");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText("Fecha Nacimiento:");

        fechanacimiento.setForeground(new java.awt.Color(0, 102, 204));
        fechanacimiento.setDateFormatString("dd-MM-yyyy");
        fechanacimiento.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechanacimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechanacimientoKeyPressed(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel55.setText("Sexo:");

        cbosexo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbosexo.setForeground(new java.awt.Color(0, 102, 204));
        cbosexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Femenino", "Masculino" }));
        cbosexo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbosexoKeyPressed(evt);
            }
        });

        jLabel53.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel53.setText("Tipo:");

        cbotipodoc.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbotipodoc.setForeground(new java.awt.Color(0, 102, 204));
        cbotipodoc.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "DNI", "LC", "LE" }));
        cbotipodoc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbotipodocKeyPressed(evt);
            }
        });

        jLabel54.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel54.setText("Numero:");

        txtdocumento.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdocumento.setForeground(new java.awt.Color(0, 102, 204));
        txtdocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdocumentoKeyPressed(evt);
            }
        });

        jLabel44.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel44.setText("N° CUIL:");

        txtCuil.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtCuil.setForeground(new java.awt.Color(0, 102, 204));
        txtCuil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCuilKeyPressed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("Tel. Celular:");

        txtTelCelular.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtTelCelular.setForeground(new java.awt.Color(0, 102, 204));
        txtTelCelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTelCelularKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTelCelularKeyReleased(evt);
            }
        });

        txtMail.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtMail.setForeground(new java.awt.Color(0, 102, 204));
        txtMail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMailKeyPressed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Mail:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Dirección:");

        txtDireccion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtDireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDireccionKeyPressed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setText("Tel. Fijo:");

        txtTelFijo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtTelFijo.setForeground(new java.awt.Color(0, 102, 204));
        txtTelFijo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTelFijoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTelFijoKeyReleased(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("Localidad:");

        txtLocalidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtLocalidad.setForeground(new java.awt.Color(0, 102, 204));
        txtLocalidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLocalidadActionPerformed(evt);
            }
        });
        txtLocalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtLocalidadKeyPressed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setText("C. P.:");

        txtCodigoPostal.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtCodigoPostal.setForeground(new java.awt.Color(0, 102, 204));
        txtCodigoPostal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodigoPostalKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Lugar de nacimiento:");

        txtLocalidadNacimiento.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtLocalidadNacimiento.setForeground(new java.awt.Color(0, 102, 204));
        txtLocalidadNacimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtLocalidadNacimientoKeyPressed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Nacionalidad:");

        txtNacionalidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtNacionalidad.setForeground(new java.awt.Color(0, 102, 204));
        txtNacionalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNacionalidadKeyPressed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel23.setText("Número:");

        txtNroDireccion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtNroDireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtNroDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNroDireccionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNroDireccionKeyReleased(evt);
            }
        });

        txtPisoDireccion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtPisoDireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtPisoDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPisoDireccionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPisoDireccionKeyReleased(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel24.setText("Piso:");

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setText("Depto:");

        txtDeptoDireccion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtDeptoDireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtDeptoDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDeptoDireccionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDeptoDireccionKeyReleased(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Estado:");

        cboEstado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cboEstado.setForeground(new java.awt.Color(0, 102, 204));
        cboEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ACTIVO", "SUSPENDIDO", "VITALICIO", "BAJA", "FALLECIDO" }));
        cboEstado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cboEstadoKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnombre)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fechanacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTelCelular)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtLocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCodigoPostal, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMail, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtLocalidadNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel7)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jLabel53)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cbotipodoc, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jLabel54)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtdocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel55)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbosexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel44)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCuil))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cboEstado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNroDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPisoDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDeptoDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTelFijo, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(fechanacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel55)
                    .addComponent(cbosexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtLocalidadNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(cboEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel53)
                    .addComponent(cbotipodoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel54)
                    .addComponent(txtdocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel44)
                    .addComponent(txtCuil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel24)
                        .addComponent(txtPisoDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel25)
                        .addComponent(txtDeptoDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel16)
                        .addComponent(txtTelFijo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel23)
                        .addComponent(txtNroDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17)
                        .addComponent(txtTelCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(txtLocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel14)
                        .addComponent(txtCodigoPostal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Datos del título"));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Matricula:");

        jLabel43.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel43.setText("Tipo de Profesional:");

        cboTipoProf.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cboTipoProf.setForeground(new java.awt.Color(0, 102, 204));
        cboTipoProf.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Bioquímico", "Técnico" }));
        cboTipoProf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cboTipoProfKeyPressed(evt);
            }
        });

        txtNumeroDeLibro.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtNumeroDeLibro.setForeground(new java.awt.Color(0, 102, 204));
        txtNumeroDeLibro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNumeroDeLibroKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumeroDeLibroKeyReleased(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Libro:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Folio:");

        txtNumeroDeFolio.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtNumeroDeFolio.setForeground(new java.awt.Color(0, 102, 204));
        txtNumeroDeFolio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNumeroDeFolioKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumeroDeFolioKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Acta:");

        txtNumeroDeActa.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtNumeroDeActa.setForeground(new java.awt.Color(0, 102, 204));
        txtNumeroDeActa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNumeroDeActaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumeroDeActaKeyReleased(evt);
            }
        });

        txtMatricula.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtMatricula.setForeground(new java.awt.Color(0, 102, 204));
        txtMatricula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMatriculaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMatriculaKeyReleased(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel33.setText("Fecha de Alta:");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setText("Fec. de Egreso:");

        fechaAlta.setForeground(new java.awt.Color(0, 102, 204));
        fechaAlta.setDateFormatString("dd-MM-yyyy");
        fechaAlta.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechaAlta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechaAltaKeyPressed(evt);
            }
        });

        fechaEgreso.setForeground(new java.awt.Color(0, 102, 204));
        fechaEgreso.setDateFormatString("dd-MM-yyyy");
        fechaEgreso.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechaEgreso.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechaEgresoKeyPressed(evt);
            }
        });

        fechaDoctorado.setForeground(new java.awt.Color(0, 102, 204));
        fechaDoctorado.setDateFormatString("dd-MM-yyyy");
        fechaDoctorado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechaDoctorado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechaDoctoradoKeyPressed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel20.setText("Fec. de Doctorado:");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("N° de Titulo:");

        txtNumeroTitulo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtNumeroTitulo.setForeground(new java.awt.Color(0, 102, 204));
        txtNumeroTitulo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNumeroTituloKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumeroTituloKeyReleased(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setText("Especialidad:");

        txtEspecialidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtEspecialidad.setForeground(new java.awt.Color(0, 102, 204));
        txtEspecialidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtEspecialidadKeyPressed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Foto", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        btnExaminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnExaminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExaminarActionPerformed(evt);
            }
        });

        btnCamara.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728917 - camera photo photograph.png"))); // NOI18N
        btnCamara.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCamaraActionPerformed(evt);
            }
        });

        lblFoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblFoto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnExaminar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCamara, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblFoto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnExaminar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCamara, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Universidad:");

        txtUniversidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtUniversidad.setForeground(new java.awt.Color(0, 102, 204));
        txtUniversidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUniversidadKeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Facultad:");

        txtFacultad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtFacultad.setForeground(new java.awt.Color(0, 102, 204));
        txtFacultad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFacultadKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fechaDoctorado, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel19)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fechaEgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNumeroTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel43)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cboTipoProf, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel9)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtFacultad))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel8)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtUniversidad, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fechaAlta, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroDeFolio, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroDeLibro, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroDeActa, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMatricula, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel43)
                            .addComponent(cboTipoProf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(txtUniversidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(txtFacultad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(fechaEgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel22)
                                        .addComponent(txtEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(fechaDoctorado, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(fechaAlta, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel21)
                                .addComponent(txtNumeroTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(txtMatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(txtNumeroDeActa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(txtNumeroDeFolio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel6)
                                .addComponent(txtNumeroDeLibro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 22, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGuardar)
                .addGap(18, 18, 18)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    void getCuil() {
        if (txtdocumento.getText().length() == 8) {

            int d1, d2, d3, d4, d5, d6, d7, d8, sumaMasc, dni, resto, resto23, digitoVerificador, digitoGenero;
            dni = Integer.valueOf(txtdocumento.getText());
            d8 = dni % 10;
            dni = dni / 10;
            d7 = dni % 10;
            dni = dni / 10;
            d6 = dni % 10;
            dni = dni / 10;
            d5 = dni % 10;
            dni = dni / 10;
            d4 = dni % 10;
            dni = dni / 10;
            d3 = dni % 10;
            dni = dni / 10;
            d2 = dni % 10;
            dni = dni / 10;
            d1 = dni % 10;
            if (cbosexo.getSelectedItem().toString().equals("Masculino")) {
                sumaMasc = (d8 * 2) + (d7 * 3) + (d6 * 4) + (d5 * 5) + (d4 * 6) + (d3 * 7) + (d2 * 2) + (d1 * 3) + 10;
                resto = sumaMasc % 11;
            } else {
                sumaMasc = (d8 * 2) + (d7 * 3) + (d6 * 4) + (d5 * 5) + (d4 * 6) + (d3 * 7) + (d2 * 2) + (d1 * 3) + 10;
                resto = (sumaMasc + 28) % 11;
            }

            if (resto == 0) {
                digitoVerificador = 0;
            } else {
                digitoVerificador = 11 - resto;
            }
            if (resto == 1) {
                digitoGenero = 23;
                resto23 = (sumaMasc + 12) % 11;
                if (resto23 == 0) {
                    digitoVerificador = 0;
                } else {
                    digitoVerificador = 11 - resto23;
                }
            } else if (cbosexo.getSelectedItem().toString().equals("Masculino")) {
                digitoGenero = 20;
            } else {
                digitoGenero = 27;
            }
            txtCuil.setText(digitoGenero + txtdocumento.getText() + digitoVerificador);

        }
    }

    void cargarcolegiado() {
        String sSQL = "";
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        sSQL = "SELECT MAX(id_colegiados) AS id_colegiados FROM colegiados";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            if (rs.getInt("id_colegiados") != 0) {
                id_colegiado = rs.getInt("id_colegiados");
            } else {
                id_colegiado = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    private Image ConvertirImagen(byte[] bytes) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName("png");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        return reader.read(0, param);
    }

    void aplicarCambios() {

        try {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            String sSQL;
            FileInputStream fis = null;
            File file = new File(pic);
            fis = new FileInputStream(file);

            sSQL = "INSERT INTO colegiados(cuil_colegiado, matricula_colegiado, nombre_colegiado,"
                    + " ndeacta_colegiado, ndefolio_colegiado, ndelibro_colegiado, celular_particular, "
                    + "codigopostal_particular, direccion_particular, especialidad_particular, fechadedoctorado_particular,"
                    + " fechadeegreso_particular,"
                    + " fechadenacimiento_particular, localidad_particular, mail_particular, numerodedocumento_particular, "
                    + "numerodetitulo_particular, telefono_particular, tipodedocumento_particular, fecha_alta_colegiado, "
                    + "tipo_profesional, sexo, lugar_de_nacimiento, universidad, facultad, foto_particular, nacionalidad,estado_colegiado, numero_direccion, piso_direccion, departamento_direccion, estado_bioquimico) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, txtCuil.getText());
            if (cboTipoProf.getSelectedItem().toString().equals("Bioquímico")) {
                pst.setString(2, txtMatricula.getText());
            } else {
                pst.setString(2, "T" + txtMatricula.getText());
            }
            pst.setString(3, txtnombre.getText());
            pst.setString(4, txtNumeroDeActa.getText());
            pst.setString(5, txtNumeroDeFolio.getText());
            pst.setString(6, txtNumeroDeLibro.getText());
            pst.setString(7, txtTelCelular.getName());
            pst.setString(8, txtCodigoPostal.getText());
            pst.setString(9, txtDireccion.getText());
            pst.setString(10, txtEspecialidad.getText());
            if(fechaDoctorado!=null){
            pst.setString(11, efechas.getFecha(fechaDoctorado));
            }else{
                pst.setString(11, null);
            }
            if(fechaEgreso!=null){
            pst.setString(12, efechas.getFecha(fechaEgreso));
            }else{
                pst.setString(12, null);
            }
            if(fechanacimiento!=null){
            pst.setString(13, efechas.getFecha(fechanacimiento));
            }else{
                pst.setString(13, null);
            }
            pst.setString(14, txtLocalidad.getText());
            pst.setString(15, txtMail.getText());
            pst.setString(16, txtdocumento.getText());
            pst.setString(17, txtNumeroTitulo.getText());
            pst.setString(18, txtTelFijo.getText());
            pst.setString(19, cbotipodoc.getSelectedItem().toString());
            if(fechaAlta!=null){
            pst.setString(20, efechas.getFecha(fechaAlta));
            }else{
                pst.setString(20, null);
            }
            
            if (cboTipoProf.getSelectedItem().toString().equals("Bioquímico")) {
                pst.setString(21, "B");
            } else {
                pst.setString(21, "T");
            }
            if (cbosexo.getSelectedItem().toString().equals("Masculino")) {
                pst.setString(22, "M");
            } else {
                pst.setString(22, "F");
            }
            pst.setString(23, txtLocalidadNacimiento.getText());
            pst.setString(24, txtUniversidad.getText());
            pst.setString(25, txtFacultad.getText());
            if (!pic.equals("")) {
                pst.setBinaryStream(26, fis, (int) file.length());
            } else {
                pst.setString(26, null);
            }
            pst.setString(27, txtNacionalidad.getText());
            pst.setString(28, "ACTIVO");
            if(!txtNroDireccion.getText().equals("")){
            pst.setInt(29, Integer.valueOf(txtNroDireccion.getText()));
            }else{
                pst.setString(29, null);
            }
            pst.setString(30, txtPisoDireccion.getText());
            pst.setString(31, txtDeptoDireccion.getText());
            pst.setString(32, cboEstado.getSelectedItem().toString());
            int n = pst.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");

                int opcion = JOptionPane.showConfirmDialog(this, "Desea imprimir el carnet?", "Mensaje", JOptionPane.YES_NO_OPTION);
                if (opcion == 0) {

                    carnet();

                }
                this.dispose();
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    void guardarCambios() throws SQLException {

        conexion = new modelo.ConexionMariaDB();

        if (banderaExaminar == 1) {

            File file = null;
            FileInputStream fis = null;
            try {
                if (!pic.equals("")) {
                    file = new File(pic);

                    fis = new FileInputStream(file);

                } else {
                    file = new File("");
                    fis = new FileInputStream(file);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DatosColegiadoModifica.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (!pic.equals("")) {
                try {
                    byte[] bytes = IOUtils.toByteArray(fis);
                    Blob imagen = new javax.sql.rowset.serial.SerialBlob(bytes);

                    Colegiado col = new Colegiado(Long.valueOf(txtCuil.getText()),
                            txtMatricula.getText(),
                            txtnombre.getText(),
                            Integer.valueOf(txtNumeroDeActa.getText()),
                            Integer.valueOf(txtNumeroDeFolio.getText()),
                            Integer.valueOf(txtNumeroDeLibro.getText()),
                            Long.valueOf(txtTelCelular.getText()),
                            Integer.valueOf(txtCodigoPostal.getText()),
                            txtDireccion.getText(),
                            txtEspecialidad.getText(),
                            UtilToDate.convert(fechaDoctorado.getDate()),
                            UtilToDate.convert(fechaEgreso.getDate()),
                            UtilToDate.convert(fechanacimiento.getDate()),
                            txtLocalidad.getText(),
                            txtMail.getText(),
                            Long.valueOf(txtdocumento.getText()),
                            Integer.valueOf(txtNumeroTitulo.getText()),
                            Long.valueOf(txtTelFijo.getText()),
                            cbotipodoc.getSelectedItem().toString(),
                            UtilToDate.convert(fechaAlta.getDate()),
                            cboTipoProf.getSelectedItem().toString(),
                            cbosexo.getSelectedItem().toString(),
                            txtLocalidadNacimiento.getText(),
                            txtUniversidad.getText(),
                            txtFacultad.getText(),
                            imagen,
                            txtNacionalidad.getText());

                    conexion.EstablecerConexion();

                    if (Colegiado.ingresarColegiado(conexion.getConnection(), col)) {
                        listaColegiados.add(col);
                    }
                    conexion.cerrarConexion();
                } catch (IOException ex) {
                    Logger.getLogger(DatosColegiadoModifica.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else {

            Colegiado col = new Colegiado(Long.valueOf(txtCuil.getText()),
                    txtMatricula.getText(),
                    txtnombre.getText(),
                    Integer.valueOf(txtNumeroDeActa.getText()),
                    Integer.valueOf(txtNumeroDeFolio.getText()),
                    Integer.valueOf(txtNumeroDeLibro.getText()),
                    Long.valueOf(txtTelCelular.getText()),
                    Integer.valueOf(txtCodigoPostal.getText()),
                    txtDireccion.getText(),
                    txtEspecialidad.getText(),
                    UtilToDate.convert(fechaDoctorado.getDate()),
                    UtilToDate.convert(fechaEgreso.getDate()),
                    UtilToDate.convert(fechanacimiento.getDate()),
                    txtLocalidad.getText(),
                    txtMail.getText(),
                    Long.valueOf(txtdocumento.getText()),
                    Integer.valueOf(txtNumeroTitulo.getText()),
                    Long.valueOf(txtTelFijo.getText()),
                    cbotipodoc.getSelectedItem().toString(),
                    UtilToDate.convert(fechaAlta.getDate()),
                    cboTipoProf.getSelectedItem().toString(),
                    cbosexo.getSelectedItem().toString(),
                    txtLocalidadNacimiento.getText(),
                    txtUniversidad.getText(),
                    txtFacultad.getText(),
                    txtNacionalidad.getText());

            conexion.EstablecerConexion();

            if (Colegiado.ingresarColegiado(conexion.getConnection(), col)) {
                listaColegiados.add(col);
            }

            conexion.cerrarConexion();
        }

    }

    void carnet() {
        new Directivo(null, true).setVisible(true);
        try {

            Map parametros = new HashMap();
            if (cboTipoProf.getSelectedItem().toString().equals("Bioquímico")) {
                if (cbosexo.getSelectedItem().toString().equals("Masculino")) {
                    parametros.put("parametro", "BIOQUÍMICO");
                } else {
                    parametros.put("parametro", "BIOQUÍMICA");
                }
            } else {
                if (cbosexo.getSelectedItem().toString().equals("Masculino")) {
                    parametros.put("parametro", "TECNICO");
                } else {
                    parametros.put("parametro", "TECNICA");
                }
            }
            BufferedImage foto = ImageIO.read(new File(pic));
            parametros.put("Matricula", txtMatricula.getText());
            parametros.put("Apellido", txtnombre.getText());
            parametros.put("FechaNac", efechas.getfechaok(fechanacimiento));
            parametros.put("Dni", txtdocumento.getText());
            parametros.put("Foto", foto);
////////////////////////////////////Carnet 2
            parametros.put("Domicilio", txtDireccion.getText());
            parametros.put("Localidad", txtLocalidad.getText());
            parametros.put("Provincia", "TUCUMAN");
            parametros.put("FechaEgreso", efechas.getfechaok(fechaEgreso));
            parametros.put("FechaAlta", efechas.getfechaok(fechaAlta));
            parametros.put("nombreDirectivo", Directivo.nombreDirectivo);
            parametros.put("cargoDirectivo", Directivo.cargoDirectivo);

//            JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
//            JasperPrintManager.printReport(jPrint, false);
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Reporte", true);
            viewer.setSize(800, 600);
            viewer.setLocationRelativeTo(null);
            JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet.jasper"));

            JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
            JasperViewer jv = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(jv.getContentPane());
            viewer.setVisible(true);
       
            JDialog viewer1 = new JDialog(new javax.swing.JFrame(), "Reporte", true);
            viewer1.setSize(800, 600);
            viewer1.setLocationRelativeTo(null);

            JasperReport reporte_sobre2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet2.jasper"));
            JasperPrint jPrint2 = JasperFillManager.fillReport(reporte_sobre2, parametros, new JREmptyDataSource());

            JasperViewer jv1 = new JasperViewer(jPrint2, false);
            viewer1.getContentPane().add(jv1.getContentPane());
            viewer1.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
            }

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed

        aplicarCambios();

    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void cbosexoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbosexoKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtLocalidadNacimiento.requestFocus();
        }

    }//GEN-LAST:event_cbosexoKeyPressed

    private void cbotipodocKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbotipodocKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtdocumento.requestFocus();
        }

    }//GEN-LAST:event_cbotipodocKeyPressed

    private void txtdocumentoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdocumentoKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            getCuil();
            txtCuil.requestFocus();
        }

    }//GEN-LAST:event_txtdocumentoKeyPressed

    private void txtCuilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCuilKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtDireccion.requestFocus();
        }
    }//GEN-LAST:event_txtCuilKeyPressed

    private void cboTipoProfKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboTipoProfKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtUniversidad.requestFocus();
        }
    }//GEN-LAST:event_cboTipoProfKeyPressed

    private void txtNumeroDeLibroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroDeLibroKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtNumeroDeActa.requestFocus();
        }

    }//GEN-LAST:event_txtNumeroDeLibroKeyPressed

    private void txtNumeroDeLibroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroDeLibroKeyReleased
        if (!Numerico.isNumeric(txtNumeroDeLibro.getText())) {
            txtNumeroDeLibro.setText(null);
        }
    }//GEN-LAST:event_txtNumeroDeLibroKeyReleased

    private void txtNumeroDeFolioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroDeFolioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            fechaEgreso.requestFocusInWindow();
        }
    }//GEN-LAST:event_txtNumeroDeFolioKeyPressed

    private void txtNumeroDeFolioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroDeFolioKeyReleased
        if (!Numerico.isNumeric(txtNumeroDeFolio.getText())) {
            txtNumeroDeFolio.setText(null);
        }
    }//GEN-LAST:event_txtNumeroDeFolioKeyReleased

    private void txtNumeroDeActaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroDeActaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtMatricula.requestFocus();
        }
    }//GEN-LAST:event_txtNumeroDeActaKeyPressed

    private void txtNumeroDeActaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroDeActaKeyReleased
        if (!Numerico.isNumeric(txtNumeroDeActa.getText())) {
            txtNumeroDeActa.setText(null);
        }
    }//GEN-LAST:event_txtNumeroDeActaKeyReleased

    private void txtMatriculaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMatriculaKeyReleased

        if (!Numerico.isNumeric(txtMatricula.getText())) {
            txtMatricula.setText(null);
        }

    }//GEN-LAST:event_txtMatriculaKeyReleased

    private void fechaEgresoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechaEgresoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtNumeroTitulo.requestFocus();
        }
    }//GEN-LAST:event_fechaEgresoKeyPressed

    private void fechaDoctoradoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechaDoctoradoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {

            txtEspecialidad.requestFocus();

        }
    }//GEN-LAST:event_fechaDoctoradoKeyPressed

    private void txtNumeroTituloKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroTituloKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            fechaDoctorado.requestFocusInWindow();
        }
    }//GEN-LAST:event_txtNumeroTituloKeyPressed

    private void txtNumeroTituloKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroTituloKeyReleased
        if (!Numerico.isNumeric(txtNumeroTitulo.getText())) {
            txtNumeroTitulo.setText(null);
        }
    }//GEN-LAST:event_txtNumeroTituloKeyReleased

    private void txtLocalidadNacimientoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLocalidadNacimientoKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtNacionalidad.requestFocus();
        }

    }//GEN-LAST:event_txtLocalidadNacimientoKeyPressed

    private void txtNacionalidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNacionalidadKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            cbotipodoc.requestFocus();
        }

    }//GEN-LAST:event_txtNacionalidadKeyPressed

    private void btnExaminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExaminarActionPerformed
        File archivo;
        JFileChooser flcAbrirArchivo;
        flcAbrirArchivo = new JFileChooser();
        flcAbrirArchivo.setFileFilter(new FileNameExtensionFilter("Grafico de red portatiles", "png", "jpg", "jpeg"));
        int respuesta = flcAbrirArchivo.showOpenDialog(this);
        if (respuesta == JFileChooser.APPROVE_OPTION) {
            archivo = flcAbrirArchivo.getSelectedFile();
            pic = archivo.getAbsolutePath();
            Image foto = getToolkit().getImage(pic);
            foto = foto.getScaledInstance(140, 140, 1);
            lblFoto.setIcon(new ImageIcon(foto));
            BufferedImage originalImage = ImageTest.loadImage(pic);
            //redimensiona la imagen original
            BufferedImage resizedImage = ImageTest.resize(originalImage, 140, 140);
            File file = new File("C:\\Descargas-CBT\\bioq.png");
            //si la la imagen no existe la crea el archivo
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            //guarda la imagen dentro del archivo creado
            ImageTest.saveImage(resizedImage, file.getPath());
            pic = "C:\\Descargas-CBT\\bioq.png";
            banderaExaminar = 1;
        }
    }//GEN-LAST:event_btnExaminarActionPerformed

    private void btnCamaraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCamaraActionPerformed
        new TomarImagen(null, true).setVisible(true);

        pic = "C:\\Descargas-CBT\\camara.jpg";

        Image foto = getToolkit().getImage(pic);
        foto = foto.getScaledInstance(180, 180, 1);
        lblFoto.setIcon(new ImageIcon(foto));
        BufferedImage originalImage = ImageTest.loadImage(pic);
        //redimensiona la imagen original
        BufferedImage resizedImage = ImageTest.resize(originalImage, 180, 180);
        File file = new File("C:\\Descargas-CBT\\bioq.png");
        //si la la imagen no existe la crea el archivo
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        //guarda la imagen dentro del archivo creado
        ImageTest.saveImage(resizedImage, file.getPath());
        pic = "C:\\Descargas-CBT\\bioq.png";
    }//GEN-LAST:event_btnCamaraActionPerformed

    private void txtEspecialidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEspecialidadKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {

            fechaAlta.requestFocusInWindow();

        }
    }//GEN-LAST:event_txtEspecialidadKeyPressed

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            fechanacimiento.requestFocusInWindow();
        }

    }//GEN-LAST:event_txtnombreKeyPressed

    private void fechanacimientoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechanacimientoKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            cbosexo.requestFocus();
        }

    }//GEN-LAST:event_fechanacimientoKeyPressed

    private void txtUniversidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUniversidadKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtFacultad.requestFocus();
        }

    }//GEN-LAST:event_txtUniversidadKeyPressed

    private void txtFacultadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFacultadKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            fechaEgreso.requestFocusInWindow();
        }

    }//GEN-LAST:event_txtFacultadKeyPressed

    private void fechaAltaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechaAltaKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtNumeroDeFolio.requestFocus();
        }

    }//GEN-LAST:event_fechaAltaKeyPressed

    private void txtMatriculaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMatriculaKeyPressed

        txtMatricula.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
                java.util.Collections.EMPTY_SET);
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {

            if (!txtMatricula.getText().equals("")) {
                String sSQL = "";
                ConexionMariaDB mysql = new ConexionMariaDB();
                Connection cn = mysql.Conectar();

                sSQL = "SELECT matricula_colegiado FROM colegiados where matricula_colegiado = " + txtMatricula.getText();
                try {
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sSQL);
                    rs.last();
                    if (rs.getString("matricula_colegiado") != null) {
                        txtMatricula.setText(null);
                        JOptionPane.showMessageDialog(null, "La matrícula " + rs.getString(1) + " ya se encuentra cargada en la base de datos");
                    } else {
                        btnExaminar.requestFocus();
                    }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            } else {
                txtMatricula.requestFocus();
            }
        }

    }//GEN-LAST:event_txtMatriculaKeyPressed

    private void txtCodigoPostalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoPostalKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtMail.requestFocus();
        }
    }//GEN-LAST:event_txtCodigoPostalKeyPressed

    private void txtLocalidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLocalidadKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtCodigoPostal.requestFocus();
        }
    }//GEN-LAST:event_txtLocalidadKeyPressed

    private void txtLocalidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLocalidadActionPerformed

        txtCodigoPostal.setText(String.valueOf(Localidad.buscarLocalidad(txtLocalidad.getText(), HiloInicio.listaLocalidades).getCp()));
    }//GEN-LAST:event_txtLocalidadActionPerformed

    private void txtDeptoDireccionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDeptoDireccionKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDeptoDireccionKeyReleased

    private void txtDeptoDireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDeptoDireccionKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDeptoDireccionKeyPressed

    private void txtTelFijoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelFijoKeyReleased
        if (!Numerico.isNumeric(txtTelFijo.getText())) {
            txtTelFijo.setText(null);
        }
    }//GEN-LAST:event_txtTelFijoKeyReleased

    private void txtTelFijoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelFijoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtTelCelular.requestFocus();
        }
    }//GEN-LAST:event_txtTelFijoKeyPressed

    private void txtDireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDireccionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtTelFijo.requestFocus();
        }
    }//GEN-LAST:event_txtDireccionKeyPressed

    private void txtPisoDireccionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPisoDireccionKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPisoDireccionKeyReleased

    private void txtPisoDireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPisoDireccionKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPisoDireccionKeyPressed

    private void txtNroDireccionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNroDireccionKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNroDireccionKeyReleased

    private void txtNroDireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNroDireccionKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNroDireccionKeyPressed

    private void txtMailKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMailKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            cboTipoProf.requestFocus();
        }
    }//GEN-LAST:event_txtMailKeyPressed

    private void txtTelCelularKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelCelularKeyReleased
        if (!Numerico.isNumeric(txtTelCelular.getText())) {
            txtTelCelular.setText(null);
        }
    }//GEN-LAST:event_txtTelCelularKeyReleased

    private void txtTelCelularKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelCelularKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtLocalidad.requestFocus();
        }
    }//GEN-LAST:event_txtTelCelularKeyPressed

    private void cboEstadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboEstadoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboEstadoKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCamara;
    private javax.swing.JButton btnExaminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboEstado;
    private javax.swing.JComboBox cboTipoProf;
    private javax.swing.JComboBox cbosexo;
    private javax.swing.JComboBox cbotipodoc;
    private com.toedter.calendar.JDateChooser fechaAlta;
    private com.toedter.calendar.JDateChooser fechaDoctorado;
    private com.toedter.calendar.JDateChooser fechaEgreso;
    private com.toedter.calendar.JDateChooser fechanacimiento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel lblFoto;
    private javax.swing.JTextField txtCodigoPostal;
    private javax.swing.JTextField txtCuil;
    private javax.swing.JTextField txtDeptoDireccion;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtEspecialidad;
    private javax.swing.JTextField txtFacultad;
    private javax.swing.JTextField txtLocalidad;
    private javax.swing.JTextField txtLocalidadNacimiento;
    private javax.swing.JTextField txtMail;
    private javax.swing.JTextField txtMatricula;
    private javax.swing.JTextField txtNacionalidad;
    private javax.swing.JTextField txtNroDireccion;
    private javax.swing.JTextField txtNumeroDeActa;
    private javax.swing.JTextField txtNumeroDeFolio;
    private javax.swing.JTextField txtNumeroDeLibro;
    private javax.swing.JTextField txtNumeroTitulo;
    private javax.swing.JTextField txtPisoDireccion;
    private javax.swing.JTextField txtTelCelular;
    private javax.swing.JTextField txtTelFijo;
    private javax.swing.JTextField txtUniversidad;
    private javax.swing.JTextField txtdocumento;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
