package vista;

import com.mxrck.autocompleter.TextAutoCompleter;
import controlador.ConexionMariaDB;
import controlador.Exportar;
import controlador.RowsRendererDDJJ;
import java.awt.Cursor;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class ListadoPagosOS extends javax.swing.JDialog {
    
    DefaultTableModel model;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    RowsRendererDDJJ rr = new RowsRendererDDJJ(11);
    int contadorobrasocial = 0, id_obrasocial;
    String[] obrasocial = new String[1500];
    public static int[] idobrasocial = new int[500];
    int x, y;

    
    public ListadoPagosOS(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargarperiodo();      
    }
    
    void cargarperiodo() {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String sql = "SELECT * FROM vista_periodos";
        cboperiodo.removeAllItems();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);            
            while (rs.next()) {
                cboperiodo.addItem(rs.getString(1));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

   

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    void cursor() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));        
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cursor2() {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));       
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.pack();
    }

   
    
     void AnularOrdenes(int periodo, int codigo_obrasocial, int id_colegiados) {
        System.out.println("periodo " + periodo);
        System.out.println("codigo_obrasocial " + codigo_obrasocial);
        System.out.println("id_colegiados " + id_colegiados);
        try {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            String sSQL5 = "UPDATE ordenes o \n"
                    + "INNER JOIN obrasocial os ON o.id_obrasocial = os.id_obrasocial \n"
                    + "SET o.estado_orden=0 \n"
                    + "WHERE ((os.codigo_obrasocial=" + codigo_obrasocial + ") and (o.periodo=" + periodo + ") and (o.id_colegiados=" + id_colegiados + "))";
            PreparedStatement pst5 = cn.prepareStatement(sSQL5);
            pst5.executeUpdate();
        } catch (Exception e) {
            cursor2();
            JOptionPane.showMessageDialog(null, e);
        }
    }
     
     public void cargartablaObraSocial(String valor, String periodo) {
        String[] Titulo = {"Código", "Matricula", "Nombre", "Fecha de declaración", "Código OS", "Obra Social", "Periodo", "Honorarios", "Importe", "Prácticas", "Ordenes", "Estado", "id_colegiados", "cod_obrasocial"};
        Object[] Registros = new Object[14];
        String sql = "SELECT id_declaracion_jurada, matricula_colegiado, nombre_colegiado, fecha_declaracion, cod_obra_social, obra_social, periodo_declaracion, uni_honorarios, replace(importe, ',', '.'), practicas, ordenes, estado,id_colegiados,cod_obra_social FROM declaracion_jurada WHERE cod_obra_social = '" + valor + "' and periodo_declaracion='" + periodo + "' order by matricula_colegiado";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        cursor();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                Registros[9] = rs.getString(10);
                Registros[10] = rs.getString(11);
                Registros[11] = rs.getString(12);
                Registros[12] = rs.getString(13);///id_colegioados
                Registros[13] = rs.getString(14);//cod obra social
                model.addRow(Registros);
                tabladdjj.setDefaultRenderer(Object.class, rr);
            }
            tabladdjj.setModel(model);
            tabladdjj.setAutoCreateRowSorter(true);
           // cargatotales();
            /////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////
            tabladdjj.getColumnModel().getColumn(11).setMaxWidth(0);
            tabladdjj.getColumnModel().getColumn(11).setMinWidth(0);
            tabladdjj.getColumnModel().getColumn(11).setPreferredWidth(0);
            ///////////////////////////////////////////////////////////////////
            tabladdjj.getColumnModel().getColumn(12).setMaxWidth(0);
            tabladdjj.getColumnModel().getColumn(12).setMinWidth(0);
            tabladdjj.getColumnModel().getColumn(12).setPreferredWidth(0);
            ////////////////////////////////////////////////////////////////////
            tabladdjj.getColumnModel().getColumn(13).setMaxWidth(0);
            tabladdjj.getColumnModel().getColumn(13).setMinWidth(0);
            tabladdjj.getColumnModel().getColumn(13).setPreferredWidth(0);
            cursor2();
            //////////////////////////
        } catch (SQLException ex) {
            cursor2();
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void cargartabla(String valor) {
        //////////////////////0         1           2               3                   4           5              6            7           8           9           10          11      12                  13       
        String[] Titulo = {"Código", "Matricula", "Nombre", "Fecha de declaración", "Código OS", "Obra Social", "Periodo", "Honorarios", "Importe", "Prácticas", "Ordenes", "Estado", "id_colegiados", "cod_obrasocial"};
        Object[] Registros = new Object[14];
        String sql = "SELECT id_declaracion_jurada, matricula_colegiado, nombre_colegiado, fecha_declaracion, cod_obra_social, obra_social, periodo_declaracion, uni_honorarios, replace(importe, ',', '.'), practicas, ordenes, estado,id_colegiados,cod_obra_social FROM declaracion_jurada WHERE periodo_declaracion = '" + valor + "'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        cursor();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                Registros[9] = rs.getString(10);
                Registros[10] = rs.getString(11);
                Registros[11] = rs.getString(12);
                Registros[12] = rs.getString(13);
                Registros[13] = rs.getString(14);
                model.addRow(Registros);
                tabladdjj.setDefaultRenderer(Object.class, rr);
            }
            tabladdjj.setModel(model);
            tabladdjj.setAutoCreateRowSorter(true);
            //cargatotales();
            /////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////
            tabladdjj.getColumnModel().getColumn(11).setMaxWidth(0);
            tabladdjj.getColumnModel().getColumn(11).setMinWidth(0);
            tabladdjj.getColumnModel().getColumn(11).setPreferredWidth(0);
            ///////////////////////////////////////////////////////////////////
            tabladdjj.getColumnModel().getColumn(12).setMaxWidth(0);
            tabladdjj.getColumnModel().getColumn(12).setMinWidth(0);
            tabladdjj.getColumnModel().getColumn(12).setPreferredWidth(0);
            ////////////////////////////////////////////////////////////////////
            tabladdjj.getColumnModel().getColumn(13).setMaxWidth(0);
            tabladdjj.getColumnModel().getColumn(13).setMinWidth(0);
            tabladdjj.getColumnModel().getColumn(13).setPreferredWidth(0);
            cursor2();
            //////////////////////////
        } catch (SQLException ex) {
            cursor2();
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        rSLabelIcon1 = new RSMaterialComponent.RSLabelIcon();
        txtbuscar = new RSMaterialComponent.RSTextFieldOne();
        cboperiodo = new RSMaterialComponent.RSComboBox();
        jPanel4 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        txttotalordenes = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txttotapracticas = new javax.swing.JTextField();
        btnImprimir = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnBaja = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnActualizar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnexportar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tabladdjj = new RSMaterialComponent.RSTableMetroCustom();
        jPanel6 = new javax.swing.JPanel();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));

        jPanel3.setBackground(new java.awt.Color(66, 133, 200));

        rSLabelIcon1.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);

        txtbuscar.setForeground(new java.awt.Color(51, 51, 51));
        txtbuscar.setBorderColor(new java.awt.Color(255, 255, 255));
        txtbuscar.setPhColor(new java.awt.Color(51, 51, 51));
        txtbuscar.setPlaceholder("Buscar");
        txtbuscar.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
        });

        cboperiodo.setForeground(new java.awt.Color(51, 51, 51));
        cboperiodo.setColorArrow(new java.awt.Color(66, 133, 200));
        cboperiodo.setColorBorde(new java.awt.Color(255, 255, 255));
        cboperiodo.setColorBoton(new java.awt.Color(255, 255, 255));
        cboperiodo.setColorFondo(new java.awt.Color(255, 255, 255));
        cboperiodo.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboperiodo.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboperiodo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboperiodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboperiodoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cboperiodo, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(rSLabelIcon1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                        .addComponent(cboperiodo, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(66, 133, 200));

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Total Ordenes:");

        txttotalordenes.setEditable(false);
        txttotalordenes.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txttotalordenes.setForeground(new java.awt.Color(255, 255, 255));
        txttotalordenes.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotalordenes.setBorder(null);
        txttotalordenes.setOpaque(false);

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("Total Practicas:");

        txttotapracticas.setEditable(false);
        txttotapracticas.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txttotapracticas.setForeground(new java.awt.Color(255, 255, 255));
        txttotapracticas.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotapracticas.setBorder(null);
        txttotapracticas.setOpaque(false);

        btnImprimir.setBackground(new java.awt.Color(255, 255, 255));
        btnImprimir.setForeground(new java.awt.Color(0, 0, 0));
        btnImprimir.setText("Imprimir");
        btnImprimir.setBackgroundHover(new java.awt.Color(66, 133, 200));
        btnImprimir.setForegroundIcon(new java.awt.Color(66, 133, 200));
        btnImprimir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnImprimir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.PRINT);
        btnImprimir.setRound(20);
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        btnBaja.setBackground(new java.awt.Color(255, 255, 255));
        btnBaja.setForeground(new java.awt.Color(0, 0, 0));
        btnBaja.setText("Baja");
        btnBaja.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnBaja.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnBaja.setForegroundText(new java.awt.Color(51, 51, 51));
        btnBaja.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.DELETE);
        btnBaja.setRound(20);
        btnBaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBajaActionPerformed(evt);
            }
        });

        btnActualizar.setBackground(new java.awt.Color(255, 255, 255));
        btnActualizar.setForeground(new java.awt.Color(0, 0, 0));
        btnActualizar.setText("Actualizar monto");
        btnActualizar.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnActualizar.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnActualizar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnActualizar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnActualizar.setRound(20);
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        btnexportar.setBackground(new java.awt.Color(255, 255, 255));
        btnexportar.setForeground(new java.awt.Color(0, 0, 0));
        btnexportar.setText("Exportar");
        btnexportar.setBackgroundHover(new java.awt.Color(66, 133, 200));
        btnexportar.setForegroundIcon(new java.awt.Color(66, 133, 200));
        btnexportar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnexportar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.VIEW_QUILT);
        btnexportar.setRound(20);
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });

        btnsalir.setBackground(new java.awt.Color(255, 255, 255));
        btnsalir.setForeground(new java.awt.Color(0, 0, 0));
        btnsalir.setText("Salir");
        btnsalir.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnsalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnsalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnsalir.setRound(20);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotalordenes, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotapracticas, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnBaja, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 22, Short.MAX_VALUE)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txttotapracticas)
                    .addComponent(txttotalordenes)
                    .addComponent(jLabel23))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tabladdjj.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tabladdjj.setForeground(new java.awt.Color(66, 133, 200));
        tabladdjj.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabladdjj.setBackgoundHead(new java.awt.Color(66, 133, 200));
        tabladdjj.setBackgoundHover(new java.awt.Color(66, 133, 200));
        tabladdjj.setBorderHead(null);
        tabladdjj.setBorderRows(null);
        tabladdjj.setColorBorderHead(new java.awt.Color(66, 133, 200));
        tabladdjj.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tabladdjj.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tabladdjj.setColorSecondary(new java.awt.Color(255, 255, 255));
        tabladdjj.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tabladdjj.setGridColor(new java.awt.Color(66, 133, 200));
        tabladdjj.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tabladdjj.setShowHorizontalLines(true);
        tabladdjj.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabladdjjMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tabladdjj);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel6.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel6MouseDragged(evt);
            }
        });
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel6MousePressed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(219, 68, 55));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar.setSizeIcon(20.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(418, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tabladdjjMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladdjjMouseClicked

    }//GEN-LAST:event_tabladdjjMouseClicked

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed

        dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void jPanel6MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);
        
    }//GEN-LAST:event_jPanel6MouseDragged

    private void jPanel6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MousePressed

        x = evt.getX();
        y = evt.getY();
        
    }//GEN-LAST:event_jPanel6MousePressed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed

        if (!txtbuscar.getText().equals("")) {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
            tabladdjj.setRowSorter(sorter);
        } else {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*.*"));
            tabladdjj.setRowSorter(sorter);
        }
      //  cargatotales();
        
    }//GEN-LAST:event_txtbuscarKeyPressed

    private void cboperiodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboperiodoActionPerformed

        if(cboperiodo.getSelectedItem()!=null){
        cargartabla(cboperiodo.getSelectedItem().toString());
       // cargatotales();
        }
        
    }//GEN-LAST:event_cboperiodoActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed

        dispose();

    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed

        if (this.tabladdjj.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tabladdjj);
            nom.add("declaracion_jurada");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                controlador.Exportar e = new Exportar(new File(archivo), tb, nom);
                if (e.export()) {
                    JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "CBT", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
            }
        }

    }//GEN-LAST:event_btnexportarActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnBajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBajaActionPerformed

       

    }//GEN-LAST:event_btnBajaActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed

    }//GEN-LAST:event_btnImprimirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonMaterialIconUno btnActualizar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnBaja;
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnImprimir;
    private RSMaterialComponent.RSButtonMaterialIconUno btnexportar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnsalir;
    private RSMaterialComponent.RSComboBox cboperiodo;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane5;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon1;
    private RSMaterialComponent.RSTableMetroCustom tabladdjj;
    private RSMaterialComponent.RSTextFieldOne txtbuscar;
    private javax.swing.JTextField txttotalordenes;
    private javax.swing.JTextField txttotapracticas;
    // End of variables declaration//GEN-END:variables
}
