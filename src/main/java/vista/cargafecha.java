package vista;

import controlador.ConexionMySQLPami;
import controlador.Funciones;
import static vista.Entregas.id_control;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class cargafecha extends javax.swing.JDialog {

    DefaultTableModel model;

    public cargafecha(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        cargartabla(id_control);
        Funciones.funcionescape(this);
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtfechacorreo = new javax.swing.JFormattedTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtobservacion = new javax.swing.JTextArea();
        txtfecharecibo = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        btnaceptar = new javax.swing.JButton();
        txtnombre = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btncancelar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Recibido", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtfechacorreo.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtfechacorreo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfechacorreo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtfechacorreo.setNextFocusableComponent(txtfecharecibo);
        txtfechacorreo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfechacorreoKeyPressed(evt);
            }
        });

        txtobservacion.setColumns(20);
        txtobservacion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtobservacion.setForeground(new java.awt.Color(0, 102, 204));
        txtobservacion.setRows(5);
        txtobservacion.setNextFocusableComponent(btnaceptar);
        txtobservacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtobservacionKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(txtobservacion);

        txtfecharecibo.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtfecharecibo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfecharecibo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtfecharecibo.setNextFocusableComponent(txtnombre);
        txtfecharecibo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfechareciboKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Observaciones");

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728934 - enter login right.png"))); // NOI18N
        btnaceptar.setText("Aceptar");
        btnaceptar.setNextFocusableComponent(btncancelar);
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        txtnombre.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.setText("Bajo puerta");
        txtnombre.setNextFocusableComponent(txtobservacion);
        txtnombre.setSelectionStart(0);
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Recibido por:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Fecha Entrega al Correo:");

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btncancelar.setText("Cancelar");
        btncancelar.setNextFocusableComponent(txtfechacorreo);
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Fecha Recibido por Medico:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnombre))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfechacorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtfecharecibo, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnaceptar)
                        .addGap(18, 18, 18)
                        .addComponent(btncancelar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtfechacorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txtfecharecibo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnaceptar)
                    .addComponent(btncancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncancelarActionPerformed

    public void cargartabla(String id_control) {

        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();

        String[] Titulo = {"Fecha Entrega Correo", "Fecha Recibido Medico", "Nombre Recibido", "Observación"};
        Object[] Registros = new Object[4];

        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        String sql = "SELECT fecha_entrega, fecha_recibido, nombre_control, observacion  FROM control where id_control=" + id_control;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                if (!rs.getString("fecha_entrega").equals("00/00/0000")) {
                    txtfechacorreo.setText(rs.getString("fecha_entrega"));
                    txtfecharecibo.setText(rs.getString("fecha_recibido"));
                    txtnombre.setText(rs.getString("nombre_control"));
                    txtobservacion.setText(rs.getString("observacion"));
                } else {
                    cargarperiodo();
                    txtfechacorreo.select(0, 0);
                }
                txtnombre.selectAll();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarperiodo() {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        txtfechacorreo.setText(formato.format(currentDate));
        txtfecharecibo.setText(formato.format(currentDate));
    }

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        String sql = "UPDATE control SET fecha_entrega=?, fecha_recibido=?, nombre_control=?, observacion=? WHERE id_Control=" + id_control;
        try {
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, txtfechacorreo.getText());
            pst.setString(2, txtfecharecibo.getText());
            pst.setString(3, txtnombre.getText());
            pst.setString(4, txtobservacion.getText());
            int n = pst.executeUpdate();
            if (n > 0) {

            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void txtobservacionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobservacionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtobservacion.transferFocus();
            evt.consume();
        }
        
    }//GEN-LAST:event_txtobservacionKeyPressed

    private void txtfechacorreoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfechacorreoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtfechacorreo.transferFocus();
            txtfecharecibo.select(0, 0);
        }
    }//GEN-LAST:event_txtfechacorreoKeyPressed

    private void txtfechareciboKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfechareciboKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtfecharecibo.transferFocus();
        }
    }//GEN-LAST:event_txtfechareciboKeyPressed

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtnombre.transferFocus();
        }
    }//GEN-LAST:event_txtnombreKeyPressed

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JFormattedTextField txtfechacorreo;
    private javax.swing.JFormattedTextField txtfecharecibo;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextArea txtobservacion;
    // End of variables declaration//GEN-END:variables
}
