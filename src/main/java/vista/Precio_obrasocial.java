package vista;

import controlador.ConexionMariaDB;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import static vista.Login.idusuario;

public class Precio_obrasocial extends javax.swing.JDialog {

    int contadorj = 0, id_obra_social, idobraimprime, id_usuario_validador, periodo_colegiado, id_usuario;
    String ObraSocial, CodObra, año, mes, periodo, colegiado, fecha2, periodo2;
    TextAutoCompleter textAutoAcompleter2;
    public static String[] obrasocial = new String[500];
    public static int[] idobrasocial = new int[500];
    public static String[] nombreobrasocial = new String[500];
    public static Double[] arancel = new Double[500];
    public static int[] idobra = new int[150000];
    public static int contadorobrasocial = 0;
    Hiloobrasocial hilo2;
    Hiloobrasocialtodas hilo3;
    Hilo_verificar_practicas hiloverificar;

    public Precio_obrasocial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        progreso.setVisible(true);
        textAutoAcompleter2 = new TextAutoCompleter(txtobrasocial);
        traerobrasosial();
        cargarperiodo();
        cargarobrasocial();
        this.setLocationRelativeTo(null);
    }

    void traerobrasosial() {

        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT id_obrasocial,razonsocial_obrasocial,importeunidaddearancel_obrasocial,codigo_obrasocial FROM obrasocial ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idobrasocial[contadorobrasocial] = rs.getInt("id_obrasocial");
                nombreobrasocial[contadorobrasocial] = (rs.getString("razonsocial_obrasocial"));
                arancel[contadorobrasocial] = (rs.getDouble("importeunidaddearancel_obrasocial"));
                obrasocial[contadorobrasocial] = (rs.getString("codigo_obrasocial") + " - " + rs.getString("razonsocial_obrasocial"));
                contadorobrasocial++;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarobrasocial() {

        TextAutoCompleter textAutoAcompleter = new TextAutoCompleter(txtobrasocial);

        int i = 0;

        // Recorro y cargo las obras sociales
        while (i < contadorobrasocial) {
            textAutoAcompleter.addItem(obrasocial[i]);

            i++;
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter.setMode(0); // infijo     

        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter.setCaseSensitive(false); //No sensible a mayúsculas        

    }

    public class Hiloobrasocial extends Thread {

        JProgressBar progreso;

        public Hiloobrasocial(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            chkobra.setEnabled(false);
            chkperiodo.setEnabled(false);
            txtaño.setEnabled(false);
            txtmes.setEnabled(false);
            txtarancel.setEnabled(false);
            jtexObservacion.setEnabled(false);
            txtobrasocial.setEnabled(false);
            btnimprimir.setEnabled(false);
            btnsalir.setEnabled(false);
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            try {
                ////////////////////////////Verificacion de Practicas con precio Fijo
                Statement st12 = cn.createStatement();
                String SqlPrecioFijo = "SELECT COUNT(determinacion) FROM obrasocial_tiene_practicasnbu WHERE id_obrasocial=" + id_obra_social + " and preciofijo!=0";
                ResultSet rsPrecioFijo = st12.executeQuery(SqlPrecioFijo);
                if (rsPrecioFijo.next() && rsPrecioFijo.getInt(1) > 0) {
                    JOptionPane.showMessageDialog(null, "Importante: La Obra Social " + txtobrasocial.getText() + " posee " + rsPrecioFijo.getString(1) + " determinaciones con precio Fijo por favor verificar manualmente.");
                }

                ////////////////////////////Actualizacion de Arancel
                Double Arancel = Double.valueOf(txtarancel.getText());

                String sSqlObraSocial = "UPDATE obrasocial set importeunidaddearancel_obrasocial=" + Arancel + " where id_obrasocial=" + id_obra_social;
                PreparedStatement pstObraSocial = cn.prepareStatement(sSqlObraSocial);
                int n = pstObraSocial.executeUpdate();
                if (n > 0) {
                    if (!chkobraActualizada.isSelected()) {
                        String sSqlObraSocialArancel = "update obrasocial_tiene_practicasnbu set obrasocial_tiene_practicasnbu.importeunidaddearancel_obrasocial=" + Arancel + "   \n"
                                + "where  obrasocial_tiene_practicasnbu.id_obrasocial=" + id_obra_social;

                        PreparedStatement pstObraSocialNbuArancel = cn.prepareStatement(sSqlObraSocialArancel);
                        pstObraSocialNbuArancel.executeUpdate();

                        String sSqlObraSocialNbu = "update obrasocial_tiene_practicasnbu set obrasocial_tiene_practicasnbu.preciototal=obrasocial_tiene_practicasnbu.unidaddebioquimica_practica*obrasocial_tiene_practicasnbu.importeunidaddearancel_obrasocial \n"
                                + "where  obrasocial_tiene_practicasnbu.id_obrasocial=" + id_obra_social + "\n"
                                + "and  preciofijo=0";
                        PreparedStatement pstObraSocialNbu = cn.prepareStatement(sSqlObraSocialNbu);
                        pstObraSocialNbu.executeUpdate();
                    }

                    ///////////////////////// Selecciono las Practicas Facturadas de una obra social en un periodo ordenadas de menor a mayor
                    Statement st4 = cn.createStatement();
                    /* String sql4 = "SELECT DISTINCT(detalle_ordenes.cod_practica),detalle_ordenes.precio_practica\n"
                        + "FROM ordenes \n"
                        + "INNER JOIN COLEGIADOS ON ORDENES.ID_COLEGIADOS=COLEGIADOS.ID_COLEGIADOS \n"
                        + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden \n"
                        + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial \n"
                        + "WHERE ordenes.periodo=201709 AND ordenes.id_obrasocial=94 and detalle_ordenes.estado=0 AND (COLEGIADOS.ID_COLEGIADOS=6 or COLEGIADOS.ID_COLEGIADOS=340)  order by (detalle_ordenes.cod_practica) ";*/
                    String sql4 = "SELECT DISTINCT(detalle_ordenes.cod_practica), detalle_ordenes.precio_practica FROM ordenes INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " AND ordenes.id_obrasocial=" + id_obra_social + " and detalle_ordenes.estado=" + 0 + "  order by (detalle_ordenes.cod_practica) ";
                    //String sql4 ="SELECT DISTINCT(detalle_ordenes.cod_practica), detalle_ordenes.precio_practica FROM ordenes INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " AND ordenes.id_obrasocial=" + id_obra_social + " and detalle_ordenes.estado=" + 0 + " and id_colegiados=516  order by (detalle_ordenes.cod_practica) ";
                    ResultSet rs4 = st4.executeQuery(sql4);

                    while (rs4.next()) {

                        ///////////////////////////busca en obra social tiene practicas//////////////////////////
                        Statement st1 = cn.createStatement();
                        String sql1 = "SELECT determinacion,preciototal,codigo_fac_practicas_obrasocial,codigo_practica FROM obrasocial_tiene_practicasnbu WHERE id_obrasocial=" + id_obra_social + " and codigo_practica=" + rs4.getInt("detalle_ordenes.cod_practica");
                        ResultSet rs1 = st1.executeQuery(sql1);

                        if (rs1.next()) {
                            System.out.println(rs1.getInt("codigo_fac_practicas_obrasocial"));
                            ///////////////////////////busca en las ordenes con cada obra social//////////////////////////
                            // String sSQL2 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.precio_practica=" + rs1.getDouble("preciototal") + " , d.cod_practica_fac=" + rs1.getInt("codigo_fac_practicas_obrasocial") + " WHERE o.id_obrasocial=" + id_obra_social + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs1.getInt("codigo_practica") + " and detalle_ordenes.estado=0 )";
                            String sSQL2 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.precio_practica=" + rs1.getDouble("preciototal") + " , d.cod_practica_fac=" + rs1.getInt("codigo_fac_practicas_obrasocial") + " WHERE o.id_obrasocial=" + id_obra_social + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs1.getInt("codigo_practica") + " and d.estado=0 )";
                            System.out.println("entro update");
                            /* String sSQL2 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.precio_practica=" + rs1.getDouble("preciototal") + " , d.cod_practica_fac=" + rs1.getInt("codigo_fac_practicas_obrasocial") + " WHERE o.id_obrasocial=" + id_obra_social + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs1.getInt("codigo_practica") + " and d.estado=0 and o.id_colegiados=516 )";*/
                            PreparedStatement pst = cn.prepareStatement(sSQL2);

                            n = pst.executeUpdate();

                            if (n > 0) {

                            }
                        }

                    }
                }

                String SQL = "INSERT INTO obrasocial_cambio_aranceles (id_obrasocial, arancelAnterior,periodoCambio,id_usuario,observacion,nuevoArancel,fecha)"
                        + "VALUES(?,?,?,?,?,?,CURDATE())";
                PreparedStatement st2 = cn.prepareStatement(SQL);
                st2.setInt(1, id_obra_social);
                st2.setDouble(2, Double.valueOf(txtArancelAnterior.getText()));
                st2.setString(3, txtaño.getText() + txtmes.getText());
                st2.setInt(4, idusuario);
                st2.setString(5, jtexObservacion.getText());
                st2.setDouble(6, Double.valueOf(txtarancel.getText()));
                int n3 = st2.executeUpdate();
                if (n3 > 0) {
                }
                JOptionPane.showMessageDialog(null, "Proceso realizado con exito...");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

            chkobra.setEnabled(true);
            chkperiodo.setEnabled(false);
            txtaño.setEnabled(true);
            txtmes.setEnabled(true);
            txtarancel.setEnabled(true);
            txtobrasocial.setEnabled(true);
            jtexObservacion.setEnabled(true);
            btnimprimir.setEnabled(true);
            btnsalir.setEnabled(true);
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public class Hiloobrasocialtodas extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialtodas(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

//////// por periodo facturado actualiza
        public void run() {
            chkobra.setEnabled(false);
            chkperiodo.setEnabled(false);
            txtaño.setEnabled(false);
            txtmes.setEnabled(false);
            txtobrasocial.setEnabled(false);
            btnimprimir.setEnabled(false);
            btnsalir.setEnabled(false);

            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            try {
                /* ///////////////////////////busca en obra social tiene practicas//////////////////////////
                 Statement st1 = cn.createStatement();
                 String sql1 = "SELECT id_obrasocial,determinacion,preciototal,codigo_fac_practicas_obrasocial,codigo_practica FROM obrasocial_tiene_practicasnbu ";
                 ResultSet rs1 = st1.executeQuery(sql1);
                 while (rs1.next()) {
                 id_obra_social = rs1.getInt(1);
                 Statement st3 = cn.createStatement();
                 String sql3 = "SELECT obrasocial.codigo_obrasocial, ordenes.nombre_afiliado, ordenes.periodo, detalle_ordenes.* FROM ordenes INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden = ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and obrasocial.id_obrasocial=" + id_obra_social + " and cod_practica=" + rs1.getInt(5);
                 ResultSet rs3 = st3.executeQuery(sql3);
                 if (rs3.next()) {
                 ///////////////////////////busca en las ordenes con cada obra social//////////////////////////
                 String sSQL2 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.precio_practica=" + rs1.getDouble(3) + " , d.cod_practica_fac=" + rs1.getInt(4) + " WHERE o.id_obrasocial=" + id_obra_social + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs1.getInt(5) + ")";
                 PreparedStatement pst = cn.prepareStatement(sSQL2);
                 int n = pst.executeUpdate();
                 if (n > 0) {
                 }
                 }
                 }*/
                ///////////////////////// Selecciono las Obras sociales Facturadas de una obra social en un periodo ordenadas de menor a mayor
                Statement st3 = cn.createStatement();
                String sql3 = "SELECT DISTINCT(ordenes.id_obrasocial) FROM ordenes INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " order by (obrasocial.Int_codigo_obrasocial)";
                ResultSet rs3 = st3.executeQuery(sql3);

                while (rs3.next()) {
                    ///////////////////////// Selecciono las Practicas Facturadas de una obra social en un periodo ordenadas de menor a mayor
                    Statement st4 = cn.createStatement();
                    String sql4 = "SELECT DISTINCT(detalle_ordenes.cod_practica), detalle_ordenes.precio_practica FROM ordenes INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " AND ordenes.id_obrasocial=" + rs3.getInt("ordenes.id_obrasocial") + " and detalle_ordenes.estado=" + 0 + " order by (detalle_ordenes.cod_practica) ";
                    ResultSet rs4 = st4.executeQuery(sql4);

                    while (rs4.next()) {
                        ///////////////////////////busca en obra social tiene practicas//////////////////////////
                        Statement st1 = cn.createStatement();
                        String sql1 = "SELECT determinacion,preciototal,codigo_fac_practicas_obrasocial,codigo_practica FROM obrasocial_tiene_practicasnbu WHERE id_obrasocial=" + rs3.getInt("ordenes.id_obrasocial") + " and codigo_practica=" + rs4.getInt("detalle_ordenes.cod_practica");
                        //String sql1 = "SELECT determinacion,preciototal,codigo_fac_practicas_obrasocial,codigo_practica FROM obrasocial_tiene_practicasnbu WHERE id_obrasocial=" + 24 + " and codigo_practica=" + 241;
                        ResultSet rs1 = st1.executeQuery(sql1);
                        rs1.next();
                        ///////////////////////////cambia por practica//////////////////////////

                        String sSQL2 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.precio_practica=" + rs1.getDouble("preciototal") + " , d.cod_practica_fac=" + rs1.getInt("codigo_fac_practicas_obrasocial") + " WHERE o.id_obrasocial=" + rs3.getInt("ordenes.id_obrasocial") + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs1.getInt("codigo_practica") + " and d.estado=0 )";
                        PreparedStatement pst = cn.prepareStatement(sSQL2);
                        int n = pst.executeUpdate();
                        if (n > 0) {
                        }

                    }
                }

                JOptionPane.showMessageDialog(null, "Proceso realizado con exito...");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            chkobra.setEnabled(true);
            chkperiodo.setEnabled(true);
            txtaño.setEnabled(true);
            txtmes.setEnabled(true);
            txtobrasocial.setEnabled(true);
            btnimprimir.setEnabled(true);
            btnsalir.setEnabled(true);

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public class Hilo_verificar_practicas extends Thread {
//
//        JProgressBar progreso;
//
//        public Hilo_verificar_practicas(JProgressBar progreso1) {
//            super();
//            this.progreso = progreso1;
//        }
//
//        //////// por periodo facturado actualiza
//        public void run() {
//            chkobra.setEnabled(false);
//            chkperiodo.setEnabled(false);
//            txtaño.setEnabled(false);
//            txtmes.setEnabled(false);
//            txtobrasocial.setEnabled(false);
//            btnimprimir.setEnabled(false);
//            btVerificarPracticas1.setEnabled(false);
//            btnsalir.setEnabled(false);
//
//            ConexionMariaDB mysql = new ConexionMariaDB();
//            Connection cn = mysql.Conectar();
//
//            try {
//                Statement st8 = cn.createStatement();
//                String sql8 = "SELECT DISTINCT(ordenes.id_obrasocial) FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " order by (obrasocial.Int_codigo_obrasocial)";
//                //String sql8 = "SELECT DISTINCT(ordenes.id_obrasocial) FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and  obrasocial.id_obrasocial="+idobrasocial+"";
//                ResultSet rs8 = st8.executeQuery(sql8);
//
//                while (rs8.next()) {
//                    // JOptionPane.showMessageDialog(null, rs8.getInt(1));
//                    ///////////////////////////////////////////////////////////////////////
//                    //if(rs8.getInt(1)>=24 || rs8.getInt(1)>=25 ){
//                    if (rs8.getInt(1) == id_obra_social) {
//                        // JOptionPane.showMessageDialog(null, rs8.getInt(1));
//                        Statement st4 = cn.createStatement();
//                        String sql4 = "SELECT DISTINCT(detalle_ordenes.cod_practica), detalle_ordenes.precio_practica FROM ordenes INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and ordenes.id_obrasocial=" + rs8.getInt(1) + " and detalle_ordenes.estado=" + 0 + " order by (detalle_ordenes.cod_practica) ";
//                        ResultSet rs4 = st4.executeQuery(sql4);
//
//                        while (rs4.next()) {
//                            ////////////// VERIFICA PRACTICAS
//                            Statement st3 = cn.createStatement();
//                            String sql3 = "SELECT codigo_practica, preciototal FROM obrasocial_tiene_practicasnbu WHERE id_obrasocial= " + rs8.getInt(1);
//                            ResultSet rs3 = st3.executeQuery(sql3);
//                            int banderacambiaestado = 0;
//                            while (rs3.next()) {
//                                if ((rs4.getInt("detalle_ordenes.cod_practica") == rs3.getInt("codigo_practica"))) {
//                                    if ((rs4.getDouble("detalle_ordenes.precio_practica") == rs3.getDouble("preciototal"))) {
//                                        rs3.last();
//                                    } else {
//                                        String sSQL5 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.estado=" + 1 + " WHERE o.id_obrasocial=" + rs8.getInt(1) + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs4.getInt("detalle_ordenes.cod_practica") + " and (d.precio_practica=" + rs4.getDouble("detalle_ordenes.precio_practica") + "))";
//                                        PreparedStatement pst5 = cn.prepareStatement(sSQL5);
//                                        pst5.executeUpdate();
//                                        
//                                    }
//                                    banderacambiaestado = 1;
//                                }
//                            }
//                            if (banderacambiaestado == 0) {
//                                String sSQL6 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.estado=" + 1 + " WHERE o.id_obrasocial=" + rs8.getInt(1) + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs4.getInt("detalle_ordenes.cod_practica") + ")";
//                                PreparedStatement pst6 = cn.prepareStatement(sSQL6);
//                                pst6.executeUpdate();
//                                
//                            }
//                        }
//                    }
//                    ////////////////////////////////////////////////////////////////////////////////////
//                }
//                JOptionPane.showMessageDialog(null, "Proceso realizado con exito...");
//            } catch (Exception e) {
//                JOptionPane.showMessageDialog(null, e);
//            }
//
//            chkobra.setEnabled(true);
//            chkperiodo.setEnabled(true);
//            txtaño.setEnabled(true);
//            txtmes.setEnabled(true);
//            txtobrasocial.setEnabled(true);
//            btVerificarPracticas1.setEnabled(true);
//            btnimprimir.setEnabled(true);
//            btnsalir.setEnabled(true);
//        }
//
//        public void pausa(int mlSeg) {
//            try {
//                // pausa para el splash
//                Thread.sleep(mlSeg);
//            } catch (Exception e) {
//            }
//
//        }
    }

    void cargarperiodo() {
        ////////////////////////////////////////////////////////////////////
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT * FROM periodos ";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            periodo = rs.getString("periodo");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        //////////////////////////////////

        SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);

        fecha2 = formato2.format(currentDate);
        /////////////////////////////////////////////////////////       

        calendar.setTime(currentDate);

    }

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    public double Redondeardosdigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    public int completatotal(double numero) {
        //int centavos=Math.rint(numero * 100) % 100;
        int n = (int) numero;
        return n;
    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        txtobrasocial = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtarancel = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtArancelAnterior = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtmes = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        txtaño = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtexObservacion = new javax.swing.JTextArea();
        chkobraActualizada = new javax.swing.JCheckBox();
        progreso = new javax.swing.JProgressBar();
        btnimprimir = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        chkobra = new javax.swing.JCheckBox();
        chkperiodo = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Obra Social", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtobrasocial.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtobrasocial.setForeground(new java.awt.Color(0, 102, 204));
        txtobrasocial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtobrasocialMouseClicked(evt);
            }
        });
        txtobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialActionPerformed(evt);
            }
        });
        txtobrasocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtobrasocialKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Obra Social:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtobrasocial)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtobrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Actualizacion", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Nuevo Arancel:");

        txtarancel.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txtarancel.setForeground(new java.awt.Color(51, 204, 0));
        txtarancel.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtarancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtarancelActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Arancel Vigente:");

        txtArancelAnterior.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txtArancelAnterior.setForeground(new java.awt.Color(0, 0, 255));
        txtArancelAnterior.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtArancelAnterior.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtArancelAnterior.setEnabled(false);
        txtArancelAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtArancelAnteriorActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Mes:");

        txtmes.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtmes.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtmes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtmes.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmes.setNextFocusableComponent(txtaño);
        txtmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmesActionPerformed(evt);
            }
        });
        txtmes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmesKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Año:");

        txtaño.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtaño.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtaño.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtaño.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtaño.setNextFocusableComponent(txtarancel);
        txtaño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtañoActionPerformed(evt);
            }
        });
        txtaño.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtañoKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("Observaciones");

        jtexObservacion.setColumns(20);
        jtexObservacion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jtexObservacion.setLineWrap(true);
        jtexObservacion.setRows(5);
        jtexObservacion.setText("Actualizacion de Aranceles autorizado por la Obra social.");
        jtexObservacion.setWrapStyleWord(true);
        jScrollPane1.setViewportView(jtexObservacion);

        chkobraActualizada.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chkobraActualizada.setText("Base ya Actualizada");
        chkobraActualizada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkobraActualizadaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel4))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtarancel, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                                    .addComponent(txtArancelAnterior)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtmes, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel6)
                                .addGap(73, 73, 73))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(155, 155, 155)
                        .addComponent(chkobraActualizada)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(chkobraActualizada, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)
                        .addComponent(jLabel3)
                        .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtArancelAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtarancel, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        btnimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728924 - clock time.png"))); // NOI18N
        btnimprimir.setText("Procesar");
        btnimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setMnemonic('v');
        btnsalir.setText("Volver");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        chkobra.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chkobra.setText("Por Obra Social");
        chkobra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkobraActionPerformed(evt);
            }
        });

        chkperiodo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chkperiodo.setText("Por Periodo");
        chkperiodo.setEnabled(false);
        chkperiodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkperiodoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnimprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 237, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(chkobra)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chkperiodo)
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(progreso, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkobra)
                    .addComponent(chkperiodo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmesActionPerformed
        txtmes.transferFocus();
    }//GEN-LAST:event_txtmesActionPerformed

    private void txtañoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtañoActionPerformed
        año = txtaño.getText();
        mes = txtmes.getText();
        periodo2 = txtaño.getText() + txtmes.getText();
        txtaño.transferFocus();
    }//GEN-LAST:event_txtañoActionPerformed

    private void txtobrasocialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtobrasocialMouseClicked
//        String cod = "", nom = "", cadena = txtobrasocial.getText();
//        int i = 0, k = 0, j = 1;
//        ///cod/// 0000 - aaaaaaaaa
//        while (k < cadena.length()) {
//            if (String.valueOf(cadena.charAt(k)).equals("-")) {
//                j = k + 2;
//                k = cadena.length();
//            } else {
//                cod = cod + cadena.charAt(k);
//            }
//            k++;
//        }
//        ///nom/// 0000 - aaaaaaaaa
//        while (j < cadena.length()) {
//            nom = nom + cadena.charAt(j);
//            j++;
//        }
//        while (i < contadorobrasocial) {
//            if (cadena.equals(obrasocial[i])) {
//                id_obra_social = idobrasocial[i];
//                break;
//            }
//            i++;
//        }
//        ObraSocial = nom;
//        CodObra = cod;
//        idobraimprime = id_obra_social;
//        txtobrasocial.transferFocus();
    }//GEN-LAST:event_txtobrasocialMouseClicked

    private void txtobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialActionPerformed

        String cod = "", nom = "", cadena = txtobrasocial.getText();
        int i = 0, k = 0, j = 1;
        ///cod/// 0000 - aaaaaaaaa
        while (k < cadena.length()) {
            if (String.valueOf(cadena.charAt(k)).equals("-")) {
                j = k + 2;
                k = cadena.length();
            } else {
                cod = cod + cadena.charAt(k);
            }
            k++;
        }
        ///nom/// 0000 - aaaaaaaaa
        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }
        while (i < contadorobrasocial) {
            if (cadena.equals(obrasocial[i])) {
                id_obra_social = idobrasocial[i];
                break;
            }
            i++;
        }
        ObraSocial = nom;
        CodObra = cod;
        idobraimprime = id_obra_social;
        txtobrasocial.transferFocus();
    }//GEN-LAST:event_txtobrasocialActionPerformed

    private void btnimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirActionPerformed

        if (chkobra.isSelected()) {
            iniciarSplash();
            hilo2 = new Hiloobrasocial(progreso);
            hilo2.start();
            hilo2 = null;
        }
        if (chkperiodo.isSelected()) {
//            iniciarSplash();
//            hilo3 = new Hiloobrasocialtodas(progreso);
//            hilo3.start();
//            hilo3 = null;
        }

    }//GEN-LAST:event_btnimprimirActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void chkperiodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkperiodoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkperiodoActionPerformed

    private void chkobraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkobraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkobraActionPerformed

    private void txtarancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtarancelActionPerformed
        txtarancel.transferFocus();
    }//GEN-LAST:event_txtarancelActionPerformed

    private void txtArancelAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtArancelAnteriorActionPerformed
        txtArancelAnterior.transferFocus();
    }//GEN-LAST:event_txtArancelAnteriorActionPerformed

    private void txtobrasocialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialKeyPressed

    }//GEN-LAST:event_txtobrasocialKeyPressed

    private void txtmesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmesKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            try {
                Statement st4 = cn.createStatement();
                String sql4 = "  Select importeunidaddearancel_obrasocial\n"
                        + "  From obrasocial\n"
                        + "  where id_obrasocial=" + id_obra_social;
                ResultSet rs4 = st4.executeQuery(sql4);
                if (rs4.next()) {
                    txtArancelAnterior.setText(rs4.getString("importeunidaddearancel_obrasocial"));
                }
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_txtmesKeyPressed

    private void txtañoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtañoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            try {
                Statement st4 = cn.createStatement();
                String sql4 = "  Select periodo\n"
                        + "  From periodos";
                ResultSet rs4 = st4.executeQuery(sql4);
                if (rs4.next()) {
                    if (Integer.valueOf(txtaño.getText() + txtmes.getText()) < rs4.getInt("periodo")) {
                        JOptionPane.showMessageDialog(null, "El periodo " + txtaño.getText() + txtmes.getText() + " es menor que el vigente " + rs4.getInt("periodo") + ",Por Favor verifique lo ingresado");

                    }

                    txtArancelAnterior.setText(rs4.getString("importeunidaddearancel_obrasocial"));
                }
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_txtañoKeyPressed

    private void chkobraActualizadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkobraActualizadaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkobraActualizadaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnimprimir;
    private javax.swing.JButton btnsalir;
    private javax.swing.JCheckBox chkobra;
    private javax.swing.JCheckBox chkobraActualizada;
    private javax.swing.JCheckBox chkperiodo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jtexObservacion;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JTextField txtArancelAnterior;
    private javax.swing.JTextField txtarancel;
    private javax.swing.JFormattedTextField txtaño;
    private javax.swing.JFormattedTextField txtmes;
    private javax.swing.JTextField txtobrasocial;
    // End of variables declaration//GEN-END:variables
}
