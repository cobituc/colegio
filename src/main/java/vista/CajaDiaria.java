package vista;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import controlador.ConexionMariaDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

public class CajaDiaria extends javax.swing.JDialog {

    int x, y;
    private modelo.ConexionMariaDB conexion;
    DefaultTableModel modelCuenta, modeloIngreso, modeloEgreso;
    public static ArrayList<modelo.Cuenta> listaCuenta;

    public CajaDiaria(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        cargarCuenta();
        cargarTablaCuenta();
        txtusuario.setText(Login.usuario);
        cargarCajaDelDia();
    }

    void cargarCajaDelDia() {
        String[] Titulo = {"id_movimientos", "fecha", "descripcion", "importe"};
        String[] Registros = new String[4];
        String sql = "SELECT id_movimientos,fecha,descripcion,importe FROM vista_caja_ingresos where Date(fecha) BETWEEN curdate() AND curdate()";
        modeloIngreso = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ///////////////////////////////
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);

                modeloIngreso.addRow(Registros);
            }
            tablaIngresos.setModel(modeloIngreso);
            tablaIngresos.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        String[] Titulo2 = {"id_movimientos", "fecha", "descripcion", "importe"};
        String[] Registros2 = new String[4];
        String sql2 = "SELECT id_movimientos,fecha,descripcion,importe FROM vista_caja_egresos where Date(fecha) BETWEEN curdate() AND curdate()";
        modeloEgreso = new DefaultTableModel(null, Titulo2) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ///////////////////////////////
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql2);
            while (rs.next()) {
                Registros2[0] = rs.getString(1);
                Registros2[1] = rs.getString(2);
                Registros2[2] = rs.getString(3);
                Registros2[3] = rs.getString(4);

                modeloEgreso.addRow(Registros2);
            }
            tablaEgresos.setModel(modeloEgreso);
            tablaEgresos.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }

    void cargarTablaIngresos(int id_cuenta) {
        String[] Titulo = {"id_movimientos", "fecha", "descripcion", "importe"};
        String[] Registros = new String[4];
        String sql = "SELECT id_movimientos,fecha,descripcion,importe FROM vista_caja_ingresos where id_cuenta=" + id_cuenta;
        modeloIngreso = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ///////////////////////////////
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);

                modeloIngreso.addRow(Registros);
            }
            tablaIngresos.setModel(modeloIngreso);
            tablaIngresos.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    void cargarTablaEgresos(int id_cuenta) {
        String[] Titulo = {"id_movimientos", "fecha", "descripcion", "importe"};
        String[] Registros = new String[4];
        String sql = "SELECT id_movimientos,fecha,descripcion,importe FROM vista_caja_egresos where id_cuenta=" + id_cuenta;
        modeloEgreso = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ///////////////////////////////
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);

                modeloEgreso.addRow(Registros);
            }
            tablaEgresos.setModel(modeloEgreso);
            tablaEgresos.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    void cargarCuenta() {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaCuenta = new ArrayList<modelo.Cuenta>();
        modelo.Cuenta.cargarCuenta(conexion.getConnection(), listaCuenta);
        conexion.cerrarConexion();

    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    void cargatotales() {
        double totalIngresos = 0.00, sumatoriaIngresos = 0.00, totalEgresos = 0.00, sumatoriaEgresos = 0.00;
        int contador = 0, contador2 = 0;
        DecimalFormat df = new DecimalFormat("0.00");
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        int totalRowIngresos = tablaIngresos.getRowCount();
        int totalRowEgresos = tablaEgresos.getRowCount();
        totalRowIngresos -= 1;
        totalRowEgresos -= 1;
        if (totalRowIngresos != -1) {
            for (int i = 0; i <= (totalRowIngresos); i++) {
                /////////////////////////////
                String x = tablaIngresos.getValueAt(i, 3).toString();
                sumatoriaIngresos = Double.valueOf(x);
                ////////////////////////////////////////////////////
                totalIngresos = Redondear(totalIngresos + sumatoriaIngresos);
                contador++;
            }
        }
        if (totalRowEgresos != -1) {
            for (int i = 0; i <= (totalRowEgresos); i++) {
                /////////////////////////////
                String x = tablaEgresos.getValueAt(i, 3).toString();
                sumatoriaEgresos = Double.valueOf(x);
                ////////////////////////////////////////////////////
                totalEgresos = Redondear(totalEgresos + sumatoriaEgresos);
                contador2++;
            }
        }
        txtTotalIngresos.setText((String.valueOf((totalIngresos))));
        txtTotalEgresos.setText((String.valueOf((totalEgresos))));
        txtSubtotalIngresos.setText((String.valueOf((totalIngresos))));
        txtSubtotalEgresos.setText((String.valueOf((totalEgresos))));
        txtMovimientosIngresos.setText((String.valueOf((contador))));
        txtMovimientosEgresos.setText((String.valueOf((contador2))));

    }

    void cargarTablaCuenta() {

        String[] Titulo = {"id", "Número", "Nombre"};
        String[] Registros = new String[3];

        modelCuenta = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (int i = 0; i < listaCuenta.size(); i++) {

            Registros[0] = String.valueOf(listaCuenta.get(i).getId());
            Registros[1] = listaCuenta.get(i).getNumero() + "";
            Registros[2] = listaCuenta.get(i).getNombre();
            modelCuenta.addRow(Registros);

        }
        tablaCuentas.setModel(modelCuenta);
        tablaCuentas.setAutoCreateRowSorter(true);

        tablaCuentas.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaCuentas.getColumnModel().getColumn(0).setMinWidth(0);
        tablaCuentas.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaCuentas.getColumnModel().getColumn(1).setPreferredWidth(50);
        tablaCuentas.getColumnModel().getColumn(2).setPreferredWidth(100);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        rSButtonIconOne1 = new RSMaterialComponent.RSButtonIconOne();
        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaIngresos = new RSMaterialComponent.RSTableMetroCustom();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtEfectivoIngresos = new javax.swing.JLabel();
        txtChequeIngresos = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtTotalIngresos = new javax.swing.JLabel();
        txtSubtotalIngresos = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtMovimientosIngresos = new javax.swing.JLabel();
        txtbuscar1 = new RSMaterialComponent.RSTextFieldOne();
        rSLabelIcon2 = new rojerusan.RSLabelIcon();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablaEgresos = new RSMaterialComponent.RSTableMetroCustom();
        txtEfectivoEgresos = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtChequesEgresos = new javax.swing.JLabel();
        txtTotalEgresos = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtSubtotalEgresos = new javax.swing.JLabel();
        txtMovimientosEgresos = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        rSLabelIcon3 = new rojerusan.RSLabelIcon();
        txtbuscar2 = new RSMaterialComponent.RSTextFieldOne();
        jPanel4 = new javax.swing.JPanel();
        btnCierre = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnCheque = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnDeudas = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnImprimir = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnSalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnIngreso = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnEgreso = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnSalir1 = new RSMaterialComponent.RSButtonMaterialIconUno();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tablaCuentas = new RSMaterialComponent.RSTableMetroCustom();
        txtbuscar = new RSMaterialComponent.RSTextFieldOne();
        rSLabelIcon1 = new rojerusan.RSLabelIcon();
        rSLabelFecha1 = new rojeru_san.rsdate.RSLabelFecha();
        txtusuario = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel3MouseDragged(evt);
            }
        });
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel3MousePressed(evt);
            }
        });

        rSButtonIconOne1.setBackground(new java.awt.Color(214, 45, 32));
        rSButtonIconOne1.setBackgroundHover(new java.awt.Color(142, 68, 55));
        rSButtonIconOne1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        rSButtonIconOne1.setPreferredSize(new java.awt.Dimension(20, 20));
        rSButtonIconOne1.setSizeIcon(20.0F);
        rSButtonIconOne1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rSButtonIconOne1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(418, Short.MAX_VALUE)
                .addComponent(rSButtonIconOne1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rSButtonIconOne1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));

        jPanel5.setBackground(new java.awt.Color(66, 133, 200));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Ingresos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tablaIngresos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaIngresos.setForeground(new java.awt.Color(255, 255, 255));
        tablaIngresos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaIngresos.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaIngresos.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaIngresos.setBorderHead(null);
        tablaIngresos.setBorderRows(null);
        tablaIngresos.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaIngresos.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaIngresos.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaIngresos.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaIngresos.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaIngresos.setGridColor(new java.awt.Color(15, 157, 88));
        tablaIngresos.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaIngresos.setShowHorizontalLines(true);
        tablaIngresos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaIngresosMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tablaIngresos);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Subtotal");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Total:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Efectivo:");

        txtEfectivoIngresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtEfectivoIngresos.setForeground(new java.awt.Color(255, 255, 255));
        txtEfectivoIngresos.setText("0.0");

        txtChequeIngresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtChequeIngresos.setForeground(new java.awt.Color(255, 255, 255));
        txtChequeIngresos.setText("0.0");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Cheques:");

        txtTotalIngresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTotalIngresos.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalIngresos.setText("0.0");

        txtSubtotalIngresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtSubtotalIngresos.setForeground(new java.awt.Color(255, 255, 255));
        txtSubtotalIngresos.setText("0.0");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Cantidad Movimientos: ");

        txtMovimientosIngresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtMovimientosIngresos.setForeground(new java.awt.Color(255, 255, 255));
        txtMovimientosIngresos.setText("0");

        txtbuscar1.setForeground(new java.awt.Color(51, 51, 51));
        txtbuscar1.setBorderColor(new java.awt.Color(255, 255, 255));
        txtbuscar1.setPhColor(new java.awt.Color(51, 51, 51));
        txtbuscar1.setPlaceholder("Buscar");
        txtbuscar1.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtbuscar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscar1ActionPerformed(evt);
            }
        });
        txtbuscar1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscar1KeyReleased(evt);
            }
        });

        rSLabelIcon2.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelIcon2.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);
        rSLabelIcon2.setMaximumSize(new java.awt.Dimension(28, 28));
        rSLabelIcon2.setMinimumSize(new java.awt.Dimension(28, 28));
        rSLabelIcon2.setPreferredSize(new java.awt.Dimension(28, 28));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 549, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSubtotalIngresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEfectivoIngresos, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtChequeIngresos, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMovimientosIngresos, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalIngresos, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(rSLabelIcon2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbuscar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rSLabelIcon2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtbuscar1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtChequeIngresos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
                    .addComponent(txtSubtotalIngresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtEfectivoIngresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtMovimientosIngresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                    .addComponent(txtTotalIngresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(66, 133, 200));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Egresos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N

        jScrollPane6.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane6.setBorder(null);

        tablaEgresos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaEgresos.setForeground(new java.awt.Color(255, 255, 255));
        tablaEgresos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaEgresos.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaEgresos.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaEgresos.setBorderHead(null);
        tablaEgresos.setBorderRows(null);
        tablaEgresos.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaEgresos.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaEgresos.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaEgresos.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaEgresos.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaEgresos.setGridColor(new java.awt.Color(15, 157, 88));
        tablaEgresos.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaEgresos.setShowHorizontalLines(true);
        tablaEgresos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaEgresosMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(tablaEgresos);

        txtEfectivoEgresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtEfectivoEgresos.setForeground(new java.awt.Color(255, 255, 255));
        txtEfectivoEgresos.setText("0.0");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Cheques:");

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Total:");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Subtotal");

        txtChequesEgresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtChequesEgresos.setForeground(new java.awt.Color(255, 255, 255));
        txtChequesEgresos.setText("0.0");

        txtTotalEgresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTotalEgresos.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalEgresos.setText("0.0");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Efectivo:");

        txtSubtotalEgresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtSubtotalEgresos.setForeground(new java.awt.Color(255, 255, 255));
        txtSubtotalEgresos.setText("0.0");

        txtMovimientosEgresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtMovimientosEgresos.setForeground(new java.awt.Color(255, 255, 255));
        txtMovimientosEgresos.setText("0");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Cantidad Movimientos: ");

        rSLabelIcon3.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelIcon3.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);
        rSLabelIcon3.setMaximumSize(new java.awt.Dimension(28, 28));
        rSLabelIcon3.setMinimumSize(new java.awt.Dimension(28, 28));
        rSLabelIcon3.setPreferredSize(new java.awt.Dimension(28, 28));

        txtbuscar2.setForeground(new java.awt.Color(51, 51, 51));
        txtbuscar2.setBorderColor(new java.awt.Color(255, 255, 255));
        txtbuscar2.setPhColor(new java.awt.Color(51, 51, 51));
        txtbuscar2.setPlaceholder("Buscar");
        txtbuscar2.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtbuscar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscar2ActionPerformed(evt);
            }
        });
        txtbuscar2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscar2KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 549, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSubtotalEgresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEfectivoEgresos, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtChequesEgresos, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMovimientosEgresos, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalEgresos, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(rSLabelIcon3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbuscar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rSLabelIcon3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtbuscar2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtChequesEgresos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                    .addComponent(txtSubtotalEgresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtEfectivoEgresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtMovimientosEgresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .addComponent(txtTotalEgresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(17, 17, 17)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel4.setBackground(new java.awt.Color(66, 133, 200));

        btnCierre.setBackground(new java.awt.Color(255, 255, 255));
        btnCierre.setForeground(new java.awt.Color(0, 0, 0));
        btnCierre.setText("Cierre");
        btnCierre.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnCierre.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnCierre.setForegroundText(new java.awt.Color(51, 51, 51));
        btnCierre.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.LOCK);
        btnCierre.setRound(20);
        btnCierre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCierreActionPerformed(evt);
            }
        });

        btnCheque.setBackground(new java.awt.Color(255, 255, 255));
        btnCheque.setForeground(new java.awt.Color(0, 0, 0));
        btnCheque.setText("Cheques");
        btnCheque.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnCheque.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnCheque.setForegroundText(new java.awt.Color(51, 51, 51));
        btnCheque.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.DESCRIPTION);
        btnCheque.setRound(20);
        btnCheque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChequeActionPerformed(evt);
            }
        });

        btnDeudas.setBackground(new java.awt.Color(255, 255, 255));
        btnDeudas.setForeground(new java.awt.Color(0, 0, 0));
        btnDeudas.setText("Deudas");
        btnDeudas.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnDeudas.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnDeudas.setForegroundText(new java.awt.Color(51, 51, 51));
        btnDeudas.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.RECEIPT);
        btnDeudas.setRound(20);
        btnDeudas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeudasActionPerformed(evt);
            }
        });

        btnImprimir.setBackground(new java.awt.Color(255, 255, 255));
        btnImprimir.setForeground(new java.awt.Color(0, 0, 0));
        btnImprimir.setText("Imprimir reporte");
        btnImprimir.setBackgroundHover(new java.awt.Color(66, 133, 200));
        btnImprimir.setForegroundIcon(new java.awt.Color(66, 133, 200));
        btnImprimir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnImprimir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.PRINT);
        btnImprimir.setRound(20);
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        btnSalir.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir.setText("Salir");
        btnSalir.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnSalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnSalir.setRound(20);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnIngreso.setBackground(new java.awt.Color(255, 255, 255));
        btnIngreso.setForeground(new java.awt.Color(0, 0, 0));
        btnIngreso.setText("Ingreso");
        btnIngreso.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnIngreso.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnIngreso.setForegroundText(new java.awt.Color(51, 51, 51));
        btnIngreso.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnIngreso.setRound(20);
        btnIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresoActionPerformed(evt);
            }
        });

        btnEgreso.setBackground(new java.awt.Color(255, 255, 255));
        btnEgreso.setForeground(new java.awt.Color(0, 0, 0));
        btnEgreso.setText("Egreso");
        btnEgreso.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnEgreso.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnEgreso.setForegroundText(new java.awt.Color(51, 51, 51));
        btnEgreso.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.REMOVE);
        btnEgreso.setRound(20);
        btnEgreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEgresoActionPerformed(evt);
            }
        });

        btnSalir1.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir1.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir1.setText("Arqueo");
        btnSalir1.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnSalir1.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnSalir1.setForegroundText(new java.awt.Color(51, 51, 51));
        btnSalir1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXPOSURE);
        btnSalir1.setRound(20);
        btnSalir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalir1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCierre, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCheque, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDeudas, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnEgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnSalir1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSalir1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnCierre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnCheque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnDeudas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnEgreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel7.setBackground(new java.awt.Color(66, 133, 200));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Cuentas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N

        jScrollPane7.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane7.setBorder(null);

        tablaCuentas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaCuentas.setForeground(new java.awt.Color(255, 255, 255));
        tablaCuentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaCuentas.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaCuentas.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaCuentas.setBorderHead(null);
        tablaCuentas.setBorderRows(null);
        tablaCuentas.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaCuentas.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaCuentas.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaCuentas.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaCuentas.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaCuentas.setGridColor(new java.awt.Color(15, 157, 88));
        tablaCuentas.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaCuentas.setShowHorizontalLines(true);
        tablaCuentas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaCuentasMouseClicked(evt);
            }
        });
        jScrollPane7.setViewportView(tablaCuentas);

        txtbuscar.setForeground(new java.awt.Color(51, 51, 51));
        txtbuscar.setBorderColor(new java.awt.Color(255, 255, 255));
        txtbuscar.setPhColor(new java.awt.Color(51, 51, 51));
        txtbuscar.setPlaceholder("Buscar");
        txtbuscar.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        rSLabelIcon1.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);
        rSLabelIcon1.setMaximumSize(new java.awt.Dimension(28, 28));
        rSLabelIcon1.setMinimumSize(new java.awt.Dimension(28, 28));
        rSLabelIcon1.setPreferredSize(new java.awt.Dimension(28, 28));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 513, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7)
                .addContainerGap())
        );

        rSLabelFecha1.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelFecha1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        txtusuario.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtusuario.setForeground(new java.awt.Color(255, 255, 255));
        txtusuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel23.setText("Usuario:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(rSLabelFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rSLabelFecha1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rSButtonIconOne1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSButtonIconOne1ActionPerformed

        dispose();

    }//GEN-LAST:event_rSButtonIconOne1ActionPerformed

    private void jPanel3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);

    }//GEN-LAST:event_jPanel3MouseDragged

    private void jPanel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MousePressed

        x = evt.getX();
        y = evt.getY();

    }//GEN-LAST:event_jPanel3MousePressed

    private void tablaIngresosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaIngresosMouseClicked

    }//GEN-LAST:event_tablaIngresosMouseClicked

    private void tablaEgresosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaEgresosMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaEgresosMouseClicked

    private void btnCierreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCierreActionPerformed
        new CierreCaja(null, true).setVisible(true);
    }//GEN-LAST:event_btnCierreActionPerformed

    private void tablaCuentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaCuentasMouseClicked
        if (evt.getClickCount() == 1) {
            int id = Integer.parseInt(tablaCuentas.getValueAt(tablaCuentas.getSelectedRow(), 0).toString());
            cargarTablaIngresos(id);
            cargarTablaEgresos(id);
            cargatotales();
        }
    }//GEN-LAST:event_tablaCuentasMouseClicked

    private void btnChequeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChequeActionPerformed

        new Cheques(null, true).setVisible(true);

    }//GEN-LAST:event_btnChequeActionPerformed

    private void btnDeudasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeudasActionPerformed

        new ConsultaDeuda(null, true).setVisible(true);

    }//GEN-LAST:event_btnDeudasActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed


    }//GEN-LAST:event_btnImprimirActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();

    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnIngresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresoActionPerformed
        new IngresoCaja(null, true).setVisible(true);
        cargarCajaDelDia();
        cargatotales();
    }//GEN-LAST:event_btnIngresoActionPerformed

    private void btnEgresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEgresoActionPerformed
        new EgresoCaja(null, true).setVisible(true);
        cargarCajaDelDia();
        cargatotales();
    }//GEN-LAST:event_btnEgresoActionPerformed

    private void btnSalir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalir1ActionPerformed
        new ArqueoCaja(null, true).setVisible(true);
    }//GEN-LAST:event_btnSalir1ActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        TableRowSorter sorter = new TableRowSorter(modelCuenta);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
        tablaCuentas.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void txtbuscar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscar1ActionPerformed
        cargatotales();
    }//GEN-LAST:event_txtbuscar1ActionPerformed

    private void txtbuscar1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscar1KeyReleased
        TableRowSorter sorter = new TableRowSorter(modeloIngreso);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar1.getText() + ".*"));
        tablaIngresos.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscar1KeyReleased

    private void txtbuscar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscar2ActionPerformed
        cargatotales();
    }//GEN-LAST:event_txtbuscar2ActionPerformed

    private void txtbuscar2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscar2KeyReleased
        TableRowSorter sorter = new TableRowSorter(modeloEgreso);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar2.getText() + ".*"));
        tablaEgresos.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscar2KeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonMaterialIconUno btnCheque;
    private RSMaterialComponent.RSButtonMaterialIconUno btnCierre;
    private RSMaterialComponent.RSButtonMaterialIconUno btnDeudas;
    private RSMaterialComponent.RSButtonMaterialIconUno btnEgreso;
    private RSMaterialComponent.RSButtonMaterialIconUno btnImprimir;
    private RSMaterialComponent.RSButtonMaterialIconUno btnIngreso;
    private RSMaterialComponent.RSButtonMaterialIconUno btnSalir;
    private RSMaterialComponent.RSButtonMaterialIconUno btnSalir1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private RSMaterialComponent.RSButtonIconOne rSButtonIconOne1;
    private rojeru_san.rsdate.RSLabelFecha rSLabelFecha1;
    private rojerusan.RSLabelIcon rSLabelIcon1;
    private rojerusan.RSLabelIcon rSLabelIcon2;
    private rojerusan.RSLabelIcon rSLabelIcon3;
    private RSMaterialComponent.RSTableMetroCustom tablaCuentas;
    private RSMaterialComponent.RSTableMetroCustom tablaEgresos;
    private RSMaterialComponent.RSTableMetroCustom tablaIngresos;
    private javax.swing.JLabel txtChequeIngresos;
    private javax.swing.JLabel txtChequesEgresos;
    private javax.swing.JLabel txtEfectivoEgresos;
    private javax.swing.JLabel txtEfectivoIngresos;
    private javax.swing.JLabel txtMovimientosEgresos;
    private javax.swing.JLabel txtMovimientosIngresos;
    private javax.swing.JLabel txtSubtotalEgresos;
    private javax.swing.JLabel txtSubtotalIngresos;
    private javax.swing.JLabel txtTotalEgresos;
    private javax.swing.JLabel txtTotalIngresos;
    private RSMaterialComponent.RSTextFieldOne txtbuscar;
    private RSMaterialComponent.RSTextFieldOne txtbuscar1;
    private RSMaterialComponent.RSTextFieldOne txtbuscar2;
    private javax.swing.JLabel txtusuario;
    // End of variables declaration//GEN-END:variables
}
