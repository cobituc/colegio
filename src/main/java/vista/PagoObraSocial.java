package vista;

public class PagoObraSocial extends javax.swing.JDialog {

    public int x, y;

    public PagoObraSocial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();
        jPanel4 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtimporteneto = new RSMaterialComponent.RSTextFieldOne();
        txtiva270 = new RSMaterialComponent.RSTextFieldOne();
        txtiva105 = new RSMaterialComponent.RSTextFieldOne();
        txtiva50 = new RSMaterialComponent.RSTextFieldOne();
        txtiva25 = new RSMaterialComponent.RSTextFieldOne();
        txtiva0 = new RSMaterialComponent.RSTextFieldOne();
        txttributos = new RSMaterialComponent.RSTextFieldOne();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtiva51 = new RSMaterialComponent.RSTextFieldOne();
        txtiva26 = new RSMaterialComponent.RSTextFieldOne();
        txtiva1 = new RSMaterialComponent.RSTextFieldOne();
        jLabel11 = new javax.swing.JLabel();
        txttributos1 = new RSMaterialComponent.RSTextFieldOne();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablapedidos = new RSMaterialComponent.RSTableMetroCustom();
        rSLabelFecha1 = new rojeru_san.rsdate.RSLabelFecha();
        jPanel2 = new javax.swing.JPanel();
        btnaceptar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btncancelar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        jLabel18 = new javax.swing.JLabel();
        txtusuario = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel6.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel6MouseDragged(evt);
            }
        });
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel6MousePressed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(219, 68, 55));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar.setSizeIcon(20.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(345, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(66, 133, 200));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel9.setText("Porcentaje Pagado:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel10.setText("Total Facurado:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel12.setText("Suma Importes:");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel13.setText("Diferencia:");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel14.setText("Periodo:");

        txtimporteneto.setForeground(new java.awt.Color(51, 51, 51));
        txtimporteneto.setBorderColor(new java.awt.Color(255, 255, 255));
        txtimporteneto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtimporteneto.setPhColor(new java.awt.Color(51, 51, 51));
        txtimporteneto.setPlaceholder("N° Recibo Interno");
        txtimporteneto.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtimporteneto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtimportenetoActionPerformed(evt);
            }
        });
        txtimporteneto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtimportenetoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtimportenetoKeyReleased(evt);
            }
        });

        txtiva270.setForeground(new java.awt.Color(51, 51, 51));
        txtiva270.setToolTipText("Obra Social");
        txtiva270.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva270.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva270.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva270.setPlaceholder("Obra Social");
        txtiva270.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva270.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva270KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva270KeyReleased(evt);
            }
        });

        txtiva105.setForeground(new java.awt.Color(51, 51, 51));
        txtiva105.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva105.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva105.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva105.setPlaceholder("");
        txtiva105.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva105.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva105KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva105KeyReleased(evt);
            }
        });

        txtiva50.setForeground(new java.awt.Color(51, 51, 51));
        txtiva50.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva50.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva50.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva50.setPlaceholder("");
        txtiva50.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva50.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva50KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva50KeyReleased(evt);
            }
        });

        txtiva25.setForeground(new java.awt.Color(51, 51, 51));
        txtiva25.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva25.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva25.setPlaceholder("");
        txtiva25.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva25.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva25KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva25KeyReleased(evt);
            }
        });

        txtiva0.setForeground(new java.awt.Color(51, 51, 51));
        txtiva0.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva0.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva0.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva0.setPlaceholder("");
        txtiva0.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva0.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva0KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva0KeyReleased(evt);
            }
        });

        txttributos.setForeground(new java.awt.Color(51, 51, 51));
        txttributos.setBorderColor(new java.awt.Color(255, 255, 255));
        txttributos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttributos.setPhColor(new java.awt.Color(51, 51, 51));
        txttributos.setPlaceholder("");
        txttributos.setSelectionColor(new java.awt.Color(244, 180, 0));
        txttributos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttributosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttributosKeyReleased(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel15.setText("Fecha de Pago");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel16.setText("N° Recibo:");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel17.setText("Leyenda:");

        txtiva51.setForeground(new java.awt.Color(51, 51, 51));
        txtiva51.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva51.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva51.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva51.setPlaceholder("");
        txtiva51.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva51.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva51KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva51KeyReleased(evt);
            }
        });

        txtiva26.setForeground(new java.awt.Color(51, 51, 51));
        txtiva26.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva26.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva26.setPlaceholder("");
        txtiva26.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva26KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva26KeyReleased(evt);
            }
        });

        txtiva1.setForeground(new java.awt.Color(51, 51, 51));
        txtiva1.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva1.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva1.setPlaceholder("");
        txtiva1.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva1KeyReleased(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel11.setText("Porcentaje a Liquidar:");

        txttributos1.setForeground(new java.awt.Color(51, 51, 51));
        txttributos1.setBorderColor(new java.awt.Color(255, 255, 255));
        txttributos1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttributos1.setPhColor(new java.awt.Color(51, 51, 51));
        txttributos1.setPlaceholder("");
        txttributos1.setSelectionColor(new java.awt.Color(244, 180, 0));
        txttributos1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttributos1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttributos1KeyReleased(evt);
            }
        });

        jScrollPane6.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane6.setBorder(null);

        tablapedidos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablapedidos.setForeground(new java.awt.Color(255, 255, 255));
        tablapedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Nombre", "Periodo", "U.H.", "Neto", "IVA", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablapedidos.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablapedidos.setBackgoundHover(new java.awt.Color(255, 255, 255));
        tablapedidos.setBorderHead(null);
        tablapedidos.setBorderRows(null);
        tablapedidos.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablapedidos.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablapedidos.setColorPrimaryText(new java.awt.Color(51, 51, 51));
        tablapedidos.setColorSecondary(new java.awt.Color(227, 233, 230));
        tablapedidos.setColorSecundaryText(new java.awt.Color(51, 51, 51));
        tablapedidos.setGridColor(new java.awt.Color(15, 157, 88));
        tablapedidos.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablapedidos.setShowHorizontalLines(true);
        tablapedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapedidosKeyPressed(evt);
            }
        });
        jScrollPane6.setViewportView(tablapedidos);

        rSLabelFecha1.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelFecha1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(txtimporteneto, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtiva270, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(rSLabelFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel10)
                    .addComponent(jLabel9)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(txttributos, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtiva50, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva25, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva0, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva105, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16)
                            .addComponent(jLabel11)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtiva51, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva26, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva1, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txttributos1, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))))
            .addComponent(jScrollPane6)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtimporteneto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtiva270, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(rSLabelFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva51, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva26, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txttributos1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva105, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva50, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva25, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiva0, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txttributos, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));

        btnaceptar.setBackground(new java.awt.Color(255, 255, 255));
        btnaceptar.setForeground(new java.awt.Color(0, 0, 0));
        btnaceptar.setText("Aceptar");
        btnaceptar.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnaceptar.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnaceptar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnaceptar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CHECK);
        btnaceptar.setRound(20);
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btncancelar.setBackground(new java.awt.Color(255, 255, 255));
        btncancelar.setForeground(new java.awt.Color(0, 0, 0));
        btncancelar.setText("Borrar");
        btncancelar.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btncancelar.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btncancelar.setForegroundText(new java.awt.Color(51, 51, 51));
        btncancelar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.DELETE);
        btncancelar.setRound(20);
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnsalir.setBackground(new java.awt.Color(255, 255, 255));
        btnsalir.setForeground(new java.awt.Color(0, 0, 0));
        btnsalir.setText("Salir");
        btnsalir.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnsalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnsalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnsalir.setRound(20);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel18.setText("Usuario:");

        txtusuario.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtusuario.setForeground(new java.awt.Color(255, 255, 255));
        txtusuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed

        dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void jPanel6MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);
    }//GEN-LAST:event_jPanel6MouseDragged

    private void jPanel6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MousePressed

        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jPanel6MousePressed

    private void txtimportenetoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtimportenetoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtimportenetoKeyPressed

    private void txtimportenetoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtimportenetoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtimportenetoKeyReleased

    private void txtiva270KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva270KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva270KeyPressed

    private void txtiva270KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva270KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva270KeyReleased

    private void txtiva105KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva105KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva105KeyPressed

    private void txtiva105KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva105KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva105KeyReleased

    private void txtiva50KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva50KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva50KeyPressed

    private void txtiva50KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva50KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva50KeyReleased

    private void txtiva25KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva25KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva25KeyPressed

    private void txtiva25KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva25KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva25KeyReleased

    private void txtiva0KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva0KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva0KeyPressed

    private void txtiva0KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva0KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva0KeyReleased

    private void txttributosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttributosKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttributosKeyPressed

    private void txttributosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttributosKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txttributosKeyReleased

    private void txtiva51KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva51KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva51KeyPressed

    private void txtiva51KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva51KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva51KeyReleased

    private void txtiva26KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva26KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva26KeyPressed

    private void txtiva26KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva26KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva26KeyReleased

    private void txtiva1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva1KeyPressed

    private void txtiva1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva1KeyReleased

    private void txttributos1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttributos1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttributos1KeyPressed

    private void txttributos1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttributos1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txttributos1KeyReleased

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed

    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed

    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed

        dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void tablapedidosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapedidosKeyPressed

    }//GEN-LAST:event_tablapedidosKeyPressed

    private void txtimportenetoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtimportenetoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtimportenetoActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnaceptar;
    private RSMaterialComponent.RSButtonMaterialIconUno btncancelar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnsalir;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane6;
    private rojeru_san.rsdate.RSLabelFecha rSLabelFecha1;
    private RSMaterialComponent.RSTableMetroCustom tablapedidos;
    private RSMaterialComponent.RSTextFieldOne txtimporteneto;
    private RSMaterialComponent.RSTextFieldOne txtiva0;
    private RSMaterialComponent.RSTextFieldOne txtiva1;
    private RSMaterialComponent.RSTextFieldOne txtiva105;
    private RSMaterialComponent.RSTextFieldOne txtiva25;
    private RSMaterialComponent.RSTextFieldOne txtiva26;
    private RSMaterialComponent.RSTextFieldOne txtiva270;
    private RSMaterialComponent.RSTextFieldOne txtiva50;
    private RSMaterialComponent.RSTextFieldOne txtiva51;
    private RSMaterialComponent.RSTextFieldOne txttributos;
    private RSMaterialComponent.RSTextFieldOne txttributos1;
    private javax.swing.JLabel txtusuario;
    // End of variables declaration//GEN-END:variables
}
