package vista;


import controlador.ConexionMariaDB;
import static controlador.HiloInicio.listaOS;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class BuscaModificaOS extends javax.swing.JDialog {

    DefaultTableModel model;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    public static int banfiltro = 0;
    public static int filasel;
    public static String codigo = "";
    String id;

    public BuscaModificaOS(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargartabla("");
        ///botondarde();
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void cargartabla(String valor) {
        String[] Titulo = {"Codigo", "Razon Social", "Arancel", "Estado"};
        Object[] Registros = new Object[4];
       
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };                   
            
            for(int i =0; i<listaOS.size(); i++){            
                    
            Registros[0] = String.valueOf(listaOS.get(i).getCodigo());
                Registros[1] = listaOS.get(i).getNombre();
                Registros[2] = listaOS.get(i).getImporteUnidadDeArancel();
                if(listaOS.get(i).getEstado()==1){
                   Registros[3] = "Activo";
               }else{
                   Registros[3] = "Baja";
               }
            model.addRow(Registros);
        }
            tablaobrasocial.setModel(model);
            tablaobrasocial.setAutoCreateRowSorter(true);
            tablaobrasocial.getColumnModel().getColumn(0).setPreferredWidth(40);
            tablaobrasocial.getColumnModel().getColumn(1).setPreferredWidth(200);
            tablaobrasocial.getColumnModel().getColumn(2).setPreferredWidth(90);
            tablaobrasocial.getColumnModel().getColumn(3).setPreferredWidth(90);          
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tablaobrasocial = new javax.swing.JTable();
        btnmodificar = new javax.swing.JButton();
        btnestado = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        txtbuscar = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tablaobrasocial.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tablaobrasocial.setAlignmentX(1.0F);
        tablaobrasocial.setAlignmentY(1.0F);
        tablaobrasocial.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tablaobrasocialFocusGained(evt);
            }
        });
        tablaobrasocial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaobrasocialMouseClicked(evt);
            }
        });
        tablaobrasocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaobrasocialKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaobrasocial);

        btnmodificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnmodificar.setText("Modificar");
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });

        btnestado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnestado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnestado.setText("Dar de Baja");
        btnestado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnestadoActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setText("Volver");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728952 - magnifier zoom.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 886, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnmodificar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnestado))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnsalir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnestado, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnmodificar, javax.swing.GroupLayout.Alignment.LEADING))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
void botondarde() {
        tablaobrasocial.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel rowSM = tablaobrasocial.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel lsm = (ListSelectionModel) e.getSource();
                if (lsm.isSelectionEmpty()) {

                } else {
                    int selectedRow = lsm.getMinSelectionIndex();
                    String estado = tablaobrasocial.getValueAt(selectedRow, 4).toString();
                    if (estado.equals("0")) {
                        btnestado.setText("Dar de Alta");

                    } else {
                        btnestado.setText("Dar de Baja");

                    }

                }
            }
        });

    }

    private void tablaobrasocialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaobrasocialKeyPressed
                if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablaobrasocialKeyPressed

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed

        filasel = tablaobrasocial.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            codigo = tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 0).toString();
            id = tablaobrasocial.getValueAt(filasel, 0).toString();

            new ModificaObraSocial(null, true).setVisible(true);
            cargartabla("");

        }
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void btnestadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnestadoActionPerformed
         String codos= tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 0).toString();
        if(btnestado.getText().equals("Dar de Baja"))
            {
               
               
            try {
                ConexionMariaDB mysql = new ConexionMariaDB();
                Connection cn = mysql.Conectar();
                String sSQL;
                
                sSQL = "UPDATE obrasocial SET estado_obrasocial=0 WHERE codigo_obrasocial="+codos;
                PreparedStatement pst = cn.prepareStatement(sSQL);
                int n = pst.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "La Obra Social se dió de baja...");
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                }
            } catch (SQLException ex) {
                Logger.getLogger(BuscaModificaOS.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
        else
            {
             try {
                ConexionMariaDB mysql = new ConexionMariaDB();
                Connection cn = mysql.Conectar();
                String sSQL;
                
                sSQL = "UPDATE obrasocial SET estado_obrasocial=1 WHERE codigo_obrasocial="+codos;
                PreparedStatement pst = cn.prepareStatement(sSQL);
                int n = pst.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "La Obra Social se dió de alta...");
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                }
            } catch (SQLException ex) {
                Logger.getLogger(BuscaModificaOS.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
    }//GEN-LAST:event_btnestadoActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();

    }//GEN-LAST:event_btnsalirActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void tablaobrasocialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaobrasocialMouseClicked
    /*    if (tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 4).equals("0")) {
            btnestado.setText("Dar de Alta");

        } else {
            btnestado.setText("Dar de Baja");

        }*/
    }//GEN-LAST:event_tablaobrasocialMouseClicked

    private void tablaobrasocialFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tablaobrasocialFocusGained
/*        if (tablaobrasocial.getValueAt(tablaobrasocial.getSelectedRow(), 4).equals("0")) {
            btnestado.setText("Dar de Alta");

        } else {
            btnestado.setText("Dar de Baja");

        }*/
    }//GEN-LAST:event_tablaobrasocialFocusGained
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnestado;
    private javax.swing.JButton btnmodificar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaobrasocial;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
