package vista;

import java.text.DecimalFormat;
import javax.swing.table.DefaultTableModel;
import static vista.ComprobantesPagoObraSocial.ObraSocial;

public class ReciboPagoOs extends javax.swing.JDialog {

    int x, y;

    public ReciboPagoOs(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        txtobrasocial.setText(ObraSocial);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();
        jPanel4 = new javax.swing.JPanel();
        btnSalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnPagar = new RSMaterialComponent.RSButtonMaterialIconUno();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaconceptos = new RSMaterialComponent.RSTableMetroCustom();
        jLabel19 = new javax.swing.JLabel();
        txttotal = new javax.swing.JLabel();
        txtconcepto = new RSMaterialComponent.RSTextFieldOne();
        txtimporte = new RSMaterialComponent.RSTextFieldOne();
        cboCuentaBanco = new RSMaterialComponent.RSComboBox();
        txtobrasocial = new RSMaterialComponent.RSTextFieldOne();
        cboForma = new RSMaterialComponent.RSComboBox();
        txtperiodo = new RSMaterialComponent.RSTextFieldOne();
        txtfactura = new RSMaterialComponent.RSTextFieldOne();
        cboTipo = new RSMaterialComponent.RSComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel3MouseDragged(evt);
            }
        });
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel3MousePressed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(214, 45, 32));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar.setSizeIcon(20.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(285, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(66, 133, 200));

        btnSalir.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir.setText("Cancelar");
        btnSalir.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnSalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnSalir.setRound(20);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnPagar.setBackground(new java.awt.Color(255, 255, 255));
        btnPagar.setForeground(new java.awt.Color(0, 0, 0));
        btnPagar.setText("Aceptar");
        btnPagar.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnPagar.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnPagar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnPagar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnPagar.setRound(20);
        btnPagar.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                btnPagarComponentResized(evt);
            }
        });
        btnPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(btnPagar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPagar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(66, 133, 200));

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tablaconceptos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaconceptos.setForeground(new java.awt.Color(255, 255, 255));
        tablaconceptos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Periodo", "N° Factura", "Importe", "Concepto", "Tipo", "Forma", "Cuenta"
            }
        ));
        tablaconceptos.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaconceptos.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaconceptos.setBorderHead(null);
        tablaconceptos.setBorderRows(null);
        tablaconceptos.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaconceptos.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaconceptos.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaconceptos.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaconceptos.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaconceptos.setGridColor(new java.awt.Color(15, 157, 88));
        tablaconceptos.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaconceptos.setShowHorizontalLines(true);
        tablaconceptos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaconceptosMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tablaconceptos);

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(244, 180, 0));
        jLabel19.setText("Importe Total:");

        txttotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txttotal.setForeground(new java.awt.Color(244, 180, 0));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane5)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txttotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)))
        );

        txtconcepto.setForeground(new java.awt.Color(51, 51, 51));
        txtconcepto.setBorderColor(new java.awt.Color(255, 255, 255));
        txtconcepto.setPhColor(new java.awt.Color(51, 51, 51));
        txtconcepto.setPlaceholder("Ingrese Concepto");
        txtconcepto.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtconcepto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtconceptoActionPerformed(evt);
            }
        });

        txtimporte.setForeground(new java.awt.Color(51, 51, 51));
        txtimporte.setBorderColor(new java.awt.Color(255, 255, 255));
        txtimporte.setPhColor(new java.awt.Color(51, 51, 51));
        txtimporte.setPlaceholder("$0.00");
        txtimporte.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtimporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtimporteActionPerformed(evt);
            }
        });

        cboCuentaBanco.setForeground(new java.awt.Color(51, 51, 51));
        cboCuentaBanco.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Banco HSBC", "Banco Galicia", "Banco Santander", "Banco Santiago" }));
        cboCuentaBanco.setColorArrow(new java.awt.Color(66, 133, 200));
        cboCuentaBanco.setColorBorde(new java.awt.Color(255, 255, 255));
        cboCuentaBanco.setColorBoton(new java.awt.Color(255, 255, 255));
        cboCuentaBanco.setColorFondo(new java.awt.Color(255, 255, 255));
        cboCuentaBanco.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboCuentaBanco.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboCuentaBanco.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboCuentaBanco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCuentaBancoActionPerformed(evt);
            }
        });

        txtobrasocial.setEditable(false);
        txtobrasocial.setBackground(new java.awt.Color(66, 133, 200));
        txtobrasocial.setForeground(new java.awt.Color(255, 255, 255));
        txtobrasocial.setBorderColor(new java.awt.Color(255, 255, 255));
        txtobrasocial.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtobrasocial.setPhColor(new java.awt.Color(51, 51, 51));
        txtobrasocial.setPlaceholder("Obra Social");
        txtobrasocial.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialActionPerformed(evt);
            }
        });
        txtobrasocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtobrasocialKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtobrasocialKeyReleased(evt);
            }
        });

        cboForma.setForeground(new java.awt.Color(51, 51, 51));
        cboForma.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Efectivo", "Cheque", "Transferencia" }));
        cboForma.setColorArrow(new java.awt.Color(66, 133, 200));
        cboForma.setColorBorde(new java.awt.Color(255, 255, 255));
        cboForma.setColorBoton(new java.awt.Color(255, 255, 255));
        cboForma.setColorFondo(new java.awt.Color(255, 255, 255));
        cboForma.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboForma.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboForma.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        txtperiodo.setForeground(new java.awt.Color(51, 51, 51));
        txtperiodo.setBorderColor(new java.awt.Color(255, 255, 255));
        txtperiodo.setPhColor(new java.awt.Color(51, 51, 51));
        txtperiodo.setPlaceholder("Periodo");
        txtperiodo.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtperiodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtperiodoActionPerformed(evt);
            }
        });

        txtfactura.setForeground(new java.awt.Color(51, 51, 51));
        txtfactura.setBorderColor(new java.awt.Color(255, 255, 255));
        txtfactura.setPhColor(new java.awt.Color(51, 51, 51));
        txtfactura.setPlaceholder("N° Factura");
        txtfactura.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtfactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfacturaActionPerformed(evt);
            }
        });

        cboTipo.setForeground(new java.awt.Color(51, 51, 51));
        cboTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Crédito", "Débito" }));
        cboTipo.setColorArrow(new java.awt.Color(66, 133, 200));
        cboTipo.setColorBorde(new java.awt.Color(255, 255, 255));
        cboTipo.setColorBoton(new java.awt.Color(255, 255, 255));
        cboTipo.setColorFondo(new java.awt.Color(255, 255, 255));
        cboTipo.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboTipo.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboTipo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtobrasocial, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtconcepto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(cboForma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(txtperiodo, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(txtfactura, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(21, 21, 21)))
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(cboCuentaBanco, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(txtimporte, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(cboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))))))
                        .addContainerGap())))
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtobrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtperiodo, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtfactura, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtimporte, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboForma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboCuentaBanco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(txtconcepto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed

        dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void jPanel3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);
    }//GEN-LAST:event_jPanel3MouseDragged

    private void jPanel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MousePressed

        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jPanel3MousePressed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void tablaconceptosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaconceptosMouseClicked

    }//GEN-LAST:event_tablaconceptosMouseClicked

    private void btnPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPagarActionPerformed

    void blanquear() {
        txtperiodo.setText("");
        txtfactura.setText("");
        txtimporte.setText("");
        txtconcepto.setText("");
        cboTipo.setSelectedIndex(0);
        cboForma.setSelectedIndex(0);
        cboCuentaBanco.setSelectedIndex(0);

    }

    void cargatotales() {
        double Debito = 0.00, sumatoria2 = 0.0, Credito = 0.00, sumatoria = 0.0;

        DecimalFormat df = new DecimalFormat("0000000000.00");
        int totalRow = tablaconceptos.getRowCount(), aux = 0;
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            String x1 = "0", x2 = "0";
            if (tablaconceptos.getValueAt(i, 4).toString().equals("Crédito")) {
                x1 = tablaconceptos.getValueAt(i, 2).toString();///suma por Obra Social                        
            }
            if (tablaconceptos.getValueAt(i, 4).toString().equals("Débito")) {
                x2 = tablaconceptos.getValueAt(i, 2).toString();/// suma por particular                
            }

            sumatoria = Double.valueOf(x1);
            sumatoria2 = Double.valueOf(x2);

            Credito = Credito + sumatoria;
            Debito = Debito + sumatoria2;
        }
        txttotal.setText((String.valueOf(Redondear(Credito - Debito))));

    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    private void txtconceptoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtconceptoActionPerformed
        DefaultTableModel temp = (DefaultTableModel) tablaconceptos.getModel();
        Object nuevo[] = {
            txtperiodo.getText(),
            txtfactura.getText(),
            txtimporte.getText(),
            txtconcepto.getText(),
            cboTipo.getSelectedItem().toString(),
            cboForma.getSelectedItem().toString(),
            cboCuentaBanco.getSelectedItem().toString()};
        temp.addRow(nuevo);
        ////////////////////////////////////////////////////////////////////
        tablaconceptos.getColumnModel().getColumn(3).setMaxWidth(0);
        tablaconceptos.getColumnModel().getColumn(3).setMinWidth(0);
        tablaconceptos.getColumnModel().getColumn(3).setPreferredWidth(0);
        ////////////////////////////////////////////////////////////////////
        tablaconceptos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablaconceptos.getColumnModel().getColumn(5).setMinWidth(0);
        tablaconceptos.getColumnModel().getColumn(5).setPreferredWidth(0);
        ////////////////////////////////////////////////////////////////////
        blanquear();
        cargatotales();
    }//GEN-LAST:event_txtconceptoActionPerformed

    private void txtimporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtimporteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtimporteActionPerformed

    private void cboCuentaBancoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCuentaBancoActionPerformed

    }//GEN-LAST:event_cboCuentaBancoActionPerformed

    private void txtobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialActionPerformed

    }//GEN-LAST:event_txtobrasocialActionPerformed

    private void txtobrasocialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtobrasocialKeyPressed

    private void txtobrasocialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtobrasocialKeyReleased

    private void txtperiodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtperiodoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtperiodoActionPerformed

    private void txtfacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfacturaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfacturaActionPerformed

    private void btnPagarComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_btnPagarComponentResized
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPagarComponentResized


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnPagar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnSalir;
    private RSMaterialComponent.RSComboBox cboCuentaBanco;
    private RSMaterialComponent.RSComboBox cboForma;
    private RSMaterialComponent.RSComboBox cboTipo;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane5;
    private RSMaterialComponent.RSTableMetroCustom tablaconceptos;
    private RSMaterialComponent.RSTextFieldOne txtconcepto;
    private RSMaterialComponent.RSTextFieldOne txtfactura;
    private RSMaterialComponent.RSTextFieldOne txtimporte;
    private RSMaterialComponent.RSTextFieldOne txtobrasocial;
    private RSMaterialComponent.RSTextFieldOne txtperiodo;
    private javax.swing.JLabel txttotal;
    // End of variables declaration//GEN-END:variables
}
