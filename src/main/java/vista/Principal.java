package vista;

import static controlador.HiloInicio.listaColegiados;
import static controlador.HiloInicio.listaLaboratorios;
import static controlador.HiloInicio.listaOS;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import modelo.Laboratorio;
import static vista.AgregarObraSocial.bandera_agrega;

public class Principal extends javax.swing.JFrame {

    int x, y;
    DefaultTableModel modelBioquimicos, modelLaboratorios, modelObraSocial;
    private modelo.ConexionMariaDB conexion = new modelo.ConexionMariaDB();
    public static int contadorprovincia;
    public static int contadorlocalidad;
    public static int banderacarga;
    public static String observacion;
    public static String[] nombreprovincia = new String[25];
    public static String[][] matloc = new String[2382][3];

    public Principal() {

        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/cbt logo.png")).getImage());
        setLocationRelativeTo(null);
        setResizable(false);
        panelColegiados.setVisible(true);
        panelLaboratorios.setVisible(false);
        panelObrasSociales.setVisible(false);
        panelFacturacion.setVisible(false);
        panelPami.setVisible(false);
        panelPagos.setVisible(false);
        panelContable.setVisible(false);
        btnColegiados.requestFocus();
        cargarTablaBioquimicos();

        txtBuscarBioquimico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtBuscarLaboratorio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtBuscarOs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

    }

    void cargarLaboratorios() {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaLaboratorios = new ArrayList<>();
        Laboratorio.cargarLaboratorio(conexion.getConnection(), listaLaboratorios);
        conexion.cerrarConexion();

    }

    void cargarTablaBioquimicos() {

        String[] Titulo = {"id", "Matricula", "Nombre", "Tipo"};
        String[] Registros = new String[4];

        modelBioquimicos = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (int i = 0; i < listaColegiados.size(); i++) {

            Registros[0] = String.valueOf(listaColegiados.get(i).getIdColegiados());
            Registros[1] = listaColegiados.get(i).getMatricula();
            Registros[2] = listaColegiados.get(i).getNombre();
            Registros[3] = listaColegiados.get(i).getTipoProfesional();
            modelBioquimicos.addRow(Registros);

        }
        tablaColegiados.setModel(modelBioquimicos);
        tablaColegiados.setAutoCreateRowSorter(true);

        tablaColegiados.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaColegiados.getColumnModel().getColumn(0).setMinWidth(0);
        tablaColegiados.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaColegiados.getColumnModel().getColumn(1).setPreferredWidth(60);
        tablaColegiados.getColumnModel().getColumn(2).setPreferredWidth(210);
        tablaColegiados.getColumnModel().getColumn(3).setPreferredWidth(60);

    }

    void cargarTablaLaboratorios() {
        String[] Titulo = {"id", "Laboratorio"};
        String[] Registros = new String[4];

        modelLaboratorios = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (int i = 0; i < listaLaboratorios.size(); i++) {

            Registros[0] = String.valueOf(listaLaboratorios.get(i).getIdLaboratorios());
            Registros[1] = listaLaboratorios.get(i).getNombre();

            modelLaboratorios.addRow(Registros);
        }
        tablaLaboratorios.setModel(modelLaboratorios);
        tablaLaboratorios.setAutoCreateRowSorter(true);
        tablaLaboratorios.getColumnModel().getColumn(0).setMinWidth(0);
        tablaLaboratorios.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaLaboratorios.getColumnModel().getColumn(0).setPreferredWidth(0);

    }

    void cargarTablaOS(String valor) {
        String[] Titulo = {"Codigo", "Razon Social", "Arancel", "Estado"};
        Object[] Registros = new Object[4];

        modelObraSocial = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (int i = 0; i < listaOS.size(); i++) {

            Registros[0] = String.valueOf(listaOS.get(i).getCodigo());
            Registros[1] = listaOS.get(i).getNombre();
            Registros[2] = listaOS.get(i).getImporteUnidadDeArancel();
            if (listaOS.get(i).getEstado() == 1) {
                Registros[3] = "Activo";
            } else {
                Registros[3] = "Baja";
            }
            modelObraSocial.addRow(Registros);
        }
        tablaOS.setModel(modelObraSocial);
        tablaOS.setAutoCreateRowSorter(true);
        tablaOS.getColumnModel().getColumn(0).setPreferredWidth(40);
        tablaOS.getColumnModel().getColumn(1).setPreferredWidth(200);
        tablaOS.getColumnModel().getColumn(2).setPreferredWidth(90);
        tablaOS.getColumnModel().getColumn(3).setPreferredWidth(90);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popuMenu1 = new rojeru_san.complementos.PopuMenu();
        Resultados = new javax.swing.JMenuItem();
        Modifica = new javax.swing.JMenuItem();
        Cargar = new javax.swing.JMenuItem();
        panelBotones = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btnMinimizar = new RSMaterialComponent.RSButtonIconOne();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();
        btnColegiados = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnLaboratorios = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnOS = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnFacturación = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnPami = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnPagos1 = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnContable = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnContable1 = new RSMaterialComponent.RSButtonMaterialIconUno();
        rSLabelIcon4 = new RSMaterialComponent.RSLabelIcon();
        btnProveeduria = new RSMaterialComponent.RSButtonMaterialIconUno();
        panelContenedor = new javax.swing.JPanel();
        panelColegiados = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        rSLabelIcon1 = new RSMaterialComponent.RSLabelIcon();
        txtBuscarBioquimico = new rojeru_san.rsfield.RSTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaColegiados = new RSMaterialComponent.RSTableMetroCustom();
        btnAgregar = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnModificar = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnCertificados = new RSMaterialComponent.RSButtonMaterialIconOne();
        panelLaboratorios = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        rSLabelIcon2 = new RSMaterialComponent.RSLabelIcon();
        txtBuscarLaboratorio = new rojeru_san.rsfield.RSTextField();
        btnABMLaboratorio = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnFacturacion = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnCuentas = new RSMaterialComponent.RSButtonMaterialIconOne();
        jScrollPane8 = new javax.swing.JScrollPane();
        tablaLaboratorios = new RSMaterialComponent.RSTableMetroCustom();
        panelObrasSociales = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        rSLabelIcon3 = new RSMaterialComponent.RSLabelIcon();
        txtBuscarOs = new rojeru_san.rsfield.RSTextField();
        btnAgregarOS = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnModificarOS = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnFacturarOS = new RSMaterialComponent.RSButtonMaterialIconOne();
        jScrollPane9 = new javax.swing.JScrollPane();
        tablaOS = new RSMaterialComponent.RSTableMetroCustom();
        btnFacturarOS1 = new RSMaterialComponent.RSButtonMaterialIconOne();
        panelFacturacion = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnGenerarFactura = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnRefacturar = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnArchivoOS = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnActualizarArancel = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnLiquidaciones = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnPracticasNBU = new RSMaterialComponent.RSButtonMaterialIconOne();
        jPanel7 = new javax.swing.JPanel();
        btnPeriodos = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnModificarPeriodo = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnRecepcion = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnAltaDiagnosticos = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnDDJJ = new RSMaterialComponent.RSButtonMaterialIconOne();
        jPanel8 = new javax.swing.JPanel();
        btnUsuarios = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnImportarArchivoOS = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnConsultaFacturacion = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnMostrarDebitos = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnAuditoriaPami = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnMostrarDebitos1 = new RSMaterialComponent.RSButtonMaterialIconOne();
        panelPami = new javax.swing.JPanel();
        panelPagos = new javax.swing.JPanel();
        btnListarPago = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnRegistrarPago = new RSMaterialComponent.RSButtonMaterialIconOne();
        panelContable = new javax.swing.JPanel();
        btnListarCuentas = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnTransferencias = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnMovimientosDeCaja = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnMovimientosDeCaja1 = new RSMaterialComponent.RSButtonMaterialIconOne();
        panelBlanco = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        popuMenu1.setBackground(new java.awt.Color(255, 255, 255));
        popuMenu1.setForeground(new java.awt.Color(0, 90, 132));
        popuMenu1.setColorBorde(new java.awt.Color(0, 90, 132));
        popuMenu1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        Resultados.setBackground(new java.awt.Color(255, 255, 255));
        Resultados.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Resultados.setForeground(new java.awt.Color(0, 90, 132));
        Resultados.setText("Cargar resultados");
        Resultados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResultadosActionPerformed(evt);
            }
        });
        popuMenu1.add(Resultados);

        Modifica.setBackground(new java.awt.Color(255, 255, 255));
        Modifica.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Modifica.setForeground(new java.awt.Color(0, 90, 132));
        Modifica.setText("Modificar orden");
        Modifica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificaActionPerformed(evt);
            }
        });
        popuMenu1.add(Modifica);

        Cargar.setBackground(new java.awt.Color(255, 255, 255));
        Cargar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Cargar.setForeground(new java.awt.Color(0, 90, 132));
        Cargar.setText("Cargar paciente");
        Cargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CargarActionPerformed(evt);
            }
        });
        popuMenu1.add(Cargar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setPreferredSize(null);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        panelBotones.setBackground(new java.awt.Color(66, 133, 200));
        panelBotones.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        panelBotones.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelBotones.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                panelBotonesMouseDragged(evt);
            }
        });
        panelBotones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                panelBotonesMousePressed(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel5MouseClicked(evt);
            }
        });

        btnMinimizar.setBackground(new java.awt.Color(66, 133, 200));
        btnMinimizar.setBackgroundHover(new java.awt.Color(204, 204, 204));
        btnMinimizar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.REMOVE);
        btnMinimizar.setRound(20);
        btnMinimizar.setSizeIcon(30.0F);
        btnMinimizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizarActionPerformed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(219, 68, 55));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLEAR);
        btnCerrar.setRound(20);
        btnCerrar.setSizeIcon(30.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnMinimizar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnMinimizar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnColegiados.setBackground(new java.awt.Color(255, 255, 255));
        btnColegiados.setForeground(new java.awt.Color(0, 0, 0));
        btnColegiados.setText("Colegiados");
        btnColegiados.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnColegiados.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnColegiados.setForegroundText(new java.awt.Color(51, 51, 51));
        btnColegiados.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.PERSON_PIN);
        btnColegiados.setRound(20);
        btnColegiados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnColegiadosActionPerformed(evt);
            }
        });

        btnLaboratorios.setBackground(new java.awt.Color(255, 255, 255));
        btnLaboratorios.setForeground(new java.awt.Color(0, 0, 0));
        btnLaboratorios.setText("Laboratorios");
        btnLaboratorios.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnLaboratorios.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnLaboratorios.setForegroundText(new java.awt.Color(51, 51, 51));
        btnLaboratorios.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.COLORIZE);
        btnLaboratorios.setRound(20);
        btnLaboratorios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLaboratoriosActionPerformed(evt);
            }
        });

        btnOS.setBackground(new java.awt.Color(255, 255, 255));
        btnOS.setForeground(new java.awt.Color(0, 0, 0));
        btnOS.setText("Obras Sociales");
        btnOS.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnOS.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnOS.setForegroundText(new java.awt.Color(51, 51, 51));
        btnOS.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.DOMAIN);
        btnOS.setRound(20);
        btnOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOSActionPerformed(evt);
            }
        });

        btnFacturación.setBackground(new java.awt.Color(255, 255, 255));
        btnFacturación.setForeground(new java.awt.Color(0, 0, 0));
        btnFacturación.setText("Facturación");
        btnFacturación.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnFacturación.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnFacturación.setForegroundText(new java.awt.Color(51, 51, 51));
        btnFacturación.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SETTINGS);
        btnFacturación.setRound(20);
        btnFacturación.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFacturaciónActionPerformed(evt);
            }
        });

        btnPami.setBackground(new java.awt.Color(255, 255, 255));
        btnPami.setForeground(new java.awt.Color(0, 0, 0));
        btnPami.setText("Pami");
        btnPami.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnPami.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnPami.setForegroundText(new java.awt.Color(51, 51, 51));
        btnPami.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ALBUM);
        btnPami.setRound(20);
        btnPami.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPamiActionPerformed(evt);
            }
        });

        btnPagos1.setBackground(new java.awt.Color(255, 255, 255));
        btnPagos1.setForeground(new java.awt.Color(0, 0, 0));
        btnPagos1.setText("Pagos");
        btnPagos1.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnPagos1.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnPagos1.setForegroundText(new java.awt.Color(51, 51, 51));
        btnPagos1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnPagos1.setRound(20);
        btnPagos1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagos1ActionPerformed(evt);
            }
        });

        btnContable.setBackground(new java.awt.Color(255, 255, 255));
        btnContable.setForeground(new java.awt.Color(0, 0, 0));
        btnContable.setText("Contable");
        btnContable.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnContable.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnContable.setForegroundText(new java.awt.Color(51, 51, 51));
        btnContable.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.DIALPAD);
        btnContable.setRound(20);
        btnContable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContableActionPerformed(evt);
            }
        });

        btnContable1.setBackground(new java.awt.Color(255, 255, 255));
        btnContable1.setForeground(new java.awt.Color(0, 0, 0));
        btnContable1.setText("Salir");
        btnContable1.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnContable1.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnContable1.setForegroundText(new java.awt.Color(51, 51, 51));
        btnContable1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnContable1.setRound(20);
        btnContable1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContable1ActionPerformed(evt);
            }
        });

        rSLabelIcon4.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelIcon4.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.GRAPHIC_EQ);
        rSLabelIcon4.setSizeIcon(120.0F);

        btnProveeduria.setBackground(new java.awt.Color(255, 255, 255));
        btnProveeduria.setForeground(new java.awt.Color(0, 0, 0));
        btnProveeduria.setText("Proveeduría");
        btnProveeduria.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnProveeduria.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnProveeduria.setForegroundText(new java.awt.Color(51, 51, 51));
        btnProveeduria.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.DONUT_LARGE);
        btnProveeduria.setRound(20);
        btnProveeduria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProveeduriaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBotonesLayout = new javax.swing.GroupLayout(panelBotones);
        panelBotones.setLayout(panelBotonesLayout);
        panelBotonesLayout.setHorizontalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBotonesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnProveeduria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnContable, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rSLabelIcon4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnColegiados, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                    .addComponent(btnLaboratorios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnOS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFacturación, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPami, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPagos1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnContable1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelBotonesLayout.setVerticalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(rSLabelIcon4, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnColegiados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnLaboratorios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFacturación, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnPami, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnPagos1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnContable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnProveeduria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(btnContable1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(panelBotones);

        panelContenedor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        panelContenedor.setLayout(new javax.swing.OverlayLayout(panelContenedor));

        panelColegiados.setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        rSLabelIcon1.setForeground(new java.awt.Color(66, 133, 200));
        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);

        txtBuscarBioquimico.setForeground(new java.awt.Color(66, 133, 200));
        txtBuscarBioquimico.setPhColor(new java.awt.Color(0, 51, 102));
        txtBuscarBioquimico.setPlaceholder("Buscar colegiado");
        txtBuscarBioquimico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarBioquimicoKeyReleased(evt);
            }
        });

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tablaColegiados.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaColegiados.setForeground(new java.awt.Color(255, 255, 255));
        tablaColegiados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaColegiados.setBackgoundHead(new java.awt.Color(66, 133, 200));
        tablaColegiados.setBackgoundHover(new java.awt.Color(66, 133, 200));
        tablaColegiados.setBorderHead(null);
        tablaColegiados.setBorderRows(null);
        tablaColegiados.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaColegiados.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaColegiados.setColorPrimaryText(new java.awt.Color(51, 51, 51));
        tablaColegiados.setColorSecondary(new java.awt.Color(227, 233, 230));
        tablaColegiados.setColorSecundaryText(new java.awt.Color(51, 51, 51));
        tablaColegiados.setGridColor(new java.awt.Color(255, 255, 255));
        tablaColegiados.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaColegiados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaColegiadosMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tablaColegiados);

        btnAgregar.setBackground(new java.awt.Color(66, 133, 200));
        btnAgregar.setText("Agregar colegiado");
        btnAgregar.setBackgroundHover(new java.awt.Color(204, 204, 204));
        btnAgregar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAgregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnAgregar.setRound(20);
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        btnModificar.setBackground(new java.awt.Color(66, 133, 200));
        btnModificar.setText("Modificar colegiado");
        btnModificar.setBackgroundHover(new java.awt.Color(204, 204, 204));
        btnModificar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnModificar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MODE_EDIT);
        btnModificar.setRound(20);
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnCertificados.setBackground(new java.awt.Color(66, 133, 200));
        btnCertificados.setText("Certificados");
        btnCertificados.setBackgroundHover(new java.awt.Color(204, 204, 204));
        btnCertificados.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnCertificados.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SCHOOL);
        btnCertificados.setRound(20);
        btnCertificados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCertificadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBuscarBioquimico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 365, Short.MAX_VALUE)
                                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnCertificados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtBuscarBioquimico, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 595, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnCertificados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout panelColegiadosLayout = new javax.swing.GroupLayout(panelColegiados);
        panelColegiados.setLayout(panelColegiadosLayout);
        panelColegiadosLayout.setHorizontalGroup(
            panelColegiadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelColegiadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelColegiadosLayout.setVerticalGroup(
            panelColegiadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelColegiadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelContenedor.add(panelColegiados);

        panelLaboratorios.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        rSLabelIcon2.setForeground(new java.awt.Color(66, 133, 200));
        rSLabelIcon2.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);

        txtBuscarLaboratorio.setForeground(new java.awt.Color(66, 133, 200));
        txtBuscarLaboratorio.setPhColor(new java.awt.Color(0, 51, 102));
        txtBuscarLaboratorio.setPlaceholder("Buscar laboratorio");
        txtBuscarLaboratorio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarLaboratorioKeyReleased(evt);
            }
        });

        btnABMLaboratorio.setBackground(new java.awt.Color(66, 133, 200));
        btnABMLaboratorio.setText("ABM Laboratorio");
        btnABMLaboratorio.setBackgroundHover(new java.awt.Color(204, 204, 204));
        btnABMLaboratorio.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnABMLaboratorio.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnABMLaboratorio.setRound(20);
        btnABMLaboratorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnABMLaboratorioActionPerformed(evt);
            }
        });

        btnFacturacion.setBackground(new java.awt.Color(66, 133, 200));
        btnFacturacion.setText("Acceso a facturación");
        btnFacturacion.setBackgroundHover(new java.awt.Color(204, 204, 204));
        btnFacturacion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnFacturacion.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnFacturacion.setRound(20);
        btnFacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFacturacionActionPerformed(evt);
            }
        });

        btnCuentas.setBackground(new java.awt.Color(66, 133, 200));
        btnCuentas.setText("Cuentas bancarias");
        btnCuentas.setBackgroundHover(new java.awt.Color(204, 204, 204));
        btnCuentas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnCuentas.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ACCOUNT_BALANCE);
        btnCuentas.setRound(20);
        btnCuentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCuentasActionPerformed(evt);
            }
        });

        jScrollPane8.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane8.setBorder(null);

        tablaLaboratorios.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaLaboratorios.setForeground(new java.awt.Color(255, 255, 255));
        tablaLaboratorios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaLaboratorios.setBackgoundHead(new java.awt.Color(66, 133, 200));
        tablaLaboratorios.setBackgoundHover(new java.awt.Color(66, 133, 200));
        tablaLaboratorios.setBorderHead(null);
        tablaLaboratorios.setBorderRows(null);
        tablaLaboratorios.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaLaboratorios.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaLaboratorios.setColorPrimaryText(new java.awt.Color(51, 51, 51));
        tablaLaboratorios.setColorSecondary(new java.awt.Color(227, 233, 230));
        tablaLaboratorios.setColorSecundaryText(new java.awt.Color(51, 51, 51));
        tablaLaboratorios.setGridColor(new java.awt.Color(255, 255, 255));
        tablaLaboratorios.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaLaboratorios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaLaboratoriosMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(tablaLaboratorios);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1011, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(rSLabelIcon2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBuscarLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, 568, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGap(0, 365, Short.MAX_VALUE)
                                .addComponent(btnABMLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnFacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtBuscarLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rSLabelIcon2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 593, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnABMLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout panelLaboratoriosLayout = new javax.swing.GroupLayout(panelLaboratorios);
        panelLaboratorios.setLayout(panelLaboratoriosLayout);
        panelLaboratoriosLayout.setHorizontalGroup(
            panelLaboratoriosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLaboratoriosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelLaboratoriosLayout.setVerticalGroup(
            panelLaboratoriosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLaboratoriosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelContenedor.add(panelLaboratorios);

        panelObrasSociales.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        rSLabelIcon3.setForeground(new java.awt.Color(66, 133, 200));
        rSLabelIcon3.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);

        txtBuscarOs.setForeground(new java.awt.Color(66, 133, 200));
        txtBuscarOs.setPhColor(new java.awt.Color(0, 51, 102));
        txtBuscarOs.setPlaceholder("Buscar Obra Social");
        txtBuscarOs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarOsKeyReleased(evt);
            }
        });

        btnAgregarOS.setBackground(new java.awt.Color(66, 133, 200));
        btnAgregarOS.setForeground(new java.awt.Color(0, 0, 0));
        btnAgregarOS.setText("Agregar Obra Social");
        btnAgregarOS.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnAgregarOS.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnAgregarOS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAgregarOS.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnAgregarOS.setRound(20);
        btnAgregarOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarOSActionPerformed(evt);
            }
        });

        btnModificarOS.setBackground(new java.awt.Color(66, 133, 200));
        btnModificarOS.setForeground(new java.awt.Color(0, 0, 0));
        btnModificarOS.setText("Modificar Obra Social");
        btnModificarOS.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnModificarOS.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnModificarOS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnModificarOS.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MODE_EDIT);
        btnModificarOS.setRound(20);
        btnModificarOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarOSActionPerformed(evt);
            }
        });

        btnFacturarOS.setBackground(new java.awt.Color(66, 133, 200));
        btnFacturarOS.setForeground(new java.awt.Color(0, 0, 0));
        btnFacturarOS.setText("Generar Recibo");
        btnFacturarOS.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnFacturarOS.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnFacturarOS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnFacturarOS.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnFacturarOS.setRound(20);
        btnFacturarOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFacturarOSActionPerformed(evt);
            }
        });

        jScrollPane9.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane9.setBorder(null);

        tablaOS.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaOS.setForeground(new java.awt.Color(255, 255, 255));
        tablaOS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaOS.setBackgoundHead(new java.awt.Color(66, 133, 200));
        tablaOS.setBackgoundHover(new java.awt.Color(66, 133, 200));
        tablaOS.setBorderHead(null);
        tablaOS.setBorderRows(null);
        tablaOS.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaOS.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaOS.setColorPrimaryText(new java.awt.Color(51, 51, 51));
        tablaOS.setColorSecondary(new java.awt.Color(227, 233, 230));
        tablaOS.setColorSecundaryText(new java.awt.Color(51, 51, 51));
        tablaOS.setGridColor(new java.awt.Color(255, 255, 255));
        tablaOS.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaOS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaOSMouseClicked(evt);
            }
        });
        jScrollPane9.setViewportView(tablaOS);

        btnFacturarOS1.setBackground(new java.awt.Color(66, 133, 200));
        btnFacturarOS1.setForeground(new java.awt.Color(0, 0, 0));
        btnFacturarOS1.setText("Facturar");
        btnFacturarOS1.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnFacturarOS1.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnFacturarOS1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnFacturarOS1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnFacturarOS1.setRound(20);
        btnFacturarOS1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFacturarOS1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 1011, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(rSLabelIcon3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBuscarOs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btnAgregarOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnModificarOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnFacturarOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnFacturarOS1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtBuscarOs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rSLabelIcon3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 593, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFacturarOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModificarOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFacturarOS1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout panelObrasSocialesLayout = new javax.swing.GroupLayout(panelObrasSociales);
        panelObrasSociales.setLayout(panelObrasSocialesLayout);
        panelObrasSocialesLayout.setHorizontalGroup(
            panelObrasSocialesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelObrasSocialesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelObrasSocialesLayout.setVerticalGroup(
            panelObrasSocialesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelObrasSocialesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelContenedor.add(panelObrasSociales);

        panelFacturacion.setBackground(new java.awt.Color(255, 255, 255));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        btnGenerarFactura.setBackground(new java.awt.Color(66, 133, 200));
        btnGenerarFactura.setText("Generar factura");
        btnGenerarFactura.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnGenerarFactura.setFont(new java.awt.Font("Roboto Bold", 1, 18)); // NOI18N
        btnGenerarFactura.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnGenerarFactura.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.DESCRIPTION);
        btnGenerarFactura.setPreferredSize(new java.awt.Dimension(100, 40));
        btnGenerarFactura.setRound(20);
        btnGenerarFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarFacturaActionPerformed(evt);
            }
        });

        btnRefacturar.setBackground(new java.awt.Color(66, 133, 200));
        btnRefacturar.setText("Refacturar");
        btnRefacturar.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnRefacturar.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnRefacturar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnRefacturar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.FLIP_TO_FRONT);
        btnRefacturar.setPreferredSize(new java.awt.Dimension(100, 40));
        btnRefacturar.setRound(20);
        btnRefacturar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefacturarActionPerformed(evt);
            }
        });

        btnArchivoOS.setBackground(new java.awt.Color(66, 133, 200));
        btnArchivoOS.setText("Archivo OS");
        btnArchivoOS.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnArchivoOS.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnArchivoOS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnArchivoOS.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.FOLDER);
        btnArchivoOS.setPreferredSize(new java.awt.Dimension(100, 40));
        btnArchivoOS.setRound(20);
        btnArchivoOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArchivoOSActionPerformed(evt);
            }
        });

        btnActualizarArancel.setBackground(new java.awt.Color(66, 133, 200));
        btnActualizarArancel.setText("Actualizar arancel");
        btnActualizarArancel.setToolTipText("");
        btnActualizarArancel.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnActualizarArancel.setFont(new java.awt.Font("Roboto Bold", 1, 18)); // NOI18N
        btnActualizarArancel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnActualizarArancel.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnActualizarArancel.setPreferredSize(new java.awt.Dimension(100, 40));
        btnActualizarArancel.setRound(20);
        btnActualizarArancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarArancelActionPerformed(evt);
            }
        });

        btnLiquidaciones.setBackground(new java.awt.Color(66, 133, 200));
        btnLiquidaciones.setText("Liquidaciones");
        btnLiquidaciones.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnLiquidaciones.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnLiquidaciones.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnLiquidaciones.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.LIST);
        btnLiquidaciones.setPreferredSize(new java.awt.Dimension(100, 40));
        btnLiquidaciones.setRound(20);
        btnLiquidaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLiquidacionesActionPerformed(evt);
            }
        });

        btnPracticasNBU.setBackground(new java.awt.Color(66, 133, 200));
        btnPracticasNBU.setText("Prácticas NBU");
        btnPracticasNBU.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnPracticasNBU.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnPracticasNBU.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnPracticasNBU.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.WB_IRIDESCENT);
        btnPracticasNBU.setPreferredSize(new java.awt.Dimension(100, 40));
        btnPracticasNBU.setRound(20);
        btnPracticasNBU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPracticasNBUActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnActualizarArancel, javax.swing.GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
                    .addComponent(btnGenerarFactura, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnArchivoOS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnRefacturar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnLiquidaciones, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPracticasNBU, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnActualizarArancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRefacturar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(btnGenerarFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnArchivoOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnLiquidaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnPracticasNBU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(345, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        btnPeriodos.setBackground(new java.awt.Color(66, 133, 200));
        btnPeriodos.setText("Periodo");
        btnPeriodos.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnPeriodos.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnPeriodos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnPeriodos.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ALARM);
        btnPeriodos.setPreferredSize(new java.awt.Dimension(100, 40));
        btnPeriodos.setRound(20);
        btnPeriodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPeriodosActionPerformed(evt);
            }
        });

        btnModificarPeriodo.setBackground(new java.awt.Color(66, 133, 200));
        btnModificarPeriodo.setText("Modificar periodo");
        btnModificarPeriodo.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnModificarPeriodo.setFont(new java.awt.Font("Roboto Bold", 1, 18)); // NOI18N
        btnModificarPeriodo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnModificarPeriodo.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ALARM_ON);
        btnModificarPeriodo.setPreferredSize(new java.awt.Dimension(100, 40));
        btnModificarPeriodo.setRound(20);
        btnModificarPeriodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarPeriodoActionPerformed(evt);
            }
        });

        btnRecepcion.setBackground(new java.awt.Color(66, 133, 200));
        btnRecepcion.setText("Recepción");
        btnRecepcion.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnRecepcion.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnRecepcion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnRecepcion.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MOVE_TO_INBOX);
        btnRecepcion.setPreferredSize(new java.awt.Dimension(100, 40));
        btnRecepcion.setRound(20);
        btnRecepcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecepcionActionPerformed(evt);
            }
        });

        btnAltaDiagnosticos.setBackground(new java.awt.Color(66, 133, 200));
        btnAltaDiagnosticos.setText("Alta diagnósticos");
        btnAltaDiagnosticos.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnAltaDiagnosticos.setFont(new java.awt.Font("Roboto Bold", 1, 18)); // NOI18N
        btnAltaDiagnosticos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAltaDiagnosticos.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD_CIRCLE);
        btnAltaDiagnosticos.setPreferredSize(new java.awt.Dimension(100, 40));
        btnAltaDiagnosticos.setRound(20);
        btnAltaDiagnosticos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAltaDiagnosticosActionPerformed(evt);
            }
        });

        btnDDJJ.setBackground(new java.awt.Color(66, 133, 200));
        btnDDJJ.setText("DDJJ");
        btnDDJJ.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnDDJJ.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnDDJJ.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnDDJJ.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.LINEAR_SCALE);
        btnDDJJ.setPreferredSize(new java.awt.Dimension(100, 40));
        btnDDJJ.setRound(20);
        btnDDJJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDDJJActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRecepcion, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE)
                    .addComponent(btnAltaDiagnosticos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnModificarPeriodo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPeriodos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDDJJ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnPeriodos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnModificarPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRecepcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnAltaDiagnosticos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDDJJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        btnUsuarios.setBackground(new java.awt.Color(66, 133, 200));
        btnUsuarios.setText("Usuarios");
        btnUsuarios.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnUsuarios.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnUsuarios.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnUsuarios.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ACCOUNT_CIRCLE);
        btnUsuarios.setPreferredSize(new java.awt.Dimension(100, 40));
        btnUsuarios.setRound(20);
        btnUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsuariosActionPerformed(evt);
            }
        });

        btnImportarArchivoOS.setBackground(new java.awt.Color(66, 133, 200));
        btnImportarArchivoOS.setText("Importar archivo OS");
        btnImportarArchivoOS.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnImportarArchivoOS.setFont(new java.awt.Font("Roboto Bold", 1, 18)); // NOI18N
        btnImportarArchivoOS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnImportarArchivoOS.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.IMPORT_EXPORT);
        btnImportarArchivoOS.setPreferredSize(new java.awt.Dimension(100, 40));
        btnImportarArchivoOS.setRound(20);
        btnImportarArchivoOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImportarArchivoOSActionPerformed(evt);
            }
        });

        btnConsultaFacturacion.setBackground(new java.awt.Color(66, 133, 200));
        btnConsultaFacturacion.setText("Facturación");
        btnConsultaFacturacion.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnConsultaFacturacion.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnConsultaFacturacion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnConsultaFacturacion.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.LOCAL_ATM);
        btnConsultaFacturacion.setPreferredSize(new java.awt.Dimension(100, 40));
        btnConsultaFacturacion.setRound(20);
        btnConsultaFacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultaFacturacionActionPerformed(evt);
            }
        });

        btnMostrarDebitos.setBackground(new java.awt.Color(66, 133, 200));
        btnMostrarDebitos.setText("Alta débitos");
        btnMostrarDebitos.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnMostrarDebitos.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnMostrarDebitos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnMostrarDebitos.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.VISIBILITY);
        btnMostrarDebitos.setPreferredSize(new java.awt.Dimension(100, 40));
        btnMostrarDebitos.setRound(20);
        btnMostrarDebitos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarDebitosActionPerformed(evt);
            }
        });

        btnAuditoriaPami.setBackground(new java.awt.Color(66, 133, 200));
        btnAuditoriaPami.setText("Auditoría PAMI");
        btnAuditoriaPami.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnAuditoriaPami.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnAuditoriaPami.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAuditoriaPami.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.WB_SUNNY);
        btnAuditoriaPami.setPreferredSize(new java.awt.Dimension(100, 40));
        btnAuditoriaPami.setRound(20);
        btnAuditoriaPami.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAuditoriaPamiActionPerformed(evt);
            }
        });

        btnMostrarDebitos1.setBackground(new java.awt.Color(66, 133, 200));
        btnMostrarDebitos1.setText("Mostrar débitos");
        btnMostrarDebitos1.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnMostrarDebitos1.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnMostrarDebitos1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnMostrarDebitos1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.VISIBILITY);
        btnMostrarDebitos1.setPreferredSize(new java.awt.Dimension(100, 40));
        btnMostrarDebitos1.setRound(20);
        btnMostrarDebitos1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarDebitos1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnUsuarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(btnImportarArchivoOS, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnConsultaFacturacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnMostrarDebitos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAuditoriaPami, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnMostrarDebitos1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnMostrarDebitos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnMostrarDebitos1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnImportarArchivoOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnConsultaFacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnAuditoriaPami, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout panelFacturacionLayout = new javax.swing.GroupLayout(panelFacturacion);
        panelFacturacion.setLayout(panelFacturacionLayout);
        panelFacturacionLayout.setHorizontalGroup(
            panelFacturacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFacturacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelFacturacionLayout.setVerticalGroup(
            panelFacturacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFacturacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelContenedor.add(panelFacturacion);

        panelPami.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout panelPamiLayout = new javax.swing.GroupLayout(panelPami);
        panelPami.setLayout(panelPamiLayout);
        panelPamiLayout.setHorizontalGroup(
            panelPamiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1041, Short.MAX_VALUE)
        );
        panelPamiLayout.setVerticalGroup(
            panelPamiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 732, Short.MAX_VALUE)
        );

        panelContenedor.add(panelPami);

        panelPagos.setBackground(new java.awt.Color(255, 255, 255));

        btnListarPago.setBackground(new java.awt.Color(66, 133, 200));
        btnListarPago.setText("Listar Pagos O.S.");
        btnListarPago.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnListarPago.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnListarPago.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnListarPago.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.VISIBILITY);
        btnListarPago.setPreferredSize(new java.awt.Dimension(100, 40));
        btnListarPago.setRound(20);
        btnListarPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListarPagoActionPerformed(evt);
            }
        });

        btnRegistrarPago.setBackground(new java.awt.Color(66, 133, 200));
        btnRegistrarPago.setText("Registrar Pago O.S.");
        btnRegistrarPago.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnRegistrarPago.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnRegistrarPago.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnRegistrarPago.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.VISIBILITY);
        btnRegistrarPago.setPreferredSize(new java.awt.Dimension(100, 40));
        btnRegistrarPago.setRound(20);
        btnRegistrarPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarPagoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelPagosLayout = new javax.swing.GroupLayout(panelPagos);
        panelPagos.setLayout(panelPagosLayout);
        panelPagosLayout.setHorizontalGroup(
            panelPagosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPagosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPagosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnRegistrarPago, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
                    .addComponent(btnListarPago, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(711, 711, 711))
        );
        panelPagosLayout.setVerticalGroup(
            panelPagosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPagosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnListarPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRegistrarPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(623, Short.MAX_VALUE))
        );

        panelContenedor.add(panelPagos);

        panelContable.setBackground(new java.awt.Color(255, 255, 255));

        btnListarCuentas.setBackground(new java.awt.Color(66, 133, 200));
        btnListarCuentas.setText("Cuentas");
        btnListarCuentas.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnListarCuentas.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnListarCuentas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnListarCuentas.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ACCOUNT_BALANCE);
        btnListarCuentas.setPreferredSize(new java.awt.Dimension(100, 40));
        btnListarCuentas.setRound(20);
        btnListarCuentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListarCuentasActionPerformed(evt);
            }
        });

        btnTransferencias.setBackground(new java.awt.Color(66, 133, 200));
        btnTransferencias.setText("Transferencias");
        btnTransferencias.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnTransferencias.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnTransferencias.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnTransferencias.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.AUTORENEW);
        btnTransferencias.setPreferredSize(new java.awt.Dimension(100, 40));
        btnTransferencias.setRound(20);
        btnTransferencias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTransferenciasActionPerformed(evt);
            }
        });

        btnMovimientosDeCaja.setBackground(new java.awt.Color(66, 133, 200));
        btnMovimientosDeCaja.setText("Movimientos de caja");
        btnMovimientosDeCaja.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnMovimientosDeCaja.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnMovimientosDeCaja.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnMovimientosDeCaja.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.WB_IRIDESCENT);
        btnMovimientosDeCaja.setPreferredSize(new java.awt.Dimension(100, 40));
        btnMovimientosDeCaja.setRound(20);
        btnMovimientosDeCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMovimientosDeCajaActionPerformed(evt);
            }
        });

        btnMovimientosDeCaja1.setBackground(new java.awt.Color(66, 133, 200));
        btnMovimientosDeCaja1.setText("Movimientos cuentas");
        btnMovimientosDeCaja1.setBackgroundHover(new java.awt.Color(51, 51, 51));
        btnMovimientosDeCaja1.setFont(new java.awt.Font("Roboto Bold", 1, 24)); // NOI18N
        btnMovimientosDeCaja1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnMovimientosDeCaja1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.AIRPLAY);
        btnMovimientosDeCaja1.setPreferredSize(new java.awt.Dimension(100, 40));
        btnMovimientosDeCaja1.setRound(20);
        btnMovimientosDeCaja1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMovimientosDeCaja1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelContableLayout = new javax.swing.GroupLayout(panelContable);
        panelContable.setLayout(panelContableLayout);
        panelContableLayout.setHorizontalGroup(
            panelContableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelContableLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelContableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnTransferencias, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                    .addComponent(btnListarCuentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnMovimientosDeCaja, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                    .addComponent(btnMovimientosDeCaja1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(716, Short.MAX_VALUE))
        );
        panelContableLayout.setVerticalGroup(
            panelContableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelContableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnMovimientosDeCaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnMovimientosDeCaja1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnListarCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnTransferencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(507, Short.MAX_VALUE))
        );

        panelContenedor.add(panelContable);

        panelBlanco.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cbt2.png"))); // NOI18N

        javax.swing.GroupLayout panelBlancoLayout = new javax.swing.GroupLayout(panelBlanco);
        panelBlanco.setLayout(panelBlancoLayout);
        panelBlancoLayout.setHorizontalGroup(
            panelBlancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBlancoLayout.createSequentialGroup()
                .addContainerGap(406, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(387, 387, 387))
        );
        panelBlancoLayout.setVerticalGroup(
            panelBlancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBlancoLayout.createSequentialGroup()
                .addGap(188, 188, 188)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(188, Short.MAX_VALUE))
        );

        panelContenedor.add(panelBlanco);

        getContentPane().add(panelContenedor);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void panelBotonesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelBotonesMousePressed

        x = evt.getX();
        y = evt.getY();

    }//GEN-LAST:event_panelBotonesMousePressed

    private void panelBotonesMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelBotonesMouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);

    }//GEN-LAST:event_panelBotonesMouseDragged

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed

        System.exit(0);

    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnMinimizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizarActionPerformed

        this.setExtendedState(ICONIFIED);

    }//GEN-LAST:event_btnMinimizarActionPerformed

    private void CargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CargarActionPerformed


    }//GEN-LAST:event_CargarActionPerformed

    private void ResultadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResultadosActionPerformed


    }//GEN-LAST:event_ResultadosActionPerformed

    private void ModificaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificaActionPerformed


    }//GEN-LAST:event_ModificaActionPerformed

    private void tablaColegiadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaColegiadosMouseClicked


    }//GEN-LAST:event_tablaColegiadosMouseClicked

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed

        new DatosColegiadoAgrega(this, true).setVisible(true);

    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed

        new BuscaModificaColegiado(this, true).setVisible(true);

    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnCertificadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCertificadosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCertificadosActionPerformed

    private void btnABMLaboratorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnABMLaboratorioActionPerformed

        new DatosLaboratorioAgrega(this, true).setVisible(true);

    }//GEN-LAST:event_btnABMLaboratorioActionPerformed

    private void btnFacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacturacionActionPerformed

        new AccesoFacturacion(this, true).setVisible(true);

    }//GEN-LAST:event_btnFacturacionActionPerformed

    private void btnCuentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCuentasActionPerformed

        new CuentasBancarias(this, true).setVisible(true);

    }//GEN-LAST:event_btnCuentasActionPerformed

    private void btnAgregarOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarOSActionPerformed

        if (banderacarga == 0) {
            /*cargaprovincia();
            cargalocalidad();*/
            banderacarga = 1;
        }
        new AgregarObraSocial(this, true).setVisible(true);

    }//GEN-LAST:event_btnAgregarOSActionPerformed

    private void btnModificarOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarOSActionPerformed

        bandera_agrega = 2;
        new Tabla_PracticasNBU(null, true).setVisible(true);

    }//GEN-LAST:event_btnModificarOSActionPerformed

    private void btnFacturarOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacturarOSActionPerformed

        new ReciboOS(this, true).setVisible(true);

    }//GEN-LAST:event_btnFacturarOSActionPerformed

    private void btnUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsuariosActionPerformed

        new Usuarios(this, true).setVisible(true);

    }//GEN-LAST:event_btnUsuariosActionPerformed

    private void btnPeriodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPeriodosActionPerformed

        new Periodo(this, true).setVisible(true);

    }//GEN-LAST:event_btnPeriodosActionPerformed

    private void btnGenerarFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarFacturaActionPerformed

        new Factura(this, true).setVisible(true);

    }//GEN-LAST:event_btnGenerarFacturaActionPerformed

    private void btnModificarPeriodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarPeriodoActionPerformed

        new PeriodoColegiado(null, true).setVisible(true);

    }//GEN-LAST:event_btnModificarPeriodoActionPerformed

    private void btnRefacturarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefacturarActionPerformed

        new Refactura(this, true).setVisible(true);

    }//GEN-LAST:event_btnRefacturarActionPerformed

    private void btnImportarArchivoOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportarArchivoOSActionPerformed

        new ImportarOS(this, true).setVisible(true);

    }//GEN-LAST:event_btnImportarArchivoOSActionPerformed

    private void btnArchivoOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArchivoOSActionPerformed

        new ArchivoObraSociales(this, true).setVisible(true);

    }//GEN-LAST:event_btnArchivoOSActionPerformed

    private void btnActualizarArancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarArancelActionPerformed

        new Precio_obrasocial(this, true).setVisible(true);

    }//GEN-LAST:event_btnActualizarArancelActionPerformed

    private void btnAltaDiagnosticosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAltaDiagnosticosActionPerformed

        new AgregaDiagnosticos(this, true).setVisible(true);

    }//GEN-LAST:event_btnAltaDiagnosticosActionPerformed

    private void btnRecepcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecepcionActionPerformed

        new RecepcionObrasSociales(this, true).setVisible(true);

    }//GEN-LAST:event_btnRecepcionActionPerformed

    private void btnListarCuentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListarCuentasActionPerformed

        new Cuentas(null, true).setVisible(true);

    }//GEN-LAST:event_btnListarCuentasActionPerformed

    private void tablaLaboratoriosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaLaboratoriosMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaLaboratoriosMouseClicked

    private void tablaOSMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaOSMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaOSMouseClicked

    private void btnTransferenciasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTransferenciasActionPerformed

        new TransferenciasBioquimicos(this, true).setVisible(true);

    }//GEN-LAST:event_btnTransferenciasActionPerformed

    private void btnLiquidacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLiquidacionesActionPerformed

        new Liquidaciones(this, true).setVisible(true);

    }//GEN-LAST:event_btnLiquidacionesActionPerformed

    private void btnDDJJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDDJJActionPerformed

        new DeclaracionJurada(this, true).setVisible(true);

    }//GEN-LAST:event_btnDDJJActionPerformed

    private void btnConsultaFacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultaFacturacionActionPerformed

        new Facturacion_Colegiados(this, true).setVisible(true);

    }//GEN-LAST:event_btnConsultaFacturacionActionPerformed

    private void btnPracticasNBUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPracticasNBUActionPerformed

        bandera_agrega = 0;
        new Tabla_PracticasNBU(this, true).setVisible(true);

    }//GEN-LAST:event_btnPracticasNBUActionPerformed

    private void btnMovimientosDeCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMovimientosDeCajaActionPerformed

        new CajaDiaria(this, true).setVisible(true);

    }//GEN-LAST:event_btnMovimientosDeCajaActionPerformed

    private void btnColegiadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnColegiadosActionPerformed

        panelColegiados.setVisible(true);
        panelLaboratorios.setVisible(false);
        panelObrasSociales.setVisible(false);
        panelFacturacion.setVisible(false);
        panelPami.setVisible(false);
        panelPagos.setVisible(false);
        panelContable.setVisible(false);
        cargarTablaBioquimicos();

    }//GEN-LAST:event_btnColegiadosActionPerformed

    private void btnMostrarDebitosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarDebitosActionPerformed

        new BuscaColegiadoDebito(this, true).setVisible(true);

    }//GEN-LAST:event_btnMostrarDebitosActionPerformed

    private void btnLaboratoriosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLaboratoriosActionPerformed

        panelColegiados.setVisible(false);
        panelLaboratorios.setVisible(true);
        panelObrasSociales.setVisible(false);
        panelFacturacion.setVisible(false);
        panelPami.setVisible(false);
        panelPagos.setVisible(false);
        panelContable.setVisible(false);
        cargarLaboratorios();
        cargarTablaLaboratorios();

    }//GEN-LAST:event_btnLaboratoriosActionPerformed

    private void btnOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOSActionPerformed

        panelColegiados.setVisible(false);
        panelLaboratorios.setVisible(false);
        panelObrasSociales.setVisible(true);
        panelFacturacion.setVisible(false);
        panelPami.setVisible(false);
        panelPagos.setVisible(false);
        panelContable.setVisible(false);
        cargarTablaOS("");

    }//GEN-LAST:event_btnOSActionPerformed

    private void btnFacturaciónActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacturaciónActionPerformed

        panelColegiados.setVisible(false);
        panelLaboratorios.setVisible(false);
        panelObrasSociales.setVisible(false);
        panelFacturacion.setVisible(true);
        panelPami.setVisible(false);
        panelPagos.setVisible(false);
        panelContable.setVisible(false);

    }//GEN-LAST:event_btnFacturaciónActionPerformed

    private void btnPamiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPamiActionPerformed

        new MainPami().setVisible(true);

    }//GEN-LAST:event_btnPamiActionPerformed

    private void btnPagos1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagos1ActionPerformed

        panelColegiados.setVisible(false);
        panelLaboratorios.setVisible(false);
        panelObrasSociales.setVisible(false);
        panelFacturacion.setVisible(false);
        panelPami.setVisible(false);
        panelPagos.setVisible(true);
        panelContable.setVisible(false);

    }//GEN-LAST:event_btnPagos1ActionPerformed

    private void btnContableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContableActionPerformed

        panelColegiados.setVisible(false);
        panelLaboratorios.setVisible(false);
        panelObrasSociales.setVisible(false);
        panelFacturacion.setVisible(false);
        panelPami.setVisible(false);
        panelPagos.setVisible(false);
        panelContable.setVisible(true);

    }//GEN-LAST:event_btnContableActionPerformed

    private void btnContable1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContable1ActionPerformed

        System.exit(0);

    }//GEN-LAST:event_btnContable1ActionPerformed

    private void btnListarPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListarPagoActionPerformed
        panelColegiados.setVisible(false);
        panelLaboratorios.setVisible(false);
        panelObrasSociales.setVisible(false);
        panelFacturacion.setVisible(false);
        panelPami.setVisible(false);
        panelPagos.setVisible(false);
        panelContable.setVisible(false);
        panelBlanco.setVisible(true);
        new PagoObraSocial(this, true).setVisible(true);
        panelPagos.setVisible(true);
    }//GEN-LAST:event_btnListarPagoActionPerformed

    private void btnRegistrarPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarPagoActionPerformed
        panelColegiados.setVisible(false);
        panelLaboratorios.setVisible(false);
        panelObrasSociales.setVisible(false);
        panelFacturacion.setVisible(false);
        panelPami.setVisible(false);
        panelPagos.setVisible(false);
        panelContable.setVisible(false);
        panelBlanco.setVisible(true);
        new ComprobantesPagoObraSocial(this, true).setVisible(true);
        panelPagos.setVisible(true);
    }//GEN-LAST:event_btnRegistrarPagoActionPerformed

    private void btnMovimientosDeCaja1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMovimientosDeCaja1ActionPerformed

        new MovimientoDeCuentas(this, true).setVisible(true);

    }//GEN-LAST:event_btnMovimientosDeCaja1ActionPerformed

    private void btnAuditoriaPamiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAuditoriaPamiActionPerformed

        new AuditoriaPami(this, true).setVisible(true);

    }//GEN-LAST:event_btnAuditoriaPamiActionPerformed

    private void txtBuscarBioquimicoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarBioquimicoKeyReleased
        TableRowSorter sorter = new TableRowSorter(modelBioquimicos);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtBuscarBioquimico.getText() + ".*"));
        tablaColegiados.setRowSorter(sorter);
    }//GEN-LAST:event_txtBuscarBioquimicoKeyReleased

    private void txtBuscarLaboratorioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarLaboratorioKeyReleased

        TableRowSorter sorter = new TableRowSorter(modelLaboratorios);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtBuscarLaboratorio.getText() + ".*"));
        tablaLaboratorios.setRowSorter(sorter);

    }//GEN-LAST:event_txtBuscarLaboratorioKeyReleased

    private void txtBuscarOsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarOsKeyReleased

        TableRowSorter sorter = new TableRowSorter(modelObraSocial);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtBuscarOs.getText() + ".*"));
        tablaOS.setRowSorter(sorter);

    }//GEN-LAST:event_txtBuscarOsKeyReleased

    private void btnProveeduriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProveeduriaActionPerformed


    }//GEN-LAST:event_btnProveeduriaActionPerformed

    private void jPanel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel5MouseClicked

    private void btnFacturarOS1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacturarOS1ActionPerformed
        new FacturaOS(this, true).setVisible(true);
    }//GEN-LAST:event_btnFacturarOS1ActionPerformed

    private void btnMostrarDebitos1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarDebitos1ActionPerformed
        new MostrarDebitos(this, true).setVisible(true);
    }//GEN-LAST:event_btnMostrarDebitos1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Cargar;
    private javax.swing.JMenuItem Modifica;
    private javax.swing.JMenuItem Resultados;
    private RSMaterialComponent.RSButtonMaterialIconOne btnABMLaboratorio;
    private RSMaterialComponent.RSButtonMaterialIconOne btnActualizarArancel;
    private RSMaterialComponent.RSButtonMaterialIconOne btnAgregar;
    private RSMaterialComponent.RSButtonMaterialIconOne btnAgregarOS;
    private RSMaterialComponent.RSButtonMaterialIconOne btnAltaDiagnosticos;
    private RSMaterialComponent.RSButtonMaterialIconOne btnArchivoOS;
    private RSMaterialComponent.RSButtonMaterialIconOne btnAuditoriaPami;
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconOne btnCertificados;
    private RSMaterialComponent.RSButtonMaterialIconUno btnColegiados;
    private RSMaterialComponent.RSButtonMaterialIconOne btnConsultaFacturacion;
    private RSMaterialComponent.RSButtonMaterialIconUno btnContable;
    private RSMaterialComponent.RSButtonMaterialIconUno btnContable1;
    private RSMaterialComponent.RSButtonMaterialIconOne btnCuentas;
    private RSMaterialComponent.RSButtonMaterialIconOne btnDDJJ;
    private RSMaterialComponent.RSButtonMaterialIconOne btnFacturacion;
    private RSMaterialComponent.RSButtonMaterialIconUno btnFacturación;
    private RSMaterialComponent.RSButtonMaterialIconOne btnFacturarOS;
    private RSMaterialComponent.RSButtonMaterialIconOne btnFacturarOS1;
    private RSMaterialComponent.RSButtonMaterialIconOne btnGenerarFactura;
    private RSMaterialComponent.RSButtonMaterialIconOne btnImportarArchivoOS;
    private RSMaterialComponent.RSButtonMaterialIconUno btnLaboratorios;
    private RSMaterialComponent.RSButtonMaterialIconOne btnLiquidaciones;
    private RSMaterialComponent.RSButtonMaterialIconOne btnListarCuentas;
    private RSMaterialComponent.RSButtonMaterialIconOne btnListarPago;
    private RSMaterialComponent.RSButtonIconOne btnMinimizar;
    private RSMaterialComponent.RSButtonMaterialIconOne btnModificar;
    private RSMaterialComponent.RSButtonMaterialIconOne btnModificarOS;
    private RSMaterialComponent.RSButtonMaterialIconOne btnModificarPeriodo;
    private RSMaterialComponent.RSButtonMaterialIconOne btnMostrarDebitos;
    private RSMaterialComponent.RSButtonMaterialIconOne btnMostrarDebitos1;
    private RSMaterialComponent.RSButtonMaterialIconOne btnMovimientosDeCaja;
    private RSMaterialComponent.RSButtonMaterialIconOne btnMovimientosDeCaja1;
    private RSMaterialComponent.RSButtonMaterialIconUno btnOS;
    private RSMaterialComponent.RSButtonMaterialIconUno btnPagos1;
    private RSMaterialComponent.RSButtonMaterialIconUno btnPami;
    private RSMaterialComponent.RSButtonMaterialIconOne btnPeriodos;
    private RSMaterialComponent.RSButtonMaterialIconOne btnPracticasNBU;
    private RSMaterialComponent.RSButtonMaterialIconUno btnProveeduria;
    private RSMaterialComponent.RSButtonMaterialIconOne btnRecepcion;
    private RSMaterialComponent.RSButtonMaterialIconOne btnRefacturar;
    private RSMaterialComponent.RSButtonMaterialIconOne btnRegistrarPago;
    private RSMaterialComponent.RSButtonMaterialIconOne btnTransferencias;
    private RSMaterialComponent.RSButtonMaterialIconOne btnUsuarios;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JPanel panelBlanco;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelColegiados;
    private javax.swing.JPanel panelContable;
    private javax.swing.JPanel panelContenedor;
    private javax.swing.JPanel panelFacturacion;
    private javax.swing.JPanel panelLaboratorios;
    private javax.swing.JPanel panelObrasSociales;
    public static javax.swing.JPanel panelPagos;
    private javax.swing.JPanel panelPami;
    private rojeru_san.complementos.PopuMenu popuMenu1;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon1;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon2;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon3;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon4;
    private RSMaterialComponent.RSTableMetroCustom tablaColegiados;
    private RSMaterialComponent.RSTableMetroCustom tablaLaboratorios;
    private RSMaterialComponent.RSTableMetroCustom tablaOS;
    private rojeru_san.rsfield.RSTextField txtBuscarBioquimico;
    private rojeru_san.rsfield.RSTextField txtBuscarLaboratorio;
    private rojeru_san.rsfield.RSTextField txtBuscarOs;
    // End of variables declaration//GEN-END:variables
}
