package vista;


import controlador.ConexionMariaDB;
import controlador.ConexionMySQLBackup;
import controlador.Funciones;
import static vista.Facturacion_Colegiados.periododjj;
import java.awt.Cursor;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Detalle_Practicas extends javax.swing.JDialog {

    DefaultTableModel model;
    public static String id_orden, afiliado, obrasocial, fecha, total, num_orden,dni,observacion;

    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    HiloOrdenes hilo;
    
    public Detalle_Practicas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);        
        txtorden.setText(num_orden);
        txtafiliado.setText(afiliado);
        txtobra.setText(obrasocial);
        txtfecha.setText(fecha);
        txttotal.setText(total);
        txtdni.setText(dni);
        txtobservaciones.setText("Observaciones: "+observacion);
        Funciones.funcionescape(this);
        this.setResizable(false);
        hilo = new HiloOrdenes(null);
        hilo.start();
        hilo = null;
        ///cargartabla(id_orden);
    }

    void cargartotales() {
        double total1 = 0.00, desc, sumatoria;

        //AQUI SE SUMAN LOS VALORES DE LA COLUMNAS
        int totalRow = tablapracticas.getRowCount();
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            String x = tablapracticas.getValueAt(i, 2).toString();
            sumatoria = Double.valueOf(x).doubleValue();
            total1 = total1 + sumatoria;
        }

        txttotal.setText(String.valueOf(Redondear(total1)));
    }

    
    void cursor() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        /* this.btnCursor02.setCursor(new Cursor(Cursor.HAND_CURSOR));
         this.btnCursor03.setCursor(new Cursor(Cursor.MOVE_CURSOR));
         this.btnCursor04.setCursor(new Cursor(Cursor.TEXT_CURSOR));*/

        //this.add(this.btnimprimir1);
        /* this.add(this.btnCursor02);
         this.add(this.btnCursor03);
         this.add(this.btnCursor04);*/
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cursor2() {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        /* this.btnCursor02.setCursor(new Cursor(Cursor.HAND_CURSOR));
         this.btnCursor03.setCursor(new Cursor(Cursor.MOVE_CURSOR));
         this.btnCursor04.setCursor(new Cursor(Cursor.TEXT_CURSOR));*/

        //this.add(this.btnimprimir1);
        /* this.add(this.btnCursor02);
         this.add(this.btnCursor03);
         this.add(this.btnCursor04);*/
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.pack();
    }
    
    public class HiloOrdenes extends Thread {

        JProgressBar progreso;

        public HiloOrdenes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            cursor();
            String[] Titulo = {"Cod", "Practica", "Precio"};
            String[] Registros = new String[3];

            String sql = "SELECT cod_practica,nombre_practica,precio_practica FROM detalle_ordenes Where id_orden=" + id_orden + " AND estado=" + 0;
            model = new DefaultTableModel(null, Titulo) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
//            if (Integer.valueOf(periododjj) >= 201604) {
                ConexionMariaDB cc = new ConexionMariaDB();
                Connection cn = cc.Conectar();
                try {
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);
                    while (rs.next()) {
                        Registros[0] = rs.getString(1);
                        Registros[1] = rs.getString(2);
                        Registros[2] = rs.getString(3);
                        model.addRow(Registros);

                    }
                    tablapracticas.setModel(model);
                //  tablapracticas.setAutoCreateRowSorter(true);
                    /////ajustar ancho de columna///////
                    tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(50);
                    tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(100);
                    tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(50);

                    /////////////////////////////////////////////////////////////*/
                    alinear();
                    tablapracticas.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                    tablapracticas.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                    tablapracticas.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                    //////////////////////////////////////////////////////////////////
                    cursor2();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
//            } else {
//                ConexionMySQLBackup cc = new ConexionMySQLBackup();
//                Connection cn2 = cc.Conectar();
//                try {
//                    Statement st = cn2.createStatement();
//                    ResultSet rs = st.executeQuery(sql);
//                    while (rs.next()) {
//                        Registros[0] = rs.getString(1);
//                        Registros[1] = rs.getString(2);
//                        Registros[2] = rs.getString(3);
//                        model.addRow(Registros);
//
//                    }
//                    tablapracticas.setModel(model);
//                //  tablapracticas.setAutoCreateRowSorter(true);
//                    /////ajustar ancho de columna///////
//                    tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(50);
//                    tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(100);
//                    tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(50);
//
//                    /////////////////////////////////////////////////////////////*/
//                    alinear();
//                    tablapracticas.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
//                    tablapracticas.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
//                    tablapracticas.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
//                    //////////////////////////////////////////////////////////////////
//                    cursor2();
//                } catch (SQLException ex) {
//                    JOptionPane.showMessageDialog(null, ex);
//                }
//            }

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }
    
    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    void cargartabla(String valor) {
        String[] Titulo = {"Cod", "Practica", "Precio"};
        String[] Registros = new String[3];

        String sql = "SELECT cod_practica,nombre_practica,precio_practica FROM detalle_ordenes Where id_orden=" + valor + " AND estado=" + 0;
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                    Registros[0] = rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    model.addRow(Registros);
                
            }
            tablapracticas.setModel(model);
            //  tablapracticas.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
            tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(100);
            tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(50);

            /////////////////////////////////////////////////////////////*/
            alinear();
            tablapracticas.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
            tablapracticas.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablapracticas.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
            //////////////////////////////////////////////////////////////////

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);

        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);

        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablapracticas = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtorden = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtafiliado = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txttotal = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtfecha = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtobra = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtdni = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtobservaciones = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Practicas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(153, 153, 153))); // NOI18N
        jPanel1.setToolTipText("");

        tablapracticas.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablapracticas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablapracticas);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("N° de Orden: ");

        txtorden.setEditable(false);
        txtorden.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtorden.setForeground(new java.awt.Color(0, 102, 204));
        txtorden.setBorder(null);
        txtorden.setOpaque(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Afiliado:");

        txtafiliado.setEditable(false);
        txtafiliado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtafiliado.setForeground(new java.awt.Color(0, 102, 204));
        txtafiliado.setBorder(null);
        txtafiliado.setOpaque(false);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Total:");

        txttotal.setEditable(false);
        txttotal.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txttotal.setForeground(new java.awt.Color(0, 102, 204));
        txttotal.setBorder(null);
        txttotal.setOpaque(false);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("$");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Fecha:");

        txtfecha.setEditable(false);
        txtfecha.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtfecha.setForeground(new java.awt.Color(0, 102, 204));
        txtfecha.setBorder(null);
        txtfecha.setOpaque(false);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Obra Social:");

        txtobra.setEditable(false);
        txtobra.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtobra.setForeground(new java.awt.Color(0, 102, 204));
        txtobra.setBorder(null);
        txtobra.setOpaque(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Documento:");

        txtdni.setEditable(false);
        txtdni.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdni.setForeground(new java.awt.Color(0, 102, 204));
        txtdni.setBorder(null);
        txtdni.setOpaque(false);

        txtobservaciones.setColumns(20);
        txtobservaciones.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        txtobservaciones.setForeground(new java.awt.Color(0, 102, 204));
        txtobservaciones.setRows(5);
        jScrollPane2.setViewportView(txtobservaciones);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtobra))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtorden, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdni)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtobra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(txtdni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(txtorden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tablapracticas;
    private javax.swing.JTextField txtafiliado;
    private javax.swing.JTextField txtdni;
    private javax.swing.JTextField txtfecha;
    private javax.swing.JTextField txtobra;
    private javax.swing.JTextArea txtobservaciones;
    private javax.swing.JTextField txtorden;
    private javax.swing.JTextField txttotal;
    // End of variables declaration//GEN-END:variables
}
