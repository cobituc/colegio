package vista;

import controlador.CustomListModel;
import controlador.Funciones;
import static controlador.HiloInicio.listaLocalidades;
import controlador.UtilToDate;
import com.mxrck.autocompleter.TextAutoCompleter;
import controlador.FuncionesTabla;
import static controlador.FuncionesTabla.alinear;
import static controlador.FuncionesTabla.alinearCentro;
import static controlador.FuncionesTabla.alinearIzquierda;
import static controlador.HiloInicio.listaColegiados;
import static controlador.HiloInicio.listaLaboratorios;
import java.awt.Color;
import java.awt.Image;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import static vista.BuscaColegiado.c;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;
import modelo.Colegiado;
import modelo.ColegiadoLaboratorio;
import modelo.Laboratorio;
import modelo.Localidad;
import static modelo.Localidad.buscarLocalidad;

public class DatosLaboratorioAgrega extends javax.swing.JDialog {

    DefaultTableModel modelo;
    CustomListModel list_model_bioq = new CustomListModel();
    CustomListModel list_model_direc = new CustomListModel();
    CustomListModel list_model_fact = new CustomListModel();
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    ArrayList<ColegiadoLaboratorio> listaColegiadoLaboratorio = new ArrayList<>();
    String pic = "";
    int id_colegiado = 0;
    Image imagen2;
    TextAutoCompleter textAutoAcompleter;
    Funciones efechas = new Funciones();
    String essociedad = "", factura = "", atravescolegio = "";
    Laboratorio lab;
    DefaultListModel<String> modeloBioquimico = new DefaultListModel<>();
    DefaultListModel<String> modeloDirector = new DefaultListModel<>();
    DefaultListModel<String> modeloFacturante = new DefaultListModel<>();

    private modelo.ConexionMariaDB conexion = new modelo.ConexionMariaDB();

    public DatosLaboratorioAgrega(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        listaBioquimicos.setModel(list_model_bioq);
        listaDt.setModel(list_model_direc);
        listaFacturante.setModel(list_model_fact);
        setTitle("Agregar laboratorio");
        setLocationRelativeTo(null);
        textAutoAcompleter = new TextAutoCompleter(txtlocalidadlab);
        textAutoAcompleter.addItems(listaLocalidades.toArray());
        textAutoAcompleter.setCaseSensitive(false);
        textAutoAcompleter.setMode(0);
        cargarLaboratorios();
        cargartablaNuevo();
        

        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtdireccionlab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtlocalidadlab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

    }

    boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
    
    void cargarLaboratorios(){
        
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaLaboratorios = new ArrayList<>();
        Laboratorio.cargarLaboratorio(conexion.getConnection(), listaLaboratorios);
        conexion.cerrarConexion();
        
    }

    void cargarcolegiado() {
        String sSQL = "";
        controlador.ConexionMariaDB mysql = new controlador.ConexionMariaDB();
        Connection cn = mysql.Conectar();

        sSQL = "SELECT MAX(id_colegiados) AS id_colegiados FROM colegiados";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            if (rs.getInt("id_colegiados") != 0) {
                id_colegiado = rs.getInt("id_colegiados");
            } else {
                id_colegiado = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    private Image ConvertirImagen(byte[] bytes) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName("png");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        return reader.read(0, param);
    }

    void traerimagen() {
        controlador.ConexionMariaDB mysql = new controlador.ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String sql7 = "Select foto_particular from colegiados where id_colegiados=" + id_colegiado;
        try {
            Statement St7 = cn.createStatement();
            ResultSet rs7 = St7.executeQuery(sql7);
            while (rs7.next()) {
                byte[] img = rs7.getBytes(1);
                if (img != null) {
                    try {
                        imagen2 = ConvertirImagen(img);
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                } else {
                    imagen2 = null;
                }
            }
        } catch (HeadlessException | SQLException ex) {
            JOptionPane.showMessageDialog(null, ex + "\n Los Datos se insertaron erroneamente...");
        }
    }

    void aplicarCambios() {

        Laboratorio lab = new Laboratorio();
        lab.setNombre(txtNombre.getText());
        if (!txtCuit.getText().equals("")) {
            lab.setCuit(Long.parseLong(txtCuit.getText()));
        }
        lab.setDireccion(txtdireccionlab.getText());
        lab.setNumero(txtnumerodireccion.getText());
        lab.setObservaciones(txtObservaciones.getText());
        
        if (!txttelcelularlab.getText().equals("")) {
            lab.setCelular(Long.valueOf(txttelcelularlab.getText()));
        }
        if (!txttelfijolab.getText().equals("")) {
            lab.setTelefono(Long.valueOf(txttelfijolab.getText()));
        }
        if (!txtlocalidadlab.getText().equals("")) {
            lab.setIdLocalidades(buscarLocalidad(txtlocalidadlab.getText(), listaLocalidades).getIdLocalidades());
        }
        lab.setMail(txtmaillab.getText());
        lab.setPaginaWeb(txtweblab.getText());
        lab.setHorario(txtHorario.getText());
        lab.setEspecialidad(txtespecialidadlab.getText());
        if (fechaAltaLaboratorio.getDate() != null) {
            lab.setFechaAlta(UtilToDate.convert(fechaAltaLaboratorio.getDate()));
        }
        if (fechaBajaLaboratorio.getDate() != null) {
            lab.setFechaBaja(UtilToDate.convert(fechaBajaLaboratorio.getDate()));
        }

        lab.setSSSRP(txtSSSRP.getText());

        if (fechavenSSSRP.getDate() != null) {
            lab.setSSSRPvenc(UtilToDate.convert(fechavenSSSRP.getDate()));
        }
        if (fechaAltaLaboratorioSiprosa.getDate() != null) {
            lab.setAltaSIPROSA(UtilToDate.convert(fechaAltaLaboratorioSiprosa.getDate()));
        }
        if (fechaVencimientoLaboratorioSiprosa.getDate() != null) {
            lab.setVencimientoSIPROSA(UtilToDate.convert(fechaVencimientoLaboratorioSiprosa.getDate()));
        }
        lab.setnHabilitacionSiprosa(txtnHabilitacionSiprosa.getText());
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        int u = 0;
        u = Laboratorio.insertarLaboratorioSP(conexion.getConnection(), lab);
        if (u > 0) {
            for (int i = 0; i < listaColegiadoLaboratorio.size(); i++) {

                ColegiadoLaboratorio.agregarColegiadoLab(conexion.getConnection(), listaColegiadoLaboratorio.get(i), u);

            }
            JOptionPane.showMessageDialog(null, "Exito al agregar el laboratorio");
            limpiarDatos();
        }
        conexion.cerrarConexion();

    }

    void aplicarCambiosUpdate() {

        lab.setNombre(txtNombre.getText());
        if (!txtCuit.getText().equals("")) {
            lab.setCuit(Long.parseLong(txtCuit.getText()));
        }
        lab.setDireccion(txtdireccionlab.getText());
        
        lab.setNumero(txtnumerodireccion.getText());
        lab.setObservaciones(txtObservaciones.getText());
        
        if (!txttelcelularlab.getText().equals("")) {
            lab.setCelular(Long.valueOf(txttelcelularlab.getText()));
        }
        if (!txttelfijolab.getText().equals("")) {
            lab.setTelefono(Long.valueOf(txttelfijolab.getText()));
        }
        if (!txtlocalidadlab.getText().equals("")) {
            lab.setIdLocalidades(buscarLocalidad(txtlocalidadlab.getText(), listaLocalidades).getIdLocalidades());
        }
        lab.setMail(txtmaillab.getText());
        lab.setPaginaWeb(txtweblab.getText());
        lab.setHorario(txtHorario.getText());
        lab.setEspecialidad(txtespecialidadlab.getText());
        if (fechaAltaLaboratorio.getDate() != null) {
            lab.setFechaAlta(UtilToDate.convert(fechaAltaLaboratorio.getDate()));
        }
        if (fechaBajaLaboratorio.getDate() != null) {
            lab.setFechaBaja(UtilToDate.convert(fechaBajaLaboratorio.getDate()));
        }

        lab.setSSSRP(txtSSSRP.getText());

        if (fechavenSSSRP.getDate() != null) {
            lab.setSSSRPvenc(UtilToDate.convert(fechavenSSSRP.getDate()));
        }
        if (fechaAltaLaboratorioSiprosa.getDate() != null) {
            lab.setAltaSIPROSA(UtilToDate.convert(fechaAltaLaboratorioSiprosa.getDate()));
        }
        if (fechaVencimientoLaboratorioSiprosa.getDate() != null) {
            lab.setVencimientoSIPROSA(UtilToDate.convert(fechaVencimientoLaboratorioSiprosa.getDate()));
        }
        lab.setnHabilitacionSiprosa(txtnHabilitacionSiprosa.getText());
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        int u;
        u = Laboratorio.actualizarLaboratorio(conexion.getConnection(), lab);
        ColegiadoLaboratorio.borrarColegiadosLab(conexion.getConnection(), lab);

        if (u > 0) {
            
            for (int i = 0; i < listaColegiadoLaboratorio.size(); i++) {

                ColegiadoLaboratorio.agregarColegiadoLab(conexion.getConnection(), listaColegiadoLaboratorio.get(i), lab.getIdLaboratorios());

            }
            JOptionPane.showMessageDialog(null, "Exito al actualizar el laboratorio");
            limpiarDatos();
            cargartablaNuevo();
        }
        conexion.cerrarConexion();

    }

    void cargartablaNuevo() {
        String[] Titulo = {"id", "Laboratorio"};
        String[] Registros = new String[4];

        modelo = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (int i = 0; i < listaLaboratorios.size(); i++) {

            Registros[0] = String.valueOf(listaLaboratorios.get(i).getIdLaboratorios());
            Registros[1] = listaLaboratorios.get(i).getNombre();

            modelo.addRow(Registros);
        }
        tablaLaboratorios.setModel(modelo);
        tablaLaboratorios.setAutoCreateRowSorter(true);
        
        TableCellRenderer headerRenderer = new FuncionesTabla(Color.darkGray, Color.darkGray);
        tablaLaboratorios.getColumnModel().getColumn(0).setHeaderRenderer(headerRenderer);
        tablaLaboratorios.getColumnModel().getColumn(1).setHeaderRenderer(headerRenderer);

        alinear();

        tablaLaboratorios.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
        tablaLaboratorios.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);

        tablaLaboratorios.getColumnModel().getColumn(0).setMinWidth(0);
        tablaLaboratorios.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaLaboratorios.getColumnModel().getColumn(0).setPreferredWidth(0);

    }

    

    void limpiarDatos() {

        txtNombre.setText(null);
        txtCuit.setText(null);
        txtdireccionlab.setText(null);
        txtnumerodireccion.setText(null);
        txtObservaciones.setText(null);
        txtlocalidadlab.setText(null);
        lblCp.setText(null);
        txttelfijolab.setText(null);
        txttelcelularlab.setText(null);
        txtmaillab.setText(null);
        txtweblab.setText(null);
        txtHorario.setText(null);
        txtespecialidadlab.setText(null);
        fechaAltaLaboratorio.setDate(null);
        fechaBajaLaboratorio.setDate(null);
        txtSSSRP.setText(null);
        fechavenSSSRP.setDate(null);
        fechaAltaLaboratorioSiprosa.setDate(null);
        txtnHabilitacionSiprosa.setText(null);
        fechaVencimientoLaboratorioSiprosa.setDate(null);
        listaBioquimicos.setModel(modeloBioquimico);
        modeloBioquimico.removeAllElements();
        listaDt.setModel(modeloDirector);
        modeloDirector.removeAllElements();
        listaFacturante.setModel(modeloFacturante);
        modeloFacturante.removeAllElements();
        txtBuscar.setText(null);
        txtNombre.requestFocus();
        list_model_bioq = null;
        list_model_direc = null;
        list_model_fact = null;

    }

    void cargarDatos(Laboratorio lab) {

        conexion.EstablecerConexion();
        ColegiadoLaboratorio.cargarColegiadoLab(conexion.getConnection(), listaColegiadoLaboratorio, lab);
        conexion.cerrarConexion();
        txtNombre.setText(lab.getNombre());
        txtCuit.setText(lab.getCuit() + "");
        txtdireccionlab.setText(lab.getDireccion());        
        txtnumerodireccion.setText(lab.getNumero());
        txtObservaciones.setText(lab.getObservaciones());
        txtlocalidadlab.setText(Localidad.buscarLocalidad(lab.getIdLocalidades(), listaLocalidades).getNombre());
        lblCp.setText(Localidad.buscarLocalidad(lab.getIdLocalidades(), listaLocalidades).getCp() + "");
        txttelfijolab.setText(lab.getTelefono() + "");
        txttelcelularlab.setText(lab.getCelular() + "");
        txtmaillab.setText(lab.getMail());
        txtweblab.setText(lab.getPaginaWeb());
        txtHorario.setText(lab.getHorario());
        txtespecialidadlab.setText(lab.getEspecialidad());
        fechaAltaLaboratorio.setDate(lab.getFechaAlta());
        fechaBajaLaboratorio.setDate(lab.getFechaBaja());
        txtSSSRP.setText(lab.getSSSRP());
        fechavenSSSRP.setDate(lab.getSSSRPvenc());
        fechaAltaLaboratorioSiprosa.setDate(lab.getAltaSIPROSA());
        txtnHabilitacionSiprosa.setText(lab.getnHabilitacionSiprosa());
        fechaVencimientoLaboratorioSiprosa.setDate(lab.getVencimientoSIPROSA());
        
       // listaBioquimicos.setModel(modeloLista);        
       // modeloLista.removeAllElements();        
       // listaDt.setModel(modeloLista);
       // modeloLista.removeAllElements();
       // listaFacturante.setModel(modeloLista);
       // modeloLista.removeAllElements();
            
       list_model_bioq = new CustomListModel();    
       list_model_direc = new CustomListModel();
       list_model_fact = new CustomListModel();
        
        listaBioquimicos.setModel(list_model_bioq);
        listaDt.setModel(list_model_direc);
        listaFacturante.setModel(list_model_fact);
        
        for (ColegiadoLaboratorio col : listaColegiadoLaboratorio) {
            list_model_bioq.addColegiado(Colegiado.buscarColegiado(col.getIdColegiado(), listaColegiados));
        }

        for (ColegiadoLaboratorio col : listaColegiadoLaboratorio) {
            if (col.getDirector() == 1) {
                list_model_direc.addColegiado(Colegiado.buscarColegiado(col.getIdColegiado(), listaColegiados));
            }

        }

        for (ColegiadoLaboratorio col : listaColegiadoLaboratorio) {

            if (col.getFacturante() == 1) {
                list_model_fact.addColegiado(Colegiado.buscarColegiado(col.getIdColegiado(), listaColegiados));
            }

        }
        txtNombre.requestFocus();

    }

    public boolean guardarImagen(String ruta) {
        cargarcolegiado();
        controlador.ConexionMariaDB mysql = new controlador.ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String insert = "UPDATE colegiados SET foto_particular=? WHERE id_colegiados=" + id_colegiado;
        FileInputStream fis = null;
        PreparedStatement ps = null;
        try {
            cn.setAutoCommit(false);
            File file = new File(ruta);
            fis = new FileInputStream(file);
            ps = cn.prepareStatement(insert);
            ps.setBinaryStream(1, fis, (int) file.length());
            ps.executeUpdate();
            cn.commit();
            return true;
        } catch (FileNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(null, ex + "\n Los Datos se insertaron erroneamente...");
        } finally {
            try {
                ps.close();
                fis.close();
            } catch (IOException | SQLException ex) {
                JOptionPane.showMessageDialog(null, ex + "\n Los Datos se insertaron erroneamente...");
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        menuDt = new javax.swing.JMenuItem();
        menuFacturante = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        panelLaboratorio = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txtlocalidadlab = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txtdireccionlab = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        txtmaillab = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txtweblab = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txttelcelularlab = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txttelfijolab = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        txtespecialidadlab = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtSSSRP = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        fechavenSSSRP = new com.toedter.calendar.JDateChooser();
        jLabel31 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaBioquimicos = new javax.swing.JList<>();
        jLabel43 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        fechaAltaLaboratorioSiprosa = new com.toedter.calendar.JDateChooser();
        jLabel41 = new javax.swing.JLabel();
        fechaVencimientoLaboratorioSiprosa = new com.toedter.calendar.JDateChooser();
        jScrollPane3 = new javax.swing.JScrollPane();
        listaDt = new javax.swing.JList<>();
        jLabel48 = new javax.swing.JLabel();
        fechaAltaLaboratorio = new com.toedter.calendar.JDateChooser();
        jLabel49 = new javax.swing.JLabel();
        fechaBajaLaboratorio = new com.toedter.calendar.JDateChooser();
        jScrollPane4 = new javax.swing.JScrollPane();
        listaFacturante = new javax.swing.JList<>();
        jLabel44 = new javax.swing.JLabel();
        btnAgregarBioquimico = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaLaboratorios = new javax.swing.JTable();
        txtBuscar = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtnHabilitacionSiprosa = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtCuit = new javax.swing.JTextField();
        txtHorario = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtnumerodireccion = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        lblCp = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        txtObservaciones = new javax.swing.JTextField();
        btnSalir = new javax.swing.JButton();
        btnAgregar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        menuDt.setText("Director técnico");
        menuDt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDtActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuDt);

        menuFacturante.setText("Facturante");
        menuFacturante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuFacturanteActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuFacturante);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panelLaboratorio.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel42.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel42.setText("Bioquímico:");

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel27.setText("Localidad:");

        txtlocalidadlab.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtlocalidadlab.setForeground(new java.awt.Color(0, 102, 204));
        txtlocalidadlab.setNextFocusableComponent(txtdireccionlab);
        txtlocalidadlab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtlocalidadlabActionPerformed(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setText("Dirección:");

        txtdireccionlab.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtdireccionlab.setForeground(new java.awt.Color(0, 102, 204));
        txtdireccionlab.setNextFocusableComponent(txtdireccionlab);
        txtdireccionlab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdireccionlabKeyPressed(evt);
            }
        });

        jLabel37.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel37.setText("Web:");

        txtmaillab.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        txtmaillab.setForeground(new java.awt.Color(0, 102, 204));
        txtmaillab.setNextFocusableComponent(txtweblab);
        txtmaillab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmaillabKeyPressed(evt);
            }
        });

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel30.setText("Mail:");

        txtweblab.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        txtweblab.setForeground(new java.awt.Color(0, 102, 204));
        txtweblab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtweblabKeyPressed(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel29.setText("Celular:");

        txttelcelularlab.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txttelcelularlab.setForeground(new java.awt.Color(0, 102, 204));
        txttelcelularlab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttelcelularlabKeyPressed(evt);
            }
        });

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel28.setText("Teléfono:");

        txttelfijolab.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txttelfijolab.setForeground(new java.awt.Color(0, 102, 204));
        txttelfijolab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttelfijolabKeyPressed(evt);
            }
        });

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel39.setText("Horario:");

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel38.setText("Especialidad: ");

        txtespecialidadlab.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        txtespecialidadlab.setForeground(new java.awt.Color(0, 102, 204));
        txtespecialidadlab.setNextFocusableComponent(txtSSSRP);
        txtespecialidadlab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtespecialidadlabKeyPressed(evt);
            }
        });

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setText("SSSRP:");

        txtSSSRP.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        txtSSSRP.setForeground(new java.awt.Color(0, 102, 204));
        txtSSSRP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSSSRPKeyPressed(evt);
            }
        });

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel35.setText("FV de SSSRP:");

        fechavenSSSRP.setForeground(new java.awt.Color(0, 102, 204));
        fechavenSSSRP.setDateFormatString("dd-MM-yyyy");
        fechavenSSSRP.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechavenSSSRP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechavenSSSRPKeyPressed(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel31.setText("Nombre:");

        txtNombre.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtNombre.setForeground(new java.awt.Color(0, 102, 204));
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreKeyPressed(evt);
            }
        });

        listaBioquimicos.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        listaBioquimicos.setForeground(new java.awt.Color(0, 102, 204));
        listaBioquimicos.setComponentPopupMenu(jPopupMenu1);
        listaBioquimicos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                listaBioquimicosKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(listaBioquimicos);

        jLabel43.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel43.setText("Director técnico:");

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel36.setText("Fecha de habilitación SI.PRO.SA:");

        fechaAltaLaboratorioSiprosa.setForeground(new java.awt.Color(0, 102, 204));
        fechaAltaLaboratorioSiprosa.setDateFormatString("dd-MM-yyyy");
        fechaAltaLaboratorioSiprosa.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechaAltaLaboratorioSiprosa.setNextFocusableComponent(fechavenSSSRP);
        fechaAltaLaboratorioSiprosa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechaAltaLaboratorioSiprosaKeyPressed(evt);
            }
        });

        jLabel41.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel41.setText("Fecha de vencimiento habilitación SI.PRO.SA:");

        fechaVencimientoLaboratorioSiprosa.setForeground(new java.awt.Color(0, 102, 204));
        fechaVencimientoLaboratorioSiprosa.setDateFormatString("dd-MM-yyyy");
        fechaVencimientoLaboratorioSiprosa.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechaVencimientoLaboratorioSiprosa.setNextFocusableComponent(fechavenSSSRP);
        fechaVencimientoLaboratorioSiprosa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechaVencimientoLaboratorioSiprosaKeyPressed(evt);
            }
        });

        listaDt.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        listaDt.setForeground(new java.awt.Color(0, 102, 204));
        listaDt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                listaDtKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(listaDt);

        jLabel48.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel48.setText("Fecha de Alta:");

        fechaAltaLaboratorio.setForeground(new java.awt.Color(0, 102, 204));
        fechaAltaLaboratorio.setDateFormatString("dd-MM-yyyy");
        fechaAltaLaboratorio.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechaAltaLaboratorio.setNextFocusableComponent(fechavenSSSRP);
        fechaAltaLaboratorio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechaAltaLaboratorioKeyPressed(evt);
            }
        });

        jLabel49.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel49.setText("Fecha de Baja:");

        fechaBajaLaboratorio.setForeground(new java.awt.Color(0, 102, 204));
        fechaBajaLaboratorio.setDateFormatString("dd-MM-yyyy");
        fechaBajaLaboratorio.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechaBajaLaboratorio.setNextFocusableComponent(btnAgregar);
        fechaBajaLaboratorio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechaBajaLaboratorioKeyPressed(evt);
            }
        });

        listaFacturante.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        listaFacturante.setForeground(new java.awt.Color(0, 102, 204));
        listaFacturante.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                listaFacturanteKeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(listaFacturante);

        jLabel44.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel44.setText("Facturante:");

        btnAgregarBioquimico.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAgregarBioquimico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnAgregarBioquimico.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAgregarBioquimico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarBioquimicoActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablaLaboratorios.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaLaboratorios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaLaboratorios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tablaLaboratorios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaLaboratoriosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaLaboratorios);

        txtBuscar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtBuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728952 - magnifier zoom.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Nº:");

        txtnHabilitacionSiprosa.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        txtnHabilitacionSiprosa.setForeground(new java.awt.Color(0, 102, 204));
        txtnHabilitacionSiprosa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnHabilitacionSiprosaKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Cuit:");

        txtCuit.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtCuit.setForeground(new java.awt.Color(0, 102, 204));
        txtCuit.setNextFocusableComponent(txtdireccionlab);
        txtCuit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCuitKeyPressed(evt);
            }
        });

        txtHorario.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtHorario.setForeground(new java.awt.Color(0, 102, 204));
        txtHorario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtHorarioKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("N°:");

        txtnumerodireccion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtnumerodireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtnumerodireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnumerodireccionKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("CP:");

        lblCp.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblCp.setForeground(new java.awt.Color(0, 102, 204));

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel32.setText("Observaciones:");

        txtObservaciones.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtObservaciones.setForeground(new java.awt.Color(0, 102, 204));
        txtObservaciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtObservacionesKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelLaboratorioLayout = new javax.swing.GroupLayout(panelLaboratorio);
        panelLaboratorio.setLayout(panelLaboratorioLayout);
        panelLaboratorioLayout.setHorizontalGroup(
            panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addComponent(jLabel26)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtdireccionlab)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtnumerodireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addComponent(jLabel31)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, Short.MAX_VALUE)
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtCuit, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addComponent(jLabel32)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtObservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtlocalidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblCp, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                                .addComponent(jLabel28)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txttelfijolab, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                                .addComponent(jLabel30)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtmaillab, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(18, 18, Short.MAX_VALUE)
                                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                                .addComponent(jLabel29)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txttelcelularlab, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelLaboratorioLayout.createSequentialGroup()
                                                .addComponent(jLabel37)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtweblab))))
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addComponent(jLabel41)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(fechaVencimientoLaboratorioSiprosa, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addComponent(jLabel25)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtSSSRP, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtnHabilitacionSiprosa, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel36)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(fechaAltaLaboratorioSiprosa, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                            .addComponent(jLabel35)
                                            .addGap(18, 18, 18)
                                            .addComponent(fechavenSSSRP, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                            .addComponent(jLabel38)
                                            .addGap(398, 398, 398))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelLaboratorioLayout.createSequentialGroup()
                                            .addComponent(jLabel39)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtHorario, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addComponent(jLabel48)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(fechaAltaLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel49)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(fechaBajaLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                        .addGap(85, 85, 85)
                                        .addComponent(txtespecialidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, 394, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLaboratorioLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLaboratorioLayout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnAgregarBioquimico, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                .addComponent(jLabel42)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel43))
                        .addGap(18, 18, 18)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel44))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelLaboratorioLayout.setVerticalGroup(
            panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(txtCuit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtdireccionlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addComponent(txtnumerodireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtObservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCp, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtlocalidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txttelfijolab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txttelcelularlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel29))
                            .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtmaillab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel37)
                            .addComponent(txtweblab, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel39)
                            .addComponent(txtHorario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel38)
                            .addComponent(txtespecialidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel48, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(fechaAltaLaboratorio, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(1, 1, 1))
                            .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(fechaBajaLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel25)
                                .addComponent(txtSSSRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(fechavenSSSRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtnHabilitacionSiprosa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(fechaAltaLaboratorioSiprosa, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(fechaVencimientoLaboratorioSiprosa, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel41, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                .addComponent(jLabel42)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLaboratorioLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnAgregarBioquimico)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelLaboratorioLayout.createSequentialGroup()
                        .addGroup(panelLaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                .addComponent(jLabel44)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelLaboratorioLayout.createSequentialGroup()
                                .addComponent(jLabel43)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnSalir.setMnemonic('v');
        btnSalir.setText("Salir");
        btnSalir.setToolTipText("Alt + v");
        btnSalir.setPreferredSize(new java.awt.Dimension(108, 23));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnAgregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnAgregar.setMnemonic('a');
        btnAgregar.setText("Agregar");
        btnAgregar.setMaximumSize(null);
        btnAgregar.setMinimumSize(null);
        btnAgregar.setPreferredSize(null);
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        btnModificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelLaboratorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnModificar)
                        .addGap(18, 18, 18)
                        .addComponent(btnCancelar)
                        .addGap(18, 18, 18)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar)
                    .addComponent(btnModificar)
                    .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        aplicarCambios();
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnAgregarBioquimicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarBioquimicoActionPerformed

        new BuscaColegiado(null, true).setVisible(true);

        int banderaEncuentra = 0;

        if (BuscaColegiado.banderaAgrega == 1) {
            listaColegiadoLaboratorio.add(new ColegiadoLaboratorio(c.getIdColegiados(), 0, 0));
            for (int i = 0; i < list_model_bioq.getSize(); i++) {

                if (c.toString().equals(list_model_bioq.getElementAt(i).toString())) {
                    banderaEncuentra = 1;
                    break;
                }
            }
            if (banderaEncuentra == 0) {
                list_model_bioq.addColegiado(c);
            }
        }
    }//GEN-LAST:event_btnAgregarBioquimicoActionPerformed

    private void menuDtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDtActionPerformed

        if (listaBioquimicos.getSelectedIndex() >= 0) {

            int banderaEncuentra = 0;

            for (int i = 0; i < list_model_direc.getSize(); i++) {
                if (list_model_bioq.getColegiado(listaBioquimicos.getSelectedIndex()).toString().equals(list_model_direc.getElementAt(i).toString())) {
                    banderaEncuentra = 1;
                    break;
                }
            }
            if (banderaEncuentra == 0) {
                list_model_direc.addColegiado(list_model_bioq.getColegiado(listaBioquimicos.getSelectedIndex()));
                ColegiadoLaboratorio.buscarColab(list_model_bioq.getColegiado(listaBioquimicos.getSelectedIndex()).getIdColegiados(), listaColegiadoLaboratorio).setDirector(1);

            }
        }
    }//GEN-LAST:event_menuDtActionPerformed

    private void menuFacturanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuFacturanteActionPerformed

        if (listaBioquimicos.getSelectedIndex() >= 0) {

            int banderaEncuentra = 0;

            for (int i = 0; i < list_model_fact.getSize(); i++) {
                if (list_model_bioq.getColegiado(listaBioquimicos.getSelectedIndex()).toString().equals(list_model_fact.getElementAt(i).toString())) {
                    banderaEncuentra = 1;
                    break;
                }
            }
            if (banderaEncuentra == 0) {
                list_model_fact.addColegiado(list_model_bioq.getColegiado(listaBioquimicos.getSelectedIndex()));
                ColegiadoLaboratorio.buscarColab(list_model_bioq.getColegiado(listaBioquimicos.getSelectedIndex()).getIdColegiados(), listaColegiadoLaboratorio).setFacturante(1);
            }
        }
    }//GEN-LAST:event_menuFacturanteActionPerformed

    private void fechaAltaLaboratorioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechaAltaLaboratorioKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            fechaBajaLaboratorio.requestFocus();
        }

    }//GEN-LAST:event_fechaAltaLaboratorioKeyPressed

    private void fechaBajaLaboratorioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechaBajaLaboratorioKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtSSSRP.requestFocus();
        }

    }//GEN-LAST:event_fechaBajaLaboratorioKeyPressed

    private void fechavenSSSRPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechavenSSSRPKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            
            txtnHabilitacionSiprosa.requestFocus();
            
        }

    }//GEN-LAST:event_fechavenSSSRPKeyPressed

    private void fechaAltaLaboratorioSiprosaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechaAltaLaboratorioSiprosaKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {

            fechaVencimientoLaboratorioSiprosa.requestFocus();
            
        }

    }//GEN-LAST:event_fechaAltaLaboratorioSiprosaKeyPressed

    private void fechaVencimientoLaboratorioSiprosaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechaVencimientoLaboratorioSiprosaKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {
            btnAgregarBioquimico.requestFocus();
        }

    }//GEN-LAST:event_fechaVencimientoLaboratorioSiprosaKeyPressed

    private void tablaLaboratoriosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaLaboratoriosMouseClicked

        if (evt.getClickCount() == 2) {
           
            int id = Integer.parseInt(tablaLaboratorios.getValueAt(tablaLaboratorios.getSelectedRow(), 0).toString());
            lab = Laboratorio.buscarLaboratorio(id, listaLaboratorios);
            listaColegiadoLaboratorio.clear();               
            cargarDatos(lab);
            btnModificar.setEnabled(true);
            btnAgregar.setEnabled(false);

        }

    }//GEN-LAST:event_tablaLaboratoriosMouseClicked

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        limpiarDatos();

    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed

        aplicarCambiosUpdate();

    }//GEN-LAST:event_btnModificarActionPerformed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased

        TableRowSorter sorter = new TableRowSorter(modelo);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtBuscar.getText() + ".*"));
        tablaLaboratorios.setRowSorter(sorter);

    }//GEN-LAST:event_txtBuscarKeyReleased

    private void listaBioquimicosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listaBioquimicosKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_DELETE && listaBioquimicos.getSelectedIndex() > -1) {

            listaColegiadoLaboratorio.remove(listaBioquimicos.getSelectedIndex());

            for (int i = 0; i < list_model_direc.getSize(); i++) {
                if (list_model_direc.getColegiado(i).toString().equals(list_model_bioq.getColegiado(listaBioquimicos.getSelectedIndex()).toString())) {
                    list_model_direc.eliminarColegiado(i);
                }
            }
            for (int i = 0; i < list_model_fact.getSize(); i++) {
                if (list_model_fact.getColegiado(i).toString().equals(list_model_bioq.getColegiado(listaBioquimicos.getSelectedIndex()).toString())) {
                    list_model_fact.eliminarColegiado(i);
                }
            }
            list_model_bioq.eliminarColegiado(listaBioquimicos.getSelectedIndex());
        }
    }//GEN-LAST:event_listaBioquimicosKeyPressed

    private void listaDtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listaDtKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_DELETE && listaDt.getSelectedIndex() > -1) {
            
            ColegiadoLaboratorio.buscarColab(list_model_direc.getColegiado(listaDt.getSelectedIndex()).getIdColegiados(), listaColegiadoLaboratorio).setDirector(0);
            list_model_direc.eliminarColegiado(listaDt.getSelectedIndex());

        }

    }//GEN-LAST:event_listaDtKeyPressed

    private void listaFacturanteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listaFacturanteKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_DELETE && listaFacturante.getSelectedIndex() > -1) {

            ColegiadoLaboratorio.buscarColab(list_model_fact.getColegiado(listaFacturante.getSelectedIndex()).getIdColegiados(), listaColegiadoLaboratorio).setFacturante(0);
            list_model_fact.eliminarColegiado(listaFacturante.getSelectedIndex());

        }

    }//GEN-LAST:event_listaFacturanteKeyPressed

    private void txtNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyPressed
        
        if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
            txtCuit.requestFocus();
        }
        
    }//GEN-LAST:event_txtNombreKeyPressed

    private void txtCuitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCuitKeyPressed

        if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
        txtdireccionlab.requestFocus();
        }
        
    }//GEN-LAST:event_txtCuitKeyPressed

    private void txtnumerodireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumerodireccionKeyPressed

        if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
        txtObservaciones.requestFocus();
        }
        
    }//GEN-LAST:event_txtnumerodireccionKeyPressed

    private void txtObservacionesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtObservacionesKeyPressed

        if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
            txtlocalidadlab.requestFocus();
        }
        
    }//GEN-LAST:event_txtObservacionesKeyPressed

    private void txttelfijolabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelfijolabKeyPressed
      
        if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
            txttelcelularlab.requestFocus();
        }
        
    }//GEN-LAST:event_txttelfijolabKeyPressed

    private void txttelcelularlabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelcelularlabKeyPressed

         if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
            txtmaillab.requestFocus();
        }
        
    }//GEN-LAST:event_txttelcelularlabKeyPressed

    private void txtmaillabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmaillabKeyPressed

         if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
            txtweblab.requestFocus();
        }
        
    }//GEN-LAST:event_txtmaillabKeyPressed

    private void txtweblabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtweblabKeyPressed

         if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
            txtHorario.requestFocus();
        }
        
    }//GEN-LAST:event_txtweblabKeyPressed

    private void txtHorarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHorarioKeyPressed

         if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
            txtespecialidadlab.requestFocus();
        }
        
    }//GEN-LAST:event_txtHorarioKeyPressed

    private void txtespecialidadlabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtespecialidadlabKeyPressed

         if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
             fechaAltaLaboratorio.requestFocus();
             
        }
        
    }//GEN-LAST:event_txtespecialidadlabKeyPressed

    private void txtSSSRPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSSSRPKeyPressed

        if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
             fechavenSSSRP.requestFocus();
             
        }
        
    }//GEN-LAST:event_txtSSSRPKeyPressed

    private void txtnHabilitacionSiprosaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnHabilitacionSiprosaKeyPressed

        if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
             fechaAltaLaboratorioSiprosa.requestFocus();
             
        }
        
    }//GEN-LAST:event_txtnHabilitacionSiprosaKeyPressed

    private void txtdireccionlabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionlabKeyPressed

        if(evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB){
            evt.consume();
             txtnumerodireccion.requestFocus();
             
        }
        
    }//GEN-LAST:event_txtdireccionlabKeyPressed

    private void txtlocalidadlabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtlocalidadlabActionPerformed

        if(!txtlocalidadlab.equals("")){
            
        lblCp.setText(Localidad.buscarLocalidad(txtlocalidadlab.getText(), listaLocalidades).getCp()+"");
        }
        txttelfijolab.requestFocus();
        
        
    }//GEN-LAST:event_txtlocalidadlabActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnAgregarBioquimico;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnSalir;
    private com.toedter.calendar.JDateChooser fechaAltaLaboratorio;
    private com.toedter.calendar.JDateChooser fechaAltaLaboratorioSiprosa;
    private com.toedter.calendar.JDateChooser fechaBajaLaboratorio;
    private com.toedter.calendar.JDateChooser fechaVencimientoLaboratorioSiprosa;
    private com.toedter.calendar.JDateChooser fechavenSSSRP;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblCp;
    private javax.swing.JList<String> listaBioquimicos;
    private javax.swing.JList<String> listaDt;
    private javax.swing.JList<String> listaFacturante;
    private javax.swing.JMenuItem menuDt;
    private javax.swing.JMenuItem menuFacturante;
    private javax.swing.JPanel panelLaboratorio;
    private javax.swing.JTable tablaLaboratorios;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtCuit;
    private javax.swing.JTextField txtHorario;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtObservaciones;
    private javax.swing.JTextField txtSSSRP;
    private javax.swing.JTextField txtdireccionlab;
    private javax.swing.JTextField txtespecialidadlab;
    private javax.swing.JTextField txtlocalidadlab;
    private javax.swing.JTextField txtmaillab;
    private javax.swing.JTextField txtnHabilitacionSiprosa;
    private javax.swing.JTextField txtnumerodireccion;
    private javax.swing.JTextField txttelcelularlab;
    private javax.swing.JTextField txttelfijolab;
    private javax.swing.JTextField txtweblab;
    // End of variables declaration//GEN-END:variables
}
