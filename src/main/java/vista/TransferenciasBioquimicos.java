package vista;


import controlador.ConexionMariaDB;
import controlador.TableHeder;
import controlador.cargar_tipo_cuenta;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;


public class TransferenciasBioquimicos extends javax.swing.JDialog {

    TextAutoCompleter textAutoAcompleter;
    TextAutoCompleter textAutoAcompleter2;
    DefaultTableModel model;
    int id_banco, id_cuenta, contadorobrasocial = 0, idbanco = 0;
    Object liquidacion_seleccionada;
    DefaultTableCellRenderer alinearCentroN, alinearCentroA, alinearCentroP, alinearCentroD, alinearCentroM, alinearCentroL, alinearCentroC;

    public TransferenciasBioquimicos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setTitle("Transferencias Bioquimicos");
        this.setLocationRelativeTo(null);
        textAutoAcompleter = new TextAutoCompleter(txtliquidacion);
        textAutoAcompleter2 = new TextAutoCompleter(txtbanco);
        cargarliquidaciones();
        cargarbancos();
        cargarperiodo();
        cargartipocuenta();
    }

    void cargarliquidaciones() {
        textAutoAcompleter.removeAllItems();
        // Recorro y cargo las obras sociales
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        String sSQL = "select distinct(id_liquidacion) from liquidacion_fecha";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                textAutoAcompleter.addItem(rs.getString(1));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter.setMode(0);
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter.setCaseSensitive(false);
    }

    /*  public static cargar_bancos banco(ObservableList<cargar_bancos> lista) {
        cargar_bancos resultado = null;
        // Recorro y cargo las obras sociales
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        try {
            Statement instruccion = cn.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT id_obrasocial, Int_codigo_obrasocial, nombre_obrasocial FROM obrasocial");
            lista.removeAll();
            while (rs.next()) {
                lista.add(new cargar_bancos(rs.getInt("idBanco"),rs.getString("BancoNombre")));
                //System.out.println(rs.getInt("id_obrasocial")+" - "+rs.getInt("Int_codigo_obrasocial")+" - "+rs.getString("nombre_obrasocial"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultado;
    }*/
    void cargarbancos() {

        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        try {
            Statement instruccion = cn.createStatement();
            ResultSet rs = instruccion.executeQuery("SELECT idBanco, BancoNombre FROM banco");
            while (rs.next()) {
                textAutoAcompleter2.addItem(rs.getString("idBanco") + "-" + rs.getString("BancoNombre"));
                contadorobrasocial++;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }

        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter2.setMode(0); // infijo     
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter2.setCaseSensitive(false); //No sensible a mayúsculas        
    }

    void cargartotales() {
        double total = 0.00, sumatoria = 0.0;
        DecimalFormat df = new DecimalFormat("0.00");
        //AQUI SE SUMAN LOS VALORES DE CADA FILA PARA COLOCARLO EN EL CAMPO DE TOTAL
        int totalRow = tablaliquidacion.getRowCount();
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            String x = tablaliquidacion.getValueAt(i, 5).toString();
            sumatoria = Double.valueOf(x);
            total = total + sumatoria;
        }
        txtcolegiados.setText((String.valueOf(totalRow + 1)));
        txttotal.setText((String.valueOf(Redondear(total))));

    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    public void cargartabla(String valor) {

        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        /////////////////      0           1           2           3               4           5     
        String[] Titulo = {"Matricula", "Colegiado", "Nº Cuenta", "CBU", "CUIT Destino", "Importe"};
        Object[] Registros = new Object[6];
        model = new DefaultTableModel(null, Titulo) {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 5) {
                    return true;
                } else {
                    return false;
                }
            }
        };

        String sql = "select * from vista_liquidaciones where id_liquidacion=" + liquidacion_seleccionada + " and Importe > 0 and idbanco=" + idbanco + " and idcuenta=" + id_cuenta + " ORDER BY matricula_colegiado";
        System.out.println("liquidacion_seleccionada" + liquidacion_seleccionada);
        try {//matricula_colegiado,nombre_colegiado,id_liquidacion,Importe,NumeroCuenta,CBU,cuitDestino,BancoColegiadoEstado
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString("matricula_colegiado");
                Registros[1] = rs.getString("nombre_colegiado");
                Registros[2] = rs.getString("NumeroCuenta");
                Registros[3] = rs.getString("CBU");
                Registros[4] = rs.getString("cuitDestino");
                Registros[5] = rs.getString("Importe");
                model.addRow(Registros);
            }
            tablaliquidacion.setModel(model);
            tablaliquidacion.setAutoCreateRowSorter(true);
            TableCellRenderer headerRenderer = new TableHeder(Color.white, Color.black);

            for (int i = 0; i < 6; i++) {
                tablaliquidacion.getColumnModel().getColumn(i).setHeaderRenderer(headerRenderer);
                alinear();
                if (i == 0) {
                    tablaliquidacion.getColumnModel().getColumn(i).setPreferredWidth(60);
                    tablaliquidacion.getColumnModel().getColumn(i).setMaxWidth(60);
                    tablaliquidacion.getColumnModel().getColumn(i).setCellRenderer(alinearCentroA);
                }
                if (i == 1) {
                    tablaliquidacion.getColumnModel().getColumn(i).setPreferredWidth(120);
                    tablaliquidacion.getColumnModel().getColumn(i).setMinWidth(120);
                    tablaliquidacion.getColumnModel().getColumn(i).setCellRenderer(alinearCentroN);
                }
                if (i == 2) {
                    tablaliquidacion.getColumnModel().getColumn(i).setPreferredWidth(80);
                    tablaliquidacion.getColumnModel().getColumn(i).setMaxWidth(80);
                    tablaliquidacion.getColumnModel().getColumn(i).setCellRenderer(alinearCentroL);
                }
                if (i == 3) {
                    tablaliquidacion.getColumnModel().getColumn(i).setPreferredWidth(90);
                    tablaliquidacion.getColumnModel().getColumn(i).setMinWidth(90);
                    tablaliquidacion.getColumnModel().getColumn(i).setCellRenderer(alinearCentroC);
                }
                if (i == 4) {
                    tablaliquidacion.getColumnModel().getColumn(i).setPreferredWidth(80);
                    tablaliquidacion.getColumnModel().getColumn(i).setMaxWidth(80);
                    tablaliquidacion.getColumnModel().getColumn(i).setCellRenderer(alinearCentroD);
                }
                if (i == 5) {
                    tablaliquidacion.getColumnModel().getColumn(i).setPreferredWidth(70);
                    tablaliquidacion.getColumnModel().getColumn(i).setMaxWidth(70);
                    tablaliquidacion.getColumnModel().getColumn(i).setCellRenderer(alinearCentroP);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void alinear() {
        alinearCentroN = new DefaultTableCellRenderer();
        alinearCentroN.setHorizontalAlignment(SwingConstants.LEFT);///AMARILLO
        alinearCentroN.setBackground(Color.decode("#FFFACD"));
        alinearCentroN.setForeground(Color.black);

        alinearCentroA = new DefaultTableCellRenderer();
        alinearCentroA.setHorizontalAlignment(SwingConstants.LEFT);///AZUL
        alinearCentroA.setBackground(Color.decode("#3A5FCD"));
        alinearCentroA.setForeground(Color.white);

        alinearCentroP = new DefaultTableCellRenderer();
        alinearCentroP.setHorizontalAlignment(SwingConstants.RIGHT);//VERDE
        alinearCentroP.setBackground(Color.decode("#C1FFC1"));
        alinearCentroP.setForeground(Color.black);

        alinearCentroD = new DefaultTableCellRenderer();
        alinearCentroD.setHorizontalAlignment(SwingConstants.LEFT);///NARANJA
        alinearCentroD.setBackground(Color.decode("#FFA54F"));
        alinearCentroD.setForeground(Color.black);

        alinearCentroM = new DefaultTableCellRenderer();
        alinearCentroM.setHorizontalAlignment(SwingConstants.LEFT);///ROSA
        alinearCentroM.setBackground(Color.decode("#FFB5C5"));
        alinearCentroM.setForeground(Color.black);

        alinearCentroL = new DefaultTableCellRenderer();
        alinearCentroL.setHorizontalAlignment(SwingConstants.LEFT);///VIOLETA
        alinearCentroL.setBackground(Color.decode("#8968CD"));
        alinearCentroL.setForeground(Color.white);

        alinearCentroC = new DefaultTableCellRenderer();
        alinearCentroC.setHorizontalAlignment(SwingConstants.LEFT);///CELESTE
        alinearCentroC.setBackground(Color.decode("#BFEFFF"));
        alinearCentroC.setForeground(Color.black);

        /*
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);

        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
         */
    }

    void cargartipocuenta() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipo.setModel(value);

        // Recorro y cargo las obras sociales
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        Statement st1 = null;
        String sSQL = "select idCuenta,CuentaNombre from tipo_cuenta";
        value.addElement(new cargar_tipo_cuenta(0, "Todos"));
        try {
            st1 = cn.createStatement();
            ResultSet rs = st1.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                value.addElement(new cargar_tipo_cuenta(rs.getInt("idCuenta"), rs.getString("CuentaNombre")));
                // value.addElement(rs.getString("CuentaNombre"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (st1 != null) {
                    st1.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void cargarperiodo() {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        //SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");
        //SimpleDateFormat formato3 = new SimpleDateFormat("yyyyMMdd");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        //fecha2 = formato2.format(currentDate);
        //fechaonline = formato3.format(currentDate);

        txtfecha.setText(fecha);
        /////////////////////////////////////////////////////////

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtliquidacion = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtbanco = new javax.swing.JTextField();
        cbotipo = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaliquidacion = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txtcolegiados = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txttotal = new javax.swing.JFormattedTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        txtfecha = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        cbotipo1 = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Liquidación:");

        txtliquidacion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtliquidacion.setNextFocusableComponent(txtfecha);
        txtliquidacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtliquidacionActionPerformed(evt);
            }
        });
        txtliquidacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtliquidacionKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Fecha:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Tipo cuenta:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Banco:");

        txtbanco.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbanco.setNextFocusableComponent(tablaliquidacion);
        txtbanco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbancoActionPerformed(evt);
            }
        });
        txtbanco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbancoKeyPressed(evt);
            }
        });

        cbotipo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbotipo.setNextFocusableComponent(txtbanco);
        cbotipo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbotipoMouseClicked(evt);
            }
        });
        cbotipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotipoActionPerformed(evt);
            }
        });
        cbotipo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbotipoKeyPressed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablaliquidacion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablaliquidacion);

        txtbuscar.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setText("Total de colegiados:");

        txtcolegiados.setEditable(false);
        txtcolegiados.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txtcolegiados.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtcolegiados.setBorder(null);
        txtcolegiados.setOpaque(false);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setText("Total: $");

        txttotal.setEditable(false);
        txttotal.setBorder(null);
        txttotal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.##"))));
        txttotal.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcolegiados, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(253, 253, 253)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotal))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 726, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel26)
                                .addComponent(txtcolegiados)))
                        .addGap(18, 18, 18))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17))))
        );

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728934 - enter login right.png"))); // NOI18N
        jButton1.setText("Confirmar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        jButton2.setText("Salir");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        try {
            txtfecha.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfecha.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtfecha.setNextFocusableComponent(cbotipo);
        txtfecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfechaKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Tipo de Transferencia:");

        cbotipo1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbotipo1.setNextFocusableComponent(txtbanco);
        cbotipo1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbotipo1MouseClicked(evt);
            }
        });
        cbotipo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotipo1ActionPerformed(evt);
            }
        });
        cbotipo1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbotipo1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtliquidacion, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbotipo1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(253, 253, 253))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbotipo, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtbanco, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbotipo1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtliquidacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)
                        .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbotipo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(jLabel4)
                        .addComponent(txtbanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 754, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtliquidacionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtliquidacionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            liquidacion_seleccionada = txtliquidacion.getText();
            System.out.println("liquidacion_seleccionada" + liquidacion_seleccionada);
            txtliquidacion.transferFocus();
        }
    }//GEN-LAST:event_txtliquidacionKeyPressed

    private void txtfechaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfechaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtfecha.transferFocus();
        }
    }//GEN-LAST:event_txtfechaKeyPressed

    private void cbotipoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbotipoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Object tipo = cbotipo.getSelectedItem();
            System.out.println("tipo " + tipo);
            cargar_tipo_cuenta cuenta = (cargar_tipo_cuenta) tipo;
            id_cuenta = cuenta.getId_cuenta();
            System.out.println("tipo seleccionado " + id_cuenta);
            cbotipo.transferFocus();
        }
    }//GEN-LAST:event_cbotipoKeyPressed

    private void txtbancoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbancoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtbanco.transferFocus();
        }
    }//GEN-LAST:event_txtbancoKeyPressed

    private void txtliquidacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtliquidacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtliquidacionActionPerformed

    private void cbotipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotipoActionPerformed
        if (cbotipo.getItemCount() != 0) {
            Object tipo = cbotipo.getSelectedItem();
            System.out.println("tipo " + tipo);
            cargar_tipo_cuenta cuenta = (cargar_tipo_cuenta) tipo;
            id_cuenta = cuenta.getId_cuenta();
            System.out.println("tipo seleccionado " + id_cuenta);
            cbotipo.transferFocus();
        }
    }//GEN-LAST:event_cbotipoActionPerformed

    private void txtbancoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbancoActionPerformed
        String cod = "", cadena = txtbanco.getText();
        int k = 0;
        ///cod/// 0000 - aaaaaaaaa
        while (k < cadena.length()) {
            if (String.valueOf(cadena.charAt(k)).equals("-")) {
                k = cadena.length();
            } else {
                cod = cod + cadena.charAt(k);
            }
            k++;
        }
        idbanco = Integer.valueOf(cod);
        System.out.println("idbanco " + idbanco);
        cargartabla(cod);
        cargartotales();
    }//GEN-LAST:event_txtbancoActionPerformed

    private void cbotipoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbotipoMouseClicked

    }//GEN-LAST:event_cbotipoMouseClicked

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        if (tablaliquidacion.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "No hay resultados que cargar...");
        } else {
            txtbuscar.transferFocus();
            tablaliquidacion.setRowSelectionInterval(0, 0);

        }
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
        tablaliquidacion.setRowSorter(sorter);
        cargartotales();
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
//String sql = "select * from vista_liquidaciones where id_liquidacion=" + liquidacion_seleccionada + " and Importe > 0 and idbanco=" + idbanco + " and idcuenta=" + id_cuenta + " ORDER BY matricula_colegiado";        

        if (tablaliquidacion.getSelectedRow() >= 0) {

            try {
                // JOptionPane.showMessageDialog(null, "Entra nivel 0");
                int n2 = 0, j = 0;
                ConexionMariaDB mysql = new ConexionMariaDB();
                Connection cn = mysql.Conectar();
                n2 = tablaliquidacion.getRowCount();
                JOptionPane.showMessageDialog(null, "liquidacion_seleccionada: " + liquidacion_seleccionada);
                while (n2 > j) {
                    String importe = tablaliquidacion.getValueAt(j, 5).toString().replace(" ", "");
                    int matricula = Integer.valueOf(tablaliquidacion.getValueAt(j, 1).toString());
                    String actualizar = "UPDATE vista_liquidaciones SET Importe=? WHERE id_liquidacion=" + liquidacion_seleccionada + " matricula_colegiado=" + matricula;
                    PreparedStatement pst = cn.prepareStatement(actualizar);
                    pst.setString(1, importe);
                    int p = pst.executeUpdate();
                    j++;
                }

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos");
                JOptionPane.showMessageDialog(null, ex);
            }

        } else {
            JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void cbotipo1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbotipo1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_cbotipo1MouseClicked

    private void cbotipo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotipo1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbotipo1ActionPerformed

    private void cbotipo1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbotipo1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbotipo1KeyPressed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbotipo;
    private javax.swing.JComboBox<String> cbotipo1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaliquidacion;
    private javax.swing.JTextField txtbanco;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtcolegiados;
    private javax.swing.JFormattedTextField txtfecha;
    private javax.swing.JTextField txtliquidacion;
    private javax.swing.JFormattedTextField txttotal;
    // End of variables declaration//GEN-END:variables
}
