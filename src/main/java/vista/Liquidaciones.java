package vista;

import controlador.FuncionesTabla;
import static controlador.FuncionesTabla.alinear;
import static controlador.FuncionesTabla.alinearCentro;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;
import modelo.ConexionMariaDB;
import modelo.Liquidacion;
import modelo.Matricula;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import static vista.LiquidacionMatricula.banderaImprime;
import static vista.LiquidacionMatricula.txtLiquidacion;
import static vista.PeriodosLiquidacion.banderaImprimePeriodos;
import static vista.PeriodosLiquidacion.txtMatricula;
import static vista.PeriodosLiquidacion.txtPeriodoDesde;
import static vista.PeriodosLiquidacion.txtPeriodoHasta;
import java.security.SecureRandom;
import java.math.BigInteger;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import static vista.Modificar_Orden.periodo;

public class Liquidaciones extends javax.swing.JDialog {

    private ConexionMariaDB conexion;
    public ArrayList<Matricula> listaMatriculas;
    public ArrayList<Liquidacion> listaLiquidaciones;
    public ArrayList<Liquidacion> listaLiquidacionesPeriodo;
    DefaultTableModel modelo;
    HiloGenerarPDF hiloGenerar;

    public Liquidaciones(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icocbt.png")).getImage());
        setLocationRelativeTo(null);
        setResizable(false);
        cargarLiquidaciones();
        cargartablaNuevo();
        progreso.setVisible(false);
    }

    public static String generateRandomText() {

        SecureRandom random = new SecureRandom();
        String text = new BigInteger(50, random).toString(32);
        return text;

    }

    void cargarLiquidaciones() {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaLiquidaciones = new ArrayList<Liquidacion>();
        Liquidacion.cargarLiquidaciones(conexion.getConnection(), listaLiquidaciones);
        conexion.cerrarConexion();
    }

    void cargartablaNuevo() {
        String[] Titulo = {"Liquidación", "Fecha", "Recibo", "Periodo"};
        Object[] Registros = new Object[4];

        modelo = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (int i = 0; i < listaLiquidaciones.size(); i++) {

            Registros[0] = String.valueOf(listaLiquidaciones.get(i).getLiquidacion());
            Registros[1] = listaLiquidaciones.get(i).getFecha();
            Registros[2] = listaLiquidaciones.get(i).getRecibo();
            Registros[3] = listaLiquidaciones.get(i).getPeriodo();

            modelo.addRow(Registros);
        }
        tablaLiquidaciones.setModel(modelo);
        tablaLiquidaciones.setAutoCreateRowSorter(true);

        TableCellRenderer headerRenderer = new FuncionesTabla(Color.darkGray, Color.darkGray);
        FuncionesTabla fecha = new FuncionesTabla();
        tablaLiquidaciones.getColumnModel().getColumn(0).setHeaderRenderer(headerRenderer);
        tablaLiquidaciones.getColumnModel().getColumn(1).setHeaderRenderer(headerRenderer);
        tablaLiquidaciones.getColumnModel().getColumn(2).setHeaderRenderer(headerRenderer);
        tablaLiquidaciones.getColumnModel().getColumn(3).setHeaderRenderer(headerRenderer);

        alinear();

        tablaLiquidaciones.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
        tablaLiquidaciones.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);

        tablaLiquidaciones.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
        tablaLiquidaciones.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);

        tablaLiquidaciones.getColumnModel().getColumn(0).setMinWidth(75);
        tablaLiquidaciones.getColumnModel().getColumn(0).setMaxWidth(75);
        tablaLiquidaciones.getColumnModel().getColumn(0).setPreferredWidth(75);
        tablaLiquidaciones.getColumnModel().getColumn(1).setMinWidth(75);
        tablaLiquidaciones.getColumnModel().getColumn(1).setMaxWidth(75);
        tablaLiquidaciones.getColumnModel().getColumn(1).setPreferredWidth(75);

        TableCellRenderer tableCellRenderer = new DefaultTableCellRenderer() {

            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                if (value instanceof Date) {
                    value = f.format(value);
                }
                return super.getTableCellRendererComponent(table, value, isSelected,
                        hasFocus, row, column);
            }
        };

        tablaLiquidaciones.getColumnModel().getColumn(1).setCellRenderer(tableCellRenderer);

    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(0, 130, 220, 255));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    public class HiloGenerarPDF extends Thread {

        JProgressBar progreso;

        public HiloGenerarPDF(JProgressBar progreso) {

            this.progreso = progreso;
        }

        @Override
        public void run() {
            conexion = new modelo.ConexionMariaDB();
            conexion.EstablecerConexion();
            int liquidacion = Integer.valueOf(tablaLiquidaciones.getValueAt(tablaLiquidaciones.getSelectedRow(), 0).toString());
            String fecha = tablaLiquidaciones.getValueAt(tablaLiquidaciones.getSelectedRow(), 1).toString();
            listaMatriculas = new ArrayList<modelo.Matricula>();
            Matricula.cargarMatriculas(conexion.getConnection(), listaMatriculas, liquidacion);
            conexion.cerrarConexion();
            int i = 0, n = listaMatriculas.size();
            progreso.setMaximum(n - 1);

            Map parametros = new HashMap();
            List<JasperPrint> jasperPrints = new ArrayList<>();
            while (i < n) {
                File folder = new File("C:\\Liquidaciones\\" + listaMatriculas.get(i).getMatricula());
                try {

                    jasperPrints.clear();
                    parametros.put("matricula", Integer.valueOf(listaMatriculas.get(i).getMatricula()));
                    parametros.put("liquidacion", liquidacion);
                    JasperReport cuerpo1 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Liquidacion-cuerpo1_1.jasper"));
                    JasperReport cuerpo3 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Liquidacion-cuerpo3_1.jasper"));
                    JasperPrint jPrint1 = JasperFillManager.fillReport(cuerpo1, parametros, conexion.Conectar());
                    JasperPrint jPrint2 = JasperFillManager.fillReport(cuerpo3, parametros, conexion.Conectar());

                    if (!jPrint1.getPages().isEmpty()) {
                        jasperPrints.add(jPrint1);
                    }

                    if (!jPrint2.getPages().isEmpty()) {
                        jasperPrints.add(jPrint2);
                    }

                    if (!folder.exists()) {
                        folder.mkdirs();
                    }
                    if (!jasperPrints.isEmpty()) {
                        JRPdfExporter exporter = new JRPdfExporter();
                        exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrints));
                        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(folder + "\\liquidacion_" + liquidacion + "_" + fecha + "_" + generateRandomText() + ".pdf"));
                        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
                        configuration.setCreatingBatchModeBookmarks(true); //add this so your bookmarks work, you may set other parameters
                        exporter.setConfiguration(configuration);
                        exporter.exportReport();
                    }
                } catch (JRException ex) {
                    Logger.getLogger(Liquidaciones.class.getName()).log(Level.SEVERE, null, ex);
                }
                progreso.setValue(i);
                i++;
            }

            JOptionPane.showMessageDialog(null, "Archivos generados");
            progreso.setValue(0);
            progreso.setMaximum(0);
            progreso.setVisible(false);

        }

    }

    void aplicarCambios() {
        progreso.setVisible(true);
        iniciarSplash();
        hiloGenerar = new HiloGenerarPDF(progreso);
        hiloGenerar.start();
        hiloGenerar = null;
    }

    void imprimirPeriodos() {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        //int liquidacion = Integer.valueOf(tablaLiquidaciones.getValueAt(tablaLiquidaciones.getSelectedRow(), 0).toString());
        listaLiquidacionesPeriodo = new ArrayList<>();
        Liquidacion.cargarLiquidaciones(conexion.getConnection(), listaLiquidacionesPeriodo, Integer.valueOf(txtPeriodoDesde.getText()), Integer.valueOf(txtPeriodoHasta.getText()));
        conexion.cerrarConexion();
        int i = 0, n = listaLiquidacionesPeriodo.size();
        Map parametros = new HashMap();

        File folder = new File("C:\\Liquidaciones\\" + txtMatricula.getText());

        try {
            while (i < n) {
                List<JasperPrint> jasperPrints = new ArrayList<>();
                jasperPrints.clear();
                parametros.put("matricula", Integer.valueOf(txtMatricula.getText()));
                parametros.put("liquidacion", listaLiquidacionesPeriodo.get(i).getLiquidacion());
                JasperReport cuerpo = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Liquidacion-cuerpo1_1.jasper"));
                JasperReport cuerpo3 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Liquidacion-cuerpo3_1.jasper"));
                JasperPrint jPrint1 = JasperFillManager.fillReport(cuerpo, parametros, conexion.Conectar());
                JasperPrint jPrint2 = JasperFillManager.fillReport(cuerpo3, parametros, conexion.Conectar());

                if (!jPrint1.getPages().isEmpty()) {
                    jasperPrints.add(jPrint1);
                }

                if (!jPrint2.getPages().isEmpty()) {
                    jasperPrints.add(jPrint2);
                }
                if (!jasperPrints.isEmpty()) {
                    JRPdfExporter exporter = new JRPdfExporter();
                    exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrints));

                    if (!folder.exists()) {
                        folder.mkdirs();
                    }

                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(folder + "\\" + listaLiquidacionesPeriodo.get(i).getLiquidacion() + ".pdf"));
                    SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
                    configuration.setCreatingBatchModeBookmarks(true); //add this so your bookmarks work, you may set other parameters
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                }
                i++;
            }

        } catch (JRException ex) {
            Logger.getLogger(Liquidaciones.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void imprimirPeriodo() {

        try {

            JasperPrint jasperprint = null;
            Map parametros = new HashMap();
            parametros.put("matricula", Integer.valueOf(LiquidacionMatricula.txtMatricula.getText()));
            parametros.put("liquidacion", Integer.valueOf(txtLiquidacion.getText()));
            JasperReport cuerpo = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Liquidacion-cuerpo1_1.jasper"));
            JasperReport cuerpo3 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Liquidacion-cuerpo3_1.jasper"));
            JasperPrint jPrint1 = JasperFillManager.fillReport(cuerpo, parametros, conexion.Conectar());
            JasperPrint jPrint2 = JasperFillManager.fillReport(cuerpo3, parametros, conexion.Conectar());

            int bandera_cuerpo3 = 0;
            if (jPrint1 != null) {
                jasperprint = jPrint1;
                if (jPrint2 != null) {
                    for (int indice = 0; indice < jPrint2.getPages().size(); indice++) {
                        JRPrintPage object = (JRPrintPage) jPrint2.getPages().get(indice);
                        jasperprint.addPage(object);
                    }
                    bandera_cuerpo3 = 1;
                }
            }

            if (jPrint2 != null && bandera_cuerpo3 == 0) {
                jasperprint = jPrint2;
            }
            if (jasperprint.getPages().size() > 0) {
                JasperPrintManager.printReport(jasperprint, true);

            }

        } catch (JRException ex) {
            Logger.getLogger(Liquidaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        txtNumeroLiq = new javax.swing.JTextField();
        btnGenerar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaLiquidaciones = new javax.swing.JTable();
        txtImprimirPeriodos = new javax.swing.JButton();
        progreso = new javax.swing.JProgressBar();
        btnImprimir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Número de liquidación"));

        txtNumeroLiq.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumeroLiqKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNumeroLiqKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtNumeroLiq, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtNumeroLiq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnGenerar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnGenerar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728930 - down download.png"))); // NOI18N
        btnGenerar.setText("Generar PDF");
        btnGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Liquidaciones"));

        tablaLiquidaciones.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaLiquidaciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaLiquidaciones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaLiquidacionesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaLiquidaciones);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtImprimirPeriodos.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtImprimirPeriodos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        txtImprimirPeriodos.setText("Imprimir periodos");
        txtImprimirPeriodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImprimirPeriodosActionPerformed(evt);
            }
        });

        btnImprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        btnImprimir.setText("Imprimir liquidación");
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSalir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnImprimir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnGenerar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtImprimirPeriodos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnGenerar)
                        .addGap(18, 18, 18)
                        .addComponent(btnImprimir)
                        .addGap(18, 18, 18)
                        .addComponent(txtImprimirPeriodos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 150, Short.MAX_VALUE)
                        .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(79, 79, 79)
                        .addComponent(btnSalir))
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtImprimirPeriodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImprimirPeriodosActionPerformed

        new PeriodosLiquidacion(null, true).setVisible(true);
        if (banderaImprimePeriodos == 1) {

            imprimirPeriodos();
        }

    }//GEN-LAST:event_txtImprimirPeriodosActionPerformed

    private void btnGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarActionPerformed

        if (tablaLiquidaciones.getSelectedRow() > -1) {
            aplicarCambios();
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un registro");
        }

    }//GEN-LAST:event_btnGenerarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();

    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtNumeroLiqKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroLiqKeyTyped

        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }

    }//GEN-LAST:event_txtNumeroLiqKeyTyped

    private void txtNumeroLiqKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroLiqKeyReleased

        TableRowSorter sorter = new TableRowSorter(modelo);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtNumeroLiq.getText() + ".*"));
        tablaLiquidaciones.setRowSorter(sorter);

    }//GEN-LAST:event_txtNumeroLiqKeyReleased

    private void tablaLiquidacionesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaLiquidacionesMouseClicked

        if (evt.getClickCount() == 2) {
            if (tablaLiquidaciones.getSelectedRow() > -1) {
                aplicarCambios();
            }
        }

    }//GEN-LAST:event_tablaLiquidacionesMouseClicked

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed

        new LiquidacionMatricula(null, true).setVisible(true);
        if (banderaImprime == 1) {

            imprimirPeriodo();
        }

    }//GEN-LAST:event_btnImprimirActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGenerar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnSalir;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JTable tablaLiquidaciones;
    private javax.swing.JButton txtImprimirPeriodos;
    private javax.swing.JTextField txtNumeroLiq;
    // End of variables declaration//GEN-END:variables
}
