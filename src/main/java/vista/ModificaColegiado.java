package vista;

import controlador.ConexionMariaDB;
import controlador.Funciones;
import controlador.ImageTest;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class ModificaColegiado extends javax.swing.JDialog {

    String id_colegiado = BuscaModificaCO.id_colegiado;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    String pic = "";
    Image imagen2;
    Funciones efechas = new Funciones();
    public static int banderaDirectivo = 0;
    public static String directivo, cargo;

    public ModificaColegiado(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icocbt.png")).getImage());
        this.setTitle("Colegiados");
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargarvalidadores();
        cargar();
        cargardatos();

    }

    void cargarvalidadores() {
        String sSQL = "";
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        cbovalidador.removeAllItems();
        sSQL = "SELECT apellido_usuario, nombre_usuario, validador_usuario FROM usuarios";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            cbovalidador.addItem("...");
            while (rs.next()) {
                cbovalidador.addItem(rs.getString("apellido_usuario") + ", " + rs.getString("nombre_usuario"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    private void cargar() {
        cbovitalicio.addItem("SI");
        cbovitalicio.addItem("NO");
        cbohorariolab.addItem("Mañana");
        cbohorariolab.addItem("Tarde");
        cbohorariolab.addItem("Mañana y Tarde");
        //txtdirector.setVisible(false);
        //cbovalidador.setVisible(false);
    }

    void cuil(int s, String dni) {
        String d = dni;
        long c, res = 0;
        long c0, c1, c2, c3, c4, c5, c6, c7, c8, c9;
        if (s == 0) {
            d = "20" + dni;

        } else {
            d = "27" + dni;

        }
        c = Long.valueOf(d);
        c0 = c % 10 * 2;
        c = c / 10;
        c1 = c % 10 * 3;
        c = c / 10;
        c2 = c % 10 * 4;
        c = c / 10;
        c3 = c % 10 * 5;
        c = c / 10;
        c4 = c % 10 * 6;
        c = c / 10;
        c5 = c % 10 * 7;
        c = c / 10;
        c6 = c % 10 * 2;
        c = c / 10;
        c7 = c % 10 * 3;
        c = c / 10;
        c8 = c % 10 * 4;
        //c = c / 10;
        c9 = c / 10 * 5;
        res = c0 + c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9;
        res = res % 11;
        if (res == 0) {
            d = d + "0";
        } else {
            if (res == 1) {
                if (s == 0) {
                    d = "23" + dni + "9";
                } else {
                    d = "23" + dni + "4";
                }
            } else {
                res = 11 - res;
                String r = String.valueOf(res);
                d = d + r;
            }
        }
        txtcuil.setText(d);
    }

    void cargardatos() {
        String sSQL = "";
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        sSQL = "SELECT * FROM colegiados where id_colegiados=" + id_colegiado;
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                if (id_colegiado.equals(rs.getString("id_colegiados"))) {

                    ///////////Datos Colegiado///////////////
                    txtnombre.setText(rs.getString("nombre_colegiado"));
                    cboTipoProf.setSelectedItem(rs.getString("tipo_profesional"));
                    txtndeacta.setText(rs.getString("ndeacta_colegiado"));
                    txtndefolio.setText(rs.getString("ndefolio_colegiado"));
                    txtmatricula.setText(rs.getString("matricula_colegiado"));
                    txtndelibro.setText(rs.getString("ndelibro_colegiado"));
                    if (rs.getBlob("foto_particular") != null) {
                        Blob blob = rs.getBlob("foto_particular");
                        //String nombre = rs.getObject("nombre").toString();
                        byte[] data = blob.getBytes(1, (int) blob.length());
                        BufferedImage img = null;
                        try {
                            img = ImageIO.read(new ByteArrayInputStream(data));
                        } catch (IOException ex) {
                            JOptionPane.showMessageDialog(null, ex);
                        }
                        Icon icon = new ImageIcon(img);
                        ///lista.add(imagen);
                        lblfoto1.setIcon(icon);
                    }
                    ////////////////////Preguntas//////////////////////
                    if (rs.getString("factura") != null) {
                        if (rs.getString("factura").equals("SI")) {
                            sifactura.setSelected(true);
                        } else {
                            nofactura.setSelected(true);
                        }
                    }

                    if (rs.getString("essociedad") != null) {
                        if (rs.getString("essociedad").equals("SI")) {
                            sisociedad.setSelected(true);
                        } else {
                            nosociedad.setSelected(true);
                        }
                    }

                    if (rs.getString("atravescolegio") != null) {
                        if (rs.getString("atravescolegio").equals("SI")) {
                            sicolegio.setSelected(true);
                        } else {
                            nocolegio.setSelected(true);
                        }
                    }

                    ////////////////////Validador/////////////////////
                    if (cbovalidador.getItemCount() > 0) {
                        if (rs.getObject("validador") != null) {
                            cbovalidador.setSelectedIndex(rs.getInt("validador"));
                        } else {
                            cbovalidador.setSelectedIndex(0);
                        }
                    }
                    ////////////////////Datos Particular////////////////////

                    if (rs.getString("tipodedocumento_particular") != null) {
                        cbotipodoc.setSelectedItem(rs.getString("tipodedocumento_particular"));
                    }
                    txtdocumento.setText(rs.getString("numerodedocumento_particular"));
                    if (rs.getString("sexo") != null) {
                        cbosexo.setSelectedItem(rs.getString("sexo"));
                    }
                    txtcuil.setText(rs.getString("cuil_colegiado"));
                    if (rs.getString("fechadenacimiento_particular") != null) {
                        fechaNac.setDate(StringADateOk(rs.getString("fechadenacimiento_particular")));
                    }
                    txtdireccion.setText(rs.getString("direccion_particular") + " " + rs.getString("numerodireccion_particular"));
                    txtlocalidad.setText(rs.getString("localidad_particular"));
                    txtcodigopostal.setText(rs.getString("codigopostal_particular"));
                    txttelfijo.setText(rs.getString("telefono_particular"));
                    txttelcelular.setText(rs.getString("celular_particular"));
                    txtmail.setText(rs.getString("mail_particular"));
                    if (rs.getString("fechadeegreso_particular") != null) {
                        fechaegreso.setDate(StringADateOk(rs.getString("fechadeegreso_particular")));
                        System.out.println(StringADateOk(rs.getString("fechadeegreso_particular")));
                    }
                    txtntitulo.setText(rs.getString("numerodetitulo_particular"));
                    if (rs.getString("vitalicio_particular") != null) {
                        cbovitalicio.setSelectedItem("vitalicio_particular");
                    }
                    if (rs.getString("fechadedoctorado_particular") != null) {
                        fechadoctorado.setDate(StringADateOk(rs.getString("fechadedoctorado_particular")));
                    }
                    txtespecialidad.setText(rs.getString("especialidad_particular"));

                    ///////////////////////Datos laboratorio////////////////////////
                    if (rs.getString("essociedad").equals("NO") && rs.getString("factura").equals("SI")) {
                        txtdirector.setText(rs.getString("nombre_colegiado"));
                    }
                    txtlocalidadlab.setText(rs.getString("localidad_laboratorio"));
                    txtdireccionlab.setText(rs.getString("direccion_laboratorio") + " " + rs.getString("numerodireccion_laboratorio"));
                    txtcodigopostallab.setText(rs.getString("codigopostal_laboratorio"));
                    txtweblab.setText(rs.getString("http_laboratorio"));
                    txtmaillab.setText(rs.getString("mail_laboratorio"));
                    txttelcelularlab.setText(rs.getString("celular_laboratorio"));
                    txtfaxlab.setText(rs.getString("fax_laboratorio"));
                    txttelfijolab.setText(rs.getString("telefono_laboratorio"));
                    if (rs.getString("horarioantecion_laboratorio") != null) {
                        if (rs.getString("horarioantecion_laboratorio").equals("8 a 12 hs")) {
                            cbohorariolab.setSelectedIndex(0);
                        }
                    }
                    txtespecialidadlab.setText(rs.getString("especialidad_laboratorio"));
                    txtusuario.setText(rs.getString("usuario_laboratorio"));
                    txtcontraseña.setText(rs.getString("contraseña_laboratorio"));
                    txtANSSALlab.setText(rs.getString("anssal_laboratorio"));
                    if (rs.getString("fechaalta_laboratorio") != null) {
                        fechaalta.setDate(StringADateOk(rs.getString("fechaalta_laboratorio")));
                    }
                    if (rs.getString("fechavencimientoanssal_laboratorio") != null) {
                        fechavenANSSAL.setDate(StringADateOk(rs.getString("fechavencimientoanssal_laboratorio")));
                    }
                    if (rs.getString("fechabaja_laboratorio") != null) {
                        fechabaja.setDate(StringADateOk(rs.getString("fechabaja_laboratorio")));
                    }
                }
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    private Image ConvertirImagen(byte[] bytes) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName("png");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        return reader.read(0, param);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        Colegiado = new javax.swing.JPanel();
        btnsiguiente = new javax.swing.JButton();
        btnatras = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtndelibro = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtndefolio = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtndeacta = new javax.swing.JTextField();
        txtmatricula = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        sifactura = new javax.swing.JCheckBox();
        jLabel9 = new javax.swing.JLabel();
        nofactura = new javax.swing.JCheckBox();
        jLabel10 = new javax.swing.JLabel();
        sisociedad = new javax.swing.JCheckBox();
        nosociedad = new javax.swing.JCheckBox();
        jLabel41 = new javax.swing.JLabel();
        sicolegio = new javax.swing.JCheckBox();
        nocolegio = new javax.swing.JCheckBox();
        cbovalidador = new javax.swing.JComboBox();
        jLabel43 = new javax.swing.JLabel();
        cboTipoProf = new javax.swing.JComboBox();
        jPanel7 = new javax.swing.JPanel();
        btnexaminar2 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        lblfoto1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        Particular = new javax.swing.JPanel();
        btnsiguiente1 = new javax.swing.JButton();
        btnatras1 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        cbotipodoc = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        txtdocumento = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        txtcuil = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtlocalidad = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtcodigopostal = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txttelfijo = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txttelcelular = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtmail = new javax.swing.JTextField();
        cbosexo = new javax.swing.JComboBox();
        jLabel47 = new javax.swing.JLabel();
        fechaNac = new com.toedter.calendar.JDateChooser();
        jPanel5 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        fechaegreso = new com.toedter.calendar.JDateChooser();
        fechadoctorado = new com.toedter.calendar.JDateChooser();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtntitulo = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txtespecialidad = new javax.swing.JTextField();
        cbovitalicio = new javax.swing.JComboBox();
        jLabel23 = new javax.swing.JLabel();
        Laboratorio = new javax.swing.JPanel();
        btnModificar = new javax.swing.JButton();
        btnAtrasLab = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        txtdireccionlab = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txtcodigopostallab = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        txtmaillab = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txtweblab = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txttelcelularlab = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtfaxlab = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txttelfijolab = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        cbohorariolab = new javax.swing.JComboBox();
        jLabel38 = new javax.swing.JLabel();
        txtespecialidadlab = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtusuario = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txtcontraseña = new javax.swing.JPasswordField();
        jLabel24 = new javax.swing.JLabel();
        txtANSSALlab = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        fechaalta = new com.toedter.calendar.JDateChooser();
        jLabel35 = new javax.swing.JLabel();
        fechavenANSSAL = new com.toedter.calendar.JDateChooser();
        jLabel34 = new javax.swing.JLabel();
        fechabaja = new com.toedter.calendar.JDateChooser();
        jLabel42 = new javax.swing.JLabel();
        txtdirector = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        txtlocalidadlab = new javax.swing.JTextField();
        btncancelar2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(650, 400));

        Colegiado.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        Colegiado.setPreferredSize(new java.awt.Dimension(580, 350));
        Colegiado.setVerifyInputWhenFocusTarget(false);

        btnsiguiente.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728938 - flag green.png"))); // NOI18N
        btnsiguiente.setMnemonic('a');
        btnsiguiente.setText("Siguiente");
        btnsiguiente.setPreferredSize(null);
        btnsiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsiguienteActionPerformed(evt);
            }
        });

        btnatras.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnatras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnatras.setMnemonic('a');
        btnatras.setText("Cancelar");
        btnatras.setPreferredSize(null);
        btnatras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnatrasActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Matriculación", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtndelibro.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtndelibro.setForeground(new java.awt.Color(0, 102, 204));
        txtndelibro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtndelibroKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtndelibroKeyReleased(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("N° de Libro:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("N° de Folio:");

        txtndefolio.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtndefolio.setForeground(new java.awt.Color(0, 102, 204));
        txtndefolio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtndefolioKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtndefolioKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("N° Acta:");

        txtndeacta.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtndeacta.setForeground(new java.awt.Color(0, 102, 204));
        txtndeacta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtndeactaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtndeactaKeyReleased(evt);
            }
        });

        txtmatricula.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmatricula.setForeground(new java.awt.Color(0, 102, 204));
        txtmatricula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Matricula:");

        txtnombre.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Ap. y Nom.:");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        sifactura.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sifactura.setText("Si");
        sifactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sifacturaActionPerformed(evt);
            }
        });
        sifactura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sifacturaKeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("¿ El Colegiado Factura ?");

        nofactura.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nofactura.setText("No");
        nofactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nofacturaActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("¿ Es Sociedad ?");

        sisociedad.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sisociedad.setText("Si");
        sisociedad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sisociedadActionPerformed(evt);
            }
        });

        nosociedad.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nosociedad.setText("No");
        nosociedad.setOpaque(false);
        nosociedad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nosociedadActionPerformed(evt);
            }
        });

        jLabel41.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel41.setText("¿ Factura por el Colegio ?");

        sicolegio.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sicolegio.setText("Si");
        sicolegio.setName(""); // NOI18N
        sicolegio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sicolegioActionPerformed(evt);
            }
        });

        nocolegio.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nocolegio.setText("No");
        nocolegio.setName(""); // NOI18N
        nocolegio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nocolegioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(jLabel41)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(sifactura)
                        .addGap(18, 18, 18)
                        .addComponent(nofactura))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(sicolegio)
                            .addGap(18, 18, 18)
                            .addComponent(nocolegio))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(sisociedad)
                            .addGap(18, 18, 18)
                            .addComponent(nosociedad))))
                .addGap(17, 17, 17))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sifactura)
                    .addComponent(jLabel9)
                    .addComponent(nofactura))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(sisociedad)
                        .addComponent(nosociedad))
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel41)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(sicolegio)
                        .addComponent(nocolegio)))
                .addContainerGap(10, Short.MAX_VALUE))
        );

        cbovalidador.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbovalidador.setForeground(new java.awt.Color(0, 102, 204));
        cbovalidador.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbovalidadorKeyPressed(evt);
            }
        });

        jLabel43.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel43.setText("Tipo de Prof:");

        cboTipoProf.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cboTipoProf.setForeground(new java.awt.Color(0, 102, 204));
        cboTipoProf.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "B", "T" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtmatricula, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtndeacta, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6)))
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtndefolio)
                    .addComponent(txtndelibro)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel43, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboTipoProf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(87, 87, 87)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(cbovalidador, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel43)
                                .addComponent(cboTipoProf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4)
                            .addComponent(txtndeacta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(txtndefolio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtndelibro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbovalidador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Foto", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        btnexaminar2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnexaminar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnexaminar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexaminar2ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728900 - application browser.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        lblfoto1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblfoto1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(btnexaminar2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblfoto1, javax.swing.GroupLayout.DEFAULT_SIZE, 182, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnexaminar2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 44, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 43, Short.MAX_VALUE)
        );

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        jButton1.setText("Imprimir Carnet");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ColegiadoLayout = new javax.swing.GroupLayout(Colegiado);
        Colegiado.setLayout(ColegiadoLayout);
        ColegiadoLayout.setHorizontalGroup(
            ColegiadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ColegiadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ColegiadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ColegiadoLayout.createSequentialGroup()
                        .addComponent(btnatras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ColegiadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(ColegiadoLayout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                        .addComponent(btnsiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        ColegiadoLayout.setVerticalGroup(
            ColegiadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ColegiadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ColegiadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addGroup(ColegiadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsiguiente, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ColegiadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnatras, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Colegiado", Colegiado);

        Particular.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        Particular.setPreferredSize(new java.awt.Dimension(580, 360));

        btnsiguiente1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsiguiente1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728938 - flag green.png"))); // NOI18N
        btnsiguiente1.setMnemonic('a');
        btnsiguiente1.setText("Siguiente");
        btnsiguiente1.setPreferredSize(null);
        btnsiguiente1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsiguiente1ActionPerformed(evt);
            }
        });

        btnatras1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnatras1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728939 - flag yellow.png"))); // NOI18N
        btnatras1.setMnemonic('a');
        btnatras1.setText("Atras");
        btnatras1.setPreferredSize(new java.awt.Dimension(108, 23));
        btnatras1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnatras1ActionPerformed(evt);
            }
        });

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Campos Obligatorios", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Tipo:");

        cbotipodoc.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbotipodoc.setForeground(new java.awt.Color(0, 102, 204));
        cbotipodoc.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "DNI", "LE", "LC", "CI" }));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Numero:");

        txtdocumento.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdocumento.setForeground(new java.awt.Color(0, 102, 204));
        txtdocumento.setNextFocusableComponent(cbosexo);
        txtdocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdocumentoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdocumentoKeyReleased(evt);
            }
        });

        jLabel44.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel44.setText("N° CUIL:");

        txtcuil.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcuil.setForeground(new java.awt.Color(0, 102, 204));
        txtcuil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcuilKeyReleased(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText("Fecha Nac:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Dirección:");

        txtdireccion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdireccionKeyReleased(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("Localidad:");

        txtlocalidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtlocalidad.setForeground(new java.awt.Color(0, 102, 204));
        txtlocalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlocalidadKeyReleased(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setText("C. P.:");

        txtcodigopostal.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcodigopostal.setForeground(new java.awt.Color(0, 102, 204));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setText("Tel. Fijo:");

        txttelfijo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txttelfijo.setForeground(new java.awt.Color(0, 102, 204));
        txttelfijo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttelfijoKeyReleased(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("Tel. Celular:");

        txttelcelular.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txttelcelular.setForeground(new java.awt.Color(0, 102, 204));
        txttelcelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttelcelularKeyReleased(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Mail:");

        txtmail.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmail.setForeground(new java.awt.Color(0, 102, 204));
        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmailKeyReleased(evt);
            }
        });

        cbosexo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbosexo.setForeground(new java.awt.Color(0, 102, 204));
        cbosexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "F", "M" }));
        cbosexo.setNextFocusableComponent(txtcuil);
        cbosexo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbosexoKeyPressed(evt);
            }
        });

        jLabel47.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel47.setText("Sexo:");

        fechaNac.setForeground(new java.awt.Color(0, 102, 204));
        fechaNac.setDateFormatString("dd-MM-yyyy");
        fechaNac.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbotipodoc, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtdocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(jLabel47)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbosexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel44)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcuil, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fechaNac, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                        .addGap(26, 26, 26))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtdireccion)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtlocalidad)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtcodigopostal, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txttelfijo))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttelcelular, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtmail)))
                        .addContainerGap())))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbotipodoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8)
                        .addComponent(jLabel7)
                        .addComponent(txtdocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel47)
                        .addComponent(cbosexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel44)
                        .addComponent(txtcuil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(fechaNac, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtlocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txtcodigopostal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(txttelfijo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtmail)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17)
                        .addComponent(txttelcelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setText("Fec. de Egreso:");

        fechaegreso.setForeground(new java.awt.Color(0, 102, 204));
        fechaegreso.setDateFormatString("dd-MM-yyyy");
        fechaegreso.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        fechadoctorado.setForeground(new java.awt.Color(0, 102, 204));
        fechadoctorado.setDateFormatString("dd-MM-yyyy");
        fechadoctorado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel20.setText("Fec. de Doctorado:");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("N° de Titulo:");

        txtntitulo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtntitulo.setForeground(new java.awt.Color(0, 102, 204));
        txtntitulo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtntituloKeyReleased(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setText("Especialidad:");

        txtespecialidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtespecialidad.setForeground(new java.awt.Color(0, 102, 204));
        txtespecialidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtespecialidadKeyReleased(evt);
            }
        });

        cbovitalicio.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbovitalicio.setForeground(new java.awt.Color(0, 102, 204));

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel23.setText("¿ Es Vitalicio?");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(fechaegreso, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                    .addComponent(fechadoctorado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtntitulo)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel23)
                        .addGap(18, 18, 18)
                        .addComponent(cbovitalicio, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtespecialidad)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(fechaegreso, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel23)
                        .addComponent(cbovitalicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21)
                        .addComponent(txtntitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(fechadoctorado, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel22)
                        .addComponent(txtespecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout ParticularLayout = new javax.swing.GroupLayout(Particular);
        Particular.setLayout(ParticularLayout);
        ParticularLayout.setHorizontalGroup(
            ParticularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ParticularLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ParticularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ParticularLayout.createSequentialGroup()
                        .addComponent(btnatras1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsiguiente1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        ParticularLayout.setVerticalGroup(
            ParticularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ParticularLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(ParticularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsiguiente1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnatras1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Datos Particular", Particular);

        Laboratorio.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        btnModificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnModificar.setMnemonic('a');
        btnModificar.setText("Modificar");
        btnModificar.setPreferredSize(null);
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnAtrasLab.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAtrasLab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728939 - flag yellow.png"))); // NOI18N
        btnAtrasLab.setMnemonic('a');
        btnAtrasLab.setText("Atras");
        btnAtrasLab.setPreferredSize(new java.awt.Dimension(108, 23));
        btnAtrasLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasLabActionPerformed(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Laboratorio", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel26.setText("Dirección:");

        txtdireccionlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdireccionlab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel31.setText("C.P.:");

        txtcodigopostallab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcodigopostallab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel37.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel37.setText("Pagina Web:");

        txtmaillab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmaillab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel30.setText("Mail:");

        txtweblab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtweblab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel29.setText("Tel Celular:");

        txttelcelularlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txttelcelularlab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel36.setText("Fax:");

        txtfaxlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtfaxlab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel28.setText("Tel Fijo:");

        txttelfijolab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txttelfijolab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel39.setText("Horario:");

        cbohorariolab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbohorariolab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel38.setText("Especialidad: ");

        txtespecialidadlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtespecialidadlab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel25.setText("Usuario:");

        txtusuario.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtusuario.setForeground(new java.awt.Color(0, 102, 204));

        jLabel40.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel40.setText("Contraseña:");

        txtcontraseña.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcontraseña.setForeground(new java.awt.Color(0, 102, 204));

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel24.setText("ANSSAL:");

        txtANSSALlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtANSSALlab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel33.setText("Fecha de Alta:");

        fechaalta.setForeground(new java.awt.Color(0, 102, 204));
        fechaalta.setDateFormatString("dd-MM-yyyy");
        fechaalta.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel35.setText("FV de ANSSAL:");

        fechavenANSSAL.setForeground(new java.awt.Color(0, 102, 204));
        fechavenANSSAL.setDateFormatString("dd-MM-yyyy");
        fechavenANSSAL.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel34.setText("Fecha de Baja:");

        fechabaja.setForeground(new java.awt.Color(0, 102, 204));
        fechabaja.setDateFormatString("dd-MM-yyyy");
        fechabaja.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        jLabel42.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel42.setText("Director Tecnico:");

        txtdirector.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdirector.setForeground(new java.awt.Color(0, 102, 204));
        txtdirector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdirectorActionPerformed(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel27.setText("Localidad:");

        txtlocalidadlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtlocalidadlab.setForeground(new java.awt.Color(0, 102, 204));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel38)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtespecialidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel40)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcontraseña))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtdireccionlab)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcodigopostallab, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel29)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttelcelularlab, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel36)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtfaxlab))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel37)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtmaillab, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttelfijolab, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel39)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbohorariolab, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtweblab))))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtdirector, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlocalidadlab))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtANSSALlab, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fechaalta, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fechabaja, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel35)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fechavenANSSAL, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtdirector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtlocalidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtdireccionlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcodigopostallab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtmaillab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37)
                    .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtweblab, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txttelfijolab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel39)
                        .addComponent(cbohorariolab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtfaxlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txttelcelularlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel29)))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel38)
                        .addComponent(txtespecialidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel25)
                        .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel40)
                        .addComponent(txtcontraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtANSSALlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fechaalta, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fechabaja, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fechavenANSSAL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout LaboratorioLayout = new javax.swing.GroupLayout(Laboratorio);
        Laboratorio.setLayout(LaboratorioLayout);
        LaboratorioLayout.setHorizontalGroup(
            LaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LaboratorioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(LaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(LaboratorioLayout.createSequentialGroup()
                        .addComponent(btnAtrasLab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        LaboratorioLayout.setVerticalGroup(
            LaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LaboratorioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addGroup(LaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAtrasLab, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Datos Laboratorio", Laboratorio);

        btncancelar2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btncancelar2.setMnemonic('c');
        btncancelar2.setText("Salir");
        btncancelar2.setPreferredSize(new java.awt.Dimension(108, 23));
        btncancelar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelar2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 753, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btncancelar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btncancelar2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsiguienteActionPerformed
        if (txtnombre.getText().equals("") || txtmatricula.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Faltan Ingresar Datos");
        } else {
            jTabbedPane1.setSelectedIndex(1);
        }
    }//GEN-LAST:event_btnsiguienteActionPerformed

    private void btnatrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnatrasActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnatrasActionPerformed

    private void btnsiguiente1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsiguiente1ActionPerformed
        try {

            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            String SQL = "SELECT periodo FROM periodos";
            int periodo;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            rs.last();
            periodo = rs.getInt("periodo");
            String sSQL;
            String essociedad = "";
            String factura = "";
            String atravescolegio = "";
            if (sisociedad.isSelected()) {
                essociedad = "SI";
            }
            if (nosociedad.isSelected()) {
                essociedad = "NO";
            }
            if (sifactura.isSelected()) {
                factura = "SI";
            }
            if (nofactura.isSelected()) {
                factura = "NO";
            }
            if (sicolegio.isSelected()) {
                atravescolegio = "SI";
            }
            if (nocolegio.isSelected()) {
                atravescolegio = "NO";
            }
            String fechadenacimiento_particular, fechadeegreso_particular, fechadedoctorado_particular, fechaalta_laboratorio, fechabaja_laboratorio, fechavencimientoanssal_laboratorio;

            fechadenacimiento_particular = efechas.getfechaok(fechaNac);

            if (fechaegreso.getDate() != null) {
                Date fechfinal = fechaegreso.getDate();
                fechadeegreso_particular = formato.format(fechfinal.getTime());
            } else {
                fechadeegreso_particular = "";
            }
            if (fechadoctorado.getDate() != null) {
                Date fechfinal = fechadoctorado.getDate();
                fechadedoctorado_particular = formato.format(fechfinal.getTime());
            } else {
                fechadedoctorado_particular = "";
            }

            if (!pic.equals("")) {
                guardarImagen(pic);
            }
            /// Pregunto para Agregar
            sSQL = "UPDATE colegiados SET cuil_colegiado=?, matricula_colegiado=?, nombre_colegiado=?, ndeacta_colegiado=?, ndefolio_colegiado=?, ndelibro_colegiado=?,"
                    + " essociedad=?, factura=?, atravescolegio=?, celular_particular=?, codigopostal_particular=?, direccion_particular=?,"
                    + " especialidad_particular=?, fechadedoctorado_particular=?, fechadeegreso_particular=?,"
                    + " fechadenacimiento_particular=?, localidad_particular=?, mail_particular=?,"
                    + " numerodedocumento_particular=?, numerodetitulo_particular=?,"
                    + " telefono_particular=?, tipodedocumento_particular=?, vitalicio_particular=?, periodos=?, estado_periodo=?, sexo=? WHERE id_colegiados=" + id_colegiado;

            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, txtcuil.getText());
            pst.setString(2, txtmatricula.getText());
            pst.setString(3, txtnombre.getText());
            pst.setString(4, txtndeacta.getText());
            pst.setString(5, txtndefolio.getText());
            pst.setString(6, txtndelibro.getText());
            pst.setString(7, essociedad);
            pst.setString(8, factura);
            pst.setString(9, atravescolegio);
            pst.setString(10, txttelcelular.getText());
            pst.setString(11, txtcodigopostal.getText());
            pst.setString(12, txtdireccion.getText());
            pst.setString(13, txtespecialidad.getText());
            pst.setString(14, fechadedoctorado_particular);
            pst.setString(15, fechadeegreso_particular);
            pst.setString(16, fechadenacimiento_particular);
            pst.setString(17, txtlocalidad.getText());
            pst.setString(18, txtmail.getText());
            pst.setString(19, txtdocumento.getText());
            pst.setString(20, txtntitulo.getText());
            pst.setString(21, txttelfijo.getText());
            pst.setString(22, cbotipodoc.getSelectedItem().toString());
            pst.setString(23, cbovitalicio.getSelectedItem().toString());
            pst.setInt(24, periodo);
            pst.setInt(25, 0);
            pst.setString(26, cbosexo.getSelectedItem().toString());
            int n = pst.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");

            }

            if (nofactura.isSelected()) {
                this.dispose();
            }
            if (sifactura.isSelected() && nosociedad.isSelected()) {
                jTabbedPane1.setEnabledAt(2, true);
                jTabbedPane1.setSelectedIndex(2);
                btnModificar.setText("Agregar");
                jTabbedPane1.setEnabledAt(1, false);
                jTabbedPane1.setEnabledAt(0, false);
                txtdirector.setEnabled(false);
            }
            if (sifactura.isSelected() && sisociedad.isSelected()) {
                jTabbedPane1.setEnabledAt(2, true);
                jTabbedPane1.setSelectedIndex(2);
                btnModificar.setText("Agregar");
                jTabbedPane1.setEnabledAt(1, false);
                jTabbedPane1.setEnabledAt(0, false);
                txtdirector.setEnabled(true);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AgregarColegiados.class.getName()).log(Level.SEVERE, null, ex);
        }
        jTabbedPane1.setSelectedIndex(2);

    }//GEN-LAST:event_btnsiguiente1ActionPerformed

    private void btnatras1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnatras1ActionPerformed
        jTabbedPane1.setSelectedIndex(0);

    }//GEN-LAST:event_btnatras1ActionPerformed

    public boolean guardarImagen(String ruta) {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String insert = "UPDATE colegiados SET foto_particular=? WHERE id_colegiados=" + id_colegiado;
        FileInputStream fis = null;
        PreparedStatement ps = null;
        try {
            cn.setAutoCommit(false);
            File file = new File(ruta);
            fis = new FileInputStream(file);
            ps = cn.prepareStatement(insert);
            ps.setBinaryStream(1, fis, (int) file.length());
            ps.executeUpdate();
            cn.commit();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "\n Los Datos se insertaron erroneamente...");
        } finally {
            try {
                ps.close();
                fis.close();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex + "\n Los Datos se insertaron erroneamente...");
            }
        }
        return false;
    }

    void traerimagen() {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String sql7 = "Select foto_particular from colegiados where id_colegiados=" + id_colegiado;
        try {
            Statement St7 = cn.createStatement();
            ResultSet rs7 = St7.executeQuery(sql7);
            while (rs7.next()) {
                byte[] img = rs7.getBytes(1);
                if (img != null) {
                    try {
                        imagen2 = ConvertirImagen(img);
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                } else {
                    imagen2 = null;
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "\n Los Datos se insertaron erroneamente...");
        }
    }

    public String revertirfecha(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        ////Dia////
        for (i = 8; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 5; i <= 6; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        /////Año/////
        for (i = 0; i <= 3; i++) {
            salida = salida + entrada.charAt(i);
        }
        return salida;
    }

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        try {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            String fechaalta_laboratorio, fechabaja_laboratorio, fechavencimientoanssal_laboratorio;
            String SQL = "SELECT periodo FROM periodos";
            int periodo;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            rs.last();
            periodo = rs.getInt("periodo");

            if (sifactura.isSelected() && nosociedad.isSelected()) {
                String sSQL;

                if (fechaalta.getDate() != null) {
                    Date fechfinal = fechaalta.getDate();
                    fechaalta_laboratorio = formato.format(fechfinal.getTime());
                } else {
                    fechaalta_laboratorio = "";
                }
                if (fechabaja.getDate() != null) {
                    Date fechfinal = fechabaja.getDate();
                    fechabaja_laboratorio = formato.format(fechfinal.getTime());
                } else {
                    fechabaja_laboratorio = "";
                }
                if (fechavenANSSAL.getDate() != null) {
                    Date fechfinal = fechavenANSSAL.getDate();
                    fechavencimientoanssal_laboratorio = formato.format(fechfinal.getTime());
                } else {
                    fechavencimientoanssal_laboratorio = "";
                }
                ///JOptionPane.showMessageDialog(null, "2");
                sSQL = "UPDATE colegiados SET anssal_laboratorio=?,"
                        + " celular_laboratorio=?, codigopostal_laboratorio=?, contraseña_laboratorio=?, direccion_laboratorio=?,"
                        + " especialidad_laboratorio=?, fax_laboratorio=?, fechaalta_laboratorio=?, fechabaja_laboratorio=?,"
                        + " fechavencimientoanssal_laboratorio=?, horarioantecion_laboratorio=?, http_laboratorio=?,"
                        + " localidad_laboratorio=?, mail_laboratorio=?, telefono_laboratorio=?,"
                        + " usuario_laboratorio=? WHERE id_colegiados=" + id_colegiado;
                PreparedStatement pst = cn.prepareStatement(sSQL);
                pst.setString(1, txtANSSALlab.getText());
                pst.setString(2, txttelcelularlab.getText());
                pst.setString(3, txtcodigopostallab.getText());
                pst.setString(4, txtcontraseña.getText());
                pst.setString(5, txtdireccionlab.getText());
                pst.setString(6, txtespecialidadlab.getText());
                pst.setString(7, txtfaxlab.getText());
                pst.setString(8, fechaalta_laboratorio);
                pst.setString(9, fechabaja_laboratorio);
                pst.setString(10, fechavencimientoanssal_laboratorio);
                pst.setString(11, cbohorariolab.getSelectedItem().toString());
                pst.setString(12, txtweblab.getText());
                pst.setString(13, txtlocalidad.getText());
                pst.setString(14, txtmaillab.getText());
                pst.setString(15, txttelfijolab.getText());
                pst.setString(16, txtusuario.getText());
                int n = pst.executeUpdate();
                /////////////////////////////////////////////////////////////////               
                guardarImagen(pic);
                ////////////////////////////////////////////////////////////////////////
                if (n > 0) {

                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                    int opcion = JOptionPane.showConfirmDialog(this, "Desea imprimir el carnet?", "Mensaje", JOptionPane.YES_NO_OPTION);
                    if (opcion == 0) {
                        try {
                            traerimagen();
                            JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet.jasper"));
                            Map parametros = new HashMap();
                            if (cboTipoProf.getSelectedItem().toString().equals("B")) {
                                if (cbosexo.getSelectedItem().toString().equals("M")) {
                                    parametros.put("parametro", "BIOQUÍMICO");
                                } else {
                                    parametros.put("parametro", "BIOQUÍMICA");
                                }
                            } else {
                                if (cbosexo.getSelectedItem().toString().equals("M")) {
                                    parametros.put("parametro", "TECNICO");
                                } else {
                                    parametros.put("parametro", "TECNICA");
                                }

                            }
                            parametros.put("Matricula", txtmatricula.getText());
                            parametros.put("Apellido", txtnombre.getText());
                            parametros.put("FechaNac", efechas.getfechaok(fechaNac));
                            parametros.put("Dni", txtdocumento.getText());
                            parametros.put("Foto", imagen2);
                            JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                            JasperPrintManager.printReport(jPrint, false);

                        } catch (Exception e) {
                            
                            JOptionPane.showMessageDialog(null, e);
                        }
                        JOptionPane.showMessageDialog(null, "Dar vuelta la hoja para imprir el reverso...");
                        try {
                            JasperReport reporte_sobre2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet2.jasper"));
                            Map parametros = new HashMap();
                            parametros.put("Domicilio", txtdireccion.getText());
                            parametros.put("Localidad", txtlocalidad.getText());
                            parametros.put("Provincia", "TUCUMAN");
                            parametros.put("FechaEgreso", efechas.getfechaok(fechaegreso));
                            parametros.put("FechaAlta", revertirfecha(fechaalta_laboratorio));
                            JasperPrint jPrint2 = JasperFillManager.fillReport(reporte_sobre2, parametros, new JREmptyDataSource());
                            JasperPrintManager.printReport(jPrint2, false);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                    }
                    this.dispose();
                }

            }
            if (sifactura.isSelected() && sisociedad.isSelected()) {
                String sSQL;

                sSQL = "UPDATE colegiados SET"
                        + " anssal_laboratorio=?,"
                        + " celular_laboratorio=?,"
                        + " codigopostal_laboratorio=?,"
                        + " contraseña_laboratorio=?,"
                        + " direccion_laboratorio=?,"
                        + " especialidad_laboratorio=?,"
                        + " fax_laboratorio=?,"
                        + " fechaalta_laboratorio=?,"
                        + " fechabaja_laboratorio=?,"
                        + " fechavencimientoanssal_laboratorio=?,"
                        + " horarioantecion_laboratorio=?,"
                        + " http_laboratorio=?,"
                        + " localidad_laboratorio=?,"
                        + " mail_laboratorio=?,"
                        + " telefono_laboratorio=?,"
                        + " usuario_laboratorio=?,"
                        + " director_laboratorio=?"
                        + " WHERE id_colegiados=" + id_colegiado;
                PreparedStatement pst = cn.prepareStatement(sSQL);

                pst.setString(1, txtANSSALlab.getText());
                pst.setString(2, txttelcelularlab.getText());
                pst.setString(3, txtcodigopostallab.getText());
                pst.setString(4, txtcontraseña.getText());
                pst.setString(5, txtdireccionlab.getText());
                pst.setString(6, txtespecialidadlab.getText());
                pst.setString(7, txtfaxlab.getText());
                pst.setString(8, efechas.getfechaok(fechaalta));
                pst.setString(9, efechas.getfechaok(fechabaja));
                pst.setString(10, efechas.getfechaok(fechavenANSSAL));
                pst.setString(11, cbohorariolab.getSelectedItem().toString());
                pst.setString(12, txtweblab.getText());
                pst.setString(13, txtlocalidadlab.getText());
                pst.setString(14, txtmaillab.getText());
                pst.setString(15, txttelfijolab.getText());
                pst.setString(16, txtusuario.getText());
                pst.setString(17, "");
                int n = pst.executeUpdate();
                guardarImagen(pic);

                if (n > 0) {

                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                    int opcion = JOptionPane.showConfirmDialog(this, "Desea imprimir el carnet?", "Mensaje", JOptionPane.YES_NO_OPTION);
                    if (opcion == 0) {
                        try {
                            traerimagen();
                            JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet.jasper"));
                            Map parametros = new HashMap();
                            if (cboTipoProf.getSelectedItem().toString().equals("B")) {
                                if (cbosexo.getSelectedItem().toString().equals("M")) {
                                    parametros.put("parametro", "BIOQUÍMICO");
                                } else {
                                    parametros.put("parametro", "BIOQUÍMICA");
                                }
                            } else {
                                if (cbosexo.getSelectedItem().toString().equals("M")) {
                                    parametros.put("parametro", "TECNICO");
                                } else {
                                    parametros.put("parametro", "TECNICA");
                                }

                            }
                            parametros.put("Matricula", cboTipoProf.getSelectedItem().toString() + txtmatricula.getText());
                            parametros.put("Apellido", txtnombre.getText());
                            parametros.put("FechaNac", efechas.getfechaok(fechaNac));
                            parametros.put("Dni", txtdocumento.getText());
                            parametros.put("Foto", imagen2);
                            JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                            JasperPrintManager.printReport(jPrint, false);

                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                        try {
                            JasperReport reporte_sobre2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet2.jasper"));
                            Map parametros = new HashMap();
                            parametros.put("Domicilio", txtdireccion.getText());
                            parametros.put("Localidad", txtlocalidad.getText());
                            parametros.put("Provincia", "TUCUMAN");
                            parametros.put("FechaEgreso", revertirfecha(efechas.getfechaok(fechaegreso)));
                            parametros.put("FechaAlta", revertirfecha(efechas.getfechaok(fechaalta)));
                            JasperPrint jPrint2 = JasperFillManager.fillReport(reporte_sobre2, parametros, new JREmptyDataSource());
                            JasperPrintManager.printReport(jPrint2, false);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                    }
                    this.dispose();
                }
                ////////////////////////////////////////////////////////////

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btncancelar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelar2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncancelar2ActionPerformed

    private void btnAtrasLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasLabActionPerformed
        jTabbedPane1.setSelectedIndex(1);

    }//GEN-LAST:event_btnAtrasLabActionPerformed

    private void txtndelibroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtndelibroKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtndelibro.transferFocus();
        }
    }//GEN-LAST:event_txtndelibroKeyPressed

    private void txtndelibroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtndelibroKeyReleased
        if (!isNumeric(txtndelibro.getText())) {
            txtndelibro.setText("");
        }
    }//GEN-LAST:event_txtndelibroKeyReleased

    private void txtndefolioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtndefolioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtndefolio.transferFocus();
        }
    }//GEN-LAST:event_txtndefolioKeyPressed

    private void txtndefolioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtndefolioKeyReleased
        if (!isNumeric(txtndefolio.getText())) {
            txtndefolio.setText("");
        }
    }//GEN-LAST:event_txtndefolioKeyReleased

    private void txtndeactaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtndeactaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtndeacta.transferFocus();
        }
    }//GEN-LAST:event_txtndeactaKeyPressed

    private void txtndeactaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtndeactaKeyReleased
        if (!isNumeric(txtndeacta.getText())) {
            txtndeacta.setText("");
        }
    }//GEN-LAST:event_txtndeactaKeyReleased

    private void txtmatriculaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmatricula.transferFocus();
        }
    }//GEN-LAST:event_txtmatriculaKeyPressed

    private void txtmatriculaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyReleased

    }//GEN-LAST:event_txtmatriculaKeyReleased

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtnombre.transferFocus();
        }
    }//GEN-LAST:event_txtnombreKeyPressed

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        String texto = txtnombre.getText().toUpperCase();
        txtnombre.setText(texto);
        btnsiguiente.setEnabled(true);
        btnatras.setEnabled(true);
    }//GEN-LAST:event_txtnombreKeyReleased

    private void sifacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sifacturaActionPerformed
        if (sifactura.isSelected()) {
            nofactura.setSelected(false);
            sisociedad.setSelected(false);
            nosociedad.setSelected(true);
            sicolegio.setSelected(false);
            nocolegio.setSelected(true);
            sisociedad.setEnabled(true);
            nosociedad.setEnabled(true);
            sicolegio.setEnabled(true);
            nocolegio.setEnabled(true);

        } else {
            nofactura.setSelected(true);
            nosociedad.setSelected(false);
            sisociedad.setSelected(false);
            sicolegio.setSelected(false);
            nocolegio.setSelected(false);
            sisociedad.setEnabled(false);
            nosociedad.setEnabled(false);
            sicolegio.setEnabled(false);
            nocolegio.setEnabled(false);
        }
    }//GEN-LAST:event_sifacturaActionPerformed

    private void sifacturaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sifacturaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            sifactura.transferFocus();
        }
    }//GEN-LAST:event_sifacturaKeyPressed

    private void nofacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nofacturaActionPerformed
        if (nofactura.isSelected()) {
            sifactura.setSelected(false);
            nosociedad.setSelected(false);
            sisociedad.setSelected(false);
            sicolegio.setSelected(false);
            nocolegio.setSelected(false);
            sisociedad.setEnabled(false);
            nosociedad.setEnabled(false);
            sicolegio.setEnabled(false);
            nocolegio.setEnabled(false);
            cbovalidador.setVisible(false);
        } else {
            sifactura.setSelected(true);
            sisociedad.setSelected(false);
            nosociedad.setSelected(true);
            sicolegio.setSelected(false);
            nocolegio.setSelected(true);
            sisociedad.setEnabled(true);
            nosociedad.setEnabled(true);
            sicolegio.setEnabled(true);
            nocolegio.setEnabled(true);
        }
    }//GEN-LAST:event_nofacturaActionPerformed

    private void sisociedadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sisociedadActionPerformed
        if (sisociedad.isSelected()) {
            nosociedad.setSelected(false);
        } else {
            nosociedad.setSelected(true);
        }
    }//GEN-LAST:event_sisociedadActionPerformed

    private void nosociedadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nosociedadActionPerformed
        if (nosociedad.isSelected()) {
            sisociedad.setSelected(false);
        } else {
            sisociedad.setSelected(true);
        }
    }//GEN-LAST:event_nosociedadActionPerformed

    private void sicolegioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sicolegioActionPerformed
        if (sicolegio.isSelected()) {
            nocolegio.setSelected(false);
            cbovalidador.setVisible(true);
        } else {
            nocolegio.setSelected(true);
            cbovalidador.setVisible(false);
        }
    }//GEN-LAST:event_sicolegioActionPerformed

    private void nocolegioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nocolegioActionPerformed
        if (nocolegio.isSelected()) {
            sicolegio.setSelected(false);
            cbovalidador.setVisible(false);
        } else {
            sicolegio.setSelected(true);
            cbovalidador.setVisible(true);
        }
    }//GEN-LAST:event_nocolegioActionPerformed

    private void cbovalidadorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbovalidadorKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cbovalidador.transferFocus();
        }
    }//GEN-LAST:event_cbovalidadorKeyPressed

    private void txtdocumentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdocumentoKeyReleased
        if (!isNumeric(txtdocumento.getText())) {
            txtdocumento.setText("");
        }
    }//GEN-LAST:event_txtdocumentoKeyReleased

    private void txtcuilKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcuilKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcuilKeyReleased

    private void txtdireccionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyReleased
        String texto = txtdireccion.getText().toUpperCase();
        txtdireccion.setText(texto);
    }//GEN-LAST:event_txtdireccionKeyReleased

    private void txtlocalidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlocalidadKeyReleased
        String texto = txtlocalidad.getText().toUpperCase();
        txtlocalidad.setText(texto);
    }//GEN-LAST:event_txtlocalidadKeyReleased

    private void txttelfijoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelfijoKeyReleased
        if (!isNumeric(txttelfijo.getText())) {
            txttelfijo.setText("");
        }
    }//GEN-LAST:event_txttelfijoKeyReleased

    private void txttelcelularKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelcelularKeyReleased
        if (!isNumeric(txttelcelular.getText())) {
            txttelcelular.setText("");
        }
    }//GEN-LAST:event_txttelcelularKeyReleased

    private void txtmailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmailKeyReleased
        String texto = txtmail.getText().toLowerCase();
        txtmail.setText(texto);
    }//GEN-LAST:event_txtmailKeyReleased

    private void txtntituloKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtntituloKeyReleased
        if (!isNumeric(txtntitulo.getText())) {
            txtntitulo.setText("");
        }
    }//GEN-LAST:event_txtntituloKeyReleased

    private void txtespecialidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtespecialidadKeyReleased
        String texto = txtespecialidad.getText().toUpperCase();
        txtespecialidad.setText(texto);
    }//GEN-LAST:event_txtespecialidadKeyReleased

    private void txtdirectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdirectorActionPerformed
        String texto = txtdirector.getText().toUpperCase();
        txtdirector.setText(texto);
    }//GEN-LAST:event_txtdirectorActionPerformed

    private void btnexaminar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexaminar2ActionPerformed
        File archivo;
        JFileChooser flcAbrirArchivo;
        flcAbrirArchivo = new JFileChooser();
        flcAbrirArchivo.setFileFilter(new FileNameExtensionFilter("Grafico de red portatiles", "png", "jpg"));
        int respuesta = flcAbrirArchivo.showOpenDialog(this);
        if (respuesta == JFileChooser.APPROVE_OPTION) {
            archivo = flcAbrirArchivo.getSelectedFile();
            pic = archivo.getAbsolutePath();
            Image foto = getToolkit().getImage(pic);
            foto = foto.getScaledInstance(120, 120, 1);
            lblfoto1.setIcon(new ImageIcon(foto));
            BufferedImage originalImage = ImageTest.loadImage(pic);
            BufferedImage resizedImage = ImageTest.resize(originalImage, 120, 120);
            File file = new File("C:\\Descargas-CBT\\bioq.png");
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            ImageTest.saveImage(resizedImage, file.getPath());
            pic = "C:\\Descargas-CBT\\bioq.png";
        }
    }//GEN-LAST:event_btnexaminar2ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        new TomarImagen(null, true).setVisible(true);
        pic = "\\\\192.168.20.10\\proyecto cbt facturacion\\colegio bioquimicos\\camera.jpg";
        Image foto = getToolkit().getImage(pic);
        foto = foto.getScaledInstance(120, 120, 1);
        lblfoto1.setIcon(new ImageIcon(foto));
        BufferedImage originalImage = ImageTest.loadImage(pic);
        BufferedImage resizedImage = ImageTest.resize(originalImage, 120, 120);
        File file = new File("C:\\Descargas-CBT\\bioq.png");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        ImageTest.saveImage(resizedImage, file.getPath());
        pic = "C:\\Descargas-CBT\\bioq.png";
    }//GEN-LAST:event_jButton2ActionPerformed

    private void cbosexoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbosexoKeyPressed
        int s;
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (cbosexo.getSelectedItem().equals("M")) {
                s = 0;
            } else {
                s = 1;
            }
            cuil(s, txtdocumento.getText());
            cbosexo.transferFocus();
        }
    }//GEN-LAST:event_cbosexoKeyPressed

    private void txtdocumentoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdocumentoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtdocumento.transferFocus();
        }
    }//GEN-LAST:event_txtdocumentoKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        new Directivo(null, true).setVisible(true);
        
                        if(banderaDirectivo==1){
                            try {
                            traerimagen();
                            JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet.jasper"));
                            Map parametros = new HashMap();
                            if (cboTipoProf.getSelectedItem().toString().equals("B")) {
                                if (cbosexo.getSelectedItem().toString().equals("M")) {
                                    parametros.put("parametro", "BIOQUÍMICO");
                                } else {
                                    parametros.put("parametro", "BIOQUÍMICA");
                                }
                            } else {
                                if (cbosexo.getSelectedItem().toString().equals("M")) {
                                    parametros.put("parametro", "TECNICO");
                                } else {
                                    parametros.put("parametro", "TECNICA");
                                }

                            }
                            parametros.put("Matricula", txtmatricula.getText());
                            parametros.put("Apellido", txtnombre.getText());
                            parametros.put("FechaNac", efechas.getfechaok(fechaNac));
                            parametros.put("Dni", txtdocumento.getText());
                            parametros.put("Foto", imagen2);
                            JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                            JasperPrintManager.printReport(jPrint, false);

                        } catch (Exception e) {
                            
                            JOptionPane.showMessageDialog(null, e);
                        }
                        JOptionPane.showMessageDialog(null, "Dar vuelta la hoja para imprir el reverso...");
                        try {
                            JasperReport reporte_sobre2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet2.jasper"));
                            Map parametros = new HashMap();
                            parametros.put("Domicilio", txtdireccion.getText());
                            parametros.put("Localidad", txtlocalidad.getText());
                            parametros.put("Provincia", "TUCUMAN");
                            parametros.put("FechaEgreso", efechas.getfechaok(fechaegreso));
                            parametros.put("FechaAlta", efechas.getfechaok(fechaalta));
                            parametros.put("Directivo",directivo);
                            parametros.put("Cargo",cargo);
                            JasperPrint jPrint2 = JasperFillManager.fillReport(reporte_sobre2, parametros, new JREmptyDataSource());
                            JasperPrintManager.printReport(jPrint2, false);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                        }
                    
        
    }//GEN-LAST:event_jButton1ActionPerformed

    public java.util.Date StringADate(String fecha) {
        SimpleDateFormat formato_del_texto = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaE = null;
        try {
            fechaE = formato_del_texto.parse(fecha);
            return fechaE;
        } catch (ParseException ex) {
            return null;
        }
    }

    public java.util.Date StringADateOk(String fecha) {
        SimpleDateFormat formato_del_texto = new SimpleDateFormat("dd-MM-yyyy");
        Date fechaE = null;
        try {
            fechaE = formato_del_texto.parse(fecha);
            return fechaE;
        } catch (ParseException ex) {
            return null;
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Colegiado;
    private javax.swing.JPanel Laboratorio;
    private javax.swing.JPanel Particular;
    private javax.swing.JButton btnAtrasLab;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnatras;
    private javax.swing.JButton btnatras1;
    private javax.swing.JButton btncancelar2;
    private javax.swing.JButton btnexaminar2;
    private javax.swing.JButton btnsiguiente;
    private javax.swing.JButton btnsiguiente1;
    private javax.swing.JComboBox cboTipoProf;
    private javax.swing.JComboBox cbohorariolab;
    private javax.swing.JComboBox cbosexo;
    private javax.swing.JComboBox cbotipodoc;
    private javax.swing.JComboBox cbovalidador;
    private javax.swing.JComboBox cbovitalicio;
    private com.toedter.calendar.JDateChooser fechaNac;
    private com.toedter.calendar.JDateChooser fechaalta;
    private com.toedter.calendar.JDateChooser fechabaja;
    private com.toedter.calendar.JDateChooser fechadoctorado;
    private com.toedter.calendar.JDateChooser fechaegreso;
    private com.toedter.calendar.JDateChooser fechavenANSSAL;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblfoto1;
    private javax.swing.JCheckBox nocolegio;
    private javax.swing.JCheckBox nofactura;
    private javax.swing.JCheckBox nosociedad;
    private javax.swing.JCheckBox sicolegio;
    private javax.swing.JCheckBox sifactura;
    private javax.swing.JCheckBox sisociedad;
    private javax.swing.JTextField txtANSSALlab;
    private javax.swing.JTextField txtcodigopostal;
    private javax.swing.JTextField txtcodigopostallab;
    private javax.swing.JPasswordField txtcontraseña;
    private javax.swing.JTextField txtcuil;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtdireccionlab;
    private javax.swing.JTextField txtdirector;
    private javax.swing.JTextField txtdocumento;
    private javax.swing.JTextField txtespecialidad;
    private javax.swing.JTextField txtespecialidadlab;
    private javax.swing.JTextField txtfaxlab;
    private javax.swing.JTextField txtlocalidad;
    private javax.swing.JTextField txtlocalidadlab;
    private javax.swing.JTextField txtmail;
    private javax.swing.JTextField txtmaillab;
    private javax.swing.JTextField txtmatricula;
    private javax.swing.JTextField txtndeacta;
    private javax.swing.JTextField txtndefolio;
    private javax.swing.JTextField txtndelibro;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txtntitulo;
    private javax.swing.JTextField txttelcelular;
    private javax.swing.JTextField txttelcelularlab;
    private javax.swing.JTextField txttelfijo;
    private javax.swing.JTextField txttelfijolab;
    private javax.swing.JTextField txtusuario;
    private javax.swing.JTextField txtweblab;
    // End of variables declaration//GEN-END:variables
}
