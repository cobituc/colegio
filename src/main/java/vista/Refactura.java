package vista;

import controlador.ConexionMariaDB;
import controlador.ConexionMariaDB;
import controlador.NumberToLetterConverter;
import controlador.camposordenes11;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Color;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

public class Refactura extends javax.swing.JDialog {

    int contadorj = 0, id_obra_social, idobraimprime, id_usuario_validador, periodo_colegiado, id_usuario;
    String ObraSocial, CodObra, año, mes, periodo, colegiado, fecha2, periodo2;
    TextAutoCompleter textAutoAcompleter2;
    public static String[] obrasocial = new String[500];
    public static int[] idobrasocial = new int[500];
    public static String[] nombreobrasocial = new String[500];
    public static Double[] arancel = new Double[500];
    public static int[] idobra = new int[150000];
    public static int contadorobrasocial = 0;
    Hiloobrasocial hilo2;
    Hiloobrasocialtodas hilo3;
    Hiloobrasocialperiodo hilo4;
    Hiloobrasocialmatricula hilo5;

    public Refactura(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        progreso.setVisible(false);
        textAutoAcompleter2 = new TextAutoCompleter(txtobrasocial);
        traerobrasosial();
        cargarperiodo();
        cargarobrasocial();
        this.setLocationRelativeTo(null);
    }

    public class Hiloobrasocial extends Thread {

        JProgressBar progreso;

        public Hiloobrasocial(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            ///////////////////facturacion/////////////////////////////////////
            int cantidad = 100 / 10;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab;
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            LinkedList<camposordenes11> Resultados3 = new LinkedList<camposordenes11>();
            Resultados3.clear();
            String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0.00, pesos = 0, importetotal = 0, centavos = 0;
            int pacientes = 0;
            DecimalFormat df = new DecimalFormat("0.00");
            String nombre_obra = "", cod_obra = "", matricula = "";
            try {
                Statement st3 = cn.createStatement();
                ResultSet rs3 = st3.executeQuery("SELECT id_colegiados,nombre_colegiado,matricula_colegiado,factura,direccion_laboratorio,numerodireccion_laboratorio,localidad_laboratorio,periodos,estado_periodo,estado_facturacion FROM colegiados  WHERE factura=" + "'SI'" + " AND id_colegiados=" + id_usuario);

                while (rs3.next()) {
                    // if (Integer.valueOf(periodo) <= (rs3.getInt("periodo")) && (false == (rs3.getBoolean("estado_facturacion")) )) {

                    colegiado = rs3.getInt("id_colegiados");
                    matricula = rs3.getString("matricula_colegiado");
                    nombre_colegiado = rs3.getString("nombre_colegiado");
                    domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                    localidad_lab = rs3.getString("localidad_laboratorio");
                    /////////////////////////////////////////////////////
                    /// SELECCIONO LAS OBRAS SOCIALES FACTURADAS POR EL BIOQUIMICO EN UN PERIODO
                    Statement st6 = cn.createStatement();
                    String sql6 = "SELECT DISTINCT(ordenes.id_obrasocial) FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " AND obrasocial.id_obrasocial=" + id_obra_social + " order by (obrasocial.Int_codigo_obrasocial)";
                    ResultSet rs6 = st6.executeQuery(sql6);

                    while (rs6.next()) {
                        int pdf = 0;
                        pesos = 0;
                        pacientes = 0;
                        practicas = 0;
                        Resultados3.clear();

                        Statement st2 = cn.createStatement();
                        String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial WHERE id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial");
                        ResultSet rs2 = st2.executeQuery(sql2);
                        rs2.next();
                        nombre_obra = rs2.getString("razonsocial_obrasocial");
                        cod_obra = rs2.getString("codigo_obrasocial");
                        uni_gastos = df.format(rs2.getDouble("importeunidaddegasto_obrasocial"));
                        uni_Honorarios = df.format(rs2.getDouble("importeunidaddearancel_obrasocial"));

                        String sql = "SELECT  DISTINCT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado FROM ordenes INNER JOIN colegiados  USING (id_colegiados) INNER JOIN detalle_ordenes USING (id_orden) INNER JOIN obrasocial USING (id_obrasocial) WHERE ordenes.periodo=" + periodo2 + "  AND colegiados.matricula_colegiado=" + matricula + "  AND obrasocial.id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial") + "  AND ordenes.estado_orden=1 and detalle_ordenes.estado=0";
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sql);

                        total = 0;
                        bandera = 0;
                        int id_ordenes = 0;
                        while (rs.next()) {
                            camposordenes11 tipo;
                            if (id_ordenes == 0) {
                                tipo = new camposordenes11(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                //band = 1;
                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {
                                    tipo = new camposordenes11(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    tipo = new camposordenes11("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                }
                            }

                            ////////////////cargo la matriz para imprimir//////////                                   
                            //  if (band == 0) {
                            total = Redondeardosdigitos(total);

                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            practicas = practicas + 1;
                            Resultados3.add(tipo);
                            bandera = 1;

                        }
                        pesos = pesos + total;
                        /////////////////////////////////////////////////////
                        if (bandera == 1) {

                            pesos = Redondeardosdigitos(pesos);
                            importetotal = pesos;
                            ///////////////////////////////////////////////////////////////////////////////////
                            String mes = "", año = periodo2.substring(0, 4), cadena = periodo2.substring(4, 6);
                            if (cadena.equals("01")) {
                                mes = "Enero";
                            }
                            if (cadena.equals("02")) {
                                mes = "Febrero";
                            }
                            if (cadena.equals("03")) {
                                mes = "Marzo";
                            }
                            if (cadena.equals("04")) {
                                mes = "Abril";
                            }
                            if (cadena.equals("05")) {
                                mes = "Mayo";
                            }
                            if (cadena.equals("06")) {
                                mes = "Junio";
                            }
                            if (cadena.equals("07")) {
                                mes = "Julio";
                            }
                            if (cadena.equals("08")) {
                                mes = "Agosto";
                            }
                            if (cadena.equals("09")) {
                                mes = "Septiembre";
                            }
                            if (cadena.equals("10")) {
                                mes = "Octubre";
                            }
                            if (cadena.equals("11")) {
                                mes = "Noviembre";
                            }
                            if (cadena.equals("12")) {
                                mes = "Diciembre";
                            }
                            String periodo3 = mes + " " + año;
                            centavos = Redondearcentavos(pesos);
                            pesos = Redondeardosdigitos(pesos - centavos / 100);
                            total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                            total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                            JasperReport jasperReport;
                            try {
                                JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\colegio bioquimicos\\src\\Reportes\\Ordenes.jrxml");
                                ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                Map parametros = new HashMap();
                                parametros.put("matricula", matricula);
                                parametros.put("periodo", periodo3);
                                parametros.put("fecha", fecha2);
                                parametros.put("obra_social", nombre_obra);
                                parametros.put("num_obra_social", cod_obra);
                                parametros.put("laboratorio", nombre_colegiado);
                                parametros.put("domicilio_lab", domicilio_lab);
                                parametros.put("localidad", localidad_lab);
                                parametros.put("pacientes", String.valueOf(pacientes));
                                parametros.put("practicas", String.valueOf(practicas));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\PDF-FACTURACION\\PDF-FACTURACION\\" + periodo2 + "-" + matricula + "-" + cod_obra + ".pdf");
                                JasperViewer.viewReport(jPrint, true);

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        ///////////////////////////////////////////////////////////////////////
                    }
                    indice = indice + cantidad;

                }

                btnimprimir.setEnabled(true);
                //////////////////////////////////////////////////////////////////////////////
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    public class Hiloobrasocialtodas extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialtodas(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {

            ///////////////////facturacion/////////////////////////////////////
            int cantidad = 100 / 10;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab;
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            LinkedList<camposordenes11> Resultados3 = new LinkedList<camposordenes11>();
            Resultados3.clear();
            String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0.00, pesos = 0, importetotal = 0, centavos = 0;
            int pacientes = 0;
            DecimalFormat df = new DecimalFormat("0.00");
            String nombre_obra = "", cod_obra = "", matricula = "";
            try {
                Statement st3 = cn.createStatement();
                ResultSet rs3 = st3.executeQuery("SELECT DISTINCT(ordenes.id_colegiados), matricula_colegiado, nombre_colegiado, direccion_laboratorio, numerodireccion_laboratorio, localidad_laboratorio FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados WHERE ordenes.periodo=" + periodo2 + " order by (ordenes.id_colegiados)");

                while (rs3.next()) {
                    // if (Integer.valueOf(periodo) <= (rs3.getInt("periodo")) && (false == (rs3.getBoolean("estado_facturacion")) )) {

                    colegiado = rs3.getInt("id_colegiados");
                    matricula = rs3.getString("matricula_colegiado");
                    nombre_colegiado = rs3.getString("nombre_colegiado");
                    domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                    localidad_lab = rs3.getString("localidad_laboratorio");
                    /////////////////////////////////////////////////////
                    /// SELECCIONO LAS OBRAS SOCIALES FACTURADAS POR EL BIOQUIMICO EN UN PERIODO
                    Statement st6 = cn.createStatement();
                    String sql6 = "SELECT DISTINCT(ordenes.id_obrasocial) FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " AND obrasocial.id_obrasocial=" + id_obra_social + " order by (obrasocial.Int_codigo_obrasocial)";
                    ResultSet rs6 = st6.executeQuery(sql6);

                    while (rs6.next()) {
                        int pdf = 0;
                        pesos = 0;
                        pacientes = 0;
                        practicas = 0;
                        Resultados3.clear();

                        Statement st2 = cn.createStatement();
                        String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial WHERE id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial");
                        ResultSet rs2 = st2.executeQuery(sql2);
                        rs2.next();
                        nombre_obra = rs2.getString("razonsocial_obrasocial");
                        cod_obra = rs2.getString("codigo_obrasocial");
                        uni_gastos = df.format(rs2.getDouble("importeunidaddegasto_obrasocial"));
                        uni_Honorarios = df.format(rs2.getDouble("importeunidaddearancel_obrasocial"));

                        String sql = "SELECT  DISTINCT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado FROM ordenes INNER JOIN colegiados  USING (id_colegiados) INNER JOIN detalle_ordenes USING (id_orden) INNER JOIN obrasocial USING (id_obrasocial) WHERE ordenes.periodo=" + periodo2 + "  AND colegiados.matricula_colegiado=" + matricula + "  AND obrasocial.id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial") + "  AND ordenes.estado_orden=1 and detalle_ordenes.estado=0";
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sql);

                        total = 0;
                        bandera = 0;
                        int id_ordenes = 0;
                        while (rs.next()) {
                            camposordenes11 tipo;
                            if (id_ordenes == 0) {
                                tipo = new camposordenes11(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                //band = 1;
                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {
                                    tipo = new camposordenes11(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    tipo = new camposordenes11("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                }
                            }

                            ////////////////cargo la matriz para imprimir//////////                                   
                            //  if (band == 0) {
                            total = Redondeardosdigitos(total);

                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            practicas = practicas + 1;
                            Resultados3.add(tipo);
                            bandera = 1;

                        }
                        pesos = pesos + total;
                        /////////////////////////////////////////////////////
                        if (bandera == 1) {

                            pesos = Redondeardosdigitos(pesos);
                            importetotal = pesos;
                            ///////////////////////////////////////////////////////////////////////////////////
                            String mes = "", año = periodo2.substring(0, 4), cadena = periodo2.substring(4, 6);
                            if (cadena.equals("01")) {
                                mes = "Enero";
                            }
                            if (cadena.equals("02")) {
                                mes = "Febrero";
                            }
                            if (cadena.equals("03")) {
                                mes = "Marzo";
                            }
                            if (cadena.equals("04")) {
                                mes = "Abril";
                            }
                            if (cadena.equals("05")) {
                                mes = "Mayo";
                            }
                            if (cadena.equals("06")) {
                                mes = "Junio";
                            }
                            if (cadena.equals("07")) {
                                mes = "Julio";
                            }
                            if (cadena.equals("08")) {
                                mes = "Agosto";
                            }
                            if (cadena.equals("09")) {
                                mes = "Septiembre";
                            }
                            if (cadena.equals("10")) {
                                mes = "Octubre";
                            }
                            if (cadena.equals("11")) {
                                mes = "Noviembre";
                            }
                            if (cadena.equals("12")) {
                                mes = "Diciembre";
                            }
                            String periodo3 = mes + " " + año;
                            centavos = Redondearcentavos(pesos);
                            pesos = Redondeardosdigitos(pesos - centavos / 100);
                            total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                            total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                            JasperReport jasperReport;
                            try {
                                JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\colegio bioquimicos\\src\\Reportes\\Ordenes.jrxml");
                                ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                Map parametros = new HashMap();
                                parametros.put("matricula", matricula);
                                parametros.put("periodo", periodo3);
                                parametros.put("fecha", fecha2);
                                parametros.put("obra_social", nombre_obra);
                                parametros.put("num_obra_social", cod_obra);
                                parametros.put("laboratorio", nombre_colegiado);
                                parametros.put("domicilio_lab", domicilio_lab);
                                parametros.put("localidad", localidad_lab);
                                parametros.put("pacientes", String.valueOf(pacientes));
                                parametros.put("practicas", String.valueOf(practicas));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\PDF-FACTURACION\\PDF-FACTURACION\\" + periodo2 + "-" + matricula + "-" + cod_obra + ".pdf");
                                // JasperViewer.viewReport(jPrint, true);

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        ///////////////////////////////////////////////////////////////////////
                    }
                    indice = indice + cantidad;

                }

                btnimprimir.setEnabled(true);
                //////////////////////////////////////////////////////////////////////////////
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    public class Hiloobrasocialperiodo extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialperiodo(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {

            ///////////////////facturacion/////////////////////////////////////
            int cantidad = 100 / 10;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab;
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            LinkedList<camposordenes11> Resultados3 = new LinkedList<camposordenes11>();
            Resultados3.clear();
            String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0.00, pesos = 0, importetotal = 0, centavos = 0;
            int pacientes = 0;
            DecimalFormat df = new DecimalFormat("0.00");
            String nombre_obra = "", cod_obra = "", matricula = "";
            try {
                Statement st3 = cn.createStatement();
                ResultSet rs3 = st3.executeQuery("SELECT DISTINCT(ordenes.id_colegiados), matricula_colegiado, nombre_colegiado, direccion_laboratorio, numerodireccion_laboratorio, localidad_laboratorio FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados WHERE ordenes.periodo=" + periodo2 + " order by (ordenes.id_colegiados)");

                while (rs3.next()) {
                    // if (Integer.valueOf(periodo) <= (rs3.getInt("periodo")) && (false == (rs3.getBoolean("estado_facturacion")) )) {

                    colegiado = rs3.getInt("id_colegiados");
                    matricula = rs3.getString("matricula_colegiado");
                    nombre_colegiado = rs3.getString("nombre_colegiado");
                    domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                    localidad_lab = rs3.getString("localidad_laboratorio");
                    /////////////////////////////////////////////////////
                    /// SELECCIONO LAS OBRAS SOCIALES FACTURADAS POR EL BIOQUIMICO EN UN PERIODO
                    Statement st6 = cn.createStatement();
                    String sql6 = "SELECT DISTINCT(ordenes.id_obrasocial) FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " order by (obrasocial.Int_codigo_obrasocial)";
                    ResultSet rs6 = st6.executeQuery(sql6);

                    while (rs6.next()) {
                        int pdf = 0;
                        pesos = 0;
                        pacientes = 0;
                        practicas = 0;
                        Resultados3.clear();

                        Statement st2 = cn.createStatement();
                        String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial WHERE id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial");
                        ResultSet rs2 = st2.executeQuery(sql2);
                        rs2.next();
                        nombre_obra = rs2.getString("razonsocial_obrasocial");
                        cod_obra = rs2.getString("codigo_obrasocial");
                        uni_gastos = df.format(rs2.getDouble("importeunidaddegasto_obrasocial"));
                        uni_Honorarios = df.format(rs2.getDouble("importeunidaddearancel_obrasocial"));

                        String sql = "SELECT  DISTINCT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado FROM ordenes INNER JOIN colegiados  USING (id_colegiados) INNER JOIN detalle_ordenes USING (id_orden) INNER JOIN obrasocial USING (id_obrasocial) WHERE ordenes.periodo=" + periodo2 + "  AND colegiados.matricula_colegiado=" + matricula + "  AND obrasocial.id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial") + "  AND ordenes.estado_orden=1 and detalle_ordenes.estado=0";
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sql);

                        total = 0;
                        bandera = 0;
                        int id_ordenes = 0;
                        while (rs.next()) {
                            camposordenes11 tipo;
                            if (id_ordenes == 0) {
                                tipo = new camposordenes11(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                //band = 1;
                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {
                                    tipo = new camposordenes11(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    tipo = new camposordenes11("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                }
                            }

                            ////////////////cargo la matriz para imprimir//////////                                   
                            //  if (band == 0) {
                            total = Redondeardosdigitos(total);

                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            practicas = practicas + 1;
                            Resultados3.add(tipo);
                            bandera = 1;

                        }
                        pesos = pesos + total;
                        /////////////////////////////////////////////////////
                        if (bandera == 1) {

                            pesos = Redondeardosdigitos(pesos);
                            importetotal = pesos;
                            ///////////////////////////////////////////////////////////////////////////////////
                            String mes = "", año = periodo2.substring(0, 4), cadena = periodo2.substring(4, 6);
                            if (cadena.equals("01")) {
                                mes = "Enero";
                            }
                            if (cadena.equals("02")) {
                                mes = "Febrero";
                            }
                            if (cadena.equals("03")) {
                                mes = "Marzo";
                            }
                            if (cadena.equals("04")) {
                                mes = "Abril";
                            }
                            if (cadena.equals("05")) {
                                mes = "Mayo";
                            }
                            if (cadena.equals("06")) {
                                mes = "Junio";
                            }
                            if (cadena.equals("07")) {
                                mes = "Julio";
                            }
                            if (cadena.equals("08")) {
                                mes = "Agosto";
                            }
                            if (cadena.equals("09")) {
                                mes = "Septiembre";
                            }
                            if (cadena.equals("10")) {
                                mes = "Octubre";
                            }
                            if (cadena.equals("11")) {
                                mes = "Noviembre";
                            }
                            if (cadena.equals("12")) {
                                mes = "Diciembre";
                            }
                            String periodo3 = mes + " " + año;
                            centavos = Redondearcentavos(pesos);
                            pesos = Redondeardosdigitos(pesos - centavos / 100);
                            total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                            total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                            JasperReport jasperReport;
                            try {
                                JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\colegio bioquimicos\\src\\Reportes\\Ordenes.jrxml");
                                ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                Map parametros = new HashMap();
                                parametros.put("matricula", matricula);
                                parametros.put("periodo", periodo3);
                                parametros.put("fecha", fecha2);
                                parametros.put("obra_social", nombre_obra);
                                parametros.put("num_obra_social", cod_obra);
                                parametros.put("laboratorio", nombre_colegiado);
                                parametros.put("domicilio_lab", domicilio_lab);
                                parametros.put("localidad", localidad_lab);
                                parametros.put("pacientes", String.valueOf(pacientes));
                                parametros.put("practicas", String.valueOf(practicas));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\PDF-FACTURACION\\PDF-FACTURACION\\" + periodo2 + "-" + matricula + "-" + cod_obra + ".pdf");
                                // JasperViewer.viewReport(jPrint, true);

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        ///////////////////////////////////////////////////////////////////////
                    }
                    indice = indice + cantidad;

                }

                btnimprimir.setEnabled(true);
                //////////////////////////////////////////////////////////////////////////////
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    public class Hiloobrasocialmatricula extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialmatricula(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {

            ///////////////////facturacion/////////////////////////////////////
            int cantidad = 100 / 10;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab;
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            LinkedList<camposordenes11> Resultados3 = new LinkedList<camposordenes11>();
            Resultados3.clear();
            String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0.00, pesos = 0, importetotal = 0, centavos = 0;
            int pacientes = 0;
            DecimalFormat df = new DecimalFormat("0.00");
            String nombre_obra = "", cod_obra = "", matricula = "";
            try {
                Statement st3 = cn.createStatement();
                ResultSet rs3 = st3.executeQuery("SELECT DISTINCT(ordenes.id_colegiados), matricula_colegiado, nombre_colegiado, direccion_laboratorio, numerodireccion_laboratorio, localidad_laboratorio FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados WHERE ordenes.periodo=" + periodo2 + " AND ordenes.id_colegiados=" + id_usuario + " order by (ordenes.id_colegiados)");

                while (rs3.next()) {
                    // if (Integer.valueOf(periodo) <= (rs3.getInt("periodo")) && (false == (rs3.getBoolean("estado_facturacion")) )) {

                    colegiado = rs3.getInt("id_colegiados");
                    matricula = rs3.getString("matricula_colegiado");
                    nombre_colegiado = rs3.getString("nombre_colegiado");
                    domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                    localidad_lab = rs3.getString("localidad_laboratorio");
                    /////////////////////////////////////////////////////
                    /// SELECCIONO LAS OBRAS SOCIALES FACTURADAS POR EL BIOQUIMICO EN UN PERIODO
                    Statement st6 = cn.createStatement();
                    String sql6 = "SELECT DISTINCT(ordenes.id_obrasocial) FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " order by (obrasocial.Int_codigo_obrasocial)";
                    ResultSet rs6 = st6.executeQuery(sql6);

                    while (rs6.next()) {
                        int pdf = 0;
                        pesos = 0;
                        pacientes = 0;
                        practicas = 0;
                        Resultados3.clear();

                        Statement st2 = cn.createStatement();
                        String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial WHERE id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial");
                        ResultSet rs2 = st2.executeQuery(sql2);
                        rs2.next();
                        nombre_obra = rs2.getString("razonsocial_obrasocial");
                        cod_obra = rs2.getString("codigo_obrasocial");
                        uni_gastos = df.format(rs2.getDouble("importeunidaddegasto_obrasocial"));
                        uni_Honorarios = df.format(rs2.getDouble("importeunidaddearancel_obrasocial"));

                        String sql = "SELECT  DISTINCT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado FROM ordenes INNER JOIN colegiados  USING (id_colegiados) INNER JOIN detalle_ordenes USING (id_orden) INNER JOIN obrasocial USING (id_obrasocial) WHERE ordenes.periodo=" + periodo2 + "  AND colegiados.matricula_colegiado=" + matricula + "  AND obrasocial.id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial") + "  AND ordenes.estado_orden=1 and detalle_ordenes.estado=0";
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sql);

                        total = 0;
                        bandera = 0;
                        int id_ordenes = 0;
                        while (rs.next()) {
                            camposordenes11 tipo;
                            if (id_ordenes == 0) {
                                tipo = new camposordenes11(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                //band = 1;
                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {
                                    tipo = new camposordenes11(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    tipo = new camposordenes11("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                }
                            }

                            ////////////////cargo la matriz para imprimir//////////                                   
                            //  if (band == 0) {
                            total = Redondeardosdigitos(total);

                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            practicas = practicas + 1;
                            Resultados3.add(tipo);
                            bandera = 1;

                        }
                        pesos = pesos + total;
                        /////////////////////////////////////////////////////
                        if (bandera == 1) {

                            pesos = Redondeardosdigitos(pesos);
                            importetotal = pesos;
                            ///////////////////////////////////////////////////////////////////////////////////
                            String mes = "", año = periodo2.substring(0, 4), cadena = periodo2.substring(4, 6);
                            if (cadena.equals("01")) {
                                mes = "Enero";
                            }
                            if (cadena.equals("02")) {
                                mes = "Febrero";
                            }
                            if (cadena.equals("03")) {
                                mes = "Marzo";
                            }
                            if (cadena.equals("04")) {
                                mes = "Abril";
                            }
                            if (cadena.equals("05")) {
                                mes = "Mayo";
                            }
                            if (cadena.equals("06")) {
                                mes = "Junio";
                            }
                            if (cadena.equals("07")) {
                                mes = "Julio";
                            }
                            if (cadena.equals("08")) {
                                mes = "Agosto";
                            }
                            if (cadena.equals("09")) {
                                mes = "Septiembre";
                            }
                            if (cadena.equals("10")) {
                                mes = "Octubre";
                            }
                            if (cadena.equals("11")) {
                                mes = "Noviembre";
                            }
                            if (cadena.equals("12")) {
                                mes = "Diciembre";
                            }
                            String periodo3 = mes + " " + año;
                            centavos = Redondearcentavos(pesos);
                            pesos = Redondeardosdigitos(pesos - centavos / 100);
                            total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                            total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                            JasperReport jasperReport;
                            try {
                                JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\colegio bioquimicos\\src\\Reportes\\Ordenes.jrxml");
                                ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                Map parametros = new HashMap();
                                parametros.put("matricula", matricula);
                                parametros.put("periodo", periodo3);
                                parametros.put("fecha", fecha2);
                                parametros.put("obra_social", nombre_obra);
                                parametros.put("num_obra_social", cod_obra);
                                parametros.put("laboratorio", nombre_colegiado);
                                parametros.put("domicilio_lab", domicilio_lab);
                                parametros.put("localidad", localidad_lab);
                                parametros.put("pacientes", String.valueOf(pacientes));
                                parametros.put("practicas", String.valueOf(practicas));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\PDF-FACTURACION\\PDF-FACTURACION\\" + periodo2 + "-" + matricula + "-" + cod_obra + ".pdf");
                                // JasperViewer.viewReport(jPrint, true);

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        ///////////////////////////////////////////////////////////////////////
                    }
                    indice = indice + cantidad;

                }

                btnimprimir.setEnabled(true);
                //////////////////////////////////////////////////////////////////////////////
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    void cargarperiodo() {
        ////////////////////////////////////////////////////////////////////
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT * FROM periodos ";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            periodo = rs.getString("periodo");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        //////////////////////////////////

        SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);

        fecha2 = formato2.format(currentDate);
        /////////////////////////////////////////////////////////       

        calendar.setTime(currentDate);

    }

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    public double Redondeardosdigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    public int completatotal(double numero) {
        //int centavos=Math.rint(numero * 100) % 100;
        int n = (int) numero;
        return n;
    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtcolegiado = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtobrasocial = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtmes = new javax.swing.JFormattedTextField();
        txtaño = new javax.swing.JFormattedTextField();
        btnimprimir = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        progreso = new javax.swing.JProgressBar();
        chk2 = new javax.swing.JCheckBox();
        chk1 = new javax.swing.JCheckBox();
        chk3 = new javax.swing.JCheckBox();
        chk4 = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar Facturante", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        jPanel3.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Matricula:");

        txtcolegiado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcolegiado.setForeground(new java.awt.Color(0, 102, 204));
        txtcolegiado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcolegiadoActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Apellido y Nombre o Razon Social");

        txtapellido.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtapellido.setForeground(new java.awt.Color(0, 102, 204));
        txtapellido.setNextFocusableComponent(txtmes);
        txtapellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtapellidoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtapellido)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(txtcolegiado, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtcolegiado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(133, 133, 133))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resumen de una Obra Social", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Obra Social:");

        txtobrasocial.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtobrasocial.setForeground(new java.awt.Color(0, 102, 204));
        txtobrasocial.setNextFocusableComponent(btnimprimir);
        txtobrasocial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtobrasocialMouseClicked(evt);
            }
        });
        txtobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtobrasocial)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtobrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Período a Imprimir", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Mes:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Año:");

        txtmes.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtmes.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtmes.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmesActionPerformed(evt);
            }
        });

        txtaño.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtaño.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtaño.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtaño.setNextFocusableComponent(txtobrasocial);
        txtaño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtañoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtmes, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtaño)
                .addGap(101, 101, 101))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728924 - clock time.png"))); // NOI18N
        btnimprimir.setText("Generar");
        btnimprimir.setNextFocusableComponent(btnsalir);
        btnimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        chk2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chk2.setText("Opcion 2");
        chk2.setToolTipText("por periodo y obra social");
        chk2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chk2ActionPerformed(evt);
            }
        });

        chk1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chk1.setText("Opcion 1");
        chk1.setToolTipText("por matricula, periodo y obra social");
        chk1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chk1ActionPerformed(evt);
            }
        });

        chk3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chk3.setText("Opcion 3");
        chk3.setToolTipText("por periodo");
        chk3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chk3ActionPerformed(evt);
            }
        });

        chk4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chk4.setText("Opcion 4");
        chk4.setToolTipText("por periodo y matricula");
        chk4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chk4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnimprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir))
                    .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(chk1)
                        .addGap(29, 29, 29)
                        .addComponent(chk2)
                        .addGap(40, 40, 40)
                        .addComponent(chk3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chk4)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chk1)
                    .addComponent(chk2)
                    .addComponent(chk3)
                    .addComponent(chk4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnimprimir)
                    .addComponent(btnsalir))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    void traerobrasosial() {

        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT id_obrasocial,razonsocial_obrasocial,importeunidaddearancel_obrasocial,codigo_obrasocial FROM obrasocial ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idobrasocial[contadorobrasocial] = rs.getInt("id_obrasocial");
                nombreobrasocial[contadorobrasocial] = (rs.getString("razonsocial_obrasocial"));
                arancel[contadorobrasocial] = (rs.getDouble("importeunidaddearancel_obrasocial"));
                obrasocial[contadorobrasocial] = (rs.getString("codigo_obrasocial") + " - " + rs.getString("razonsocial_obrasocial"));
                contadorobrasocial++;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarobrasocial() {
        TextAutoCompleter textAutoAcompleter = new TextAutoCompleter(txtobrasocial);

        int i = 0;

        // Recorro y cargo las obras sociales
        while (i < contadorobrasocial) {
            textAutoAcompleter.addItem(obrasocial[i]);

            i++;
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter.setMode(0); // infijo     

        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter.setCaseSensitive(false); //No sensible a mayúsculas        

    }

    private void txtobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialActionPerformed
        String cod = "", nom = "", cadena = txtobrasocial.getText();
        int i = 0, k = 0, j = 1;
        ///cod/// 0000 - aaaaaaaaa
        while (k < cadena.length()) {
            if (String.valueOf(cadena.charAt(k)).equals("-")) {
                j = k + 2;
                k = cadena.length();
            } else {
                cod = cod + cadena.charAt(k);
            }
            k++;
        }
        ///nom/// 0000 - aaaaaaaaa
        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }
        while (i < contadorobrasocial) {
            if (cadena.equals(obrasocial[i])) {
                id_obra_social = idobrasocial[i];
                break;
            }
            i++;
        }
        ObraSocial = nom;
        CodObra = cod;
        idobraimprime = id_obra_social;
        txtobrasocial.transferFocus();
    }//GEN-LAST:event_txtobrasocialActionPerformed

    private void txtmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmesActionPerformed
        txtmes.transferFocus();
    }//GEN-LAST:event_txtmesActionPerformed

    private void txtañoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtañoActionPerformed
        año = txtaño.getText();
        mes = txtmes.getText();
        periodo2 = txtaño.getText() + txtmes.getText();
        txtaño.transferFocus();
    }//GEN-LAST:event_txtañoActionPerformed

    private void txtcolegiadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcolegiadoActionPerformed
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        int i = 0;

        try {
            String sSQL = "SELECT id_colegiados,matricula_colegiado,nombre_colegiado,validador,periodos FROM colegiados where matricula_colegiado="+txtcolegiado.getText();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {                
                    txtapellido.setText(rs.getString("nombre_colegiado"));
                    periodo_colegiado = rs.getInt("periodos");
                    id_usuario = rs.getInt("id_colegiados");
                    colegiado = txtcolegiado.getText();
                    txtcolegiado.transferFocus();
                    i = 1;                
            }
            if (i == 0) {
                JOptionPane.showMessageDialog(null, "Matricula erronea o el validador no puede realizar ordenes para esta matricula...");
                txtcolegiado.requestFocus();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Error en la base de datos");
        }
    }//GEN-LAST:event_txtcolegiadoActionPerformed

    private void txtapellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtapellidoActionPerformed
        if (!txtapellido.getText().equals("")) {
            txtapellido.transferFocus();
        }
    }//GEN-LAST:event_txtapellidoActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirActionPerformed
        if (chk1.isSelected()) {
            btnimprimir.setEnabled(false);
            iniciarSplash();
            hilo2 = new Hiloobrasocial(progreso);
            hilo2.start();
            hilo2 = null;
        }
        if (chk2.isSelected()) {
            btnimprimir.setEnabled(false);
            iniciarSplash();
            hilo3 = new Hiloobrasocialtodas(progreso);
            hilo3.start();
            hilo3 = null;
        }
        if (chk3.isSelected()) {
            btnimprimir.setEnabled(false);
            iniciarSplash();
            hilo4 = new Hiloobrasocialperiodo(progreso);
            hilo4.start();
            hilo4 = null;
        }
        if (chk4.isSelected()) {
            btnimprimir.setEnabled(false);
            iniciarSplash();
            hilo5 = new Hiloobrasocialmatricula(progreso);
            hilo5.start();
            hilo5 = null;
        }
    }//GEN-LAST:event_btnimprimirActionPerformed

    private void txtobrasocialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtobrasocialMouseClicked
        String cod = "", nom = "", cadena = txtobrasocial.getText();
        int i = 0, k = 0, j = 1;
        ///cod/// 0000 - aaaaaaaaa
        while (k < cadena.length()) {
            if (String.valueOf(cadena.charAt(k)).equals("-")) {
                j = k + 2;
                k = cadena.length();
            } else {
                cod = cod + cadena.charAt(k);
            }
            k++;
        }
        ///nom/// 0000 - aaaaaaaaa
        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }
        while (i < contadorobrasocial) {
            if (cadena.equals(obrasocial[i])) {
                id_obra_social = idobrasocial[i];
                break;
            }
            i++;
        }
        ObraSocial = nom;
        CodObra = cod;
        idobraimprime = id_obra_social;
        txtobrasocial.transferFocus();
    }//GEN-LAST:event_txtobrasocialMouseClicked

    private void chk1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chk1ActionPerformed
        if (chk1.isSelected()) {
            txtcolegiado.setEnabled(true);
            txtapellido.setEnabled(true);
            txtaño.setEditable(true);
            txtmes.setEditable(true);
            txtobrasocial.setEditable(true);
        }
    }//GEN-LAST:event_chk1ActionPerformed

    private void chk2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chk2ActionPerformed
        if (chk2.isSelected()) {
            txtcolegiado.setEnabled(false);
            txtapellido.setEnabled(false);
            txtaño.setEditable(true);
            txtmes.setEditable(true);
            txtobrasocial.setEditable(true);
        }
    }//GEN-LAST:event_chk2ActionPerformed

    private void chk3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chk3ActionPerformed
        if (chk3.isSelected()) {
            txtcolegiado.setEnabled(false);
            txtapellido.setEnabled(false);
            txtaño.setEditable(true);
            txtmes.setEditable(true);
            txtobrasocial.setEditable(false);
        }
    }//GEN-LAST:event_chk3ActionPerformed

    private void chk4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chk4ActionPerformed
        if (chk4.isSelected()) {
            txtcolegiado.setEnabled(true);
            txtapellido.setEnabled(true);
            txtaño.setEditable(true);
            txtmes.setEditable(true);
            txtobrasocial.setEditable(false);
        }
    }//GEN-LAST:event_chk4ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnimprimir;
    private javax.swing.JButton btnsalir;
    private javax.swing.JCheckBox chk1;
    private javax.swing.JCheckBox chk2;
    private javax.swing.JCheckBox chk3;
    private javax.swing.JCheckBox chk4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JFormattedTextField txtaño;
    private javax.swing.JTextField txtcolegiado;
    private javax.swing.JFormattedTextField txtmes;
    private javax.swing.JTextField txtobrasocial;
    // End of variables declaration//GEN-END:variables
}
