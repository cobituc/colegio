package vista;

import com.mxrck.autocompleter.TextAutoCompleter;
import static controlador.HiloInicio.listaBancos;
import java.awt.event.KeyEvent;
import java.util.Date;
import javax.swing.JOptionPane;
import modelo.Banco;
import modelo.FormaDePagoCheque;

public class ChequesIngresar extends javax.swing.JDialog {

    int x, y;
    public static int numero;
    public static int idBanco = 0,idCheque;
    public static String fechaEmision;
    public static String fechaVencimiento;
    public static double importe;
    private modelo.ConexionMariaDB conexion;
    TextAutoCompleter textAutoAcompleterBanco;

    public ChequesIngresar(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);       
        textAutoAcompleterBanco = new TextAutoCompleter(txtBanco);
        cargarBancos();
    }

     void cargarBancos() {
         try {
            textAutoAcompleterBanco.removeAllItems();
            for (int i = 0; i < listaBancos.size(); i++) {
                textAutoAcompleterBanco.addItem(listaBancos.get(i).toString());
            }
            textAutoAcompleterBanco.setMode(0);
            textAutoAcompleterBanco.setCaseSensitive(false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar cuentas. e(155)");
        }
    }

    String formatoFechaMySql(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        /////Año/////
        for (i = 6; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 3; i <= 4; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ////Dia////
        for (i = 0; i <= 1; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        txtnumero = new RSMaterialComponent.RSTextFieldOne();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtImporte = new RSMaterialComponent.RSTextFieldOne();
        txtFechaEmision = new javax.swing.JFormattedTextField();
        txtFechaVencimiento = new javax.swing.JFormattedTextField();
        txtBanco = new RSMaterialComponent.RSTextFieldOne();
        jPanel2 = new javax.swing.JPanel();
        btnIngresarCaja = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnSalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        jPanel4 = new javax.swing.JPanel();
        btnCerrar1 = new RSMaterialComponent.RSButtonIconOne();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel5.setBackground(new java.awt.Color(66, 133, 200));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Cheques", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Número:");

        txtnumero.setForeground(new java.awt.Color(51, 51, 51));
        txtnumero.setBorderColor(new java.awt.Color(255, 255, 255));
        txtnumero.setPhColor(new java.awt.Color(51, 51, 51));
        txtnumero.setPlaceholder("Número de cheque");
        txtnumero.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtnumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnumeroActionPerformed(evt);
            }
        });
        txtnumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnumeroKeyPressed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Fecha Emisión:");

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel22.setText("Banco:");

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Fecha Venc:");

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("Importe:");

        txtImporte.setForeground(new java.awt.Color(51, 51, 51));
        txtImporte.setBorderColor(new java.awt.Color(255, 255, 255));
        txtImporte.setPhColor(new java.awt.Color(51, 51, 51));
        txtImporte.setPlaceholder("0.0");
        txtImporte.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtImporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImporteActionPerformed(evt);
            }
        });
        txtImporte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtImporteKeyPressed(evt);
            }
        });

        try {
            txtFechaEmision.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtFechaEmision.setText("  /  /    ");
        txtFechaEmision.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtFechaEmision.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFechaEmisionKeyPressed(evt);
            }
        });

        try {
            txtFechaVencimiento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtFechaVencimiento.setText("  /  /    ");
        txtFechaVencimiento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtFechaVencimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFechaVencimientoKeyPressed(evt);
            }
        });

        txtBanco.setForeground(new java.awt.Color(51, 51, 51));
        txtBanco.setBorderColor(new java.awt.Color(255, 255, 255));
        txtBanco.setPhColor(new java.awt.Color(51, 51, 51));
        txtBanco.setPlaceholder("Seleccionar Banco");
        txtBanco.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtBanco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBancoActionPerformed(evt);
            }
        });
        txtBanco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBancoKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaEmision, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBanco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtImporte, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtnumero, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtBanco, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFechaEmision, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21)
                        .addComponent(jLabel23)
                        .addComponent(jLabel24)
                        .addComponent(txtImporte, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtFechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));

        btnIngresarCaja.setBackground(new java.awt.Color(255, 255, 255));
        btnIngresarCaja.setForeground(new java.awt.Color(0, 0, 0));
        btnIngresarCaja.setText("Ingresar a movimiento");
        btnIngresarCaja.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnIngresarCaja.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnIngresarCaja.setForegroundText(new java.awt.Color(51, 51, 51));
        btnIngresarCaja.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnIngresarCaja.setRound(20);
        btnIngresarCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarCajaActionPerformed(evt);
            }
        });

        btnSalir.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir.setText("Salir");
        btnSalir.setBackgroundHover(new java.awt.Color(214, 45, 32));
        btnSalir.setForegroundIcon(new java.awt.Color(214, 45, 32));
        btnSalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnSalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnSalir.setRound(20);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnIngresarCaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnIngresarCaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel4.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel4MouseDragged(evt);
            }
        });
        jPanel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel4MousePressed(evt);
            }
        });

        btnCerrar1.setBackground(new java.awt.Color(214, 45, 32));
        btnCerrar1.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar1.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar1.setSizeIcon(20.0F);
        btnCerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(285, Short.MAX_VALUE)
                .addComponent(btnCerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIngresarCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarCajaActionPerformed
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        if (!txtnumero.getText().equals("")
                && !txtFechaEmision.getText().equals("")
                && !txtFechaVencimiento.getText().equals("")
                && !txtImporte.getText().equals("")
                && idBanco != 0) {
            numero = Integer.valueOf(txtnumero.getText());
            fechaEmision = formatoFechaMySql(txtFechaEmision.getText());
            fechaVencimiento = formatoFechaMySql(txtFechaVencimiento.getText());
            importe = Double.valueOf(txtImporte.getText());
            FormaDePagoCheque cheque = new FormaDePagoCheque(numero, idBanco, importe, fechaEmision, fechaVencimiento);
            idCheque = FormaDePagoCheque.insertarCheque(conexion.getConnection(), cheque);
            conexion.cerrarConexion();
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(rootPane, "Debe ingresar todos los campos requeridos");
        }

    }//GEN-LAST:event_btnIngresarCajaActionPerformed

    private void btnCerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrar1ActionPerformed

        dispose();
    }//GEN-LAST:event_btnCerrar1ActionPerformed

    private void jPanel4MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);
    }//GEN-LAST:event_jPanel4MouseDragged

    private void jPanel4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MousePressed

        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jPanel4MousePressed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();

    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtnumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnumeroActionPerformed
    txtBanco.requestFocus();
    }//GEN-LAST:event_txtnumeroActionPerformed

    private void txtnumeroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumeroKeyPressed

    }//GEN-LAST:event_txtnumeroKeyPressed

    private void txtImporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImporteActionPerformed
        btnIngresarCaja.doClick();
    }//GEN-LAST:event_txtImporteActionPerformed

    private void txtImporteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtImporteKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImporteKeyPressed

    private void txtFechaEmisionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFechaEmisionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtFechaEmision.getText().equals("")) {
                if (!txtFechaEmision.equals("  /  /    ")) {
                    if (Integer.valueOf(txtFechaEmision.getText().substring(0, 2)) <= 31 && Integer.valueOf(txtFechaEmision.getText().substring(0, 2)) >= 1) {
                        if (Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) <= 12 && Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) >= 1) {
                            if (Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 1
                                    || Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 3
                                    || Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 5
                                    || Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 7
                                    || Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 8
                                    || Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 10
                                    || Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 12) {
                                txtFechaVencimiento.requestFocus();
                            } else {
                                if (Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 4
                                        || Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 6
                                        || Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 9
                                        || Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 11) {
                                    if (Integer.valueOf(txtFechaEmision.getText().substring(0, 2)) <= 30) {
                                        txtFechaVencimiento.requestFocus();
                                    }
                                } else {
                                    if (Integer.valueOf(txtFechaEmision.getText().substring(3, 5)) == 2) {
                                        if (Integer.valueOf(txtFechaEmision.getText().substring(6, 10)) % 4 == 0) {
                                            if (Integer.valueOf(txtFechaEmision.getText().substring(0, 2)) <= 29) {
                                                txtFechaVencimiento.requestFocus();
                                            }
                                        } else {
                                            if (Integer.valueOf(txtFechaEmision.getText().substring(0, 2)) <= 28) {
                                                txtFechaVencimiento.requestFocus();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }//GEN-LAST:event_txtFechaEmisionKeyPressed

    private void txtFechaVencimientoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFechaVencimientoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtFechaVencimiento.getText().equals("")) {
                if (!txtFechaVencimiento.equals("  /  /    ")) {
                    if (Integer.valueOf(txtFechaVencimiento.getText().substring(0, 2)) <= 31 && Integer.valueOf(txtFechaVencimiento.getText().substring(0, 2)) >= 1) {
                        if (Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) <= 12 && Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) >= 1) {
                            if (Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 1
                                    || Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 3
                                    || Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 5
                                    || Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 7
                                    || Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 8
                                    || Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 10
                                    || Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 12) {
                                txtImporte.requestFocus();
                            } else {
                                if (Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 4
                                        || Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 6
                                        || Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 9
                                        || Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 11) {
                                    if (Integer.valueOf(txtFechaVencimiento.getText().substring(0, 2)) <= 30) {
                                        txtImporte.requestFocus();
                                    }
                                } else {
                                    if (Integer.valueOf(txtFechaVencimiento.getText().substring(3, 5)) == 2) {
                                        if (Integer.valueOf(txtFechaVencimiento.getText().substring(6, 10)) % 4 == 0) {
                                            if (Integer.valueOf(txtFechaVencimiento.getText().substring(0, 2)) <= 29) {
                                                txtImporte.requestFocus();
                                            }
                                        } else {
                                            if (Integer.valueOf(txtFechaVencimiento.getText().substring(0, 2)) <= 28) {
                                                txtImporte.requestFocus();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }//GEN-LAST:event_txtFechaVencimientoKeyPressed

    private void txtBancoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBancoActionPerformed
        if(!txtBanco.getText().equals("")){
            Banco banco = Banco.buscarBanco(txtBanco.getText(), listaBancos);
            idBanco=banco.getIdBanco();
            txtFechaEmision.requestFocus();
        }else{
            JOptionPane.showConfirmDialog(null, "Debe ingresar un banco");
        } 
        
    }//GEN-LAST:event_txtBancoActionPerformed

    private void txtBancoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBancoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBancoKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonIconOne btnCerrar1;
    private RSMaterialComponent.RSButtonMaterialIconUno btnIngresarCaja;
    private RSMaterialComponent.RSButtonMaterialIconUno btnSalir;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private RSMaterialComponent.RSTextFieldOne txtBanco;
    private javax.swing.JFormattedTextField txtFechaEmision;
    private javax.swing.JFormattedTextField txtFechaVencimiento;
    private RSMaterialComponent.RSTextFieldOne txtImporte;
    private RSMaterialComponent.RSTextFieldOne txtnumero;
    // End of variables declaration//GEN-END:variables
}
