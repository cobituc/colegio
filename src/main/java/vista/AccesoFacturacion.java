
package vista;

import controlador.HiloInicio;
import java.util.ArrayList;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelo.Colegiado;
import modelo.Facturantes;
import modelo.Laboratorio;


public class AccesoFacturacion extends javax.swing.JDialog {
    
    private modelo.ConexionMariaDB conexion;
    public static ArrayList <modelo.Facturantes> listaFacturantes;
    DefaultTableModel model;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    public String matricula, laboratorio;


    public AccesoFacturacion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
         this.setLocationRelativeTo(null);
         cargartabla();
    }
    
   public void cargartabla(){
       
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaFacturantes = new ArrayList<>();
        Facturantes.cargarFacturante(conexion.getConnection(), listaFacturantes);
        conexion.cerrarConexion();        
        
         String[] Titulo = {"Matricula", "Nombre", "Laboratorio", "Usuario", "Contraseña", "Contraseña Fac"};
        String[] Registros = new String[6];
        
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };        
      
        
        for(int i =0; i<listaFacturantes.size(); i++){
            
                    
                Registros[0] = listaFacturantes.get(i).getMatricula();
                Registros[1] = listaFacturantes.get(i).getNombre();
                Registros[2] = listaFacturantes.get(i).getLaboratorio();
                Registros[3] = listaFacturantes.get(i).getUsuario();
                Registros[4] = listaFacturantes.get(i).getContraseña();
                Registros[5] = listaFacturantes.get(i).getContraseña();
                
            model.addRow(Registros);
        }
            tablaFacturantes.setModel(model);
            tablaFacturantes.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
            tablaFacturantes.getColumnModel().getColumn(0).setMinWidth(60);
            tablaFacturantes.getColumnModel().getColumn(0).setPreferredWidth(60);
            tablaFacturantes.getColumnModel().getColumn(0).setMaxWidth(60);
            
            tablaFacturantes.getColumnModel().getColumn(1).setMinWidth(250);
            tablaFacturantes.getColumnModel().getColumn(1).setPreferredWidth(250);
            tablaFacturantes.getColumnModel().getColumn(1).setMaxWidth(250);
            
            tablaFacturantes.getColumnModel().getColumn(2).setMinWidth(250);
            tablaFacturantes.getColumnModel().getColumn(2).setPreferredWidth(250);
            tablaFacturantes.getColumnModel().getColumn(2).setMaxWidth(250);
            //////alinear tabla///////
            
            alinear();
            tablaFacturantes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablaFacturantes.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            tablaFacturantes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
   }
   
   void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaFacturantes = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        btnSalir = new javax.swing.JButton();
        lblColegiado = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        txtContraseña = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        txtContraseña1 = new javax.swing.JPasswordField();
        jButton1 = new javax.swing.JButton();

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablaFacturantes.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaFacturantes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaFacturantes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaFacturantesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaFacturantes);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 944, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        lblColegiado.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblColegiado.setForeground(new java.awt.Color(0, 102, 204));
        lblColegiado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Usuario:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Contraseña:");

        txtUsuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtUsuario.setForeground(new java.awt.Color(0, 102, 204));
        txtUsuario.setNextFocusableComponent(txtContraseña);

        txtContraseña.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtContraseña.setForeground(new java.awt.Color(0, 102, 204));
        txtContraseña.setNextFocusableComponent(txtContraseña1);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Contraseña admin:");

        txtContraseña1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtContraseña1.setForeground(new java.awt.Color(0, 102, 204));

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728930 - down download.png"))); // NOI18N
        jButton1.setText("Guardar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 427, Short.MAX_VALUE)
                        .addComponent(jLabel4))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblColegiado, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtContraseña1)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                    .addComponent(txtContraseña, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(60, 60, 60)
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addComponent(btnSalir)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnSalir)
                                .addComponent(jButton1))
                            .addComponent(lblColegiado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtContraseña1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();
        
    }//GEN-LAST:event_btnSalirActionPerformed

    private void tablaFacturantesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaFacturantesMouseClicked

        if (evt.getClickCount() == 2) {

            lblColegiado.setText(tablaFacturantes.getValueAt(tablaFacturantes.getSelectedRow(), 1).toString());
            matricula = tablaFacturantes.getValueAt(tablaFacturantes.getSelectedRow(), 0).toString();
            laboratorio = tablaFacturantes.getValueAt(tablaFacturantes.getSelectedRow(), 2).toString();
           
            
        }
        
    }//GEN-LAST:event_tablaFacturantesMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        conexion.EstablecerConexion();
        String contraseñaU="", contraseñaF="";
        
            char [] passwordU = txtContraseña.getPassword();
            char [] passwordF = txtContraseña1.getPassword();
            for (int i=0; i<passwordU.length; i++){
                contraseñaU+=passwordU[i];
            }
            for (int i=0; i<passwordF.length; i++){
                contraseñaF+=passwordF[i];
            }
            System.out.println(Colegiado.buscarColegiado(matricula, HiloInicio.listaColegiados).getIdColegiados());
            System.out.println(laboratorio);
            System.out.println(Laboratorio.buscarLaboratorio(laboratorio, HiloInicio.listaLaboratorios).getIdLaboratorios());
            
        Facturantes.actualizarFacturante(conexion.getConnection(), Colegiado.buscarColegiado(matricula, HiloInicio.listaColegiados).getIdColegiados(), txtUsuario.getText(), contraseñaU, contraseñaF, Laboratorio.buscarLaboratorio(laboratorio, HiloInicio.listaLaboratorios).getIdLaboratorios());        
        conexion.cerrarConexion();
        cargartabla();
        lblColegiado.setText(null);
        txtContraseña.setText(null);
        txtContraseña1.setText(null);
        txtUsuario.setText(null);
        
    }//GEN-LAST:event_jButton1ActionPerformed
      

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblColegiado;
    private javax.swing.JTable tablaFacturantes;
    private javax.swing.JPasswordField txtContraseña;
    private javax.swing.JPasswordField txtContraseña1;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
