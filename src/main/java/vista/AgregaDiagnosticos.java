package vista;

import controlador.ConexionMariaDB;
import com.mxrck.autocompleter.TextAutoCompleter;
import static controlador.FuncionesTabla.alinear;
import static controlador.FuncionesTabla.alinearCentro;
import static controlador.HiloInicio.listaColegiados;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.sql.PreparedStatement;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;
import static controlador.HiloInicio.listaOS;
import java.util.ArrayList;
import modelo.Colegiado;
import modelo.DiagnosticoPami;
import static modelo.DiagnosticoPami.buscarDiagnostico;
import static modelo.DiagnosticoPami.buscarDiagnosticoBool;
import modelo.ObraSocial;

public class AgregaDiagnosticos extends javax.swing.JDialog {

    DefaultTableModel model;
    ArrayList<DiagnosticoPami> listaDiagnosticos;
    private modelo.ConexionMariaDB conexion;
    
    public AgregaDiagnosticos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaDiagnosticos = new ArrayList<>();
        DiagnosticoPami.cargarDiagnosticos(conexion.getConnection(), listaDiagnosticos);
        conexion.cerrarConexion();
        
        setLocationRelativeTo(null);        
        TextAutoCompleter textAutoAcompleter = new TextAutoCompleter(txtObrasocial);
        textAutoAcompleter.addItems(listaOS.toArray());
        textAutoAcompleter.setMode(0);
        textAutoAcompleter.setCaseSensitive(false);
        TextAutoCompleter textAutoAcompleterMatricula = new TextAutoCompleter(txtMatricula);
        textAutoAcompleterMatricula.addItems(listaColegiados.toArray());
        textAutoAcompleterMatricula.setMode(0);
        textAutoAcompleterMatricula.setCaseSensitive(false);
        tablaordenes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
    }   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaordenes = new javax.swing.JTable();
        txtordenes = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtMes = new javax.swing.JFormattedTextField();
        txtAño = new javax.swing.JFormattedTextField();
        jLabel1 = new javax.swing.JLabel();
        txtObrasocial = new javax.swing.JTextField();
        txtMatricula = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ordenes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaordenes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tablaordenes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaordenes.setToolTipText("");
        tablaordenes.setInheritsPopupMenu(true);
        tablaordenes.setOpaque(false);
        tablaordenes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaordenesKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tablaordenes);

        txtordenes.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtordenes.setForeground(new java.awt.Color(0, 102, 204));
        txtordenes.setToolTipText("Buscar por dni paciente o num paciente o nom paciente o num orden");
        txtordenes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtordenesKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(txtordenes, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtordenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtrar por Obra social", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Año:");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(51, 51, 51));
        jLabel18.setText("Colegiado:");

        txtMes.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtMes.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtMes.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtMes.setNextFocusableComponent(txtAño);
        txtMes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMesKeyPressed(evt);
            }
        });

        txtAño.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtAño.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtAño.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtAño.setNextFocusableComponent(txtObrasocial);
        txtAño.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAñoKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Obra Social:");

        txtObrasocial.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtObrasocial.setForeground(new java.awt.Color(0, 102, 204));
        txtObrasocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtObrasocialKeyPressed(evt);
            }
        });

        txtMatricula.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtMatricula.setForeground(new java.awt.Color(0, 102, 204));
        txtMatricula.setNextFocusableComponent(txtMes);
        txtMatricula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMatriculaKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Mes:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMes, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAño, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMatricula, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtObrasocial)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(txtAño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(txtMatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtObrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnSalir.setMnemonic('v');
        btnSalir.setText("Salir");
        btnSalir.setToolTipText("[Alt + s]");
        btnSalir.setPreferredSize(new java.awt.Dimension(80, 23));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnBuscar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728952 - magnifier zoom.png"))); // NOI18N
        btnBuscar.setMnemonic('u');
        btnBuscar.setText("Buscar");
        btnBuscar.setNextFocusableComponent(tablaordenes);
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        btnBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnBuscarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaordenesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaordenesKeyPressed
        DefaultTableModel temp = (DefaultTableModel) tablaordenes.getModel();
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            tablaordenes.transferFocus();
            evt.consume();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (tablaordenes.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
            } else {
                    if(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 5)!=null){
                String diagnostico=tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 5).toString();
                ConexionMariaDB cc = new ConexionMariaDB();
                Connection cn = cc.Conectar();
                String id_orden = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString();
                int id_diagnostico;
                
                if(buscarDiagnosticoBool(diagnostico, listaDiagnosticos)){
                try {
                    id_diagnostico= buscarDiagnostico(diagnostico, listaDiagnosticos).getId();
                    String sSQL3 = "update ordenes set id_diagnostico=? where id_orden=" + id_orden;
                    PreparedStatement pst = cn.prepareStatement(sSQL3);
                    pst.setInt(1, id_diagnostico);                   
                    int n4 = pst.executeUpdate();
                    if (n4 > 0) {
                        JOptionPane.showMessageDialog(null, "Orden Modificada...");
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
                }else{
                    JOptionPane.showMessageDialog(null, "No se encuentra el código");
                }
            } }
        }
    }//GEN-LAST:event_tablaordenesKeyPressed

    
    private void txtordenesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtordenesKeyReleased
    
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*" + txtordenes.getText() + ".*"));
            tablaordenes.setRowSorter(sorter);
    }//GEN-LAST:event_txtordenesKeyReleased

   
    
    public void cargartabla(){
                    
            ConexionMariaDB cc = new ConexionMariaDB();
            Connection cn = cc.Conectar();
            String periodo = txtAño.getText() + txtMes.getText();            
            String[] titulos = {"id", "Nº", "N° Afiliado", "Nombre Afiliado", " Número de orden", "Diagnóstico"};
            Object[] datos = new Object[6];
            model = new DefaultTableModel(null, titulos) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        
                        return column==5;
                    }
                };
            try {
                Statement St = cn.createStatement();
                ResultSet Rs;
                    Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.numero_afiliado, ordenes.nombre_afiliado, ordenes.numero_orden, diagnosticos_pami.codigo_diagnostico FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial LEFT JOIN diagnosticos_pami ON diagnosticos_pami.id_diagnosticos=ordenes.id_diagnostico WHERE ordenes.periodo=" + periodo + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtObrasocial.getText(), listaOS).getIdObraSocial() + " AND colegiados.id_colegiados="+Colegiado.buscarColegiadoCompleto(txtMatricula.getText(), listaColegiados).getIdColegiados());
                    
                int i = 1;
                while (Rs.next()) {
                    datos[0] = Rs.getInt(1);
                    datos[1] = completarceros(String.valueOf(i), 5);
                    datos[2] = Rs.getString(2);
                    datos[3] = Rs.getString(3);
                    datos[4] = Rs.getString(4);
                    datos[5] = Rs.getString(5);
                    
                    i++;
                    model.addRow(datos);
                }
                alinear();
                tablaordenes.setModel(model);
                
                tablaordenes.setRowSorter(new TableRowSorter(model));
                
                tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
                
                tablaordenes.getColumnModel().getColumn(1).setMaxWidth(70);
                tablaordenes.getColumnModel().getColumn(1).setMinWidth(70);
                tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(70);
                
                tablaordenes.getColumnModel().getColumn(2).setMaxWidth(150);
                tablaordenes.getColumnModel().getColumn(2).setMinWidth(150);
                tablaordenes.getColumnModel().getColumn(2).setPreferredWidth(150);
                
                tablaordenes.getColumnModel().getColumn(4).setMaxWidth(150);
                tablaordenes.getColumnModel().getColumn(4).setMinWidth(150);
                tablaordenes.getColumnModel().getColumn(4).setPreferredWidth(150);
                
                tablaordenes.getColumnModel().getColumn(5).setMaxWidth(80);
                tablaordenes.getColumnModel().getColumn(5).setMinWidth(80);
                tablaordenes.getColumnModel().getColumn(5).setPreferredWidth(80);
                
                tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                            
                txtordenes.requestFocus();
                btnBuscar.setEnabled(true);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);

            }       
    }

    String completarceros(String v, int d) {
        String ceros = "";
        if (v.length() < d) {
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        return v;
    }
    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtMatriculaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMatriculaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            txtObrasocial.requestFocus();

        }
    }//GEN-LAST:event_txtMatriculaKeyPressed

    private void txtObrasocialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtObrasocialKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnBuscar.requestFocus();
        }
    }//GEN-LAST:event_txtObrasocialKeyPressed

    private void btnBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnBuscarKeyPressed
      
        
        
    }//GEN-LAST:event_btnBuscarKeyPressed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed

        cargartabla();
        
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void txtMesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMesKeyPressed
        
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            txtAño.requestFocus();

        }
        
    }//GEN-LAST:event_txtMesKeyPressed

    private void txtAñoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAñoKeyPressed

      if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            txtMatricula.requestFocus();

        }
        
    }//GEN-LAST:event_txtAñoKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tablaordenes;
    private javax.swing.JFormattedTextField txtAño;
    private javax.swing.JTextField txtMatricula;
    private javax.swing.JFormattedTextField txtMes;
    private javax.swing.JTextField txtObrasocial;
    private javax.swing.JTextField txtordenes;
    // End of variables declaration//GEN-END:variables
}
