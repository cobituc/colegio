package vista;

import controlador.ConexionMariaDB;
import controlador.NumberToLetterConverter;
import controlador.camposobrasocial;
import static vista.Periodo.fecha2;
import java.awt.Color;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ProcesoPeriodo extends javax.swing.JDialog {

    HiloOrdenes hilo;
    public static String periodo;

    public ProcesoPeriodo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        iniciarSplash();
        hilo = new HiloOrdenes(progreso);
        hilo.start();
        hilo = null;
        this.dispose();
    }

    private void progresoStateChanged(javax.swing.event.ChangeEvent evt) {
        if (progreso.getValue() == 100) {
            this.dispose();
        }
    }

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    public class HiloOrdenes extends Thread {

        JProgressBar progreso;

        public HiloOrdenes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;

        }

        public void run() {
            // new Proceso(null, true).setVisible(true);
            int cantidad = 100 / 800;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab;

            ////////////////////////////////////////////
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            LinkedList<camposobrasocial> Resultados = new LinkedList<camposobrasocial>();
            Resultados.clear();
            String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0, pesos = 0, importetotal = 0, centavos = 0;

            String nombre_obra = "", cod_obra = "", matricula = "";

            try {
                Statement st3 = cn.createStatement();
                ResultSet rs3 = st3.executeQuery("SELECT id_colegiados,nombre_colegiado,matricula_colegiado,direccion_laboratorio,localidad_laboratorio FROM colegiados");
                while (rs3.next()) {
                    pesos = 0;
                    band = 0;
                    ordenesgral = 0;
                    practicasgral = 0;
                    Resultados.clear();
                    bandera = 0;
                    colegiado = rs3.getInt("id_colegiados");
                    matricula = rs3.getString("matricula_colegiado");
                    nombre_colegiado = rs3.getString("nombre_colegiado");
                    domicilio_lab = rs3.getString("direccion_laboratorio");////
                    localidad_lab = rs3.getString("localidad_laboratorio");
                    /////////////////////////////////////////////////////
                    Statement st2 = cn.createStatement();
                    String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial";// WHERE id_obrasocial"               
                    ResultSet rs2 = st2.executeQuery(sql2);

                    while (rs2.next()) {
                        String sql = "SELECT * FROM ordenes WHERE id_obrasocial=" + rs2.getString("id_obrasocial");
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sql);
                        rs.beforeFirst();
                        ordenes = 0;
                        practicas = 0;
                        total = 0;
                        while (rs.next()) {
                            if (rs.getString("id_obrasocial").equals(rs2.getString("id_obrasocial")) && colegiado == rs.getInt("id_colegiados") && periodo.equals(rs.getString("periodo"))) {
                                nombre_obra = rs2.getString("razonsocial_obrasocial");
                                cod_obra = rs2.getString("codigo_obrasocial");
                                uni_gastos = rs2.getString("importeunidaddegasto_obrasocial");
                                uni_Honorarios = rs2.getString("importeunidaddearancel_obrasocial");
                                Statement st4 = cn.createStatement();
                                String sql4 = "SELECT * FROM detalle_ordenes WHERE id_orden=" + rs.getString("id_orden");// WHERE id_obrasocial"               
                                ResultSet rs4 = st4.executeQuery(sql4);
                                rs4.beforeFirst();
                                while (rs4.next()) {
                                    if (rs.getInt("id_orden") == rs4.getInt("id_orden")) {
                                        ////////////////cargo la matriz para imprimir//////////                                   
                                        practicas = practicas + 1;
                                        practicasgral = practicasgral + 1;
                                        total = total + rs4.getDouble("precio_practica");
                                    }
                                }
                                ordenes = ordenes + 1;
                                ordenesgral = ordenesgral + 1;
                                band = 1;
                            }
                        }
                        if (band == 1) {
                            camposobrasocial tipo;
                            tipo = new camposobrasocial(cod_obra, nombre_obra, periodo, uni_gastos, uni_Honorarios, String.valueOf(total), String.valueOf(practicas), String.valueOf(ordenes));
                            Resultados.add(tipo);
                            bandera = 1;
                            band = 0;
                        }
                        pesos = pesos + total;
                    }

                    /////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////
                    if (bandera == 1) {
                        importetotal = pesos;
                        ///////////////////////////////////////////////////////////////////////////////////
                        centavos = Redondearcentavos(pesos);
                        pesos = pesos - centavos / 100;

                        total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                        total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                        JasperReport jasperReport;
                        try {
                            // JasperReport reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ObraSocial.jasper"));
                            JasperReport report = JasperCompileManager.compileReport("F:\\usuarios\\alumno\\Escritorio\\colegio bioquimicos\\src\\Reportes\\ObraSocial.jrxml");
                            ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                            Map parametros = new HashMap();
                            parametros.put("matricula", matricula);
                            parametros.put("apellidonombre", nombre_colegiado);
                            parametros.put("fecha", fecha2);
                            parametros.put("laboratorio", "F.BIOQ.QCA.Y FCIA.");
                            parametros.put("domicilio_lab", domicilio_lab);
                            parametros.put("localidad", localidad_lab);
                            parametros.put("pacientes", String.valueOf(ordenesgral));
                            parametros.put("practicas", String.valueOf(practicasgral));
                            parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                            parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                            parametros.put("total", String.valueOf(importetotal));
                            JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados));
                            JasperExportManager.exportReportToPdfFile(jPrint, "F:\\usuarios\\alumno\\Escritorio\\colegio bioquimicos\\src\\PDF\\" + periodo + matricula + ".pdf");
                        } catch (JRException ex) {
                            System.err.println("Error iReport: " + ex.getMessage());
                        }

                    }
                    progreso.setValue(0);
                    for (int j = 0; j < 100; j++) {
                        progreso.setValue(j);
                        pausa(1);//aumentamos la barra 
                    }
                    //porcentaje.setText(((indice * 100) / cantidad) + "%");
                    indice = indice + cantidad;
                    n++;
                    if (n == 800) {
                        rs3.last();
                    }
                }
                //////////////////////////////////////////////////////////////////////////////
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

            //con las lineas siguiente damos un tiempo para que se vea el funcionamiento de la barra 
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                //Logger.getLogger(jprogress.class.getName()).log(Level.SEVERE, null, ex);
            }

            progreso.setValue(100);
            dispose();
            addWindowListener(new java.awt.event.WindowAdapter() {
                public void windowClosing(java.awt.event.WindowEvent e) {
                    dispose();
                }
            });
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        progreso = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Realizando Declaración Jurada", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Cargar.gif"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("En Proceso espere unos minutos...");

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JProgressBar progreso;
    // End of variables declaration//GEN-END:variables
}
