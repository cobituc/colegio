package vista;

import controlador.ConexionMariaDB;
import static controlador.HiloInicio.listaOS;
import controlador.Impresor;
import controlador.NumberToLetterConverter;
import controlador.RowsRenderer;
import controlador.camposordenes_osde;
import controlador.camposordenes_ss;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import modelo.ObraSocial;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class RecepcionObrasSociales extends javax.swing.JDialog {

    DefaultTableModel model;
    int matricula, periodo, idColegiado;
    int contadorVerdes = 0;
    public static String obs;
    ArrayList<modelo.DeclaracionJurada> listaDeclaracion;
    private modelo.ConexionMariaDB conexion;
    Hiloobrasocial hilo2;
    int idObraSocial;
    int valor = 0;
    String fecha2;
    String periodoString;
    public static String observacion;
    int x, y;

    public RecepcionObrasSociales(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setTitle("Recepción de órdenes");
        txtPeriodo.requestFocus();
        tablaOS.setDefaultRenderer(Object.class, new RowsRenderer());
        cargarperiodo();
    }

    String devuelve_mes() {
        String mes = "", cadena = txtPeriodo.getText().substring(4, 6);
        if (cadena.equals("01")) {
            mes = "Enero";
        }
        if (cadena.equals("02")) {
            mes = "Febrero";
        }
        if (cadena.equals("03")) {
            mes = "Marzo";
        }
        if (cadena.equals("04")) {
            mes = "Abril";
        }
        if (cadena.equals("05")) {
            mes = "Mayo";
        }
        if (cadena.equals("06")) {
            mes = "Junio";
        }
        if (cadena.equals("07")) {
            mes = "Julio";
        }
        if (cadena.equals("08")) {
            mes = "Agosto";
        }
        if (cadena.equals("09")) {
            mes = "Septiembre";
        }
        if (cadena.equals("10")) {
            mes = "Octubre";
        }
        if (cadena.equals("11")) {
            mes = "Noviembre";
        }
        if (cadena.equals("12")) {
            mes = "Diciembre";
        }
        String periodo2 = mes + " " + txtPeriodo.getText().substring(0, 4);
        return periodo2;
    }

    void impresionDetalle() {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        Connection cn2 = mysql.Conectar();
        int indice = 1;
        int cantidad = 100 / 10;
        String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
        String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab, cuit;
        int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
        double total = 0.00, pesos = 0, importetotal = 0, centavos = 0, coseguro = 0.00, totalconcoseguro = 0.00, coseguro_ss = 0.00;
        int pacientes = 0, bandera_periodo = 0;
        LinkedList<camposordenes_osde> Resultados3 = new LinkedList<camposordenes_osde>();
        LinkedList<camposordenes_ss> Resultados_ss = new LinkedList<camposordenes_ss>();
        Resultados3.clear();
        Resultados_ss.clear();
        DecimalFormat df = new DecimalFormat("0.00");
        String nombre_obra = "", cod_obra = "", matricula = "";

        total_pesos_letras = "";
        total_centavos_letras = "";
        try {
            Statement st3 = cn2.createStatement();
            ResultSet rs3 = st3.executeQuery("SELECT id_colegiados,nombre_colegiado,matricula_colegiado,factura,direccion_laboratorio,localidad_laboratorio,periodos,estado_periodo,numerodireccion_laboratorio, periodos.periodo,estado_facturacion,cuil_colegiado \n"
                    + " FROM colegiados , periodos\n"
                    + " WHERE matricula_colegiado=" + txtMatricula.getText());

            while (rs3.next()) {
                int ban_caso = 0;
                System.out.println(rs3.getInt("matricula_colegiado"));
                colegiado = rs3.getInt("id_colegiados");
                matricula = rs3.getString("matricula_colegiado");
                nombre_colegiado = rs3.getString("nombre_colegiado");
                domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                localidad_lab = rs3.getString("localidad_laboratorio");
                cuit = rs3.getString("cuil_colegiado");
                /////////////////////////////////////////////////////
                /// SELECCIONO LAS OBRAS SOCIALES FACTURADAS POR EL BIOQUIMICO EN UN PERIODO
                Statement st6 = cn.createStatement();
                String sql6 = "SELECT DISTINCT(ordenes.id_obrasocial), Int_codigo_obrasocial "
                        + "FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados "
                        + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial "
                        + "WHERE ordenes.periodo=" + txtPeriodo.getText() + " and colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " and estado_orden!=0  order by (obrasocial.Int_codigo_obrasocial)";
                ResultSet rs6 = st6.executeQuery(sql6);
                /////////////////SELECCIONO TODAS LAS 
                String sql = "";
                //////////BIOMED NOA S.R.L.
                if (rs3.getInt("id_colegiados") == 1173) {
                    sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,round(detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial,2) as UB\n"
                            + "FROM ordenes \n"
                            + "INNER JOIN colegiados  USING (id_colegiados) \n"
                            + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                            + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                            + "WHERE ordenes.periodo=" + txtPeriodo.getText() + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                            + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 order by obrasocial.Int_codigo_obrasocial,ordenes.id_orden,detalle_ordenes.id_detalle,ip";

                } else {
                    sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,round(detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial,2) as UB\n"
                            + "FROM ordenes \n"
                            + "INNER JOIN colegiados  USING (id_colegiados) \n"
                            + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                            + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                            + "WHERE ordenes.periodo=" + txtPeriodo.getText() + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                            + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 order by obrasocial.Int_codigo_obrasocial,ordenes.id_orden, detalle_ordenes.id_detalle";

                }

                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs6.next()) {

                    System.out.println(rs6.getInt("Int_codigo_obrasocial") + " - 1");
                    pesos = 0;
                    pacientes = 0;
                    practicas = 0;
                    coseguro = 0;
                    coseguro_ss = 0;
                    Resultados3.clear();
                    Resultados_ss.clear();
                    total = 0;
                     long id_ordenes = 0;

                    ////////////////////////DATOS OBRASOCIAL/////////////////////////////
                    Statement st2 = cn.createStatement();
                    String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial "
                            + "FROM obrasocial WHERE id_obrasocial=" + rs6.getInt("ordenes.id_obrasocial");
                    ResultSet rs2 = st2.executeQuery(sql2);
                    rs2.next();
                    nombre_obra = rs2.getString("razonsocial_obrasocial");
                    cod_obra = rs2.getString("codigo_obrasocial");
                    uni_gastos = df.format(rs2.getDouble("importeunidaddegasto_obrasocial"));
                    uni_Honorarios = df.format(rs2.getDouble("importeunidaddearancel_obrasocial"));
                    /////////////////////////////////////////////////////////////////////
                    if (ban_caso == 1) {
                        if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                            System.out.println("caso base");
                            camposordenes_ss tipo_ss;
                            if (id_ordenes == 0) {
                                System.out.println("orden iugual a 0");
                                tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));

                                System.out.println("pasa tipo_ss");
                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {
                                    System.out.println("orden distinto  a " + rs.getInt("id_orden"));
                                    tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    System.out.println("orden igual  a " + rs.getInt("id_orden"));
                                    tipo_ss = new camposordenes_ss("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''", df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                }
                            }
                            total = Redondeardosdigitos(total);
                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            coseguro = Redondeardosdigitos(coseguro);
                            coseguro = coseguro + Redondeardosdigitos(rs.getDouble("detalle_ordenes.coseguro"));
                            practicas = practicas + 1;
                            Resultados_ss.add(tipo_ss);
                            bandera = 1;

                        } else {
                            camposordenes_osde tipo;
                            if (id_ordenes == 0) {
                                tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {
                                    tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                    band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    tipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                }
                            }
                            ////////////////cargo la matriz para imprimir//////////                                   
                            total = Redondeardosdigitos(total);
                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            practicas = practicas + 1;
                            Resultados3.add(tipo);
                            bandera = 1;
                            ban_caso = 0;
                        }

                    }
                    /////////////////////////////////////////////////////////////////////////////////////
                    if (rs6.getString(1).equals("89") || rs6.getString(1).equals("95") || rs6.getString(1).equals("21") || rs6.getString(1).equals("108")) {
                        Statement st7 = cn.createStatement();
                        String sql7 = "SELECT  sum(coseguro)\n"
                                + "FROM ordenes\n"
                                + "WHERE periodo=" + txtPeriodo.getText() + " and estado_orden=1 and id_obrasocial=" + rs6.getString(1) + " and id_colegiados=" + rs3.getInt("id_colegiados");
                        ResultSet rs7 = st7.executeQuery(sql7);
                        if (rs7.next()) {
                            coseguro = 0;
                            coseguro = Redondeardosdigitos(rs7.getDouble(1));
                        }
                    }

                    while (rs.next() && rs6.getInt("id_obrasocial") == rs.getInt("obrasocial.id_obrasocial")) {

                        if (rs6.getInt("id_obrasocial") == rs.getInt("obrasocial.id_obrasocial")) {

                           /* if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                    || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                    || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                                //System.out.println("pasa el caso base");
                                camposordenes_ss tipo_ss;
                                if (id_ordenes == 0) {
                                    System.out.println("pasa el caso base 0:" + rs.getString("nombre_afiliado"));
                                    tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                    id_ordenes = rs.getInt("id_orden");

                                    pacientes = pacientes + 1;
                                } else {
                                    if (id_ordenes != rs.getInt("id_orden")) {
                                        System.out.println("pasa el caso base !=:" + rs.getString("nombre_afiliado"));
                                        tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                        System.out.println("pasa tipo_ss:" + rs.getString("nombre_afiliado"));
                                        band = 1;
                                        id_ordenes = rs.getLong("id_orden");
                                        System.out.println("Idordenes:" + id_ordenes);
                                        pacientes = pacientes + 1;
                                    } else {
                                        tipo_ss = new camposordenes_ss("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''", df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                        //ipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                    }

                                }
                                //System.out.println(tipo_ss);
                                total = Redondeardosdigitos(total);
                                total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                coseguro = Redondeardosdigitos(coseguro);
                                coseguro = coseguro + Redondeardosdigitos(rs.getDouble("detalle_ordenes.coseguro"));
                                practicas = practicas + 1;
                                System.out.println("importe:" + total);
                                System.out.println("coseguro:" + coseguro);
                                System.out.println("Practica:" + practicas);
                                Resultados_ss.add(tipo_ss);
                                bandera = 1;

                            } else {*/
                                camposordenes_osde tipo;
                                if (id_ordenes == 0) {
                                    //tipo = new camposordenes(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                    tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                    //band = 1;
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    if (id_ordenes != rs.getInt("id_orden")) {
                                        //tipo = new camposordenes(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                        tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                        band = 1;
                                        id_ordenes = rs.getInt("id_orden");

                                        pacientes = pacientes + 1;
                                    } else {
                                        //tipo = new camposordenes("''", "''", rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"));
                                        tipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                    }

                                }

                                total = Redondeardosdigitos(total);
                                total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                                practicas = practicas + 1;
                                Resultados3.add(tipo);
                                bandera = 1;
                         //   }
                        }
                    }

                    if (ban_caso == 0) {
                        ban_caso = 1;
                    }

                    pesos = pesos + total;
                    /////////////////////////////////////////////////////
                    if (bandera == 1) {
                        System.out.println("llega antes de reporte");
                        pesos = Redondeardosdigitos(pesos);
                        importetotal = pesos;
                        totalconcoseguro = importetotal - coseguro;
                        ///////////////Mes y año en letras////////////
                        String periodo2 = devuelve_mes();
                        //////////////////////////////////////////////
                        centavos = Redondearcentavos(pesos);
                        pesos = Redondeardosdigitos(pesos - centavos / 100);
                        total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                        total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                        Map parametros = new HashMap();
                        parametros.put("matricula", matricula);
                        parametros.put("periodo", periodo2);
                        parametros.put("fecha", fecha2);
                        parametros.put("obra_social", nombre_obra);
                        parametros.put("num_obra_social", cod_obra);
                        parametros.put("laboratorio", nombre_colegiado);
                        parametros.put("domicilio_lab", domicilio_lab);
                        parametros.put("localidad", localidad_lab);
                        parametros.put("pacientes", String.valueOf(pacientes));
                        parametros.put("practicas", String.valueOf(practicas));

                        if (cod_obra.equals("511")) { ////////comparo para saber que pdf realizar 511 - MEDIFE

                            try {
                                // String periodo2 = mes + " " + año;
                                pesos = pesos * 1.21;
                                centavos = Redondearcentavos(pesos);
                                pesos = Redondeardosdigitos(pesos - centavos / 100);
                                total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);

                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_IVA.jasper"));

                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("Subtotal", df.format(importetotal));
                                parametros.put("IVA", df.format(importetotal * 0.21));
                                parametros.put("total", df.format(importetotal * 1.21));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");
                                JasperPrintManager.printReport(jPrint, false);
                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                                JOptionPane.showMessageDialog(null, ex.getMessage());
                            }
                        }

                        if (cod_obra.equals("50015") || cod_obra.equals("50016") || cod_obra.equals("2700") || cod_obra.equals("40813")) {
                            try {
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_coseguro.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                ///Coseguro
                                parametros.put("coseguro", df.format(coseguro));
                                parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                                if (cod_obra.equals("2700") || cod_obra.equals("40813")) {
//                                            JasperViewer.viewReport(jPrint, true);
                                    JasperPrintManager.printReport(jPrint, false);
                                }

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                                JOptionPane.showMessageDialog(null, ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                            try {
                                System.out.println("ingresa a reporte 180*");
                                pesos = totalconcoseguro;
                                centavos = Redondearcentavos(pesos);
                                pesos = Redondeardosdigitos(pesos - centavos / 100);
                                total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);

                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_SS.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                ///Coseguro
                                parametros.put("cuit", cuit);
                                parametros.put("coseguro", df.format(coseguro));
                                parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados_ss));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");
                                //JasperViewer.viewReport(jPrint, true);
                                //JasperPrintManager.printReport(jPrint, false);
                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                                JOptionPane.showMessageDialog(null, ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("3100") || cod_obra.equals("3101") || cod_obra.equals("3102")) {
                            try {
                                //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_osde.jrxml");
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_OSDE.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");
                                JasperPrintManager.printReport(jPrint, false);
                                //JasperViewer.viewReport(jPrint, true);

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                                JOptionPane.showMessageDialog(null, ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("13800")) {
                            try {
                                //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_Fecha.jrxml");
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_Fecha.jasper"));
                                ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");
                                //JasperPrintManager.printReport(jPrint, false);
                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                                JOptionPane.showMessageDialog(null, ex.getMessage());
                            }
                        }

                        if (cod_obra.equals("50015") || cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805") || cod_obra.equals("1807")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("511")
                                || cod_obra.equals("3100") || cod_obra.equals("3101") || cod_obra.equals("3102") || cod_obra.equals("13800")
                                || cod_obra.equals("50016") || cod_obra.equals("2700") || cod_obra.equals("40813")) {
                        } else {

                            System.out.println(cod_obra);
                            try {
                                //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_Fecha.jrxml");
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_Fecha.jasper"));
                                ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");
                                JasperPrintManager.printReport(jPrint, false);
                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                                JOptionPane.showMessageDialog(null, ex.getMessage());
                            }

                        }
                    }
                }
                indice = indice + cantidad;
            }

            JOptionPane.showMessageDialog(null, "Los detalles fueron generados correctamente");

        } catch (Exception e) {
        }

    }

    void leer() {

        int id_declaracion = Integer.valueOf(txtLector.getText().substring(0, 8));
        int ordenes = Integer.valueOf(txtLector.getText().substring(8, 13));
        double importe = Double.valueOf(txtLector.getText().substring(13, 24));

        for (int i = 0; i < tablaOS.getRowCount(); i++) {

            if (id_declaracion == Integer.valueOf(tablaOS.getValueAt(i, 0).toString())) {
                if (Integer.valueOf(tablaOS.getValueAt(i, 6).toString()) == 0) {
                    if (importe != Double.valueOf(tablaOS.getValueAt(i, 3).toString())
                            || ordenes != Integer.valueOf(tablaOS.getValueAt(i, 5).toString())) {

                        if (importe != Double.valueOf(tablaOS.getValueAt(i, 3).toString())) {
                            listaDeclaracion.get(i).setObservacion("Difiere importe");
                        }
                        if (ordenes != Integer.valueOf(tablaOS.getValueAt(i, 5).toString())) {
                            listaDeclaracion.get(i).setObservacion("Difiere el número de órdenes");
                        }
                    }
                    listaDeclaracion.get(i).setEstado(1);
                    break;

                } else if (Integer.valueOf(tablaOS.getValueAt(i, 6).toString()) == 2) {

                    if (importe == Double.valueOf(tablaOS.getValueAt(i, 3).toString())) {
                        listaDeclaracion.get(i).setEstado(1);
                        break;
                    }

                } else {
                    break;
                }
            }
        }
    }

    void cargarListaDeclaracionJurada(int matricula, int periodo) {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaDeclaracion = new ArrayList<>();
        modelo.DeclaracionJurada.cargarDeclaracion(conexion.getConnection(), listaDeclaracion, matricula, periodo);
        conexion.cerrarConexion();
    }

    void cargartabla(int matricula, int periodo) {

//        ((DefaultTableCellRenderer) tablaOS.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        JTableHeader th;
        th = tablaOS.getTableHeader();
        Font fuente = new Font("Thaoma", Font.BOLD, 13);
        th.setFont(fuente);
        String[] Titulo = {"id", "Cod OS", "Obra Social", "Importe", "Prácticas", "Órdenes", "Estado", "Obs"};
        Object[] Registros = new Object[8];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (modelo.DeclaracionJurada declaracion : listaDeclaracion) {

            Registros[0] = declaracion.getId();
            lblColegiado.setText(declaracion.getColegiado());
            Registros[1] = declaracion.getCodigoOS();
            Registros[2] = declaracion.getObraSocial();
            Registros[3] = declaracion.getImporte();
            Registros[4] = declaracion.getPracticas();
            Registros[5] = declaracion.getOrdenes();
            Registros[6] = declaracion.getEstado();
            Registros[7] = declaracion.getObservacion();
            idColegiado = declaracion.getIdColegiados();
            model.addRow(Registros);

        }

        tablaOS.setModel(model);
        tablaOS.setAutoCreateRowSorter(true);
        tablaOS.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaOS.getColumnModel().getColumn(0).setMinWidth(0);
        tablaOS.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaOS.getColumnModel().getColumn(6).setMaxWidth(0);
        tablaOS.getColumnModel().getColumn(6).setMinWidth(0);
        tablaOS.getColumnModel().getColumn(6).setPreferredWidth(0);
        tablaOS.getColumnModel().getColumn(7).setMaxWidth(0);
        tablaOS.getColumnModel().getColumn(7).setMinWidth(0);
        tablaOS.getColumnModel().getColumn(7).setPreferredWidth(0);
        tablaOS.getColumnModel().getColumn(1).setPreferredWidth(40);
        tablaOS.getColumnModel().getColumn(2).setPreferredWidth(270);
        tablaOS.getColumnModel().getColumn(3).setPreferredWidth(70);
        tablaOS.getColumnModel().getColumn(4).setPreferredWidth(50);
        tablaOS.getColumnModel().getColumn(5).setPreferredWidth(50);
    }

    void descargarpdfvalidacion() throws PrinterException {

        String ipddjj = "http://ddjj.cobituc.info/";//
        String url = ipddjj + "validacion/" + periodo + "-" + matricula + ".pdf";
        String name = periodo + "-" + matricula + ".pdf";
        String folder = "C:\\Descargas-CBT\\";
        try {
            File dir = new File(folder);

            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    return; // no se pudo crear la carpeta de destino
                }
            }
            File file = new File(folder + name);
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            System.out.println("\nempezando descarga: \n");
            System.out.println(">> URL: " + url);
            System.out.println(">> Nombre: " + name);
            System.out.println(">> tamaño: " + conn.getContentLength() + " bytes");
            InputStream in = conn.getInputStream();
            OutputStream out = new FileOutputStream(file);
            int b = 0;
            while (b != -1) {
                b = in.read();
                if (b != -1) {
                    out.write(b);
                }
            }
            out.close();
            in.close();
        } catch (MalformedURLException e) {
            System.out.println("la url: " + url + " no es valida!");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Sus comprobantes esta en cola de espera, intente nuevamente...");
        }
        java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
        java.io.File fichero = new java.io.File("C:\\Descargas-CBT\\" + periodo + "-" + matricula + ".pdf");
        if (desktop.isSupported(Desktop.Action.PRINT)) {
            try {
                Impresor doc =new Impresor();
                doc.imprimir(fichero);
               // desktop.print(fichero);
                 System.out.println("archivo impreso");
            } catch (IOException e) {
                System.out.print("El sistema no permite imprimir usando la clase Desktop");
            }
        } else {
            System.out.print("El sistema no permite imprimir usando la clase Desktop");
        }
    }

    void cargarperiodo() {
        ////////////////////////////////////////////////////////////////////
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT * FROM periodos ";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            periodoString = rs.getString("periodo");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        //////////////////////////////////

        SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha2 = formato2.format(currentDate);
        /////////////////////////////////////////////////////////       

        calendar.setTime(currentDate);

    }

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    public double Redondeardosdigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    public int completatotal(double numero) {
        int n = (int) numero;
        return n;
    }

    public class Hiloobrasocial extends Thread {

        public Hiloobrasocial() {
            super();
        }

        @Override
        public void run() {

            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab, cuit;
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            LinkedList<camposordenes_osde> Resultados3 = new LinkedList<>();
            LinkedList<camposordenes_ss> Resultados_ss = new LinkedList<>();
            Resultados3.clear();
            Resultados_ss.clear();
            String nombre_colegiado;
            int practicas, bandera = 0;
            double total, pesos, importetotal, centavos, coseguro, totalconcoseguro;
            int pacientes;
            DecimalFormat df = new DecimalFormat("0.00");
            String nombre_obra, cod_obra, matricula;
            int bandera20012 = 0;

            try {
                Statement st3 = cn.createStatement();
                ResultSet rs3 = st3.executeQuery("SELECT id_colegiados,nombre_colegiado,matricula_colegiado,factura,direccion_laboratorio,localidad_laboratorio,periodos,estado_periodo,numerodireccion_laboratorio, periodos.periodo,estado_facturacion,cuil_colegiado \n"
                        + " FROM colegiados , periodos\n"
                        + " WHERE matricula_colegiado=" + txtMatricula.getText() + " and tipo_profesional='B'");

                if (rs3.next()) {
                    System.out.println("matricula: " + rs3.getInt("matricula_colegiado"));
                    System.out.println("id_colegiado: " + rs3.getInt("id_colegiados"));
                    matricula = rs3.getString("matricula_colegiado");
                    nombre_colegiado = rs3.getString("nombre_colegiado");
                    domicilio_lab = rs3.getString("direccion_laboratorio") + " " + rs3.getString("numerodireccion_laboratorio");////
                    localidad_lab = rs3.getString("localidad_laboratorio");
                    cuit = rs3.getString("cuil_colegiado");

                    String sql = "";

                    if (rs3.getInt("id_colegiados") == 1173 && (Integer.valueOf(sql) == 24 || idObraSocial == 94)) {
                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtPeriodo.getText() + " and ordenes.id_obrasocial=" + ObraSocial.buscarOS(tablaOS.getValueAt(tablaOS.getSelectedRow(), 2).toString(), listaOS) + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0  order by ip,ordenes.id_orden,detalle_ordenes.id_detalle";
                        bandera20012 = 1;
                        System.out.println("ingresa en 20012");

                    } else {
                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtPeriodo.getText() + " and ordenes.id_obrasocial=" + idObraSocial + "  AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 ";

                        System.out.println("ingresa en otras os");
                    }

                    if ((idObraSocial == 24 || idObraSocial == 94) && valor == 3) {

                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtPeriodo.getText() + " and ordenes.id_obrasocial=" + idObraSocial + " and tipo_orden=" + valor + " AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 ";

                    } else if (bandera20012 == 0) {

                        sql = "SELECT detalle_ordenes.*, ordenes.*, colegiados.matricula_colegiado,obrasocial.int_codigo_obrasocial,obrasocial.id_obrasocial,obrasocial.id_obrasocial,detalle_ordenes.precio_practica/obrasocial.importeunidaddearancel_obrasocial as UB\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados  USING (id_colegiados) \n"
                                + "INNER JOIN detalle_ordenes USING (id_orden) \n"
                                + "INNER JOIN obrasocial USING (id_obrasocial) \n"
                                + "WHERE ordenes.periodo=" + txtPeriodo.getText() + " and ordenes.id_obrasocial=" + idObraSocial + " and tipo_orden!=3 AND colegiados.id_colegiados=" + rs3.getInt("id_colegiados") + " \n"
                                + "AND ordenes.estado_orden!=0 and detalle_ordenes.estado=0 ";
                    }

                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);
                    pesos = 0;
                    pacientes = 0;
                    practicas = 0;
                    coseguro = 0;
                    Resultados3.clear();
                    Resultados_ss.clear();
                    total = 0;
                    int id_ordenes = 0;
                    Statement st2 = cn.createStatement();
                    String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial WHERE id_obrasocial= " + idObraSocial;
                    ResultSet rs2 = st2.executeQuery(sql2);
                    rs2.next();
                    nombre_obra = rs2.getString("razonsocial_obrasocial");
                    cod_obra = rs2.getString("codigo_obrasocial");

                    while (rs.next()) {
                        if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {

                            camposordenes_ss tipo_ss;
                            if (id_ordenes == 0) {
                                tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {
                                    tipo_ss = new camposordenes_ss(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"), df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));

                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    tipo_ss = new camposordenes_ss("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''", df.format(rs.getDouble("UB")), df.format(rs.getDouble("detalle_ordenes.coseguro")));
                                }
                            }
                            total = Redondeardosdigitos(total);
                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            coseguro = Redondeardosdigitos(coseguro);
                            coseguro = coseguro + Redondeardosdigitos(rs.getDouble("detalle_ordenes.coseguro"));
                            practicas = practicas + 1;
                            Resultados_ss.add(tipo_ss);
                            bandera = 1;

                        } else {
                            camposordenes_osde tipo;
                            if (id_ordenes == 0) {

                                tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));

                                id_ordenes = rs.getInt("id_orden");
                                pacientes = pacientes + 1;
                            } else {
                                if (id_ordenes != rs.getInt("id_orden")) {

                                    tipo = new camposordenes_osde(rs.getString("numero_afiliado"), rs.getString("nombre_afiliado"), rs.getString("numero_orden"), rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), rs.getString("fecha_orden"));
                                    id_ordenes = rs.getInt("id_orden");
                                    pacientes = pacientes + 1;
                                } else {
                                    tipo = new camposordenes_osde("''", "''", "''", rs.getString("cod_practica"), rs.getString("nombre_practica"), df.format(rs.getDouble("precio_practica")), rs.getString("cod_practica_fac"), "''");
                                }
                            }
                            total = Redondeardosdigitos(total);
                            total = total + Redondeardosdigitos(rs.getDouble("precio_practica"));
                            practicas = practicas + 1;
                            Resultados3.add(tipo);
                            bandera = 1;
                        }

                    }
                    if (idObraSocial == 89 || idObraSocial == 21 || idObraSocial == 108 || idObraSocial == 95) {

                        Statement st7 = cn.createStatement();
                        String sql7 = "SELECT  sum(coseguro)\n"
                                + "FROM ordenes\n"
                                + "WHERE periodo=" + txtPeriodo.getText() + " and estado_orden=1 and id_obrasocial=" + idObraSocial + " and id_colegiados=" + rs3.getInt("id_colegiados");
                        ResultSet rs7 = st7.executeQuery(sql7);
                        if (rs7.next()) {
                            coseguro = 0;
                            coseguro = Redondeardosdigitos(rs7.getDouble(1));
                        }
                    }
                    pesos = pesos + total;
                    if (bandera == 1) {
                        System.out.println("Sale del while: " + cod_obra);
                        pesos = Redondeardosdigitos(pesos);
                        importetotal = pesos;
                        totalconcoseguro = importetotal - coseguro;
                        String mes = "", año = txtPeriodo.getText().substring(0, 3);
                        String cadena = txtPeriodo.getText().substring(4);
                        switch (cadena) {
                            case "01":
                                mes = "Enero";
                                break;
                            case "02":
                                mes = "Febrero";
                                break;
                            case "03":
                                mes = "Marzo";
                                break;
                            case "04":
                                mes = "Abril";
                                break;
                            case "05":
                                mes = "Mayo";
                                break;
                            case "06":
                                mes = "Junio";
                                break;
                            case "07":
                                mes = "Julio";
                                break;
                            case "08":
                                mes = "Agosto";
                                break;
                            case "09":
                                mes = "Septiembre";
                                break;
                            case "10":
                                mes = "Octubre";
                                break;
                            case "11":
                                mes = "Noviembre";
                                break;
                            case "12":
                                mes = "Diciembre";
                                break;
                            default:
                                break;
                        }
                        String periodo2 = mes + " " + año;
                        centavos = Redondearcentavos(pesos);
                        pesos = Redondeardosdigitos(pesos - centavos / 100);
                        total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                        total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                        Map parametros = new HashMap();
                        parametros.put("matricula", matricula);
                        parametros.put("periodo", periodo2);
                        parametros.put("fecha", fecha2);
                        if ((cod_obra.equals("3100") || cod_obra.equals("3102")) && valor == 3) {
                            nombre_obra = nombre_obra + " INTERNADO";
                            parametros.put("obra_social", nombre_obra);
                        } else {
                            parametros.put("obra_social", nombre_obra);
                        }
                        parametros.put("num_obra_social", cod_obra);
                        parametros.put("laboratorio", nombre_colegiado);
                        parametros.put("domicilio_lab", domicilio_lab);
                        parametros.put("localidad", localidad_lab);
                        parametros.put("pacientes", String.valueOf(pacientes));
                        parametros.put("practicas", String.valueOf(practicas));
                        /////////////////////////////////////
                        JDialog viewer = new JDialog(new javax.swing.JFrame(), "Detalle", true);
                        viewer.setSize(800, 600);
                        viewer.setLocationRelativeTo(null);
                        JasperViewer jv = null;
                        JasperPrint jPrint = null;

                        if (cod_obra.equals("511")) {

                            try {
                                pesos = pesos * 1.21;
                                centavos = Redondearcentavos(pesos);
                                pesos = Redondeardosdigitos(pesos - centavos / 100);
                                total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_IVA.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("Subtotal", df.format(importetotal));
                                parametros.put("IVA", df.format(importetotal * 0.21));
                                parametros.put("total", df.format(importetotal * 1.21));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");
                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }

                        if (cod_obra.equals("50015") || cod_obra.equals("2700") || cod_obra.equals("40813") || cod_obra.equals("50016")) {
                            try {
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_coseguro.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                parametros.put("coseguro", df.format(coseguro));
                                parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));

                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("1807")) {
                            try {
                                pesos = totalconcoseguro;
                                centavos = Redondearcentavos(pesos);
                                pesos = Redondeardosdigitos(pesos - centavos / 100);
                                total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                                total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_SS.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                parametros.put("cuit", cuit);
                                parametros.put("coseguro", df.format(coseguro));
                                parametros.put("totalconcoseguro", df.format(totalconcoseguro));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados_ss));

                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("3100") || cod_obra.equals("3101") || cod_obra.equals("3102")) {
                            try {
                                //JasperReport report = JasperCompileManager.compileReport("C:\\PROYECTO CBT FACTURACION\\Generador facturacion\\src\\Reportes\\Ordenes_osde.jrxml");
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_OSDE.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }
                        }
                        if (cod_obra.equals("50015") || cod_obra.equals("1800") || cod_obra.equals("1801") || cod_obra.equals("1802")
                                || cod_obra.equals("1803") || cod_obra.equals("1804") || cod_obra.equals("1805")
                                || cod_obra.equals("1806") || cod_obra.equals("1810") || cod_obra.equals("511") || cod_obra.equals("1807") || cod_obra.equals("2700")
                                || cod_obra.equals("40813") || cod_obra.equals("50016")) {
                        } else {

                            System.out.println(cod_obra);
                            try {
                                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes_Fecha.jasper"));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                                parametros.put("total", df.format(importetotal));
                                jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados3));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + txtPeriodo.getText() + "-" + matricula + "-" + cod_obra + ".pdf");

                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }

                        }
                        jv = new JasperViewer(jPrint);
                        viewer.getContentPane().add(jv.getContentPane());
                        viewer.setVisible(true);
                    }

                }
                JOptionPane.showMessageDialog(null, "Los detalles fueron generados correctamente");
                cn.close();
            } catch (Exception e) {
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        rSButtonIconOne1 = new RSMaterialComponent.RSButtonIconOne();
        jPanel3 = new javax.swing.JPanel();
        txtPeriodo = new RSMaterialComponent.RSTextFieldOne();
        panel = new javax.swing.JPanel();
        btnMostrar = new RSMaterialComponent.RSButtonMaterialIconUno();
        txtMatricula = new RSMaterialComponent.RSTextFieldOne();
        jPanel5 = new javax.swing.JPanel();
        txtLector = new RSMaterialComponent.RSTextFieldOne();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaOS = new RSMaterialComponent.RSTableMetroCustom();
        jPanel7 = new javax.swing.JPanel();
        btnProcesar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnActualizarMontos = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnBajaOs = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnImprimirRotulos = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnSalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        lblColegiado = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel2MouseDragged(evt);
            }
        });
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel2MousePressed(evt);
            }
        });

        rSButtonIconOne1.setBackground(new java.awt.Color(214, 45, 32));
        rSButtonIconOne1.setBackgroundHover(new java.awt.Color(142, 68, 55));
        rSButtonIconOne1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        rSButtonIconOne1.setPreferredSize(new java.awt.Dimension(20, 20));
        rSButtonIconOne1.setSizeIcon(20.0F);
        rSButtonIconOne1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rSButtonIconOne1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(418, Short.MAX_VALUE)
                .addComponent(rSButtonIconOne1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rSButtonIconOne1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(66, 133, 200));

        txtPeriodo.setForeground(new java.awt.Color(51, 51, 51));
        txtPeriodo.setBorderColor(new java.awt.Color(255, 255, 255));
        txtPeriodo.setPhColor(new java.awt.Color(51, 51, 51));
        txtPeriodo.setPlaceholder("Periodo");
        txtPeriodo.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtPeriodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPeriodoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtPeriodo, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        panel.setBackground(new java.awt.Color(66, 133, 200));

        btnMostrar.setBackground(new java.awt.Color(255, 255, 255));
        btnMostrar.setForeground(new java.awt.Color(0, 0, 0));
        btnMostrar.setText("Mostrar");
        btnMostrar.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnMostrar.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnMostrar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnMostrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SHOW_CHART);
        btnMostrar.setRound(20);
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });

        txtMatricula.setForeground(new java.awt.Color(51, 51, 51));
        txtMatricula.setBorderColor(new java.awt.Color(255, 255, 255));
        txtMatricula.setPhColor(new java.awt.Color(51, 51, 51));
        txtMatricula.setPlaceholder("Matrícula");
        txtMatricula.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtMatricula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMatriculaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtMatricula, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtMatricula, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .addComponent(btnMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(66, 133, 200));

        txtLector.setForeground(new java.awt.Color(51, 51, 51));
        txtLector.setBorderColor(new java.awt.Color(255, 255, 255));
        txtLector.setPhColor(new java.awt.Color(51, 51, 51));
        txtLector.setPlaceholder("Código de barra");
        txtLector.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtLector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLectorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtLector, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtLector, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tablaOS.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaOS.setForeground(new java.awt.Color(255, 255, 255));
        tablaOS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaOS.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaOS.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaOS.setBorderHead(null);
        tablaOS.setBorderRows(null);
        tablaOS.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaOS.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaOS.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaOS.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaOS.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaOS.setGridColor(new java.awt.Color(15, 157, 88));
        tablaOS.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaOS.setShowHorizontalLines(true);
        tablaOS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaOSMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tablaOS);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel7.setBackground(new java.awt.Color(66, 133, 200));

        btnProcesar.setBackground(new java.awt.Color(255, 255, 255));
        btnProcesar.setForeground(new java.awt.Color(0, 0, 0));
        btnProcesar.setText("Procesar");
        btnProcesar.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnProcesar.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnProcesar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnProcesar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CHECK);
        btnProcesar.setRound(20);
        btnProcesar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcesarActionPerformed(evt);
            }
        });

        btnActualizarMontos.setBackground(new java.awt.Color(255, 255, 255));
        btnActualizarMontos.setForeground(new java.awt.Color(0, 0, 0));
        btnActualizarMontos.setText("Actualizar montos");
        btnActualizarMontos.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnActualizarMontos.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnActualizarMontos.setForegroundText(new java.awt.Color(51, 51, 51));
        btnActualizarMontos.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.VERIFIED_USER);
        btnActualizarMontos.setRound(20);
        btnActualizarMontos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarMontosActionPerformed(evt);
            }
        });

        btnBajaOs.setBackground(new java.awt.Color(255, 255, 255));
        btnBajaOs.setForeground(new java.awt.Color(0, 0, 0));
        btnBajaOs.setText("Baja OS");
        btnBajaOs.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnBajaOs.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnBajaOs.setForegroundText(new java.awt.Color(51, 51, 51));
        btnBajaOs.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.DELETE);
        btnBajaOs.setRound(20);
        btnBajaOs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBajaOsActionPerformed(evt);
            }
        });

        btnImprimirRotulos.setBackground(new java.awt.Color(255, 255, 255));
        btnImprimirRotulos.setForeground(new java.awt.Color(0, 0, 0));
        btnImprimirRotulos.setText("Imprimir rótulos");
        btnImprimirRotulos.setBackgroundHover(new java.awt.Color(66, 133, 200));
        btnImprimirRotulos.setForegroundIcon(new java.awt.Color(66, 133, 200));
        btnImprimirRotulos.setForegroundText(new java.awt.Color(51, 51, 51));
        btnImprimirRotulos.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.PRINT);
        btnImprimirRotulos.setRound(20);
        btnImprimirRotulos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirRotulosActionPerformed(evt);
            }
        });

        btnSalir.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir.setText("Salir");
        btnSalir.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnSalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnSalir.setRound(20);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnProcesar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnBajaOs, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnActualizarMontos, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnImprimirRotulos, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnProcesar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizarMontos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBajaOs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnImprimirRotulos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        lblColegiado.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblColegiado.setForeground(new java.awt.Color(255, 255, 255));
        lblColegiado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(lblColegiado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(12, 12, 12)
                .addComponent(lblColegiado, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rSButtonIconOne1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSButtonIconOne1ActionPerformed

        dispose();

    }//GEN-LAST:event_rSButtonIconOne1ActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed

        periodo = Integer.valueOf(txtPeriodo.getText());
        matricula = Integer.valueOf(txtMatricula.getText());
        cargarListaDeclaracionJurada(matricula, periodo);
        cargartabla(matricula, periodo);
        txtLector.requestFocus();

    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnProcesarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcesarActionPerformed

        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        String update = "";

        int n = 0;

        for (int i = 0; i < tablaOS.getRowCount(); i++) {
            if (tablaOS.getValueAt(i, 7) != null) {
                obs = tablaOS.getValueAt(i, 7).toString();
            }
            update = "UPDATE declaracion_jurada SET estado=?, observacion=?, id_usuario=? WHERE id_declaracion_jurada=" + Integer.valueOf(tablaOS.getValueAt(i, 0).toString());
            System.out.println(Integer.valueOf(tablaOS.getValueAt(i, 0).toString()));
            PreparedStatement pst;
            try {
                pst = cn.prepareStatement(update);
                pst.setInt(1, Integer.valueOf(tablaOS.getValueAt(i, 6).toString()));
                if (tablaOS.getValueAt(i, 7) != null) {
                    pst.setString(2, tablaOS.getValueAt(i, 7).toString());
                } else {
                    pst.setString(2, "");
                }
                pst.setInt(3, Login.idusuario);

                n = n + pst.executeUpdate();

            } catch (SQLException ex) {
                Logger.getLogger(RecepcionObrasSociales.class.getName()).log(Level.SEVERE, null, ex);
            }
            ///////////////////////////////
            if (tablaOS.getValueAt(i, 1).toString().equals("13800")) {
                if (Integer.valueOf(tablaOS.getValueAt(i, 6).toString()) == 1) {
                    Statement st3;
                    try {
                        st3 = cn.createStatement();

                        ResultSet rs3 = st3.executeQuery("SELECT id_recepcion, estado_recepcion FROM recepcion_bioquimico WHERE id_colegiados=" + idColegiado + " AND periodo=" + txtPeriodo.getText());

                        if (rs3.next()) {
                            if (rs3.getInt("estado_recepcion") == 1) {
                                String ActualizarRecepcion = "UPDATE recepcion_bioquimico SET Fecha = CURDATE(), id_colegiados=" + idColegiado + ", numero_entrega=1,"
                                        + " cantidad_ordenes=" + tablaOS.getValueAt(i, 5) + ", estado_recepcion=1, periodo=" + txtPeriodo.getText() + ", paquete=1 WHERE id_colegiados=" + idColegiado + " AND periodo = " + txtPeriodo.getText();
                                System.out.println("entra en actualizar");

                                Statement ST1 = cn.createStatement();
                                ST1.executeUpdate(ActualizarRecepcion);
                            } else {
                                JOptionPane.showMessageDialog(null, "No se pudo actualizar el registro de PAMI porque las ordenes ya fueron entregadas para auditoria");
                            }
                        } else {
                            String InsertarRecepcion = "INSERT INTO recepcion_bioquimico(Fecha, id_colegiados, numero_entrega,"
                                    + " cantidad_ordenes, estado_recepcion, periodo, paquete) VALUES (CURDATE()," + idColegiado + ","
                                    + "1" + ","
                                    + tablaOS.getValueAt(i, 5) + ",1," + txtPeriodo.getText() + ", " + "1" + ")";
                            System.out.println("entra en ingresar");
                            Statement ST1 = cn.createStatement();
                            ST1.executeUpdate(InsertarRecepcion);
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(RecepcionObrasSociales.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            //////////////////////////////
        }
        if (n > 0) {
            JOptionPane.showMessageDialog(null, "Se actualizó la recepción");

            try {
                Map parametros = new HashMap();
                parametros.put("id_colegiados", idColegiado);
                parametros.put("periodo_ddjj", periodo);
                parametros.put("fecha", fecha2);
                parametros.put("usuario", Login.nombreUsuario + " " + Login.apellidoUsuario);

                //  JOptionPane.showMessageDialog(null, "ddjj");
                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ObraSocial_controladas.jasper"));
                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, cn);
                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + periodo + "-" + lblColegiado.getText() + ".pdf");
                JasperPrintManager.printReport(jPrint, false);

            } catch (JRException ex) {
                System.err.println("Error iReport: " + ex.getMessage());
            }

            int opcion = JOptionPane.YES_NO_OPTION;
            int dialogButton = JOptionPane.showConfirmDialog(null, "¿Desea generar el resumen?", "", opcion);
            if (dialogButton == 0) {
                impresionDetalle();
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se actualizó nada");
        }

    }//GEN-LAST:event_btnProcesarActionPerformed

    private void btnActualizarMontosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarMontosActionPerformed

        if (tablaOS.getSelectedRow() > -1) {
            if (Integer.valueOf(tablaOS.getValueAt(tablaOS.getSelectedRow(), 6).toString()) == 2) {
                idObraSocial = ObraSocial.buscarOS(tablaOS.getValueAt(tablaOS.getSelectedRow(), 1).toString() + " - " + tablaOS.getValueAt(tablaOS.getSelectedRow(), 2).toString(), listaOS).getIdObraSocial();
                hilo2 = new Hiloobrasocial();
                hilo2.start();
                hilo2 = null;
                ConexionMariaDB cc = new ConexionMariaDB();
                Connection cn = cc.Conectar();
                String update = "UPDATE declaracion_jurada SET estado=1 WHERE cod_obra_social = " + Integer.valueOf(tablaOS.getValueAt(tablaOS.getSelectedRow(), 1).toString()) + " AND matricula_colegiado= " + matricula + " AND periodo_declaracion=" + periodo;

                PreparedStatement pst;
                try {
                    pst = cn.prepareStatement(update);
                    pst.executeUpdate();
                    listaDeclaracion.get(tablaOS.getSelectedRow()).setEstado(1);

                } catch (SQLException ex) {
                    Logger.getLogger(AltaEspecialidad.class.getName()).log(Level.SEVERE, null, ex);
                }
                cargartabla(matricula, periodo);
            } else {
                JOptionPane.showMessageDialog(null, "No se puede actualizar esta obra social");
            }
        }

    }//GEN-LAST:event_btnActualizarMontosActionPerformed

    private void btnBajaOsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBajaOsActionPerformed

        if (tablaOS.getSelectedRow() > -1) {
            int opcion = JOptionPane.showConfirmDialog(null, "¿Realmente desea dar de baja la obra social? Se anularán todas las órdenes", "Atencion", 0);
            if (opcion == 0) {
                new ObservacionBajaRecepcion(null, true).setVisible(true);
                ConexionMariaDB cc = new ConexionMariaDB();
                Connection cn = cc.Conectar();
                String update = "UPDATE declaracion_jurada SET estado=4, observacion='" + observacion + "' WHERE cod_obra_social = " + Integer.valueOf(tablaOS.getValueAt(tablaOS.getSelectedRow(), 1).toString()) + " AND matricula_colegiado= " + matricula + " AND periodo_declaracion=" + periodo;
                String bajaOrdenes = "UPDATE ordenes o INNER JOIN obrasocial os ON o.id_obrasocial = os.id_obrasocial SET o.estado_orden=0 WHERE ((os.codigo_obrasocial='" + tablaOS.getValueAt(tablaOS.getSelectedRow(), 1).toString() + "') and (o.periodo='" + periodo + "') and (o.id_colegiados='" + idColegiado + "'))";
                PreparedStatement pst;
                try {
                    pst = cn.prepareStatement(update);
                    int n = pst.executeUpdate();

                    if (n > 0) {

                        try {
                            pst = cn.prepareStatement(bajaOrdenes);
                            int m = pst.executeUpdate();

                            if (m > 0) {

                                cargarListaDeclaracionJurada(matricula, periodo);
                                cargartabla(matricula, periodo);
                            } else {
                                JOptionPane.showMessageDialog(null, "Error al actualizar el estado");
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(AltaEspecialidad.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "Error al actualizar el estado");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AltaEspecialidad.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un registro");
        }

    }//GEN-LAST:event_btnBajaOsActionPerformed

    private void btnImprimirRotulosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirRotulosActionPerformed

        try {
            descargarpdfvalidacion();
        } catch (PrinterException ex) {
            Logger.getLogger(RecepcionObrasSociales.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_btnImprimirRotulosActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();

    }//GEN-LAST:event_btnSalirActionPerformed

    private void tablaOSMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaOSMouseClicked

    }//GEN-LAST:event_tablaOSMouseClicked

    private void jPanel2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MousePressed

        x = evt.getX();
        y = evt.getY();

    }//GEN-LAST:event_jPanel2MousePressed

    private void jPanel2MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);

    }//GEN-LAST:event_jPanel2MouseDragged

    private void txtMatriculaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMatriculaActionPerformed

        btnMostrar.requestFocus();

    }//GEN-LAST:event_txtMatriculaActionPerformed

    private void txtPeriodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPeriodoActionPerformed

        txtMatricula.requestFocus();

    }//GEN-LAST:event_txtPeriodoActionPerformed

    private void txtLectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLectorActionPerformed

        leer();
        cargartabla(Integer.valueOf(txtMatricula.getText()), Integer.valueOf(txtPeriodo.getText()));
        txtLector.setText("");


    }//GEN-LAST:event_txtLectorActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonMaterialIconUno btnActualizarMontos;
    private RSMaterialComponent.RSButtonMaterialIconUno btnBajaOs;
    private RSMaterialComponent.RSButtonMaterialIconUno btnImprimirRotulos;
    private RSMaterialComponent.RSButtonMaterialIconUno btnMostrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnProcesar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnSalir;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel lblColegiado;
    private javax.swing.JPanel panel;
    private RSMaterialComponent.RSButtonIconOne rSButtonIconOne1;
    private RSMaterialComponent.RSTableMetroCustom tablaOS;
    private RSMaterialComponent.RSTextFieldOne txtLector;
    private RSMaterialComponent.RSTextFieldOne txtMatricula;
    private RSMaterialComponent.RSTextFieldOne txtPeriodo;
    // End of variables declaration//GEN-END:variables
}
