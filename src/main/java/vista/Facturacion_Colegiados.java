package vista;

import controlador.ConexionMariaDB;
import static controlador.HiloInicio.listaColegiados;
import controlador.camposordenes;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.print.PrinterException;
import java.sql.PreparedStatement;
import java.text.MessageFormat;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;
import java.awt.Cursor;
import static controlador.HiloInicio.listaOS;
import modelo.Colegiado;
import modelo.ObraSocial;

public class Facturacion_Colegiados extends javax.swing.JDialog {

    DefaultTableModel model, model2, model1;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    HiloOrdenes hilo;
    HiloOrdenestodas hilo2;
    Hiloobrasocial hilo3;
    Hiloobrasocialtodas hilo4;
    Hiloobrasocialintervalo hilo5;
    Hiloobrasocialtodasintervalo hilo6;
    public static int id_colegiado;
    public static String periododjj;
    public static String CodObra, mes = "", año = "";
    public static int IdMedico = 0, contadormedico = 0;
    public String validador;
    int[] idvalidador = new int[100];
    int[] idmedico = new int[1500];
    long total_mysql;
    String[] medico = new String[1500];

    public Facturacion_Colegiados(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        dobleclick();
        //cargarobrasocial();
        cargarvalidador();
        this.setLocationRelativeTo(null);
        if (Login.nbu_usuario == false) {
            cbovalidador.setVisible(false);
            btnimprimir1.setVisible(false);
        } else {
            cbovalidador.setVisible(true);
            btnimprimir1.setVisible(true);
        }
        TextAutoCompleter textAutoAcompleter = new TextAutoCompleter(txtobrasocial);
        textAutoAcompleter.addItems(listaOS.toArray());
        textAutoAcompleter.setMode(0);
        textAutoAcompleter.setCaseSensitive(false);
    }

    void cursor() {
        this.btnimprimir1.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.add(this.btnimprimir1);
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cargarvalidador() {
        int i = 0;
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String sql = "SELECT id_usuario, apellido_usuario FROM usuarios";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            cbovalidador.addItem("Todos");
            idvalidador[i] = 0;
            i++;
            while (rs.next()) {
                idvalidador[i] = rs.getInt("id_usuario");
                cbovalidador.addItem(rs.getString("apellido_usuario"));
                i++;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void dobleclick() {
        tablaordenes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {

                }
            }
        });
    }

    public class HiloOrdenes extends Thread {

        JProgressBar progreso;

        public HiloOrdenes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            int banderacolegiado = 0;
            int bandera_pami2 = 1;
            ////////////////////////////////////////////
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB cc = new ConexionMariaDB();
            Connection cn = cc.Conectar();
            try {
                if (bandera_pami2 == 0) {

                    /////////////////////////////////////////////////////
                    Statement st5 = cn.createStatement();
                    ResultSet rs5 = st5.executeQuery("SELECT id_colegiados,matricula_colegiado,direccion_laboratorio,localidad_laboratorio FROM colegiados");
                    while (rs5.next()) {
                        if (txtcolegiado1.getText().equals(rs5.getString("matricula_colegiado"))) {
                            if (txtcolegiado1.getText().equals(rs5.getString("matricula_colegiado"))) {
                                id_colegiado = rs5.getInt("id_colegiados");
                                banderacolegiado = 0;
                                break;
                            }
                        } else {
                            banderacolegiado = 1;
                        }
                    }
                    if (banderacolegiado == 0) {
                        String periodo2 = txtaño1.getText() + txtmes1.getText();
                        String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observación"};//estos seran los titulos de la tabla.            
                        String[] datos = new String[13];
                        model2 = new DefaultTableModel(null, titulos) {
                            ////Celdas no editables////////
                            public boolean isCellEditable(int row, int column) {
                                return false;
                            }
                        };
                        try {
                            Statement St = cn.createStatement();
                            ResultSet Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo, obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,ordenes.total,ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE colegiados.id_colegiados=" + id_colegiado + "  AND (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=58)");
                            String estado = "";
                            while (Rs.next()) {
                                if (Rs.getInt(11) == 0) {
                                    estado = "ANULADA";
                                }
                                if (Rs.getInt(11) == 1) {
                                    estado = "OK";
                                }
                                if (Rs.getInt(11) == 2) {
                                    estado = "OBSERVADA";
                                }
                                if (Rs.getInt(11) == 3) {
                                    estado = "AUDITORIA";
                                }
                                if (Rs.getInt(11) == 4) {
                                    estado = "DEBITADA";
                                }
                                datos[0] = Rs.getString(1);
                                datos[1] = Rs.getString(2);
                                datos[2] = Rs.getString(3);
                                datos[3] = Rs.getString(4);
                                datos[4] = Rs.getString(5);
                                datos[5] = Rs.getString(6);
                                datos[6] = Rs.getString(7);
                                datos[7] = Rs.getString(8);
                                datos[8] = Rs.getString(9);
                                datos[9] = Rs.getString(10);
                                datos[10] = estado;
                                datos[11] = Rs.getString(12);
                                if (Rs.getString(13) != null) {
                                    datos[12] = Rs.getString(13);
                                } else {
                                    datos[12] = "";
                                }
                                model2.addRow(datos);
                            }
                            tablaordenes.setModel(model2);
                            tablaordenes.setRowSorter(new TableRowSorter(model2));
                            tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                            /////////////////////////////////////////////////////////////////
                            tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                            ////////////////////////////////////////////////////////////////////
                            alinear();
                            tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(11).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(12).setCellRenderer(alinearCentro);
                            progreso.setValue(100);
                            cargatotales();
                            txtordenes.requestFocus();
                            btnbuscar.setEnabled(true);
                            txtordenes.setEnabled(true);
                            cargatotalesordenesfacturacion();
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, ex);

                        }
                    }
                } else {
                    /////////////////////////////////////////////////////
                    Statement st5 = cn.createStatement();
                    ResultSet rs5 = st5.executeQuery("SELECT id_colegiados,matricula_colegiado,direccion_laboratorio,localidad_laboratorio,validador FROM colegiados");
                    while (rs5.next()) {
                        if (txtcolegiado1.getText().equals(rs5.getString("matricula_colegiado"))) {

                            id_colegiado = rs5.getInt("id_colegiados");
                            banderacolegiado = 1;
                            break;

                        } else {
                            banderacolegiado = 0;
                        }
                    }
                    if (banderacolegiado == 1) {
                        String periodo2 = txtaño1.getText() + txtmes1.getText();
                        String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observación"};//estos seran los titulos de la tabla.            
                        String[] datos = new String[13];
                        model2 = new DefaultTableModel(null, titulos) {
                            ////Celdas no editables////////
                            public boolean isCellEditable(int row, int column) {
                                return false;
                            }
                        };
                        try {
                            Statement St = cn.createStatement();
                            ResultSet Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo, obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,ordenes.total,ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE (colegiados.id_colegiados=" + id_colegiado + "  AND ordenes.periodo=" + periodo2 + ")");
                            String estado = "";
                            while (Rs.next()) {
                                if (Rs.getInt(11) == 0) {
                                    estado = "ANULADA";
                                }
                                if (Rs.getInt(11) == 1) {
                                    estado = "OK";
                                }
                                if (Rs.getInt(11) == 2) {
                                    estado = "OBSERVADA";
                                }
                                if (Rs.getInt(11) == 3) {
                                    estado = "AUDITORIA";
                                }
                                if (Rs.getInt(11) == 4) {
                                    estado = "DEBITADA";
                                }
                                datos[0] = Rs.getString(1);
                                datos[1] = Rs.getString(2);
                                datos[2] = Rs.getString(3);
                                datos[3] = Rs.getString(4);
                                datos[4] = Rs.getString(5);
                                datos[5] = Rs.getString(6);
                                datos[6] = Rs.getString(7);
                                datos[7] = Rs.getString(8);
                                datos[8] = Rs.getString(9);
                                datos[9] = Rs.getString(10);
                                datos[10] = estado;
                                datos[11] = Rs.getString(12);
                                if (Rs.getString(13) != null) {
                                    datos[12] = Rs.getString(13);
                                } else {
                                    datos[12] = "";
                                }
                                model2.addRow(datos);
                            }
                            tablaordenes.setModel(model2);

                            tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                            /////////////////////////////////////////////////////////////////
                            tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                            alinear();
                            tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(11).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(12).setCellRenderer(alinearCentro);
                            // progreso.setValue(100);
                            cargatotales();
                            txtordenes.requestFocus();
                            btnbuscar.setEnabled(true);
                            txtordenes.setEnabled(true);
                            cargatotalesordenesfacturacion();
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, ex);

                        }

                    }
                }
                btnbuscar.setEnabled(true);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            }
            int cantidad = tablaordenes.getRowCount(), contador = 0;
            progreso.setMaximum(cantidad);
            progreso.setValue(cantidad);
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public class HiloOrdenestodas extends Thread {

        JProgressBar progreso;

        public HiloOrdenestodas(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            int banderacolegiado = 1;
            int bandera_pami2 = 1;
            ////////////////////////////////////////////
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB cc = new ConexionMariaDB();
            Connection cn = cc.Conectar();
            try {
                if (bandera_pami2 == 0) {

                    if (banderacolegiado == 0) {
                        String periodo2 = txtaño1.getText() + txtmes1.getText();
                        String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observación"};//estos seran los titulos de la tabla.            
                        String[] datos = new String[13];
                        model2 = new DefaultTableModel(null, titulos) {
                            ////Celdas no editables////////
                            public boolean isCellEditable(int row, int column) {
                                return false;
                            }
                        };
                        try {
                            Statement St = cn.createStatement();
                            ResultSet Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo, obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,ordenes.total,ordenes.estado_orden, usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=58)");
                            String estado = "";
                            while (Rs.next()) {
                                if (Rs.getInt(11) == 0) {
                                    estado = "ANULADA";
                                }
                                if (Rs.getInt(11) == 1) {
                                    estado = "OK";
                                }
                                if (Rs.getInt(11) == 2) {
                                    estado = "OBSERVADA";
                                }
                                if (Rs.getInt(11) == 3) {
                                    estado = "AUDITORIA";
                                }
                                if (Rs.getInt(11) == 4) {
                                    estado = "DEBITADA";
                                }
                                datos[0] = Rs.getString(1);
                                datos[1] = Rs.getString(2);
                                datos[2] = Rs.getString(3);
                                datos[3] = Rs.getString(4);
                                datos[4] = Rs.getString(5);
                                datos[5] = Rs.getString(6);
                                datos[6] = Rs.getString(7);
                                datos[7] = Rs.getString(8);
                                datos[8] = Rs.getString(9);
                                datos[9] = Rs.getString(10);
                                datos[10] = estado;
                                datos[11] = Rs.getString(12);
                                if (Rs.getString(13) != null) {
                                    datos[12] = Rs.getString(13);
                                } else {
                                    datos[12] = "";
                                }
                                model2.addRow(datos);
                            }
                            tablaordenes.setModel(model2);
                            tablaordenes.setRowSorter(new TableRowSorter(model2));

                            tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);

                            tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);

                            alinear();
                            tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                            progreso.setValue(100);
                            cargatotales();
                            txtordenes.requestFocus();
                            btnbuscar.setEnabled(true);
                            txtordenes.setEnabled(true);
                            cargatotalesordenesfacturacion();
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, ex);

                        }
                    }

                } else {
                    /////////////////////////////////////////////////////
                    Statement st5 = cn.createStatement();
                    ResultSet rs5 = st5.executeQuery("SELECT id_colegiados,matricula_colegiado,direccion_laboratorio,localidad_laboratorio,validador FROM colegiados");
                    while (rs5.next()) {

                    }
                    if (banderacolegiado == 1) {
                        String periodo2 = txtaño1.getText() + txtmes1.getText();
                        String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observación"};//estos seran los titulos de la tabla.            
                        String[] datos = new String[13];
                        model2 = new DefaultTableModel(null, titulos) {
                            ////Celdas no editables////////
                            public boolean isCellEditable(int row, int column) {
                                return false;
                            }
                        };
                        try {
                            Statement St = cn.createStatement();
                            ResultSet Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo, obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,ordenes.total,ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion  FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE ordenes.periodo=" + periodo2);
                            String estado = "";
                            while (Rs.next()) {
                                if (Rs.getInt(11) == 0) {
                                    estado = "ANULADA";
                                }
                                if (Rs.getInt(11) == 1) {
                                    estado = "OK";
                                }
                                if (Rs.getInt(11) == 2) {
                                    estado = "OBSERVADA";
                                }
                                if (Rs.getInt(11) == 3) {
                                    estado = "AUDITORIA";
                                }
                                if (Rs.getInt(11) == 4) {
                                    estado = "DEBITADA";
                                }
                                datos[0] = Rs.getString(1);
                                datos[1] = Rs.getString(2);
                                datos[2] = Rs.getString(3);
                                datos[3] = Rs.getString(4);
                                datos[4] = Rs.getString(5);
                                datos[5] = Rs.getString(6);
                                datos[6] = Rs.getString(7);
                                datos[7] = Rs.getString(8);
                                datos[8] = Rs.getString(9);
                                datos[9] = Rs.getString(10);
                                datos[10] = estado;
                                datos[11] = Rs.getString(12);
                                if (Rs.getString(13) != null) {
                                    datos[12] = Rs.getString(13);
                                } else {
                                    datos[12] = "";
                                }
                                model2.addRow(datos);
                            }
                            tablaordenes.setModel(model2);

                            tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);

                            tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);

                            alinear();
                            tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                            // progreso.setValue(100);
                            cargatotales();
                            txtordenes.requestFocus();
                            btnbuscar.setEnabled(true);
                            txtordenes.setEnabled(true);
                            cargatotalesordenesfacturacion();
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, ex);

                        }

                    }

                }
                btnbuscar.setEnabled(true);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            }
            int cantidad = tablaordenes.getRowCount(), contador = 0;
            progreso.setMaximum(cantidad);
            progreso.setValue(cantidad);
        }

        public void pausa(int mlSeg) {
            try {

                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    void cargatotales() {
        double total = 0.00, sumatoria = 0.00;

        int totalRow = tablaordenes.getRowCount();
        totalRow -= 1;
        if (totalRow != -1) {
            for (int i = 0; i <= (totalRow); i++) {
                if (tablaordenes.getValueAt(i, 10).toString().equals("OK") || tablaordenes.getValueAt(i, 10).toString().equals("OBSERVADA") || tablaordenes.getValueAt(i, 10).toString().equals("PENDIENTE")) {
                    String x = tablaordenes.getValueAt(i, 9).toString();
                    sumatoria = Double.valueOf(x);
                    total = Redondear(total + sumatoria);

                }
            }
        }
        txttotal.setText((String.valueOf((total))));

    }

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        Modificar = new javax.swing.JMenuItem();
        jPopupMenu2 = new javax.swing.JPopupMenu();
        estado = new javax.swing.JMenu();
        ok = new javax.swing.JMenuItem();
        observar = new javax.swing.JMenuItem();
        anular = new javax.swing.JMenuItem();
        Debitar = new javax.swing.JMenuItem();
        Detalle = new javax.swing.JMenuItem();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaordenes = new javax.swing.JTable();
        txtordenes = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txtvalidador = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txttotalordenes = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txttotalanuladas = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txttotalobservadas = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txttotal = new javax.swing.JTextField();
        cboestado = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtdesde = new javax.swing.JTextField();
        txthasta = new javax.swing.JTextField();
        txtTotalesPendientes = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnbuscar = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        txtmes1 = new javax.swing.JFormattedTextField();
        txtaño1 = new javax.swing.JFormattedTextField();
        txtcolegiado1 = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtmes2 = new javax.swing.JFormattedTextField();
        txtaño2 = new javax.swing.JFormattedTextField();
        jLabel1 = new javax.swing.JLabel();
        txtobrasocial = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        txtcolegiado2 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cbovalidador = new javax.swing.JComboBox();
        btnsalir1 = new javax.swing.JButton();
        progreso = new javax.swing.JProgressBar();
        btnsalir2 = new javax.swing.JButton();
        btnimprimir = new javax.swing.JButton();
        btnimprimir1 = new javax.swing.JButton();

        Modificar.setText("Modificar Orden");
        Modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Modificar);

        estado.setText("Cambiar Estado");

        ok.setText("OK");
        ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okActionPerformed(evt);
            }
        });
        estado.add(ok);

        observar.setText("OBSERVAR");
        observar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                observarActionPerformed(evt);
            }
        });
        estado.add(observar);

        anular.setText("ANULAR");
        anular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                anularActionPerformed(evt);
            }
        });
        estado.add(anular);

        Debitar.setText("DEBITAR");
        Debitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DebitarActionPerformed(evt);
            }
        });
        estado.add(Debitar);

        jPopupMenu2.add(estado);

        Detalle.setText("Ver Detalle");
        Detalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DetalleActionPerformed(evt);
            }
        });
        jPopupMenu2.add(Detalle);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ordenes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaordenes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tablaordenes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaordenes.setToolTipText("");
        tablaordenes.setComponentPopupMenu(jPopupMenu2);
        tablaordenes.setInheritsPopupMenu(true);
        tablaordenes.setOpaque(false);
        tablaordenes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaordenesMouseClicked(evt);
            }
        });
        tablaordenes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaordenesKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tablaordenes);

        txtordenes.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtordenes.setForeground(new java.awt.Color(0, 102, 204));
        txtordenes.setToolTipText("Buscar por dni paciente o num paciente o nom paciente o num orden");
        txtordenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtordenesActionPerformed(evt);
            }
        });
        txtordenes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtordenesKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtordenesKeyReleased(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setText("Validador:");

        txtvalidador.setEditable(false);
        txtvalidador.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txtvalidador.setForeground(new java.awt.Color(0, 102, 204));
        txtvalidador.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtvalidador.setBorder(null);
        txtvalidador.setOpaque(false);
        txtvalidador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtvalidadorActionPerformed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel23.setText("Total Ordenes OK:");

        txttotalordenes.setEditable(false);
        txttotalordenes.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotalordenes.setForeground(new java.awt.Color(0, 102, 204));
        txttotalordenes.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotalordenes.setBorder(null);
        txttotalordenes.setOpaque(false);
        txttotalordenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalordenesActionPerformed(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel24.setText("Total Anuladas:");

        txttotalanuladas.setEditable(false);
        txttotalanuladas.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotalanuladas.setForeground(new java.awt.Color(0, 102, 204));
        txttotalanuladas.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotalanuladas.setBorder(null);
        txttotalanuladas.setOpaque(false);
        txttotalanuladas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalanuladasActionPerformed(evt);
            }
        });

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setText("Total Observadas:");

        txttotalobservadas.setEditable(false);
        txttotalobservadas.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotalobservadas.setForeground(new java.awt.Color(0, 102, 204));
        txttotalobservadas.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotalobservadas.setBorder(null);
        txttotalobservadas.setOpaque(false);
        txttotalobservadas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalobservadasActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Total: $");

        txttotal.setEditable(false);
        txttotal.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotal.setForeground(new java.awt.Color(0, 102, 204));
        txttotal.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotal.setBorder(null);
        txttotal.setOpaque(false);
        txttotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalActionPerformed(evt);
            }
        });

        cboestado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboestado.setForeground(new java.awt.Color(0, 102, 204));
        cboestado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Filtrar por Estado", "OK", "OBSERVADA", "ANULADA" }));
        cboestado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboestadoActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Filtrar Ordenes  Desde:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Hasta:");

        txtdesde.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdesde.setForeground(new java.awt.Color(0, 102, 204));
        txtdesde.setNextFocusableComponent(txthasta);
        txtdesde.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdesdeKeyPressed(evt);
            }
        });

        txthasta.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txthasta.setForeground(new java.awt.Color(0, 102, 204));
        txthasta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txthastaActionPerformed(evt);
            }
        });
        txthasta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txthastaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txthastaKeyReleased(evt);
            }
        });

        txtTotalesPendientes.setEditable(false);
        txtTotalesPendientes.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txtTotalesPendientes.setForeground(new java.awt.Color(0, 102, 204));
        txtTotalesPendientes.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtTotalesPendientes.setBorder(null);
        txtTotalesPendientes.setOpaque(false);
        txtTotalesPendientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalesPendientesActionPerformed(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setText("Total Pendientes:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotalordenes, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotalanuladas, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotalobservadas, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalesPendientes, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(140, 140, 140)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotal, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtvalidador, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(txtordenes, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cboestado, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtdesde, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txthasta, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtordenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboestado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(txtdesde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txthasta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTotalesPendientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel26))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txttotalanuladas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel24)
                            .addComponent(txttotalobservadas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25)
                            .addComponent(jLabel15)
                            .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel23)))
                    .addComponent(txttotalordenes, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtvalidador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtrar por Periodo", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Mes:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(51, 51, 51));
        jLabel7.setText("Año:");

        btnbuscar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnbuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728952 - magnifier zoom.png"))); // NOI18N
        btnbuscar.setMnemonic('b');
        btnbuscar.setText("Buscar");
        btnbuscar.setToolTipText("[Alt + b]");
        btnbuscar.setNextFocusableComponent(txtcolegiado2);
        btnbuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnbuscarMouseClicked(evt);
            }
        });
        btnbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbuscarActionPerformed(evt);
            }
        });
        btnbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnbuscarKeyPressed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(51, 51, 51));
        jLabel17.setText("Matricula:");

        txtmes1.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtmes1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtmes1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmes1.setNextFocusableComponent(txtaño1);
        txtmes1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmes1ActionPerformed(evt);
            }
        });
        txtmes1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmes1KeyPressed(evt);
            }
        });

        txtaño1.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtaño1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtaño1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtaño1.setNextFocusableComponent(btnbuscar);
        txtaño1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtaño1ActionPerformed(evt);
            }
        });
        txtaño1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtaño1KeyPressed(evt);
            }
        });

        txtcolegiado1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcolegiado1.setForeground(new java.awt.Color(0, 102, 204));
        txtcolegiado1.setNextFocusableComponent(txtmes1);
        txtcolegiado1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcolegiado1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtcolegiado1, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                            .addComponent(txtmes1)
                            .addComponent(txtaño1))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 103, Short.MAX_VALUE)
                        .addComponent(btnbuscar)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnbuscar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(txtcolegiado1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtmes1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtaño1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtrar por Obra social", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Año:");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(51, 51, 51));
        jLabel18.setText("Matricula:");

        txtmes2.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtmes2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtmes2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmes2.setNextFocusableComponent(txtaño2);
        txtmes2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmes2ActionPerformed(evt);
            }
        });
        txtmes2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmes2KeyPressed(evt);
            }
        });

        txtaño2.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtaño2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtaño2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtaño2.setNextFocusableComponent(txtobrasocial);
        txtaño2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtaño2ActionPerformed(evt);
            }
        });
        txtaño2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtaño2KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Obra Social:");

        txtobrasocial.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtobrasocial.setForeground(new java.awt.Color(0, 102, 204));
        txtobrasocial.setNextFocusableComponent(cbovalidador);
        txtobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialActionPerformed(evt);
            }
        });
        txtobrasocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtobrasocialKeyPressed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728952 - magnifier zoom.png"))); // NOI18N
        jButton1.setMnemonic('u');
        jButton1.setText("Buscar");
        jButton1.setNextFocusableComponent(tablaordenes);
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        txtcolegiado2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcolegiado2.setForeground(new java.awt.Color(0, 102, 204));
        txtcolegiado2.setNextFocusableComponent(txtmes2);
        txtcolegiado2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcolegiado2KeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Mes:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Validador:");

        cbovalidador.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbovalidador.setForeground(new java.awt.Color(0, 102, 204));
        cbovalidador.setNextFocusableComponent(jButton1);
        cbovalidador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbovalidadorActionPerformed(evt);
            }
        });
        cbovalidador.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbovalidadorKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(txtcolegiado2, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtmes2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtaño2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtobrasocial)
                    .addComponent(cbovalidador, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(jLabel8)
                            .addComponent(txtmes2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtaño2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtcolegiado2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtobrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(cbovalidador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnsalir1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir1.setMnemonic('v');
        btnsalir1.setText("Volver");
        btnsalir1.setToolTipText("[Alt + v]");
        btnsalir1.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir1ActionPerformed(evt);
            }
        });

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        btnsalir2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnsalir2.setMnemonic('b');
        btnsalir2.setText("Borrar");
        btnsalir2.setToolTipText("[Alt + b]");
        btnsalir2.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir2ActionPerformed(evt);
            }
        });

        btnimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        btnimprimir.setMnemonic('i');
        btnimprimir.setText("Imprimir Tabla");
        btnimprimir.setToolTipText("[Alt + i]");
        btnimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirActionPerformed(evt);
            }
        });

        btnimprimir1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728900 - application browser.png"))); // NOI18N
        btnimprimir1.setMnemonic('r');
        btnimprimir1.setText("Imprimir Reporte de Validadores");
        btnimprimir1.setToolTipText("[Alt + r]");
        btnimprimir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimir1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnimprimir1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnsalir1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsalir1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnimprimir1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaordenesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaordenesKeyPressed
        DefaultTableModel temp = (DefaultTableModel) tablaordenes.getModel();
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            tablaordenes.transferFocus();
            evt.consume();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (tablaordenes.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
            } else {

                ConexionMariaDB cc = new ConexionMariaDB();
                Connection cn = cc.Conectar();
                String id_orden = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString();
                try {
                    String sSQL3 = "update ordenes set numero_afiliado=?, numero_orden=?, fecha_orden=?, coseguro=? where id_orden=" + id_orden;
                    PreparedStatement pst = cn.prepareStatement(sSQL3);
                    pst.setString(1, tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 3).toString());
                    pst.setString(2, tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 6).toString());
                    pst.setString(3, tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 7).toString());
                    pst.setString(4, tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 10).toString());
                    int n4 = pst.executeUpdate();
                    if (n4 > 0) {
                        JOptionPane.showMessageDialog(null, "Orden Modificada...");
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
            }
        }
    }//GEN-LAST:event_tablaordenesKeyPressed

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void filtrarordenes(String valor) {
        //estos seran los titulos de la tabla.
        String[] Titulo = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observacion"};
        String[] Registros = new String[13];
        String sql = "SELECT id_orden,periodo,id_obrasocial,numero_afiliado,nombre_afiliado,matricula_prescripcion,numero_orden,fecha_orden,colegiados.matricula_colegiado,total,estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE CONCAT(numero_orden, ' ',numero_afiliado ,' ',dni_afiliado,' ',nombre_afiliado,' ',fecha_orden)"
                + "LIKE '%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////"
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();

        try {
            /////////////////////////////////////////////////////
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                while (rs.next()) {
                    if (!txtobrasocial.getText().equals("")) {

                        if ((rs.getString("periodo").equals(txtaño1.getText() + txtmes1.getText()) || rs.getString("periodo").equals(txtaño2.getText() + txtmes2.getText())) && rs.getInt("id_obrasocial") == ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial()) {

                            Registros[0] = rs.getString("id_orden");
                            Registros[1] = rs.getString("periodo");
                            ///////////////////////////////////
                            Statement st2 = cn.createStatement();
                            String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial FROM obrasocial WHERE id_obrasocial=" + rs.getString("id_obrasocial");
                            ResultSet rs2 = st2.executeQuery(sql2);
                            while (rs2.next()) {
                                if (rs.getString("id_obrasocial").equals(rs2.getString("id_obrasocial"))) {
                                    Registros[2] = rs2.getString("razonsocial_obrasocial");
                                }
                            }
                            /////////////////////////////////////////////////////
                            Registros[3] = rs.getString("numero_afiliado");
                            Registros[4] = rs.getString("nombre_afiliado");
                            Registros[5] = rs.getString("matricula_prescripcion");
                            Registros[6] = rs.getString("numero_orden");
                            Registros[7] = rs.getString("fecha_orden");
                            Registros[8] = rs.getString("colegiados.matricula_colegiado");
                            Registros[9] = rs.getString("total");
                            String estado = "";
                            if (rs.getInt(11) == 0) {
                                estado = "ANULADA";
                            }
                            if (rs.getInt(11) == 1) {
                                estado = "OK";
                            }
                            if (rs.getInt(11) == 2) {
                                estado = "OBSERVADA";
                            }
                            Registros[10] = estado;
                            Registros[11] = rs.getString(12);
                            if (rs.getString(13) != null) {
                                Registros[12] = rs.getString(13);
                            } else {
                                Registros[12] = "";
                            }
                            model.addRow(Registros);
                            tablaordenes.setModel(model);
                            tablaordenes.setRowSorter(new TableRowSorter(model));
                            /////////////////////////////////////////////////////////////////
                            tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                            /////////////////////////////////////////////////////////////////
                            tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                            tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                            /////////////////////////////////////////////////////////////
                            alinear();
                            tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                            tablaordenes.getColumnModel().getColumn(11).setCellRenderer(alinearCentro);
                        }
                    } else {

                    }

                }
            } else {
                JOptionPane.showMessageDialog(null, "No se encontraron resultados...");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void cargatotalesordenesfacturacion() {
        int totalRow = tablaordenes.getRowCount(), contador = 0, contador2 = 0, contador3 = 0, contador4 = 0;
        totalRow -= 1;
        if (totalRow != -1) {
            for (int i = 0; i <= (totalRow); i++) {
                if (tablaordenes.getValueAt(i, 10).toString().equals("OK")) {
                    contador++;
                }
                if (tablaordenes.getValueAt(i, 10).toString().equals("ANULADA")) {
                    contador2++;
                }
                if (tablaordenes.getValueAt(i, 10).toString().equals("OBSERVADA")) {
                    contador3++;
                }
                if (tablaordenes.getValueAt(i, 10).toString().equals("PENDIENTE")) {
                    contador4++;
                }
            }
        }
        txttotalordenes.setText((String.valueOf(contador)));
        txttotalanuladas.setText((String.valueOf(contador2)));
        txttotalobservadas.setText((String.valueOf(contador3)));
        txtTotalesPendientes.setText((String.valueOf(contador4)));
    }

    private void txtordenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtordenesActionPerformed


    }//GEN-LAST:event_txtordenesActionPerformed

    private void txtordenesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtordenesKeyPressed

    }//GEN-LAST:event_txtordenesKeyPressed

    private void txtordenesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtordenesKeyReleased

        TableRowSorter sorter = new TableRowSorter(model2);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtordenes.getText() + ".*"));
        tablaordenes.setRowSorter(sorter);
        cargatotales();
        cargatotalesordenesfacturacion();

    }//GEN-LAST:event_txtordenesKeyReleased

    void borrartabla() {
        DefaultTableModel temp = (DefaultTableModel) tablaordenes.getModel();
        int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
        for (int i = a; i >= 0; i--) {
            temp.removeRow(i);
        }
    }

    private void btnbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbuscarActionPerformed

        if (Login.nbu_usuario == false) {
            cbovalidador.setVisible(false);
        } else {
            cbovalidador.setVisible(true);
        }
        if (!txtcolegiado1.getText().equals("") || !txtmes1.getText().equals("") || !txtaño1.getText().equals("")) {

            borrartabla();
            String periodo2 = txtaño1.getText() + txtmes1.getText();
            periododjj = periodo2;
            //if (Integer.valueOf(periodo2) <= Integer.valueOf(periodo)) {
            String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observación"};//estos seran los titulos de la tabla.
            Object[][] datos = {}; //en esta matriz bidimencional cargaremos los datos
            //  model2 = new DefaultTableModel(datos, titulos);
            model2 = new DefaultTableModel(datos, titulos) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            btnbuscar.setEnabled(false);
            tablaordenes.setModel(model2);
            iniciarSplash();
            hilo = new HiloOrdenes(progreso);
            hilo.start();
            hilo = null;
        }
        if (txtcolegiado1.getText().equals("") && !txtmes1.getText().equals("") && !txtaño1.getText().equals("")) {

            borrartabla();
            String periodo2 = txtaño1.getText() + txtmes1.getText();
            periododjj = periodo2;
            //if (Integer.valueOf(periodo2) <= Integer.valueOf(periodo)) {
            String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observación"};//estos seran los titulos de la tabla.
            Object[][] datos = {}; //en esta matriz bidimencional cargaremos los datos
            //  model2 = new DefaultTableModel(datos, titulos);
            model2 = new DefaultTableModel(datos, titulos) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            btnbuscar.setEnabled(false);
            tablaordenes.setModel(model2);
            iniciarSplash();
            hilo2 = new HiloOrdenestodas(progreso);
            hilo2.start();
            hilo2 = null;
        }
    }//GEN-LAST:event_btnbuscarActionPerformed

    private void progresoStateChanged(javax.swing.event.ChangeEvent evt) {
        if (progreso.getValue() == 100) {
            this.dispose();
        }
    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }


    private void btnbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnbuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
        }
    }//GEN-LAST:event_btnbuscarKeyPressed

    boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    private void txtmes1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmes1ActionPerformed

    }//GEN-LAST:event_txtmes1ActionPerformed

    private void txtmes1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmes1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isNumeric(txtmes1.getText())) {
                txtaño1.setEnabled(true);
                txtmes1.transferFocus();
            }
        }

    }//GEN-LAST:event_txtmes1KeyPressed

    private void txtaño1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtaño1ActionPerformed

    }//GEN-LAST:event_txtaño1ActionPerformed

    private void txtaño1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtaño1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isNumeric(txtaño1.getText())) {
                txtaño1.transferFocus();
            }
        }

    }//GEN-LAST:event_txtaño1KeyPressed

    public class Hiloobrasocialtodas extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialtodas(JProgressBar progreso) {
            super();
            this.progreso = progreso;
        }

        public void run() {
            progreso.setValue(0);
            int banderacolegiado = 0;
            ConexionMariaDB cc = new ConexionMariaDB();
            Connection cn = cc.Conectar();
            ////////////////////carga tabla///////////////////////////////////////////////
            try {
                /////////////////////////////////////////////////////
                Statement st5 = cn.createStatement();
                ResultSet rs5 = st5.executeQuery("SELECT id_colegiados,matricula_colegiado,direccion_laboratorio,localidad_laboratorio FROM colegiados");
                while (rs5.next()) {
                    if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                        if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                            id_colegiado = rs5.getInt("id_colegiados");
                            banderacolegiado = 0;
                            break;
                        }
                    } else {
                        banderacolegiado = 1;
                    }
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            ////SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,ordenes.total,ordenes.estado_orden FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE (colegiados.id_colegiados=61  AND (ordenes.periodo=201502 AND obrasocial.id_obrasocial=11))
            DecimalFormat df = new DecimalFormat("0.00");
            String periodo2 = txtaño2.getText() + txtmes2.getText();
            periododjj = periodo2;
            String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observacion", " Nº "};//estos seran los titulos de la tabla.            
            String[] datos = new String[14];

            if (Login.nbu_usuario == false) {
                model2 = new DefaultTableModel(null, titulos) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };
            } else {
                model2 = new DefaultTableModel(null, titulos) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return true;
                    }
                };
            }
            try {
                Statement St = cn.createStatement();
                ResultSet Rs;
                if (cbovalidador.getSelectedItem().toString().equals("Todos")) {
                    Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + ")");
                } else {
                    Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + " AND usuarios.apellido_usuario='" + cbovalidador.getSelectedItem().toString() + "')");
                }
                String estado = "";
                int i = 1;
                while (Rs.next()) {
                    if (Rs.getInt(11) == 0) {
                        estado = "ANULADA";
                    }
                    if (Rs.getInt(11) == 1) {
                        estado = "OK";
                    }
                    if (Rs.getInt(11) == 2) {
                        estado = "OBSERVADA";
                    }
                    if (Rs.getInt(11) == 3) {
                        estado = "AUDITORIA";
                    }
                    if (Rs.getInt(11) == 4) {
                        estado = "DEBITADA";
                    }
                    datos[0] = Rs.getString(1);
                    datos[1] = Rs.getString(2);
                    datos[2] = Rs.getString(3);
                    datos[3] = Rs.getString(4);
                    datos[4] = Rs.getString(5);
                    datos[5] = Rs.getString(6);
                    datos[6] = Rs.getString(7);
                    datos[7] = Rs.getString(8);
                    datos[8] = Rs.getString(9);
                    datos[9] = Rs.getString(10);
                    datos[10] = estado;
                    datos[11] = Rs.getString(12);
                    if (Rs.getString(13) != null) {
                        datos[12] = Rs.getString(13);
                    } else {
                        datos[12] = "";
                    }
                    datos[13] = completarceros(String.valueOf(i), 5);
                    i++;
                    model2.addRow(datos);
                }
                tablaordenes.setModel(model2);
                tablaordenes.setRowSorter(new TableRowSorter(model2));
                /////////////////////////////////////////////////////////////
                ///   tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
                //  tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
                //  tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                ///////////////////////////////////////////////////////////////////
                alinear();
                tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(11).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(12).setCellRenderer(alinearCentro);

                //  progreso.setValue(100);
                cargatotales();
                cargatotalesordenesfacturacion();
                txtordenes.requestFocus();
                btnbuscar.setEnabled(true);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);

            }
            /////////////////////////////imprime reporte////////////////////////////////
            int n, i = 0, pacientes = 0, practicas = 0, band = 0;
            LinkedList<camposordenes> Resultados = new LinkedList<camposordenes>();
            Resultados.clear();
            String orden = "";
            int cantidad = tablaordenes.getRowCount(), contador = 0;
            progreso.setMaximum(cantidad);
            double pesos = 0, centavos = 0;
            double totalpesospracticas = 0, total = Double.valueOf(txttotal.getText());
            progreso.setValue(cantidad);

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public class Hiloobrasocialtodasintervalo extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialtodasintervalo(JProgressBar progreso) {
            super();
            this.progreso = progreso;
        }

        public void run() {
            progreso.setValue(0);
            int banderacolegiado = 0;
            ConexionMariaDB cc = new ConexionMariaDB();
            Connection cn = cc.Conectar();
            ////////////////////carga tabla///////////////////////////////////////////////
            try {
                /////////////////////////////////////////////////////
                Statement st5 = cn.createStatement();
                ResultSet rs5 = st5.executeQuery("SELECT id_colegiados,matricula_colegiado,direccion_laboratorio,localidad_laboratorio FROM colegiados");
                while (rs5.next()) {
                    if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                        if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                            id_colegiado = rs5.getInt("id_colegiados");
                            banderacolegiado = 0;
                            break;
                        }
                    } else {
                        banderacolegiado = 1;
                    }
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            ////SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,ordenes.total,ordenes.estado_orden FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE (colegiados.id_colegiados=61  AND (ordenes.periodo=201502 AND obrasocial.id_obrasocial=11))
            DecimalFormat df = new DecimalFormat("0.00");
            String periodo2 = txtaño2.getText() + txtmes2.getText();
            String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observacion"};//estos seran los titulos de la tabla.            
            String[] datos = new String[13];
            model2 = new DefaultTableModel(null, titulos) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };

            try {
                Statement St = cn.createStatement();
                ResultSet Rs;
                if (cbovalidador.getSelectedItem().toString().equals("Todos")) {
                    Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + ") AND ordenes.numero_orden BETWEEN '" + txtdesde.getText() + "' AND '" + txthasta.getText() + "'");
                } else {
                    Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + " AND usuarios.apellido_usuario='" + cbovalidador.getSelectedItem().toString() + "') AND ordenes.numero_orden BETWEEN '" + txtdesde.getText() + "' AND '" + txthasta.getText() + "'");
                }
                String estado = "";
                if (Rs.next()) {
                    while (Rs.next()) {
                        if (Rs.getInt(11) == 0) {
                            estado = "ANULADA";
                        }
                        if (Rs.getInt(11) == 1) {
                            estado = "OK";
                        }
                        if (Rs.getInt(11) == 2) {
                            estado = "OBSERVADA";
                        }
                        datos[0] = Rs.getString(1);
                        datos[1] = Rs.getString(2);
                        datos[2] = Rs.getString(3);
                        datos[3] = Rs.getString(4);
                        datos[4] = Rs.getString(5);
                        datos[5] = Rs.getString(6);
                        datos[6] = Rs.getString(7);
                        datos[7] = Rs.getString(8);
                        datos[8] = Rs.getString(9);
                        datos[9] = Rs.getString(10);
                        datos[10] = estado;
                        datos[11] = Rs.getString(12);
                        if (Rs.getString(13) != null) {
                            datos[12] = Rs.getString(13);
                        } else {
                            datos[12] = "";
                        }
                        model2.addRow(datos);
                    }
                    tablaordenes.setModel(model2);
                    tablaordenes.setRowSorter(new TableRowSorter(model2));
                    /////////////////////////////////////////////////////////////
                    ///   tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
                    //  tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
                    //  tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
                    /////////////////////////////////////////////////////////////////
                    tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                    tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                    tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                    /////////////////////////////////////////////////////////////////
                    /////////////////////////////////////////////////////////////////
                    tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                    tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                    tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                    ///////////////////////////////////////////////////////////////////
                    alinear();
                    tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(11).setCellRenderer(alinearCentro);
                    cargatotales();
                    cargatotalesordenesfacturacion();
                } else {
                    JOptionPane.showMessageDialog(null, "No se encontró ningun resultado...");
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);

            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public class Hiloobrasocialintervalo extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialintervalo(JProgressBar progreso) {
            super();
            this.progreso = progreso;
        }

        public void run() {
            progreso.setValue(0);
            int banderacolegiado = 0;
            ConexionMariaDB cc = new ConexionMariaDB();
            Connection cn = cc.Conectar();
            ////////////////////carga tabla///////////////////////////////////////////////
            try {
                /////////////////////////////////////////////////////
                Statement st5 = cn.createStatement();
                ResultSet rs5 = st5.executeQuery("SELECT id_colegiados,matricula_colegiado,direccion_laboratorio,localidad_laboratorio FROM colegiados");

                while (rs5.next()) {
                    if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                        if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                            id_colegiado = rs5.getInt("id_colegiados");

                            banderacolegiado = 0;
                            break;
                        }
                    } else {
                        banderacolegiado = 1;
                    }
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            ////SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,ordenes.total,ordenes.estado_orden FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE (colegiados.id_colegiados=61  AND (ordenes.periodo=201502 AND obrasocial.id_obrasocial=11))
            DecimalFormat df = new DecimalFormat("0.00");
            String periodo2 = txtaño2.getText() + txtmes2.getText();
            String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observacion"};//estos seran los titulos de la tabla.            
            String[] datos = new String[13];
            model2 = new DefaultTableModel(null, titulos) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };

            try {
                Statement St = cn.createStatement();
                ResultSet Rs;
                if (cbovalidador.getSelectedItem().toString().equals("Todos")) {
                    Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE colegiados.id_colegiados=" + id_colegiado + "  AND (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + ") AND ordenes.numero_orden BETWEEN '" + txtdesde.getText() + "' AND '" + txthasta.getText() + "'");
                } else {
                    Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE colegiados.id_colegiados=" + id_colegiado + "  AND (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + " AND usuarios.apellido_usuario='" + cbovalidador.getSelectedItem().toString() + "') AND ordenes.numero_orden BETWEEN '" + txtdesde.getText() + "' AND '" + txthasta.getText() + "'");
                }
                String estado = "";
                if (Rs.next()) {
                    while (Rs.next()) {

                        if (Rs.getInt(11) == 0) {
                            estado = "ANULADA";
                        }
                        if (Rs.getInt(11) == 1) {
                            estado = "OK";
                        }
                        if (Rs.getInt(11) == 2) {
                            estado = "OBSERVADA";
                        }
                        datos[0] = Rs.getString(1);
                        datos[1] = Rs.getString(2);
                        datos[2] = Rs.getString(3);
                        datos[3] = Rs.getString(4);
                        datos[4] = Rs.getString(5);
                        datos[5] = Rs.getString(6);
                        datos[6] = Rs.getString(7);
                        datos[7] = Rs.getString(8);
                        datos[8] = Rs.getString(9);
                        datos[9] = Rs.getString(10);
                        datos[10] = estado;
                        datos[11] = Rs.getString(12);
                        if (Rs.getString(13) != null) {
                            datos[12] = Rs.getString(13);
                        } else {
                            datos[12] = "";
                        }
                        model2.addRow(datos);
                    }

                    tablaordenes.setModel(model2);
                    tablaordenes.setRowSorter(new TableRowSorter(model2));
                    /////////////////////////////////////////////////////////////
                    //tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
                    //tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
                    //tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
                    /////////////////////////////////////////////////////////////////
                    tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                    tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                    tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                    /////////////////////////////////////////////////////////////////
                    tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                    tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                    tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                    ///////////////////////////////////////////////////////////////////
                    /* tablaordenes.getColumnModel().getColumn(8).setMaxWidth(0);
                     tablaordenes.getColumnModel().getColumn(8).setMinWidth(0);
                     tablaordenes.getColumnModel().getColumn(8).setPreferredWidth(0);*/
                    alinear();
                    tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(11).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(12).setCellRenderer(alinearCentro);
                    cargatotales();
                    cargatotalesordenesfacturacion();
                } else {
                    JOptionPane.showMessageDialog(null, "No se encontró ningun resultado...");
                }

                txtordenes.requestFocus();
                btnbuscar.setEnabled(true);

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);

            }
            /////////////////////////////imprime reporte////////////////////////////////
            int n, i = 0, pacientes = 0, practicas = 0, band = 0;
            LinkedList<camposordenes> Resultados = new LinkedList<camposordenes>();
            Resultados.clear();
            String orden = "";
            int cantidad = tablaordenes.getRowCount(), contador = 0;
            progreso.setMaximum(cantidad);
            double pesos = 0, centavos = 0;
            double totalpesospracticas = 0, total = Double.valueOf(txttotal.getText());
            progreso.setValue(cantidad);

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public class Hiloobrasocial extends Thread {

        JProgressBar progreso;

        public Hiloobrasocial(JProgressBar progreso) {
            super();
            this.progreso = progreso;
        }

        public void run() {
            progreso.setValue(0);
            int banderacolegiado = 0;
            ConexionMariaDB cc = new ConexionMariaDB();
            Connection cn = cc.Conectar();
            ////////////////////carga tabla///////////////////////////////////////////////
            try {
                /////////////////////////////////////////////////////
                Statement st5 = cn.createStatement();
                ResultSet rs5 = st5.executeQuery("SELECT id_colegiados,matricula_colegiado,direccion_laboratorio,localidad_laboratorio FROM colegiados");
                while (rs5.next()) {
                    if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                        if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                            id_colegiado = rs5.getInt("id_colegiados");

                            banderacolegiado = 0;
                            break;
                        }
                    } else {
                        banderacolegiado = 1;
                    }
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            ////SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,ordenes.total,ordenes.estado_orden FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE (colegiados.id_colegiados=61  AND (ordenes.periodo=201502 AND obrasocial.id_obrasocial=11))
            DecimalFormat df = new DecimalFormat("0.00");
            String periodo2 = txtaño2.getText() + txtmes2.getText();
            String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Coseguro", "Estado", "Validador", "Observacion", " Nº "};//estos seran los titulos de la tabla.            
            String[] datos = new String[15];
            if (Login.nbu_usuario == false) {
                model2 = new DefaultTableModel(null, titulos) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };
            } else {
                model2 = new DefaultTableModel(null, titulos) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return true;
                    }
                };
            }

            try {
                Statement St = cn.createStatement();
                ResultSet Rs;
                if (cbovalidador.getSelectedItem().toString().equals("Todos")) {
                    Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),coseguro,ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE colegiados.id_colegiados='" + Colegiado.buscarColegiado(txtcolegiado2.getText(), listaColegiados).getIdColegiados() + "'  AND (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial='" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + "')");
                } else {
                    Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),coseguro,ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE colegiados.id_colegiados='" + Colegiado.buscarColegiado(txtcolegiado2.getText(), listaColegiados).getIdColegiados() + "'  AND (ordenes.periodo='" + periodo2 + "' AND obrasocial.id_obrasocial='" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + "' AND usuarios.apellido_usuario='" + cbovalidador.getSelectedItem().toString() + "')");
                }
                String estado = "";
                int i = 1;
                while (Rs.next()) {

                    if (Rs.getInt(12) == 0) {
                        estado = "ANULADA";
                    }
                    if (Rs.getInt(12) == 1) {
                        estado = "OK";
                    }
                    if (Rs.getInt(12) == 2) {
                        estado = "OBSERVADA";
                    }
                    if (Rs.getInt(12) == 3) {
                        estado = "AUDITORIA";
                    }
                    if (Rs.getInt(12) == 4) {
                        estado = "DEBITADA";
                    }
                    datos[0] = Rs.getString(1);
                    datos[1] = Rs.getString(2);
                    datos[2] = Rs.getString(3);
                    datos[3] = Rs.getString(4);
                    datos[4] = Rs.getString(5);
                    datos[5] = Rs.getString(6);
                    datos[6] = Rs.getString(7);
                    datos[7] = Rs.getString(8);
                    datos[8] = Rs.getString(9);
                    datos[9] = Rs.getString("round(ordenes.total,2)");
                    datos[10] = Rs.getString("Coseguro");
                    datos[11] = estado;
                    datos[12] = Rs.getString("usuarios.apellido_usuario");
                    if (Rs.getString("ordenes.observacion") != null) {
                        datos[13] = Rs.getString("ordenes.observacion");
                    } else {
                        datos[13] = "";
                    }
                    datos[14] = completarceros(String.valueOf(i), 5);
                    i++;
                    model2.addRow(datos);
                }

                tablaordenes.setModel(model2);
                tablaordenes.setRowSorter(new TableRowSorter(model2));
                /////////////////////////////////////////////////////////////
                //tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
                //tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
                //tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                ///////////////////////////////////////////////////////////////////
                /* tablaordenes.getColumnModel().getColumn(8).setMaxWidth(0);
                 tablaordenes.getColumnModel().getColumn(8).setMinWidth(0);
                 tablaordenes.getColumnModel().getColumn(8).setPreferredWidth(0);*/
                alinear();
                tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(11).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(12).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(13).setCellRenderer(alinearCentro);
                //  progreso.setValue(100);
                cargatotales();
                cargatotalesordenesfacturacion();
                txtordenes.requestFocus();
                btnbuscar.setEnabled(true);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);

            }
            /////////////////////////////imprime reporte////////////////////////////////
            int n, i = 0, pacientes = 0, practicas = 0, band = 0;
            LinkedList<camposordenes> Resultados = new LinkedList<camposordenes>();
            Resultados.clear();
            String orden = "";
            int cantidad = tablaordenes.getRowCount(), contador = 0;
            progreso.setMaximum(cantidad);
            double pesos = 0, centavos = 0;
            double totalpesospracticas = 0, total = Double.valueOf(txttotal.getText());
            progreso.setValue(cantidad);

            periododjj = txtaño2.getText() + txtmes2.getText();

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    String completarceros(String v, int d) {
        String ceros = "";
        if (v.length() < d) {
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        return v;
    }

    void borrartablaordenes() {
        /* DefaultTableModel temp = (DefaultTableModel) tablaordenes.getModel();
         int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
         for (int i = a; i >= 0; i--) {
         temp.removeRow(i);
         }*/
    }


    private void txtmes2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmes2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmes2ActionPerformed

    private void txtmes2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmes2KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isNumeric(txtmes2.getText())) {
                txtaño2.setEnabled(true);
                txtmes2.transferFocus();
            }
        }
    }//GEN-LAST:event_txtmes2KeyPressed

    private void txtaño2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtaño2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtaño2ActionPerformed

    private void txtaño2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtaño2KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isNumeric(txtaño2.getText())) {
                txtaño2.transferFocus();
            }
        }
    }//GEN-LAST:event_txtaño2KeyPressed

    void cargarobrasocial() {/*
        TextAutoCompleter textAutoAcompleter = new TextAutoCompleter(txtobrasocial);
        int i = 0;

        // Recorro y cargo las obras sociales
        while (i < contadorobrasocial) {
            textAutoAcompleter.addItem(obrasocial[i]);
            i++;
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter.setMode(0); // infijo     
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter.setCaseSensitive(false); //No sensible a mayúsculas        
         */
    }

    private void txtobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialActionPerformed
        /* String cod = "", nom = "", cadena = txtobrasocial.getText();
        int i = 0, k = 0, j = 1, id_obra_social = 0;
        ///cod/// 0000 - aaaaaaaaa
        while (k < cadena.length()) {
            if (String.valueOf(cadena.charAt(k)).equals("-")) {
                j = k + 2;
                k = cadena.length();
            } else {
                cod = cod + cadena.charAt(k);
            }
            k++;
        }
        ///nom/// 0000 - aaaaaaaaa
        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }
        /*while (i < contadorobrasocial) {
            if (cadena.equals(obrasocial[i])) {
                id_obra_social = idobrasocial[i];
                break;
            }
            i++;
        }
        ObraSocial = nom;
        CodObra = cod;
        idobraimprime = id_obra_social;*/
        txtobrasocial.transferFocus();
    }//GEN-LAST:event_txtobrasocialActionPerformed

    private void btnsalir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalir1ActionPerformed

    private void btnsalir2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir2ActionPerformed
        if (Login.nbu_usuario == false) {
            cbovalidador.setVisible(false);
        } else {
            cbovalidador.setVisible(true);
        }
        borrartabla();
        progreso.setValue(0);
        txtmes1.setText("");
        txtmes2.setText("");
        txtaño1.setText("");
        txtaño2.setText("");
        txtcolegiado1.setText("");
        txtcolegiado2.setText("");
        txtobrasocial.setText("");
        txtordenes.setText("");
        txttotal.setText("0");
        txttotalordenes.setText("0");
        txtcolegiado1.requestFocus();

    }//GEN-LAST:event_btnsalir2ActionPerformed

    private void txtcolegiado1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcolegiado1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            txtcolegiado1.transferFocus();

        }
    }//GEN-LAST:event_txtcolegiado1KeyPressed

    private void txtcolegiado2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcolegiado2KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            txtcolegiado2.transferFocus();

        }
    }//GEN-LAST:event_txtcolegiado2KeyPressed

    private void btnimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirActionPerformed
        try {
            //Mensaje de encabezado
            MessageFormat encabezado = new MessageFormat("Periodo:" + txtmes1.getText() + txtaño1.getText() + txtmes2.getText() + txtaño2.getText());
            //Mensaje en el pie de pagina
            MessageFormat pie = new MessageFormat("Ordenes OK: " + txttotalordenes.getText() + "    Ordenes Anuladas: " + txttotalanuladas.getText() + "                   Total: " + txttotal.getText());
            //Imprimir JTable
            tablaordenes.print(JTable.PrintMode.FIT_WIDTH, encabezado, pie);
        } catch (PrinterException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }//GEN-LAST:event_btnimprimirActionPerformed

    private void txtvalidadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtvalidadorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtvalidadorActionPerformed

    private void txtobrasocialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtobrasocial.transferFocus();
        }
    }//GEN-LAST:event_txtobrasocialKeyPressed

    private void cbovalidadorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbovalidadorKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cbovalidador.transferFocus();
        }
    }//GEN-LAST:event_cbovalidadorKeyPressed

    private void txttotalordenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalordenesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalordenesActionPerformed

    private void txttotalanuladasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalanuladasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalanuladasActionPerformed

    private void txttotalobservadasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalobservadasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalobservadasActionPerformed

    private void txttotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalActionPerformed

    private void cbovalidadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbovalidadorActionPerformed
        int band = 0;
        String nombre = cbovalidador.getSelectedItem().toString();
        int i = 0;
        /* while (i < contadorobrasocial) {
//            if (nombre.equals(validador[i])) {
            ///        break;
            ///   }
            i++;
        }*/
    }//GEN-LAST:event_cbovalidadorActionPerformed

    private void ModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificarActionPerformed

        new Modificar_Orden(null, true).setVisible(true);
    }//GEN-LAST:event_ModificarActionPerformed

    private void cboestadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboestadoActionPerformed
        if (!cboestado.getSelectedItem().toString().equals("Filtrar por Estado")) {
            TableRowSorter sorter = new TableRowSorter(model2);
            sorter.setRowFilter(RowFilter.regexFilter(".*" + cboestado.getSelectedItem().toString() + ".*"));
            tablaordenes.setRowSorter(sorter);
            cargatotales();
            cargatotalesordenesfacturacion();
        } else {
            TableRowSorter sorter = new TableRowSorter(model2);
            sorter.setRowFilter(RowFilter.regexFilter(".*.*"));
            tablaordenes.setRowSorter(sorter);
            cargatotales();
            cargatotalesordenesfacturacion();
        }
    }//GEN-LAST:event_cboestadoActionPerformed

    private void txtdesdeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdesdeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtdesde.transferFocus();
        }
    }//GEN-LAST:event_txtdesdeKeyPressed

    private void txthastaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txthastaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtcolegiado2.getText().equals("") && !txtobrasocial.getText().equals("") && !txtmes2.getText().equals("") && !txtaño2.getText().equals("")) {
                if (!txtobrasocial.getText().equals("")) {

                    /////////////////////////////////////////////////////////////////////////
                    String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observacion"};//estos seran los titulos de la tabla.
                    Object[][] datos = {}; //en esta matriz bidimensional cargaremos los datos
                    //  model2 = new DefaultTableModel(datos, titulos);
                    model1 = new DefaultTableModel(datos, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    //  tablaordenes.setModel(model1);
                    borrartablaordenes();

                    iniciarSplash();
                    hilo5 = new Hiloobrasocialintervalo(progreso);
                    hilo5.start();
                    hilo5 = null;
                    txtvalidador.setText(cbovalidador.getSelectedItem().toString());

                    ////////////////////////////////////////////////////////////////////////
                    borrartablaordenes();
                    txttotal.setText("0.0");
                    /// txtobrasocial1.setText("");
                    txtmes1.requestFocus();
                }
            }

            if (txtcolegiado2.getText().equals("") && !txtobrasocial.getText().equals("") && !txtmes2.getText().equals("") && !txtaño2.getText().equals("")) {

                /////////////////////////////////////////////////////////////////////////
                String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observacion"};//estos seran los titulos de la tabla.
                Object[][] datos = {}; //en esta matriz bidimensional cargaremos los datos
                //  model2 = new DefaultTableModel(datos, titulos);
                model1 = new DefaultTableModel(datos, titulos) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };
                //  tablaordenes.setModel(model1);
                borrartablaordenes();

                iniciarSplash();
                hilo6 = new Hiloobrasocialtodasintervalo(progreso);
                hilo6.start();
                hilo6 = null;
                txtvalidador.setText(cbovalidador.getSelectedItem().toString());
                ////////////////////////////////////////////////////////////////////////

                borrartablaordenes();
                txttotal.setText("0.0");
                /// txtobrasocial1.setText("");
                txtmes1.requestFocus();
            }

        }
    }//GEN-LAST:event_txthastaKeyPressed

    private void txthastaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txthastaActionPerformed

    }//GEN-LAST:event_txthastaActionPerformed

    private void okActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okActionPerformed
        if (!tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 10).toString().equals("OK")) {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            int id_orden = Integer.valueOf(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString());
            try {
                String sSQL2 = "UPDATE ordenes SET estado_orden=? WHERE id_orden=" + id_orden;
                PreparedStatement pst = cn.prepareStatement(sSQL2);
                pst.setInt(1, 1);
                int n = pst.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "La orden fué modificada con exito...");
                    tablaordenes.setValueAt("OK", tablaordenes.getSelectedRow(), 10);
                    cargatotales();
                    cargatotalesordenesfacturacion();
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else {
            JOptionPane.showMessageDialog(null, "La orden ya está OK...");
        }
    }//GEN-LAST:event_okActionPerformed

    private void observarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_observarActionPerformed
        if (!tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 10).toString().equals("OBSERVADA")) {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            int id_orden = Integer.valueOf(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString());
            try {
                String sSQL2 = "UPDATE ordenes SET estado_orden=?, observacion='Falta Firma y/o Sello Médico' WHERE id_orden=" + id_orden;
                PreparedStatement pst = cn.prepareStatement(sSQL2);
                pst.setInt(1, 2);
                int n = pst.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "La orden fué modificada con exito...");
                    tablaordenes.setValueAt("OBSERVADA", tablaordenes.getSelectedRow(), 10);
                    cargatotales();
                    cargatotalesordenesfacturacion();
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else {
            JOptionPane.showMessageDialog(null, "La orden ya está OBSERVADA...");
        }
    }//GEN-LAST:event_observarActionPerformed

    private void anularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_anularActionPerformed
        if (!tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 10).toString().equals("ANULADA")) {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            int id_orden = Integer.valueOf(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString());
            try {
                String sSQL2 = "UPDATE ordenes SET estado_orden=? WHERE id_orden=" + id_orden;
                PreparedStatement pst = cn.prepareStatement(sSQL2);
                pst.setInt(1, 0);
                int n = pst.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "La orden fué modificada con exito...");
                    tablaordenes.setValueAt("ANULADA", tablaordenes.getSelectedRow(), 10);
                    cargatotales();
                    cargatotalesordenesfacturacion();
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else {
            JOptionPane.showMessageDialog(null, "La orden ya está ANULADA...");
        }
    }//GEN-LAST:event_anularActionPerformed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed

        if (Login.nbu_usuario == false) {
            cbovalidador.setVisible(false);
        } else {
            cbovalidador.setVisible(true);
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtcolegiado2.getText().equals("") && !txtobrasocial.getText().equals("") && !txtmes2.getText().equals("") && !txtaño2.getText().equals("")) {
                if (!txtobrasocial.getText().equals("")) {
                    /////////////////////////////////////////////////////////////////////////
                    String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Coseguro", "Estado", "Validador", "Observacion"};//estos seran los titulos de la tabla.
                    Object[][] datos = {}; //en esta matriz bidimensional cargaremos los datos
                    model1 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    tablaordenes.setModel(model1);
                    borrartablaordenes();
                    iniciarSplash();
                    hilo3 = new Hiloobrasocial(progreso);
                    hilo3.start();
                    hilo3 = null;
                    txtvalidador.setText(cbovalidador.getSelectedItem().toString());
                    ////////////////////////////////////////////////////////////////////////
                    borrartablaordenes();
                    txttotal.setText("0.0");
                    /// txtobrasocial1.setText("");
                    txtmes1.requestFocus();
                }
            }
            if (txtcolegiado2.getText().equals("") && !txtobrasocial.getText().equals("") && !txtmes2.getText().equals("") && !txtaño2.getText().equals("")) {
                ///JOptionPane.showMessageDialog(null, "2");
                /////////////////////////////////////////////////////////////////////////
                String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado", "Validador", "Observacion"};//estos seran los titulos de la tabla.
                Object[][] datos = {}; //en esta matriz bidimensional cargaremos los datos
                model1 = new DefaultTableModel(null, titulos) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };
                tablaordenes.setModel(model1);
                borrartablaordenes();
                iniciarSplash();
                hilo4 = new Hiloobrasocialtodas(progreso);
                hilo4.start();
                hilo4 = null;
                txtvalidador.setText(cbovalidador.getSelectedItem().toString());
                ////////////////////////////////////////////////////////////////////////
                borrartablaordenes();
                txttotal.setText("0.0");
                /// txtobrasocial1.setText("");
                txtmes1.requestFocus();
            }
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void txthastaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txthastaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txthastaKeyReleased

    void filtrarmedico(String valor) {
        int banderacolegiado = 0;
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        ////////////////////carga tabla///////////////////////////////////////////////

        if (!txtcolegiado2.getText().equals("") || !txtobrasocial.getText().equals("") || !txtmes2.getText().equals("") || !txtaño2.getText().equals("")) {
            if (!txtobrasocial.getText().equals("")) {
                JOptionPane.showMessageDialog(null, IdMedico);
                /*try {
                    /////////////////////////////////////////////////////
                    Statement st5 = cn.createStatement();
                    ResultSet rs5 = st5.executeQuery("SELECT id_colegiados,matricula_colegiado,direccion_laboratorio,localidad_laboratorio FROM colegiados");
                    while (rs5.next()) {
                        if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                            if (txtcolegiado2.getText().equals(rs5.getString("matricula_colegiado"))) {
                                id_colegiado = rs5.getInt("id_colegiados");
                                banderacolegiado = 0;
                                break;
                            }
                        } else {
                            banderacolegiado = 1;
                        }
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }*/
                ////SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,ordenes.total,ordenes.estado_orden FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE (colegiados.id_colegiados=61  AND (ordenes.periodo=201502 AND obrasocial.id_obrasocial=11))
                DecimalFormat df = new DecimalFormat("0.00");
                String periodo2 = txtaño2.getText() + txtmes2.getText();
                periododjj = periodo2;
                String[] titulos = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Coseguro", "Estado", "Validador", "Observacion"};//estos seran los titulos de la tabla.            
                String[] datos = new String[14];
                model2 = new DefaultTableModel(null, titulos) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };

                try {
                    Statement St = cn.createStatement();
                    ResultSet Rs;
                    if (cbovalidador.getSelectedItem().toString().equals("Todos")) {
//                        Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE colegiados.id_colegiados=" + Colegiado.buscarColegiado(txtcolegiado2.getText(), listaColegiados) + "  AND (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + ")");
                        Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),coseguro, ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE colegiados.id_colegiados=" + Colegiado.buscarColegiado(txtcolegiado2.getText(), listaColegiados) + "  AND (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + ")");
                    } else {
                        Rs = St.executeQuery("SELECT ordenes.id_orden, ordenes.periodo,obrasocial.razonsocial_obrasocial,ordenes.numero_afiliado, ordenes.nombre_afiliado,ordenes.matricula_prescripcion,ordenes.numero_orden,ordenes.fecha_orden,colegiados.matricula_colegiado,round(ordenes.total,2),coseguro, ordenes.estado_orden,usuarios.apellido_usuario,ordenes.observacion FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial  LEFT JOIN usuarios ON usuarios.id_usuario=ordenes.id_validador WHERE colegiados.id_colegiados=" + Colegiado.buscarColegiado(txtcolegiado2.getText(), listaColegiados) + "  AND (ordenes.periodo=" + periodo2 + " AND obrasocial.id_obrasocial=" + ObraSocial.buscarOS(txtobrasocial.getText(), listaOS).getIdObraSocial() + " AND usuarios.apellido_usuario='" + cbovalidador.getSelectedItem().toString() + "')");
                    }
                    String estado = "";
                    while (Rs.next()) {

                        if (Rs.getInt(11) == 0) {
                            estado = "ANULADA";
                        }
                        if (Rs.getInt(11) == 1) {
                            estado = "OK";
                        }
                        if (Rs.getInt(11) == 2) {
                            estado = "OBSERVADA";
                        }
                        datos[0] = Rs.getString(1);
                        datos[1] = Rs.getString(2);
                        datos[2] = Rs.getString(3);
                        datos[3] = Rs.getString(4);
                        datos[4] = Rs.getString(5);
                        datos[5] = Rs.getString(6);
                        datos[6] = Rs.getString(7);
                        datos[7] = Rs.getString(8);
                        datos[8] = Rs.getString(9);
                        datos[9] = Rs.getString(10);
                        datos[10] = Rs.getString(11);
                        datos[11] = estado;
                        datos[12] = Rs.getString(13);
                        if (Rs.getString(14) != null) {
                            datos[13] = Rs.getString(14);
                        } else {
                            datos[13] = "";
                        }
                        model2.addRow(datos);
                    }

                    tablaordenes.setModel(model2);
                    tablaordenes.setRowSorter(new TableRowSorter(model2));
                    /////////////////////////////////////////////////////////////////
                    tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                    tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                    tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                    /////////////////////////////////////////////////////////////////
                    tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                    tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                    tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                    alinear();
                    tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(11).setCellRenderer(alinearCentro);
                    tablaordenes.getColumnModel().getColumn(12).setCellRenderer(alinearCentro);
                    cargatotales();
                    cargatotalesordenesfacturacion();
                    txtordenes.requestFocus();
                    btnbuscar.setEnabled(true);
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex);

                }
            }
        }
        if (txtcolegiado2.getText().equals("") && !txtobrasocial.getText().equals("") && !txtmes2.getText().equals("") && !txtaño2.getText().equals("")) {

        }

        /////////////////////////////imprime reporte////////////////////////////////
        int n, i = 0, pacientes = 0, practicas = 0, band = 0;
        LinkedList<camposordenes> Resultados = new LinkedList<camposordenes>();
        Resultados.clear();
        String orden = "";
        int cantidad = tablaordenes.getRowCount(), contador = 0;
        double pesos = 0, centavos = 0;
        double totalpesospracticas = 0, total = Double.valueOf(txttotal.getText());

    }

    private void btnimprimir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimir1ActionPerformed
        cursor();
        btnbuscar.setEnabled(false);
        btnimprimir.setEnabled(false);
        btnimprimir1.setEnabled(false);
        btnsalir1.setEnabled(false);
        btnsalir2.setEnabled(false);
        jButton1.setEnabled(false);
        new Periodo_pami(null, true).setVisible(true);
        if (!periododjj.equals("")) {
            new Resumen_Validadores(null, true).setVisible(true);
        }
        btnbuscar.setEnabled(true);
        btnimprimir.setEnabled(true);
        btnimprimir1.setEnabled(true);
        btnsalir1.setEnabled(true);
        btnsalir2.setEnabled(true);
        jButton1.setEnabled(true);
        this.dispose();
    }//GEN-LAST:event_btnimprimir1ActionPerformed

    private void DetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DetalleActionPerformed
        if (!txtaño1.getText().equals("") && !txtmes1.getText().equals("")) {
            periododjj = txtaño1.getText() + txtmes1.getText();
        } else {
            periododjj = txtaño2.getText() + txtmes2.getText();
        }
        Detalle_Practicas.num_orden = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 6).toString();
        Detalle_Practicas.id_orden = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString();
        Detalle_Practicas.afiliado = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 4).toString();
        Detalle_Practicas.obrasocial = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 2).toString();
        Detalle_Practicas.fecha = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 7).toString();
        Detalle_Practicas.observacion = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 13).toString();
        Detalle_Practicas.total = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 9).toString();
        new Detalle_Practicas(null, true).setVisible(true);
    }//GEN-LAST:event_DetalleActionPerformed

    private void txtTotalesPendientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalesPendientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalesPendientesActionPerformed

    private void DebitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DebitarActionPerformed
        if (!tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 10).toString().equals("ANULADA")) {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            int id_orden = Integer.valueOf(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString());
            new Observaciones(null, true).setVisible(true);
            try {
                String sSQL2 = "UPDATE ordenes SET estado_orden=?,observacion=? WHERE id_orden=" + id_orden;
                PreparedStatement pst = cn.prepareStatement(sSQL2);
                pst.setInt(1, 4);
                pst.setString(2, Observaciones.observacion);
                int n = pst.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "La orden fué modificada con exito...");
                    tablaordenes.setValueAt("DEBITADA", tablaordenes.getSelectedRow(), 10);
                    cargatotales();
                    cargatotalesordenesfacturacion();
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else {
            JOptionPane.showMessageDialog(null, "La orden ya está ANULADA...");
        }
    }//GEN-LAST:event_DebitarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1MouseClicked

    private void btnbuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbuscarMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnbuscarMouseClicked

    private void tablaordenesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaordenesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaordenesMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Debitar;
    private javax.swing.JMenuItem Detalle;
    private javax.swing.JMenuItem Modificar;
    private javax.swing.JMenuItem anular;
    private javax.swing.JButton btnbuscar;
    private javax.swing.JButton btnimprimir;
    private javax.swing.JButton btnimprimir1;
    private javax.swing.JButton btnsalir1;
    private javax.swing.JButton btnsalir2;
    private javax.swing.JComboBox cboestado;
    private javax.swing.JComboBox cbovalidador;
    private javax.swing.JMenu estado;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenu2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenuItem observar;
    private javax.swing.JMenuItem ok;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JTable tablaordenes;
    private javax.swing.JTextField txtTotalesPendientes;
    private javax.swing.JFormattedTextField txtaño1;
    private javax.swing.JFormattedTextField txtaño2;
    private javax.swing.JTextField txtcolegiado1;
    private javax.swing.JTextField txtcolegiado2;
    private javax.swing.JTextField txtdesde;
    private javax.swing.JTextField txthasta;
    private javax.swing.JFormattedTextField txtmes1;
    private javax.swing.JFormattedTextField txtmes2;
    private javax.swing.JTextField txtobrasocial;
    private javax.swing.JTextField txtordenes;
    private javax.swing.JTextField txttotal;
    private javax.swing.JTextField txttotalanuladas;
    private javax.swing.JTextField txttotalobservadas;
    private javax.swing.JTextField txttotalordenes;
    private javax.swing.JTextField txtvalidador;
    // End of variables declaration//GEN-END:variables
}
