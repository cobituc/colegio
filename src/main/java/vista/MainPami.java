package vista;

import controlador.ColoreaTabla;
import controlador.ConexionMySQLPami;
import controlador.Sobres;
import controlador.reporte_entrega;
import controlador.camposorden_3;
import static vista.EncabezadoyPie.encabezado2;
import static vista.EncabezadoyPie.pie2;
import static vista.Entregas.id_medico;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ImageIcon;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JREmptyDataSource;

public class MainPami extends javax.swing.JFrame {

    DefaultTableModel model;
    public static int idmedico, capita, idcontrol, x4, x2, dx4, dx2, h4, h2, i, j;
    public static String nombremed, direccionmed, especialidad;
    String numero_orden, fecha, id_especialidad, nombre, localidad_filtro, especialidad1, codigo_postal, desdex4, hastax4, desdex2, hastax2, fecha_vencimiento, entrega;
    public static String mes = "ENERO", año = "2015", direccion, localidad;
    int resto, resto1, bandera = 0, cantidad;
    TextAutoCompleter textAutoAcompleter;

    public MainPami() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icocbt.png")).getImage());
        this.setLocationRelativeTo(null);
        textAutoAcompleter = new TextAutoCompleter(txtdireccion);
        setExtendedState(MAXIMIZED_BOTH);
        cargartabla("");
        eventotabla();
        cargarcombolocalidad();
        cargarcombozona();
        cargarcombodireccion();
        dobleclick();
        cargardatosentregaclic();
        cargarperiodo();
        tabla.setRowHeight(25);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(3);
        tabla.getColumnModel().getColumn(1).setPreferredWidth(150);
        tabla.getColumnModel().getColumn(2).setPreferredWidth(150);
        tabla.getColumnModel().getColumn(3).setPreferredWidth(5);
        tabla.getColumnModel().getColumn(4).setPreferredWidth(50);
        tabla.getColumnModel().getColumn(5).setPreferredWidth(100);
        tabla.getColumnModel().getColumn(6).setPreferredWidth(10);
        tabla.getColumnModel().getColumn(8).setPreferredWidth(10);
        tabla.getColumnModel().getColumn(9).setPreferredWidth(100);
        tabla.getColumnModel().getColumn(10).setPreferredWidth(100);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        cbomes = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cboaño = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        cboentrega = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        cbomedico = new javax.swing.JComboBox();
        txtdireccion = new javax.swing.JTextField();
        cbolocalidad = new javax.swing.JComboBox();
        cbozona = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnaltamed = new javax.swing.JButton();
        btnaltaesp = new javax.swing.JButton();
        btnaltamed2 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnmodmed = new javax.swing.JButton();
        btnmodesp = new javax.swing.JButton();
        btnaltamed1 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtcantidad = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtdesdex4 = new javax.swing.JTextField();
        txthastax4 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtobs = new javax.swing.JTextArea();
        btnmodmed1 = new javax.swing.JButton();
        btnimprimetabla = new javax.swing.JButton();
        btnmodmed3 = new javax.swing.JButton();
        btnmodmed4 = new javax.swing.JButton();
        btnmodmed5 = new javax.swing.JButton();
        btnmodmed6 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtperiodo = new javax.swing.JTextField();
        txtcantidad2 = new javax.swing.JTextField();
        txttipo = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtdesde2 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txthasta2 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtentrega = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtrecibido = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        btntabla = new javax.swing.JButton();
        btnsobres = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Mes:");

        cbomes.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbomes.setForeground(new java.awt.Color(0, 102, 204));
        cbomes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" }));
        cbomes.setNextFocusableComponent(txtcantidad);
        cbomes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbomesActionPerformed(evt);
            }
        });

        tabla.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tabla);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728952 - magnifier zoom.png"))); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Año:");

        cboaño.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboaño.setForeground(new java.awt.Color(0, 102, 204));
        cboaño.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024" }));
        cboaño.setNextFocusableComponent(txtcantidad);
        cboaño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboañoActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Entrega:");

        cboentrega.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboentrega.setForeground(new java.awt.Color(0, 102, 204));
        cboentrega.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "PRIMERA ENTREGA", "SEGUNDA ENTREGA", "TERCERA ENTREGA", "CUARTA ENTREGA", "QUINTA ENTREGA", "SEXTA ENTREGA", "SÉPTIMA ENTREGA" }));
        cboentrega.setNextFocusableComponent(txtcantidad);
        cboentrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboentregaActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("Medico:");

        cbomedico.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbomedico.setForeground(new java.awt.Color(0, 102, 204));
        cbomedico.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "...", "GENERALISTA", "ESPECIALISTA" }));
        cbomedico.setNextFocusableComponent(txtcantidad);
        cbomedico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbomedicoActionPerformed(evt);
            }
        });

        txtdireccion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtdireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdireccionActionPerformed(evt);
            }
        });

        cbolocalidad.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbolocalidad.setForeground(new java.awt.Color(0, 102, 204));
        cbolocalidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbolocalidadActionPerformed(evt);
            }
        });

        cbozona.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbozona.setForeground(new java.awt.Color(0, 102, 204));
        cbozona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbozonaActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("Filtrar por Zona:");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText("Filtrar por Localidad:");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setText("Filtrar por Dirección:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbozona, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cboaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbomes, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cboentrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addComponent(jLabel12)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbomedico, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel19)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbolocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel12)
                        .addComponent(cbomedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbolocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel18))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(cboaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11)
                        .addComponent(cboentrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbomes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbozona, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel19)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Altas"));

        btnaltamed.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaltamed.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728943 - glasses reading.png"))); // NOI18N
        btnaltamed.setText("Médico");
        btnaltamed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaltamedActionPerformed(evt);
            }
        });

        btnaltaesp.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaltaesp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728972 - gold medal.png"))); // NOI18N
        btnaltaesp.setText("Especialidad");
        btnaltaesp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaltaespActionPerformed(evt);
            }
        });

        btnaltamed2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaltamed2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728931 - browser earth world.png"))); // NOI18N
        btnaltamed2.setText("Zonas");
        btnaltamed2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaltamed2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnaltaesp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnaltamed, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnaltamed2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnaltamed, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnaltaesp, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnaltamed2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Modificaciones"));
        jPanel3.setPreferredSize(new java.awt.Dimension(262, 129));

        btnmodmed.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodmed.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728943 - glasses reading.png"))); // NOI18N
        btnmodmed.setText("Médico");
        btnmodmed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodmedActionPerformed(evt);
            }
        });

        btnmodesp.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodesp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728972 - gold medal.png"))); // NOI18N
        btnmodesp.setText("Especialidad");
        btnmodesp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodespActionPerformed(evt);
            }
        });

        btnaltamed1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaltamed1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728931 - browser earth world.png"))); // NOI18N
        btnaltamed1.setText("Zonas");
        btnaltamed1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaltamed1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnmodesp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnmodmed, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnaltamed1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnaltamed1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(btnmodmed)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnmodesp, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Bonos"));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Cantidad:");

        txtcantidad.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcantidad.setForeground(new java.awt.Color(0, 102, 204));
        txtcantidad.setNextFocusableComponent(txtdesdex4);
        txtcantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcantidadActionPerformed(evt);
            }
        });
        txtcantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcantidadKeyPressed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Desde:");

        txtdesdex4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdesdex4.setForeground(new java.awt.Color(0, 102, 204));
        txtdesdex4.setText("0");
        txtdesdex4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtdesdex4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdesdex4ActionPerformed(evt);
            }
        });
        txtdesdex4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdesdex4KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdesdex4KeyReleased(evt);
            }
        });

        txthastax4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txthastax4.setForeground(new java.awt.Color(0, 102, 204));
        txthastax4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txthastax4ActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Hasta:");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txthastax4)
                    .addComponent(txtcantidad)
                    .addComponent(txtdesdex4, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtdesdex4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txthastax4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Observaciones"));

        txtobs.setColumns(10);
        txtobs.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtobs.setRows(5);
        txtobs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtobsKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(txtobs);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );

        btnmodmed1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodmed1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        btnmodmed1.setText("Impimir Bono");
        btnmodmed1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodmed1ActionPerformed(evt);
            }
        });

        btnimprimetabla.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimetabla.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728928 - document paper.png"))); // NOI18N
        btnimprimetabla.setText("Impimir Remito");
        btnimprimetabla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimetablaActionPerformed(evt);
            }
        });

        btnmodmed3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodmed3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728953 - letter mail.png"))); // NOI18N
        btnmodmed3.setText("Sobres 1° Entrega");
        btnmodmed3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodmed3ActionPerformed(evt);
            }
        });

        btnmodmed4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodmed4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728962 - discount pricetag shopping.png"))); // NOI18N
        btnmodmed4.setText("Bonos 1° Entrega");
        btnmodmed4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodmed4ActionPerformed(evt);
            }
        });

        btnmodmed5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodmed5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728913 - book bookmark reading.png"))); // NOI18N
        btnmodmed5.setText("Bonos en Blanco");
        btnmodmed5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodmed5ActionPerformed(evt);
            }
        });

        btnmodmed6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodmed6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnmodmed6.setText("Salir");
        btnmodmed6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodmed6ActionPerformed(evt);
            }
        });

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ultima entrega", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(102, 102, 102));
        jLabel5.setText("Periodo:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(102, 102, 102));
        jLabel9.setText("Cantidad:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(102, 102, 102));
        jLabel10.setText("Tipo de Entrega:");

        txtperiodo.setEditable(false);
        txtperiodo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtperiodo.setForeground(new java.awt.Color(0, 102, 204));
        txtperiodo.setBorder(null);

        txtcantidad2.setEditable(false);
        txtcantidad2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcantidad2.setForeground(new java.awt.Color(0, 102, 204));
        txtcantidad2.setBorder(null);

        txttipo.setEditable(false);
        txttipo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttipo.setForeground(new java.awt.Color(0, 102, 204));
        txttipo.setBorder(null);

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(102, 102, 102));
        jLabel13.setText("Desde:");

        txtdesde2.setEditable(false);
        txtdesde2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdesde2.setForeground(new java.awt.Color(0, 102, 204));
        txtdesde2.setBorder(null);
        txtdesde2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtdesde2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdesde2ActionPerformed(evt);
            }
        });
        txtdesde2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdesde2KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdesde2KeyReleased(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(102, 102, 102));
        jLabel14.setText("Hasta:");

        txthasta2.setEditable(false);
        txthasta2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txthasta2.setForeground(new java.awt.Color(0, 102, 204));
        txthasta2.setBorder(null);
        txthasta2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txthasta2ActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(102, 102, 102));
        jLabel6.setText("Fecha Entega:");

        txtentrega.setEditable(false);
        txtentrega.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtentrega.setForeground(new java.awt.Color(0, 102, 204));
        txtentrega.setBorder(null);

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(102, 102, 102));
        jLabel15.setText("Fecha Recibido:");

        txtrecibido.setEditable(false);
        txtrecibido.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtrecibido.setForeground(new java.awt.Color(0, 102, 204));
        txtrecibido.setBorder(null);

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(102, 102, 102));
        jLabel16.setText("Recibido por:");

        txtnombre.setEditable(false);
        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.setBorder(null);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtperiodo)
                            .addComponent(txtcantidad2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtdesde2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txttipo, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(txthasta2))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(txtentrega))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtrecibido))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(txtnombre)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtperiodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtcantidad2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttipo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtdesde2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txthasta2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtentrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtrecibido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        btntabla.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btntabla.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728900 - application browser.png"))); // NOI18N
        btntabla.setText("Impimir Tabla");
        btntabla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntablaActionPerformed(evt);
            }
        });

        btnsobres.setText("Sobres Medicos");
        btnsobres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsobresActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnmodmed1)
                        .addGap(18, 18, 18)
                        .addComponent(btnimprimetabla)
                        .addGap(18, 18, 18)
                        .addComponent(btntabla)
                        .addGap(18, 18, 18)
                        .addComponent(btnmodmed3)
                        .addGap(18, 18, 18)
                        .addComponent(btnmodmed4)
                        .addGap(18, 18, 18)
                        .addComponent(btnmodmed5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnsobres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnmodmed6, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnmodmed6)
                        .addComponent(btnmodmed1)
                        .addComponent(btnimprimetabla)
                        .addComponent(btnmodmed3)
                        .addComponent(btnmodmed5)
                        .addComponent(btnmodmed4)
                        .addComponent(btntabla))
                    .addComponent(btnsobres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    String completarceros(String v, int d) {
        if (v.length() < d) {
            String ceros = "";
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        return v;
    }

    void cargardatosentregaclic() {
        tabla.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    id_medico = tabla.getValueAt(tabla.getSelectedRow(), 0).toString();
                    ConexionMySQLPami mysql = new ConexionMySQLPami();
                    Connection cn = mysql.Conectar();
                    String sql = "SELECT mes, año, entrega, cantidadx4, desdex4, hastax4, fecha_entrega, fecha_recibido, control.nombre_control FROM control INNER JOIN medicos_control ON medicos_control.id_Control = control.id_Control INNER JOIN medicos ON medicos.id_Medico = medicos_control.id_Medico where medicos.id_Medico=" + id_medico + " order by control.id_control ASC";
                    try {
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sql);
                        while (rs.next()) {
                            txtperiodo.setText(rs.getString("Mes") + " " + rs.getString("año"));
                            txtcantidad2.setText(rs.getString("cantidadx4"));
                            txttipo.setText(rs.getString("entrega"));
                            txtdesde2.setText(rs.getString("desdex4"));
                            txthasta2.setText(rs.getString("hastax4"));
                            txtentrega.setText(rs.getString("fecha_entrega"));
                            txtrecibido.setText(rs.getString("fecha_recibido"));
                            txtnombre.setText(rs.getString("nombre_control"));
                        }
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                }
            }
        });

    }

    void eventotabla() {
        tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionModel rowSM = tabla.getSelectionModel();

        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {

                String observacion = "";
                if (e.getValueIsAdjusting()) {
                    return;
                }

                ListSelectionModel lsm = (ListSelectionModel) e.getSource();

                if (lsm.isSelectionEmpty()) {
                } else {
                    if (tabla.getValueAt(tabla.getSelectedRow(), 9) != null) {
                        observacion = tabla.getValueAt(tabla.getSelectedRow(), 9).toString();
                    } else {
                        observacion = "";
                    }
                    txtdesdex4.setText("");
                    txthastax4.setText("");
                    int selectedRow = lsm.getMinSelectionIndex();

                    if (tabla.getSelectedRow() != -1) {
                        id_especialidad = tabla.getValueAt(tabla.getSelectedRow(), 4).toString();
                        capita = Integer.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 3).toString());

                        capita = (int) (capita * 0.2);

                        if (id_especialidad.equals("GENERALISTA")) {

                            if (Integer.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 3).toString()) > 100) {

                                cantidad = (int) (capita * 2 / 3 + capita * 1 / 6 + 1);
                            }
                            if (cantidad >= 50 && Integer.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 3).toString()) > 100) {

                                cantidad = (int) Math.round(cantidad * 0.8) + 1;
                                ////////////////bonos de 3
                                cantidad = (int) Math.round(cantidad * 4 / 3);
                                txtcantidad.setText(String.valueOf(cantidad));
                                txtobs.setText(String.valueOf(observacion));

                            } else {
                                if (cantidad > 10 && cantidad <= 49) {
                                    ////////////////bonos de 3
                                    cantidad = Math.round(cantidad * 4 / 3);
                                    cantidad = (int) (cantidad + 1);

                                    // cantidad = (int) (capita * 2 / 3 + capita * 1 / 6 + 1);
                                    txtcantidad.setText(String.valueOf(cantidad));
                                    txtobs.setText(String.valueOf(observacion));

                                } else {
                                    cantidad = 10;
                                    txtcantidad.setText(String.valueOf(cantidad));
                                    txtobs.setText(String.valueOf(observacion));
                                }
                            }
                        } else {
                            cantidad = (int) ((capita * 0.75) + 1);

                            if (cantidad > 10 && Integer.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 3).toString()) > 100) {

                                cantidad = (int) Math.round(cantidad * 0.7) + 1;
                                cantidad = (int) (Math.round(cantidad * 4 / 3) + 1);

                                //cantidad = (int) (capita * 0.75) + 1;
                                txtcantidad.setText(String.valueOf(cantidad));
                                txtobs.setText(String.valueOf(observacion));
                                if (cantidad > 10 && cantidad <= 49) {
                                    ////////////////bonos de 3
                                    cantidad = Math.round(cantidad * 4 / 3);
                                    cantidad = (int) (cantidad + 1);
                                    txtcantidad.setText(String.valueOf(cantidad));
                                    txtobs.setText(String.valueOf(observacion));
                                }
                            } else {
                                cantidad = 10;
                                ///////////////bonos de 3
                                //  cantidad = Math.round(cantidad * 4 / 3);
                                // cantidad = cantidad + 1;
                                txtcantidad.setText(String.valueOf(cantidad));
                                txtobs.setText(String.valueOf(observacion));
                            }

                        }
                    }

                }
            }
        });
    }

    private void btnaltamedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaltamedActionPerformed
        new AltaMedicos(this, true).setVisible(true);
        cargartabla("");
    }//GEN-LAST:event_btnaltamedActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
///        cargartabla(txtbuscar.getText());
        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
        tabla.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnaltaespActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaltaespActionPerformed
        new AltaEspecialidad(this, true).setVisible(true);
    }//GEN-LAST:event_btnaltaespActionPerformed

    private void btnmodespActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodespActionPerformed
        new ModEspecialidad(this, true).setVisible(true);
    }//GEN-LAST:event_btnmodespActionPerformed

    private void btnmodmedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodmedActionPerformed
        if (tabla.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "No seleccionó ningún médico");
        } else {
            idmedico = Integer.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 0).toString());
            /*nombremed = tabla.getValueAt(tabla.getSelectedRow(), 1).toString();
             direccionmed = tabla.getValueAt(tabla.getSelectedRow(), 2).toString();
             capita = Integer.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 3).toString());
             especialidad = tabla.getValueAt(tabla.getSelectedRow(), 4).toString();*/

            new ModMed(this, true).setVisible(true);
            cargartabla("");
            cargarcombolocalidad();
            borrarpractica();
            cargarcombodireccion();
        }
    }//GEN-LAST:event_btnmodmedActionPerformed

    private void txtdesdex4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdesdex4KeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            dx4 = Integer.valueOf(txtdesdex4.getText());
            x4 = Integer.valueOf(txtcantidad.getText());
            h4 = dx4 + x4 - 1;
            txthastax4.setText(String.valueOf(MainPami.h4));
        }
    }//GEN-LAST:event_txtdesdex4KeyPressed

    public class ToggleSelectionModel extends DefaultListSelectionModel {

        private boolean gestureStarted = false;

        public void setSelectionInterval(int index0, int index1) {
            if (isSelectedIndex(index0) && !gestureStarted) {
                super.removeSelectionInterval(index0, index1);
            } else {
                super.addSelectionInterval(index0, index1);
            }
            gestureStarted = true;
        }

        public void setValueIsAdjusting(boolean isAdjusting) {
            if (isAdjusting == false) {
                gestureStarted = false;
            }
        }
    }

    private void txtobsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobsKeyPressed
    }//GEN-LAST:event_txtobsKeyPressed

    private void txtcantidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcantidadKeyPressed
        // TODO add your handling code here:
        //int opcion; 
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtdesdex4.setEditable(true);
            txthastax4.setEditable(true);
            txtcantidad.transferFocus();
            //opcion = JOptionPane.showConfirmDialog(this, "¿Es Segunda Entrega?", "Mensaje", JOptionPane.YES_NO_OPTION);
            /* if (cboentrega.getSelectedItem().toString().equals("SEGUNDA ENTREGA")) {
             txtdesdex4.setEditable(false);
             txthastax4.setEditable(false);
             } else {
             txtdesdex4.setEditable(true);
             txthastax4.setEditable(true);
             txtcantidad.transferFocus();

             }*/
 /* cantidad = Integer.valueOf(txtcantidad.getText());
             if (id_especialidad.equals("GENERALISTA")) {
             if (cantidad > 100) {
             x4 = new Double((cantidad * 0.666666667) / 10).intValue();
             x4 = x4 * 10;
             txtcantidad.setText(String.valueOf(Main.x4));
             x2 = cantidad - x4;
             //  txtx2.setText(String.valueOf(Main.x2));
             } else {
             x4 = 10;
             txtcantidad.setText(String.valueOf(Main.x4));
             x2 = 0;
             //  txtx2.setText(String.valueOf(Main.x2));
             }
             } else {
             x2 = new Double((cantidad * 0.5) / 10).intValue();
             x2 = x2 * 10;
             x4 = cantidad - x2;
             //  txtx2.setText(String.valueOf(Main.x2));
             txtcantidad.setText(String.valueOf(Main.x4));

             }*/


    }//GEN-LAST:event_txtcantidadKeyPressed
    }

    void cargarperiodo() {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
    }

    public double Redondearentero(double numero) {
        return Math.rint(numero);
    }

    private void cbomedicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbomedicoActionPerformed
        if (cbomedico.getSelectedIndex() == 0) {
            cboaño.setSelectedIndex(0);
            cbomes.setSelectedIndex(0);
            cboentrega.setSelectedIndex(0);
            cargartabla("");
            txtbuscar.setText("");
        }
        if (cbomedico.getSelectedIndex() == 1) {
            buscargeneralistas("1");
            txtbuscar.setText("");
        }
        if (cbomedico.getSelectedIndex() == 2) {
            buscarespecialidad("1");
            txtbuscar.setText("");
        }
    }//GEN-LAST:event_cbomedicoActionPerformed

    private void txtdesdex4KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdesdex4KeyReleased
        if (!txtdesdex4.getText().equals("")) {
            dx4 = Integer.valueOf(txtdesdex4.getText());
            x4 = Integer.valueOf(txtcantidad.getText());
            h4 = dx4 + x4 - 1;
            txthastax4.setText(String.valueOf(MainPami.h4));
        }
    }//GEN-LAST:event_txtdesdex4KeyReleased

    private void cbomesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbomesActionPerformed

        if (cbomes.getSelectedItem().toString().equals("ENERO")) {
            fecha_vencimiento = "31 Enero de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("FEBRERO")) {
            fecha_vencimiento = "28 Febrero de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("MARZO")) {
            fecha_vencimiento = "31 Marzo de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("ABRIL")) {
            fecha_vencimiento = "30 Abril de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("MAYO")) {
            fecha_vencimiento = "31 Mayo de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("JUNIO")) {
            fecha_vencimiento = "30 Junio de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("JULIO")) {
            fecha_vencimiento = "31 Julio de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("AGOSTO")) {
            fecha_vencimiento = "31 Agosto de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("SEPTIEMBRE")) {
            fecha_vencimiento = "30 Septiembre de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("OCTUBRE")) {
            fecha_vencimiento = "31 Octubre de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("NOVIEMBRE")) {
            fecha_vencimiento = "30 Noviembre de " + cboaño.getSelectedItem().toString();
        }
        if (cbomes.getSelectedItem().toString().equals("DICIEMBRE")) {
            fecha_vencimiento = "31 Diciembre de " + cboaño.getSelectedItem().toString();
        }
        mes = cbomes.getSelectedItem().toString();
    }//GEN-LAST:event_cbomesActionPerformed

    private void cbolocalidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbolocalidadActionPerformed

        if (!cbolocalidad.getSelectedItem().toString().equals("Localidad")) {
            filtarlocalidad(cbolocalidad.getSelectedItem().toString());
        } else {
            filtarlocalidad("");
        }

    }//GEN-LAST:event_cbolocalidadActionPerformed

    private void txtdireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdireccionActionPerformed
        if (!txtdireccion.getText().equals("Domicilio")) {
            filtardomicilio(txtdireccion.getText());
            txtbuscar.setText("");
        } else {
            if (!txtdireccion.getText().equals("")) {
                filtardomicilio("");
                txtbuscar.setText("");
            }
        }
    }//GEN-LAST:event_txtdireccionActionPerformed

    private void btnmodmed1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodmed1ActionPerformed

        if (tabla.getSelectedRow() != -1 && !txtcantidad.getText().equals("") && !txthastax4.getText().equals("") && !txtdesdex4.getText().equals("")) {
            int desde = Integer.valueOf(txtdesdex4.getText()), hasta = Integer.valueOf(txthastax4.getText());
            int opcion = 0, contador = desde;
            cantidad = Integer.valueOf(txtcantidad.getText());
            mes = (String) cbomes.getSelectedItem();
            año = (String) cboaño.getSelectedItem();
            ConexionMySQLPami mysql = new ConexionMySQLPami();
            Connection cn = mysql.Conectar();
            opcion = JOptionPane.showConfirmDialog(this, "Desea grabar la información?", "Mensaje", JOptionPane.YES_NO_OPTION);
            if (opcion == 0) {
                /////////////////////////////////////graba/////////////////////////////////////////////
                String sql = "INSERT INTO control (Año, Mes, Cantidad, Observaciones, Entrega, cantidadx4, desdex4, hastax4, estado_control, direccion_entrega, capita_entrega) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?) ";
                nombre = tabla.getValueAt(tabla.getSelectedRow(), 1).toString() + "-MP: " + tabla.getValueAt(tabla.getSelectedRow(), 10).toString();
                idmedico = Integer.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 0).toString());
                id_especialidad = tabla.getValueAt(tabla.getSelectedRow(), 4).toString();
                try {
                    PreparedStatement pst = cn.prepareStatement(sql);
                    pst.setString(1, año);
                    pst.setString(2, mes);
                    pst.setInt(3, cantidad);
                    pst.setString(4, txtobs.getText());
                    pst.setString(5, cboentrega.getSelectedItem().toString());
                    pst.setInt(6, cantidad);
                    pst.setInt(7, desde);
                    pst.setInt(8, hasta);
                    pst.setInt(9, Integer.valueOf("1"));
                    pst.setString(10, tabla.getValueAt(tabla.getSelectedRow(), 2).toString());
                    pst.setInt(11, capita);
                    int n = pst.executeUpdate();
                    if (n > 0) {
                        String ssql = "SELECT MAX(id_Control) AS id_Control FROM control";
                        Statement st2 = cn.createStatement();
                        ResultSet rs = st2.executeQuery(ssql);
                        rs.last();
                        idcontrol = rs.getInt("id_Control");
                        String isql = "INSERT INTO medicos_control (id_Medico, id_Control) VALUES (" + idmedico + ", " + idcontrol + ")";
                        PreparedStatement pst3 = cn.prepareStatement(isql);
                        int n2 = pst3.executeUpdate();
                        if (n2 > 0) {
                            int i2 = 0;
                            JOptionPane.showMessageDialog(null, "Coloque los bonos del Dr/a. " + nombre);
                            /*opcion = JOptionPane.showConfirmDialog(this, "Desea grabar la información?", "Mensaje", JOptionPane.YES_NO_OPTION);
                             if (opcion == 0) {}*/

                            while (contador <= hasta) {
                                try {

                                    JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/orden-bono.jasper"));
                                    Map parametros = new HashMap();
                                    parametros.put("nombre", nombre);
                                    parametros.put("numero_bono", completarceros(String.valueOf(desde + i2), 6));
                                    if (!id_especialidad.equals("GENERALISTA")) {
                                        parametros.put("especialidad", id_especialidad);
                                    } else {
                                        parametros.put("especialidad", "");
                                    }

                                    JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                                    JasperPrintManager.printReport(jPrint, false);
                                } catch (Exception e) {
                                    JOptionPane.showMessageDialog(null, e);
                                }
                                contador++;
                                i2++;
                            }
                            try {
                                ///// Traigo los datos de control, medico, localidad y medico control
                                String sql2 = "SELECT institucion, horario_atencion  from medicos where id_Medico=" + tabla.getValueAt(tabla.getSelectedRow(), 0);
                                Statement st4 = cn.createStatement();
                                ResultSet rs4 = st4.executeQuery(sql2);
                                rs4.next();
                                String institucion = rs4.getString("institucion");
                                String horario_atencion = rs4.getString("horario_atencion");
                                if (institucion == null) {
                                    try {

                                        JOptionPane.showMessageDialog(null, "Una vez terminada la impresión de los bonos coloque el sobre...");
                                        /*opcion = JOptionPane.showConfirmDialog(this, "Desea grabar la información?", "Mensaje", JOptionPane.YES_NO_OPTION);
                                         if (opcion == 0) {}*/
                                        Map parametros = new HashMap();
                                        if (id_especialidad.equals("GENERALISTA")) {
                                            parametros.put("especialidad", tabla.getValueAt(tabla.getSelectedRow(), 4));
                                        } else {
                                            parametros.put("especialidad", tabla.getValueAt(tabla.getSelectedRow(), 4));
                                        }
                                        parametros.put("nombre", tabla.getValueAt(tabla.getSelectedRow(), 1) + "-MP: " + tabla.getValueAt(tabla.getSelectedRow(), 10).toString());
                                        parametros.put("direccion", tabla.getValueAt(tabla.getSelectedRow(), 2));
                                        parametros.put("cantidad", txtcantidad.getText());
                                        parametros.put("periodo", mes + " " + año);
                                        parametros.put("fecha", fecha);
                                        parametros.put("desdex4", txtdesdex4.getText());
                                        parametros.put("hastax4", txthastax4.getText());
                                        parametros.put("nombre_localidad", tabla.getValueAt(tabla.getSelectedRow(), 5));
                                        parametros.put("codigo_postal", tabla.getValueAt(tabla.getSelectedRow(), 6));
                                        parametros.put("institucion", "");

                                        if (horario_atencion == null) {
                                            parametros.put("horario_atencion", "");
                                        } else {
                                            parametros.put("horario_atencion", horario_atencion);
                                        }
                                        if (cboentrega.getSelectedItem().toString().equals("PRIMERA ENTREGA")) {
                                            parametros.put("entrega", "1° Entrega");
                                        }////PRIMERA ENTREGA, SEGUNDA ENTREGA, TERCERA ENTREGA, CUARTA ENTREGA
                                        if (cboentrega.getSelectedItem().toString().equals("SEGUNDA ENTREGA")) {
                                            parametros.put("entrega", "2° Entrega");
                                        }
                                        if (cboentrega.getSelectedItem().toString().equals("TERCERA ENTREGA")) {
                                            parametros.put("entrega", "3° Entrega");
                                        }
                                        if (cboentrega.getSelectedItem().toString().equals("CUARTA ENTREGA")) {
                                            parametros.put("entrega", "4° Entrega");
                                        }
                                        JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Sobres-2da-entrega.jasper"));
                                        JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                                        JasperPrintManager.printReport(jPrint, false);
                                    } catch (Exception e) {
                                        JOptionPane.showMessageDialog(null, e);
                                    }
                                } else {
                                    try {
                                        JOptionPane.showMessageDialog(null, "Una vez terminada la impresión de los bonos coloque el sobre...");
                                        Map parametros = new HashMap();
                                        parametros.put("especialidad", tabla.getValueAt(tabla.getSelectedRow(), 4));
                                        parametros.put("nombre", tabla.getValueAt(tabla.getSelectedRow(), 1) + "-MP: " + tabla.getValueAt(tabla.getSelectedRow(), 10).toString());
                                        parametros.put("direccion", tabla.getValueAt(tabla.getSelectedRow(), 2));
                                        parametros.put("cantidad", txtcantidad.getText());
                                        parametros.put("periodo", mes + " " + año);
                                        parametros.put("fecha", fecha);
                                        parametros.put("desdex4", txtdesdex4.getText());
                                        parametros.put("hastax4", txthastax4.getText());
                                        parametros.put("nombre_localidad", tabla.getValueAt(tabla.getSelectedRow(), 5));
                                        parametros.put("codigo_postal", tabla.getValueAt(tabla.getSelectedRow(), 6));
                                        parametros.put("institucion", institucion);
                                        if (horario_atencion == null) {
                                            parametros.put("horario_atencion", "");
                                        } else {
                                            parametros.put("horario_atencion", horario_atencion);
                                        }
                                        if (cboentrega.getSelectedItem().toString().equals("PRIMERA ENTREGA")) {
                                            parametros.put("entrega", "1° Entrega");
                                        }////PRIMERA ENTREGA, SEGUNDA ENTREGA, TERCERA ENTREGA, CUARTA ENTREGA
                                        if (cboentrega.getSelectedItem().toString().equals("SEGUNDA ENTREGA")) {
                                            parametros.put("entrega", "2° Entrega");
                                        }
                                        if (cboentrega.getSelectedItem().toString().equals("TERCERA ENTREGA")) {
                                            parametros.put("entrega", "3° Entrega");
                                        }
                                        if (cboentrega.getSelectedItem().toString().equals("CUARTA ENTREGA")) {
                                            parametros.put("entrega", "4° Entrega");
                                        }
                                        JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Sobres-2da-entrega.jasper"));
                                        JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                                        JasperPrintManager.printReport(jPrint, false);
                                    } catch (Exception e) {
                                        JOptionPane.showMessageDialog(null, e);
                                    }
                                }

                            } catch (SQLException ex) {
                                Logger.getLogger(MainPami.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Error al cargar el médico");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AltaMedicos.class.getName()).log(Level.SEVERE, null, ex);
                }
                /////////////////////////////////////////////////////////////////////////////////
            } else {
                ///////////////////////////////////////no graba///////////////////////////////////////////////////////
                nombre = tabla.getValueAt(tabla.getSelectedRow(), 1).toString();
                idmedico = Integer.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 0).toString());
                id_especialidad = tabla.getValueAt(tabla.getSelectedRow(), 4).toString();
                int i2 = 0;
                opcion = JOptionPane.showConfirmDialog(this, "Desea imprimir los bonos?", "Mensaje", JOptionPane.YES_NO_OPTION);
                if (opcion == 0) {
                    JOptionPane.showMessageDialog(null, "Coloque los bonos del Dr/a. " + nombre);
                    //  JOptionPane.showMessageDialog(null, hasta);
                    //JOptionPane.showMessageDialog(null, contador);
                    while (contador <= hasta) {
                        try {
                            JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/orden-bono.jasper"));
                            Map parametros = new HashMap();
                            parametros.put("nombre", nombre + "-MP: " + tabla.getValueAt(tabla.getSelectedRow(), 10).toString());
                            parametros.put("numero_bono", completarceros(String.valueOf(desde + i2), 6));
                            if (!id_especialidad.equals("GENERALISTA")) {
                                parametros.put("especialidad", id_especialidad);
                            } else {
                                parametros.put("especialidad", "");
                            }
                            JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                            JasperPrintManager.printReport(jPrint, false);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                        contador++;
                        i2++;
                    }
                }
                opcion = JOptionPane.showConfirmDialog(this, "Desea imprimir el sobre?", "Mensaje", JOptionPane.YES_NO_OPTION);
                if (opcion == 0) {
                    try {
                        ///// Traigo los datos de control, medico, localidad y medico control
                        String sql2 = "SELECT institucion,horario_atencion  from medicos where id_Medico=" + tabla.getValueAt(tabla.getSelectedRow(), 0);
                        Statement st4 = cn.createStatement();
                        ResultSet rs4 = st4.executeQuery(sql2);
                        rs4.next();
                        String institucion = rs4.getString("institucion");
                        String horario_atencion = rs4.getString("horario_atencion");
                        if (institucion == null) {
                            try {
                                JOptionPane.showMessageDialog(null, "Una vez terminada la impresión de los bonos coloque el sobre...");
                                Map parametros = new HashMap();
                                if (id_especialidad.equals("GENERALISTA")) {
                                    parametros.put("especialidad", tabla.getValueAt(tabla.getSelectedRow(), 4));
                                } else {
                                    parametros.put("especialidad", tabla.getValueAt(tabla.getSelectedRow(), 4));
                                }
                                parametros.put("nombre", tabla.getValueAt(tabla.getSelectedRow(), 1) + "-MP: " + tabla.getValueAt(tabla.getSelectedRow(), 10).toString());
                                parametros.put("direccion", tabla.getValueAt(tabla.getSelectedRow(), 2));
                                parametros.put("cantidad", txtcantidad.getText());
                                parametros.put("periodo", mes + " " + año);
                                parametros.put("fecha", fecha);
                                parametros.put("desdex4", txtdesdex4.getText());
                                parametros.put("hastax4", txthastax4.getText());
                                parametros.put("nombre_localidad", tabla.getValueAt(tabla.getSelectedRow(), 5));
                                parametros.put("codigo_postal", tabla.getValueAt(tabla.getSelectedRow(), 6));
                                parametros.put("institucion", "");
                                if (horario_atencion == null) {
                                    parametros.put("horario_atencion", "");
                                } else {
                                    parametros.put("horario_atencion", horario_atencion);
                                }
                                if (cboentrega.getSelectedItem().toString().equals("PRIMERA ENTREGA")) {
                                    parametros.put("entrega", "1° Entrega");
                                }////PRIMERA ENTREGA, SEGUNDA ENTREGA, TERCERA ENTREGA, CUARTA ENTREGA
                                if (cboentrega.getSelectedItem().toString().equals("SEGUNDA ENTREGA")) {
                                    parametros.put("entrega", "2° Entrega");
                                }
                                if (cboentrega.getSelectedItem().toString().equals("TERCERA ENTREGA")) {
                                    parametros.put("entrega", "3° Entrega");
                                }
                                if (cboentrega.getSelectedItem().toString().equals("CUARTA ENTREGA")) {
                                    parametros.put("entrega", "4° Entrega");
                                }
                                JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Sobres-2da-entrega.jasper"));
                                JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                                JasperPrintManager.printReport(jPrint, false);
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, e);
                            }
                        } else {
                            try {
                                JOptionPane.showMessageDialog(null, "Una vez terminada la impresión de los bonos coloque el sobre...");
                                Map parametros = new HashMap();
                                parametros.put("especialidad", tabla.getValueAt(tabla.getSelectedRow(), 4));
                                parametros.put("nombre", tabla.getValueAt(tabla.getSelectedRow(), 1) + "-MP: " + tabla.getValueAt(tabla.getSelectedRow(), 10).toString());
                                parametros.put("direccion", tabla.getValueAt(tabla.getSelectedRow(), 2));
                                parametros.put("cantidad", txtcantidad.getText());
                                parametros.put("periodo", mes + " " + año);
                                parametros.put("fecha", fecha);
                                parametros.put("desdex4", txtdesdex4.getText());
                                parametros.put("hastax4", txthastax4.getText());
                                parametros.put("nombre_localidad", tabla.getValueAt(tabla.getSelectedRow(), 5));
                                parametros.put("codigo_postal", tabla.getValueAt(tabla.getSelectedRow(), 6));
                                parametros.put("institucion", institucion);
                                if (horario_atencion == null) {
                                    parametros.put("horario_atencion", "");
                                } else {
                                    parametros.put("horario_atencion", horario_atencion);
                                }
                                if (cboentrega.getSelectedItem().toString().equals("PRIMERA ENTREGA")) {
                                    parametros.put("entrega", "1° Entrega");
                                }////PRIMERA ENTREGA, SEGUNDA ENTREGA, TERCERA ENTREGA, CUARTA ENTREGA
                                if (cboentrega.getSelectedItem().toString().equals("SEGUNDA ENTREGA")) {
                                    parametros.put("entrega", "2° Entrega");
                                }
                                if (cboentrega.getSelectedItem().toString().equals("TERCERA ENTREGA")) {
                                    parametros.put("entrega", "3° Entrega");
                                }
                                if (cboentrega.getSelectedItem().toString().equals("CUARTA ENTREGA")) {
                                    parametros.put("entrega", "4° Entrega");
                                }
                                JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Sobres-2da-entrega.jasper"));
                                JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                                JasperPrintManager.printReport(jPrint, false);
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, e);
                            }
                        }

                    } catch (SQLException ex) {
                        Logger.getLogger(MainPami.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                /////////////////////////////////////////////////////////////////////////////////////////////
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe completar los campos obligatorios...");
        }

        // }
        /////////////////////////////////////////////////////////////////////////////////////MEDICOS ESPECIALISTAS
        id_especialidad = "0";
        id_especialidad = tabla.getValueAt(tabla.getSelectedRow(), 4).toString();
        //JOptionPane.showMessageDialog(null, id_especialidad);
        /*if (tabla.getSelectedRow() != -1 && !id_especialidad.equals("GENERALISTA")) {

         if (opcion == 0) {

         if (tabla.getSelectedRow() != -1 && !txtcantidad.getText().equals("") && !txtcantidad.getText().equals("")) {

         ConexionMySQL mysql = new ConexionMySQL();
         Connection cn = mysql.Conectar();
         String sql = "INSERT INTO control (Mes,Cantidad,Observacion,cantidadx4,cantidadx2,desdex4,hastax4,desdex2,hastax2) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ";
         mes = (String) cbomes.getSelectedItem();
         idmedico = Integer.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 0).toString());

         try {
         PreparedStatement pst = cn.prepareStatement(sql);
         pst.setString(1, mes);
         pst.setInt(2, Integer.valueOf(txtcantidad.getText()));
         pst.setString(3, txtobs.getText());
         pst.setInt(4, Integer.valueOf(txtcantidad.getText()));
         pst.setInt(5, Integer.valueOf("0"));
         pst.setInt(6, Integer.valueOf(txtdesdex4.getText()));
         pst.setInt(7, Integer.valueOf(txthastax4.getText()));
         pst.setInt(8, Integer.valueOf("0"));
         pst.setInt(9, Integer.valueOf("0"));
         //pst.setInt(10, idmedico);
         int n = pst.executeUpdate();
         if (n > 0) {
         String ssql = "SELECT MAX(id_Control) AS id_Control FROM control";
         Statement st2 = cn.createStatement();
         ResultSet rs = st2.executeQuery(ssql);
         rs.last();
         idcontrol = rs.getInt("id_Control");
         String isql = "INSERT INTO medicos_control (id_Medico, id_Control) VALUES (" + idmedico + ", " + idcontrol + ")";
         PreparedStatement pst3 = cn.prepareStatement(isql);
         n = pst3.executeUpdate();

         // this.dispose();
         } else {
         // JOptionPane.showMessageDialog(null, "Error al cargar el médico");
         JOptionPane.showMessageDialog(null, "Error al cargar el médi555co");
         }
         } catch (SQLException ex) {
         Logger.getLogger(AltaMedicos.class.getName()).log(Level.SEVERE, null, ex);
         }
         }

         nombre = tabla.getValueAt(tabla.getSelectedRow(), 1).toString();
         direccion = tabla.getValueAt(tabla.getSelectedRow(), 2).toString();
         especialidad1 = tabla.getValueAt(tabla.getSelectedRow(), 4).toString();
         localidad = tabla.getValueAt(tabla.getSelectedRow(), 5).toString();;
         codigo_postal = tabla.getValueAt(tabla.getSelectedRow(), 6).toString();;
         desdex4 = txtdesdex4.getText();
         hastax4 = txthastax4.getText();

         LinkedList<Ordenes> Resultados = new LinkedList<Ordenes>();
         Ordenes tipo;
         Resultados.clear();
         try {
         JasperReport reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Sobres.jasper"));
         tipo = new Ordenes(numero_orden, nombre, direccion, localidad, especialidad1, codigo_postal, desdex4, desdex2, hastax4, hastax2);
         Resultados.add(tipo);
         JasperPrint jPrint = JasperFillManager.fillReport(reporte, null, new JRBeanCollectionDataSource(Resultados));
         JasperPrintManager.printReport(jPrint, true);
         } catch (Exception e) {
         JOptionPane.showMessageDialog(null, e);
         }
         LinkedList<Ordenes> Resultados2 = new LinkedList<Ordenes>();
         Ordenes tipo1;
         Resultados2.clear();
         try {
         //  
         i = Integer.valueOf(txtdesdex4.getText());
         //JOptionPane.showMessageDialog(null,i);
         // JOptionPane.showMessageDialog(null,h4);
         while (i <= h4) {

         numero_orden = completarceros(String.valueOf(i), 4) + completarceros(tabla.getValueAt(tabla.getSelectedRow(), 0).toString(), 4) + completarceros(tabla.getValueAt(tabla.getSelectedRow(), 7).toString(), 4);

         //JOptionPane.showMessageDialog(null,numero_orden);
         tipo1 = new Ordenes(numero_orden, nombre, direccion, localidad, especialidad1, codigo_postal, desdex4, hastax4, desdex2, hastax2);
         Resultados2.add(tipo1);
         i++;
         }
         JasperReport reporte2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenx4-esp.jasper"));
         JasperPrint jPrint = JasperFillManager.fillReport(reporte2, null, new JRBeanCollectionDataSource(Resultados2));
         JasperPrintManager.printReport(jPrint, true);
         } catch (Exception e) {
         JOptionPane.showMessageDialog(null, e);
         }

         LinkedList<Ordenes> Resultados3 = new LinkedList<Ordenes>();
         Ordenes tipo3;
         Resultados3.clear();
         try {
         //  
         /*i = Integer.valueOf(txtdesdex2.getText());
         // JOptionPane.showMessageDialog(null,h4);
         while (i <= h2) {

         numero_orden = completarceros(String.valueOf(i), 4) + completarceros(tabla.getValueAt(tabla.getSelectedRow(), 0).toString(), 4) + completarceros(tabla.getValueAt(tabla.getSelectedRow(), 7).toString(), 4);

         tipo3 = new Ordenes(numero_orden, nombre, direccion, localidad, especialidad1, codigo_postal, desdex4, hastax4, desdex2, hastax2);
         Resultados3.add(tipo3);
         i++;
         }
         JasperReport reporte3 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenx4-esp.jasper"));
         JasperPrint jPrint = JasperFillManager.fillReport(reporte3, null, new JRBeanCollectionDataSource(Resultados3));
         JasperPrintManager.printReport(jPrint, true);
         } catch (Exception e) {
         JOptionPane.showMessageDialog(null, e);
         }
         }                    
         }*/
    }//GEN-LAST:event_btnmodmed1ActionPerformed

    private void btnimprimetablaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimetablaActionPerformed
        new EncabezadoyPie(this, true).setVisible(true);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO add your handling code here:
        //Declaro la conexion Mysql
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        //Declaro variables privadas 
        mes = (String) cbomes.getSelectedItem().toString();
        año = (String) cboaño.getSelectedItem().toString();
        entrega = (String) cboentrega.getSelectedItem();
        localidad_filtro = (String) cbolocalidad.getSelectedItem();
        int sobre = 0;

        //Declaro una lista desde ordenes 
        String institucion, localidad = "";
        LinkedList<reporte_entrega> reporte_entrega = new LinkedList<reporte_entrega>();
        reporte_entrega tipos = null;
        reporte_entrega.clear();
        //L!,Z...,D""
        System.out.println("1 paso");
        if (txtdireccion.getText().equals("") && !cbolocalidad.getSelectedItem().toString().equals("Localidad") && cbozona.getSelectedItem().toString().equals("...")) {
            try {
                ///// Traigo los datos de control, medico, localidad y medico control
                String ssql = "SELECT especialidad, medicos.nombre,direccion,cantidad,desdex4,hastax4,institucion,nombre_localidad,codigo_postal  from medicos INNER JOIN especialidades using(id_especialidad) INNER JOIN medicos_control using(id_Medico) INNER JOIN control  using(id_control) INNER JOIN localidades using(id_localidad) INNER JOIN zonas ON medicos.id_zona=zonas.id where Mes='" + mes + "' and año=" + año + " and nombre_localidad='" + localidad_filtro + "' order by  zonas.nombre, direccion, medicos.nombre";
                Statement st4 = cn.createStatement();
                ResultSet rs4 = st4.executeQuery(ssql);
                System.out.println("2 paso");
                while (rs4.next()) {
                    tipos = new reporte_entrega(rs4.getString("especialidad"), rs4.getString("medicos.nombre"), rs4.getString("direccion"), rs4.getString("cantidad"), rs4.getString("desdex4"), rs4.getString("hastax4"), "", rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), "", "", String.valueOf(sobre));
                    reporte_entrega.add(tipos);
                    localidad = rs4.getString("nombre_localidad");
                    sobre++;
                }
                System.out.println("pasa el while rs4 paso");
                try {
                    JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/reporte_entrega.jasper"));
                    Map parametros = new HashMap();
                    parametros.put("localidad1", localidad);
                    parametros.put("encabezado", encabezado2);
                    parametros.put("pie", pie2);
                    JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JRBeanCollectionDataSource(reporte_entrega));
                    JasperPrintManager.printReport(jPrint, false);
                    System.out.println("jasper");
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }

            } catch (SQLException ex) {
                Logger.getLogger(MainPami.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }//z!,D"",L...
        //if (!cbozona.getSelectedItem().toString().equals("...")) {
        if (txtdireccion.getText().equals("") && cbolocalidad.getSelectedItem().toString().equals("Localidad") && !cbozona.getSelectedItem().toString().equals("...")) {
            System.out.println("if 2");

            try {
                ///// Traigo los datos de control, medico, localidad y medico control
                String ssql = "SELECT especialidad, medicos.nombre,direccion,cantidad,desdex4,hastax4,institucion,nombre_localidad,codigo_postal  from medicos INNER JOIN especialidades using(id_especialidad) INNER JOIN medicos_control using(id_Medico) INNER JOIN control  using(id_control) INNER JOIN localidades using(id_localidad) INNER JOIN zonas ON medicos.id_zona=zonas.id where mes='" + mes + "' and año=" + año + " and zonas.nombre='" + cbozona.getSelectedItem().toString() + "' order by  zonas.nombre, direccion, medicos.nombre";
                Statement st4 = cn.createStatement();
                ResultSet rs4 = st4.executeQuery(ssql);

                while (rs4.next()) {
                    tipos = new reporte_entrega(rs4.getString("especialidad"), rs4.getString("medicos.nombre"), rs4.getString("direccion"), rs4.getString("cantidad"), rs4.getString("desdex4"), rs4.getString("hastax4"), "", rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), "", "", String.valueOf(sobre));
                    reporte_entrega.add(tipos);
                    System.out.println(rs4.getString("nombre_localidad"));
                    localidad = rs4.getString("nombre_localidad");
                    sobre++;
                }
                try {
                    System.out.println("jaspert  2");
                    JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/reporte_entrega.jasper"));
                    Map parametros = new HashMap();
                    parametros.put("localidad1", localidad);
                    parametros.put("encabezado", encabezado2);
                    parametros.put("pie", pie2);
                    JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JRBeanCollectionDataSource(reporte_entrega));
                    JasperPrintManager.printReport(jPrint, true);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }

            } catch (SQLException ex) {
                Logger.getLogger(MainPami.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        /////L!,Z!,D!
        if (!txtdireccion.getText().equals("") && !cbolocalidad.getSelectedItem().toString().equals("Localidad") && !cbozona.getSelectedItem().toString().equals("...")) {
            try {
                ///// Traigo los datos de control, medico, localidad y medico control
                String ssql = "SELECT especialidad, medicos.nombre,direccion,cantidad,desdex4,hastax4,institucion,nombre_localidad,codigo_postal  from medicos INNER JOIN especialidades using(id_especialidad) INNER JOIN medicos_control using(id_Medico) INNER JOIN control  using(id_control) INNER JOIN localidades using(id_localidad) INNER JOIN zonas ON medicos.id_zona=zonas.id where mes='" + mes + "' and año=" + año + " and nombre_localidad='" + localidad_filtro + "' and direccion='" + txtdireccion.getText() + "' and zonas.nombre='" + cbozona.getSelectedItem().toString() + "' order by medicos.nombre, zonas.nombre";
                Statement st4 = cn.createStatement();
                ResultSet rs4 = st4.executeQuery(ssql);

                while (rs4.next()) {
                    tipos = new reporte_entrega(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), rs4.getString("cantidad"), rs4.getString("desdex4"), rs4.getString("hastax4"), "", rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), "", "", String.valueOf(sobre));
                    reporte_entrega.add(tipos);
                    localidad = rs4.getString("nombre_localidad");
                    sobre++;
                }
                try {
                    JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/reporte_entrega.jasper"));
                    Map parametros = new HashMap();
                    parametros.put("localidad1", localidad);
                    parametros.put("encabezado", encabezado2);
                    parametros.put("pie", pie2);
                    JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JRBeanCollectionDataSource(reporte_entrega));
                    JasperPrintManager.printReport(jPrint, false);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }

            } catch (SQLException ex) {
                Logger.getLogger(MainPami.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        /////L!,Z...,D!
        if (!txtdireccion.getText().equals("") && !cbolocalidad.getSelectedItem().toString().equals("Localidad") && cbozona.getSelectedItem().toString().equals("...")) {
            try {
                ///// Traigo los datos de control, medico, localidad y medico control
                String ssql = "SELECT especialidad, medicos.nombre,direccion,cantidad,desdex4,hastax4,institucion,nombre_localidad,codigo_postal  from medicos INNER JOIN especialidades using(id_especialidad) INNER JOIN medicos_control using(id_Medico) INNER JOIN control  using(id_control) INNER JOIN localidades using(id_localidad) where mes='" + mes + "' and año=" + año + " and nombre_localidad='" + localidad_filtro + "' and direccion='" + txtdireccion.getText() + "' order by medicos.nombre";
                Statement st4 = cn.createStatement();
                ResultSet rs4 = st4.executeQuery(ssql);

                while (rs4.next()) {
                    tipos = new reporte_entrega(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), rs4.getString("cantidad"), rs4.getString("desdex4"), rs4.getString("hastax4"), "", rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), "", "", String.valueOf(sobre));
                    reporte_entrega.add(tipos);
                    localidad = rs4.getString("nombre_localidad");
                    sobre++;
                }
                try {
                    JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/reporte_entrega.jasper"));
                    Map parametros = new HashMap();
                    parametros.put("localidad1", localidad);
                    parametros.put("encabezado", encabezado2);
                    parametros.put("pie", pie2);
                    JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JRBeanCollectionDataSource(reporte_entrega));
                    JasperPrintManager.printReport(jPrint, false);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }

            } catch (SQLException ex) {
                Logger.getLogger(MainPami.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        /////L...,Z!,D!
        if (!txtdireccion.getText().equals("") && cbolocalidad.getSelectedItem().toString().equals("Localidad") && !cbozona.getSelectedItem().toString().equals("...")) {
            try {
                ///// Traigo los datos de control, medico, localidad y medico control
                String ssql = "SELECT especialidad, medicos.nombre,direccion,cantidad,desdex4,hastax4,institucion,nombre_localidad,codigo_postal  from medicos INNER JOIN especialidades using(id_especialidad) INNER JOIN medicos_control using(id_Medico) INNER JOIN control  using(id_control) INNER JOIN localidades using(id_localidad) INNER JOIN zonas ON medicos.id_zona=zonas.id where mes='" + mes + "' and año=" + año + " and zonas.nombre='" + cbozona.getSelectedItem().toString() + "' and direccion='" + txtdireccion.getText() + "' order by medicos.nombre, zonas.nombre";
                Statement st4 = cn.createStatement();
                ResultSet rs4 = st4.executeQuery(ssql);

                while (rs4.next()) {
                    tipos = new reporte_entrega(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), rs4.getString("cantidad"), rs4.getString("desdex4"), rs4.getString("hastax4"), "", rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), "", "", String.valueOf(sobre));
                    reporte_entrega.add(tipos);
                    localidad = rs4.getString("nombre_localidad");
                    sobre++;
                }
                try {
                    JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/reporte_entrega.jasper"));
                    Map parametros = new HashMap();
                    parametros.put("localidad1", localidad);
                    parametros.put("encabezado", encabezado2);
                    parametros.put("pie", pie2);
                    JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JRBeanCollectionDataSource(reporte_entrega));
                    JasperPrintManager.printReport(jPrint, false);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }

            } catch (SQLException ex) {
                Logger.getLogger(MainPami.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnimprimetablaActionPerformed

    private void btnmodmed3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodmed3ActionPerformed
        // TODO add your handling code here:
        //Declaro la conexion Mysql
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        //Declaro variables privadas 
        mes = (String) cbomes.getSelectedItem();
        año = (String) cboaño.getSelectedItem();
        entrega = (String) cboentrega.getSelectedItem();
        int sobre = 1;
        //Declaro una lista desde ordenes 
        String institucion;
        LinkedList<Sobres> Resultado_Sobre = new LinkedList<Sobres>();
        Sobres tipos = null;
        Resultado_Sobre.clear();

        try {
            ///// Traigo los datos de control, medico, localidad y medico control
            String ssql = "SELECT especialidad, nombre,direccion,cantidad,desdex4,hastax4,institucion,nombre_localidad,codigo_postal, entrega, fecha_entrega,horario_atencion  from medicos INNER JOIN especialidades using(id_especialidad) INNER JOIN medicos_control using(id_Medico) INNER JOIN control  using(id_control) INNER JOIN localidades using(id_localidad) where mes='" + mes + "' and año=" + año + " order by(id_control)";
            Statement st4 = cn.createStatement();
            ResultSet rs4 = st4.executeQuery(ssql);

            while (rs4.next()) {
                //JOptionPane.showMessageDialog(null,rs4.getString("institucion") );
                institucion = rs4.getString("institucion");
                if (institucion == null) {
                    if (rs4.getString("horario_atencion") == null) {
                        tipos = new Sobres(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), rs4.getString("cantidad"), rs4.getString("desdex4"), rs4.getString("hastax4"), "", rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), mes, año, String.valueOf(sobre), rs4.getString("entrega"), rs4.getString("fecha_entrega"), "");
                        Resultado_Sobre.add(tipos);
                    } else {
                        tipos = new Sobres(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), rs4.getString("cantidad"), rs4.getString("desdex4"), rs4.getString("hastax4"), "", rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), mes, año, String.valueOf(sobre), rs4.getString("entrega"), rs4.getString("fecha_entrega"), rs4.getString("horario_atencion"));
                        Resultado_Sobre.add(tipos);
                    }
                } else {
                    if (rs4.getString("horario_atencion") == null) {
                        tipos = new Sobres(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), rs4.getString("cantidad"), rs4.getString("desdex4"), rs4.getString("hastax4"), rs4.getString("institucion"), rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), mes, año, String.valueOf(sobre), rs4.getString("entrega"), rs4.getString("fecha_entrega"), "");
                        Resultado_Sobre.add(tipos);
                    } else {
                        tipos = new Sobres(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), rs4.getString("cantidad"), rs4.getString("desdex4"), rs4.getString("hastax4"), rs4.getString("institucion"), rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), mes, año, String.valueOf(sobre), rs4.getString("entrega"), rs4.getString("fecha_entrega"), rs4.getString("horario_atencion"));
                        Resultado_Sobre.add(tipos);
                    }

                }
                sobre++;
            }
            try {
                JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Sobres_1.jasper"));
                JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, null, new JRBeanCollectionDataSource(Resultado_Sobre));
                JasperPrintManager.printReport(jPrint, true);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);

            }

        } catch (SQLException ex) {
            Logger.getLogger(MainPami.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnmodmed3ActionPerformed

    private void btnmodmed4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodmed4ActionPerformed
        mes = (String) cbomes.getSelectedItem();
        año = (String) cboaño.getSelectedItem();
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn5 = mysql.Conectar();
        String sssql = "SELECT DISTINCT(mes), año from control where mes='" + mes + "' and año=" + año;
        Statement st5;
        String mes_num="";
        if (mes.equals("ENERO")) {
            mes_num = "01";
        }
        if (mes.equals("FEBRERO")) {
            mes_num = "02";
        }
        if (mes.equals("MARZO")) {
            mes_num = "03";
        }
        if (mes.equals("ABRIL")) {
            mes_num = "04";
        }
        if (mes.equals("MAYO")) {
            mes_num = "05";
        }
        if (mes.equals("JUNIO")) {
            mes_num = "06";
        }
        if (mes.equals("JULIO")) {
            mes_num = "07";
        }
        if (mes.equals("AGOSTO")) {
            mes_num = "08";
        }
        if (mes.equals("SEPTIEMBRE")) {
            mes_num = "09";
        }
        if (mes.equals("OCTUBRE")) {
            mes_num = "10";
        }
        if (mes.equals("NOVIEMBRE")) {
            mes_num = "11";
        }
        if (mes.equals("DICIEMBRE")) {
            mes_num = "12";
        }

        try {
            st5 = cn5.createStatement();
            ResultSet rs5 = st5.executeQuery(sssql);

            if (rs5.next() && (cbomedico.getSelectedIndex() == 0 && cbomedico.getSelectedIndex() != 0)) {
                JOptionPane.showMessageDialog(null, "Los bonos ya fueron Generados para ese periodo");
            } else {
                JOptionPane.showMessageDialog(null, " Va a generar las Ordenes de Pami");

                if (cbomedico.getSelectedIndex() != 0) {
                    int i = 0;
                    int n = tabla.getRowCount();
                    int contador = 1, desde = 0, hasta = 0, cont = 0, recorre_matriz = 0, indice = 0;
                    String Matriz_bono1[][] = new String[4][9616];
                    String Matriz_bono2[][] = new String[4][9616];
                    String Matriz_bono3[][] = new String[4][9616];
                    String Matriz_bono4[][] = new String[4][1250];
                    String Matriz_bono5[][] = new String[4][1250];
                    String Matriz_bono6[][] = new String[4][1250];
                    LinkedList<camposorden_3> Resultado_final = new LinkedList<camposorden_3>();
                    camposorden_3 tipos = null;
                    Resultado_final.clear();
                    Connection cn = mysql.Conectar();
                    String sql = "INSERT INTO control (Año, Mes, Cantidad, Observaciones, Entrega, cantidadx4, desdex4, hastax4, estado_control, direccion_entrega, capita_entrega) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?) ";
                    String Activo;
                    while (i < n) {
                        id_especialidad = tabla.getValueAt(i, 4).toString();
                        capita = Integer.valueOf(tabla.getValueAt(i, 3).toString());
                        capita = (int) (capita * 0.2);

                        if (id_especialidad.equals("GENERALISTA")) {
                            if (Integer.valueOf(tabla.getValueAt(i, 3).toString()) > 100) {
                                cantidad = (int) (capita * 2 / 3 + capita * 1 / 6 + 1);
                            }
                            if (cantidad >= 50 && Integer.valueOf(tabla.getValueAt(i, 3).toString()) > 100) {

                                cantidad = (int) Math.round(cantidad * 0.8) + 1;
                                ////////////////bonos de 3
                                cantidad = (int) Math.round(cantidad * 4 / 3);
                            } else {
                                if (cantidad > 10 && cantidad <= 49) {
                                    ////////////////bonos de 3
                                    cantidad = Math.round(cantidad * 4 / 3);
                                    cantidad = (int) (cantidad + 1);
                                } else {
                                    cantidad = 10;
                                }
                            }
                        } else {
                            cantidad = (int) ((capita * 0.75) + 1);
                            if (cantidad > 10 && Integer.valueOf(tabla.getValueAt(i, 3).toString()) > 100) {
                                ////////////////bonos de 3
                                cantidad = (int) Math.round(cantidad * 0.7) + 1;
                                cantidad = (int) (Math.round(cantidad * 4 / 3) + 1);

                            } else {
                                if (cantidad > 10 && cantidad <= 49) {
                                    ////////////////bonos de 3
                                    cantidad = Math.round(cantidad * 4 / 3);
                                    cantidad = (int) (cantidad + 1);
                                } else {
                                    cantidad = 10;
                                    ////////////////bonos de 3
                                    // cantidad = Math.round(cantidad * 4 / 3);
                                    //cantidad = cantidad + 1;
                                }
                            }
                        }
                        idmedico = Integer.valueOf(tabla.getValueAt(i, 0).toString());
                        desde = contador;
                        hasta = contador + cantidad - 1;
                        contador = hasta;
                        try {
                            PreparedStatement pst = cn.prepareStatement(sql);
                            pst.setString(1, año);
                            pst.setString(2, mes);
                            pst.setInt(3, cantidad);
                            pst.setString(4, txtobs.getText());
                            pst.setString(5, "PRIMERA ENTREGA");
                            pst.setInt(6, cantidad);
                            pst.setInt(7, desde);
                            pst.setInt(8, hasta);
                            pst.setInt(9, Integer.valueOf("1"));
                            pst.setString(10, tabla.getValueAt(i, 2).toString());
                            pst.setInt(11, capita);
                            int m = pst.executeUpdate();
                            int cuenta = 0;
                            if (m > 0) {
                                String ssql = "SELECT MAX(id_Control) AS id_Control FROM control";
                                Statement st2 = cn.createStatement();
                                ResultSet rs = st2.executeQuery(ssql);
                                rs.last();
                                idcontrol = rs.getInt("id_Control");
                                String isql = "INSERT INTO medicos_control (id_Medico, id_Control) VALUES (" + idmedico + ", " + idcontrol + ")";
                                PreparedStatement pst3 = cn.prepareStatement(isql);
                                m = pst3.executeUpdate();
                                cont = desde;
                                if (m > 0) {
                                    while (cont <= hasta) {
                                        if (id_especialidad.equals("GENERALISTA") && capita > 0) {
                                            //va hasta la hoja predeterminada 8800
                                            //va hasta la hoja predeterminada 9616
                                            if (cont <= 9616) {
                                                indice = cont - 1;
                                                Matriz_bono1[0][indice] = tabla.getValueAt(i, 1).toString() + "-MP: " + tabla.getValueAt(i, 10).toString();
                                                Matriz_bono1[1][indice] = completarceros(String.valueOf(cont), 5);
                                                Matriz_bono1[2][indice] = "";
                                                Matriz_bono1[3][indice] = "G" + completarceros(String.valueOf(cont), 5);
                                            }
                                            //cont va 8801 a 8800*2
                                            //9616  19232
                                            if (cont >= 9617 && cont <= 19232) {
                                                cuenta = cont;
                                                //9501
                                                indice = cont - 9617;
                                                if (n == (i + 1)) {
                                                    //19000
                                                    while (cuenta <= 19232) {
                                                        Matriz_bono2[0][indice] = "";
                                                        Matriz_bono2[1][indice] = completarceros(String.valueOf(cuenta), 5);
                                                        Matriz_bono2[2][indice] = "";
                                                        Matriz_bono2[3][indice] = null;
                                                        cuenta++;
                                                        indice++;
                                                    }
                                                }
                                                //19000
                                                if (cuenta <= 19232) {
                                                    Matriz_bono2[0][indice] = tabla.getValueAt(i, 1).toString() + "-MP: " + tabla.getValueAt(i, 10).toString();
                                                    Matriz_bono2[1][indice] = completarceros(String.valueOf(cont), 5);
                                                    Matriz_bono2[2][indice] = "";
                                                    Matriz_bono2[3][indice] = "G" + completarceros(String.valueOf(cont), 5);
                                                }

                                            }
                                            //19001 
                                            if (cont >= 19233 || cuenta == 19233) {
                                                //19001
                                                indice = cont - 19233;
                                                if (n == (i + 1)) {
                                                    if (cuenta < 1) {
                                                        cuenta = cont;
                                                        //19001
                                                        indice = cuenta - 19233;
                                                        //28500
                                                        while (cuenta <= 28848) {
                                                            Matriz_bono3[0][indice] = "";
                                                            Matriz_bono3[1][indice] = completarceros(String.valueOf(cuenta), 5);
                                                            Matriz_bono3[2][indice] = "";
                                                            Matriz_bono3[3][indice] = null;
                                                            cuenta++;
                                                            indice++;
                                                        }
                                                    } else {
                                                        //19001
                                                        indice = cuenta - 19233;
                                                        //28500
                                                        while (cuenta <= 28848) {
                                                            Matriz_bono3[0][indice] = "";
                                                            Matriz_bono3[1][indice] = completarceros(String.valueOf(cuenta), 5);
                                                            Matriz_bono3[2][indice] = "";
                                                            Matriz_bono3[3][indice] = null;
                                                            cuenta++;
                                                            indice++;
                                                        }
                                                    }
                                                }
                                                //28500
                                                if (cuenta <= 28848) {
                                                    Matriz_bono3[0][indice] = tabla.getValueAt(i, 1).toString() + "-MP: " + tabla.getValueAt(i, 10).toString();
                                                    Matriz_bono3[1][indice] = completarceros(String.valueOf(cont), 5);
                                                    Matriz_bono3[2][indice] = "";
                                                    Matriz_bono3[3][indice] = "G" + completarceros(String.valueOf(cont), 5);
                                                }
                                            }
                                        }
                                        if (!id_especialidad.equals("GENERALISTA") && capita > 0) {
                                            int largo = 0;
                                            //1500
                                            if (cont <= 1250) {
                                                indice = cont - 1;
                                                largo = tabla.getValueAt(i, 10).toString().length();
                                                Matriz_bono4[0][indice] = tabla.getValueAt(i, 1).toString() + "-MP: " + tabla.getValueAt(i, 10).toString();
                                                Matriz_bono4[1][indice] = mes_num + año.substring(2, 4) + tabla.getValueAt(i, 10).toString().substring(largo - 2, largo) + completarceros(String.valueOf(cont), 5);
                                                Matriz_bono4[2][indice] = id_especialidad;
                                                Matriz_bono4[3][indice] = mes_num + año.substring(2, 4) + tabla.getValueAt(i, 10).toString().substring(largo - 2, largo) + completarceros(String.valueOf(cont), 5);
                                            }
                                            // cont >= 1501 && cont <= 3000
                                            if (cont >= 1251 && cont <= 2500) {
                                                cuenta = cont;
                                                indice = cont - 1251;
                                                if (n == (i + 1)) {
                                                    // JOptionPane.showMessageDialog(null, "sad" + n);
                                                    //3000
                                                    while (cuenta <= 2500) {
                                                        //  JOptionPane.showMessageDialog(null, cont);
                                                        // JOptionPane.showMessageDialog(null, cuenta);
                                                        //  JOptionPane.showMessageDialog(null, indice);
                                                        Matriz_bono5[0][indice] = "";
                                                        Matriz_bono5[1][indice] = "    " + completarceros(String.valueOf(cuenta), 5);
                                                        Matriz_bono5[2][indice] = "";
                                                        Matriz_bono5[3][indice] = null;
                                                        cuenta++;
                                                        indice++;
                                                    }
                                                }//3000
                                                if (cuenta <= 2500) {
                                                    largo = tabla.getValueAt(i, 10).toString().length();
                                                    Matriz_bono5[0][indice] = tabla.getValueAt(i, 1).toString() + "-MP: " + tabla.getValueAt(i, 10).toString();
                                                    Matriz_bono5[1][indice] = mes_num + año.substring(2, 4) + tabla.getValueAt(i, 10).toString().substring(largo - 2, largo) + completarceros(String.valueOf(cont), 5);
                                                    Matriz_bono5[2][indice] = id_especialidad;
                                                    Matriz_bono5[3][indice] = mes_num + año.substring(2, 4) + tabla.getValueAt(i, 10).toString().substring(largo - 2, largo) + completarceros(String.valueOf(cont), 5);
                                                }

                                            }
                                            //if (cont >= 3001 || cuenta == 3001) {
                                            if (cont >= 2501 || cuenta == 2501) {
                                                //indice = cont - 3001;
                                                indice = cont - 2501;
                                                if (n == (i + 1)) {
                                                    if (cuenta < 1) {
                                                        cuenta = cont;
                                                        //indice = cuenta - 3001;
                                                        indice = cuenta - 2501;
                                                        //while (cuenta <= 4500) {
                                                        while (cuenta <= 3750) {
                                                            Matriz_bono6[0][indice] = "";
                                                            Matriz_bono6[1][indice] = "    " + completarceros(String.valueOf(cuenta), 5);
                                                            Matriz_bono6[2][indice] = "";
                                                            Matriz_bono6[3][indice] = null;
                                                            cuenta++;
                                                            indice++;
                                                        }
                                                    } else {
                                                        //indice = cuenta - 3001;
                                                        indice = cuenta - 2501;
                                                        //while (cuenta <= 4500) {
                                                        while (cuenta <= 2750) {
                                                            Matriz_bono6[0][indice] = "";
                                                            Matriz_bono6[1][indice] = "    " + completarceros(String.valueOf(cuenta), 5);
                                                            Matriz_bono6[2][indice] = "";
                                                            Matriz_bono6[3][indice] = null;
                                                            cuenta++;
                                                            indice++;
                                                        }
                                                    }
                                                }
                                                //if (cuenta <= 4500) {
                                                if (cuenta <= 2750) {
                                                    largo = tabla.getValueAt(i, 10).toString().length();
                                                    Matriz_bono6[0][indice] = tabla.getValueAt(i, 1).toString() + "-MP: " + tabla.getValueAt(i, 10).toString();
                                                    Matriz_bono6[1][indice] = mes_num + año.substring(2, 4) + tabla.getValueAt(i, 10).toString().substring(largo - 2, largo) + completarceros(String.valueOf(cont), 5);
                                                    Matriz_bono6[2][indice] = id_especialidad;
                                                    Matriz_bono6[3][indice] = mes_num + año.substring(2, 4) + tabla.getValueAt(i, 10).toString().substring(largo - 2, largo) + completarceros(String.valueOf(cont), 5);
                                                }
                                            }
                                        }
                                        cont++;
                                    }
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Error al cargar el médico");
                            }
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, ex);
                        }
                        contador++;
                        //}
                        i++;

                    }
                    recorre_matriz = 0;
                    if (id_especialidad.equals("GENERALISTA")) {
                        while (recorre_matriz < 9616) {
                            //JOptionPane.showMessageDialog(null, Matriz_bono1[0][recorre_matriz]);
                            //JOptionPane.showMessageDialog(null, Matriz_bono2[0][recorre_matriz]);
                            tipos = new camposorden_3(Matriz_bono1[0][recorre_matriz], Matriz_bono1[1][recorre_matriz], Matriz_bono1[2][recorre_matriz], Matriz_bono1[3][recorre_matriz], Matriz_bono2[0][recorre_matriz], Matriz_bono2[1][recorre_matriz], Matriz_bono2[2][recorre_matriz], Matriz_bono2[3][recorre_matriz], Matriz_bono3[0][recorre_matriz], Matriz_bono3[1][recorre_matriz], Matriz_bono3[2][recorre_matriz], Matriz_bono3[3][recorre_matriz], fecha_vencimiento);
                            recorre_matriz++;
                            Resultado_final.add(tipos);
                        }

                        try {
                            JasperReport reporte9 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenx3-cabe_1.jasper"));
                            //tipo = new Ordenes(numero_orden, nombre, direccion, localidad, especialidad1, codigo_postal, desdex4, desdex2, hastax4, hastax2);
                            JasperPrint jPrint = JasperFillManager.fillReport(reporte9, null, new JRBeanCollectionDataSource(Resultado_final));
                            JasperPrintManager.printReport(jPrint, true);
                            //JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + "Bonos Generalistas" + "-" + mes +"-"+ año+ ".pdf");
                            JOptionPane.showMessageDialog(null, "Proceso terminó con exito...");
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                    } else {

                        while (recorre_matriz < 1250) {
                            //JOptionPane.showMessageDialog(null, Matriz_bono1[0][recorre_matriz]);
                            //JOptionPane.showMessageDialog(null, Matriz_bono2[0][recorre_matriz]);
                            tipos = new camposorden_3(Matriz_bono4[0][recorre_matriz], Matriz_bono4[1][recorre_matriz], Matriz_bono4[2][recorre_matriz], Matriz_bono4[3][recorre_matriz], Matriz_bono5[0][recorre_matriz], Matriz_bono5[1][recorre_matriz], Matriz_bono5[2][recorre_matriz], Matriz_bono5[3][recorre_matriz], Matriz_bono6[0][recorre_matriz], Matriz_bono6[1][recorre_matriz], Matriz_bono6[2][recorre_matriz], Matriz_bono6[3][recorre_matriz], fecha_vencimiento);
                            recorre_matriz++;
                            Resultado_final.add(tipos);
                        }
                        try {
                            JasperReport reporte9 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenx3-esp_1.jasper"));
                            //tipo = new Ordenes(numero_orden, nombre, direccion, localidad, especialidad1, codigo_postal, desdex4, desdex2, hastax4, hastax2);
                            JasperPrint jPrint = JasperFillManager.fillReport(reporte9, null, new JRBeanCollectionDataSource(Resultado_final));
                            JasperPrintManager.printReport(jPrint, true);
                            //JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + "Bonos Especialistas" + "-" + mes + "-" + año + ".pdf");
                            JOptionPane.showMessageDialog(null, "Proceso terminó con exito...");
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "No puede mandar a imprimir ambos tipos de medicos. ");
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_btnmodmed4ActionPerformed

    private void btnmodmed5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodmed5ActionPerformed
        int i = 0;
        int n = tabla.getRowCount();
        int contador = 1, desde = 0, hasta = 0, cont = 0, recorre_matriz = 0, indice = 0;
        String Matriz_bono1[][] = new String[4][9616];
        String Matriz_bono2[][] = new String[4][9616];
        String Matriz_bono3[][] = new String[4][9616];
        String Matriz_bono4[][] = new String[4][5800];
        String Matriz_bono5[][] = new String[4][5800];
        String Matriz_bono6[][] = new String[4][5800];
        LinkedList<camposorden_3> Resultado_final = new LinkedList<camposorden_3>();
        camposorden_3 tipos = null;
        Resultado_final.clear();
        int total = Integer.valueOf(txtcantidad.getText()), total1 = 0, total2 = 0, numero_bonog, numero_bonoe;

        numero_bonog = 28848;
        numero_bonoe = 17400;

        total1 = total / 3;
        total2 = total1 * 2;
        total++;

        mes = (String) cbomes.getSelectedItem();
        año = (String) cboaño.getSelectedItem();
        especialidad = (String) cbomedico.getSelectedItem();

        if (especialidad.equals("GENERALISTA")) {

            if (Integer.valueOf(txtdesdex4.getText()) != 0) {
                numero_bonog = Integer.valueOf(txtdesdex4.getText());
            }

            while (cont < total1) {
                Matriz_bono1[0][indice] = "";
                Matriz_bono1[1][indice] = completarceros(String.valueOf(numero_bonog), 5);
                Matriz_bono1[2][indice] = "";
                Matriz_bono1[3][indice] = null;
                numero_bonog++;
                cont++;
                indice++;
            }
            indice = 0;
            while (cont >= total1 && cont < total2) {
                Matriz_bono2[0][indice] = "";
                Matriz_bono2[1][indice] = completarceros(String.valueOf(numero_bonog), 5);
                Matriz_bono2[2][indice] = "";
                Matriz_bono2[3][indice] = null;
                numero_bonog++;
                cont++;
                indice++;
            }
            indice = 0;
            while (cont >= total2 && cont < total) {
                Matriz_bono3[0][indice] = "";
                Matriz_bono3[1][indice] = completarceros(String.valueOf(numero_bonog), 5);
                Matriz_bono3[2][indice] = "";
                Matriz_bono3[3][indice] = null;
                numero_bonog++;
                cont++;
                indice++;
            }
            indice = 0;
        } else {
            if (Integer.valueOf(txtdesdex4.getText()) != 0) {
                numero_bonoe = Integer.valueOf(txtdesdex4.getText());
            }
            while (cont < total1) {

                Matriz_bono1[0][indice] = "";
                Matriz_bono1[1][indice] = completarceros(String.valueOf(numero_bonoe), 5);
                Matriz_bono1[2][indice] = "";
                Matriz_bono1[3][indice] = null;
                numero_bonoe++;
                cont++;
                indice++;
            }
            indice = 0;
            while (cont >= total1 && cont < total2) {
                Matriz_bono2[0][indice] = "";
                Matriz_bono2[1][indice] = completarceros(String.valueOf(numero_bonoe), 5);
                Matriz_bono2[2][indice] = "";
                Matriz_bono2[3][indice] = null;
                numero_bonoe++;
                cont++;
                indice++;
            }
            indice = 0;
            while (cont >= total2 && cont < total) {
                Matriz_bono3[0][indice] = "";
                Matriz_bono3[1][indice] = completarceros(String.valueOf(numero_bonoe), 5);
                Matriz_bono3[2][indice] = "";
                Matriz_bono3[3][indice] = null;
                numero_bonoe++;
                cont++;
                indice++;
            }

        }
        recorre_matriz = 0;

        if (especialidad.equals("GENERALISTA")) {
            while (recorre_matriz < total1) {
                tipos = new camposorden_3(Matriz_bono1[0][recorre_matriz], Matriz_bono1[1][recorre_matriz], Matriz_bono1[2][recorre_matriz], Matriz_bono1[3][recorre_matriz], Matriz_bono2[0][recorre_matriz], Matriz_bono2[1][recorre_matriz], Matriz_bono2[2][recorre_matriz], Matriz_bono2[3][recorre_matriz], Matriz_bono3[0][recorre_matriz], Matriz_bono3[1][recorre_matriz], Matriz_bono3[2][recorre_matriz], Matriz_bono3[3][recorre_matriz], fecha_vencimiento);
                recorre_matriz++;
                Resultado_final.add(tipos);
            }

            try {

                JasperReport reporte9 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenx3-cabe_1.jasper"));
                JasperPrint jPrint = JasperFillManager.fillReport(reporte9, null, new JRBeanCollectionDataSource(Resultado_final));
                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + "Bonos en blanco Generalistas" + "-" + mes + "-" + año + ".pdf");
                //  JasperPrintManager.printReport(jPrint, true);
                JOptionPane.showMessageDialog(null, "Los Sobres se Generaron Correctamente");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else {
            while (recorre_matriz <= total1) {
                tipos = new camposorden_3(Matriz_bono1[0][recorre_matriz], Matriz_bono1[1][recorre_matriz], Matriz_bono1[2][recorre_matriz], Matriz_bono1[3][recorre_matriz], Matriz_bono2[0][recorre_matriz], Matriz_bono2[1][recorre_matriz], Matriz_bono2[2][recorre_matriz], Matriz_bono2[3][recorre_matriz], Matriz_bono3[0][recorre_matriz], Matriz_bono3[1][recorre_matriz], Matriz_bono3[2][recorre_matriz], Matriz_bono3[3][recorre_matriz], fecha_vencimiento);
                recorre_matriz++;
                Resultado_final.add(tipos);
            }

            try {
                JasperReport reporte9 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenx3-esp_1.jasper"));
                JasperPrint jPrint = JasperFillManager.fillReport(reporte9, null, new JRBeanCollectionDataSource(Resultado_final));
                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Descargas-CBT\\" + "Bonos en blanco Especialistas" + "-" + mes + "-" + año + ".pdf");
                // JasperPrintManager.printReport(jPrint, true);
                JOptionPane.showMessageDialog(null, "Los Sobres se Generaron Correctamente");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btnmodmed5ActionPerformed

    private void btnmodmed6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodmed6ActionPerformed
        dispose();
    }//GEN-LAST:event_btnmodmed6ActionPerformed

    private void txtcantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcantidadActionPerformed

    private void txtdesdex4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdesdex4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdesdex4ActionPerformed

    private void txthastax4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txthastax4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txthastax4ActionPerformed

    private void cboentregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboentregaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboentregaActionPerformed

    private void cboañoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboañoActionPerformed
        año = cboaño.getSelectedItem().toString();
    }//GEN-LAST:event_cboañoActionPerformed

    private void txtdesde2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdesde2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdesde2ActionPerformed

    private void txtdesde2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdesde2KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdesde2KeyReleased

    private void txtdesde2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdesde2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdesde2KeyPressed

    private void txthasta2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txthasta2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txthasta2ActionPerformed

    private void btnaltamed1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaltamed1ActionPerformed
        AgregarZona.modifica = 1;
        new AgregarZona(this, true).setVisible(true);
    }//GEN-LAST:event_btnaltamed1ActionPerformed

    private void btnaltamed2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaltamed2ActionPerformed
        AgregarZona.modifica = 0;
        new AgregarZona(this, true).setVisible(true);
    }//GEN-LAST:event_btnaltamed2ActionPerformed

    private void btntablaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntablaActionPerformed
        try {
            //Mensaje de encabezado
            MessageFormat encabezado = new MessageFormat("Listado");
            //Mensaje en el pie de pagina
            MessageFormat pie = new MessageFormat("");
            //Imprimir JTable
            tabla.print(JTable.PrintMode.FIT_WIDTH, encabezado, pie);
        } catch (PrinterException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }//GEN-LAST:event_btntablaActionPerformed

    private void cbozonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbozonaActionPerformed
        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + cbozona.getSelectedItem().toString() + ".*"));
        tabla.setRowSorter(sorter);
    }//GEN-LAST:event_cbozonaActionPerformed

    private void btnsobresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsobresActionPerformed
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        //Declaro variables privadas 
        mes = (String) cbomes.getSelectedItem();
        año = (String) cboaño.getSelectedItem();
        int sobre = 1;
        //Declaro una lista desde ordenes 
        String institucion;
        LinkedList<Sobres> Resultado_Sobre = new LinkedList<Sobres>();
        Sobres tipos = null;
        Resultado_Sobre.clear();

        try {
            ///// Traigo los datos de control, medico, localidad y medico control
            String ssql = "SELECT especialidad, nombre,direccion,institucion,nombre_localidad,codigo_postal, horario_atencion \n"
                    + "from medicos \n"
                    + "INNER JOIN especialidades using(id_especialidad) \n"
                    + "INNER JOIN localidades using(id_localidad) \n"
                    + "where id_especialidad=1 and estado='ACTIVO' \n"
                    + "order by nombre_localidad,direccion";
            Statement st4 = cn.createStatement();
            ResultSet rs4 = st4.executeQuery(ssql);
            while (rs4.next()) {
                institucion = rs4.getString("institucion");
                if (institucion == null) {
                    if (rs4.getString("horario_atencion") == null) {
                        tipos = new Sobres(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), "", "", "", "", rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), mes, año, String.valueOf(sobre), "", "", "");
                        Resultado_Sobre.add(tipos);
                    } else {
                        tipos = new Sobres(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), "", "", "", "", rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), mes, año, String.valueOf(sobre), "", "", rs4.getString("horario_atencion"));
                        Resultado_Sobre.add(tipos);
                    }
                } else {
                    if (rs4.getString("horario_atencion") == null) {
                        tipos = new Sobres(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), "", "", "", rs4.getString("institucion"), rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), mes, año, String.valueOf(sobre), "", "", "");
                        Resultado_Sobre.add(tipos);
                    } else {
                        tipos = new Sobres(rs4.getString("especialidad"), rs4.getString("nombre"), rs4.getString("direccion"), "", "", "", rs4.getString("institucion"), rs4.getString("nombre_localidad"), rs4.getString("codigo_postal"), mes, año, String.valueOf(sobre), "", "", rs4.getString("horario_atencion"));
                        Resultado_Sobre.add(tipos);
                    }
                }
                sobre++;
            }
            try {
                JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Sobres.jasper"));
                JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, null, new JRBeanCollectionDataSource(Resultado_Sobre));
                JasperPrintManager.printReport(jPrint, true);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);

            }

        } catch (SQLException ex) {
            Logger.getLogger(MainPami.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnsobresActionPerformed

    void cargarcombolocalidad() {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        String sql = "select nombre_localidad from localidades order by nombre_localidad";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            cbolocalidad.addItem("Localidad");
            while (rs.next()) {
                cbolocalidad.addItem(rs.getString(1));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarcombozona() {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        String sql = "select nombre from zonas order by nombre";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            cbozona.addItem("...");
            while (rs.next()) {
                cbozona.addItem(rs.getString(1));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarcombodireccion() {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        /*String sql2 = "select DISTINCT(Direccion) from medicos ";
         try {
         Statement st2 = cn.createStatement();
         ResultSet rs2 = st2.executeQuery(sql2);
         cbodireccion.addItem("Domicilio");
         while (rs2.next()) {
         cbodireccion.addItem(rs2.getString(1));
         }
         } catch (Exception e) {
         JOptionPane.showMessageDialog(null, e);
         }*/
        int i = 0;
        //practica = new String[500000];
        // preciopractica = new String[500000];
        //contadorj = 0;
        //ConexionMySQLLocal cc = new ConexionMySQLLocal();
        // Connection cn = cc.Conectar();
        try {
            Statement St = cn.createStatement();
            ResultSet Rs = St.executeQuery("select DISTINCT(Direccion) from medicos");
            while (Rs.next()) {
                textAutoAcompleter.addItem(Rs.getString(1));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        textAutoAcompleter.setMode(0); // infijo
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter.setCaseSensitive(false);

    }

    void borrarpractica() {
        textAutoAcompleter.removeAllItems();
    }

    public void filtarlocalidad(String valor) {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();

        String[] Titulo = {"Codigo", "Nombre", "Dirección", "Cápita", "Especialidad", "Localidad", "CP", "id_Especialidad", "Estado", "Observación", "Matricula", "Zona"};
        Object[] Registros = new Object[12];

        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        String sql = "SELECT id_Medico, medicos.Nombre, "
                + "Direccion, Capita, Especialidad,localidades.nombre_localidad,localidades.codigo_postal, id_Especialidad, Estado, descripcion, Matricula_Prof,zonas.nombre "
                + "FROM medicos INNER JOIN especialidades USING (id_Especialidad)  "
                + "INNER JOIN localidades USING (id_localidad) LEFT JOIN zonas ON zonas.id = medicos.id_zona "
                + "WHERE CONCAT(nombre_localidad)"
                + "LIKE '%" + valor + "%'";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getInt(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getInt(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                Registros[9] = rs.getString(10);
                Registros[10] = rs.getString(11);
                Registros[11] = rs.getString(12);
                model.addRow(Registros);

            }
            tabla.setModel(model);
            tabla.getColumnModel().getColumn(7).setMaxWidth(0);
            tabla.getColumnModel().getColumn(7).setMinWidth(0);
            tabla.getColumnModel().getColumn(7).setPreferredWidth(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        tabla.setModel(model);
        tabla.setAutoCreateRowSorter(true);
    }

    public void filtardomicilio(String valor) {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();

        String[] Titulo = {"Codigo", "Nombre", "Dirección", "Cápita", "Especialidad", "Localidad", "CP", "id_Especialidad", "Estado", "Observación", "Matricula", "Zona"};
        Object[] Registros = new Object[12];

        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT id_Medico, medicos.Nombre,Direccion, Capita, Especialidad,localidades.nombre_localidad,localidades.codigo_postal, id_Especialidad, Estado, descripcion, Matricula_Prof,zonas.nombre  FROM medicos INNER JOIN especialidades USING (id_Especialidad) INNER JOIN localidades USING (id_localidad) LEFT JOIN zonas ON zonas.id = medicos.id_zona WHERE nombre_localidad ='" + cbolocalidad.getSelectedItem().toString() + "' and CONCAT(Direccion) LIKE '" + valor + "'";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getInt(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getInt(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                Registros[9] = rs.getString(10);
                Registros[10] = rs.getString(11);
                Registros[11] = rs.getString(12);
                model.addRow(Registros);

            }
            tabla.setModel(model);
            tabla.getColumnModel().getColumn(7).setMaxWidth(0);
            tabla.getColumnModel().getColumn(7).setMinWidth(0);
            tabla.getColumnModel().getColumn(7).setPreferredWidth(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        tabla.setModel(model);
        tabla.setAutoCreateRowSorter(true);
    }

    public void cargartabla(String valor) {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();

        String[] Titulo = {"Codigo", "Nombre", "Dirección", "Cápita", "Especialidad", "Localidad", "CP", "id_Especialidad", "Estado", "Observación", "Matricula", "Zona"};
        Object[] Registros = new Object[12];

        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        String sql = "SELECT id_Medico, medicos.Nombre, "
                + "Direccion, Capita, Especialidad,localidades.nombre_localidad,localidades.codigo_postal, id_Especialidad, Estado, descripcion, Matricula_Prof,zonas.nombre "
                + "FROM medicos INNER JOIN especialidades USING (id_Especialidad)  "
                + "INNER JOIN localidades USING (id_localidad) LEFT JOIN zonas ON zonas.id = medicos.id_zona  "
                + "WHERE CONCAT(id_Medico, ' ',medicos.Nombre)"
                + "LIKE '%" + valor + "%'";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getInt(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getInt(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                Registros[9] = rs.getString(10);
                Registros[10] = rs.getString(11);
                Registros[11] = rs.getString(12);
                model.addRow(Registros);

            }
            tabla.setModel(model);
            tabla
                    .setDefaultRenderer(Object.class, new ColoreaTabla());
            tabla.getColumnModel()
                    .getColumn(7).setMaxWidth(0);
            tabla.getColumnModel()
                    .getColumn(7).setMinWidth(0);
            tabla.getColumnModel()
                    .getColumn(7).setPreferredWidth(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        tabla.setModel(model);
        tabla.setAutoCreateRowSorter(true);
    }

    void dobleclick() {
        tabla.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    Entregas.id_medico = tabla.getValueAt(tabla.getSelectedRow(), 0).toString();

                    new Entregas(null, true).setVisible(true);
                }
            }
        });
    }

    public void buscargeneralistas(String valor) {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();

        String[] Titulo = {"Codigo", "Nombre", "Dirección", "Cápita", "Especialidad", "Localidad", "CP", "id_Especialidad", "Estado", "Observación", "Matricula", "Zona"};
        Object[] Registros = new Object[12];

        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        String sql = "SELECT id_Medico, medicos.Nombre, "
                + "Direccion, Capita, Especialidad,localidades.nombre_localidad,localidades.codigo_postal, id_Especialidad, Estado, descripcion, Matricula_Prof,zonas.nombre "
                + "FROM medicos INNER JOIN especialidades USING (id_Especialidad)  "
                + "INNER JOIN localidades USING (id_localidad) LEFT JOIN zonas ON zonas.id = medicos.id_zona "
                + "WHERE id_Especialidad=" + valor;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                if (rs.getString(9).equals("ACTIVO")) {
                    Registros[0] = rs.getInt(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    Registros[3] = rs.getInt(4);
                    Registros[4] = rs.getString(5);
                    Registros[5] = rs.getString(6);
                    Registros[6] = rs.getString(7);
                    Registros[7] = rs.getString(8);
                    Registros[8] = rs.getString(9);
                    Registros[9] = rs.getString(10);
                    Registros[10] = rs.getString(11);
                    Registros[11] = rs.getString(12);
                    model.addRow(Registros);
                }
            }
            tabla.setModel(model);
            tabla.getColumnModel().getColumn(7).setMaxWidth(0);
            tabla.getColumnModel().getColumn(7).setMinWidth(0);
            tabla.getColumnModel().getColumn(7).setPreferredWidth(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        tabla.setModel(model);
        tabla.setAutoCreateRowSorter(true);
    }

    public void buscarespecialidad(String valor) {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();

        String[] Titulo = {"Codigo", "Nombre", "Dirección", "Cápita", "Especialidad", "Localidad", "CP", "id_Especialidad", "Estado", "Observación", "Matricula", "Zona"};
        Object[] Registros = new Object[12];

        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        String sql = "SELECT id_Medico, medicos.Nombre, "
                + "Direccion, Capita, Especialidad,localidades.nombre_localidad,localidades.codigo_postal, id_Especialidad, Estado, descripcion, Matricula_Prof,zonas.nombre "
                + "FROM medicos INNER JOIN especialidades USING (id_Especialidad) LEFT JOIN zonas ON zonas.id = medicos.id_zona "
                + "INNER JOIN localidades USING (id_localidad)  "
                + "WHERE id_Especialidad !=" + valor;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                if (rs.getString(9).equals("ACTIVO")) {
                    Registros[0] = rs.getInt(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    Registros[3] = rs.getInt(4);
                    Registros[4] = rs.getString(5);
                    Registros[5] = rs.getString(6);
                    Registros[6] = rs.getString(7);
                    Registros[7] = rs.getString(8);
                    Registros[8] = rs.getString(9);
                    Registros[9] = rs.getString(10);
                    Registros[10] = rs.getString(11);
                    Registros[11] = rs.getString(12);
                    model.addRow(Registros);
                }
            }
            tabla.setModel(model);
            tabla.getColumnModel().getColumn(7).setMaxWidth(0);
            tabla.getColumnModel().getColumn(7).setMinWidth(0);
            tabla.getColumnModel().getColumn(7).setPreferredWidth(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        tabla.setModel(model);
        tabla.setAutoCreateRowSorter(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaltaesp;
    private javax.swing.JButton btnaltamed;
    private javax.swing.JButton btnaltamed1;
    private javax.swing.JButton btnaltamed2;
    private javax.swing.JButton btnimprimetabla;
    private javax.swing.JButton btnmodesp;
    private javax.swing.JButton btnmodmed;
    private javax.swing.JButton btnmodmed1;
    private javax.swing.JButton btnmodmed3;
    private javax.swing.JButton btnmodmed4;
    private javax.swing.JButton btnmodmed5;
    private javax.swing.JButton btnmodmed6;
    private javax.swing.JButton btnsobres;
    private javax.swing.JButton btntabla;
    private javax.swing.JComboBox cboaño;
    private javax.swing.JComboBox cboentrega;
    private javax.swing.JComboBox cbolocalidad;
    private javax.swing.JComboBox cbomedico;
    private javax.swing.JComboBox cbomes;
    private javax.swing.JComboBox cbozona;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtcantidad;
    private javax.swing.JTextField txtcantidad2;
    private javax.swing.JTextField txtdesde2;
    private javax.swing.JTextField txtdesdex4;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtentrega;
    private javax.swing.JTextField txthasta2;
    private javax.swing.JTextField txthastax4;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextArea txtobs;
    private javax.swing.JTextField txtperiodo;
    private javax.swing.JTextField txtrecibido;
    private javax.swing.JTextField txttipo;
    // End of variables declaration//GEN-END:variables
}
