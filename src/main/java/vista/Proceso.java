package vista;

import controlador.ConexionMariaDB;
import controlador.NumberToLetterConverter;
import controlador.camposobrasocial;
import controlador.camposvalidacion;
import static vista.Periodo.fecha2;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class Proceso extends javax.swing.JDialog {

    HiloOrdenes hilo;
    private int id_ddjj;
    public static String periodo, fecha = "";

    public Proceso(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        iniciarSplash();
        hilo = new HiloOrdenes(progreso);
        hilo.start();
        hilo = null;
    }

    void cargarperiodo() {

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);

        fecha = formato.format(currentDate);

    }

    private void progresoStateChanged(javax.swing.event.ChangeEvent evt) {
        if (progreso.getValue() == 100) {
            this.dispose();
        }
    }

    
    
    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    public class HiloOrdenes extends Thread {

        JProgressBar progreso;

        public HiloOrdenes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;

        }

        public void run() {
            int cantidad = 100 / 800;
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab;

            ////////////////////////////////////////////
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            LinkedList<camposobrasocial> Resultados = new LinkedList<camposobrasocial>();
            LinkedList<camposvalidacion> Resultados2 = new LinkedList<camposvalidacion>();
            Resultados.clear();
            Resultados2.clear();
            String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, i = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0, pesos = 0, importetotal = 0, centavos = 0;

            String nombre_obra = "", cod_obra = "", matricula = "";

            try {
                Statement st3 = cn.createStatement();
                ResultSet rs3 = st3.executeQuery("SELECT id_colegiados,nombre_colegiado,matricula_colegiado,factura,direccion_laboratorio,localidad_laboratorio,periodo,estado_periodo FROM colegiados  WHERE factura=" + "'SI'");
                while (rs3.next()) {
                    if (Integer.valueOf(periodo) >= (rs3.getInt("periodo")) && false == (rs3.getBoolean("estado_periodo"))) {

                        pesos = 0;
                        band = 0;
                        ordenesgral = 0;
                        practicasgral = 0;
                        Resultados.clear();
                        bandera = 0;
                        colegiado = rs3.getInt("id_colegiados");
                        matricula = rs3.getString("matricula_colegiado");
                        nombre_colegiado = rs3.getString("nombre_colegiado");
                        domicilio_lab = rs3.getString("direccion_laboratorio");////
                        localidad_lab = rs3.getString("localidad_laboratorio");
                        /////////////////////////////////////////////////////
                        Statement st2 = cn.createStatement();
                        String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial,codigo_obrasocial,importeunidaddegasto_obrasocial,importeunidaddearancel_obrasocial FROM obrasocial";// WHERE id_obrasocial"               
                        ResultSet rs2 = st2.executeQuery(sql2);

                        while (rs2.next()) {
                            String sql = "SELECT * FROM ordenes";
                            Statement st = cn.createStatement();
                            ResultSet rs = st.executeQuery(sql);
                            rs.beforeFirst();
                            ordenes = 0;
                            practicas = 0;
                            total = 0.00;
                            while (rs.next()) {
                                if (rs.getString("id_obrasocial").equals(rs2.getString("id_obrasocial")) && true == rs.getBoolean("estado_orden") && colegiado == rs.getInt("id_colegiados") && periodo.equals(rs.getString("periodo"))) {
                                    nombre_obra = rs2.getString("razonsocial_obrasocial");
                                    cod_obra = rs2.getString("codigo_obrasocial");
                                    uni_gastos = rs2.getString("importeunidaddegasto_obrasocial");
                                    uni_Honorarios = rs2.getString("importeunidaddearancel_obrasocial");
                                    Statement st4 = cn.createStatement();
                                    String sql4 = "SELECT id_orden,precio_practica FROM detalle_ordenes WHERE id_orden=" + rs.getInt("id_orden");
                                    ResultSet rs4 = st4.executeQuery(sql4);
                                    rs4.beforeFirst();
                                    while (rs4.next()) {
                                        if (rs.getInt("id_orden") == rs4.getInt("id_orden")) {
                                            ////////////////cargo la matriz para imprimir//////////                                   
                                            practicas = practicas + 1;
                                            practicasgral = practicasgral + 1;
                                            total = total + rs4.getDouble("precio_practica");
                                        }
                                    }
                                    ordenes = ordenes + 1;
                                    ordenesgral = ordenesgral + 1;
                                    band = 1;

                                }
                            }
                            if (band == 1) {
                                camposobrasocial tipo;
                                tipo = new camposobrasocial(cod_obra, nombre_obra, periodo, uni_gastos, uni_Honorarios, String.valueOf(total), String.valueOf(practicas), String.valueOf(ordenes));
                                Resultados.add(tipo);
                                bandera = 1;
                                band = 0;
                                ////////////////////////declaracion jurada/////////////////////////
                                String sSQL = "INSERT INTO declaracion_jurada(matricula_colegiado,nombre_colegiado,fecha_declaracion,"
                                        + "cod_obra_social,obra_social,periodo_declaracion,uni_honorarios,"
                                        + "importe, practicas,ordenes)"
                                        + "VALUES(?,?,?,?,?,?,?,?,?,?)";
                                try {

                                    PreparedStatement pst = cn.prepareStatement(sSQL);
                                    pst.setString(1, matricula);
                                    pst.setString(2, nombre_colegiado);
                                    pst.setString(3, fecha);
                                    pst.setString(4, cod_obra);
                                    pst.setString(5, nombre_obra);
                                    pst.setInt(6, Integer.valueOf(periodo));
                                    pst.setString(7, uni_Honorarios);
                                    pst.setString(8, String.valueOf(Redondeardosdigitos(total)));
                                    pst.setString(9, String.valueOf(practicas));
                                    pst.setString(10, String.valueOf(ordenes));
                                    int n1 = pst.executeUpdate();
                                    if (n1 > 0) {
                                        cargarddjj();
                                        camposvalidacion tipo2;
                                        tipo2 = new camposvalidacion(completarceros(matricula, 5), completarceros(cod_obra, 5), periodo, nombre_obra, String.valueOf(total), completarceros(String.valueOf(practicas), 5), completarceros(String.valueOf(ordenes), 4), completarceros(String.valueOf(id_ddjj), 6));
                                        Resultados2.add(tipo2);
                                    }
                                } catch (SQLException ex) {
                                    JOptionPane.showMessageDialog(null, ex);
                                }
                            }
                            pesos = pesos + total;
                        }
                        ///////////////////////////////////////////////////////////////////////
                        //   JOptionPane.showMessageDialog(null, bandera);
                        if (bandera == 1) {
                            try {
                                String sSQL2 = "UPDATE colegiados SET estado_periodo=? WHERE id_colegiados=" + colegiado;
                                PreparedStatement pst = cn.prepareStatement(sSQL2);
                                pst.setBoolean(1, true);
                                int s = pst.executeUpdate();
                                if (s > 0) {
                                }
                            } catch (SQLException e) {
                                JOptionPane.showMessageDialog(null, e);
                            }
                            pesos = Redondeardosdigitos(pesos);
                            importetotal = pesos;
                            ///////////////////////////////////////////////////////////////////////////////////
                            centavos = Redondearcentavos(pesos);
                            pesos = pesos - centavos / 100;
                            total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                            total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                            JasperReport jasperReport;
                            // JOptionPane.showMessageDialog(null, "1");
                            try {
                                //    JOptionPane.showMessageDialog(null, "2");
                                // JasperReport reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ObraSocial.jasper"));
                                JasperReport report = JasperCompileManager.compileReport("C:\\AppTray\\src\\Reportes\\ObraSocial.jrxml");
                                ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                                Map parametros = new HashMap();
                                parametros.put("matricula", matricula);
                                parametros.put("apellidonombre", nombre_colegiado);
                                parametros.put("fecha", fecha2);
                                parametros.put("laboratorio", "F.BIOQ.QCA.Y FCIA.");
                                parametros.put("domicilio_lab", domicilio_lab);
                                parametros.put("localidad", localidad_lab);
                                parametros.put("pacientes", String.valueOf(ordenesgral));
                                parametros.put("practicas", String.valueOf(practicasgral));
                                parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                                parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");

                                parametros.put("total", String.valueOf(importetotal));
                                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados));
                                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\PDF-FACTURACION\\PDF-DECLARACION-JURADA\\" + periodo + matricula + ".pdf");
                                n = n + 1;
                            } catch (JRException ex) {
                                System.err.println("Error iReport: " + ex.getMessage());
                            }

                        }
                        JasperReport jasperReport;
                        try {
                            JasperReport report = JasperCompileManager.compileReport("C:\\AppTray\\src\\Reportes\\ObraSocial_Validacion.jrxml");
                            ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                            Map parametros = new HashMap();

                            JasperPrint jPrint = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(Resultados2));
                            JasperExportManager.exportReportToPdfFile(jPrint, "C:\\PDF-FACTURACION\\PDF-VALIDACION\\" + periodo + matricula + ".pdf");

                        } catch (JRException ex) {
                            System.err.println("Error iReport: " + ex.getMessage());
                        }

                    }
                }
                //////////////////////////////////////////////////////////////////////////////
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

            //con las lineas siguiente damos un tiempo para que se vea el funcionamiento de la barra 
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                //Logger.getLogger(jprogress.class.getName()).log(Level.SEVERE, null, ex);
            }
            dispose();

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

     void cargarddjj() {
        String sSQL = "";
        String numero = "";
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        sSQL = "SELECT MAX(id_declaracion_jurada) AS id_declaracion_jurada FROM declaracion_jurada";

        //“SELECT MAX(id) AS id FROM tabla”
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            if (rs.getInt("id_declaracion_jurada") != 0) {
                id_ddjj = rs.getInt("id_declaracion_jurada");
            } else {
                id_ddjj = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
   String completarceros(String v, int d) {
        if (v.length() < d) {
            String ceros = "";
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        return v;
    }
    
    public double Redondeardosdigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        progreso = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Realizando Declaración Jurada", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("En Proceso espere unos minutos...");

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Cargar.gif"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JProgressBar progreso;
    // End of variables declaration//GEN-END:variables
}
