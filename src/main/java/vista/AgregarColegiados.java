package vista;

import controlador.ConexionMariaDB;
import controlador.Funciones;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Image;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import controlador.Localidad;
import javax.swing.table.DefaultTableModel;

public class AgregarColegiados extends javax.swing.JDialog {

    DefaultTableModel modelo;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    String pic = "";
    int id_colegiado = 0;
    Image imagen2;
    TextAutoCompleter textAutoAcompleter;
    ArrayList<Localidad> cp = new ArrayList<Localidad>();
    Localidad local;
    Funciones efechas = new Funciones();
    String essociedad = "", factura = "", atravescolegio = "";

    public AgregarColegiados(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setTitle("Agregar colegiado");
        this.setLocationRelativeTo(null);
        cargar();/*
        txtnombre.setDocument(new solomayusculas());
        txtlocalidad.setDocument(new solomayusculas());
        txtdireccion.setDocument(new solomayusculas());
        txtespecialidad.setDocument(new solomayusculas());
        textAutoAcompleter = new TextAutoCompleter(txtlocalidad);*/
        //cargarLocalidad();
    }

    void cargarLocalidad() {

        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            Statement St = cn.createStatement();
            ResultSet Rs = St.executeQuery("SELECT nombre_localidad, codigo_postal FROM localidades");
            while (Rs.next()) {

                textAutoAcompleter.addItem(Rs.getString(1));
                local = new Localidad(Rs.getString(1), Rs.getString(2));
                cp.add(local);

            }
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        textAutoAcompleter.setMode(0);
        textAutoAcompleter.setCaseSensitive(false);
    }

    boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    private void cargar() {/*
        btnSiguienteColegiado.setEnabled(false);
        jTabbedPane1.setEnabledAt(1, false);
        jTabbedPane1.setEnabledAt(2, false);
        siFactura.setSelected(true);
        noSociedad.setSelected(true);        
        cbotipodoc.addItem("DNI");
        cbotipodoc.addItem("LC");
        cbotipodoc.addItem("LE");
        cbotipodoc.addItem("CI");
        cbovitalicio.addItem("SI");
        cbovitalicio.addItem("NO");
        cbohorariolab.addItem("8 a 12 hs");
        cbohorariolab.addItem("16 a 20 hs");
        cbohorariolab.addItem("8 a 20 hs");
        ////  txtdirector.setVisible(false);*/
        
    }

    void cargarcolegiado() {
        String sSQL = "";
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        sSQL = "SELECT MAX(id_colegiados) AS id_colegiados FROM colegiados";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            if (rs.getInt("id_colegiados") != 0) {
                id_colegiado = rs.getInt("id_colegiados");
            } else {
                id_colegiado = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    private Image ConvertirImagen(byte[] bytes) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName("png");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        return reader.read(0, param);
    }

    void traerimagen() {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String sql7 = "Select foto_particular from colegiados where id_colegiados=" + id_colegiado;
        try {
            Statement St7 = cn.createStatement();
            ResultSet rs7 = St7.executeQuery(sql7);
            while (rs7.next()) {
                byte[] img = rs7.getBytes(1);
                if (img != null) {
                    try {
                        imagen2 = ConvertirImagen(img);
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                } else {
                    imagen2 = null;
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "\n Los Datos se insertaron erroneamente...");
        }
    }

    public boolean guardarImagen(String ruta) {
        cargarcolegiado();
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String insert = "UPDATE colegiados SET foto_particular=? WHERE id_colegiados=" + id_colegiado;
        FileInputStream fis = null;
        PreparedStatement ps = null;
        try {
            cn.setAutoCommit(false);
            File file = new File(ruta);
            fis = new FileInputStream(file);
            ps = cn.prepareStatement(insert);
            ps.setBinaryStream(1, fis, (int) file.length());
            ps.executeUpdate();
            cn.commit();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "\n Los Datos se insertaron erroneamente...");
        } finally {
            try {
                ps.close();
                fis.close();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex + "\n Los Datos se insertaron erroneamente...");
            }
        }
        return false;
    }

    void cuil(int s, String dni) {/*
        String d = dni;
        long c, res = 0;
        long c0, c1, c2, c3, c4, c5, c6, c7, c8, c9;
        if (s == 0) {
            d = "20" + dni;

        } else {
            d = "27" + dni;

        }
        c = Long.valueOf(d);
        c0 = c % 10 * 2;
        c = c / 10;
        c1 = c % 10 * 3;
        c = c / 10;
        c2 = c % 10 * 4;
        c = c / 10;
        c3 = c % 10 * 5;
        c = c / 10;
        c4 = c % 10 * 6;
        c = c / 10;
        c5 = c % 10 * 7;
        c = c / 10;
        c6 = c % 10 * 2;
        c = c / 10;
        c7 = c % 10 * 3;
        c = c / 10;
        c8 = c % 10 * 4;
        //c = c / 10;
        c9 = c / 10 * 5;
        res = c0 + c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9;
        res = res % 11;
        if (res == 0) {
            d = d + "0";
        } else {
            if (res == 1) {
                if (s == 0) {
                    d = "23" + dni + "9";
                } else {
                    d = "23" + dni + "4";
                }
            } else {
                res = 11 - res;
                String r = String.valueOf(res);
                d = d + r;
            }
        }
        txtcuil.setText(d);*/
    }
    
   void insertarDatosColegiado(){/*
         try {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            String sSQL;

            if (siSociedad.isSelected()) {
                essociedad = "SI";
            }
            if (noSociedad.isSelected()) {
                essociedad = "NO";
            }
            if (siFactura.isSelected()) {
                factura = "SI";
            }
            if (noFactura.isSelected()) {
                factura = "NO";
            }           
            
            if (!pic.equals("")) {
                guardarImagen(pic);
            }
            sSQL = "INSERT INTO colegiados(cuil_colegiado, matricula_colegiado, nombre_colegiado,"
                    + " ndeacta_colegiado, ndefolio_colegiado, ndelibro_colegiado, essociedad, factura,"
                    + " atravescolegio, celular_particular, codigopostal_particular, direccion_particular,"
                    + " especialidad_particular, fechadedoctorado_particular, fechadeegreso_particular,"
                    + " fechadenacimiento_particular, localidad_particular, mail_particular,"
                    + " numerodedocumento_particular, numerodetitulo_particular,"
                    + " telefono_particular, tipodedocumento_particular, vitalicio_particular, fecha_alta_colegiado, fecha_baja_colegiado, tipo_profesional, sexo) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, txtcuil.getText());
            pst.setString(2, txtmatricula.getText());
            pst.setString(3, txtnombre.getText());
            pst.setString(4, txtndeacta.getText());
            pst.setString(5, txtndefolio.getText());
            pst.setString(6, txtndelibro.getText());
            pst.setString(7, essociedad);
            pst.setString(8, factura);
            pst.setString(9, atravescolegio);
            pst.setString(10, txttelcelular.getName());
            pst.setString(11, txtcodigopostal.getText());
            pst.setString(12, txtdireccion.getText());
            pst.setString(13, txtespecialidad.getText());
            pst.setString(14, efechas.getfechaok(fechadoctorado));
            pst.setString(15, efechas.getfechaok(fechaegreso));
            pst.setString(16, efechas.getfechaok(fechanacimiento));
            pst.setString(17, txtlocalidad.getText());
            pst.setString(18, txtmail.getText());
            pst.setString(19, txtdocumento.getText());
            pst.setString(20, txtntitulo.getText());
            pst.setString(21, txttelfijo.getText());
            pst.setString(22, cbotipodoc.getSelectedItem().toString());
            pst.setString(23, cbovitalicio.getSelectedItem().toString());
            pst.setString(24, efechas.getfechaok(fechaalta));
            pst.setString(25, efechas.getfechaok(fechabaja));
            pst.setString(26, cboTipoProf.getSelectedItem().toString());
            pst.setString(27, cbosexo.getSelectedItem().toString());
            int n = pst.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");

                int opcion = JOptionPane.showConfirmDialog(this, "Desea imprimir el carnet?", "Mensaje", JOptionPane.YES_NO_OPTION);
                if (opcion == 0) {
                    try {
                        traerimagen();
                        JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet.jasper"));
                        Map parametros = new HashMap();
                        if (cboTipoProf.getSelectedItem().toString().equals("B")) {
                            if (cbosexo.getSelectedItem().toString().equals("M")) {
                                parametros.put("parametro", "BIOQUÍMICO");
                            } else {
                                parametros.put("parametro", "BIOQUÍMICA");
                            }
                        } else {
                            if (cbosexo.getSelectedItem().toString().equals("M")) {
                                parametros.put("parametro", "TECNICO");
                            } else {
                                parametros.put("parametro", "TECNICA");
                            }

                        }
                        parametros.put("Matricula", txtmatricula.getText());
                        parametros.put("Apellido", txtnombre.getText());
                        parametros.put("FechaNac", efechas.getfechaok(fechanacimiento));
                        parametros.put("Dni", txtdocumento.getText());
                        parametros.put("Foto", imagen2);
                        JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                        JasperPrintManager.printReport(jPrint, false);

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                    JOptionPane.showMessageDialog(null, "Dar vuelta la hoja para imprir el reverso...");
                    try {
                        JasperReport reporte_sobre2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet2.jasper"));
                        Map parametros = new HashMap();
                        parametros.put("Domicilio", txtdireccion.getText());
                        parametros.put("Localidad", txtlocalidad.getText());
                        parametros.put("Provincia", "TUCUMAN");
                        parametros.put("FechaEgreso", efechas.getfechaok(fechaegreso));
                        parametros.put("FechaAlta", efechas.getfechaok(fechaalta));
                        JasperPrint jPrint2 = JasperFillManager.fillReport(reporte_sobre2, parametros, new JREmptyDataSource());
                        JasperPrintManager.printReport(jPrint2, false);
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }
                this.dispose();
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        if (siFactura.isSelected() && noSociedad.isSelected()) {
            jTabbedPane1.setEnabledAt(2, true);
            jTabbedPane1.setSelectedIndex(2);
            btnGrabar.setText("Agregar");
            jTabbedPane1.setEnabledAt(1, false);
            jTabbedPane1.setEnabledAt(0, false);
            //// txtdirector.setEnabled(false);
        }
        if (siFactura.isSelected() && siSociedad.isSelected()) {
            jTabbedPane1.setEnabledAt(2, true);
            jTabbedPane1.setSelectedIndex(2);
            btnGrabar.setText("Agregar");
            jTabbedPane1.setEnabledAt(1, false);
            jTabbedPane1.setEnabledAt(0, false);
        }*/
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoBotonBanco = new javax.swing.ButtonGroup();
        grupoFactura = new javax.swing.ButtonGroup();
        grupoSociedad = new javax.swing.ButtonGroup();
        lblfoto1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        Colegiado = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        Particular = new javax.swing.JPanel();
        btnSiguienteParticular = new javax.swing.JButton();
        btnAtrasParticular = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        Laboratorio = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        txtdirector = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        txtlocalidadlab = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txtdireccionlab = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        txtndireccionlab = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txtcodigopostallab = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        txtmaillab = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txtweblab = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txttelcelularlab = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtfaxlab = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txttelfijolab = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        cbohorariolab = new javax.swing.JComboBox();
        jLabel38 = new javax.swing.JLabel();
        txtespecialidadlab = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtusuario = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txtcontraseña = new javax.swing.JPasswordField();
        jLabel24 = new javax.swing.JLabel();
        txtANSSALlab = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        fechavenANSSAL = new com.toedter.calendar.JDateChooser();
        fechaAltaLaboratorio = new com.toedter.calendar.JDateChooser();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        fechaBajaLaboratorio = new com.toedter.calendar.JDateChooser();
        btnAtrasLaboratorio = new javax.swing.JButton();
        btnGrabar = new javax.swing.JButton();
        btnSiguienteCuentasBancarias = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        txtEntidadBancaria = new javax.swing.JTextField();
        cboTipoCuenta = new javax.swing.JComboBox<>();
        cboTipoTransferencia = new javax.swing.JComboBox<>();
        txtNumeroCuenta = new javax.swing.JTextField();
        txtCBU = new javax.swing.JTextField();
        txtCuitDestino = new javax.swing.JTextField();
        radioActivo = new javax.swing.JRadioButton();
        radioInactivo = new javax.swing.JRadioButton();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTablaBancos = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnSiguienteColegiado = new javax.swing.JButton();

        lblfoto1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(650, 400));

        Colegiado.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        Colegiado.setPreferredSize(new java.awt.Dimension(580, 350));
        Colegiado.setVerifyInputWhenFocusTarget(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Matriculación", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 470, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 107, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(391, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout ColegiadoLayout = new javax.swing.GroupLayout(Colegiado);
        Colegiado.setLayout(ColegiadoLayout);
        ColegiadoLayout.setHorizontalGroup(
            ColegiadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ColegiadoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(311, Short.MAX_VALUE))
        );
        ColegiadoLayout.setVerticalGroup(
            ColegiadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ColegiadoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Colegiado", Colegiado);

        Particular.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        Particular.setPreferredSize(new java.awt.Dimension(580, 360));

        btnSiguienteParticular.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSiguienteParticular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728938 - flag green.png"))); // NOI18N
        btnSiguienteParticular.setMnemonic('s');
        btnSiguienteParticular.setText("Siguiente");
        btnSiguienteParticular.setToolTipText("Alt + s");
        btnSiguienteParticular.setPreferredSize(null);
        btnSiguienteParticular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteParticularActionPerformed(evt);
            }
        });

        btnAtrasParticular.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAtrasParticular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728939 - flag yellow.png"))); // NOI18N
        btnAtrasParticular.setMnemonic('a');
        btnAtrasParticular.setText("Atras");
        btnAtrasParticular.setToolTipText("Alt + a");
        btnAtrasParticular.setPreferredSize(new java.awt.Dimension(108, 23));
        btnAtrasParticular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasParticularActionPerformed(evt);
            }
        });

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Campos Obligatorios", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 795, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 160, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout ParticularLayout = new javax.swing.GroupLayout(Particular);
        Particular.setLayout(ParticularLayout);
        ParticularLayout.setHorizontalGroup(
            ParticularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ParticularLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ParticularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ParticularLayout.createSequentialGroup()
                        .addComponent(btnAtrasParticular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSiguienteParticular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        ParticularLayout.setVerticalGroup(
            ParticularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ParticularLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 311, Short.MAX_VALUE)
                .addGroup(ParticularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSiguienteParticular, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAtrasParticular, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Datos Particular", Particular);

        Laboratorio.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Laboratorio", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N

        jLabel42.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel42.setForeground(new java.awt.Color(51, 51, 51));
        jLabel42.setText("Director Tecnico:");

        txtdirector.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdirector.setForeground(new java.awt.Color(0, 102, 204));
        txtdirector.setNextFocusableComponent(txtlocalidadlab);
        txtdirector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdirectorActionPerformed(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(51, 51, 51));
        jLabel27.setText("Localidad:");

        txtlocalidadlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtlocalidadlab.setForeground(new java.awt.Color(0, 102, 204));
        txtlocalidadlab.setNextFocusableComponent(txtdireccionlab);

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(51, 51, 51));
        jLabel26.setText("Dirección:");

        txtdireccionlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdireccionlab.setForeground(new java.awt.Color(0, 102, 204));
        txtdireccionlab.setNextFocusableComponent(txtdireccionlab);

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(51, 51, 51));
        jLabel32.setText("Codigo Postal:");

        txtndireccionlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtndireccionlab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(51, 51, 51));
        jLabel31.setText("N°:");

        txtcodigopostallab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcodigopostallab.setForeground(new java.awt.Color(0, 102, 204));
        txtcodigopostallab.setNextFocusableComponent(txtmaillab);

        jLabel37.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel37.setForeground(new java.awt.Color(51, 51, 51));
        jLabel37.setText("Pagina Web:");

        txtmaillab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmaillab.setForeground(new java.awt.Color(0, 102, 204));
        txtmaillab.setNextFocusableComponent(txtweblab);

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(51, 51, 51));
        jLabel30.setText("Mail:");

        txtweblab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtweblab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(51, 51, 51));
        jLabel29.setText("Tel Celular:");

        txttelcelularlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txttelcelularlab.setForeground(new java.awt.Color(0, 102, 204));
        txttelcelularlab.setNextFocusableComponent(txtfaxlab);

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(51, 51, 51));
        jLabel36.setText("Fax:");

        txtfaxlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtfaxlab.setForeground(new java.awt.Color(0, 102, 204));
        txtfaxlab.setNextFocusableComponent(txttelfijolab);

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(51, 51, 51));
        jLabel28.setText("Tel Fijo:");

        txttelfijolab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txttelfijolab.setForeground(new java.awt.Color(0, 102, 204));
        txttelfijolab.setNextFocusableComponent(cbohorariolab);

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(51, 51, 51));
        jLabel39.setText("Horario:");

        cbohorariolab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbohorariolab.setForeground(new java.awt.Color(0, 102, 204));
        cbohorariolab.setNextFocusableComponent(txtespecialidadlab);

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(51, 51, 51));
        jLabel38.setText("Especialidad: ");

        txtespecialidadlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtespecialidadlab.setForeground(new java.awt.Color(0, 102, 204));
        txtespecialidadlab.setNextFocusableComponent(txtusuario);

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(51, 51, 51));
        jLabel25.setText("Usuario:");

        txtusuario.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtusuario.setForeground(new java.awt.Color(0, 102, 204));
        txtusuario.setNextFocusableComponent(txtcontraseña);

        jLabel40.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(51, 51, 51));
        jLabel40.setText("Contraseña:");

        txtcontraseña.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcontraseña.setForeground(new java.awt.Color(0, 102, 204));
        txtcontraseña.setNextFocusableComponent(txtANSSALlab);

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(51, 51, 51));
        jLabel24.setText("ANSSAL:");

        txtANSSALlab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtANSSALlab.setForeground(new java.awt.Color(0, 102, 204));

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(51, 51, 51));
        jLabel35.setText("FV de ANSSAL:");

        fechavenANSSAL.setForeground(new java.awt.Color(0, 102, 204));
        fechavenANSSAL.setDateFormatString("dd-MM-yyyy");
        fechavenANSSAL.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        fechaAltaLaboratorio.setForeground(new java.awt.Color(0, 102, 204));
        fechaAltaLaboratorio.setDateFormatString("dd-MM-yyyy");
        fechaAltaLaboratorio.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechaAltaLaboratorio.setNextFocusableComponent(fechavenANSSAL);

        jLabel48.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel48.setText("Fecha de Alta:");

        jLabel49.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel49.setText("Fecha de Baja:");

        fechaBajaLaboratorio.setForeground(new java.awt.Color(0, 102, 204));
        fechaBajaLaboratorio.setDateFormatString("dd-MM-yyyy");
        fechaBajaLaboratorio.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        fechaBajaLaboratorio.setNextFocusableComponent(btnGrabar);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel29)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttelcelularlab, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel36)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtfaxlab, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel30)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtmaillab, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(21, 21, 21)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttelfijolab, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel39)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbohorariolab, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel37)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtweblab))))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel42)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdirector))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdireccionlab, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(txtndireccionlab, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel32)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtcodigopostallab))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                .addGap(0, 81, Short.MAX_VALUE)
                                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtlocalidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel38)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtespecialidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, Short.MAX_VALUE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel6Layout.createSequentialGroup()
                                        .addComponent(jLabel24)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtANSSALlab, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel6Layout.createSequentialGroup()
                                        .addComponent(jLabel35)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(fechavenANSSAL, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(55, 55, 55)))
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel49)
                                    .addComponent(jLabel48))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(fechaBajaLaboratorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(fechaAltaLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(79, 79, 79))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel40)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtcontraseña)))))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtdirector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtlocalidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtdireccionlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtndireccionlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcodigopostallab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtmaillab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtweblab, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txttelfijolab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel39)
                        .addComponent(cbohorariolab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtfaxlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txttelcelularlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel29))
                    .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel38)
                        .addComponent(txtespecialidadlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel25)
                        .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel40)
                        .addComponent(txtcontraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(19, 19, 19)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtANSSALlab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fechavenANSSAL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                .addComponent(fechaAltaLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(fechaBajaLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        btnAtrasLaboratorio.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAtrasLaboratorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728939 - flag yellow.png"))); // NOI18N
        btnAtrasLaboratorio.setMnemonic('a');
        btnAtrasLaboratorio.setText("Atras");
        btnAtrasLaboratorio.setPreferredSize(new java.awt.Dimension(108, 23));
        btnAtrasLaboratorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasLaboratorioActionPerformed(evt);
            }
        });

        btnGrabar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728991 - diskette save.png"))); // NOI18N
        btnGrabar.setMnemonic('a');
        btnGrabar.setText("Grabar");
        btnGrabar.setPreferredSize(new java.awt.Dimension(108, 23));
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });

        btnSiguienteCuentasBancarias.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSiguienteCuentasBancarias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728938 - flag green.png"))); // NOI18N
        btnSiguienteCuentasBancarias.setMnemonic('s');
        btnSiguienteCuentasBancarias.setText("Siguiente");
        btnSiguienteCuentasBancarias.setToolTipText("Alt + s");
        btnSiguienteCuentasBancarias.setPreferredSize(null);
        btnSiguienteCuentasBancarias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteCuentasBancariasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout LaboratorioLayout = new javax.swing.GroupLayout(Laboratorio);
        Laboratorio.setLayout(LaboratorioLayout);
        LaboratorioLayout.setHorizontalGroup(
            LaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LaboratorioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(LaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(LaboratorioLayout.createSequentialGroup()
                        .addComponent(btnAtrasLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSiguienteCuentasBancarias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(btnGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        LaboratorioLayout.setVerticalGroup(
            LaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LaboratorioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 198, Short.MAX_VALUE)
                .addGroup(LaboratorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAtrasLaboratorio, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSiguienteCuentasBancarias, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Datos Laboratorio", Laboratorio);

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Cuentas"));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Nº de cuenta:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("CBU:");

        jLabel45.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel45.setText("CUIT de destino:");

        jLabel46.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel46.setText("Entidad bancaria:");

        jLabel50.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel50.setText("Tipo de cuenta:");

        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel51.setText("Tipo de transferencia:");

        jLabel52.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel52.setText("Estado:");

        grupoBotonBanco.add(radioActivo);
        radioActivo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioActivo.setText("Activo");

        grupoBotonBanco.add(radioInactivo);
        radioInactivo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioInactivo.setText("Inactivo");

        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTablaBancos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTablaBancos);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 416, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        jButton1.setText("Agregar");

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        jButton3.setText("Cancelar");
        jButton3.setToolTipText("");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel52, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel45, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel46, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtEntidadBancaria)
                            .addComponent(cboTipoCuenta, 0, 194, Short.MAX_VALUE)
                            .addComponent(cboTipoTransferencia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNumeroCuenta)
                            .addComponent(txtCBU)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(radioActivo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(radioInactivo))
                            .addComponent(txtCuitDestino)))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton3)))
                .addGap(18, 18, 18)
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel46)
                            .addComponent(txtEntidadBancaria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel50)
                            .addComponent(cboTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel51)
                            .addComponent(cboTipoTransferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtNumeroCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txtCBU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel45)
                            .addComponent(txtCuitDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel52)
                            .addComponent(radioActivo)
                            .addComponent(radioInactivo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton3)))
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Cuentas bancarias", jPanel4);

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnSalir.setMnemonic('v');
        btnSalir.setText("Salir");
        btnSalir.setToolTipText("Alt + v");
        btnSalir.setPreferredSize(new java.awt.Dimension(108, 23));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnSiguienteColegiado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSiguienteColegiado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728938 - flag green.png"))); // NOI18N
        btnSiguienteColegiado.setMnemonic('s');
        btnSiguienteColegiado.setText("Siguiente");
        btnSiguienteColegiado.setToolTipText("Alt + s");
        btnSiguienteColegiado.setPreferredSize(null);
        btnSiguienteColegiado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteColegiadoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 836, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSiguienteColegiado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 593, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSiguienteColegiado, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAtrasParticularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasParticularActionPerformed
        jTabbedPane1.setSelectedIndex(0);
        jTabbedPane1.setEnabledAt(1, false);
        jTabbedPane1.setEnabledAt(2, false);
        jTabbedPane1.setEnabledAt(0, true);
        btnSiguienteParticular.setText("Siguiente");
        btnGrabar.setText("Siguiente");
    }//GEN-LAST:event_btnAtrasParticularActionPerformed

    private void btnSiguienteParticularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteParticularActionPerformed
    /*    try {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            String sSQL;

            if (siSociedad.isSelected()) {
                essociedad = "SI";
            }
            if (noSociedad.isSelected()) {
                essociedad = "NO";
            }
            if (siFactura.isSelected()) {
                factura = "SI";
            }
            if (noFactura.isSelected()) {
                factura = "NO";
            }           
            
            if (!pic.equals("")) {
                guardarImagen(pic);
            }
            sSQL = "INSERT INTO colegiados(cuil_colegiado, matricula_colegiado, nombre_colegiado,"
                    + " ndeacta_colegiado, ndefolio_colegiado, ndelibro_colegiado, essociedad, factura,"
                    + " atravescolegio, celular_particular, codigopostal_particular, direccion_particular,"
                    + " especialidad_particular, fechadedoctorado_particular, fechadeegreso_particular,"
                    + " fechadenacimiento_particular, localidad_particular, mail_particular,"
                    + " numerodedocumento_particular, numerodetitulo_particular,"
                    + " telefono_particular, tipodedocumento_particular, vitalicio_particular, fecha_alta_colegiado, fecha_baja_colegiado, tipo_profesional, sexo) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, txtcuil.getText());
            pst.setString(2, txtmatricula.getText());
            pst.setString(3, txtnombre.getText());
            pst.setString(4, txtndeacta.getText());
            pst.setString(5, txtndefolio.getText());
            pst.setString(6, txtndelibro.getText());
            pst.setString(7, essociedad);
            pst.setString(8, factura);
            pst.setString(9, atravescolegio);
            pst.setString(10, txttelcelular.getName());
            pst.setString(11, txtcodigopostal.getText());
            pst.setString(12, txtdireccion.getText());
            pst.setString(13, txtespecialidad.getText());
            pst.setString(14, efechas.getfechaok(fechadoctorado));
            pst.setString(15, efechas.getfechaok(fechaegreso));
            pst.setString(16, efechas.getfechaok(fechanacimiento));
            pst.setString(17, txtlocalidad.getText());
            pst.setString(18, txtmail.getText());
            pst.setString(19, txtdocumento.getText());
            pst.setString(20, txtntitulo.getText());
            pst.setString(21, txttelfijo.getText());
            pst.setString(22, cbotipodoc.getSelectedItem().toString());
            pst.setString(23, cbovitalicio.getSelectedItem().toString());
            pst.setString(24, efechas.getfechaok(fechaalta));
            pst.setString(25, efechas.getfechaok(fechabaja));
            pst.setString(26, cboTipoProf.getSelectedItem().toString());
            pst.setString(27, cbosexo.getSelectedItem().toString());
            int n = pst.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");

                int opcion = JOptionPane.showConfirmDialog(this, "Desea imprimir el carnet?", "Mensaje", JOptionPane.YES_NO_OPTION);
                if (opcion == 0) {
                    try {
                        traerimagen();
                        JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet.jasper"));
                        Map parametros = new HashMap();
                        if (cboTipoProf.getSelectedItem().toString().equals("B")) {
                            if (cbosexo.getSelectedItem().toString().equals("M")) {
                                parametros.put("parametro", "BIOQUÍMICO");
                            } else {
                                parametros.put("parametro", "BIOQUÍMICA");
                            }
                        } else {
                            if (cbosexo.getSelectedItem().toString().equals("M")) {
                                parametros.put("parametro", "TECNICO");
                            } else {
                                parametros.put("parametro", "TECNICA");
                            }

                        }
                        parametros.put("Matricula", txtmatricula.getText());
                        parametros.put("Apellido", txtnombre.getText());
                        parametros.put("FechaNac", efechas.getfechaok(fechanacimiento));
                        parametros.put("Dni", txtdocumento.getText());
                        parametros.put("Foto", imagen2);
                        JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                        JasperPrintManager.printReport(jPrint, false);

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                    JOptionPane.showMessageDialog(null, "Dar vuelta la hoja para imprir el reverso...");
                    try {
                        JasperReport reporte_sobre2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet2.jasper"));
                        Map parametros = new HashMap();
                        parametros.put("Domicilio", txtdireccion.getText());
                        parametros.put("Localidad", txtlocalidad.getText());
                        parametros.put("Provincia", "TUCUMAN");
                        parametros.put("FechaEgreso", efechas.getfechaok(fechaegreso));
                        parametros.put("FechaAlta", efechas.getfechaok(fechaalta));
                        JasperPrint jPrint2 = JasperFillManager.fillReport(reporte_sobre2, parametros, new JREmptyDataSource());
                        JasperPrintManager.printReport(jPrint2, false);
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }
                this.dispose();
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        if (siFactura.isSelected() && noSociedad.isSelected()) {
            jTabbedPane1.setEnabledAt(2, true);
            jTabbedPane1.setSelectedIndex(2);
            btnGrabar.setText("Agregar");
            jTabbedPane1.setEnabledAt(1, false);
            jTabbedPane1.setEnabledAt(0, false);
            //// txtdirector.setEnabled(false);
        }
        if (siFactura.isSelected() && siSociedad.isSelected()) {
            jTabbedPane1.setEnabledAt(2, true);
            jTabbedPane1.setSelectedIndex(2);
            btnGrabar.setText("Agregar");
            jTabbedPane1.setEnabledAt(1, false);
            jTabbedPane1.setEnabledAt(0, false);
        }*/
    }//GEN-LAST:event_btnSiguienteParticularActionPerformed

    private void btnAtrasLaboratorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasLaboratorioActionPerformed
        jTabbedPane1.setSelectedIndex(1);
        jTabbedPane1.setEnabledAt(0, false);
        jTabbedPane1.setEnabledAt(1, true);
        jTabbedPane1.setEnabledAt(2, false);
        btnSiguienteParticular.setText("Siguiente");

        btnGrabar.setText("Siguiente");

    }//GEN-LAST:event_btnAtrasLaboratorioActionPerformed

    private void btnGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGrabarActionPerformed
      /*  try {
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            ///Agrego Periodo
            String SQL = "SELECT periodo FROM periodos", cos = txtmatricula.getText(), dni = txtdocumento.getText(), id = "";
            int periodo;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            rs.last();
            periodo = rs.getInt("periodo");
            ///////
            try {
                String sqlos = "SELECT matricula_colegiado, id_colegiados FROM colegiados WHERE matricula_colegiado='" + cos + "' AND numerodedocumento_particular='" + dni + "'";

                Statement stos = cn.createStatement();
                ResultSet rsos = stos.executeQuery(sqlos);

                while (rsos.next()) {
                    id = rsos.getString("id_colegiados");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            if (siFactura.isSelected() && noSociedad.isSelected() && id == "") {

                String sSQL;

                if (siSociedad.isSelected()) {
                    essociedad = "SI";
                }
                if (noSociedad.isSelected()) {
                    essociedad = "NO";
                }
                if (siFactura.isSelected()) {
                    factura = "SI";
                }
                if (noFactura.isSelected()) {
                    factura = "NO";
                }                

                sSQL = "INSERT INTO colegiados(anssal_laboratorio,"
                        + " celular_laboratorio, codigopostal_laboratorio, contraseña_laboratorio, direccion_laboratorio,"
                        + " especialidad_laboratorio, fax_laboratorio, fechaalta_laboratorio, fechabaja_laboratorio,"
                        + " fechavencimientoanssal_laboratorio, horarioantecion_laboratorio, http_laboratorio,"
                        + " localidad_laboratorio, mail_laboratorio, numerodireccion_laboratorio, telefono_laboratorio,"
                        + " usuario_laboratorio, periodos, estado_periodo, periodo_pami,estado_colegiado) "
                        + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement pst = cn.prepareStatement(sSQL);

                pst.setString(1, txtANSSALlab.getText());
                pst.setString(2, txttelcelularlab.getText());
                pst.setString(3, txtcodigopostallab.getText());
                pst.setString(4, txtcontraseña.getText());
                pst.setString(5, txtdireccionlab.getText());
                pst.setString(6, txtespecialidadlab.getText());
                pst.setString(7, txtfaxlab.getText());
                pst.setString(8, efechas.getfechaok(fechaAltaLaboratorio));
                pst.setString(9, efechas.getfechaok(fechaBajaLaboratorio));
                pst.setString(10, efechas.getfechaok(fechavenANSSAL));
                pst.setString(11, cbohorariolab.getSelectedItem().toString());
                pst.setString(12, txtweblab.getText());
                pst.setString(13, txtlocalidadlab.getText());
                pst.setString(14, txtmaillab.getText());
                pst.setString(15, txttelfijolab.getText());
                pst.setString(16, txtusuario.getText());
                pst.setInt(17, periodo);
                pst.setInt(18, 0);
                pst.setInt(19, periodo);
                pst.setString(20, "ACTIVOF");
                int n = pst.executeUpdate();
                if (!pic.equals("")) {
                    guardarImagen(pic);
                }
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                    this.dispose();
                }

            }
            if (siFactura.isSelected() && siSociedad.isSelected() && id == "") {
                String sSQL;
                String matricula_colegiado;
                int mat = Integer.valueOf(txtmatricula.getText()) + 20000;
                matricula_colegiado = String.valueOf(mat);
                if (siSociedad.isSelected()) {
                    essociedad = "SI";
                }
                if (noSociedad.isSelected()) {
                    essociedad = "NO";
                }
                if (siFactura.isSelected()) {
                    factura = "SI";
                }
                if (noFactura.isSelected()) {
                    factura = "NO";
                }
                
                sSQL = "INSERT INTO colegiados(anssal_laboratorio,"
                        + " celular_laboratorio, codigopostal_laboratorio, contraseña_laboratorio, direccion_laboratorio,"
                        + " especialidad_laboratorio, fax_laboratorio, fechaalta_laboratorio, fechabaja_laboratorio,"
                        + " fechavencimientoanssal_laboratorio, horarioantecion_laboratorio, http_laboratorio,"
                        + " localidad_laboratorio, mail_laboratorio, numerodireccion_laboratorio, telefono_laboratorio,"
                        + " usuario_laboratorio, director_laboratorio, periodos, estado_periodo, periodo_pami,estado_coleagido) "
                        + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement pst = cn.prepareStatement(sSQL);
                pst.setString(1, txtANSSALlab.getText());
                pst.setString(2, txttelcelularlab.getText());
                pst.setString(3, txtcodigopostallab.getText());
                pst.setString(4, txtcontraseña.getText());
                pst.setString(5, txtdireccionlab.getText());
                pst.setString(6, txtespecialidadlab.getText());
                pst.setString(7, txtfaxlab.getText());
                pst.setString(8, efechas.getfechaok(fechaAltaLaboratorio));
                pst.setString(9, efechas.getfechaok(fechaBajaLaboratorio));
                pst.setString(10, efechas.getfechaok(fechavenANSSAL));
                pst.setString(11, cbohorariolab.getSelectedItem().toString());
                pst.setString(12, txtweblab.getText());
                pst.setString(13, txtlocalidadlab.getText());
                pst.setString(14, txtmaillab.getText());
                pst.setString(15, txttelfijolab.getText());
                pst.setString(16, txtusuario.getText());
                pst.setInt(17, periodo);
                pst.setInt(18, 0);
                pst.setInt(19, periodo);
                pst.setString(20, "ACTIVOF");
                int n = pst.executeUpdate();
                if (!pic.equals("")) {
                    guardarImagen(pic);
                }
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                    int opcion = JOptionPane.showConfirmDialog(this, "Desea imprimir el carnet?", "Mensaje", JOptionPane.YES_NO_OPTION);
                    if (opcion == 0) {
                        try {
                            traerimagen();
                            JasperReport reporte_sobre = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet.jasper"));
                            Map parametros = new HashMap();
                            if (cboTipoProf.getSelectedItem().toString().equals("B")) {
                                if (cbosexo.getSelectedItem().toString().equals("M")) {
                                    parametros.put("parametro", "BIOQUÍMICO");
                                } else {
                                    parametros.put("parametro", "BIOQUÍMICA");
                                }
                            } else {
                                if (cbosexo.getSelectedItem().toString().equals("M")) {
                                    parametros.put("parametro", "TECNICO");
                                } else {
                                    parametros.put("parametro", "TECNICA");
                                }
                            }
                            parametros.put("Matricula", cboTipoProf.getSelectedItem().toString() + matricula_colegiado);
                            parametros.put("Apellido", txtnombre.getText());
                            parametros.put("FechaNac", efechas.getfechaok(fechanacimiento));
                            parametros.put("Dni", txtdocumento.getText());
                            parametros.put("Foto", imagen2);
                            JasperPrint jPrint = JasperFillManager.fillReport(reporte_sobre, parametros, new JREmptyDataSource());
                            JasperPrintManager.printReport(jPrint, false);

                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                        JOptionPane.showMessageDialog(null, "Dar vuelta la hoja para imprir el reverso...");
                        try {
                            JasperReport reporte_sobre2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Carnet2.jasper"));
                            Map parametros = new HashMap();
                            parametros.put("Domicilio", txtdireccion.getText());
                            parametros.put("Localidad", txtlocalidad.getText());
                            parametros.put("Provincia", "TUCUMAN");
                            parametros.put("FechaEgreso", efechas.getfechaok(fechaegreso));
                            parametros.put("FechaAlta", efechas.getfechaok(fechaalta));
                            JasperPrint jPrint2 = JasperFillManager.fillReport(reporte_sobre2, parametros, new JREmptyDataSource());
                            JasperPrintManager.printReport(jPrint2, false);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                    }
                    this.dispose();
                }
            }
            if (id != "") {
                JOptionPane.showMessageDialog(null, "El colegiado ya existe...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }*/
    }//GEN-LAST:event_btnGrabarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnSiguienteColegiadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteColegiadoActionPerformed
      /*  if (txtnombre.getText().equals("") || txtmatricula.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Faltan Ingresar Datos");
        } else {
            if (noFactura.isSelected()) {
                jTabbedPane1.setEnabledAt(0, false);
                jTabbedPane1.setEnabledAt(2, false);
                jTabbedPane1.setEnabledAt(1, true);                
                btnSiguienteParticular.setText("Agregar");                
            }
            if (siFactura.isSelected() && noSociedad.isSelected()) {
                jTabbedPane1.setEnabledAt(1, true);
                jTabbedPane1.setEnabledAt(0, false);
                jTabbedPane1.setEnabledAt(2, false);
                
            }
            if (siFactura.isSelected() && siSociedad.isSelected()) {
                jTabbedPane1.setEnabledAt(1, true);
                jTabbedPane1.setEnabledAt(0, false);
                jTabbedPane1.setEnabledAt(2, false);
                
            }
            insertarDatosColegiado();
                    
            jTabbedPane1.setSelectedIndex(1);
        }*/
    }//GEN-LAST:event_btnSiguienteColegiadoActionPerformed

    private void txtdirectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdirectorActionPerformed
        String texto = txtdirector.getText().toUpperCase();
        txtdirector.setText(texto);
    }//GEN-LAST:event_txtdirectorActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnSiguienteCuentasBancariasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteCuentasBancariasActionPerformed

        jTabbedPane1.setSelectedIndex(0);
        jTabbedPane1.setEnabledAt(0, false);
        jTabbedPane1.setEnabledAt(1, false);
        jTabbedPane1.setEnabledAt(2, false);
        jTabbedPane1.setEnabledAt(3, true);
        //cargarTablaValidador(2);
        /*btnSiguienteParticular.setText("Siguiente");
        btnGrabar.setText("Siguiente");*/
    }//GEN-LAST:event_btnSiguienteCuentasBancariasActionPerformed

    void cargarTablaBanco(int idvaidcolegiados) {

        ConexionMariaDB CN = new ConexionMariaDB();
        Connection con = CN.Conectar();
        String[] titulo = {"Banco", "Nº Cuenta", "CBU", "CUIT Destino", "Tipo cuenta", "Tipo de Transferencia", "Estado"};

        String[] filas = new String[7];

        int totalOrdenes_1 = 0;

        modelo = new DefaultTableModel(null, titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        try {
            String ConsultaMatricula = "Select * from  vista_banco_tipo\n"
                    + "where id_colegiados=3";

            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaMatricula);

            while (RS.next()) {
                filas[0] = RS.getString("BancoNombre");
                filas[1] = RS.getString("NumeroCuenta");
                filas[2] = RS.getString("CBU");
                filas[3] = RS.getString("cuitDestino");
                filas[4] = RS.getString("CuentaNombre");
                filas[5] = RS.getString("Transferencia");
                if (RS.getInt("Estado") == 1) {
                    filas[6] = "OK";
                } else {
                    filas[6] = "Baja";
                }
                modelo.addRow(filas);
            }
            jTablaBancos.setModel(modelo);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Colegiado;
    private javax.swing.JPanel Laboratorio;
    private javax.swing.JPanel Particular;
    private javax.swing.JButton btnAtrasLaboratorio;
    private javax.swing.JButton btnAtrasParticular;
    private javax.swing.JButton btnGrabar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnSiguienteColegiado;
    private javax.swing.JButton btnSiguienteCuentasBancarias;
    private javax.swing.JButton btnSiguienteParticular;
    private javax.swing.JComboBox<String> cboTipoCuenta;
    private javax.swing.JComboBox<String> cboTipoTransferencia;
    private javax.swing.JComboBox cbohorariolab;
    private com.toedter.calendar.JDateChooser fechaAltaLaboratorio;
    private com.toedter.calendar.JDateChooser fechaBajaLaboratorio;
    private com.toedter.calendar.JDateChooser fechavenANSSAL;
    private javax.swing.ButtonGroup grupoBotonBanco;
    private javax.swing.ButtonGroup grupoFactura;
    private javax.swing.ButtonGroup grupoSociedad;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTablaBancos;
    private javax.swing.JLabel lblfoto1;
    private javax.swing.JRadioButton radioActivo;
    private javax.swing.JRadioButton radioInactivo;
    private javax.swing.JTextField txtANSSALlab;
    private javax.swing.JTextField txtCBU;
    private javax.swing.JTextField txtCuitDestino;
    private javax.swing.JTextField txtEntidadBancaria;
    private javax.swing.JTextField txtNumeroCuenta;
    private javax.swing.JTextField txtcodigopostallab;
    private javax.swing.JPasswordField txtcontraseña;
    private javax.swing.JTextField txtdireccionlab;
    private javax.swing.JTextField txtdirector;
    private javax.swing.JTextField txtespecialidadlab;
    private javax.swing.JTextField txtfaxlab;
    private javax.swing.JTextField txtlocalidadlab;
    private javax.swing.JTextField txtmaillab;
    private javax.swing.JTextField txtndireccionlab;
    private javax.swing.JTextField txttelcelularlab;
    private javax.swing.JTextField txttelfijolab;
    private javax.swing.JTextField txtusuario;
    private javax.swing.JTextField txtweblab;
    // End of variables declaration//GEN-END:variables
}
