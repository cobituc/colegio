/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import com.mxrck.autocompleter.TextAutoCompleter;
import controlador.ConexionMariaDB;
import controlador.fnAlinear;
import controlador.cboFormasdepago;
import controlador.fnEditarCeldas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import modelo.ClaseFacturacion;
import modelo.fnCargarFecha;
import modelo.fnCompletar;
import modelo.fnRedondear;
import static vista.Login.usuario;
import static vista.ObraSocialElegir.cuit_obrasocial;
import static vista.ObraSocialElegir.direccion_obrasocial;
import static vista.ObraSocialElegir.id_obrasocial_cliente;
import static vista.ObraSocialElegir.nombre_obrasocial;

/**
 *
 * @author CBT-COMISIONES
 */
public class ReciboOS extends javax.swing.JDialog {

    public static String total, subtotal;
    cboFormasdepago[] forma = new cboFormasdepago[6];
    static int[] fornas_detalle = new int[100];
    static int indexformapago = 1;
    DefaultTableModel model, model2, model3;
    String periodo;
    public static int bcant = 0;
    int contadorobrasocial = 0, id_obrasocial, id_orden;
    String[] obrasocial = new String[1500];
    public static int[] idobrasocial = new int[500];
    public int x, y;

    public ReciboOS(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        ////// FUNCIONES /////
        cargarnumero();
        cargarformadepago();
        cargatotales();
        cargarperiodo();
        // fnCargarFecha f = new fnCargarFecha();
        // txtfecha.setText(f.cargarfecha());
        dobleclickagregaobrasocial();
        //tamañotablapedido();
        txtusuario.setText(usuario);
        filtrartablaperiodo(cboperiodo.getSelectedItem().toString());
        indexformapago = 1;
        radioTransferencia.setEnabled(false);
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        radioContado.setSelected(true);
    }

    void cursor() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cursor2() {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.pack();
    }

    void cargarperiodo() {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String sql = "SELECT * FROM vista_periodos";
        cboperiodo.removeAllItems();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                cboperiodo.addItem(rs.getString(1));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarnumero() {
        if ("FI".equals(tipoventa.getText())) {
            ConexionMariaDB maria = new ConexionMariaDB();
            Connection cn = maria.Conectar();
            Statement SelectIdPedidos = null;
            String sSQL = "SELECT MAX(id_recibo) AS id_recibo FROM os_recibos";
            fnCompletar c = new fnCompletar();
            try {
                SelectIdPedidos = cn.createStatement();
                ResultSet rs = SelectIdPedidos.executeQuery(sSQL);
                if (rs.next()) {
                    String pedido = String.valueOf(rs.getInt("id_recibo") + 1);
                    txtnumero.setText(c.completarString(pedido, 8));
                } else {
                    txtnumero.setText(c.completarString("1", 8));
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                try {
                    if (SelectIdPedidos != null) {
                        SelectIdPedidos.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }

        //Forma de pago CONTADO
        //indexformapago = 0;
    }
    ///// FUNCION BORRAR TABLA /////

    void borrartabla() {
        DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
        int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
        for (int i = a; i >= 0; i--) {
            temp.removeRow(i);
        }
    }
    ////////////////////////////////////////

    ///// FUNCION CARGAR IVA /////
    void cargarIVA() {

        Double ImpNetobase = 0.0;
        Double ImpTotal = 0.0;

        Double Iva270base = 0.0;
        Double Iva210base = 0.0;
        Double Iva105base = 0.0;
        Double Iva50base = 0.0;
        Double Iva25base = 0.0;
        Double Iva0base = 0.0;

        Double Iva270 = 0.0;
        Double Iva210 = 0.0;
        Double Iva105 = 0.0;
        Double Iva50 = 0.0;
        Double Iva25 = 0.0;
        Double Iva0 = 0.0;

        int totalRow = tablapedidos.getRowCount();
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {

            fnRedondear redondear = new fnRedondear();
            double precio = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
            double iva = 0;

            if (tablapedidos.getValueAt(i, 5).toString().equals("Exento")) {
                iva = 0;
            } else {
                iva = Double.valueOf(tablapedidos.getValueAt(i, 5).toString());
            }

            if (iva == 27) {
                Iva270base = Iva270base + Double.valueOf(redondear.dosDigitos(precio));
            }
            if (iva == 21) {
                Iva210base = Iva210base + Double.valueOf(redondear.dosDigitos(precio));
            }
            if (iva == 10.5) {
                Iva105base = Iva105base + Double.valueOf(redondear.dosDigitos(precio));
            }
            if (iva == 5) {
                Iva50base = Iva50base + Double.valueOf(redondear.dosDigitos(precio));
            }
            if (iva == 2.5) {
                Iva25base = Iva25base + Double.valueOf(redondear.dosDigitos(precio));
            }
            if (iva == 0) {
                Iva0base = Iva0base + Double.valueOf(redondear.dosDigitos(precio));
            }

        }

        fnRedondear redondear = new fnRedondear();
        if (Iva270base != 0.0) {
            ImpNetobase = Iva270base + ImpNetobase;
            Iva270 = redondear.dosDigitos(Iva270base * 27 / 100);
            ImpTotal = ImpNetobase + Iva270 + ImpTotal;
        }
        if (Iva210base != 0.0) {
            ImpNetobase = Iva210base + ImpNetobase;
            Iva210 = redondear.dosDigitos(Iva210base * 21 / 100);
            ImpTotal = ImpNetobase + Iva210 + ImpTotal;
        }
        if (Iva105base != 0.0) {
            ImpNetobase = Iva105base + ImpNetobase;
            Iva105 = redondear.dosDigitos(Iva105base * 10.5 / 100);
            ImpTotal = ImpNetobase + Iva105 + ImpTotal;
        }
        if (Iva50base != 0.0) {
            ImpNetobase = Iva50base + ImpNetobase;
            Iva50 = redondear.dosDigitos(Iva50base * 5 / 100);
            ImpTotal = ImpNetobase + Iva50 + ImpTotal;
        }
        if (Iva25base != 0.0) {
            ImpNetobase = Iva25base + ImpNetobase;
            Iva25 = redondear.dosDigitos(Iva25base * 2.5 / 100);
            ImpTotal = ImpNetobase + Iva25 + ImpTotal;
        }
        if (Iva0base != 0.0) {
            ImpNetobase = Iva0base + ImpNetobase;
            Iva0 = redondear.dosDigitos(Iva0base * 0 / 100);
            ImpTotal = ImpNetobase + Iva0 + ImpTotal;
        }

        txtimporteneto.setText(String.valueOf(redondear.dosDigitos(ImpNetobase)));
        txtiva270.setText(String.valueOf(Iva270));
        txtiva210.setText(String.valueOf(Iva210));
        txtiva105.setText(String.valueOf(Iva105));
        txtiva50.setText(String.valueOf(Iva50));
        txtiva25.setText(String.valueOf(Iva25));
        txtiva0.setText(String.valueOf(Iva0));
        txttributos.setText("0.0");
        txttotal.setText(String.valueOf(redondear.dosDigitos(ImpTotal)));

    }

    void cargarformadepago() {
        ConexionMariaDB maria = new ConexionMariaDB();
        Connection cn = maria.Conectar();
        Statement SelectTarjetaDebito = null;
        String sSQL = "SELECT * FROM formasdepago";
        try {
            SelectTarjetaDebito = cn.createStatement();
            ResultSet rs = SelectTarjetaDebito.executeQuery(sSQL);
            int i = 0;
            while (rs.next()) {
                forma[i] = new cboFormasdepago(rs.getInt("idFormasDePago"), rs.getString("nombre"), rs.getDouble("descuento"));
                i++;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTarjetaDebito != null) {
                    SelectTarjetaDebito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    void cargatotales() {
        //Suma los iportes del pedido 
        double total = 0.00, sumatoria = 0.0;
        int totalRow = tablapedidos.getRowCount();
        totalRow -= 1;
        fnRedondear redondear = new fnRedondear();
        System.out.println("vista.FacturaOS.cargatotales()");

        //Recorrer para el descuento
        for (int i = 0; i <= (totalRow); i++) {
            double iva;
            double precio = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
            if (tablapedidos.getValueAt(i, 5).toString().equals("Exento")) {
                iva = 0;
            } else {
                iva = Double.valueOf(tablapedidos.getValueAt(i, 5).toString());
            }
            tablapedidos.setValueAt(redondear.dosDigitos(redondear.dosDigitos(precio) * (1 + (iva / 100))), i, 6);

        }

        for (int i = 0; i <= (totalRow); i++) {

            sumatoria = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 6).toString()));
            total = total + sumatoria;
        }

        txttotal.setText(String.valueOf(redondear.dosDigitos(total)));
        txtsubtotal.setText(String.valueOf(redondear.dosDigitos(total)));
        cargarIVA();
    }
    ////////////////////////////////////////
/*
    ///// FUNCION AJUSTE TABLA PEDIDOS /////
    void tamañotablapedido() {

        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
        DefaultTableCellRenderer tcr2 = new DefaultTableCellRenderer();
        DefaultTableCellRenderer tcr3 = new DefaultTableCellRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tcr2.setHorizontalAlignment(SwingConstants.RIGHT);
        tcr3.setHorizontalAlignment(SwingConstants.LEFT);

        for (int i = 0; i < 12; i++) {

            tablapedidos.getTableHeader().setDefaultRenderer(tcr);

        }
        tablapedidos.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablapedidos.getColumnModel().getColumn(9).setCellRenderer(tcr);
        tablapedidos.getColumnModel().getColumn(11).setCellRenderer(tcr);
        tablapedidos.getColumnModel().getColumn(4).setCellRenderer(tcr);
        tablapedidos.getColumnModel().getColumn(6).setCellRenderer(tcr);
        tablapedidos.getColumnModel().getColumn(7).setCellRenderer(tcr);
        tablapedidos.getColumnModel().getColumn(3).setCellRenderer(tcr2);
        tablapedidos.getColumnModel().getColumn(8).setCellRenderer(tcr);
        tablapedidos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(0).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablapedidos.getColumnModel().getColumn(1).setPreferredWidth(60);
        tablapedidos.getColumnModel().getColumn(2).setPreferredWidth(260);
        tablapedidos.getColumnModel().getColumn(3).setPreferredWidth(30);
        tablapedidos.getColumnModel().getColumn(4).setPreferredWidth(30);
        tablapedidos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(5).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(5).setPreferredWidth(0);
        tablapedidos.getColumnModel().getColumn(6).setPreferredWidth(30);
        tablapedidos.getColumnModel().getColumn(7).setPreferredWidth(35);
        tablapedidos.getColumnModel().getColumn(8).setPreferredWidth(30);
        tablapedidos.getColumnModel().getColumn(9).setPreferredWidth(30);
        tablapedidos.getColumnModel().getColumn(9).setCellRenderer(new fnEditarCeldas());
        tablapedidos.getColumnModel().getColumn(10).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setPreferredWidth(0);
        tablapedidos.getColumnModel().getColumn(11).setPreferredWidth(5);

    }
     */
    ///// CARGAR TABLA PEDIDO /////
    void dobleclickagregaobrasocial() {
        tablabuscar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {

                    DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
                    if (tablabuscar.getSelectedRow() == -1) {
                        JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
                    } else {

                        //stock = Integer.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 7).toString());
                        ///Monto.bc = 0;
                        /// new Monto(null, true).setVisible(true);
                        int bandera = 0;
                        double iva = 0;
                        double precio = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 4).toString());
                        if (tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 6).toString().equals("Exento")) {
                            iva = 0;
                        } else {
                            iva = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 6).toString());
                        }

                        fnRedondear redondear = new fnRedondear();
                        Object nuevo[] = {
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 1).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 2).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 3).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 4).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 6).toString(),
                            redondear.dosDigitos(redondear.dosDigitos(precio * (1 + (iva / 100))))};
                        /////Sacao el codigo de la tabla buscar
                        String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                        /////Recorro la tabla pedidos
                        int filaspedidos = tablapedidos.getRowCount();
                        int i = 0;
                        if (filaspedidos != 0) {
                            while (filaspedidos > i) {
                                /////Sacao el codigo tabla pedido
                                String codigopedido = tablapedidos.getValueAt(i, 0).toString();

                                if (codigobuscar.equals(codigopedido)) {
                                    bandera = 1;
                                    break;
                                }
                                i++;
                            }
                            if (bandera == 0) {

                                temp.addRow(nuevo);
                                /////Resto la cantidad al stock de la tabla buscar

                            } else {
                                /////Recooro la tabla pedidos
                                i = 0;
                                while (filaspedidos > i) {
                                    String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                                    /////Sumo cantidad en la tabla pedidos si el cod son iguales
                                    if (codigobuscar.equals(codigopedido2)) {
                                        precio = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                        iva = Double.valueOf(tablapedidos.getValueAt(i, 5).toString());
                                        tablapedidos.setValueAt(redondear.dosDigitos(redondear.dosDigitos(precio) * (1 + (iva / 100))), i, 6);

                                        break;
                                    }
                                    i++;
                                }
                            }
                        } else {
                            temp.addRow(nuevo);

                        }
                        cargatotales();
                        /////////////////////////////////////////////////////////////
                        txtbuscar.setText("");
                        txtbuscar.requestFocus();

                    }
                }
            }
        }
        );
    }

    void filtrartablaperiodo(String valor) {
        String[] Titulo = {"Código OS", "Obra Social", "Periodo", "Honorarios", "Importe", "Total Colegiados", "Tipo IVA"};
        Object[] Registros = new Object[7];
        String sql = "SELECT * FROM vista_totales_facturado_os_iva WHERE periodo_declaracion = '" + valor + "'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        Statement st = null;
        try {
            st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                model.addRow(Registros);
            }
            tablabuscar.setModel(model);
            tablabuscar.setAutoCreateRowSorter(true);
            cargatotales();
            /////////////////////////////////////////////////////////////   
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelContenedor = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();
        jPanel2 = new javax.swing.JPanel();
        rSLabelIcon1 = new RSMaterialComponent.RSLabelIcon();
        txtbuscar = new RSMaterialComponent.RSTextFieldOne();
        cboperiodo = new RSMaterialComponent.RSComboBox();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablabuscar = new RSMaterialComponent.RSTableMetroCustom();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablapedidos = new RSMaterialComponent.RSTableMetroCustom();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtimporteneto = new RSMaterialComponent.RSTextFieldOne();
        txtiva270 = new RSMaterialComponent.RSTextFieldOne();
        txtiva210 = new RSMaterialComponent.RSTextFieldOne();
        txtiva105 = new RSMaterialComponent.RSTextFieldOne();
        txtiva50 = new RSMaterialComponent.RSTextFieldOne();
        txtiva25 = new RSMaterialComponent.RSTextFieldOne();
        txtiva0 = new RSMaterialComponent.RSTextFieldOne();
        txttributos = new RSMaterialComponent.RSTextFieldOne();
        jPanel5 = new javax.swing.JPanel();
        radioOtros = new rojerusan.RSRadioButton();
        cboformacheque = new RSMaterialComponent.RSComboBox();
        radioCheque = new rojerusan.RSRadioButton();
        radioContado = new rojerusan.RSRadioButton();
        radioTransferencia = new rojerusan.RSRadioButton();
        jPanel7 = new javax.swing.JPanel();
        tipoventa = new javax.swing.JLabel();
        txttotal = new javax.swing.JLabel();
        txtsubtotal = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        txttipo = new javax.swing.JLabel();
        txtdocCliente = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JLabel();
        txtnombreCliente = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtnumero = new javax.swing.JFormattedTextField();
        jPanel1 = new javax.swing.JPanel();
        btnaceptar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btncancelar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        txtusuario = new javax.swing.JLabel();
        btnaceptar1 = new RSMaterialComponent.RSButtonMaterialIconUno();
        jLabel15 = new javax.swing.JLabel();
        rSLabelFecha1 = new rojeru_san.rsdate.RSLabelFecha();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        PanelContenedor.setBackground(new java.awt.Color(66, 133, 200));
        PanelContenedor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        PanelContenedor.setMinimumSize(new java.awt.Dimension(1359, 683));
        PanelContenedor.setPreferredSize(new java.awt.Dimension(1359, 683));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel6.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel6MouseDragged(evt);
            }
        });
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel6MousePressed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(219, 68, 55));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar.setSizeIcon(20.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(418, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));

        rSLabelIcon1.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);

        txtbuscar.setForeground(new java.awt.Color(51, 51, 51));
        txtbuscar.setBorderColor(new java.awt.Color(255, 255, 255));
        txtbuscar.setPhColor(new java.awt.Color(51, 51, 51));
        txtbuscar.setPlaceholder("Buscar");
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        cboperiodo.setForeground(new java.awt.Color(51, 51, 51));
        cboperiodo.setColorArrow(new java.awt.Color(66, 133, 200));
        cboperiodo.setColorBorde(new java.awt.Color(255, 255, 255));
        cboperiodo.setColorBoton(new java.awt.Color(255, 255, 255));
        cboperiodo.setColorFondo(new java.awt.Color(255, 255, 255));
        cboperiodo.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboperiodo.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboperiodo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboperiodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboperiodoActionPerformed(evt);
            }
        });

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tablabuscar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablabuscar.setForeground(new java.awt.Color(255, 255, 255));
        tablabuscar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablabuscar.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablabuscar.setBackgoundHover(new java.awt.Color(255, 255, 255));
        tablabuscar.setBorderHead(null);
        tablabuscar.setBorderRows(null);
        tablabuscar.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablabuscar.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablabuscar.setColorPrimaryText(new java.awt.Color(51, 51, 51));
        tablabuscar.setColorSecondary(new java.awt.Color(227, 233, 230));
        tablabuscar.setColorSecundaryText(new java.awt.Color(51, 51, 51));
        tablabuscar.setGridColor(new java.awt.Color(15, 157, 88));
        tablabuscar.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablabuscar.setShowHorizontalLines(true);
        tablabuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablabuscarKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(tablabuscar);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtbuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(cboperiodo, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 606, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboperiodo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .addComponent(rSLabelIcon1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5))
        );

        jPanel3.setBackground(new java.awt.Color(66, 133, 200));

        jScrollPane6.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane6.setBorder(null);

        tablapedidos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablapedidos.setForeground(new java.awt.Color(255, 255, 255));
        tablapedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Nombre", "Periodo", "U.H.", "Neto", "IVA", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablapedidos.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablapedidos.setBackgoundHover(new java.awt.Color(255, 255, 255));
        tablapedidos.setBorderHead(null);
        tablapedidos.setBorderRows(null);
        tablapedidos.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablapedidos.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablapedidos.setColorPrimaryText(new java.awt.Color(51, 51, 51));
        tablapedidos.setColorSecondary(new java.awt.Color(227, 233, 230));
        tablapedidos.setColorSecundaryText(new java.awt.Color(51, 51, 51));
        tablapedidos.setGridColor(new java.awt.Color(15, 157, 88));
        tablapedidos.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablapedidos.setShowHorizontalLines(true);
        tablapedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapedidosKeyPressed(evt);
            }
        });
        jScrollPane6.setViewportView(tablapedidos);

        jPanel4.setBackground(new java.awt.Color(66, 133, 200));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("Importe Neto Gravado: $");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("IVA 27%: $");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel8.setText("IVA 21%: $");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel9.setText("IVA 10.5%: $");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel10.setText("IVA 5%: $");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel12.setText("IVA 2.5%: $");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel13.setText("IVA 0%: $");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel14.setText("Importe Otros Tributos: $");

        txtimporteneto.setForeground(new java.awt.Color(51, 51, 51));
        txtimporteneto.setBorderColor(new java.awt.Color(255, 255, 255));
        txtimporteneto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtimporteneto.setPhColor(new java.awt.Color(51, 51, 51));
        txtimporteneto.setPlaceholder("");
        txtimporteneto.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtimporteneto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtimportenetoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtimportenetoKeyReleased(evt);
            }
        });

        txtiva270.setForeground(new java.awt.Color(51, 51, 51));
        txtiva270.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva270.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva270.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva270.setPlaceholder("");
        txtiva270.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva270.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva270KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva270KeyReleased(evt);
            }
        });

        txtiva210.setForeground(new java.awt.Color(51, 51, 51));
        txtiva210.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva210.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva210.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva210.setPlaceholder("");
        txtiva210.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva210.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva210KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva210KeyReleased(evt);
            }
        });

        txtiva105.setForeground(new java.awt.Color(51, 51, 51));
        txtiva105.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva105.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva105.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva105.setPlaceholder("");
        txtiva105.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva105.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva105KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva105KeyReleased(evt);
            }
        });

        txtiva50.setForeground(new java.awt.Color(51, 51, 51));
        txtiva50.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva50.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva50.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva50.setPlaceholder("");
        txtiva50.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva50.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva50KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva50KeyReleased(evt);
            }
        });

        txtiva25.setForeground(new java.awt.Color(51, 51, 51));
        txtiva25.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva25.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva25.setPlaceholder("");
        txtiva25.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva25.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva25KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva25KeyReleased(evt);
            }
        });

        txtiva0.setForeground(new java.awt.Color(51, 51, 51));
        txtiva0.setBorderColor(new java.awt.Color(255, 255, 255));
        txtiva0.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtiva0.setPhColor(new java.awt.Color(51, 51, 51));
        txtiva0.setPlaceholder("");
        txtiva0.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtiva0.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtiva0KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiva0KeyReleased(evt);
            }
        });

        txttributos.setForeground(new java.awt.Color(51, 51, 51));
        txttributos.setBorderColor(new java.awt.Color(255, 255, 255));
        txttributos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttributos.setPhColor(new java.awt.Color(51, 51, 51));
        txttributos.setPlaceholder("");
        txttributos.setSelectionColor(new java.awt.Color(244, 180, 0));
        txttributos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttributosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttributosKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel10)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtimporteneto, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva270, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva210, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva105, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva50, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva25, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva0, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttributos, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtimporteneto, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva270, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva210, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva105, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva50, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva25, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtiva0, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttributos, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel5.setBackground(new java.awt.Color(66, 133, 200));

        radioOtros.setForeground(new java.awt.Color(255, 255, 255));
        radioOtros.setText("Otros");
        radioOtros.setColorCheck(new java.awt.Color(255, 255, 255));
        radioOtros.setColorUnCheck(new java.awt.Color(255, 255, 255));
        radioOtros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioOtrosActionPerformed(evt);
            }
        });

        cboformacheque.setForeground(new java.awt.Color(51, 51, 51));
        cboformacheque.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cheque a 30 Dias", "Cheque a 45 Dias", "Cheque a 60 Dias", "Cheque a 90 Dias" }));
        cboformacheque.setColorArrow(new java.awt.Color(66, 133, 200));
        cboformacheque.setColorBorde(new java.awt.Color(255, 255, 255));
        cboformacheque.setColorBoton(new java.awt.Color(255, 255, 255));
        cboformacheque.setColorFondo(new java.awt.Color(255, 255, 255));
        cboformacheque.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboformacheque.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboformacheque.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboformacheque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboformachequeActionPerformed(evt);
            }
        });

        radioCheque.setForeground(new java.awt.Color(255, 255, 255));
        radioCheque.setText("Cheque");
        radioCheque.setColorCheck(new java.awt.Color(255, 255, 255));
        radioCheque.setColorUnCheck(new java.awt.Color(255, 255, 255));
        radioCheque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioChequeActionPerformed(evt);
            }
        });

        radioContado.setForeground(new java.awt.Color(255, 255, 255));
        radioContado.setText("Contado");
        radioContado.setColorCheck(new java.awt.Color(255, 255, 255));
        radioContado.setColorUnCheck(new java.awt.Color(255, 255, 255));
        radioContado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioContadoActionPerformed(evt);
            }
        });

        radioTransferencia.setForeground(new java.awt.Color(255, 255, 255));
        radioTransferencia.setText("Transferencia");
        radioTransferencia.setColorCheck(new java.awt.Color(255, 255, 255));
        radioTransferencia.setColorUnCheck(new java.awt.Color(255, 255, 255));
        radioTransferencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioTransferenciaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(radioOtros, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(radioCheque, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(radioTransferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(radioContado, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(cboformacheque, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 20, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(radioTransferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(radioContado, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(radioCheque, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cboformacheque, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(radioOtros, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(66, 133, 200));

        tipoventa.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        tipoventa.setForeground(new java.awt.Color(244, 180, 0));
        tipoventa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tipoventa.setText("Recibo");
        tipoventa.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tipo", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(255, 255, 255))); // NOI18N
        tipoventa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tipoventaMouseClicked(evt);
            }
        });

        txttotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txttotal.setForeground(new java.awt.Color(255, 255, 255));
        txttotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txttotal.setText("0.0");
        txttotal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(255, 255, 255))); // NOI18N

        txtsubtotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtsubtotal.setForeground(new java.awt.Color(219, 68, 55));
        txtsubtotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtsubtotal.setText("0.0");
        txtsubtotal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SubTotal", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(255, 255, 255))); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tipoventa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtsubtotal, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                    .addComponent(txttotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tipoventa, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtsubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel8.setBackground(new java.awt.Color(66, 133, 200));

        txttipo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txttipo.setForeground(new java.awt.Color(244, 180, 0));
        txttipo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        txtdocCliente.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtdocCliente.setForeground(new java.awt.Color(244, 180, 0));
        txtdocCliente.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        txtdireccion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtdireccion.setForeground(new java.awt.Color(244, 180, 0));
        txtdireccion.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        txtnombreCliente.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtnombreCliente.setForeground(new java.awt.Color(244, 180, 0));
        txtnombreCliente.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txtnombreCliente.setText("Consumidor Final");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(244, 180, 0));
        jLabel11.setText("N°:");

        txtnumero.setEditable(false);
        txtnumero.setBorder(null);
        txtnumero.setForeground(new java.awt.Color(244, 180, 0));
        try {
            txtnumero.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtnumero.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtnumero.setOpaque(false);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(txtnombreCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtdocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtnombreCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                        .addGap(1, 1, 1))
                    .addComponent(txtnumero, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtdireccion, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(txtdocCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txttipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));

        btnaceptar.setBackground(new java.awt.Color(255, 255, 255));
        btnaceptar.setForeground(new java.awt.Color(0, 0, 0));
        btnaceptar.setText("Aceptar");
        btnaceptar.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnaceptar.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnaceptar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnaceptar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CHECK);
        btnaceptar.setRound(20);
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btncancelar.setBackground(new java.awt.Color(255, 255, 255));
        btncancelar.setForeground(new java.awt.Color(0, 0, 0));
        btncancelar.setText("Borrar");
        btncancelar.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btncancelar.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btncancelar.setForegroundText(new java.awt.Color(51, 51, 51));
        btncancelar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.DELETE);
        btncancelar.setRound(20);
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnsalir.setBackground(new java.awt.Color(255, 255, 255));
        btnsalir.setForeground(new java.awt.Color(0, 0, 0));
        btnsalir.setText("Salir");
        btnsalir.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnsalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnsalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnsalir.setRound(20);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        txtusuario.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtusuario.setForeground(new java.awt.Color(255, 255, 255));
        txtusuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        btnaceptar1.setBackground(new java.awt.Color(255, 255, 255));
        btnaceptar1.setForeground(new java.awt.Color(0, 0, 0));
        btnaceptar1.setText("Obra Social");
        btnaceptar1.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnaceptar1.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnaceptar1.setForegroundText(new java.awt.Color(51, 51, 51));
        btnaceptar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.LOUPE);
        btnaceptar1.setRound(20);
        btnaceptar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptar1ActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel15.setText("Usuario:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnaceptar1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(255, 255, 255)
                .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(71, 71, 71)
                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnaceptar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtusuario, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        rSLabelFecha1.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelFecha1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout PanelContenedorLayout = new javax.swing.GroupLayout(PanelContenedor);
        PanelContenedor.setLayout(PanelContenedorLayout);
        PanelContenedorLayout.setHorizontalGroup(
            PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelContenedorLayout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelContenedorLayout.createSequentialGroup()
                        .addComponent(rSLabelFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelContenedorLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelContenedorLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        PanelContenedorLayout.setVerticalGroup(
            PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelContenedorLayout.createSequentialGroup()
                .addGroup(PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(PanelContenedorLayout.createSequentialGroup()
                        .addGroup(PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(rSLabelFecha1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PanelContenedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PanelContenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 691, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed

        dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void jPanel6MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);

    }//GEN-LAST:event_jPanel6MouseDragged

    private void jPanel6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MousePressed

        x = evt.getX();
        y = evt.getY();

    }//GEN-LAST:event_jPanel6MousePressed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed

        if (!txtbuscar.getText().equals("")) {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
            tablabuscar.setRowSorter(sorter);
        } else {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*.*"));
            tablabuscar.setRowSorter(sorter);
        }
        cargatotales();

    }//GEN-LAST:event_txtbuscarKeyPressed

    private void cboperiodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboperiodoActionPerformed
        if (cboperiodo.getSelectedItem() != null) {
            filtrartablaperiodo(cboperiodo.getSelectedItem().toString());
            periodo = cboperiodo.getSelectedItem().toString();
        }
    }//GEN-LAST:event_cboperiodoActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        borrartabla();
        cargarnumero();
        cargarformadepago();
        cargatotales();
        filtrartablaperiodo("");
        txtbuscar.requestFocus();
    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        //No se Puede Hacer un Pedido si la Caja esta Cerrada

        if (txttotal.getText().equals("") || txtimporteneto.getText().equals("")
                || txtnombreCliente.getText().equals("") || tablapedidos.getRowCount() == 0 || id_obrasocial_cliente == 0) {
            JOptionPane.showMessageDialog(null, "Debe completar todos los campos obligatorios...");
        } else {
            /////   Carga Variables
            fnCargarFecha f = new fnCargarFecha();
            total = txttotal.getText();
            int[] datos = new int[tablapedidos.getRowCount()];
            for (int i = 0; i < tablapedidos.getRowCount(); i++) {
                datos[i] = Integer.valueOf(tablapedidos.getValueAt(i, 0).toString());
            }

            /////   Pregunto Si es una Pedido o un Presupuesto o un Modificar Pedido
            if ("Recibo".equals(tipoventa.getText())) {
                cursor();
                int IdRecibo = 0;
                ClaseFacturacion recibo = new ClaseFacturacion();
                try {
                    IdRecibo = recibo.AgregarRecibo(datos, f.cargarfechayhoraSQL(), f.cargarHora(), f.cargarfechayhoraSQL(), periodo, Double.valueOf(txttotal.getText()), id_obrasocial_cliente);
                    cursor2();
                    JOptionPane.showMessageDialog(null, "El Recibo se realizó con exito...");
                } catch (Exception e) {
                    cursor2();
                    JOptionPane.showMessageDialog(null, "Error al cargar el pedido");
                }
                if (IdRecibo != 0) {
                    cursor();
                    recibo.ImprimirRecibo(IdRecibo);
                    cursor2();
                }
                borrartabla();
                cargarnumero();
                cargarformadepago();
                cargatotales();
                filtrartablaperiodo("");
                txtbuscar.requestFocus();
            }
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed

        dispose();

    }//GEN-LAST:event_btnsalirActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        if (!txtbuscar.getText().equals("")) {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
            tablabuscar.setRowSorter(sorter);
        } else {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*.*"));
            tablabuscar.setRowSorter(sorter);
        }
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void tablapedidosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapedidosKeyPressed
        /////Borrar una fila de la tabla pedidos
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            int filaspedidos = tablapedidos.getRowCount();
            if (tablapedidos.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
            } else {
                if (filaspedidos != 0) {
                    int cantidadpedidos = Integer.valueOf(tablapedidos.getValueAt((tablapedidos.getSelectedRow()), 7).toString());
                    String codigopedidos = tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString();
                    /////Recorro la tabla buscar
                    int filasbuscar = tablabuscar.getRowCount();
                    if (filasbuscar != 0) {
                        int i = 0;
                        while (filasbuscar > i) {
                            String codigobuscar = tablabuscar.getValueAt(i, 0).toString();
                            if (codigopedidos.equals(codigobuscar)) {
                                int stock = Integer.valueOf(tablabuscar.getValueAt(i, 7).toString());
                                /////Sumo en la tabla buscar la cantidad y stock
                                tablabuscar.setValueAt((stock + cantidadpedidos), i, 7);
                                break;
                            }
                            i++;
                        }
                    }
                    model3 = (DefaultTableModel) tablapedidos.getModel();
                    model3.removeRow(tablapedidos.getSelectedRow());
                    model3 = null;
                } else {
                    JOptionPane.showMessageDialog(null, "No hay ningun producto en la lista...");
                }
            }
            cargatotales();
        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            tablapedidos.transferFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            tablapedidos.transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablapedidosKeyPressed

    private void btnaceptar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptar1ActionPerformed
        new ObraSocialElegir(null, true).setVisible(true);
        txtnombreCliente.setText(nombre_obrasocial);
        txtdireccion.setText(direccion_obrasocial);
        txtdocCliente.setText(cuit_obrasocial);
    }//GEN-LAST:event_btnaceptar1ActionPerformed

    private void cboformachequeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboformachequeActionPerformed
        indexformapago = 4;
        cargatotales();
    }//GEN-LAST:event_cboformachequeActionPerformed

    private void txtimportenetoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtimportenetoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtimportenetoKeyPressed

    private void txtimportenetoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtimportenetoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtimportenetoKeyReleased

    private void txtiva270KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva270KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva270KeyPressed

    private void txtiva270KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva270KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva270KeyReleased

    private void txtiva210KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva210KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva210KeyPressed

    private void txtiva210KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva210KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva210KeyReleased

    private void txtiva105KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva105KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva105KeyPressed

    private void txtiva105KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva105KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva105KeyReleased

    private void txtiva50KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva50KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva50KeyPressed

    private void txtiva50KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva50KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva50KeyReleased

    private void txtiva25KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva25KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva25KeyPressed

    private void txtiva25KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva25KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva25KeyReleased

    private void txtiva0KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva0KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva0KeyPressed

    private void txtiva0KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiva0KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtiva0KeyReleased

    private void txttributosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttributosKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttributosKeyPressed

    private void txttributosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttributosKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txttributosKeyReleased

    private void tablabuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablabuscarKeyReleased

    }//GEN-LAST:event_tablabuscarKeyReleased

    private void radioTransferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioTransferenciaActionPerformed
        cboformacheque.setEnabled(false);
        indexformapago = 0;
        cargatotales();
    }//GEN-LAST:event_radioTransferenciaActionPerformed

    private void radioChequeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioChequeActionPerformed
        cboformacheque.setEnabled(true);
        indexformapago = 4;
        cargatotales();
    }//GEN-LAST:event_radioChequeActionPerformed

    private void radioOtrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioOtrosActionPerformed
        cboformacheque.setEnabled(false);
        indexformapago = 5;
        cargatotales();
    }//GEN-LAST:event_radioOtrosActionPerformed

    private void radioContadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioContadoActionPerformed
        cboformacheque.setEnabled(false);
        indexformapago = 0;
        cargatotales();
    }//GEN-LAST:event_radioContadoActionPerformed

    private void tipoventaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tipoventaMouseClicked

    }//GEN-LAST:event_tipoventaMouseClicked

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanelContenedor;
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnaceptar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnaceptar1;
    private RSMaterialComponent.RSButtonMaterialIconUno btncancelar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnsalir;
    private RSMaterialComponent.RSComboBox cboformacheque;
    private RSMaterialComponent.RSComboBox cboperiodo;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private rojeru_san.rsdate.RSLabelFecha rSLabelFecha1;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon1;
    private rojerusan.RSRadioButton radioCheque;
    private rojerusan.RSRadioButton radioContado;
    private rojerusan.RSRadioButton radioOtros;
    private rojerusan.RSRadioButton radioTransferencia;
    private RSMaterialComponent.RSTableMetroCustom tablabuscar;
    private RSMaterialComponent.RSTableMetroCustom tablapedidos;
    private javax.swing.JLabel tipoventa;
    private RSMaterialComponent.RSTextFieldOne txtbuscar;
    private javax.swing.JLabel txtdireccion;
    private javax.swing.JLabel txtdocCliente;
    private RSMaterialComponent.RSTextFieldOne txtimporteneto;
    private RSMaterialComponent.RSTextFieldOne txtiva0;
    private RSMaterialComponent.RSTextFieldOne txtiva105;
    private RSMaterialComponent.RSTextFieldOne txtiva210;
    private RSMaterialComponent.RSTextFieldOne txtiva25;
    private RSMaterialComponent.RSTextFieldOne txtiva270;
    private RSMaterialComponent.RSTextFieldOne txtiva50;
    private javax.swing.JLabel txtnombreCliente;
    private javax.swing.JFormattedTextField txtnumero;
    private javax.swing.JLabel txtsubtotal;
    private javax.swing.JLabel txttipo;
    private javax.swing.JLabel txttotal;
    private RSMaterialComponent.RSTextFieldOne txttributos;
    private javax.swing.JLabel txtusuario;
    // End of variables declaration//GEN-END:variables
}
