package vista;

import com.mxrck.autocompleter.TextAutoCompleter;
import controlador.ConexionMariaDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import static vista.CajaDiaria.listaCuenta;
import static controlador.HiloInicio.listaColegiados;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.table.DefaultTableModel;
import modelo.Caja;
import modelo.Colegiado;
import modelo.Cuenta;
import modelo.FormaDePago;
import modelo.FormaDePagoCredito;
import modelo.FormaDePagoCuentaCorriente;
import modelo.FormaDePagoDebito;
import static vista.IngresoValorizacion.datosPagos;

public class EgresoCaja extends javax.swing.JDialog {

    TextAutoCompleter textAutoAcompleterCuenta;
    TextAutoCompleter textAutoAcompleterAsociado;
    int x, y, idCuenta, idAsociado, idBanco, idFormaDePago, tipoCliente, idCuentaCorriente, idTarjeta = 0, idCheque = 0;
    String cuentaPropiedad, DetallePago, nombreCuenta;
    
    double importe;
    private modelo.ConexionMariaDB conexion;

    public EgresoCaja(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        textAutoAcompleterCuenta = new TextAutoCompleter(txtCuenta);
        textAutoAcompleterAsociado = new TextAutoCompleter(txtAsociado);
        cargarCuentas();
        tamañoTabla();
    }

    void tamañoTabla() {
        tablaIngreso.getColumnModel().getColumn(1).setMaxWidth(0);
        tablaIngreso.getColumnModel().getColumn(1).setMinWidth(0);
        tablaIngreso.getColumnModel().getColumn(1).setPreferredWidth(0);
        //////////////////////////////////////////////////////////////////
        tablaIngreso.getColumnModel().getColumn(2).setMaxWidth(0);
        tablaIngreso.getColumnModel().getColumn(2).setMinWidth(0);
        tablaIngreso.getColumnModel().getColumn(2).setPreferredWidth(0);
        //////////////////////////////////////////////////////////////////////

    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    void cargatotales() {
        double totalIngresos = 0.00, sumatoriaIngresos = 0.00;
        int contador = 0;
        DecimalFormat df = new DecimalFormat("0.00");
        int totalRowIngresos = tablaIngreso.getRowCount();

        totalRowIngresos -= 1;

        if (totalRowIngresos != -1) {
            for (int i = 0; i <= (totalRowIngresos); i++) {
                /////////////////////////////
                String x = tablaIngreso.getValueAt(i, 4).toString();
                sumatoriaIngresos = Double.valueOf(x);
                ////////////////////////////////////////////////////
                totalIngresos = Redondear(totalIngresos + sumatoriaIngresos);
                contador++;
            }
        }

        txtTotalIngresos.setText((String.valueOf((totalIngresos))));

    }

    void cargarCuentas() {
        try {
            textAutoAcompleterCuenta.removeAllItems();
            for (int i = 0; i < listaCuenta.size(); i++) {
                textAutoAcompleterCuenta.addItem(listaCuenta.get(i).toString());
            }
            textAutoAcompleterCuenta.setMode(0);
            textAutoAcompleterCuenta.setCaseSensitive(false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar cuentas. e(96)");
        }
    }

    void cargarAsociadoColegiado() {
        try {
            textAutoAcompleterAsociado.removeAllItems();
            for (int i = 0; i < listaColegiados.size(); i++) {
                textAutoAcompleterAsociado.addItem(listaColegiados.get(i).toString());
            }
            textAutoAcompleterAsociado.setMode(0);
            textAutoAcompleterAsociado.setCaseSensitive(false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar colegiados. e(109)");
        }
    }

    void blanquearDatos() {
        txtCuenta.setText("");
        // txtAsociado.setText("");
        txtImporte.setText("");
        txtdescripcion.setText("");
        idCuenta = 0;
        //idAsociado = 0;
        idBanco = 0;
        idFormaDePago = 0;
        tipoCliente = 0;
        idTarjeta = 0;
        idCuentaCorriente = 0;
        idCheque = 0;
        txtCuenta.requestFocus();
        IngresoValorizacion.importeTotal = 0.00;
        IngresoValorizacion.tipo=0;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        btnSalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnIngreso = new RSMaterialComponent.RSButtonMaterialIconUno();
        jLabel16 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaIngreso = new RSMaterialComponent.RSTableMetroCustom();
        jLabel19 = new javax.swing.JLabel();
        txtTotalIngresos = new javax.swing.JLabel();
        txtdescripcion = new RSMaterialComponent.RSTextFieldOne();
        jLabel20 = new javax.swing.JLabel();
        txtCuenta = new RSMaterialComponent.RSTextFieldOne();
        lblAsociado = new javax.swing.JLabel();
        txtAsociado = new RSMaterialComponent.RSTextFieldOne();
        btnagregar = new RSMaterialComponent.RSButtonMaterialIconUno();
        txtImporte = new RSMaterialComponent.RSTextFieldOne();
        jLabel17 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel3MouseDragged(evt);
            }
        });
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel3MousePressed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(214, 45, 32));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar.setSizeIcon(20.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(285, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 7, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 383, Short.MAX_VALUE)
        );

        jPanel4.setBackground(new java.awt.Color(66, 133, 200));

        btnSalir.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir.setText("Salir");
        btnSalir.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnSalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnSalir.setRound(20);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnIngreso.setBackground(new java.awt.Color(255, 255, 255));
        btnIngreso.setForeground(new java.awt.Color(0, 0, 0));
        btnIngreso.setText("Valorizar");
        btnIngreso.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnIngreso.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnIngreso.setForegroundText(new java.awt.Color(51, 51, 51));
        btnIngreso.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnIngreso.setRound(20);
        btnIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Descripción:");

        jPanel5.setBackground(new java.awt.Color(66, 133, 200));

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tablaIngreso.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaIngreso.setForeground(new java.awt.Color(255, 255, 255));
        tablaIngreso.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cuenta", "idCuenta", "idAsociado", "Descripción", "Importe"
            }
        ));
        tablaIngreso.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaIngreso.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaIngreso.setBorderHead(null);
        tablaIngreso.setBorderRows(null);
        tablaIngreso.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaIngreso.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaIngreso.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaIngreso.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaIngreso.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaIngreso.setGridColor(new java.awt.Color(15, 157, 88));
        tablaIngreso.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaIngreso.setShowHorizontalLines(true);
        tablaIngreso.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaIngresoMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tablaIngreso);

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Importe:");

        txtTotalIngresos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTotalIngresos.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalIngresos.setText("0.0");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTotalIngresos, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 637, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtTotalIngresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        txtdescripcion.setForeground(new java.awt.Color(51, 51, 51));
        txtdescripcion.setBorderColor(new java.awt.Color(255, 255, 255));
        txtdescripcion.setPhColor(new java.awt.Color(51, 51, 51));
        txtdescripcion.setPlaceholder("Ingrese descripción");
        txtdescripcion.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtdescripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdescripcionActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Cuenta:");

        txtCuenta.setForeground(new java.awt.Color(51, 51, 51));
        txtCuenta.setBorderColor(new java.awt.Color(255, 255, 255));
        txtCuenta.setPhColor(new java.awt.Color(51, 51, 51));
        txtCuenta.setPlaceholder("Seleccionar Cuenta");
        txtCuenta.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCuentaActionPerformed(evt);
            }
        });
        txtCuenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCuentaKeyPressed(evt);
            }
        });

        lblAsociado.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblAsociado.setForeground(new java.awt.Color(255, 255, 255));
        lblAsociado.setText("Asociado:");

        txtAsociado.setForeground(new java.awt.Color(51, 51, 51));
        txtAsociado.setBorderColor(new java.awt.Color(255, 255, 255));
        txtAsociado.setPhColor(new java.awt.Color(51, 51, 51));
        txtAsociado.setPlaceholder("Ingrese Asociado");
        txtAsociado.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtAsociado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAsociadoActionPerformed(evt);
            }
        });

        btnagregar.setBackground(new java.awt.Color(255, 255, 255));
        btnagregar.setForeground(new java.awt.Color(0, 0, 0));
        btnagregar.setText("Agregar");
        btnagregar.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnagregar.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnagregar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnagregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnagregar.setRound(20);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        txtImporte.setForeground(new java.awt.Color(51, 51, 51));
        txtImporte.setBorderColor(new java.awt.Color(255, 255, 255));
        txtImporte.setPhColor(new java.awt.Color(51, 51, 51));
        txtImporte.setPlaceholder("$0.00");
        txtImporte.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtImporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImporteActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Importe:");

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(244, 180, 0));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("Egreso");
        jLabel23.setToolTipText("");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 26, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel20)
                                .addGap(32, 32, 32)
                                .addComponent(txtCuenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(lblAsociado, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel17))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtdescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 552, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtAsociado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtImporte, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 93, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCuenta, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAsociado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtAsociado, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtdescripcion, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtImporte, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)))
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed

        dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void jPanel3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);
    }//GEN-LAST:event_jPanel3MouseDragged

    private void jPanel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MousePressed

        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jPanel3MousePressed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void tablaIngresoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaIngresoMouseClicked

    }//GEN-LAST:event_tablaIngresoMouseClicked

    private void btnIngresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresoActionPerformed
        IngresoValorizacion.importeTotal = Double.valueOf(txtTotalIngresos.getText());
        IngresoValorizacion.tipo=0;
        new IngresoValorizacion(null, true).setVisible(true);
        if (datosPagos!=null) {
            int cantidad = tablaIngreso.getRowCount(), i = 0;
            conexion = new modelo.ConexionMariaDB();
            conexion.EstablecerConexion();
            if (cantidad != -1) {
                Caja ingreso = Caja.crearCaja();
                ingreso.setTipoMovimiento(0);//Egreso
                ingreso.setIdCliente(Integer.valueOf(tablaIngreso.getValueAt(i, 1).toString()));
                ingreso.setTipoCliente(tipoCliente);
                String[][] datos = new String[tablaIngreso.getRowCount()][6];
                for (i = 0; i < cantidad; i++) {
                    datos[i][0] = tablaIngreso.getValueAt(i, 4).toString();//importe
                    datos[i][1] = tablaIngreso.getValueAt(i, 3).toString();//descripcion
                    datos[i][2] = tablaIngreso.getValueAt(i, 1).toString();//idCuenta
                }
                ingreso.setDatosMovimientos(datos);
                ingreso.setDatosMovimientosPagos(datosPagos);
                int estado = Caja.insertarIngresoCaja(conexion.getConnection(), ingreso);
                if (estado != 1) {
                    JOptionPane.showMessageDialog(null, "El movimiento se realizó con exito...");
                } else {
                    JOptionPane.showMessageDialog(null, "Error al registrar el movimiento. e(561)");
                }
                conexion.cerrarConexion();
                this.dispose();
            }
        }
    }//GEN-LAST:event_btnIngresoActionPerformed

    private void txtdescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdescripcionActionPerformed
        txtImporte.requestFocus();
    }//GEN-LAST:event_txtdescripcionActionPerformed

    private void txtImporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImporteActionPerformed
        if (!txtImporte.getText().equals("")) {
            importe = Double.valueOf(txtImporte.getText());
            btnagregar.requestFocus();
        }

    }//GEN-LAST:event_txtImporteActionPerformed

    private void txtCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCuentaActionPerformed

        Cuenta cuenta = modelo.Cuenta.buscarCuenta(txtCuenta.getText(), listaCuenta);
        if (cuenta != null) {
            nombreCuenta = cuenta.getNombre();
            idCuenta = cuenta.getId();
            cuentaPropiedad = cuenta.getPropiedad();
            if (cuentaPropiedad.equals("Colegiado")) {
                cargarAsociadoColegiado();
                tipoCliente = 1;
            }
            if (cuentaPropiedad.equals("No Colegiado")) {
                cargarAsociadoColegiado();
                tipoCliente = 2;
            }
            if (cuentaPropiedad.equals("Proveedor")) {
                cargarAsociadoColegiado();
                tipoCliente = 3;
            }
            if (cuentaPropiedad.equals("Empleado")) {
                cargarAsociadoColegiado();
                tipoCliente = 4;
            }
            if (cuentaPropiedad.equals("Obra Social")) {
                cargarAsociadoColegiado();
                tipoCliente = 5;
            }
            txtAsociado.requestFocus();
        }
    }//GEN-LAST:event_txtCuentaActionPerformed

    private void txtAsociadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAsociadoActionPerformed
        if (cuentaPropiedad.equals("Colegiado")) {
            Colegiado colegiado = Colegiado.buscarColegiadoCompleto(txtAsociado.getText(), listaColegiados);
            idAsociado = colegiado.getIdColegiados();
        }

        txtdescripcion.requestFocus();
    }//GEN-LAST:event_txtAsociadoActionPerformed


    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        DefaultTableModel temp = (DefaultTableModel) tablaIngreso.getModel();
        Object nuevo[] = {
            nombreCuenta,
            idCuenta,
            idAsociado,
            txtdescripcion.getText(),
            importe
        };
        temp.addRow(nuevo);
        blanquearDatos();
        cargatotales();
    }//GEN-LAST:event_btnagregarActionPerformed

    private void txtCuentaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCuentaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ADD) {
            btnIngreso.doClick();
        }
    }//GEN-LAST:event_txtCuentaKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnIngreso;
    private RSMaterialComponent.RSButtonMaterialIconUno btnSalir;
    private RSMaterialComponent.RSButtonMaterialIconUno btnagregar;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel lblAsociado;
    private RSMaterialComponent.RSTableMetroCustom tablaIngreso;
    private RSMaterialComponent.RSTextFieldOne txtAsociado;
    private RSMaterialComponent.RSTextFieldOne txtCuenta;
    private RSMaterialComponent.RSTextFieldOne txtImporte;
    private javax.swing.JLabel txtTotalIngresos;
    private RSMaterialComponent.RSTextFieldOne txtdescripcion;
    // End of variables declaration//GEN-END:variables
}
