package vista;

import controlador.ConexionMySQLPami;
import controlador.Funciones;
import static vista.MainPami.idmedico;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ModMed extends javax.swing.JDialog {

    int[] idespecialidades = new int[200];
    int[] idlocalidades = new int[2500];
    int[] cp = new int[2500];
    String estado;
    int[] idzonas = new int[100];

    public ModMed(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        cargarespecialidades();
        cargarlocalidades();
        cargarzonas();
        cargadatos();
        Funciones.funcionescape(this);
    }

    void cargarzonas() {
        int i = 0;
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        String sql = "SELECT * FROM zonas";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            cbozona.addItem("...");
            idzonas[i] = 0;
            i++;
            while (rs.next()) {

                idzonas[i] = rs.getInt("id");
                cbozona.addItem(rs.getString("nombre"));
                i++;

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }
    
    void cargadatos() {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        String sql = "SELECT medicos.Nombre,direccion,nombre_localidad,institucion,especialidad,matricula_prof,genero,tel_fijo,celular,mail,horario_atencion,capita,estado, descripcion,zonas.nombre   FROM medicos INNER JOIN especialidades using(id_especialidad) LEFT JOIN zonas ON zonas.id = medicos.id_zona INNER JOIN localidades using(id_localidad)  where id_Medico=" + idmedico;
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.first();
            txtnombre.setText(rs.getString("Nombre"));
            txtdireccion.setText(rs.getString("direccion"));
            cboloc.setSelectedItem(rs.getString("nombre_localidad"));
            txtinstitucion.setText(rs.getString("institucion"));
            cboesp.setSelectedItem(rs.getString("especialidad"));
            txtmatricula.setText(rs.getString("matricula_prof"));
            txtgenero.setText(rs.getString("genero"));
            txttelefono.setText(rs.getString("tel_fijo"));
            txtcelular.setText(rs.getString("celular"));
            txthorario.setText(rs.getString("horario_atencion"));
            txtcapita.setText(rs.getString("capita"));
            cboestado.setSelectedItem(rs.getString("estado"));
            txtdescripcion.setText(rs.getString("descripcion"));
            txtmail.setText(rs.getString("mail"));
            if(rs.getString("zonas.nombre")!=null){
                cbozona.setSelectedItem(rs.getString("zonas.nombre"));
            }else{
                cbozona.setSelectedIndex(0);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    void cargarlocalidades() {
        int i = 0;
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        String sql = "SELECT * FROM localidades";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            cboloc.addItem("...");
            idlocalidades[i] = 0;
            cp[i] = 0;
            i++;
            while (rs.next()) {

                idlocalidades[i] = rs.getInt("id_localidad");
                cboloc.addItem(rs.getString("nombre_localidad"));
                cp[i] = rs.getInt("codigo_postal");
                i++;

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    void cargarespecialidades() {
        int i = 0;
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        String sql = "SELECT * FROM especialidades";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            cboesp.addItem("...");
            idespecialidades[i] = 0;
            i++;
            while (rs.next()) {

                idespecialidades[i] = rs.getInt("id_Especialidad");
                cboesp.addItem(rs.getString("Especialidad"));
                i++;

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtcelular = new javax.swing.JFormattedTextField();
        cboloc = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        txtgenero = new javax.swing.JTextField();
        txtcapita = new javax.swing.JFormattedTextField();
        txtinstitucion = new javax.swing.JTextField();
        cboesp = new javax.swing.JComboBox();
        txtnombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txttelefono = new javax.swing.JFormattedTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtmatricula = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txthorario = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        cboestado = new javax.swing.JComboBox();
        txtmail = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtdescripcion = new javax.swing.JTextArea();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        cbozona = new javax.swing.JComboBox();
        btnmodificar = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Modificar Medico"));

        txtcelular.setForeground(new java.awt.Color(0, 102, 204));
        txtcelular.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("####"))));
        txtcelular.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcelularKeyPressed(evt);
            }
        });

        cboloc.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cboloc.setForeground(new java.awt.Color(0, 102, 204));
        cboloc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbolocActionPerformed(evt);
            }
        });
        cboloc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbolocKeyPressed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Celular:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Localidad:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("E-Mail:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Especialidad:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Sexo:");

        txtdireccion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdireccionKeyPressed(evt);
            }
        });

        txtgenero.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtgenero.setForeground(new java.awt.Color(0, 102, 204));
        txtgenero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtgeneroActionPerformed(evt);
            }
        });
        txtgenero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtgeneroKeyPressed(evt);
            }
        });

        txtcapita.setForeground(new java.awt.Color(0, 102, 204));
        txtcapita.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("####"))));
        txtcapita.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcapita.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcapitaKeyPressed(evt);
            }
        });

        txtinstitucion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtinstitucion.setForeground(new java.awt.Color(0, 102, 204));
        txtinstitucion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtinstitucionActionPerformed(evt);
            }
        });
        txtinstitucion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtinstitucionKeyPressed(evt);
            }
        });

        cboesp.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cboesp.setForeground(new java.awt.Color(0, 102, 204));
        cboesp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cboespKeyPressed(evt);
            }
        });

        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Cápita:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Dirección:");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Nombre:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Telefono:");

        txttelefono.setForeground(new java.awt.Color(0, 102, 204));
        txttelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("####"))));
        txttelefono.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttelefonoActionPerformed(evt);
            }
        });
        txttelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttelefonoKeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Matricula:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Institucion:");

        txtmatricula.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtmatricula.setForeground(new java.awt.Color(0, 102, 204));
        txtmatricula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Horario:");

        txthorario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txthorario.setForeground(new java.awt.Color(0, 102, 204));
        txthorario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txthorarioKeyPressed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("Estado:");

        cboestado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboestado.setForeground(new java.awt.Color(0, 102, 204));
        cboestado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ACTIVO", "BAJA", "RECESO", "FALLECIDO" }));
        cboestado.setToolTipText("");
        cboestado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboestadoActionPerformed(evt);
            }
        });
        cboestado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cboestadoKeyPressed(evt);
            }
        });

        txtmail.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtmail.setForeground(new java.awt.Color(0, 102, 204));
        txtmail.setNextFocusableComponent(txthorario);
        txtmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmailActionPerformed(evt);
            }
        });
        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmailKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmailKeyReleased(evt);
            }
        });

        txtdescripcion.setColumns(20);
        txtdescripcion.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
        txtdescripcion.setForeground(new java.awt.Color(0, 102, 204));
        txtdescripcion.setRows(2);
        txtdescripcion.setNextFocusableComponent(btnmodificar);
        txtdescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdescripcionKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(txtdescripcion);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setText("Observación:");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Zona:");

        cbozona.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbozona.setForeground(new java.awt.Color(0, 102, 204));
        cbozona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbozonaActionPerformed(evt);
            }
        });
        cbozona.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbozonaKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cboloc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cboesp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel2)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtcapita, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(txtgenero, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel7))
                                .addContainerGap(20, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtinstitucion, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel15)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbozona, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txttelefono, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
                            .addComponent(txtcelular))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtmail, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txthorario, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(cboestado, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(cboestado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(cbozona, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cboloc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtinstitucion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cboesp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(txtgenero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcapita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txthorario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnmodificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnmodificar.setText("Modificar");
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        jButton2.setText("Volver");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnmodificar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnmodificar)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        jPanel2.getAccessibleContext().setAccessibleName("Modificar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
        if (!txtnombre.getText().equals("") && !txtdireccion.getText().equals("") && !txtcapita.getText().equals("")) {
            if (!cboesp.getSelectedItem().toString().equals("...")) {

                ConexionMySQLPami mysql = new ConexionMySQLPami();
                Connection cn = mysql.Conectar();
                String sql = "UPDATE medicos SET nombre=?, direccion=?, capita=?, id_Especialidad=?,id_localidad=? , genero=?, horario_Atencion=?, matricula_prof=?, tel_fijo=?, celular=?, mail=?, institucion=?, estado=?, descripcion=?,id_zona=?  WHERE id_Medico=" + MainPami.idmedico;
                try {
                    /*PreparedStatement pst = cn.prepareStatement(sql);
                     pst.setString(1, txtnombre.getText());
                     pst.setString(2, txtdireccion.getText());
                     pst.setInt(3, Integer.valueOf(txtcapita.getText()));
                     pst.setInt(4, idespecialidades[cboesp.getSelectedIndex()]);*/
                    estado = cboestado.getSelectedItem().toString();
                    PreparedStatement pst = cn.prepareStatement(sql);
                    pst.setString(1, txtnombre.getText());
                    pst.setString(2, txtdireccion.getText());
                    pst.setInt(3, Integer.valueOf(txtcapita.getText()));
                    pst.setInt(4, idespecialidades[cboesp.getSelectedIndex()]);
                    pst.setInt(5, idlocalidades[cboloc.getSelectedIndex()]);
                    pst.setString(6, txtgenero.getText());
                    pst.setString(7, txthorario.getText());
                    pst.setString(8, txtmatricula.getText());
                    pst.setString(9, txttelefono.getText());
                    pst.setString(10, txtcelular.getText());
                    pst.setString(11, txtmail.getText());
                    pst.setString(12, txtinstitucion.getText());
                    pst.setString(13, estado);
                    pst.setString(14, txtdescripcion.getText());
                    pst.setInt(15, idzonas[cbozona.getSelectedIndex()]);
                    int n = pst.executeUpdate();

                    if (n > 0) {
                        JOptionPane.showMessageDialog(null, "El Medico se modifico Correctamente");
                        this.dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "Error al modificar el médico");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AltaMedicos.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar una especialidad");
            }

        } else {
            JOptionPane.showMessageDialog(null, "Debe completar todos los campos");
        }

    }//GEN-LAST:event_btnmodificarActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void cbolocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbolocActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbolocActionPerformed

    private void txtgeneroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtgeneroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtgeneroActionPerformed

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtnombre.transferFocus();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyPressed

    private void txtdireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtdireccion.transferFocus();
        }        // TODO add your handling code here:
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdireccionKeyPressed

    private void txtinstitucionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtinstitucionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            txtinstitucion.transferFocus();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_txtinstitucionKeyPressed

    private void txtmatriculaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmatricula.transferFocus();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_txtmatriculaKeyPressed

    private void txtgeneroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtgeneroKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtgenero.transferFocus();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_txtgeneroKeyPressed

    private void txttelefonoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelefonoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txttelefono.transferFocus();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_txttelefonoKeyPressed

    private void txtcelularKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcelularKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtcelular.transferFocus();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_txtcelularKeyPressed

    private void txthorarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txthorarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txthorario.transferFocus();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_txthorarioKeyPressed

    private void txtcapitaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcapitaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtcapita.transferFocus();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_txtcapitaKeyPressed

    private void cbolocKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbolocKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cboloc.transferFocus();
        } // TODO add your handling code here:
    }//GEN-LAST:event_cbolocKeyPressed

    private void cboespKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboespKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cboesp.transferFocus();
        } // T// TODO add your handling code here:
    }//GEN-LAST:event_cboespKeyPressed

    private void cboestadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboestadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboestadoActionPerformed

    private void cboestadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboestadoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboestadoKeyPressed

    private void txtinstitucionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtinstitucionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtinstitucionActionPerformed

    private void txtmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmailActionPerformed

    private void txtmailKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmailKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmail.transferFocus();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_txtmailKeyPressed

    private void txtdescripcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdescripcionKeyPressed
          if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtdescripcion.transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_txtdescripcionKeyPressed

    private void txttelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttelefonoActionPerformed

    private void cbozonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbozonaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbozonaActionPerformed

    private void cbozonaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbozonaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbozonaKeyPressed

    private void txtmailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmailKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmailKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnmodificar;
    private javax.swing.JComboBox cboesp;
    private javax.swing.JComboBox cboestado;
    private javax.swing.JComboBox cboloc;
    private javax.swing.JComboBox cbozona;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JFormattedTextField txtcapita;
    private javax.swing.JFormattedTextField txtcelular;
    private javax.swing.JTextArea txtdescripcion;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtgenero;
    private javax.swing.JTextField txthorario;
    private javax.swing.JTextField txtinstitucion;
    private javax.swing.JTextField txtmail;
    private javax.swing.JTextField txtmatricula;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JFormattedTextField txttelefono;
    // End of variables declaration//GEN-END:variables
}
