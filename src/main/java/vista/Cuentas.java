package vista;

import controlador.Funciones;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Cuenta;
import static vista.CuentasPrincipales.idCuenta;

public class Cuentas extends javax.swing.JDialog {

    int x, y;
    private modelo.ConexionMariaDB conexion;
    DefaultTableModel modelCuenta;
    public static ArrayList<modelo.Cuenta> listaCuenta;

    public Cuentas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        cargarCuenta();
        cargarTablaCuenta();
        radioCuentaNo.setSelected(true);
        radioCuentaSi.setSelected(false);
    }

    void cargarCuenta() {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaCuenta = new ArrayList<modelo.Cuenta>();
        modelo.Cuenta.cargarCuenta(conexion.getConnection(), listaCuenta);
        conexion.cerrarConexion();

    }

    void cargarTablaCuenta() {

        String[] Titulo = {"id", "Número", "Nombre"};
        String[] Registros = new String[3];

        modelCuenta = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (int i = 0; i < listaCuenta.size(); i++) {

            Registros[0] = String.valueOf(listaCuenta.get(i).getId());
            Registros[1] = listaCuenta.get(i).getNumero() + "";
            Registros[2] = listaCuenta.get(i).getNombre();
            modelCuenta.addRow(Registros);

        }
        tablaCuentas.setModel(modelCuenta);
        tablaCuentas.setAutoCreateRowSorter(true);

        tablaCuentas.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaCuentas.getColumnModel().getColumn(0).setMinWidth(0);
        tablaCuentas.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaCuentas.getColumnModel().getColumn(1).setPreferredWidth(50);
        tablaCuentas.getColumnModel().getColumn(2).setPreferredWidth(100);

    }

    void aplicarCambios() {

        Calendar calendar = new GregorianCalendar();
        Date fecha = new Date();
        Cuenta cuenta = new Cuenta();
        int sub = 0;
        Funciones.stringAdate(calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH));
        cuenta.setPropiedad(cboPropiedad.getSelectedItem().toString());
        cuenta.setNumero(Long.valueOf(txtNumero.getText()));
        cuenta.setNombre(txtNombre.getText());
        cuenta.setBancoNombre(txtBanco.getText());
        cuenta.setTipo(cboTipo.getSelectedItem().toString());
        cuenta.setTransferencia(cboTransferencia.getSelectedItem().toString());
        cuenta.setCbu(txtCbu.getText());
        cuenta.setCuit(Long.valueOf(txtCuit.getText()));
        cuenta.setEstado(1);
        cuenta.setIdUsuario(Login.idusuario);
        cuenta.setFecha(fecha);

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        sub = Cuenta.insertarCuenta(conexion.getConnection(), cuenta);
        System.out.println(sub);
        if (sub > 0) {
            Cuenta.insertarSubCuenta(conexion.getConnection(), idCuenta, sub);
        }
        conexion.cerrarConexion();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoCuenta = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNombre = new RSMaterialComponent.RSTextFieldOne();
        txtNumero = new RSMaterialComponent.RSTextFieldOne();
        jLabel4 = new javax.swing.JLabel();
        txtBanco = new RSMaterialComponent.RSTextFieldOne();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtCbu = new RSMaterialComponent.RSTextFieldOne();
        jLabel8 = new javax.swing.JLabel();
        txtCuit = new RSMaterialComponent.RSTextFieldOne();
        cboPropiedad = new RSMaterialComponent.RSComboBox();
        cboTipo = new RSMaterialComponent.RSComboBox();
        cboTransferencia = new RSMaterialComponent.RSComboBox();
        jLabel9 = new javax.swing.JLabel();
        radioCuentaSi = new RSMaterialComponent.RSRadioButtonMaterial();
        radioCuentaNo = new RSMaterialComponent.RSRadioButtonMaterial();
        jPanel3 = new javax.swing.JPanel();
        btnSalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnAgregar = new RSMaterialComponent.RSButtonMaterialIconUno();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaCuentas = new RSMaterialComponent.RSTableMetroCustom();
        jPanel5 = new javax.swing.JPanel();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Número:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nombre:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Propiedad:");

        txtNombre.setForeground(new java.awt.Color(51, 51, 51));
        txtNombre.setBorderColor(new java.awt.Color(255, 255, 255));
        txtNombre.setPhColor(new java.awt.Color(51, 51, 51));
        txtNombre.setPlaceholder("Ingrese nombre de cuenta");
        txtNombre.setSelectionColor(new java.awt.Color(244, 180, 0));

        txtNumero.setForeground(new java.awt.Color(51, 51, 51));
        txtNumero.setBorderColor(new java.awt.Color(255, 255, 255));
        txtNumero.setPhColor(new java.awt.Color(51, 51, 51));
        txtNumero.setPlaceholder("Ingrese número de cuenta");
        txtNumero.setSelectionColor(new java.awt.Color(244, 180, 0));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Banco:");

        txtBanco.setForeground(new java.awt.Color(51, 51, 51));
        txtBanco.setBorderColor(new java.awt.Color(255, 255, 255));
        txtBanco.setPhColor(new java.awt.Color(51, 51, 51));
        txtBanco.setPlaceholder("Ingrese nombre de cuenta");
        txtBanco.setSelectionColor(new java.awt.Color(244, 180, 0));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Tipo cuenta:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Transferencia:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("CBU:");

        txtCbu.setForeground(new java.awt.Color(51, 51, 51));
        txtCbu.setBorderColor(new java.awt.Color(255, 255, 255));
        txtCbu.setPhColor(new java.awt.Color(51, 51, 51));
        txtCbu.setPlaceholder("Ingrese CBU");
        txtCbu.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtCbu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCbuKeyTyped(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("CUIT:");

        txtCuit.setForeground(new java.awt.Color(51, 51, 51));
        txtCuit.setBorderColor(new java.awt.Color(255, 255, 255));
        txtCuit.setPhColor(new java.awt.Color(51, 51, 51));
        txtCuit.setPlaceholder("Ingrese CUIT");
        txtCuit.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtCuit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCuitKeyTyped(evt);
            }
        });

        cboPropiedad.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Empleado", "Colegiado", "Colegio", "Proveedores" }));
        cboPropiedad.setColorArrow(new java.awt.Color(66, 133, 200));
        cboPropiedad.setColorBorde(new java.awt.Color(255, 255, 255));
        cboPropiedad.setColorBoton(new java.awt.Color(255, 255, 255));
        cboPropiedad.setColorFondo(new java.awt.Color(66, 133, 200));
        cboPropiedad.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboPropiedad.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboPropiedad.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cboTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Caja de ahorro pesos", "Caja de ahorro dólares", "Cuenta corriente pesos", "Cuenta corriente dólares" }));
        cboTipo.setColorArrow(new java.awt.Color(66, 133, 200));
        cboTipo.setColorBorde(new java.awt.Color(66, 133, 200));
        cboTipo.setColorBoton(new java.awt.Color(255, 255, 255));
        cboTipo.setColorFondo(new java.awt.Color(66, 133, 200));
        cboTipo.setColorListaItemsTXT(new java.awt.Color(51, 51, 51));
        cboTipo.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboTipo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cboTransferencia.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cuenta sueldo", "Judicial", "Caja" }));
        cboTransferencia.setColorArrow(new java.awt.Color(66, 133, 200));
        cboTransferencia.setColorBorde(new java.awt.Color(66, 133, 200));
        cboTransferencia.setColorBoton(new java.awt.Color(255, 255, 255));
        cboTransferencia.setColorFondo(new java.awt.Color(66, 133, 200));
        cboTransferencia.setColorSeleccion(new java.awt.Color(66, 133, 200));
        cboTransferencia.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Subcuenta:");

        grupoCuenta.add(radioCuentaSi);
        radioCuentaSi.setForeground(new java.awt.Color(255, 255, 255));
        radioCuentaSi.setText("Si");
        radioCuentaSi.setColorCheck(new java.awt.Color(255, 255, 255));
        radioCuentaSi.setColorUnCheck(new java.awt.Color(255, 255, 255));
        radioCuentaSi.setRippleColor(new java.awt.Color(244, 180, 0));

        grupoCuenta.add(radioCuentaNo);
        radioCuentaNo.setForeground(new java.awt.Color(255, 255, 255));
        radioCuentaNo.setText("No");
        radioCuentaNo.setColorCheck(new java.awt.Color(255, 255, 255));
        radioCuentaNo.setColorUnCheck(new java.awt.Color(255, 255, 255));
        radioCuentaNo.setRippleColor(new java.awt.Color(244, 180, 0));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtCbu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNumero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtBanco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboPropiedad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(10, 10, 10)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cboTransferencia, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                            .addComponent(cboTipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCuit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(radioCuentaSi, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(radioCuentaNo, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboPropiedad, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBanco, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboTransferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCbu, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCuit, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(radioCuentaSi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioCuentaNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49))
        );

        jPanel3.setBackground(new java.awt.Color(66, 133, 200));

        btnSalir.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir.setText("Salir");
        btnSalir.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnSalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnSalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnSalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnSalir.setRound(20);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnAgregar.setBackground(new java.awt.Color(255, 255, 255));
        btnAgregar.setForeground(new java.awt.Color(0, 0, 0));
        btnAgregar.setText("Agregar");
        btnAgregar.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnAgregar.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnAgregar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnAgregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnAgregar.setRound(20);
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tablaCuentas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaCuentas.setForeground(new java.awt.Color(255, 255, 255));
        tablaCuentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaCuentas.setBackgoundHead(new java.awt.Color(66, 133, 200));
        tablaCuentas.setBackgoundHover(new java.awt.Color(66, 133, 200));
        tablaCuentas.setBorderHead(null);
        tablaCuentas.setBorderRows(null);
        tablaCuentas.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaCuentas.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaCuentas.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaCuentas.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaCuentas.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaCuentas.setGridColor(new java.awt.Color(255, 255, 255));
        tablaCuentas.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaCuentas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaCuentasMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tablaCuentas);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5)
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel5MouseDragged(evt);
            }
        });
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel5MousePressed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(219, 68, 55));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar.setSizeIcon(20.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(418, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 378, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed

        if (!txtNumero.getText().equals("") && !txtNombre.getText().equals("") && !txtBanco.getText().equals("") && !txtCbu.getText().equals("") && !txtCuit.getText().equals("")) {

            if (radioCuentaSi.isSelected()) {
                new CuentasPrincipales(null, true).setVisible(true);
                if (idCuenta != 0) {
                    aplicarCambios();
                    cargarCuenta();
                    cargarTablaCuenta();
                } else {
                    JOptionPane.showMessageDialog(null, "No hay una cuenta principal asociada");
                }
            } else {
                aplicarCambios();
                cargarCuenta();
                cargarTablaCuenta();
            }

        } else {
            JOptionPane.showMessageDialog(null, "Debe completar todos los campos");
        }

    }//GEN-LAST:event_btnAgregarActionPerformed

    private void tablaCuentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaCuentasMouseClicked

    }//GEN-LAST:event_tablaCuentasMouseClicked

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed

        dispose();

    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();

    }//GEN-LAST:event_btnSalirActionPerformed

    private void jPanel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MousePressed

        x = evt.getX();
        y = evt.getY();

    }//GEN-LAST:event_jPanel5MousePressed

    private void jPanel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);

    }//GEN-LAST:event_jPanel5MouseDragged

    private void txtCbuKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCbuKeyTyped

        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }

    }//GEN-LAST:event_txtCbuKeyTyped

    private void txtCuitKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCuitKeyTyped

        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }

    }//GEN-LAST:event_txtCuitKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonMaterialIconUno btnAgregar;
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnSalir;
    private RSMaterialComponent.RSComboBox cboPropiedad;
    private RSMaterialComponent.RSComboBox cboTipo;
    private RSMaterialComponent.RSComboBox cboTransferencia;
    private javax.swing.ButtonGroup grupoCuenta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane5;
    private RSMaterialComponent.RSRadioButtonMaterial radioCuentaNo;
    private RSMaterialComponent.RSRadioButtonMaterial radioCuentaSi;
    private RSMaterialComponent.RSTableMetroCustom tablaCuentas;
    private RSMaterialComponent.RSTextFieldOne txtBanco;
    private RSMaterialComponent.RSTextFieldOne txtCbu;
    private RSMaterialComponent.RSTextFieldOne txtCuit;
    private RSMaterialComponent.RSTextFieldOne txtNombre;
    private RSMaterialComponent.RSTextFieldOne txtNumero;
    // End of variables declaration//GEN-END:variables
}
