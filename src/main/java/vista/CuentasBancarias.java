package vista;

import controlador.ConexionMariaDB;
import controlador.Funciones;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Image;
import java.sql.*;
import java.text.*;
import java.util.ArrayList;
import javax.swing.*;
import controlador.Localidad;
import javax.swing.table.DefaultTableModel;
import modelo.Banco;
import modelo.Facturantes;
import modelo.TipoCuenta;
import modelo.TipoTransferencia;

public class CuentasBancarias extends javax.swing.JDialog {

    DefaultTableModel model;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    String pic = "";
    int id_colegiado = 0;
    Image imagen2;
    TextAutoCompleter textAutoAcompleter;
    ArrayList<Localidad> cp = new ArrayList<>();
    Localidad local;
    Funciones efechas = new Funciones();
    String essociedad = "", factura = "", atravescolegio = "";
    DefaultListModel listModelBio = new DefaultListModel();
    DefaultListModel listModelDt = new DefaultListModel();
    DefaultListModel listModelFac = new DefaultListModel();
    private modelo.ConexionMariaDB conexion;
    public static ArrayList <modelo.TipoCuenta> listaCuentas;
    public static ArrayList <modelo.TipoTransferencia> listaTransferencias;
    public static ArrayList <Facturantes> listaFacturantes;
    public static ArrayList <Banco> listaBancos;
    TextAutoCompleter autoFact, autoBanco;

    public CuentasBancarias(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Cuentas bancarias");
        setLocationRelativeTo(null);
        autoFact = new TextAutoCompleter(txtFacturante);
        autoBanco = new TextAutoCompleter(txtEntidadBancaria);
        cargar();
        cargarTablaBanco();
        radioActivo.setSelected(true);      
    }

   

    boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public void cargar() {
        
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaCuentas = new ArrayList<>();
        TipoCuenta.cargarTipoCuenta(conexion.getConnection(), listaCuentas);
        listaTransferencias = new ArrayList<>();
        TipoTransferencia.cargarTipoTransferencia(conexion.getConnection(), listaTransferencias);
        listaFacturantes = new ArrayList<>();
        Facturantes.cargarFacturante(conexion.getConnection(), listaFacturantes);
        listaBancos = new ArrayList<>();
        Banco.cargarBanco(conexion.getConnection(), listaBancos);
        conexion.cerrarConexion();
        for(TipoCuenta tipo : listaCuentas){
            cboTipoCuenta.addItem(tipo.getNombre());
        }
        for(TipoTransferencia tipo : listaTransferencias){
            cboTipoTransferencia.addItem(tipo.getNombre());
        }
           autoFact.setItems(listaFacturantes.toArray());
           autoFact.setMode(0);
           autoBanco.setItems(listaBancos.toArray());
           autoBanco.setMode(0);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoBotonBanco = new javax.swing.ButtonGroup();
        jPanel8 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        txtEntidadBancaria = new javax.swing.JTextField();
        cboTipoCuenta = new javax.swing.JComboBox<>();
        cboTipoTransferencia = new javax.swing.JComboBox<>();
        txtNumeroCuenta = new javax.swing.JTextField();
        txtCBU = new javax.swing.JTextField();
        txtCuitDestino = new javax.swing.JTextField();
        radioActivo = new javax.swing.JRadioButton();
        radioInactivo = new javax.swing.JRadioButton();
        btnAgregar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel47 = new javax.swing.JLabel();
        txtFacturante = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaBancos = new javax.swing.JTable();
        btnSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Cuentas"));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Nº de cuenta:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("CBU:");

        jLabel45.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel45.setText("CUIT de destino:");

        jLabel46.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel46.setText("Entidad bancaria:");

        jLabel50.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel50.setText("Tipo de cuenta:");

        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel51.setText("Tipo de transferencia:");

        jLabel52.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel52.setText("Estado:");

        txtEntidadBancaria.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtEntidadBancaria.setForeground(new java.awt.Color(0, 102, 204));
        txtEntidadBancaria.setNextFocusableComponent(cboTipoCuenta);

        cboTipoCuenta.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboTipoCuenta.setForeground(new java.awt.Color(0, 102, 204));

        cboTipoTransferencia.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboTipoTransferencia.setForeground(new java.awt.Color(0, 102, 204));
        cboTipoTransferencia.setNextFocusableComponent(txtNumeroCuenta);

        txtNumeroCuenta.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtNumeroCuenta.setForeground(new java.awt.Color(0, 102, 204));
        txtNumeroCuenta.setNextFocusableComponent(txtCBU);
        txtNumeroCuenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumeroCuentaKeyReleased(evt);
            }
        });

        txtCBU.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtCBU.setForeground(new java.awt.Color(0, 102, 204));
        txtCBU.setNextFocusableComponent(txtCuitDestino);
        txtCBU.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCBUKeyReleased(evt);
            }
        });

        txtCuitDestino.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtCuitDestino.setForeground(new java.awt.Color(0, 102, 204));
        txtCuitDestino.setNextFocusableComponent(radioActivo);
        txtCuitDestino.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCuitDestinoKeyReleased(evt);
            }
        });

        grupoBotonBanco.add(radioActivo);
        radioActivo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioActivo.setText("Activo");

        grupoBotonBanco.add(radioInactivo);
        radioInactivo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioInactivo.setText("Inactivo");

        btnAgregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnAgregar.setText("Agregar");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setToolTipText("");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jLabel47.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel47.setText("Facturante:");

        txtFacturante.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtFacturante.setForeground(new java.awt.Color(0, 102, 204));
        txtFacturante.setNextFocusableComponent(txtEntidadBancaria);

        tablaBancos.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaBancos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablaBancos);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAgregar)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFacturante))
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnCancelar)
                        .addGroup(jPanel8Layout.createSequentialGroup()
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel52, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel45, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel46, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtNumeroCuenta))
                                .addGroup(jPanel8Layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel8Layout.createSequentialGroup()
                                            .addComponent(radioActivo)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(radioInactivo))
                                        .addComponent(cboTipoCuenta, 0, 176, Short.MAX_VALUE)
                                        .addComponent(cboTipoTransferencia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGroup(jPanel8Layout.createSequentialGroup()
                                    .addGap(4, 4, 4)
                                    .addComponent(txtEntidadBancaria))
                                .addGroup(jPanel8Layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtCBU))
                                .addGroup(jPanel8Layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtCuitDestino))))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel47)
                            .addComponent(txtFacturante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel46)
                            .addComponent(txtEntidadBancaria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel50)
                            .addComponent(cboTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel51)
                            .addComponent(cboTipoTransferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtNumeroCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txtCBU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel45)
                            .addComponent(txtCuitDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel52)
                            .addComponent(radioActivo)
                            .addComponent(radioInactivo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 20, Short.MAX_VALUE)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAgregar)
                            .addComponent(btnCancelar)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnSalir.setMnemonic('v');
        btnSalir.setText("Salir");
        btnSalir.setToolTipText("Alt + v");
        btnSalir.setPreferredSize(new java.awt.Dimension(108, 23));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 902, Short.MAX_VALUE)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(7, 7, 7)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        txtFacturante.setText(null);
        txtEntidadBancaria.setText(null);
        cboTipoCuenta.setSelectedIndex(0);
        cboTipoTransferencia.setSelectedIndex(0);
        txtNumeroCuenta.setText(null);
        txtCBU.setText(null);
        txtCuitDestino.setText(null);
        radioActivo.setSelected(true);
        
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed

    
        
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void txtNumeroCuentaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroCuentaKeyReleased

        if(!isNumeric(txtNumeroCuenta.getText())){
            txtNumeroCuenta.setText(null);
        }
        
    }//GEN-LAST:event_txtNumeroCuentaKeyReleased

    private void txtCBUKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCBUKeyReleased

        if(!isNumeric(txtCBU.getText())){
            txtCBU.setText(null);
        }
        
    }//GEN-LAST:event_txtCBUKeyReleased

    private void txtCuitDestinoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCuitDestinoKeyReleased

       if(!isNumeric(txtCuitDestino.getText())){
            txtCuitDestino.setText(null);
        }
        
    }//GEN-LAST:event_txtCuitDestinoKeyReleased

    void cargarTablaBanco() {

        ConexionMariaDB CN = new ConexionMariaDB();
        Connection con = CN.Conectar();
        String[] titulo = {"Banco", "Nº Cuenta", "CBU", "CUIT Destino", "Tipo cuenta", "Tipo de Transferencia", "Estado"};

        String[] filas = new String[7];
       
        model = new DefaultTableModel(null, titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        try {
            String ConsultaMatricula = "Select * from  vista_banco_tipo";

            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaMatricula);

            while (RS.next()) {
                filas[0] = RS.getString("BancoNombre");
                filas[1] = RS.getString("NumeroCuenta");
                filas[2] = RS.getString("CBU");
                filas[3] = RS.getString("cuitDestino");
                filas[4] = RS.getString("CuentaNombre");
                filas[5] = RS.getString("TransferenciaNombre");
                
                if (RS.getInt("Estado") == 1) {
                    filas[6] = "OK";
                } else {
                    filas[6] = "Baja";
                }
                model.addRow(filas);
            }
            tablaBancos.setModel(model);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox<String> cboTipoCuenta;
    private javax.swing.JComboBox<String> cboTipoTransferencia;
    private javax.swing.ButtonGroup grupoBotonBanco;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton radioActivo;
    private javax.swing.JRadioButton radioInactivo;
    private javax.swing.JTable tablaBancos;
    private javax.swing.JTextField txtCBU;
    private javax.swing.JTextField txtCuitDestino;
    private javax.swing.JTextField txtEntidadBancaria;
    private javax.swing.JTextField txtFacturante;
    private javax.swing.JTextField txtNumeroCuenta;
    // End of variables declaration//GEN-END:variables
}
