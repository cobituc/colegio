package vista;

import controlador.ConexionMariaDB;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class PeriodoColegiado extends javax.swing.JDialog {

    public static String mes = "", año = "";

    public PeriodoColegiado(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        buttonGroup1.add(bton_colegiado);
        buttonGroup1.add(bton_todos);
        buttonGroup2.add(bton_abrirPeriodo);
        buttonGroup2.add(bton_cerrarPeriodo);
        this.setLocationRelativeTo(null);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                mes = "";
                año = "";
                Modificar_Orden.periododjj = "";
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtaño = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        txtcolegiado = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        txtnombreColegiado = new javax.swing.JTextField();
        bton_colegiado = new javax.swing.JRadioButton();
        bton_todos = new javax.swing.JRadioButton();
        bton_abrirPeriodo = new javax.swing.JRadioButton();
        bton_cerrarPeriodo = new javax.swing.JRadioButton();
        btnatras = new javax.swing.JButton();
        btnatras1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Período a Finalizar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Periodo:");

        txtaño.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtaño.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("######")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtaño.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtaño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtañoActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Matricula:");

        txtcolegiado.setForeground(new java.awt.Color(0, 102, 204));
        txtcolegiado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        txtcolegiado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcolegiado.setNextFocusableComponent(txtnombreColegiado);
        txtcolegiado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcolegiadoActionPerformed(evt);
            }
        });
        txtcolegiado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcolegiadoKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Colegiado:");

        txtnombreColegiado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnombreColegiado.setForeground(new java.awt.Color(0, 102, 204));
        txtnombreColegiado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreColegiadoKeyPressed(evt);
            }
        });

        bton_colegiado.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        bton_colegiado.setText("Colegiado");
        bton_colegiado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bton_colegiadoActionPerformed(evt);
            }
        });

        bton_todos.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        bton_todos.setText("Todos");
        bton_todos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bton_todosActionPerformed(evt);
            }
        });

        bton_abrirPeriodo.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        bton_abrirPeriodo.setText("Abrir Periodo");
        bton_abrirPeriodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bton_abrirPeriodoActionPerformed(evt);
            }
        });

        bton_cerrarPeriodo.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        bton_cerrarPeriodo.setText("Cerrar Periodo");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bton_colegiado)
                            .addComponent(bton_abrirPeriodo))
                        .addGap(50, 50, 50)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bton_todos)
                            .addComponent(bton_cerrarPeriodo))
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtnombreColegiado)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtcolegiado, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 200, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bton_colegiado)
                    .addComponent(bton_todos))
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bton_abrirPeriodo)
                    .addComponent(bton_cerrarPeriodo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtcolegiado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtnombreColegiado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        btnatras.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnatras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnatras.setMnemonic('c');
        btnatras.setText("Volver");
        btnatras.setToolTipText("Alt + c");
        btnatras.setPreferredSize(new java.awt.Dimension(108, 23));
        btnatras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnatrasActionPerformed(evt);
            }
        });

        btnatras1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnatras1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728934 - enter login right.png"))); // NOI18N
        btnatras1.setMnemonic('c');
        btnatras1.setText("Aceptar");
        btnatras1.setToolTipText("Alt + c");
        btnatras1.setNextFocusableComponent(btnatras);
        btnatras1.setPreferredSize(new java.awt.Dimension(108, 23));
        btnatras1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnatras1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnatras1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnatras, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnatras, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnatras1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtañoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtañoActionPerformed
        año = txtaño.getText();
        String periodo = txtaño.getText();
        txtaño.transferFocus();
    }//GEN-LAST:event_txtañoActionPerformed

    private void txtcolegiadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcolegiadoActionPerformed
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        int i = 0, periodo = 0;
        String periodos;

        try {
            String sSQL = "SELECT id_colegiados,matricula_colegiado,nombre_colegiado,validador,periodos FROM colegiados where matricula_colegiado=" + txtcolegiado.getText() + " and tipo_profesional='B'";
            String sSQL1 = "select periodo from periodos";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            Statement st1 = cn.createStatement();
            ResultSet rs1 = st1.executeQuery(sSQL1);
            rs1.next();
            if (bton_colegiado.isSelected()) {
                if (rs.next()) {
                    txtnombreColegiado.setText(rs.getString("nombre_colegiado"));
                    txtnombreColegiado.setEnabled(false);
                    txtaño.transferFocus();
                } else {
                    JOptionPane.showMessageDialog(null, "Matricula erronea o el validador no puede realizar ordenes para esta matricula...");
                    txtcolegiado.requestFocus();
                    i = 1;
                }
                if (i == 0) {
                    if (rs.getInt("periodos") == rs1.getInt("periodo")) {
                        JOptionPane.showMessageDialog(null, "El Periodo se encuentra abierto");

                        periodo = Integer.valueOf(rs1.getString("periodo").substring(3, 5));
                        System.out.println(periodo);
                        if (periodo == 12) {
                            periodo = Integer.valueOf(rs1.getString("periodo").substring(0, 4)) + 1;
                            periodos = String.valueOf(periodo) + "01";
                            txtaño.setText(periodos);
                        } else {
                            periodo = rs.getInt("periodos") + 1;
                            txtaño.setText(String.valueOf(periodo));
                            txtaño.setEditable(false);
                        }

                    } else if (rs.getInt("periodos") > rs1.getInt("periodo")) {
                        JOptionPane.showMessageDialog(null, "El Periodo se encuentra cerrado, va a actualizar ");
                        txtaño.setEnabled(false);
                    }
                }
            }
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Error en la base de datos");
        }

    }//GEN-LAST:event_txtcolegiadoActionPerformed

    private void btnatrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnatrasActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnatrasActionPerformed

    private void btnatras1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnatras1ActionPerformed
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        int n = 0;

        if ((bton_colegiado.isSelected() || bton_todos.isSelected()) && (bton_cerrarPeriodo.isSelected() || bton_abrirPeriodo.isSelected())) {
            try {
                if (bton_colegiado.isSelected()) {
                    if (bton_cerrarPeriodo.isSelected()) {
                        String sSQL = "update colegiados set periodos=?, estado_periodo=0, estado_facturacion=0 where matricula_colegiado=?";
                        PreparedStatement pst = cn.prepareStatement(sSQL);
                        pst.setString(1, txtaño.getText());
                        pst.setString(2, txtcolegiado.getText());
                        n = pst.executeUpdate();
                        if (n > 0) {
                            JOptionPane.showMessageDialog(null, "Periodo cerrado con exito, para generar la DDJJ aguarde 3 minuto");
                        } else {
                            JOptionPane.showMessageDialog(null, "No se pudo cerrar el periodo, informe al dpto de Computos");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "No posee permisos para abrir periodos");
                    }
                } else if (bton_cerrarPeriodo.isSelected()) {
                    String sSQL = "update colegiados set periodos=?, estado_periodo=0, estado_facturacion=0 ";
                    PreparedStatement pst = cn.prepareStatement(sSQL);
                    pst.setString(1, txtaño.getText());
                    pst.executeUpdate();
                } else {
                    JOptionPane.showMessageDialog(null, "No posee permisos para abrir periodos");
                }

            } catch (SQLException ex) {
                Logger.getLogger(PeriodoColegiado.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe elegir alguna opción");
        }

    }//GEN-LAST:event_btnatras1ActionPerformed

    private void txtcolegiadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcolegiadoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtcolegiado.transferFocus();
        }
    }//GEN-LAST:event_txtcolegiadoKeyPressed

    private void txtnombreColegiadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreColegiadoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtnombreColegiado.transferFocus();
        }
    }//GEN-LAST:event_txtnombreColegiadoKeyPressed

    private void bton_colegiadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bton_colegiadoActionPerformed
        txtcolegiado.setEnabled(true);
    }//GEN-LAST:event_bton_colegiadoActionPerformed

    private void bton_abrirPeriodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bton_abrirPeriodoActionPerformed

    }//GEN-LAST:event_bton_abrirPeriodoActionPerformed

    private void bton_todosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bton_todosActionPerformed
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        int i = 0, periodo = 0;
        String periodos;
        try {
            String sSQL1 = "select periodo from periodos";
            Statement st1 = cn.createStatement();
            ResultSet rs1 = st1.executeQuery(sSQL1);
            rs1.next();
            txtcolegiado.setEnabled(false);
            txtnombreColegiado.setEnabled(false);
            periodo = Integer.valueOf(rs1.getString("periodo").substring(3, 5));
            if (periodo == 12) {
                periodo = Integer.valueOf(rs1.getString("periodo").substring(0, 4)) + 1;
                periodos = String.valueOf(periodo) + "01";
                txtaño.setText(periodos);
            } else {
                periodo = rs1.getInt("periodo") + 1;
                txtaño.setText(String.valueOf(periodo));
                txtaño.setEditable(false);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Error en la base de datos");
        }
    }//GEN-LAST:event_bton_todosActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnatras;
    private javax.swing.JButton btnatras1;
    private javax.swing.JRadioButton bton_abrirPeriodo;
    private javax.swing.JRadioButton bton_cerrarPeriodo;
    private javax.swing.JRadioButton bton_colegiado;
    private javax.swing.JRadioButton bton_todos;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JFormattedTextField txtaño;
    private javax.swing.JFormattedTextField txtcolegiado;
    private javax.swing.JTextField txtnombreColegiado;
    // End of variables declaration//GEN-END:variables
}
