package vista;

import controlador.ConexionMariaDB;
import controlador.ConexionMySQLBackup;
import controlador.HiloInicio;
import controlador.Numerico;
import controlador.camposordenes11;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.table.DefaultTableModel;

public class ArchivoObraSociales extends javax.swing.JDialog {

    int contadorj = 0, id_usuario_validador, periodo_colegiado, id_usuario;
    String año, mes, periodo, colegiado, fecha2, periodo2;
    TextAutoCompleter textAutoAcompleter;

    Hiloobrasocialtodas hilo3;
    public static String ruta = "C:\\Archivos-Obras-Sociales\\";
    DefaultTableModel model2;
    Numerico num = new Numerico();

    public ArchivoObraSociales(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        progreso.setVisible(false);
        textAutoAcompleter = new TextAutoCompleter(txtobrasocial);
        textAutoAcompleter.addItems(HiloInicio.listaOS.toArray());
        //  traerobrasosial();

        cargarperiodo();
        // cargarobrasocial();
        this.setLocationRelativeTo(null);
    }

    public class Hiloobrasocialtodas extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialtodas(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {

            btnimprimir.setEnabled(false);
            ///////////////////facturacion/////////////////////////////////////
            int cantidad = 100 / 10;
            String errores = "";
            String total_pesos_letras, total_centavos_letras, domicilio_lab, localidad_lab;
            int indice = 1; //indice contara los numeros primos que vamos encontrando             
            //////////////////////////////////////////////////////////////            
            ConexionMariaDB mysql = new ConexionMariaDB();
            Connection cn = mysql.Conectar();
            ///////////////////////////////////////////////////////////////////////
            ConexionMariaDB mysql2 = new ConexionMariaDB();
            Connection cn2 = mysql2.Conectar();
            ///////////////////////////////////////////////////////////////////
            LinkedList<camposordenes11> Resultados3 = new LinkedList<camposordenes11>();
            Resultados3.clear();
            String orden = "", nombre_colegiado = "", uni_gastos = "", uni_Honorarios = "";
            int n = 0, ordenes = 0, ordenesgral = 0, practicasgral = 0, band = 0, practicas = 0, bandera = 0, obrasocial = 0, colegiado = 0;
            double total = 0.00, pesos = 0, importetotal = 0, centavos = 0;
            int pacientes = 0;
            DecimalFormat df = new DecimalFormat("0.00");
            periodo2 = txtaño.getText() + txtmes.getText();

            if (Integer.valueOf(periodo2) >= 201612) {
                System.out.println(periodo2);
                if (txtobrasocial.getText().equals("510 - MEDIFE - OBLIGATORIO- PRE PAGA C.M.C.  S.A.")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                    String[] Registros = new String[23];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "";
                    try {

                        File archivo = new File(ruta + "Transferencia-" + txtobrasocial.getText().substring(0, 12) + "-" + periodo2 + "-.txt");
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            archivo.delete();
                        }

                        FileWriter escribir = new FileWriter(archivo, true);
                        Statement st3 = cn.createStatement();
                        ResultSet rs3 = st3.executeQuery("SELECT LPAD(colegiados.matricula_colegiado,6,'0'),LPAD(ordenes.matricula_prescripcion,6,'0'),\n"
                                + "concat(substring((REPLACE(ordenes.fecha_orden,'/','')),5,4),substring((REPLACE(ordenes.fecha_orden,'/','')),3,2),substring((REPLACE(ordenes.fecha_orden,'/','')),1,2)),\n"
                                + "LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,1),3,'0'),\n"
                                + "LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),2,9),8,'0'),\n"
                                + "LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),10,11),2,'0'),\n"
                                + "LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),12,14),3,'0'),\n"
                                + "LPAD(concat(detalle_ordenes.cod_practica,ordenes.tipo_orden),6,'0'),\n"
                                + "LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),12,'0')\n"
                                + "FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados\n"
                                + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden\n"
                                + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial\n"
                                + "WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and detalle_ordenes.estado=0 \n"
                                + "and (ordenes.id_obrasocial=4 or ordenes.id_obrasocial=107) order by ordenes.id_obrasocial, (CAST(colegiados.matricula_colegiado as UNSIGNED)) ,ordenes.id_orden,detalle_ordenes.id_detalle ");

                        while (rs3.next()) {
                            //String num_afiliado = completarnumafiliadoMEDIFE(rs3.getString(8));
                            Registros[0] = completarceros(String.valueOf(txtfactura.getText()), 8);
                            Registros[1] = "06";
                            Registros[2] = "T";
                            //Registros[3] = completarceros(rs3.getString(1), 6);
                            Registros[3] = rs3.getString(1);
                            // Registros[4] = completarespaciosapellido(rs3.getString(2), 20);
                            Registros[4] = "                                        ";
                            // Registros[5] = "                    ";
                            Registros[5] = "01";
                            Registros[6] = "T";
                            //Registros[8] = completarceros(rs3.getString(9), 6);
                            Registros[7] = rs3.getString(2);
                            Registros[8] = "                                        ";
                            //Registros[10] = "                    ";
                            Registros[9] = rs3.getString(3);//AAAAMMDD
                            Registros[10] = rs3.getString(4);//3
                            Registros[11] = rs3.getString(5);//8
                            Registros[12] = rs3.getString(6);//2
                            Registros[13] = rs3.getString(7);//3
                            Registros[14] = rs3.getString(3); //AAAAMMDD;
                            Registros[15] = "AMB";
                            Registros[16] = "003";
                            Registros[17] = rs3.getString(8);
                            Registros[18] = "00000001";
                            Registros[19] = rs3.getString(9);
                            //Registros[21] = rs3.getString(24);
                            Registros[20] = "TOD";
                            escribir.write(Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + "\r\n");

                            model2.addRow(Registros);

                        }
                        escribir.close();
                        rs3.close();
                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        temp.setModel(model2);
                        ///////////////////////////////////////////////////

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }
                //////////////////////////////////////////////////////////////////////////////////////////////
                if (txtobrasocial.getText().equals("511 - MEDIFE - VOLUNTARIO- PRE PAGA C.M.C.  S.A.")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                    String[] Registros = new String[23];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "";
                    try {

                        File archivo = new File(ruta + "Transferencia-" + txtobrasocial.getText().substring(0, 12) + "-" + periodo2 + "-.txt");
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            archivo.delete();
                        }

                        FileWriter escribir = new FileWriter(archivo, true);
                        Statement st3 = cn.createStatement();
                        ResultSet rs3 = st3.executeQuery("SELECT LPAD(colegiados.matricula_colegiado,6,'0'),LPAD(ordenes.matricula_prescripcion,6,'0'),\n"
                                + "concat(substring((REPLACE(ordenes.fecha_orden,'/','')),5,4),substring((REPLACE(ordenes.fecha_orden,'/','')),3,2),substring((REPLACE(ordenes.fecha_orden,'/','')),1,2)),\n"
                                + "LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,1),3,'0'),\n"
                                + "LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),2,9),8,'0'),\n"
                                + "LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),10,11),2,'0'),\n"
                                + "LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),12,14),3,'0'),\n"
                                + "LPAD(concat(detalle_ordenes.cod_practica,ordenes.tipo_orden),6,'0'),\n"
                                + "LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),12,'0')\n"
                                + "FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados\n"
                                + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden\n"
                                + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial\n"
                                + "WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and detalle_ordenes.estado=0 \n"
                                + "and (ordenes.id_obrasocial=97 or ordenes.id_obrasocial=116) order by ordenes.id_obrasocial, (CAST(colegiados.matricula_colegiado as UNSIGNED)) ,ordenes.id_orden,detalle_ordenes.id_detalle ");

                        while (rs3.next()) {
                            //String num_afiliado = completarnumafiliadoMEDIFE(rs3.getString(8));
                            Registros[0] = completarceros(String.valueOf(txtfactura.getText()), 8);
                            Registros[1] = "06";
                            Registros[2] = "T";
                            //Registros[3] = completarceros(rs3.getString(1), 6);
                            Registros[3] = rs3.getString(1);
                            // Registros[4] = completarespaciosapellido(rs3.getString(2), 20);
                            Registros[4] = "                                        ";
                            // Registros[5] = "                    ";
                            Registros[5] = "01";
                            Registros[6] = "T";
                            //Registros[8] = completarceros(rs3.getString(9), 6);
                            Registros[7] = rs3.getString(2);
                            Registros[8] = "                                        ";
                            //Registros[10] = "                    ";
                            Registros[9] = rs3.getString(3);//AAAAMMDD
                            Registros[10] = rs3.getString(4);//3
                            Registros[11] = rs3.getString(5);//8
                            Registros[12] = rs3.getString(6);//2
                            Registros[13] = rs3.getString(7);//3
                            Registros[14] = rs3.getString(3); //AAAAMMDD;
                            Registros[15] = "AMB";
                            Registros[16] = "003";
                            Registros[17] = rs3.getString(8);
                            Registros[18] = "00000001";
                            Registros[19] = rs3.getString(9);
                            //Registros[21] = rs3.getString(24);
                            Registros[20] = "TOD";
                            escribir.write(Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + "\r\n");

                            model2.addRow(Registros);

                        }
                        escribir.close();
                        rs3.close();
                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        temp.setModel(model2);
                        ///////////////////////////////////////////////////

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }

                /////////////////////////////////////////////Boreal
                if (txtobrasocial.getText().equals("50015 - BOREAL") || txtobrasocial.getText().equals("50015 - BOREAL")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                    String[] Registros = new String[23];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "";
                    try {

                        File archivo = new File(ruta + "Transferencia-BOREAL-" + periodo2 + "-.txt");
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            archivo.delete();
                        }

                        FileWriter escribir = new FileWriter(archivo, true);
                        Statement st3 = cn.createStatement();
                        ResultSet rs3 = st3.executeQuery("SELECT LPAD(REPLACE((REPLACE((REPLACE(ordenes.numero_orden,'-','')),' ','')),'/',''),10,'0'),LPAD(ordenes.dni_afiliado,8,'0'),LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,12),12,'0'),\n"
                                + "substring((REPLACE(ordenes.fecha_orden,'/','')),1,2),substring((REPLACE(ordenes.fecha_orden,'/','')),3,2),substring((REPLACE(ordenes.fecha_orden,'/','')),5,4),\n"
                                + "RPAD(detalle_ordenes.nombre_practica,40,' '),detalle_ordenes.cod_practica, LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),10,'0'),\n"
                                + "LPAD(ordenes.matricula_prescripcion,6,'0'),LPAD(colegiados.matricula_colegiado,6,'0')\n"
                                + "FROM ordenes\n"
                                + "INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados\n"
                                + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden\n"
                                + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial\n"
                                + "WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and detalle_ordenes.estado=0 and (ordenes.id_obrasocial=89 or ordenes.id_obrasocial=95)\n"
                                + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED)),ordenes.id_orden");

                        while (rs3.next()) {
                            //String num_afiliado = completarnumafiliadoMEDIFE(rs3.getString(8));
                            Registros[0] = "P";
                            Registros[1] = "FCEMA";
                            Registros[2] = completarceros(String.valueOf(txtpuntoventa.getText()), 4);
                            Registros[3] = completarceros(String.valueOf(txtfactura.getText()), 8);
                            Registros[4] = rs3.getString(1);////numero de orden
                            Registros[5] = "P";
                            Registros[6] = rs3.getString(2);////DNI
                            Registros[7] = rs3.getString(3);////N° Afiliado ;
                            Registros[8] = rs3.getString(4); //DD
                            Registros[9] = rs3.getString(5); //MM
                            Registros[10] = rs3.getString(6);//AAAA
                            Registros[11] = rs3.getString(7);//PRACTICA DESCRIPCION
                            Registros[12] = rs3.getString(8);//CODIGO PRACTICA
                            Registros[13] = rs3.getString(9);//IMPORTE
                            Registros[14] = "1";
                            Registros[15] = rs3.getString(10); //MATRICULA PRESC
                            Registros[16] = rs3.getString(11); //MATRICULA BIOQ
                            escribir.write(Registros[0] + ";" + Registros[1] + ";" + Registros[2] + ";" + Registros[3] + ";" + Registros[4] + ";" + Registros[5] + ";" + Registros[6] + ";" + Registros[7] + ";;" + Registros[8] + ";" + Registros[9] + ";" + Registros[10] + ";" + Registros[11] + ";" + Registros[12] + ";" + Registros[13] + ";" + Registros[14] + ";" + Registros[15] + ";;" + Registros[16] + "\r\n");

                            model2.addRow(Registros);

                        }
                        escribir.close();
                        rs3.close();
                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        temp.setModel(model2);

                        //////////////////////////////////////////////////////////////////////////////
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }

                ////////////////////////////////////////////
                if (txtobrasocial.getText().equals("340 - OSSEG - OBRA SOCIAL SEGUROS Y REASEGUROS")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                    String[] Registros = new String[23];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "";
                    try {
                        File archivo = new File(ruta + "Osseg" + mes + año.substring(2, 4) + " " + completarceros(String.valueOf(txtfactura.getText()), 6) + ".txt");
                        //Osseg0814 17348.txt 
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            archivo.delete();
                        }

                        FileWriter escribir = new FileWriter(archivo, true);

                        Statement st3 = cn.createStatement();
                        ResultSet rs3 = st3.executeQuery("SELECT RPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,12),20,' '),\n"
                                + "concat(substring((REPLACE(ordenes.fecha_orden,'/','')),5,4),substring((REPLACE(ordenes.fecha_orden,'/','')),3,2),substring((REPLACE(ordenes.fecha_orden,'/','')),1,2)),\n"
                                + "LPAD(ordenes.matricula_prescripcion,6,'0') ,LPAD(colegiados.matricula_colegiado,6,'0'),\n"
                                + "detalle_ordenes.cod_practica,LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),10,'0'), LPAD(substring(ordenes.id_orden,1,8),8,'0') \n"
                                + "FROM ordenes\n"
                                + "INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados\n"
                                + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden\n"
                                + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial\n"
                                + "WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and detalle_ordenes.estado=0 and ordenes.id_obrasocial=2");

                        //JOptionPane.showMessageDialog(null, "1");
                        int i = 1;
                        String i2 = "";

                        while (rs3.next()) {

                            Registros[0] = completarceros(String.valueOf(txtfactura.getText()), 6);//num fac
                            i2 = String.valueOf(i);
                            Registros[1] = completarceros(i2, 8);/////contador linea
                            Registros[2] = "416";////????????
                            // Registros[3] = completarnumorden(rs3.getString(10), 8);//////num de orden
                            Registros[3] = rs3.getString(7);
                            Registros[4] = "O";
                            Registros[5] = rs3.getString(1);
                            Registros[6] = "";
                            Registros[7] = rs3.getString(2);
                            Registros[8] = rs3.getString(2);
                            Registros[9] = "";
                            Registros[10] = "   ";
                            Registros[11] = rs3.getString(3);//matricula medica
                            Registros[12] = rs3.getString(4);//matricula Bioq
                            Registros[13] = rs3.getString(5);//cod_practica
                            Registros[14] = "  ";//2
                            Registros[15] = "01";
                            Registros[16] = rs3.getString(6);//importe sin punto
                            Registros[17] = "      ";
                            Registros[18] = "S ";
                            Registros[19] = "N";
                            Registros[20] = " 11705";
                            Registros[21] = "M";
                            Registros[22] = "";

                            escribir.write(Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + "\r\n");
                            //errores = errores + Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + "\r\n";
                            model2.addRow(Registros);
                            i++;
                        }

                        temp.setModel(model2);
                        ///////////////////////////////////////////////////
                        escribir.close();
                        rs3.close();
                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        //////////////////////////////////////////////////////////////////////////////
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }
                ///////////////////////////////ASUNT

                if (txtobrasocial.getText().equals("2700 - UNT - Accion  Social de la  UNT")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                    String[] Registros = new String[23];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "";
                    try {
                        File archivo = new File(ruta + "ASUNT_" + mes + "-" + año + ".txt");
                        //Osseg0814 17348.txt 
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            archivo.delete();
                        }

                        FileWriter escribir = new FileWriter(archivo, true);

                        Statement st3 = cn.createStatement();
                        ResultSet rs3 = st3.executeQuery("SELECT LPAD(30522483881,20,' ') as FACTURADOR,LPAD((REPLACE((REPLACE((REPLACE(ordenes.dni_afiliado,'-','')),' ','')),'/','')),20,' ') as AFILIADO,\n"
                                + "ordenes.fecha_orden AS FECHA,IF(LENGTH(CAST(numero_orden as UNSIGNED))>8,LPAD(CAST(numero_orden as UNSIGNED),13,' '),LPAD(concat('3',LPAD(CAST(numero_orden as UNSIGNED),10,'0')),13,' ')) as BONO  \n"
                                + ",LPAD(cod_practica,10,' ') as PRESTACION,LPAD(0,4,0)AS COMPONENTE, LPAD(count(cod_practica),10,'0') as CANTIDAD,\n"
                                + " LPAD(cuil_colegiado,20,' ') as EFECTOR,LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),15,'0') as IMPORTE, LPAD(REPLACE((detalle_ordenes.precio_practica*count(cod_practica)),'.',''),15,'0') as TOTAL,\n"
                                + " IF(LENGTH(CAST(numero_orden as UNSIGNED))>8,LPAD(CAST(substring(CAST(numero_orden as UNSIGNED),1,2)as UNSIGNED),4,0),LPAD(3,4,0)) as TIPO_ORDEN,\n"
                                + " LPAD(0,4,0),0, LPAD(0,10,0)\n"
                                + " FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados\n"
                                + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden \n"
                                + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial \n"
                                + "where periodo=" + periodo2 + " and obrasocial.codigo_obrasocial=2700  AND (ordenes.id_colegiados!=1212 AND ordenes.id_colegiados!=1315 AND ordenes.id_colegiados!=1214)"
                                + "and estado_orden=1 \n"
                                + "group by numero_orden, afiliado,matricula_colegiado,cod_practica \n"
                                + "order by matricula_colegiado,ordenes.id_orden");
                        System.out.println("ingresa : " + periodo2);
                        while (rs3.next()) {
                            Registros[0] = rs3.getString("FACTURADOR");//FACTURADOR
                            Registros[1] = rs3.getString("AFILIADO");/////contador linea
                            Registros[2] = rs3.getString("FECHA");////????????
                            Registros[3] = rs3.getString("BONO");
                            Registros[4] = rs3.getString("PRESTACION");
                            Registros[5] = rs3.getString("COMPONENTE");
                            Registros[6] = rs3.getString("CANTIDAD");
                            Registros[7] = rs3.getString("EFECTOR");
                            Registros[8] = rs3.getString("importe");
                            Registros[9] = rs3.getString("TOTAL");
                            Registros[10] = rs3.getString("TOTAL");//FACTURADO
                            if (rs3.getInt("TIPO_ORDEN") > 15) {
                                Registros[11] = "000" + String.valueOf(rs3.getInt("TIPO_ORDEN") / 10);//matricula medica
                            } else {
                                Registros[11] = rs3.getString("TIPO_ORDEN");
                            }
                            Registros[12] = rs3.getString("LPAD(0,4,0)");//matricula Bioq
                            Registros[13] = rs3.getString("0");//cod_practica
                            Registros[14] = rs3.getString("LPAD(0,10,0)");

                            escribir.write(Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + "\r\n");
                            //errores = errores + Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + "\r\n";
                            model2.addRow(Registros);

                        }

                        temp.setModel(model2);
                        ///////////////////////////////////////////////////
                        escribir.close();
                        rs3.close();
                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        //////////////////////////////////////////////////////////////////////////////
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }

///////////////////////////////OSPE 
                if (txtobrasocial.getText().equals("40600 - OSPE- OBRA SOCIAL DE PETROLEROS") || txtobrasocial.getText().equals("40601 - OSPE- OBRA SOCIAL DE PETROLEROS -OFFLINE")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                    String[] Registros = new String[23];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "";
                    
                    try {
                        
                        String Ruta = "";
                        
                        if (txtobrasocial.getText().equals("40600 - OSPE- OBRA SOCIAL DE PETROLEROS")) {
                            Ruta = ruta + "OSPE-40600" + mes + "-" + año + ".txt";
                            
                        } else {
                            Ruta = ruta + "OSPE-40601" + mes + "-" + año + ".txt";
                        }
                        
                        File archivo = new File(Ruta);
                        //Osseg0814 17348.txt 
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            archivo.delete();
                        }

                        FileWriter escribir = new FileWriter(archivo, true);

                        Statement st3 = cn.createStatement();
                        ResultSet rs3 = st3.executeQuery("SELECT ordenes.fecha_orden AS rea_fecha,\n"
                                + "LPAD(' ',10,' ') as orden_fecha1,\n"
                                + "LPAD(' ',10,' ') as ori_matri_naci,\n"
                                + "LPAD(matricula_prescripcion,10,' ') as ori_matri_prov,\n"
                                + "'15' as ori_matri_prov_provin,\n"
                                + "LPAD(' ',100,' ') as ori_nombre,\n"
                                + "substring(LPAD((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),11,' '),1,11) as contra,\n"
                                + "substring(LPAD((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),13,' '),13,1) as inte, \n"
                                + "LPAD(' ',4,' ') as sucur,\n"
                                + "LPAD(' ',8,' ') as auto, \n"
                                + "LPAD(cod_practica,10,' ') as prestac,\n"
                                + "LPAD(count(cod_practica),3,' ') as canti,\n"
                                + "LPAD(round(detalle_ordenes.precio_practica*count(cod_practica),2),9,' ') as ingre_valor ,\n"
                                + "LPAD(' ',10,' ') as efe_matri_naci, \n"
                                + "LPAD(colegiados.matricula_colegiado,10,' ') as efe_matri_prov,\n"
                                + "'15' as  efe_matri_prov_provin \n"
                                + "FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados\n"
                                + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden \n"
                                + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial \n"
                                + "where periodo=" + periodo2 + " and (obrasocial.codigo_obrasocial=40600 or obrasocial.codigo_obrasocial=40601)  AND (ordenes.id_colegiados!=1212 AND ordenes.id_colegiados!=1315 AND ordenes.id_colegiados!=1214)"
                                + "and estado_orden=1 \n"
                                + "group by numero_orden, numero_afiliado,matricula_colegiado,cod_practica \n"
                                + "order by (CAST(colegiados.matricula_colegiado as UNSIGNED)),ordenes.id_orden ");

                        System.out.println("ingresa : " + periodo2);
                        while (rs3.next()) {
                            Registros[0] = rs3.getString("rea_fecha");//FACTURADOR
                            Registros[1] = rs3.getString("orden_fecha1");/////contador linea
                            Registros[2] = rs3.getString("ori_matri_naci");////????????
                            Registros[3] = rs3.getString("ori_matri_prov");
                            Registros[4] = rs3.getString("ori_matri_prov_provin");
                            Registros[5] = rs3.getString("ori_nombre");
                            Registros[6] = rs3.getString("contra");
                            Registros[7] = rs3.getString("inte");
                            Registros[8] = rs3.getString("sucur");
                            Registros[9] = rs3.getString("auto");
                            Registros[10] = rs3.getString("prestac");//FACTURADO
                            Registros[11] = rs3.getString("canti");
                            Registros[12] = rs3.getString("ingre_valor");
                            Registros[13] = rs3.getString("efe_matri_naci");
                            Registros[14] = rs3.getString("efe_matri_prov");
                            Registros[15] = rs3.getString("efe_matri_prov_provin");

                            escribir.write(Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + "\r\n");
                            //errores = errores + Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + "\r\n";
                            model2.addRow(Registros);

                        }

                        temp.setModel(model2);
                        ///////////////////////////////////////////////////
                        escribir.close();
                        rs3.close();
                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        //////////////////////////////////////////////////////////////////////////////
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }

                System.out.println(" antes de entrar");
                ////////////////////////////Subsidio de Saludo
                if (txtobrasocial.getText().equals("1800 - SUBSIDIO DE SALUD - IPSST")) {
                    System.out.println("entra");
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12"};
                    //"12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};
                    String[] Registros = new String[12];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "", linea_txt = "";

                    try {
                        //////////////Creo el Archivo siempre en try catch
                        int peri;
                        peri = Integer.valueOf(año) - 2000;
                        File archivo = new File(ruta + "000071" + peri + "." + mes);

                        int ban = 0, ban1 = 0;
                        String n_orden = null;
                        FileWriter escribir = new FileWriter(archivo, true);
                        Statement st3 = cn.createStatement();

                        ResultSet rs3 = st3.executeQuery("SELECT if(ordenes.id_obrasocial=100 or ordenes.id_obrasocial=103,LPAD((CAST(ordenes.numero_orden as UNSIGNED)),10,'0'), LPAD(Truncate((CAST((REPLACE((REPLACE((REPLACE(ordenes.numero_orden ,'-','')),' ','')),'/','' )) AS UNSIGNED)/100),0),10,'0')) as numero_orden,\n"
                                + "LPAD(substring(REPLACE((REPLACE((REPLACE(ordenes.dni_afiliado,'-','')),' ','')),'/',''),1,8),8,'0'),\n"
                                + "if(LENGTH(REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/',''))=11,'',lpad(substring(REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/',''),1,2),2,'0')),\n"
                                + "if(LENGTH(REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/',''))=11,'',lpad(substring(REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/',''),3,6),6,'0')),\n"
                                + "if(LENGTH(REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/',''))=11,'',lpad(substring(REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/',''),9,2),2,'0')),\n"
                                + "LPAD(colegiados.matricula_colegiado,6,'0'),substring((REPLACE(ordenes.fecha_orden,'/','')),1,2),substring((REPLACE(ordenes.fecha_orden,'/','')),3,2),substring((REPLACE(ordenes.fecha_orden,'/','')),5,4),\n"
                                + "detalle_ordenes.cod_practica as cod_practica,LPAD(REPLACE(detalle_ordenes.precio_practica,'.',','),8,'0'),count(cod_practica) as cantidad, LPAD(REPLACE((precio_practica*count(cod_practica)),'.',','),8,'0') as precio_total\n"
                                + "FROM ordenes\n"
                                + "INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados\n"
                                + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden\n"
                                + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial\n"
                                + "WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and matricula_colegiado!=99999 and detalle_ordenes.estado=0 and\n"
                                + "(ordenes.id_obrasocial=11 or ordenes.id_obrasocial=12 or \n"
                                + "ordenes.id_obrasocial=14 or ordenes.id_obrasocial=15 or ordenes.id_obrasocial=16 or\n"
                                + "ordenes.id_obrasocial=90 or ordenes.id_obrasocial=100 or ordenes.id_obrasocial=103)\n"
                                + "group by numero_orden, ordenes.id_orden,matricula_colegiado,cod_practica\n"
                                + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED)),ordenes.id_orden");
                        int i = 0;
                        System.out.println("Antes del if");
                        while (rs3.next()) {
                            System.out.println(i++);

                            ban = 0;
                            if (ban == 0) {
                                Registros[0] = rs3.getString(1);//N° DE ORDEN
                                Registros[1] = rs3.getString(2);//DNI AFILIADO

                                if (isNumeric(rs3.getString(3))) {
                                    Registros[2] = rs3.getString(3);//GRUPO
                                } else {
                                    Registros[2] = "";
                                }
                                if (isNumeric(rs3.getString(4))) {
                                    Registros[3] = rs3.getString(4);//AFILIADO
                                } else {
                                    Registros[3] = "";
                                }
                                if (isNumeric(rs3.getString(5))) {
                                    Registros[4] = rs3.getString(5);//MIEMBRO
                                } else {
                                    Registros[4] = "";
                                }
                                Registros[5] = rs3.getString(6);//MATRICULA BIOQ
                                Registros[6] = rs3.getString(7);//DIA
                                Registros[7] = rs3.getString(8);//MES

                                if (Integer.valueOf(rs3.getString(9)) <= Integer.valueOf(año) && Integer.valueOf(rs3.getString(9)) > Integer.valueOf(año) - 1) {
                                    Registros[8] = rs3.getString(9);//AÑO
                                } else {
                                    Registros[8] = "2020";
                                }
                                Registros[9] = rs3.getString(10);// PRACTICA
//                                if (rs3.getString(10).equals("661135")) {
//                                    Registros[9] = "661024";// PRACTICA
//                                }
                                if (rs3.getString(10).equals("661105")) {
                                    Registros[9] = "666264";
                                }
//                                if (rs3.getString(10).equals("669443")) {
//                                    Registros[9] = "669433";
//                                }

                                Registros[10] = rs3.getString("precio_total");//IMPORTE
                                Registros[11] = rs3.getString("cantidad");

                                escribir.write(Registros[0] + ";P;" + Registros[1] + ";" + Registros[2] + ";" + Registros[3] + ";" + Registros[4] + ";" + Registros[5] + ";"
                                        + Registros[6] + ";" + Registros[7] + ";" + Registros[8] + ";" + Registros[9] + ";" + Registros[10] + ";" + Registros[11] + ";;;\r\n");//+ ";01;;;\r\n");

                                model2.addRow(Registros);

                            }

                        }

                        escribir.close();

                        temp.setModel(model2);
                        ///////////////////////////////////////////////////
                        btnimprimir.setEnabled(true);
                        //////////////////////////////////////////////////////////////////////////////
                        rs3.close();
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }

                }

                if (txtobrasocial.getText().equals("3100 - OSDE") || txtobrasocial.getText().equals("3101 - OSDE  ( RESPONSABLES INSCRIPTOS)") || txtobrasocial.getText().equals("3102 - OSDE - OFFLINE")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};
                    String[] Registros = new String[30];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "", linea_txt = "";

                    try {
                        File archivo = new File(ruta + "Transferencia_" + txtobrasocial.getText() + "-" + periodo2 + "-.txt");
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            archivo.delete();
                        }

                        FileWriter escribir = new FileWriter(archivo, true);
                        String sql = "", sql1 = "";
                        int osde_3102 = 0;

                        if (txtobrasocial.getText().equals("3101 - OSDE  ( RESPONSABLES INSCRIPTOS)")) {

                            sql = "SELECT LPAD(colegiados.codigo_osde,6,'0'),LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,11),11,'0'),\n"
                                    + "LPAD(concat(detalle_ordenes.cod_practica,ordenes.tipo_orden),7,'0'),\n"
                                    + "if(ordenes.id_obrasocial=24,concat(substring((REPLACE(ordenes.fecha,'/','')),1,4),substring((REPLACE(ordenes.fecha,'/','')),7,2)),concat(substring((REPLACE(ordenes.fecha_orden,'/','')),1,4),substring((REPLACE(ordenes.fecha_orden,'/','')),7,2))),\n"
                                    + "LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),15,'0'),LPAD(ordenes.matricula_prescripcion,10,'0'),LPAD(colegiados.matricula_colegiado,10,'0'),\n"
                                    + "substring(ordenes.id_orden,2,10)\n"
                                    + "FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial \n"
                                    + "WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and ordenes.tipo_orden=3  and detalle_ordenes.estado=0 and (ordenes.id_obrasocial=24 or ordenes.id_obrasocial=94 )  \n"
                                    + " and str_to_date(ordenes.fecha_orden,'%d/%m/%Y')> '2020-07-08'"
                                    + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED)),ordenes.id_orden";
                        }
                        if (txtobrasocial.getText().equals("3100 - OSDE")) {
                            sql = "SELECT LPAD(colegiados.codigo_osde,6,'0'),LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,11),11,'0'),\n"
                                    + "LPAD(concat(detalle_ordenes.cod_practica,ordenes.tipo_orden),7,'0'),\n"
                                    + "if(ordenes.id_obrasocial=24,concat(substring((REPLACE(ordenes.fecha,'/','')),1,4),substring((REPLACE(ordenes.fecha,'/','')),7,2)),concat(substring((REPLACE(ordenes.fecha_orden,'/','')),1,4),substring((REPLACE(ordenes.fecha_orden,'/','')),7,2))),\n"
                                    + "LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),15,'0'),LPAD(ordenes.matricula_prescripcion,10,'0'),LPAD(colegiados.matricula_colegiado,10,'0'),\n"
                                    + "substring(ordenes.id_orden,2,10)\n"
                                    + "FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial= ordenes.id_obrasocial\n"
                                    + "WHERE ordenes.periodo=" + periodo2 + "  and ordenes.estado_orden=1 and ordenes.tipo_orden!=3  and detalle_ordenes.estado=0 and ordenes.id_obrasocial=24 \n"
                                    + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED)),ordenes.id_orden";

                            sql1 = "SELECT LPAD(colegiados.codigo_osde,6,'0'),LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,11),11,'0') AS numero_afiliado,\n"
                                    + "LPAD(concat(detalle_ordenes.cod_practica,ordenes.tipo_orden),7,'0'),\n"
                                    + "if(ordenes.id_obrasocial=24,concat(substring((REPLACE(ordenes.fecha,'/','')),1,4),substring((REPLACE(ordenes.fecha,'/','')),7,2)),concat(substring((REPLACE(ordenes.fecha_orden,'/','')),1,4),substring((REPLACE(ordenes.fecha_orden,'/','')),7,2))),\n"
                                    + "LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),15,'0'),LPAD(ordenes.matricula_prescripcion,10,'0'),LPAD(colegiados.matricula_colegiado,10,'0'),\n"
                                    + "substring(ordenes.id_orden,2,10)\n"
                                    + "FROM ordenes \n"
                                    + "INNER JOIN (SELECT DISTINCT(matricula_colegiado), id_colegiados FROM ordenes INNER JOIN colegiados using (id_colegiados) \n"
                                    + "WHERE  ordenes.estado_orden=1 and ordenes.tipo_orden!=3  and id_obrasocial=24 and periodo=" + periodo2 + " \n"
                                    + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED))) as osde using (id_colegiados)\n"
                                    + "INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados \n"
                                    + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden \n"
                                    + "INNER JOIN obrasocial ON obrasocial.id_obrasocial= ordenes.id_obrasocial\n"
                                    + "WHERE ordenes.periodo=" + periodo2 + "   and ordenes.estado_orden=1 and ordenes.tipo_orden!=3  and detalle_ordenes.estado=0 and ordenes.id_obrasocial=94 \n"
                                    + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED)),ordenes.id_orden ";
                            osde_3102 = 1;
                        }
                        if (txtobrasocial.getText().equals("3102 - OSDE - OFFLINE")) {

                            sql = "SELECT LPAD(colegiados.codigo_osde,6,'0'),LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,11),11,'0'),\n"
                                    + "LPAD(concat(detalle_ordenes.cod_practica,ordenes.tipo_orden),7,'0'),\n"
                                    + "if(ordenes.id_obrasocial=24,concat(substring((REPLACE(ordenes.fecha,'/','')),1,4),substring((REPLACE(ordenes.fecha,'/','')),7,2)),concat(substring((REPLACE(ordenes.fecha_orden,'/','')),1,4),substring((REPLACE(ordenes.fecha_orden,'/','')),7,2))),\n"
                                    + "LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),15,'0'),LPAD(ordenes.matricula_prescripcion,10,'0'),LPAD(colegiados.matricula_colegiado,10,'0'),\n"
                                    + "substring(ordenes.id_orden,2,10)\n"
                                    + "FROM ordenes \n"
                                    + "LEFT JOIN (SELECT DISTINCT(matricula_colegiado), id_colegiados FROM ordenes INNER JOIN colegiados using (id_colegiados) \n"
                                    + "WHERE  ordenes.estado_orden=1 and ordenes.tipo_orden!=3  and id_obrasocial=24 and periodo=" + periodo2 + " order by(CAST(colegiados.matricula_colegiado as UNSIGNED))) as osde using (id_colegiados)\n"
                                    + "INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados \n"
                                    + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden \n"
                                    + "INNER JOIN obrasocial ON obrasocial.id_obrasocial= ordenes.id_obrasocial\n"
                                    + "WHERE  osde.id_colegiados is null and ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and ordenes.tipo_orden!=3  and detalle_ordenes.estado=0 and ordenes.id_obrasocial=94  \n"
                                    + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED)),ordenes.id_orden ";

                        }
                        Statement st3 = cn.createStatement();
                        ResultSet rs3 = st3.executeQuery(sql);
                        Statement st4 = null;
                        ResultSet rs4 = null;
                        if (osde_3102 == 1) {
                            st4 = cn.createStatement();
                            rs4 = st4.executeQuery(sql1);
                        }

                        while (rs3.next()) {

                            Registros[0] = " ";
                            Registros[1] = rs3.getString(1);
                            Registros[2] = "   ";
                            Registros[3] = rs3.getString(2);
                            Registros[4] = "   ";
                            Registros[5] = rs3.getString(3);
                            Registros[6] = "           ";
                            Registros[7] = "001";
                            Registros[8] = "   ";
                            Registros[9] = rs3.getString(4);
                            Registros[10] = "   ";
                            Registros[11] = "00";
                            Registros[12] = "            ";
                            Registros[13] = "0";
                            Registros[14] = "   ";
                            Registros[15] = rs3.getString(5);
                            Registros[16] = "   ";
                            Registros[17] = "M";
                            Registros[18] = "   ";
                            Registros[19] = rs3.getString(6);
                            Registros[20] = "   ";
                            Registros[21] = "T";
                            Registros[22] = "   ";
                            Registros[23] = "B";
                            Registros[24] = "   ";
                            Registros[25] = rs3.getString(7);
                            Registros[26] = "   ";
                            Registros[27] = "T";
                            Registros[28] = "            ";
                            Registros[29] = rs3.getString(8);//8
                            escribir.write(Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + Registros[23] + Registros[24] + Registros[25] + Registros[26] + Registros[27] + Registros[28] + Registros[29] + "\r\n");

                            model2.addRow(Registros);
                        }
                        if (osde_3102 == 1) {
                            while (rs4.next()) {

                                Registros[0] = " ";
                                Registros[1] = rs4.getString(1);
                                Registros[2] = "   ";
                                Registros[3] = rs4.getString(2);
                                Registros[4] = "   ";
                                Registros[5] = rs4.getString(3);
                                Registros[6] = "           ";
                                Registros[7] = "001";
                                Registros[8] = "   ";
                                Registros[9] = rs4.getString(4);
                                Registros[10] = "   ";
                                Registros[11] = "00";
                                Registros[12] = "            ";
                                Registros[13] = "0";
                                Registros[14] = "   ";
                                Registros[15] = rs4.getString(5);
                                Registros[16] = "   ";
                                Registros[17] = "M";
                                Registros[18] = "   ";
                                Registros[19] = rs4.getString(6);
                                Registros[20] = "   ";
                                Registros[21] = "T";
                                Registros[22] = "   ";
                                Registros[23] = "B";
                                Registros[24] = "   ";
                                Registros[25] = rs4.getString(7);
                                Registros[26] = "   ";
                                Registros[27] = "T";
                                Registros[28] = "            ";
                                Registros[29] = rs4.getString(8);//8
                                escribir.write(Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + Registros[23] + Registros[24] + Registros[25] + Registros[26] + Registros[27] + Registros[28] + Registros[29] + "\r\n");

                                model2.addRow(Registros);
                            }
                        }

                        escribir.close();

                        temp.setModel(model2);
                        ///////////////////////////////////////////////////

                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        //////////////////////////////////////////////////////////////////////////////
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }

            } else {

                if (txtobrasocial.getText().equals("510 - MEDIFE - PRE PAGA C.M.C.  S.A.")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                    String[] Registros = new String[23];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "";
                    try {
                        Statement st3 = cn2.createStatement();
                        ResultSet rs3 = st3.executeQuery("SELECT colegiados.matricula_colegiado, colegiados.nombre_colegiado ,ordenes.*, obrasocial.codigo_obrasocial, detalle_ordenes.* FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and detalle_ordenes.estado=0 and ordenes.id_obrasocial=" + modelo.ObraSocial.buscarOS(txtobrasocial.getText(), HiloInicio.listaOS).getIdObraSocial());

                        while (rs3.next()) {
                            String num_afiliado = completarnumafiliadoMEDIFE(rs3.getString(8));
                            Registros[0] = completarceros(String.valueOf(txtfactura.getText()), 8);
                            Registros[1] = "06";
                            Registros[2] = "T";
                            Registros[3] = completarceros(rs3.getString(1), 6);
                            Registros[4] = completarespaciosapellido(rs3.getString(2), 20);
                            Registros[5] = "                    ";
                            Registros[6] = "01";
                            Registros[7] = "T";
                            Registros[8] = completarceros(rs3.getString(9), 6);
                            Registros[9] = "                    ";
                            Registros[10] = "                    ";
                            Registros[11] = revertir(rs3.getString(12));
                            Registros[12] = num_afiliado.substring(0, 3);//3
                            Registros[13] = num_afiliado.substring(3, 11);//8
                            Registros[14] = num_afiliado.substring(11, 13);//2
                            Registros[15] = num_afiliado.substring(13, num_afiliado.length());//3
                            Registros[16] = revertir(rs3.getString(12));
                            Registros[17] = "AMB";
                            Registros[18] = "003";
                            Registros[19] = rs3.getString(23);
                            Registros[20] = "00000001";
                            Registros[21] = completarimporteMedife(rs3.getString(25), 12);
                            //Registros[21] = rs3.getString(24);
                            Registros[22] = "TOD";
                            errores = errores + Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + "\r\n";
                            model2.addRow(Registros);

                        }

                        temp.setModel(model2);
                        ///////////////////////////////////////////////////
                        try {

                            StringBuffer numeros = null;
                            for (int j = 0; j < temp.getRowCount(); j++) //recorro las filas
                            {
                                numeros = new StringBuffer(errores);
                                EscribeTxt("TransferenciaMEDIFE.txt", numeros);

                            }
                            Desktop.getDesktop().open(new File(ruta + "TransferenciaMEDIFE.txt"));
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        //////////////////////////////////////////////////////////////////////////////
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }
                if (txtobrasocial.getText().equals("340 - OSSEG - OBRA SOCIAL SEGUROS Y REASEGUROS")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                    String[] Registros = new String[23];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "";
                    try {
                        File archivo = new File(ruta + "Transferencia_OSEG-" + periodo2 + "-.txt");
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            archivo.delete();
                        }

                        FileWriter escribir = new FileWriter(archivo, true);

                        Statement st3 = cn2.createStatement();
                        ResultSet rs3 = st3.executeQuery("SELECT LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,12),20,'0')as Afiliado,\n"
                                + "concat(substring((REPLACE(ordenes.fecha_orden,'/','')),5,4),substring((REPLACE(ordenes.fecha_orden,'/','')),3,2),substring((REPLACE(ordenes.fecha_orden,'/','')),1,2))as fecha,\n"
                                + "LPAD(ordenes.matricula_prescripcion,6,'0') as prescripcion,LPAD(colegiados.matricula_colegiado,6,'0') as Colegiado,\n"
                                + "detalle_ordenes.cod_practica,LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),10,'0')\n"
                                + "FROM ordenes \n"
                                + "INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados \n"
                                + "INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden \n"
                                + "INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial \n"
                                + "WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and detalle_ordenes.estado=0 and ordenes.id_obrasocial=2"
                                + "order by ordenes.id_orden, detalle_ordenes.id_detalle ");
                        // "SELECT colegiados.matricula_colegiado, colegiados.nombre_colegiado ,ordenes.*, obrasocial.codigo_obrasocial, detalle_ordenes.* FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and detalle_ordenes.estado=0 and ordenes.id_obrasocial=" + id_obra_social);
                        int i = 1;
                        String i2 = "";
                        while (rs3.next()) {
                            ///String num_afiliado = completarceros(rs3.getString(8), 12);
                            Registros[0] = completarceros(String.valueOf(txtfactura.getText()), 6);//num fac
                            i2 = String.valueOf(i);
                            Registros[1] = completarceros(i2, 8);/////contador linea
                            Registros[2] = "416";////????????
                            Registros[3] = completarnumorden(rs3.getString(10), 8);//////num de orden
                            Registros[4] = "O";
                            Registros[5] = rs3.getString(1);
                            Registros[6] = "        ";
                            Registros[7] = rs3.getString(2);
                            Registros[8] = rs3.getString(2);
                            Registros[9] = "2";
                            Registros[10] = "  ";
                            Registros[11] = rs3.getString(3);//matricula medica
                            Registros[12] = rs3.getString(4);//matricula Bioq
                            Registros[13] = rs3.getString(5);//cod_practica
                            Registros[14] = "  ";//2
                            Registros[15] = "01";
                            Registros[16] = rs3.getString(6);//importe sin punto
                            Registros[17] = "      ";
                            Registros[18] = "S ";
                            Registros[19] = "N";
                            Registros[20] = "000000";
                            Registros[21] = "M";
                            Registros[22] = "      ";
                            errores = errores + Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + "\r\n";
                            model2.addRow(Registros);
                            i++;
                        }
                        temp.setModel(model2);
                        ///////////////////////////////////////////////////
                        try {
                            StringBuffer numeros = null;
                            for (int j = 0; j < temp.getRowCount(); j++) //recorro las filas
                            {
                                numeros = new StringBuffer(errores);
                                EscribeTxt("TransferenciaOSSEG.txt", numeros);

                            }
                            Desktop.getDesktop().open(new File(ruta + "TransferenciaOSSEG.txt"));
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        //////////////////////////////////////////////////////////////////////////////
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                } else {
                    btnimprimir.setEnabled(true);
                }
                if (txtobrasocial.getText().equals("3100 - OSDE")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};
                    String[] Registros = new String[30];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "", linea_txt = "";

                    try {
                        //////////////Creo el Archivo siempre en try catch
                        File archivo = new File(ruta + "TransferenciaOSDE-" + periodo2 + "-.txt");
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            if (archivo.delete()) {

                                FileWriter escribir = new FileWriter(archivo, true);

                                Statement st3 = cn2.createStatement();
                                ResultSet rs3 = st3.executeQuery("SELECT LPAD(colegiados.codigo_osde,6,'0'),LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,11),11,'0'),\n"
                                        + "LPAD(concat(detalle_ordenes.cod_practica_fac,1),7,'0'),concat(substring((REPLACE(ordenes.fecha_orden,'/','')),1,4),substring((REPLACE(ordenes.fecha_orden,'/','')),7,2)),\n"
                                        + "LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),15,'0'),LPAD(ordenes.matricula_prescripcion,10,'0'),LPAD(colegiados.matricula_colegiado,10,'0'),\n"
                                        + "substring(ordenes.id_orden,2,10)"
                                        + " FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and detalle_ordenes.estado=0 and (ordenes.id_obrasocial=" + modelo.ObraSocial.buscarOS(txtobrasocial.getText(), HiloInicio.listaOS).getIdObraSocial() + " or ordenes.id_obrasocial=25) "
                                        + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED)),ordenes.id_orden");

                                while (rs3.next()) {
                                    // String num_afiliado = completarnumafiliadoOsde(rs3.getString(15));
                                    Registros[0] = " ";
                                    //Registros[1] = completarceros(rs3.getString(28), 6);
                                    Registros[1] = rs3.getString(1);
                                    Registros[2] = "   ";
                                    //Registros[3] = completarceros(rs3.getString(29), 11);
                                    Registros[3] = rs3.getString(2);
                                    Registros[4] = "   ";
                                    //Registros[5] = completarcerospractica(rs3.getString(6), 7);
                                    Registros[5] = rs3.getString(3);
                                    Registros[6] = "           ";
                                    Registros[7] = "001";
                                    Registros[8] = "   ";
                                    //Registros[9] = completafecha(rs3.getString(19));
                                    Registros[9] = rs3.getString(4);
                                    Registros[10] = "   ";
                                    Registros[11] = "00";
                                    Registros[12] = "            ";
                                    Registros[13] = "0";
                                    Registros[14] = "   ";
                                    //Registros[15] = completarimporteMedife(rs3.getString(5), 15);
                                    Registros[15] = rs3.getString(5);
                                    Registros[16] = "   ";
                                    Registros[17] = "M";
                                    Registros[18] = "   ";
                                    //Registros[19] = completarceros(rs3.getString(16), 10);
                                    Registros[19] = rs3.getString(6);
                                    Registros[20] = "   ";
                                    Registros[21] = "T";
                                    Registros[22] = "   ";
                                    Registros[23] = "B";
                                    Registros[24] = "   ";
                                    //Registros[25] = completarceros(rs3.getString(8), 10);
                                    Registros[25] = rs3.getString(7);
                                    Registros[26] = "   ";
                                    Registros[27] = "T";
                                    Registros[28] = "            ";
                                    //Registros[29] = rs3.getString(10).substring(1, 7);//8  
                                    Registros[29] = rs3.getString(8);//8 
                                    escribir.write(Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + Registros[23] + Registros[24] + Registros[25] + Registros[26] + Registros[27] + Registros[28] + Registros[29] + "\r\n");
                                    //errores = errores + Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + Registros[23] + Registros[24] + Registros[25] + Registros[26] + Registros[27] + Registros[28] + Registros[29] + "\r\n";
                                    model2.addRow(Registros);
                                }
                                escribir.close();
                            } else {
                                JOptionPane.showMessageDialog(null, "El Archivo no puede ser eliminado");
                            }
                        }
                        temp.setModel(model2);
                        /////////////////////////////////////////////////// 
                        btnimprimir.setEnabled(true);
                        //////////////////////////////////////////////////////////////////////////////
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }

                if (txtobrasocial.getText().equals("3101 - OSDE  ( RESPONSABLES INSCRIPTOS)")) {
                    String[] titulos = {"1", "2", "3", "4", "5", "6", "7", "8", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};
                    String[] Registros = new String[30];
                    model2 = new DefaultTableModel(null, titulos) {
                        ////Celdas no editables////////
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String nombre_obra = "", cod_obra = "", matricula = "", linea_txt = "";

                    try {
                        //////////////Creo el Archivo siempre en try catch
                        File archivo = new File(ruta + "TransferenciaOSDE-" + periodo2 + "-.txt");
                        /////////// Verifico si existe el Archivo y si existe lo elimino
                        if (archivo.exists()) {
                            if (archivo.delete()) {

                                FileWriter escribir = new FileWriter(archivo, true);

                                Statement st3 = cn.createStatement();
                                ResultSet rs3 = st3.executeQuery("SELECT LPAD(colegiados.codigo_osde,6,'0'),LPAD(substring((REPLACE((REPLACE((REPLACE(ordenes.numero_afiliado,'-','')),' ','')),'/','')),1,11),11,'0'),\n"
                                        + "LPAD(concat(detalle_ordenes.cod_practica_fac,1),7,'0'),concat(substring((REPLACE(ordenes.fecha_orden,'/','')),1,4),substring((REPLACE(ordenes.fecha_orden,'/','')),7,2)),\n"
                                        + "LPAD(REPLACE(detalle_ordenes.precio_practica,'.',''),15,'0'),LPAD(ordenes.matricula_prescripcion,10,'0'),LPAD(colegiados.matricula_colegiado,10,'0'),\n"
                                        + "substring(ordenes.id_orden,2,10)"
                                        + " FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and ordenes.estado_orden=1 and detalle_ordenes.estado=0 and (ordenes.id_obrasocial=" + modelo.ObraSocial.buscarOS(txtobrasocial.getText(), HiloInicio.listaOS).getIdObraSocial() + " or ordenes.id_obrasocial=25) "
                                        + "order by(CAST(colegiados.matricula_colegiado as UNSIGNED)),ordenes.id_orden");

                                while (rs3.next()) {
                                    // String num_afiliado = completarnumafiliadoOsde(rs3.getString(15));
                                    Registros[0] = " ";
                                    //Registros[1] = completarceros(rs3.getString(28), 6);
                                    Registros[1] = rs3.getString(1);
                                    Registros[2] = "   ";
                                    //Registros[3] = completarceros(rs3.getString(29), 11);
                                    Registros[3] = rs3.getString(2);
                                    Registros[4] = "   ";
                                    //Registros[5] = completarcerospractica(rs3.getString(6), 7);
                                    Registros[5] = rs3.getString(3);
                                    Registros[6] = "           ";
                                    Registros[7] = "001";
                                    Registros[8] = "   ";
                                    //Registros[9] = completafecha(rs3.getString(19));
                                    Registros[9] = rs3.getString(4);
                                    Registros[10] = "   ";
                                    Registros[11] = "00";
                                    Registros[12] = "            ";
                                    Registros[13] = "0";
                                    Registros[14] = "   ";
                                    //Registros[15] = completarimporteMedife(rs3.getString(5), 15);
                                    Registros[15] = rs3.getString(5);
                                    Registros[16] = "   ";
                                    Registros[17] = "M";
                                    Registros[18] = "   ";
                                    //Registros[19] = completarceros(rs3.getString(16), 10);
                                    Registros[19] = rs3.getString(6);
                                    Registros[20] = "   ";
                                    Registros[21] = "T";
                                    Registros[22] = "   ";
                                    Registros[23] = "B";
                                    Registros[24] = "   ";
                                    //Registros[25] = completarceros(rs3.getString(8), 10);
                                    Registros[25] = rs3.getString(7);
                                    Registros[26] = "   ";
                                    Registros[27] = "T";
                                    Registros[28] = "            ";
                                    //Registros[29] = rs3.getString(10).substring(1, 7);//8  
                                    Registros[29] = rs3.getString(8);//8 
                                    escribir.write(Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + Registros[23] + Registros[24] + Registros[25] + Registros[26] + Registros[27] + Registros[28] + Registros[29] + "\r\n");
                                    //errores = errores + Registros[0] + Registros[1] + Registros[2] + Registros[3] + Registros[4] + Registros[5] + Registros[6] + Registros[7] + Registros[8] + Registros[9] + Registros[10] + Registros[11] + Registros[12] + Registros[13] + Registros[14] + Registros[15] + Registros[16] + Registros[17] + Registros[18] + Registros[19] + Registros[20] + Registros[21] + Registros[22] + Registros[23] + Registros[24] + Registros[25] + Registros[26] + Registros[27] + Registros[28] + Registros[29] + "\r\n";
                                    model2.addRow(Registros);

                                }
                                escribir.close();
                            } else {
                                JOptionPane.showMessageDialog(null, "El Archivo no puede ser eliminado");
                            }
                        }
                        temp.setModel(model2);
                        ///////////////////////////////////////////////////   
                        btnimprimir.setEnabled(true);
                        //////////////////////////////////////////////////////////////////////////////
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }
            }

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    public String revertir(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        /////Año/////
        for (i = 6; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        ///Mes///
        for (i = 3; i <= 4; i++) {
            salida = salida + entrada.charAt(i);
        }
        ////Dia////
        for (i = 0; i <= 1; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }

    public String revertirfecha(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }

        String salida = "";
        int i = 0;
        ////Dia////
        for (i = 8; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        ///Mes///
        for (i = 5; i <= 6; i++) {
            salida = salida + entrada.charAt(i);
        }
        /////Año/////
        for (i = 0; i <= 3; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";

        salida = salida + "-";

        return salida;

    }

    public static void EscribeTxt(String ruta_archivo, StringBuffer numeros) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(ruta + ruta_archivo);
            pw = new PrintWriter(fichero);
            pw.println(numeros + "\n");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    void cargarperiodo() {
        ////////////////////////////////////////////////////////////////////
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT * FROM periodos ";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            periodo = rs.getString("periodo");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        //////////////////////////////////

        SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);

        fecha2 = formato2.format(currentDate);
        /////////////////////////////////////////////////////////       

        calendar.setTime(currentDate);

    }

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    public double Redondeardosdigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }
////

    String completarcerospractica(String v, int d) {
        String ceros = "";
        if (v.length() < d) {
            for (int i = v.length(); i < (d - 1); i++) {
                ceros += "0";
            }
            v = ceros + v + "1";
        }
        if (v.equals("")) {
            v = "00000000000";
        }
        return v;
    }

    String completarceros(String v, int d) {
        String ceros = "";
        if (v.length() < d) {
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        if (v.equals("")) {
            v = "00000000000";
        }
        return v;
    }

    String completarnumorden(String v, int d) {
        String valor = "";
        String ceros = "";
        if (v.length() == d) {
            valor = v;
        }
        if (v.length() > d) {
            for (int i = 0; i < d; i++) {
                ceros += v.charAt(i);
            }
            valor = ceros;
        }
        if (v.length() < d) {
            for (int i = valor.length(); i < d; i++) {
                ceros += "0";
            }
            valor = ceros + valor;
        }

        return valor;
    }

    ///completarnumafiliado
    String completarnumafiliadoMEDIFE(String v) {
        String valor = "";

        for (int j = 0; j < v.length(); j++) {
            if (!String.valueOf(v.charAt(j)).equals(" ") && !String.valueOf(v.charAt(j)).equals("-") && !String.valueOf(v.charAt(j)).equals("/")) {
                valor = valor + String.valueOf(v.charAt(j));
            }
        }

        if (valor.length() < 16) {
            String ceros = "";
            for (int i = valor.length(); i < 16; i++) {
                ceros += "0";
            }
            valor = ceros + valor;
        }

        return valor;
    }

    String completafecha(String v) {
        String valor = "";
        for (int j = 0; j < v.length(); j++) {
            if (!String.valueOf(v.charAt(j)).equals(" ") && !String.valueOf(v.charAt(j)).equals("-") && !String.valueOf(v.charAt(j)).equals("/")) {
                valor = valor + String.valueOf(v.charAt(j));
            }
        }
        valor = valor.substring(0, 4) + valor.substring(6, 8);
        return valor;
    }

    String completarnumafiliadoOsde(String v) {
        String valor = "", valor1 = "";

        for (int j = 0; j < v.length(); j++) {
            if (!String.valueOf(v.charAt(j)).equals(" ") && !String.valueOf(v.charAt(j)).equals("-") && !String.valueOf(v.charAt(j)).equals("/")) {
                valor = valor + String.valueOf(v.charAt(j));
            }
        }

        if (valor.length() < 11) {
            String ceros = "";
            for (int i = valor.length(); i < 11; i++) {
                ceros += "0";
            }
            valor = ceros + valor;
        }

        return valor;
    }

    String completarnumafiliadoOSSG(String v) {
        String valor = "", resultado = "";
        /////saco guiones y barras/////////////////////////////////////////
        for (int j = 0; j < v.length(); j++) {
            if (!String.valueOf(v.charAt(j)).equals("-") && !String.valueOf(v.charAt(j)).equals("/")) {
                valor = valor + String.valueOf(v.charAt(j));
            }
        }

        /////completo con ceros/////////////////////////////////////////
        if (valor.length() < 12) {
            String ceros = "";
            for (int i = valor.length(); i < 12; i++) {
                ceros += "0";
            }
            resultado = ceros + valor;
        }
        if (valor.length() > 12) {
            String ceros = "";
            for (int i = valor.length(); i < 12; i++) {
                ceros += valor.length();
            }
            resultado = ceros;
        }
        if (valor.length() == 12) {
            resultado = valor;
        }
        return resultado;
    }

    String completarimporteMedife(String v, int d) {
        String ceros = "";
        double v2 = Double.valueOf(v) * 100;
        int v3 = (int) v2;
        v = String.valueOf(v3);
        for (int i = v.length(); i < d; i++) {
            ceros += "0";
        }
        v = ceros + v;
        return v;
    }

    String completarimporteossg(String v, int d) {
        String ceros = "";
        double v2 = Double.valueOf(v) * 100;
        int v3 = (int) v2;
        v = String.valueOf(v3);
        for (int i = v.length(); i < d; i++) {
            ceros += "0";
        }
        v = ceros + v;
        return v;
    }

    String completarespacios(String v, int d) {
        if (v.length() < d) {
            String ceros = "";
            for (int i = v.length(); i < d; i++) {
                ceros += " ";
            }
            v = ceros + v;
        }
        return v;
    }

    String completarespaciosapellido(String v, int d) {
        String ceros = "";
        if (v.length() < 20) {
            for (int i = v.length(); i < 20; i++) {
                ceros += " ";
            }
            v = v + ceros;
        }
        if (v.length() == 20) {
            v = v;
        }
        if (v.length() > 20) {
            for (int i = 0; i < 20; i++) {
                ceros += v.charAt(i);
            }
            v = ceros;
        }
        return v;
    }

    public int completatotal(double numero) {
        //int centavos=Math.rint(numero * 100) % 100;
        int n = (int) numero;
        return n;
    }

    public static boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        this.getjProgressBar1().setStringPainted(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtobrasocial = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtmes = new javax.swing.JFormattedTextField();
        txtaño = new javax.swing.JFormattedTextField();
        btnimprimir = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        progreso = new javax.swing.JProgressBar();
        jScrollPane1 = new javax.swing.JScrollPane();
        temp = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtfactura = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        txtpuntoventa = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Obra Social:");

        txtobrasocial.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtobrasocial.setForeground(new java.awt.Color(0, 102, 204));
        txtobrasocial.setNextFocusableComponent(btnimprimir);
        txtobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtobrasocial, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtobrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Mes:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Año:");

        txtmes.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtmes.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtmes.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmesActionPerformed(evt);
            }
        });

        txtaño.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtaño.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtaño.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtaño.setNextFocusableComponent(txtobrasocial);
        txtaño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtañoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtmes, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtmes)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnimprimir.setMnemonic('g');
        btnimprimir.setText("Generar");
        btnimprimir.setNextFocusableComponent(btnsalir);
        btnimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setMnemonic('v');
        btnsalir.setText("Volver");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        temp.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        temp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(temp);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Pto Venta:");

        txtfactura.setForeground(new java.awt.Color(0, 102, 204));
        txtfactura.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        txtfactura.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtfactura.setNextFocusableComponent(txtmes);
        txtfactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfacturaActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Nº Factura:");

        txtpuntoventa.setForeground(new java.awt.Color(0, 102, 204));
        txtpuntoventa.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        txtpuntoventa.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtpuntoventa.setNextFocusableComponent(txtfactura);
        txtpuntoventa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpuntoventaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtpuntoventa, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtfactura, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtfactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtpuntoventa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1266, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnimprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(175, 175, 175))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void txtobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialActionPerformed

        btnimprimir.requestFocus();

    }//GEN-LAST:event_txtobrasocialActionPerformed

    private void txtmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmesActionPerformed
        txtmes.transferFocus();
    }//GEN-LAST:event_txtmesActionPerformed

    private void txtañoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtañoActionPerformed
        año = txtaño.getText();
        mes = txtmes.getText();
        periodo2 = txtaño.getText() + txtmes.getText();
        txtaño.transferFocus();
    }//GEN-LAST:event_txtañoActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirActionPerformed
        btnimprimir.setEnabled(false);
        iniciarSplash();
        hilo3 = new Hiloobrasocialtodas(progreso);
        hilo3.start();
        hilo3 = null;
    }//GEN-LAST:event_btnimprimirActionPerformed

    private void txtfacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfacturaActionPerformed
        txtfactura.transferFocus();
    }//GEN-LAST:event_txtfacturaActionPerformed

    private void txtpuntoventaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpuntoventaActionPerformed
        txtpuntoventa.transferFocus();
    }//GEN-LAST:event_txtpuntoventaActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnimprimir;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JTable temp;
    private javax.swing.JFormattedTextField txtaño;
    private javax.swing.JFormattedTextField txtfactura;
    private javax.swing.JFormattedTextField txtmes;
    private javax.swing.JTextField txtobrasocial;
    private javax.swing.JFormattedTextField txtpuntoventa;
    // End of variables declaration//GEN-END:variables
}
