package vista;

import controlador.ConexionMariaDB;
import static vista.AltaDebitos.idOrden;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class MostrarDebitos extends javax.swing.JDialog {

    DefaultTableModel modelo;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;

    public MostrarDebitos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icocbt.png")).getImage());
        this.setResizable(false);
        this.setTitle("Débitos");
        this.setLocationRelativeTo(null);
        cargartabla();
        dobleclick();
    }
    
    void dobleclick() {
        tablaDebitos.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    idOrden = Integer.valueOf(tablaDebitos.getValueAt(tablaDebitos.getSelectedRow(), 5).toString());
                    new MotivoDebito(null, true).setVisible(true);
                    //new MotivoDebito(null, true).setVisible(true);
                    
                }
            }
        });
    }

    void cargartabla() {

        String[] titulos = {"Matrícula", "Colegiado", "Obra Social", "Orden", "Afiliado", "idDebito"};       
        String[] datos = new String[6];
        String consulta = "SELECT colegiados.matricula_colegiado, colegiados.nombre_colegiado, CONCAT (obrasocial.codigo_obrasocial, ' - ',obrasocial.razonsocial_obrasocial) AS ObraSocial, ordenes.numero_orden, ordenes.nombre_afiliado, debito.id_debito FROM colegiados INNER JOIN ordenes ON colegiados.id_colegiados = ordenes.id_colegiados INNER JOIN obrasocial ON ordenes.id_obrasocial = obrasocial.id_obrasocial INNER JOIN debito ON debito.id_ordenes = ordenes.id_orden INNER JOIN concepto ON debito.id_concepto = concepto.id_concepto";
        
        modelo = new DefaultTableModel(null, titulos) {
            ////Celdas no editables////////
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            Statement St = cn.createStatement();

            ResultSet Rs = St.executeQuery(consulta);
            while (Rs.next()) {
                datos[0] = Rs.getString(1);
                datos[1] = Rs.getString(2);
                datos[2] = Rs.getString(3);
                datos[3] = Rs.getString(4);
                datos[4] = Rs.getString(5);
                datos[5] = Rs.getString(6);
                
                modelo.addRow(datos);
            }
            tablaDebitos.setModel(modelo);
            /////////////////////////////////////////////////////////////
            alinear();
            tablaDebitos.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
            tablaDebitos.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablaDebitos.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            tablaDebitos.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
            tablaDebitos.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
            
            tablaDebitos.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaDebitos.getColumnModel().getColumn(1).setPreferredWidth(200);
            tablaDebitos.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablaDebitos.getColumnModel().getColumn(4).setPreferredWidth(200);
            tablaDebitos.getColumnModel().getColumn(5).setPreferredWidth(0);
            tablaDebitos.getColumnModel().getColumn(5).setMaxWidth(0);
            tablaDebitos.getColumnModel().getColumn(5).setMinWidth(0);
            tablaDebitos.getTableHeader().getColumnModel().getColumn(5).setMaxWidth(0);
//tablaOS                
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }
    
    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaDebitos = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        jButton1.setText("Volver");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Débitos"));

        tablaDebitos.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaDebitos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablaDebitos);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 904, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        dispose();

    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaDebitos;
    // End of variables declaration//GEN-END:variables
}
