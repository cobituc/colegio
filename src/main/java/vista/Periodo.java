package vista;

import controlador.ConexionMariaDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

public class Periodo extends javax.swing.JDialog {

    public static String fecha, fecha2;

    public Periodo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cargarperiodo();

        this.setLocationRelativeTo(null);
    }

    void cargarperiodo() {
        ////////////////////////////////////////////////////////////////////
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT * FROM periodos ";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            fecha = rs.getString("periodo");
            if (rs.getInt("estado") == 1) {
                chkhabilita.setSelected(true);
            } else {
                chkhabilita.setSelected(false);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        //////////////////////////////////

        SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);

        fecha2 = formato2.format(currentDate);

        /////////////////////////////////////////////////////////
        String año2 = "", mes2 = "", dia2 = "", salida = "";

        calendar.setTime(currentDate);

        int i = 0;
        ///año/// 198506
        for (i = 0; i <= 3; i++) {
            
            año2 = año2 + fecha.charAt(i);
        }

        /////mes/////
        for (i = 4; i <= 5; i++) {
            mes2 = mes2 + fecha.charAt(i);
        }

        Proceso.periodo = año2 + mes2 ;
        txtmes.setText(mes2);
        txtaño.setText(año2);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtmes = new javax.swing.JTextField();
        txtaño = new javax.swing.JTextField();
        chkhabilita = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Período de Facturación", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Mes:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Año:");

        txtmes.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmes.setForeground(new java.awt.Color(0, 102, 204));
        txtmes.setText("12");
        txtmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmesActionPerformed(evt);
            }
        });

        txtaño.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtaño.setForeground(new java.awt.Color(0, 102, 204));
        txtaño.setText("2014");
        txtaño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtañoActionPerformed(evt);
            }
        });

        chkhabilita.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkhabilita.setText("Habilitar/Deshabilitar");
        chkhabilita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkhabilitaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtmes, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(chkhabilita)))
                .addGap(15, 15, 15))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(txtmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(chkhabilita)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmesActionPerformed
        txtmes.transferFocus();
    }//GEN-LAST:event_txtmesActionPerformed

    private void txtañoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtañoActionPerformed
        txtaño.transferFocus();
    }//GEN-LAST:event_txtañoActionPerformed

    private void chkhabilitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkhabilitaActionPerformed
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        boolean periodo = false;
        if (chkhabilita.isSelected() == true) {
            periodo = true;
        } else {
            periodo = false;
        }
        int i = 1;
        try {
            String sSQL = "SELECT * FROM periodos ";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            String sSQL2 = "UPDATE periodos SET estado=? WHERE id_periodo=" + rs.getString("id_periodo");
            PreparedStatement pst = cn.prepareStatement(sSQL2);
            pst.setBoolean(1, periodo);
            int n = pst.executeUpdate();
            if (n > 0) {
                if (periodo == false) {
                    JOptionPane.showMessageDialog(null, "El período vigente fue deshabilitado...");
                } else {
                    JOptionPane.showMessageDialog(null, "El período vigente esta habilitado...");
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_chkhabilitaActionPerformed

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkhabilita;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtaño;
    private javax.swing.JTextField txtmes;
    // End of variables declaration//GEN-END:variables
}
