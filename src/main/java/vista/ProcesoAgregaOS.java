package vista;

import controlador.ConexionMariaDB;
import static vista.AgregarObraSocial.año;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

public class ProcesoAgregaOS extends javax.swing.JDialog {

    HiloOrdenes hilo;

    public ProcesoAgregaOS(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        iniciarSplash();
        hilo = new HiloOrdenes(progreso);
        hilo.start();
        hilo = null;
        this.dispose();
    }

    private void progresoStateChanged(javax.swing.event.ChangeEvent evt) {
        if (progreso.getValue() == 100) {
            this.dispose();
        }
    }

    public class HiloOrdenes extends Thread {

        JProgressBar progreso;

        public HiloOrdenes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;

        }

        public void run() {

            try {
                ConexionMariaDB mysql = new ConexionMariaDB();
                Connection cn = mysql.Conectar();
                String sqlpnbu = "SELECT id_practicasnbu, codigo_practica, unidadbioquimica_practica FROM practicasnbu WHERE añonbu_practicas="+año;
                Statement stpnbu = cn.createStatement();
                ResultSet rspnbu = stpnbu.executeQuery(sqlpnbu);
                int m = 0;
                while (rspnbu.next()) {
                    String sqlostp = "INSERT INTO obrasocial_tiene_practicasnbu (id_obrasocial, id_practicasnbu, codigo_practica, codigo_fac_practicas_obrasocial, unidaddebioquimica_practica, importeunidaddearancel_obrasocial, preciofijo, preciototal, fecha, añonbu, id_usuario) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

                    PreparedStatement pstostp = cn.prepareStatement(sqlostp);

                    pstostp.setInt(1, AgregarObraSocial.ide);
                    pstostp.setInt(2, rspnbu.getInt("id_practicasnbu"));
                    pstostp.setInt(3, rspnbu.getInt("codigo_practica"));
                    pstostp.setString(4, AgregarObraSocial.codfac);
                    pstostp.setString(5, rspnbu.getString("unidadbioquimica_practica"));
                    pstostp.setString(6, AgregarObraSocial.uniar);
                    pstostp.setString(7, "0");
                    pstostp.setString(8, String.valueOf(Integer.valueOf(AgregarObraSocial.uniar) * rspnbu.getInt("unidadbioquimica_practica")));
                    pstostp.setString(9, "");
                    pstostp.setString(10, año);
                    pstostp.setInt(11, Login.idusuario);
                    m = pstostp.executeUpdate();
                    progreso.setValue(0);
                    for (int j = 0; j < 100; j++) {
                        progreso.setValue(j);
                        pausa(1);//aumentamos la barra 
                    }
                }

                if (m > 0) {

                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            //con las lineas siguiente damos un tiempo para que se vea el funcionamiento de la barra 
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
               
            }

            progreso.setValue(100);
                            JOptionPane.showMessageDialog(null, "La Obra Social se agregó exitosamente");
                          
            dispose();
            addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    
                    dispose();
                }
            });
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        progreso = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregando Obra Social", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Cargar.gif"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("En Proceso espere unos minutos...");

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JProgressBar progreso;
    // End of variables declaration//GEN-END:variables
}
