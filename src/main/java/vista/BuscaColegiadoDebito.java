package vista;


import static controlador.HiloInicio.listaColegiados;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.*;

public class BuscaColegiadoDebito extends javax.swing.JDialog {

    DefaultTableModel model;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    public static int filasel;
    static int id_colegiado;

    public BuscaColegiadoDebito(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icocbt.png")).getImage());
        this.setTitle("Colegiados");
        this.setLocationRelativeTo(null);
        dobleclick();
        this.setResizable(false);
        cargartablaNuevo();
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
    }

     void dobleclick() {
        tablacolegiados.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    btnmodificar.doClick();
                    txtbuscar.setText("");
                }
            }
        });
    }
    
    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

  /*  void cargartabla(String valor) {
        String[] Titulo = {"", "Matricula", "Nombre", "Tipo"};
        String[] Registros = new String[4];
        String sql = "SELECT id_colegiados, matricula_colegiado, nombre_colegiado, tipo_profesional FROM colegiados WHERE CONCAT (matricula_colegiado,' ',nombre_colegiado) LIKE '%" + valor + "%' ORDER BY tipo_profesional, CAST(matricula_colegiado as unsigned)";
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ///////////////////////////////
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
               
               
                model.addRow(Registros);
            }
            tablacolegiados.setModel(model);
            tablacolegiados.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
            tablacolegiados.getColumnModel().getColumn(0).setMaxWidth(0);
            tablacolegiados.getColumnModel().getColumn(0).setMinWidth(0);
            tablacolegiados.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablacolegiados.getColumnModel().getColumn(1).setPreferredWidth(60);
            tablacolegiados.getColumnModel().getColumn(1).setMaxWidth(60);
            tablacolegiados.getColumnModel().getColumn(1).setMinWidth(60);
            
            tablacolegiados.getColumnModel().getColumn(2).setPreferredWidth(280);
            tablacolegiados.getColumnModel().getColumn(2).setMaxWidth(280);
            
            
            
            tablacolegiados.getColumnModel().getColumn(3).setPreferredWidth(60);
            tablacolegiados.getColumnModel().getColumn(3).setMaxWidth(60);
            tablacolegiados.getColumnModel().getColumn(3).setMinWidth(60);
            
            
            //////alinear tabla///////
            alinear();
            tablacolegiados.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablacolegiados.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            tablacolegiados.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
            
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }*/
    
    void cargartablaNuevo() {
        String[] Titulo = {"id", "Matricula", "Nombre", "Tipo"};
        String[] Registros = new String[4];
        
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };        
      
        
        for(int i =0; i<listaColegiados.size(); i++){
            
                    
            Registros[0] = String.valueOf(listaColegiados.get(i).getIdColegiados());
                Registros[1] = listaColegiados.get(i).getMatricula();
                Registros[2] = listaColegiados.get(i).getNombre();
                Registros[3] = listaColegiados.get(i).getTipoProfesional();
            model.addRow(Registros);
        }
            tablacolegiados.setModel(model);
            tablacolegiados.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
            tablacolegiados.getColumnModel().getColumn(0).setMaxWidth(0);
            tablacolegiados.getColumnModel().getColumn(0).setMinWidth(0);
            tablacolegiados.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablacolegiados.getColumnModel().getColumn(1).setPreferredWidth(60);
            tablacolegiados.getColumnModel().getColumn(2).setPreferredWidth(210);
            tablacolegiados.getColumnModel().getColumn(3).setPreferredWidth(60);
            
            //////alinear tabla///////
            
            alinear();
            tablacolegiados.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablacolegiados.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            tablacolegiados.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
            
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tablacolegiados = new javax.swing.JTable();
        btnmodificar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        txtbuscar = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tablacolegiados.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tablacolegiados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablacolegiadosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablacolegiados);

        btnmodificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnmodificar.setText("Ingresar");
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setMnemonic('v');
        btnsalir.setText("Volver");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        txtbuscar.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Buscar Colegiado:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 85, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablacolegiadosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablacolegiadosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablacolegiadosKeyPressed

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
        filasel = tablacolegiados.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            //matricula = tablacolegiados.getValueAt(tablacolegiados.getSelectedRow(), 0).toString();
            id_colegiado = Integer.valueOf(tablacolegiados.getValueAt(tablacolegiados.getSelectedRow(), 0).toString());
            new AltaDebitos(null, true).setVisible(true);
            txtbuscar.setText("");
            cargartablaNuevo();
            
        }
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
        tablacolegiados.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscarKeyReleased
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnmodificar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablacolegiados;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
