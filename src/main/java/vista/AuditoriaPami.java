package vista;

import com.mxrck.autocompleter.TextAutoCompleter;
import controlador.ConexionMariaDB;
import controlador.ConexionMariaDataB;
import controlador.DatosGaficoValidadores;
import controlador.DatosGrafico;
import controlador.Funciones;
import controlador.Reportes;
import controlador.Validadores;
import controlador.reporteEntregaValidadores;
import java.awt.image.BufferedImage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import modelo.Colegiado;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import static vista.Recepcion.id_validador;
import static vista.Recepcion.totalOrdenes;
import static vista.Recepcion.total_validadores;

public class AuditoriaPami extends javax.swing.JDialog {
    
    DefaultTableModel modelo, modelo1, modelo2, modelo3, modeloDevolucion;
    ArrayList<Validadores> lista = new ArrayList<>();
    TextAutoCompleter textAutoAcompleter, textAutoAcompleter2;
    int validadores = 0;
    private ConexionMariaDataB conexion;
    int x, y;
    String fecha;
    DefaultCategoryDataset datosGrafico = new DefaultCategoryDataset();
    DefaultCategoryDataset datosGraficoValidadores = new DefaultCategoryDataset();
    JFreeChart grafico, graficoValidadores;
    ArrayList<DatosGaficoValidadores> vectorDatosValidadores = new ArrayList<>();
    ArrayList<DatosGrafico> vectorDatos = new ArrayList<>();
    
    
    public AuditoriaPami(java.awt.Frame parent, boolean modal) {
     
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        panelEntrega.setVisible(true);
        panelDevolucion.setVisible(false);
        panelGraficos.setVisible(false);
        textAutoAcompleter = new TextAutoCompleter(txtAuditorEntrega);
        textAutoAcompleter2 = new TextAutoCompleter(txtAuditorDevolucion);
        cargarDatosaño();
        cargarValidador();
        conexion = new ConexionMariaDataB();
        conexion.EstablecerConexion();
        Validadores.cargarValidador(conexion.getConnection(), lista);
        textAutoAcompleter.removeAllItems();
        textAutoAcompleter2.removeAllItems();
        
        for (int i = 0; i < lista.size(); i++) {
            textAutoAcompleter.addItem(lista.get(i).toString());
            textAutoAcompleter2.addItem(lista.get(i).toString());
        }
        textAutoAcompleter.setMode(0);
        textAutoAcompleter.setCaseSensitive(false);
        textAutoAcompleter2.setMode(0);
        textAutoAcompleter2.setCaseSensitive(false);
    }
    
    void cargarDatosaño() {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();

            String ConsultaPeriodo = "select YEAR(NOW()) as año, MONTH (NOW()) as mes, DATE_FORMAT(CURDATE(),'%d-%m-%Y') AS fecha";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(ConsultaPeriodo);

            if (rs.next()) {

                fecha = rs.getString("fecha");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void cargarValidador() {
        textAutoAcompleter.removeAllItems();
        int i = 0;
        ConexionMariaDB CN = new ConexionMariaDB();
        Connection conexion = CN.Conectar();
        try {
            String ConsultaMatricula = "SELECT count(id_usuario) FROM usuarios where validacion_pami=1";
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(ConsultaMatricula);

            while (rs.next()) {

                validadores = rs.getInt(1);
            }
            conexion.close();
        } catch (NumberFormatException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    private void CargarTablaDisponible(int periodo) {

        try {
            String[] titulo = {"MP", "Colegiado", "Cant. Ordenes"/*, "Entrega", "Paquete"*/, "idrecep"};
            String[] filas = new String[4];
            modelo = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();

            String CargarTablaDisponible = "SELECT *\n"
                    + "FROM vista_left_outer_join\n"
                    + "where periodo=" + periodo + " and estado_recepcion=1 \n"
                    + "order by CAST(cantidad_ordenes as UNSIGNED) DESC, CAST(matricula_colegiado as UNSIGNED) ";
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(CargarTablaDisponible);
            while (RS.next()) {
                filas[0] = RS.getString(9);
                filas[1] = RS.getString(10);
                filas[2] = RS.getString(2);
                /*filas[3] = RS.getString("numero_entrega");
                filas[4] = RS.getString("paquete");*/
                filas[3] = RS.getString("id_recepcion");
                modelo.addRow(filas);
            }
            tablaDisponibles.setModel(modelo);
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        tablaDisponibles.getColumnModel().getColumn(3).setMaxWidth(0);
        tablaDisponibles.getColumnModel().getColumn(3).setMinWidth(0);
        tablaDisponibles.getColumnModel().getColumn(3).setPreferredWidth(0);

    }
    
    void ConsultaOrdenesPeriodo(int periodo) {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();
            
            String ConsultaPeriodo = "SELECT total, totalValidadores FROM vista_total_ordenes_pami WHERE periodo=" + periodo;
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaPeriodo);

            if (RS.next()) {
                totalOrdenes = RS.getInt(1);
                total_validadores = RS.getInt(2);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void cargarTablaValidador(int periodo) {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();
         String[] titulo = {"MP", "Bioquimico", "Órdenes",/* "Entrega", "Paquete",*/ "Fecha", "Impresion"};
            Object[] filas = new Object[5];
            int totalOrdenes_1 = 0;
            modelo1 = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    if (column == 4) {
                        return true;
                    } else {
                        return false;
                    }
                }

                public Class<?> getColumnClass(int i) {
                    if (i == 4) {
                        return java.lang.Boolean.class;
                    }
                    return super.getColumnClass(i);
                }
            };

            String ConsultaMatricula = "SELECT * FROM vista_validador WHERE  id_validador= " + Validadores.verificarValidador(lista, txtAuditorEntrega.getText()).getIdValidadores() + " AND periodo = " + periodo + " AND estado = 1";

            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaMatricula);

            while (RS.next()) {

                filas[0] = RS.getString("matricula_colegiado");
                filas[1] = RS.getString("nombre_colegiado");
                filas[2] = RS.getString("cantidad_ordenes");
//                filas[3] = RS.getString("numero_entrega");
//                filas[4] = RS.getString("paquete");
                filas[3] = RS.getString("fecha");
                filas[4] = false;
                modelo1.addRow(filas);

                totalOrdenes_1 = totalOrdenes_1 + RS.getInt("cantidad_ordenes");

            }
            lbtotalValidador1.setText(String.valueOf(totalOrdenes_1));
            
            
            tablaAsignados.setModel(modelo1);
            txtBuscar.requestFocus();
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    void cargarTablaValidadorDevolucion(int periodo) {

        try {
            ConexionMariaDB CNs = new ConexionMariaDB();
            Connection cons = CNs.Conectar();
            String[] titulo = {"MP", "Bioquimico", "Órdenes", "Entrega", "Paquete", "Fecha", "id recepcion"};
            Object[] registros = new Object[7];
            id_validador = Validadores.verificarValidador(lista, txtAuditorDevolucion.getText()).getIdValidadores();
            int totalOrdenes_1 = 0;
            modeloDevolucion = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };

            String ConsultaMatricula = "SELECT * FROM vista_validador WHERE  id_validador= " + id_validador + " AND periodo = " + periodo + " AND estado = 1 AND estado_recepcion = 0";

            Statement STt = cons.createStatement();
            ResultSet RSs = STt.executeQuery(ConsultaMatricula);

            while (RSs.next()) {

                registros[0] = RSs.getString("matricula_colegiado");
                registros[1] = RSs.getString("nombre_colegiado");
                registros[2] = RSs.getString("cantidad_ordenes");
                registros[3] = RSs.getString("numero_entrega");
                registros[4] = RSs.getString("paquete");
                registros[5] = RSs.getString("fecha");
                registros[6] = RSs.getString("id_recepcion");
                System.out.println(RSs.getString("matricula_colegiado"));
                modeloDevolucion.addRow(registros);
                totalOrdenes_1 = totalOrdenes_1 + RSs.getInt("cantidad_ordenes");
            }
            tablaOrdenesAsignadas.setModel(modeloDevolucion);

        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    void cargarTablaValidadorDevolucionDevuelta(int periodo) {

        try {
            ConexionMariaDB CN = new ConexionMariaDB();
            Connection con = CN.Conectar();
            String[] titulo = {"MP", "Bioquimico", "Órdenes", "Entrega", "Fecha", "id recepcion"};

            String[] filas = new String[6];

            int totalOrdenes_1 = 0;

            modelo2 = new DefaultTableModel(null, titulo) {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            id_validador = Validadores.verificarValidador(lista, txtAuditorDevolucion.getText()).getIdValidadores();

            String ConsultaMatricula = "SELECT * FROM vista_validador WHERE  id_validador= " + id_validador + " AND periodo = " + periodo + " AND estado = 2";

            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaMatricula);

            while (RS.next()) {

                filas[0] = RS.getString(2);
                filas[1] = RS.getString(8);
                filas[2] = RS.getString(3);
                filas[3] = RS.getString(4);
                filas[4] = RS.getString(6);
                filas[5] = RS.getString(1);
                modelo2.addRow(filas);
                totalOrdenes_1 = RS.getInt(2) + totalOrdenes_1;
            }
            tablaDevolucion.setModel(modelo2);
        } catch (SQLException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    void cargarTablaCargadas(int periodo) {

        ConexionMariaDB CN = new ConexionMariaDB();
        Connection con = CN.Conectar();
        String[] titulo = {"MP", "Bioquimico", "Órdenes OK", "Órdenes obs", "Órdenes anuladas", "Total"};

        String[] filas = new String[6];

        modelo3 = new DefaultTableModel(null, titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        try {
            String ConsultaMatricula = "SELECT * FROM vista_ordenes_cargadas_validador WHERE  id_validador= " + Validadores.verificarValidador(lista, txtAuditorDevolucion.getText()).getIdValidadores() + " AND periodo = " + periodo;
            System.out.println("periodo: " + periodo);
            System.out.println("id: " + Validadores.verificarValidador(lista, txtAuditorDevolucion.getText()).getIdValidadores());
            Statement ST = con.createStatement();
            ResultSet RS = ST.executeQuery(ConsultaMatricula);

            while (RS.next()) {

                filas[0] = RS.getString(8);
                filas[1] = RS.getString(9);
                filas[2] = RS.getString(3);
                filas[3] = RS.getString(4);
                filas[4] = RS.getString(5);
                filas[5] = RS.getString(6);
                modelo3.addRow(filas);
            }
            tablaOrdenesCargadas.setModel(modelo3);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    void creaGraficoValidadores(int periodoValidadores) {
        lblGrafico1.setIcon(null);
        datosGraficoValidadores.clear();
        DatosGaficoValidadores.cargarDatos(conexion.getConnection(), vectorDatosValidadores, periodoValidadores);
        for (int i = 0; i < vectorDatosValidadores.size(); i++) {
            datosGraficoValidadores.setValue(vectorDatosValidadores.get(i).getOrdenes(), vectorDatosValidadores.get(i).getValidador(), "");
        }
        graficoValidadores = ChartFactory.createBarChart3D("", "Histórico", "", datosGraficoValidadores);
        BufferedImage primeraImagen = graficoValidadores.createBufferedImage(800, 500);
        lblGrafico1.setIcon(new ImageIcon(primeraImagen));
    }

        @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnSalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        jLabel1 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        btnEntrega = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnDevolucion = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnGraficos = new RSMaterialComponent.RSButtonMaterialIconUno();
        jPanel3 = new javax.swing.JPanel();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();
        panelContenedor = new javax.swing.JPanel();
        panelEntrega = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        txtAuditorEntrega = new RSMaterialComponent.RSTextFieldOne();
        txtMesEntrega = new RSMaterialComponent.RSTextFieldOne();
        txtAñoEntrega = new RSMaterialComponent.RSTextFieldOne();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaDisponibles = new RSMaterialComponent.RSTableMetroCustom();
        txtBuscar = new RSMaterialComponent.RSTextFieldOne();
        labelIcon1 = new necesario.LabelIcon();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablaAsignados = new RSMaterialComponent.RSTableMetroCustom();
        jPanel13 = new javax.swing.JPanel();
        lbl_validadorentrega2 = new javax.swing.JLabel();
        lblTotalOrdenes = new javax.swing.JLabel();
        lbl_validadorentrega1 = new javax.swing.JLabel();
        lbtotalValidador1 = new javax.swing.JLabel();
        lbl_validadorentrega5 = new javax.swing.JLabel();
        lblodenesRestantes = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        btnImprimirCalco = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnImprimirEntrega = new RSMaterialComponent.RSButtonMaterialIconUno();
        panelDevolucion = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        txtAuditorDevolucion = new RSMaterialComponent.RSTextFieldOne();
        txtMesDevolucion = new RSMaterialComponent.RSTextFieldOne();
        txtAñoDevolucion = new RSMaterialComponent.RSTextFieldOne();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tablaOrdenesAsignadas = new RSMaterialComponent.RSTableMetroCustom();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tablaDevolucion = new RSMaterialComponent.RSTableMetroCustom();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tablaOrdenesCargadas = new RSMaterialComponent.RSTableMetroCustom();
        jPanel18 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblTotalOrdenesDevolucion = new javax.swing.JLabel();
        lblTotalCargadasDevolucionç = new javax.swing.JLabel();
        lblTotalDevueltas = new javax.swing.JLabel();
        btnImprimirComprobante = new RSMaterialComponent.RSButtonMaterialIconUno();
        panelGraficos = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        lblGrafico1 = new javax.swing.JLabel();
        txtMesGraficos = new RSMaterialComponent.RSTextFieldOne();
        txtAñoGraficos = new RSMaterialComponent.RSTextFieldOne();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));

        btnSalir.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir.setText("Salir");
        btnSalir.setBackgroundHover(new java.awt.Color(214, 45, 32));
        btnSalir.setForegroundIcon(new java.awt.Color(214, 45, 32));
        btnSalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnSalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnSalir.setRound(20);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/pami.png"))); // NOI18N

        jPanel8.setBackground(new java.awt.Color(66, 133, 200));

        btnEntrega.setBackground(new java.awt.Color(255, 255, 255));
        btnEntrega.setForeground(new java.awt.Color(0, 0, 0));
        btnEntrega.setText("Entrega");
        btnEntrega.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnEntrega.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnEntrega.setForegroundText(new java.awt.Color(51, 51, 51));
        btnEntrega.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ARROW_FORWARD);
        btnEntrega.setRound(20);
        btnEntrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEntregaActionPerformed(evt);
            }
        });

        btnDevolucion.setBackground(new java.awt.Color(255, 255, 255));
        btnDevolucion.setForeground(new java.awt.Color(0, 0, 0));
        btnDevolucion.setText("Devolución");
        btnDevolucion.setBackgroundHover(new java.awt.Color(214, 45, 32));
        btnDevolucion.setForegroundIcon(new java.awt.Color(214, 45, 32));
        btnDevolucion.setForegroundText(new java.awt.Color(51, 51, 51));
        btnDevolucion.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ARROW_BACK);
        btnDevolucion.setRound(20);
        btnDevolucion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDevolucionActionPerformed(evt);
            }
        });

        btnGraficos.setBackground(new java.awt.Color(255, 255, 255));
        btnGraficos.setForeground(new java.awt.Color(0, 0, 0));
        btnGraficos.setText("Gráficos");
        btnGraficos.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnGraficos.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnGraficos.setForegroundText(new java.awt.Color(51, 51, 51));
        btnGraficos.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.GRAPHIC_EQ);
        btnGraficos.setRound(20);
        btnGraficos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGraficosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(btnDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(btnGraficos, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnGraficos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSalir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel3MouseDragged(evt);
            }
        });
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel3MousePressed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(214, 45, 32));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar.setSizeIcon(20.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(375, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelContenedor.setBackground(new java.awt.Color(66, 133, 200));
        panelContenedor.setLayout(new javax.swing.OverlayLayout(panelContenedor));

        panelEntrega.setBackground(new java.awt.Color(66, 133, 200));

        jPanel5.setBackground(new java.awt.Color(66, 133, 200));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Entrega", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 255, 255))); // NOI18N

        jPanel10.setBackground(new java.awt.Color(66, 133, 200));

        txtAuditorEntrega.setForeground(new java.awt.Color(51, 51, 51));
        txtAuditorEntrega.setBorderColor(new java.awt.Color(255, 255, 255));
        txtAuditorEntrega.setPhColor(new java.awt.Color(51, 51, 51));
        txtAuditorEntrega.setPlaceholder("Auditor");
        txtAuditorEntrega.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtAuditorEntrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAuditorEntregaActionPerformed(evt);
            }
        });

        txtMesEntrega.setForeground(new java.awt.Color(51, 51, 51));
        txtMesEntrega.setBorderColor(new java.awt.Color(255, 255, 255));
        txtMesEntrega.setPhColor(new java.awt.Color(51, 51, 51));
        txtMesEntrega.setPlaceholder("Mes");
        txtMesEntrega.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtMesEntrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMesEntregaActionPerformed(evt);
            }
        });

        txtAñoEntrega.setForeground(new java.awt.Color(51, 51, 51));
        txtAñoEntrega.setBorderColor(new java.awt.Color(255, 255, 255));
        txtAñoEntrega.setPhColor(new java.awt.Color(51, 51, 51));
        txtAñoEntrega.setPlaceholder("Año");
        txtAñoEntrega.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtAñoEntrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAñoEntregaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtMesEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtAñoEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtAuditorEntrega, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMesEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAuditorEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAñoEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel11.setBackground(new java.awt.Color(66, 133, 200));
        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Paquetes disponibles", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N
        jPanel11.setPreferredSize(new java.awt.Dimension(388, 122));

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);

        tablaDisponibles.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaDisponibles.setForeground(new java.awt.Color(255, 255, 255));
        tablaDisponibles.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaDisponibles.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaDisponibles.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaDisponibles.setBorderHead(null);
        tablaDisponibles.setBorderRows(null);
        tablaDisponibles.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaDisponibles.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaDisponibles.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaDisponibles.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaDisponibles.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaDisponibles.setGridColor(new java.awt.Color(15, 157, 88));
        tablaDisponibles.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaDisponibles.setShowHorizontalLines(true);
        tablaDisponibles.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaDisponiblesMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tablaDisponibles);

        txtBuscar.setForeground(new java.awt.Color(51, 51, 51));
        txtBuscar.setBorderColor(new java.awt.Color(255, 255, 255));
        txtBuscar.setPhColor(new java.awt.Color(51, 51, 51));
        txtBuscar.setPlaceholder("Buscar");
        txtBuscar.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBuscarKeyTyped(evt);
            }
        });

        labelIcon1.setForeground(new java.awt.Color(255, 255, 255));
        labelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);
        labelIcon1.setPreferredSize(new java.awt.Dimension(28, 28));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(labelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(labelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel12.setBackground(new java.awt.Color(66, 133, 200));
        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Paquetes asignados", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N
        jPanel12.setPreferredSize(new java.awt.Dimension(388, 100));

        jScrollPane6.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane6.setBorder(null);

        tablaAsignados.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaAsignados.setForeground(new java.awt.Color(255, 255, 255));
        tablaAsignados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaAsignados.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaAsignados.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaAsignados.setBorderHead(null);
        tablaAsignados.setBorderRows(null);
        tablaAsignados.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaAsignados.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaAsignados.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaAsignados.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaAsignados.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaAsignados.setGridColor(new java.awt.Color(15, 157, 88));
        tablaAsignados.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaAsignados.setShowHorizontalLines(true);
        jScrollPane6.setViewportView(tablaAsignados);

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel13.setBackground(new java.awt.Color(66, 133, 200));
        jPanel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        lbl_validadorentrega2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_validadorentrega2.setForeground(new java.awt.Color(255, 255, 255));
        lbl_validadorentrega2.setText("Total órdenes:");

        lblTotalOrdenes.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTotalOrdenes.setForeground(new java.awt.Color(255, 255, 255));
        lblTotalOrdenes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lbl_validadorentrega1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_validadorentrega1.setForeground(new java.awt.Color(255, 255, 255));
        lbl_validadorentrega1.setText("Total:");

        lbtotalValidador1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbtotalValidador1.setForeground(new java.awt.Color(255, 255, 255));
        lbtotalValidador1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lbl_validadorentrega5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_validadorentrega5.setForeground(new java.awt.Color(255, 255, 255));
        lbl_validadorentrega5.setText("Ordenes Restantes:");

        lblodenesRestantes.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblodenesRestantes.setForeground(new java.awt.Color(255, 255, 255));
        lblodenesRestantes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(lbl_validadorentrega2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotalOrdenes, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbl_validadorentrega1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbtotalValidador1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                        .addGap(0, 628, Short.MAX_VALUE)
                        .addComponent(lbl_validadorentrega5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblodenesRestantes, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbl_validadorentrega2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblTotalOrdenes, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbl_validadorentrega1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbtotalValidador1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblodenesRestantes, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_validadorentrega5, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel14.setBackground(new java.awt.Color(66, 133, 200));

        btnImprimirCalco.setBackground(new java.awt.Color(255, 255, 255));
        btnImprimirCalco.setForeground(new java.awt.Color(0, 0, 0));
        btnImprimirCalco.setText("Imprimir");
        btnImprimirCalco.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnImprimirCalco.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnImprimirCalco.setForegroundText(new java.awt.Color(51, 51, 51));
        btnImprimirCalco.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.PRINT);
        btnImprimirCalco.setRound(20);
        btnImprimirCalco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirCalcoActionPerformed(evt);
            }
        });

        btnImprimirEntrega.setBackground(new java.awt.Color(255, 255, 255));
        btnImprimirEntrega.setForeground(new java.awt.Color(0, 0, 0));
        btnImprimirEntrega.setText("Comprobante de entrega");
        btnImprimirEntrega.setBackgroundHover(new java.awt.Color(244, 180, 0));
        btnImprimirEntrega.setForegroundIcon(new java.awt.Color(244, 180, 0));
        btnImprimirEntrega.setForegroundText(new java.awt.Color(51, 51, 51));
        btnImprimirEntrega.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnImprimirEntrega.setRound(20);
        btnImprimirEntrega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirEntregaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnImprimirCalco, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnImprimirEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnImprimirEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnImprimirCalco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE))
                    .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout panelEntregaLayout = new javax.swing.GroupLayout(panelEntrega);
        panelEntrega.setLayout(panelEntregaLayout);
        panelEntregaLayout.setHorizontalGroup(
            panelEntregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEntregaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelEntregaLayout.setVerticalGroup(
            panelEntregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEntregaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelContenedor.add(panelEntrega);

        panelDevolucion.setBackground(new java.awt.Color(66, 133, 200));

        jPanel6.setBackground(new java.awt.Color(66, 133, 200));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Devolución", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 255, 255))); // NOI18N

        jPanel15.setBackground(new java.awt.Color(66, 133, 200));

        txtAuditorDevolucion.setForeground(new java.awt.Color(51, 51, 51));
        txtAuditorDevolucion.setBorderColor(new java.awt.Color(255, 255, 255));
        txtAuditorDevolucion.setPhColor(new java.awt.Color(51, 51, 51));
        txtAuditorDevolucion.setPlaceholder("Auditor");
        txtAuditorDevolucion.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtAuditorDevolucion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAuditorDevolucionActionPerformed(evt);
            }
        });

        txtMesDevolucion.setForeground(new java.awt.Color(51, 51, 51));
        txtMesDevolucion.setBorderColor(new java.awt.Color(255, 255, 255));
        txtMesDevolucion.setPhColor(new java.awt.Color(51, 51, 51));
        txtMesDevolucion.setPlaceholder("Mes");
        txtMesDevolucion.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtMesDevolucion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMesDevolucionActionPerformed(evt);
            }
        });

        txtAñoDevolucion.setForeground(new java.awt.Color(51, 51, 51));
        txtAñoDevolucion.setBorderColor(new java.awt.Color(255, 255, 255));
        txtAñoDevolucion.setPhColor(new java.awt.Color(51, 51, 51));
        txtAñoDevolucion.setPlaceholder("Año");
        txtAñoDevolucion.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtAñoDevolucion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAñoDevolucionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtMesDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtAñoDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtAuditorDevolucion, javax.swing.GroupLayout.DEFAULT_SIZE, 722, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMesDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAuditorDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAñoDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(66, 133, 200));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Asignadas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N
        jPanel4.setPreferredSize(new java.awt.Dimension(388, 100));

        jScrollPane7.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane7.setBorder(null);

        tablaOrdenesAsignadas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaOrdenesAsignadas.setForeground(new java.awt.Color(255, 255, 255));
        tablaOrdenesAsignadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaOrdenesAsignadas.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaOrdenesAsignadas.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaOrdenesAsignadas.setBorderHead(null);
        tablaOrdenesAsignadas.setBorderRows(null);
        tablaOrdenesAsignadas.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaOrdenesAsignadas.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaOrdenesAsignadas.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaOrdenesAsignadas.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaOrdenesAsignadas.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaOrdenesAsignadas.setGridColor(new java.awt.Color(15, 157, 88));
        tablaOrdenesAsignadas.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaOrdenesAsignadas.setShowHorizontalLines(true);
        jScrollPane7.setViewportView(tablaOrdenesAsignadas);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel16.setBackground(new java.awt.Color(66, 133, 200));
        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Devueltas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N
        jPanel16.setPreferredSize(new java.awt.Dimension(388, 100));

        jScrollPane8.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane8.setBorder(null);

        tablaDevolucion.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaDevolucion.setForeground(new java.awt.Color(255, 255, 255));
        tablaDevolucion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaDevolucion.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaDevolucion.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaDevolucion.setBorderHead(null);
        tablaDevolucion.setBorderRows(null);
        tablaDevolucion.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaDevolucion.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaDevolucion.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaDevolucion.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaDevolucion.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaDevolucion.setGridColor(new java.awt.Color(15, 157, 88));
        tablaDevolucion.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaDevolucion.setShowHorizontalLines(true);
        jScrollPane8.setViewportView(tablaDevolucion);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel17.setBackground(new java.awt.Color(66, 133, 200));
        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Ordenes auditadas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N

        jScrollPane9.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane9.setBorder(null);

        tablaOrdenesCargadas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablaOrdenesCargadas.setForeground(new java.awt.Color(255, 255, 255));
        tablaOrdenesCargadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaOrdenesCargadas.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablaOrdenesCargadas.setBackgoundHover(new java.awt.Color(15, 157, 88));
        tablaOrdenesCargadas.setBorderHead(null);
        tablaOrdenesCargadas.setBorderRows(null);
        tablaOrdenesCargadas.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablaOrdenesCargadas.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablaOrdenesCargadas.setColorPrimaryText(new java.awt.Color(66, 133, 200));
        tablaOrdenesCargadas.setColorSecondary(new java.awt.Color(255, 255, 255));
        tablaOrdenesCargadas.setColorSecundaryText(new java.awt.Color(66, 133, 200));
        tablaOrdenesCargadas.setGridColor(new java.awt.Color(15, 157, 88));
        tablaOrdenesCargadas.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablaOrdenesCargadas.setShowHorizontalLines(true);
        jScrollPane9.setViewportView(tablaOrdenesCargadas);

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 848, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel18.setBackground(new java.awt.Color(66, 133, 200));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Total cargadas: ");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Total devueltas: ");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Total órdenes: ");

        lblTotalOrdenesDevolucion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotalOrdenesDevolucion.setForeground(new java.awt.Color(255, 255, 255));

        lblTotalCargadasDevolucionç.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotalCargadasDevolucionç.setForeground(new java.awt.Color(255, 255, 255));

        lblTotalDevueltas.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotalDevueltas.setForeground(new java.awt.Color(255, 255, 255));

        btnImprimirComprobante.setBackground(new java.awt.Color(255, 255, 255));
        btnImprimirComprobante.setForeground(new java.awt.Color(0, 0, 0));
        btnImprimirComprobante.setText("Imprimir comprobante");
        btnImprimirComprobante.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnImprimirComprobante.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnImprimirComprobante.setForegroundText(new java.awt.Color(51, 51, 51));
        btnImprimirComprobante.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.PRINT);
        btnImprimirComprobante.setRound(20);

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotalDevueltas, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnImprimirComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel18Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTotalOrdenesDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel18Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTotalCargadasDevolucionç, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnImprimirComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTotalOrdenesDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(lblTotalCargadasDevolucionç, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblTotalDevueltas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel13))))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 437, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE))
                    .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 285, Short.MAX_VALUE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, 285, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout panelDevolucionLayout = new javax.swing.GroupLayout(panelDevolucion);
        panelDevolucion.setLayout(panelDevolucionLayout);
        panelDevolucionLayout.setHorizontalGroup(
            panelDevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDevolucionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelDevolucionLayout.setVerticalGroup(
            panelDevolucionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDevolucionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelContenedor.add(panelDevolucion);

        panelGraficos.setBackground(new java.awt.Color(66, 133, 200));

        jPanel9.setBackground(new java.awt.Color(66, 133, 200));
        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)), "Gráficos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 255, 255))); // NOI18N

        jPanel7.setBackground(new java.awt.Color(66, 133, 200));

        lblGrafico1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblGrafico1, javax.swing.GroupLayout.DEFAULT_SIZE, 858, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblGrafico1, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtMesGraficos.setForeground(new java.awt.Color(51, 51, 51));
        txtMesGraficos.setBorderColor(new java.awt.Color(255, 255, 255));
        txtMesGraficos.setPhColor(new java.awt.Color(51, 51, 51));
        txtMesGraficos.setPlaceholder("Mes");
        txtMesGraficos.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtMesGraficos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMesGraficosActionPerformed(evt);
            }
        });

        txtAñoGraficos.setForeground(new java.awt.Color(51, 51, 51));
        txtAñoGraficos.setBorderColor(new java.awt.Color(255, 255, 255));
        txtAñoGraficos.setPhColor(new java.awt.Color(51, 51, 51));
        txtAñoGraficos.setPlaceholder("Año");
        txtAñoGraficos.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtAñoGraficos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAñoGraficosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(txtMesGraficos, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtAñoGraficos, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMesGraficos, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAñoGraficos, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout panelGraficosLayout = new javax.swing.GroupLayout(panelGraficos);
        panelGraficos.setLayout(panelGraficosLayout);
        panelGraficosLayout.setHorizontalGroup(
            panelGraficosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGraficosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelGraficosLayout.setVerticalGroup(
            panelGraficosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGraficosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelContenedor.add(panelGraficos);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelContenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelContenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed

        dispose();
        
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnDevolucionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDevolucionActionPerformed

        panelEntrega.setVisible(false);
        panelDevolucion.setVisible(true);
        panelGraficos.setVisible(false);
        
    }//GEN-LAST:event_btnDevolucionActionPerformed

    private void btnGraficosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGraficosActionPerformed

        panelEntrega.setVisible(false);
        panelDevolucion.setVisible(false);
        panelGraficos.setVisible(true);
        
    }//GEN-LAST:event_btnGraficosActionPerformed

    private void btnEntregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEntregaActionPerformed

        panelEntrega.setVisible(true);
        panelDevolucion.setVisible(false);
        panelGraficos.setVisible(false);
        
    }//GEN-LAST:event_btnEntregaActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();
        
    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtMesEntregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMesEntregaActionPerformed

        if (Funciones.isNumeric(txtMesEntrega.getText()) && txtMesEntrega.getText().length() == 2) {

            txtAñoEntrega.requestFocus();

        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
            txtMesEntrega.setText("");
        }
        
    }//GEN-LAST:event_txtMesEntregaActionPerformed

    private void txtAñoEntregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAñoEntregaActionPerformed

        if (Funciones.isNumeric(txtAñoEntrega.getText())) {

            try {
                ConexionMariaDB CN = new ConexionMariaDB();
                Connection con = CN.Conectar();
                
                int periodo = Integer.parseInt(txtAñoEntrega.getText()+txtMesEntrega.getText());
                
                String ConsultaMatricula = "SELECT * FROM vista_left_outer_join where periodo = " + periodo;
                Statement ST = con.createStatement();
                ResultSet RS = ST.executeQuery(ConsultaMatricula);

                if (RS.next()) {
                    CargarTablaDisponible(periodo);

                } else {
                    JOptionPane.showMessageDialog(null, "No hay ordenes para repartir");

                }
                
                
                txtAuditorEntrega.requestFocus();
                ConsultaOrdenesPeriodo(periodo);
                lblTotalOrdenes.setText(String.valueOf(totalOrdenes));                
                lblodenesRestantes.setText(String.valueOf(totalOrdenes - total_validadores));
            } catch (SQLException ex) {
                Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un año valido");
            txtAñoEntrega.setText("");
        }
        
    }//GEN-LAST:event_txtAñoEntregaActionPerformed

    private void txtAuditorEntregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAuditorEntregaActionPerformed

        if (Validadores.verificarValidador(lista, txtAuditorEntrega.getText()) != null) {
            
            if (tablaAsignados.getRowCount() != 0) {
                modelo1.setRowCount(0);
            }
            int periodo = Integer.parseInt(txtAñoEntrega.getText()+txtMesEntrega.getText());
            cargarTablaValidador(periodo);

        } else {
            txtAuditorEntrega.setText("");
        }
        
    }//GEN-LAST:event_txtAuditorEntregaActionPerformed

    private void jPanel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MousePressed

        x = evt.getX();
        y = evt.getY();
        
    }//GEN-LAST:event_jPanel3MousePressed

    private void jPanel3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);
        
    }//GEN-LAST:event_jPanel3MouseDragged

    private void tablaDisponiblesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaDisponiblesMouseClicked

        if (evt.getClickCount() == 2) {
            if (!txtAuditorEntrega.getText().equals("")) {
                if (Validadores.verificarValidador(lista, txtAuditorEntrega.getText()) != null) {
                    ConexionMariaDB cc = new ConexionMariaDB();
                    Connection cn = cc.Conectar();
                    try {
                        String update = "UPDATE recepcion_bioquimico SET estado_recepcion = 0 WHERE id_recepcion = " + Integer.valueOf(tablaDisponibles.getValueAt(tablaDisponibles.getSelectedRow(), 3).toString());
                        String insert = "INSERT INTO validador_pami (fecha, id_validador, estado, id_recepcion) VALUES(CURDATE(),?,?,?)";
                        PreparedStatement pst = cn.prepareStatement(insert);
                        pst.setInt(1, Validadores.verificarValidador(lista, txtAuditorEntrega.getText()).getIdValidadores());
                        pst.setInt(2, 1);
                        pst.setInt(3, Integer.valueOf(tablaDisponibles.getValueAt(tablaDisponibles.getSelectedRow(), 3).toString()));
                        pst.executeUpdate();
                        pst = cn.prepareStatement(update);
                        int n = pst.executeUpdate();
                        if (n <= 0) {
                            JOptionPane.showMessageDialog(null, "No se pudo asignar el el validador");
                        } else {
                            int periodo = Integer.parseInt(txtAñoEntrega.getText() + txtMesEntrega.getText());
                            CargarTablaDisponible(periodo);
                            cargarTablaValidador(periodo);
                            ConsultaOrdenesPeriodo(periodo);
                            lblTotalOrdenes.setText(String.valueOf(totalOrdenes));                            
                            lblodenesRestantes.setText(String.valueOf(totalOrdenes - total_validadores));
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        
    }//GEN-LAST:event_tablaDisponiblesMouseClicked

    private void btnImprimirCalcoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirCalcoActionPerformed

        if (tablaAsignados.getRowCount() > 0) {
            if (tablaAsignados.getSelectedRow() > -1) {
                ArrayList<Colegiado> listaColegiados = new ArrayList<>();
                Colegiado c;
                Colegiado.cargarColegiado(conexion.getConnection(), listaColegiados);
                c = Colegiado.buscarColegiado(tablaAsignados.getValueAt(tablaAsignados.getSelectedRow(), 0).toString(), listaColegiados);
                Reportes rotulo = new Reportes();
                rotulo.ReporteRotulo(c.getNombre(),
                        Integer.valueOf(c.getMatricula()),
                        Integer.valueOf(tablaAsignados.getValueAt(tablaAsignados.getSelectedRow(), 2).toString()),
                        Integer.valueOf(txtMesEntrega.getText()),
                        fecha, txtAuditorEntrega.getText());

            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un registro");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un registro");
        }
        
    }//GEN-LAST:event_btnImprimirCalcoActionPerformed

    private void btnImprimirEntregaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirEntregaActionPerformed

        LinkedList<reporteEntregaValidadores> resultadoEntrega = new LinkedList<>();
        resultadoEntrega.clear();
        reporteEntregaValidadores datos;
        for (int i = 0; i < tablaAsignados.getRowCount(); i++) {
            if (Boolean.valueOf(tablaAsignados.getValueAt(i, 4).toString()) == true) {
                System.out.println(tablaAsignados.getValueAt(i, 1).toString());
                datos = new reporteEntregaValidadores(
                        tablaAsignados.getValueAt(i, 0).toString(),
                        tablaAsignados.getValueAt(i, 1).toString(),
                        Integer.valueOf(tablaAsignados.getValueAt(i, 2).toString()),
                        tablaAsignados.getValueAt(i, 3).toString()
                        );
                resultadoEntrega.add(datos);
            }
        }
        int periodo = Integer.parseInt(txtAñoEntrega.getText() + txtMesEntrega.getText());
        cargarTablaValidador(periodo);
        JDialog viewer = new JDialog(new javax.swing.JFrame(), "Reporte", true);
        viewer.setSize(800, 600);
        viewer.setLocationRelativeTo(null);

        try {
            JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/reporte_auditores.jasper"));
            Map parametros = new HashMap();
            parametros.put("nombre", txtAuditorEntrega.getText());

            JasperPrint jPrintreporte = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(resultadoEntrega));
            JasperViewer jv = new JasperViewer(jPrintreporte, false);
            viewer.getContentPane().add(jv.getContentPane());
            viewer.setVisible(true);

        } catch (JRException ex) {
            Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_btnImprimirEntregaActionPerformed

    private void txtMesDevolucionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMesDevolucionActionPerformed

        if (Funciones.isNumeric(txtMesDevolucion.getText()) && txtMesDevolucion.getText().length() == 2) {
            txtAñoDevolucion.requestFocus();
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
            txtMesDevolucion.setText("");
        }
        
    }//GEN-LAST:event_txtMesDevolucionActionPerformed

    private void txtAñoDevolucionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAñoDevolucionActionPerformed

        if (Funciones.isNumeric(txtAñoDevolucion.getText())) {

            try {
                ConexionMariaDB CN = new ConexionMariaDB();
                Connection con = CN.Conectar();
                
                String ConsultaMatricula = "SELECT * FROM vista_left_outer_join";
                Statement ST = con.createStatement();
                ResultSet RS = ST.executeQuery(ConsultaMatricula);

                if (RS.next()) {

                } else {
                    JOptionPane.showMessageDialog(null, "No hay ordenes para repartir");
                }
                txtAuditorDevolucion.requestFocus();
            } catch (SQLException ex) {
                Logger.getLogger(Recepcion.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un año valido");
            txtAñoDevolucion.setText("");
        }
        
    }//GEN-LAST:event_txtAñoDevolucionActionPerformed

    private void txtAuditorDevolucionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAuditorDevolucionActionPerformed

         if (Validadores.verificarValidador(lista, txtAuditorDevolucion.getText()) != null) {
            int periodo = Integer.parseInt(txtAñoDevolucion.getText() + txtMesDevolucion.getText());
            cargarTablaValidadorDevolucion(periodo);
            cargarTablaValidadorDevolucionDevuelta(periodo);
            cargarTablaCargadas(periodo);

        } else {
            txtAuditorDevolucion.setText("");
        }
        
    }//GEN-LAST:event_txtAuditorDevolucionActionPerformed

    private void txtMesGraficosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMesGraficosActionPerformed

        if (Funciones.isNumeric(txtMesGraficos.getText()) && txtMesGraficos.getText().length()==2 ) {

            if (1 <= Integer.valueOf(txtMesGraficos.getText()) && Integer.valueOf(txtMesGraficos.getText()) <= 12) {

                txtAñoGraficos.setEnabled(true);
                txtAñoGraficos.requestFocus();
            } else {

                JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
                txtMesGraficos.setText("");
            }

        } else {

            JOptionPane.showMessageDialog(null, "Ingrese un mes valido");
            txtMesGraficos.setText("");
        }
        
    }//GEN-LAST:event_txtMesGraficosActionPerformed

    private void txtAñoGraficosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAñoGraficosActionPerformed

        Date fecha1 = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy");
        SimpleDateFormat formato1 = new SimpleDateFormat("MM");
        String añolocal = formato.format(fecha1);
        String meslocal = formato1.format(fecha1);
        int añohoy = Integer.parseInt(añolocal);
        int meshoy = Integer.parseInt(meslocal);
        int año1 = Integer.parseInt(txtAñoGraficos.getText());
        int mes1 = Integer.parseInt(txtMesGraficos.getText());

        if (Funciones.isNumeric(txtAñoGraficos.getText())) {

            if ((año1 == añohoy && mes1 == meshoy) || ((año1 < añohoy) && ((meshoy - 1) == 12))) {

                String Periodo = txtAñoGraficos.getText() + txtMesGraficos.getText();

                int periodoGrafico = Integer.parseInt(Periodo);

                creaGraficoValidadores(periodoGrafico);

            } else {

                String PeriodoString = txtAñoGraficos.getText() + txtMesGraficos.getText();

                int periodoGrafico = Integer.parseInt(PeriodoString);
                creaGraficoValidadores(periodoGrafico);

            }
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese un año valido");
            txtAñoGraficos.setText("");
        }
        
    }//GEN-LAST:event_txtAñoGraficosActionPerformed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
         TableRowSorter sorter = new TableRowSorter(modelo);
            sorter.setRowFilter(RowFilter.regexFilter(".*" + txtBuscar.getText() + ".*"));
            tablaDisponibles.setRowSorter(sorter);
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void txtBuscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBuscarKeyTyped
 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnDevolucion;
    private RSMaterialComponent.RSButtonMaterialIconUno btnEntrega;
    private RSMaterialComponent.RSButtonMaterialIconUno btnGraficos;
    private RSMaterialComponent.RSButtonMaterialIconUno btnImprimirCalco;
    private RSMaterialComponent.RSButtonMaterialIconUno btnImprimirComprobante;
    private RSMaterialComponent.RSButtonMaterialIconUno btnImprimirEntrega;
    private RSMaterialComponent.RSButtonMaterialIconUno btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private necesario.LabelIcon labelIcon1;
    private javax.swing.JLabel lblGrafico1;
    private javax.swing.JLabel lblTotalCargadasDevolucionç;
    private javax.swing.JLabel lblTotalDevueltas;
    private javax.swing.JLabel lblTotalOrdenes;
    private javax.swing.JLabel lblTotalOrdenesDevolucion;
    private javax.swing.JLabel lbl_validadorentrega1;
    private javax.swing.JLabel lbl_validadorentrega2;
    private javax.swing.JLabel lbl_validadorentrega5;
    private javax.swing.JLabel lblodenesRestantes;
    private javax.swing.JLabel lbtotalValidador1;
    private javax.swing.JPanel panelContenedor;
    private javax.swing.JPanel panelDevolucion;
    private javax.swing.JPanel panelEntrega;
    private javax.swing.JPanel panelGraficos;
    private RSMaterialComponent.RSTableMetroCustom tablaAsignados;
    private RSMaterialComponent.RSTableMetroCustom tablaDevolucion;
    private RSMaterialComponent.RSTableMetroCustom tablaDisponibles;
    private RSMaterialComponent.RSTableMetroCustom tablaOrdenesAsignadas;
    private RSMaterialComponent.RSTableMetroCustom tablaOrdenesCargadas;
    private RSMaterialComponent.RSTextFieldOne txtAuditorDevolucion;
    private RSMaterialComponent.RSTextFieldOne txtAuditorEntrega;
    private RSMaterialComponent.RSTextFieldOne txtAñoDevolucion;
    private RSMaterialComponent.RSTextFieldOne txtAñoEntrega;
    private RSMaterialComponent.RSTextFieldOne txtAñoGraficos;
    private RSMaterialComponent.RSTextFieldOne txtBuscar;
    private RSMaterialComponent.RSTextFieldOne txtMesDevolucion;
    private RSMaterialComponent.RSTextFieldOne txtMesEntrega;
    private RSMaterialComponent.RSTextFieldOne txtMesGraficos;
    // End of variables declaration//GEN-END:variables
}
