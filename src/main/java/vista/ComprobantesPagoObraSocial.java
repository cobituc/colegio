/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.ObraSocialAfip;
import static controlador.HiloInicio.listaOSAfip;
import com.mxrck.autocompleter.TextAutoCompleter;
import controlador.ConexionMariaDB;
import controlador.Funciones;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Optional;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class ComprobantesPagoObraSocial extends javax.swing.JDialog {

    public int x, y;
    TextAutoCompleter textAutoAcompleterOS;
    ObraSocialAfip OS;
    DefaultTableModel modelo;
    int idObraSocial;
    double total = 0.0;
    public static String ObraSocial;

    public ComprobantesPagoObraSocial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(700, 250);
        txtConcepto.setLineWrap(true);
        textAutoAcompleterOS = new TextAutoCompleter(txtobrasocial);
        cargarobrasocial();
        txtusuario.setText(Login.usuario);
    }

    void cargarobrasocial() {
        borrarobrasocial();
        textAutoAcompleterOS.addItems(listaOSAfip.toArray());
        textAutoAcompleterOS.setMode(0); // infijo     
        textAutoAcompleterOS.setCaseSensitive(false); //No sensible a mayúsculas        
    }

    void borrarobrasocial() {
        textAutoAcompleterOS.removeAllItems();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnCerrar = new RSMaterialComponent.RSButtonIconOne();
        jPanel4 = new javax.swing.JPanel();
        txtdireccion = new RSMaterialComponent.RSTextFieldOne();
        jLabel15 = new javax.swing.JLabel();
        txtprovincia = new RSMaterialComponent.RSTextFieldOne();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtConcepto = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablarecibos = new RSMaterialComponent.RSTableMetroCustom();
        txtbuscar = new RSMaterialComponent.RSTextFieldOne();
        jPanel2 = new javax.swing.JPanel();
        btnaceptar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btncancelar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconUno();
        jLabel18 = new javax.swing.JLabel();
        txtusuario = new javax.swing.JLabel();
        rSLabelFecha1 = new rojeru_san.rsdate.RSLabelFecha();
        txtobrasocial = new RSMaterialComponent.RSTextFieldOne();
        txtcuit = new RSMaterialComponent.RSTextFieldOne();
        txttipoiva = new RSMaterialComponent.RSTextFieldOne();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(66, 133, 200));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(66, 133, 200)));
        jPanel6.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel6MouseDragged(evt);
            }
        });
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel6MousePressed(evt);
            }
        });

        btnCerrar.setBackground(new java.awt.Color(219, 68, 55));
        btnCerrar.setBackgroundHover(new java.awt.Color(142, 68, 55));
        btnCerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnCerrar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnCerrar.setSizeIcon(20.0F);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(418, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(66, 133, 200));

        txtdireccion.setForeground(new java.awt.Color(51, 51, 51));
        txtdireccion.setToolTipText("Domicilio Comercial");
        txtdireccion.setBorderColor(new java.awt.Color(255, 255, 255));
        txtdireccion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdireccion.setPhColor(new java.awt.Color(51, 51, 51));
        txtdireccion.setPlaceholder("Domicilio Comercial");
        txtdireccion.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdireccionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdireccionKeyReleased(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(244, 180, 0));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel15.setText("Total: $");

        txtprovincia.setForeground(new java.awt.Color(51, 51, 51));
        txtprovincia.setToolTipText("");
        txtprovincia.setBorderColor(new java.awt.Color(255, 255, 255));
        txtprovincia.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtprovincia.setPhColor(new java.awt.Color(51, 51, 51));
        txtprovincia.setPlaceholder("Provincia");
        txtprovincia.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtprovincia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtprovinciaActionPerformed(evt);
            }
        });
        txtprovincia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtprovinciaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtprovinciaKeyReleased(evt);
            }
        });

        txtConcepto.setColumns(20);
        txtConcepto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtConcepto.setRows(5);
        txtConcepto.setSelectionColor(new java.awt.Color(244, 180, 0));
        jScrollPane1.setViewportView(txtConcepto);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel12.setText("Descripción");

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(244, 180, 0));
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTotal.setText("0.0");

        jScrollPane6.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane6.setBorder(null);

        tablarecibos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        tablarecibos.setForeground(new java.awt.Color(255, 255, 255));
        tablarecibos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablarecibos.setBackgoundHead(new java.awt.Color(15, 157, 88));
        tablarecibos.setBackgoundHover(new java.awt.Color(255, 255, 255));
        tablarecibos.setBorderHead(null);
        tablarecibos.setBorderRows(null);
        tablarecibos.setColorBorderHead(new java.awt.Color(255, 255, 255));
        tablarecibos.setColorBorderRows(new java.awt.Color(255, 255, 255));
        tablarecibos.setColorPrimaryText(new java.awt.Color(51, 51, 51));
        tablarecibos.setColorSecondary(new java.awt.Color(227, 233, 230));
        tablarecibos.setColorSecundaryText(new java.awt.Color(51, 51, 51));
        tablarecibos.setGridColor(new java.awt.Color(15, 157, 88));
        tablarecibos.setSelectionBackground(new java.awt.Color(66, 133, 200));
        tablarecibos.setShowHorizontalLines(true);
        tablarecibos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablarecibosKeyPressed(evt);
            }
        });
        jScrollPane6.setViewportView(tablarecibos);

        txtbuscar.setForeground(new java.awt.Color(51, 51, 51));
        txtbuscar.setToolTipText("Buscar");
        txtbuscar.setBorderColor(new java.awt.Color(255, 255, 255));
        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.setPhColor(new java.awt.Color(51, 51, 51));
        txtbuscar.setPlaceholder("Buscar");
        txtbuscar.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(txtdireccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtprovincia, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane6)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtprovincia, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtdireccion.getAccessibleContext().setAccessibleDescription("Domicilio Comercial");

        jPanel2.setBackground(new java.awt.Color(66, 133, 200));

        btnaceptar.setBackground(new java.awt.Color(255, 255, 255));
        btnaceptar.setForeground(new java.awt.Color(0, 0, 0));
        btnaceptar.setText("Agregar");
        btnaceptar.setBackgroundHover(new java.awt.Color(15, 157, 88));
        btnaceptar.setForegroundIcon(new java.awt.Color(15, 157, 88));
        btnaceptar.setForegroundText(new java.awt.Color(51, 51, 51));
        btnaceptar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnaceptar.setRound(20);
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btncancelar.setBackground(new java.awt.Color(255, 255, 255));
        btncancelar.setForeground(new java.awt.Color(0, 0, 0));
        btncancelar.setText("Borrar");
        btncancelar.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btncancelar.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btncancelar.setForegroundText(new java.awt.Color(51, 51, 51));
        btncancelar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.REMOVE);
        btncancelar.setRound(20);
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnsalir.setBackground(new java.awt.Color(255, 255, 255));
        btnsalir.setForeground(new java.awt.Color(0, 0, 0));
        btnsalir.setText("Salir");
        btnsalir.setBackgroundHover(new java.awt.Color(219, 68, 55));
        btnsalir.setForegroundIcon(new java.awt.Color(219, 68, 55));
        btnsalir.setForegroundText(new java.awt.Color(51, 51, 51));
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnsalir.setRound(20);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel18.setText("Usuario:");

        txtusuario.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtusuario.setForeground(new java.awt.Color(255, 255, 255));
        txtusuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        rSLabelFecha1.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelFecha1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        txtobrasocial.setForeground(new java.awt.Color(51, 51, 51));
        txtobrasocial.setBorderColor(new java.awt.Color(255, 255, 255));
        txtobrasocial.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtobrasocial.setPhColor(new java.awt.Color(51, 51, 51));
        txtobrasocial.setPlaceholder("Obra Social");
        txtobrasocial.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialActionPerformed(evt);
            }
        });
        txtobrasocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtobrasocialKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtobrasocialKeyReleased(evt);
            }
        });

        txtcuit.setForeground(new java.awt.Color(51, 51, 51));
        txtcuit.setToolTipText("");
        txtcuit.setBorderColor(new java.awt.Color(255, 255, 255));
        txtcuit.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcuit.setPhColor(new java.awt.Color(51, 51, 51));
        txtcuit.setPlaceholder("CUIT");
        txtcuit.setSelectionColor(new java.awt.Color(244, 180, 0));
        txtcuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcuitActionPerformed(evt);
            }
        });
        txtcuit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcuitKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcuitKeyReleased(evt);
            }
        });

        txttipoiva.setForeground(new java.awt.Color(51, 51, 51));
        txttipoiva.setToolTipText("");
        txttipoiva.setBorderColor(new java.awt.Color(255, 255, 255));
        txttipoiva.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttipoiva.setPhColor(new java.awt.Color(51, 51, 51));
        txttipoiva.setPlaceholder("Tipo IVA");
        txttipoiva.setSelectionColor(new java.awt.Color(244, 180, 0));
        txttipoiva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttipoivaActionPerformed(evt);
            }
        });
        txttipoiva.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttipoivaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttipoivaKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(rSLabelFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txtobrasocial, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcuit, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttipoiva, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rSLabelFecha1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtobrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcuit, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttipoiva, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void jPanel6MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseDragged

        this.setLocation(this.getLocation().x + evt.getX() - x, this.getLocation().y + evt.getY() - y);
    }//GEN-LAST:event_jPanel6MouseDragged

    private void jPanel6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MousePressed
        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jPanel6MousePressed

    void cargartabla() {
        String[] titulos = {"N° Recibo/Pago", "Fecha", "Total", "Estado", "Fecha Vencimiento/N Recibo"};
        String[] datos = new String[6];

        String consulta = "(SELECT\n"
                + "os_pagos.idPago,\n"
                + "os_pagos.fecha,\n"
                + "os_pagos.total,\n"
                + "'PAGADO',\n"
                + "os_pagos.idRecibo,\n"
                + "os_pagos.id_obrasocial as id_obrasocial\n"
                + "FROM\n"
                + "os_pagos\n"
                + "where id_obrasocial=" + idObraSocial + ")\n"
                + "UNION\n"
                + "(SELECT\n"
                + "os_recibos.id_recibo,\n"
                + "os_recibos.fecha,\n"
                + "os_recibos.total,\n"
                + "'DEBE',\n"
                + "os_recibos.fecha_vencimiento,\n"
                + "os_recibos.id_obrasocial  as id_obrasocial\n"
                + "FROM\n"
                + "os_recibos\n"
                + "where id_obrasocial=" + idObraSocial + ")";

        modelo = new DefaultTableModel(null, titulos) {

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            Statement St = cn.createStatement();

            ResultSet Rs = St.executeQuery(consulta);
            while (Rs.next()) {
                datos[0] = Rs.getString(1);
                datos[1] = Rs.getString(2);
                datos[2] = Rs.getString(3);
                datos[3] = Rs.getString(4);
                datos[4] = Rs.getString(5);
                datos[5] = Rs.getString(6);
                total = total + Rs.getDouble(3);

                modelo.addRow(datos);
            }
            tablarecibos.setModel(modelo);

            lblTotal.setText("$" + String.valueOf(total));
            /////////////////////////////////////////////////////////////
//            alinear();
//            tablarecibos.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
//            tablarecibos.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
//            tablarecibos.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            tablarecibos.getColumnModel().getColumn(1).setPreferredWidth(270);
            tablarecibos.getColumnModel().getColumn(1).setMaxWidth(270);
            tablarecibos.getColumnModel().getColumn(1).setMinWidth(270);

            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }

//    void alinear() {
//        alinearCentro = new DefaultTableCellRenderer();
//        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
//        alinearDerecha = new DefaultTableCellRenderer();
//        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
//        alinearIzquierda = new DefaultTableCellRenderer();
//        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
//    }

    private void txtobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialActionPerformed
        Optional<ObraSocialAfip> osResult = ObraSocialAfip.buscarOSAfip(txtobrasocial.getText(), listaOSAfip);
        if (osResult.isPresent()) {
            OS = osResult.get();
            idObraSocial = OS.getIdObraSocial();
            txtcuit.setText(String.valueOf(OS.getCuit()));
            txttipoiva.setText(OS.getDescripcion());
            txtdireccion.setText(OS.getDireccion());
            txtprovincia.setText(OS.getNombre_provincia());
            cargartabla();
        } else {
            JOptionPane.showMessageDialog(null, "Obra social no encontrada");
        }
    }//GEN-LAST:event_txtobrasocialActionPerformed

    private void txtobrasocialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtobrasocialKeyPressed

    private void txtobrasocialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtobrasocialKeyReleased

    private void txtdireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdireccionKeyPressed

    private void txtdireccionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdireccionKeyReleased

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        ObraSocial=txtobrasocial.getText();
        if(!ObraSocial.equals("")){
                new ReciboPagoOs(null, true).setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null, "Debe seleccionar una obra social");
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed

    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed

        dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void txtprovinciaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprovinciaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtprovinciaKeyPressed

    private void txtprovinciaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprovinciaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtprovinciaKeyReleased

    private void txtprovinciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtprovinciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtprovinciaActionPerformed

    private void txtcuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcuitActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcuitActionPerformed

    private void txtcuitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcuitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcuitKeyPressed

    private void txtcuitKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcuitKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcuitKeyReleased

    private void txttipoivaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttipoivaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttipoivaActionPerformed

    private void txttipoivaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttipoivaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttipoivaKeyPressed

    private void txttipoivaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttipoivaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txttipoivaKeyReleased

    private void tablarecibosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablarecibosKeyPressed
        /////Borrar una fila de la tabla pedidos

    }//GEN-LAST:event_tablarecibosKeyPressed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyPressed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        TableRowSorter sorter = new TableRowSorter(modelo);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
        tablarecibos.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscarKeyReleased

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSButtonIconOne btnCerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnaceptar;
    private RSMaterialComponent.RSButtonMaterialIconUno btncancelar;
    private RSMaterialComponent.RSButtonMaterialIconUno btnsalir;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JLabel lblTotal;
    private rojeru_san.rsdate.RSLabelFecha rSLabelFecha1;
    private RSMaterialComponent.RSTableMetroCustom tablarecibos;
    private javax.swing.JTextArea txtConcepto;
    private RSMaterialComponent.RSTextFieldOne txtbuscar;
    private RSMaterialComponent.RSTextFieldOne txtcuit;
    private RSMaterialComponent.RSTextFieldOne txtdireccion;
    private RSMaterialComponent.RSTextFieldOne txtobrasocial;
    private RSMaterialComponent.RSTextFieldOne txtprovincia;
    private RSMaterialComponent.RSTextFieldOne txttipoiva;
    private javax.swing.JLabel txtusuario;
    // End of variables declaration//GEN-END:variables
}
