package vista;

import controlador.ConexionMySQLPami;
import controlador.Funciones;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Entregas extends javax.swing.JDialog {

    DefaultTableModel model;
    public static String id_control, id_medico, direccion, localidad, nombre_medico, medico;
    public static int desde, hasta, cantidad;

    public Entregas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        //  setIconImage(new ImageIcon(getClass().getResource("/Imagenes/cbtcirc.ico")).getImage());
        this.setLocationRelativeTo(null);
        tablaentregas.setRowHeight(25);
        cargartabla();
        dobleclick();
        Funciones.funcionescape(this);
        tablaentregas.getColumnModel().getColumn(0).setPreferredWidth(3);
        tablaentregas.getColumnModel().getColumn(1).setPreferredWidth(140);
        tablaentregas.getColumnModel().getColumn(2).setPreferredWidth(50);
        tablaentregas.getColumnModel().getColumn(3).setPreferredWidth(3);
        tablaentregas.getColumnModel().getColumn(4).setPreferredWidth(100);
        tablaentregas.getColumnModel().getColumn(5).setPreferredWidth(20);
        tablaentregas.getColumnModel().getColumn(6).setPreferredWidth(10);
        tablaentregas.getColumnModel().getColumn(7).setPreferredWidth(10);
        tablaentregas.getColumnModel().getColumn(8).setPreferredWidth(10);
        tablaentregas.getColumnModel().getColumn(9).setPreferredWidth(10);
        tablaentregas.getColumnModel().getColumn(10).setPreferredWidth(10);
        tablaentregas.getColumnModel().getColumn(11).setPreferredWidth(100);
        tablaentregas.getColumnModel().getColumn(12).setPreferredWidth(100);

    }

    void dobleclick() {
        tablaentregas.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    id_control = tablaentregas.getValueAt(tablaentregas.getSelectedRow(), 0).toString();
                    new cargafecha(null, true).setVisible(true);
                    cargartabla();
                }
            }
        });
    }

    public void cargartabla() {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();

        String[] Titulo = {"id_control", "Dirección", "Mes", "Año", "Entrega", "Cápita", "Desde", "Hasta", "Cantidad", "Fecha Entrega Correo", "Fecha Recibido Medico", "Nombre Recibido", "Observación", "Especialidad"};
        Object[] Registros = new Object[14];

        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        String sql = "SELECT control.id_Control, medicos.Nombre, capita_entrega, especialidades.Especialidad, medicos.Direccion, localidades.nombre_localidad, mes, año, cantidadx4, entrega, cantidadx4, desdex4, hastax4, Observaciones, estado, fecha_entrega, fecha_recibido, control.nombre_control, control.observacion, direccion_entrega  FROM control INNER JOIN medicos_control ON medicos_control.id_Control = control.id_Control INNER JOIN medicos ON medicos.id_Medico = medicos_control.id_Medico INNER JOIN especialidades ON medicos.id_Especialidad = especialidades.id_Especialidad INNER JOIN localidades ON medicos.id_localidad = localidades.id_localidad where medicos.id_Medico=" + id_medico;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString("control.id_Control");
                Registros[1] = rs.getString("direccion_entrega");
                Registros[2] = rs.getString("Mes");
                Registros[3] = rs.getString("año");
                Registros[4] = rs.getString("entrega");
                Registros[5] = rs.getString("capita_entrega");
                Registros[6] = rs.getString("desdex4");
                Registros[7] = rs.getString("hastax4");
                Registros[8] = rs.getString("cantidadx4");
                Registros[9] = rs.getString("fecha_entrega");
                Registros[10] = rs.getString("fecha_recibido");
                Registros[11] = rs.getString("control.nombre_control");
                Registros[12] = rs.getString("control.observacion");
                Registros[13] = rs.getString("especialidades.Especialidad");
                model.addRow(Registros);
                nombre_medico = rs.getString("medicos.Nombre");
                direccion = rs.getString("medicos.Direccion");
                localidad = rs.getString("localidades.nombre_localidad");
                medico = rs.getString("especialidades.Especialidad");

            }
            txtmedico.setText(nombre_medico);
            txtdireccion.setText(direccion);
            txtlocalidad.setText(localidad);
            txtespecialidad.setText(medico);
            tablaentregas.setModel(model);
            //tablaentregas.setDefaultRenderer (Object.class, new ColoreaTabla());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        tablaentregas.setModel(model);
        tablaentregas.setAutoCreateRowSorter(true);
    }

    public void filtrar(String valor) {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();

        String[] Titulo = {"id_control", "Dirección", "Mes", "Año", "Entrega", "Cápita", "Desde", "Hasta", "Cantidad", "Fecha Entrega Correo", "Fecha Recibido Medico", "Nombre Recibido", "Observación", "Especialidad"};
        Object[] Registros = new Object[14];

        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        String sql = "SELECT control.id_Control, medicos.Nombre, capita_entrega, especialidades.Especialidad, medicos.Direccion, localidades.nombre_localidad, mes, año, cantidadx4, entrega, cantidadx4, desdex4, hastax4, Observaciones, estado, fecha_entrega, fecha_recibido, control.nombre_control, control.observacion, direccion_entrega  FROM control INNER JOIN medicos_control ON medicos_control.id_Control = control.id_Control INNER JOIN medicos ON medicos.id_Medico = medicos_control.id_Medico INNER JOIN especialidades ON medicos.id_Especialidad = especialidades.id_Especialidad INNER JOIN localidades ON medicos.id_localidad = localidades.id_localidad where medicos.id_Medico=" + id_medico + " AND CONCAT(control.id_Control, ' ',medicos.Nombre, ' ', mes, ' ', año, ' ', entrega)"
                + "LIKE '%" + valor + "%'";// medicos.Nombre="+valor;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString("control.id_Control");
                Registros[1] = rs.getString("direccion_entrega");
                Registros[2] = rs.getString("Mes");
                Registros[3] = rs.getString("año");
                Registros[4] = rs.getString("entrega");
                Registros[5] = rs.getString("capita_entrega");
                Registros[6] = rs.getString("desdex4");
                Registros[7] = rs.getString("hastax4");
                Registros[8] = rs.getString("cantidadx4");
                Registros[9] = rs.getString("fecha_entrega");
                Registros[10] = rs.getString("fecha_recibido");
                Registros[11] = rs.getString("control.nombre_control");
                Registros[12] = rs.getString("control.observacion");
                Registros[13] = rs.getString("especialidades.Especialidad");
                model.addRow(Registros);
                nombre_medico = rs.getString("medicos.Nombre");
                direccion = rs.getString("medicos.Direccion");
                localidad = rs.getString("localidades.nombre_localidad");
                medico = rs.getString("especialidades.Especialidad");

            }
            txtmedico.setText(nombre_medico);
            txtdireccion.setText(direccion);
            txtlocalidad.setText(localidad);
            txtespecialidad.setText(medico);
            tablaentregas.setModel(model);
            //tablaentregas.setDefaultRenderer (Object.class, new ColoreaTabla());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        tablaentregas.setModel(model);
        tablaentregas.setAutoCreateRowSorter(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        Modificar = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaentregas = new javax.swing.JTable();
        txtmedico = new javax.swing.JTextField();
        txtespecialidad = new javax.swing.JTextField();
        txtdireccion = new javax.swing.JTextField();
        txtlocalidad = new javax.swing.JTextField();
        btnmodmed6 = new javax.swing.JButton();

        Modificar.setText("Modificar");
        Modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Modificar);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Medicos"));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Apellido y Nombre:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Dirección:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Localidad:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("Medico");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Buscar:");

        txtbuscar.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        tablaentregas.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tablaentregas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "id_control", "Dirección", "Mes", "Año", "Entrega", "Cápita", "Desde", "Hasta", "Cantidad", "Fecha Entrega Correro", "Fecha Recibido  Medico", "Nombre Recibido", "Observación", "Especialidad"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaentregas.setComponentPopupMenu(jPopupMenu1);
        tablaentregas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaentregasKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaentregas);

        txtmedico.setEditable(false);
        txtmedico.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmedico.setForeground(new java.awt.Color(0, 102, 204));
        txtmedico.setBorder(null);
        txtmedico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmedicoKeyReleased(evt);
            }
        });

        txtespecialidad.setEditable(false);
        txtespecialidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtespecialidad.setForeground(new java.awt.Color(0, 102, 204));
        txtespecialidad.setBorder(null);
        txtespecialidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtespecialidadKeyReleased(evt);
            }
        });

        txtdireccion.setEditable(false);
        txtdireccion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtdireccion.setBorder(null);
        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdireccionKeyReleased(evt);
            }
        });

        txtlocalidad.setEditable(false);
        txtlocalidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtlocalidad.setForeground(new java.awt.Color(0, 102, 204));
        txtlocalidad.setBorder(null);
        txtlocalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlocalidadKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 363, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtlocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtmedico, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtespecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 668, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtmedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(txtespecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(txtlocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnmodmed6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodmed6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnmodmed6.setText("Volver");
        btnmodmed6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodmed6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnmodmed6)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnmodmed6)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        filtrar(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void txtmedicoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmedicoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmedicoKeyReleased

    private void txtespecialidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtespecialidadKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtespecialidadKeyReleased

    private void txtdireccionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdireccionKeyReleased

    private void txtlocalidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlocalidadKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtlocalidadKeyReleased

    private void btnmodmed6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodmed6ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnmodmed6ActionPerformed

    private void tablaentregasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaentregasKeyPressed

    }//GEN-LAST:event_tablaentregasKeyPressed

    private void ModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificarActionPerformed
        desde = Integer.valueOf(tablaentregas.getValueAt(tablaentregas.getSelectedRow(), 6).toString());
        cantidad = Integer.valueOf(tablaentregas.getValueAt(tablaentregas.getSelectedRow(), 8).toString());
        hasta = desde + cantidad - 1;
        new PeriodoEntrega(null, true).setVisible(true);
        if (desde != 0 && hasta != 0) {
            ConexionMySQLPami mysql = new ConexionMySQLPami();
            Connection cn = mysql.Conectar();
            String control = tablaentregas.getValueAt(tablaentregas.getSelectedRow(), 0).toString();
            String sql = "UPDATE control SET desdex4=?, hastax4=? WHERE id_Control=" + control;
            try {
                PreparedStatement pst = cn.prepareStatement(sql);
                pst.setInt(1, desde);
                tablaentregas.setValueAt(hasta, tablaentregas.getSelectedRow(), tablaentregas.getSelectedColumn() + 1);
                pst.setInt(2, hasta);
                int n = pst.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Modificado con exito...");
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
            cargartabla();
        }
    }//GEN-LAST:event_ModificarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Modificar;
    private javax.swing.JButton btnmodmed6;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaentregas;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtespecialidad;
    private javax.swing.JTextField txtlocalidad;
    private javax.swing.JTextField txtmedico;
    // End of variables declaration//GEN-END:variables
}
