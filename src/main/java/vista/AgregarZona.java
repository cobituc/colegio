package vista;

import controlador.ConexionMySQLPami;
import controlador.Funciones;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class AgregarZona extends javax.swing.JDialog {

    DefaultTableModel model;
    public static int modifica = 0;

    public AgregarZona(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        cargartabla();
        dobleclick();
        Funciones.funcionescape(this);

    }

    public void cargartabla() {
        ConexionMySQLPami mysql = new ConexionMySQLPami();
        Connection cn = mysql.Conectar();
        String[] Titulo = {"Codigo", "Observación"};
        Object[] Registros = new Object[2];
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT nombre,observacion FROM zonas";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                model.addRow(Registros);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        tablaespecialidad.setModel(model);
        tablaespecialidad.setAutoCreateRowSorter(true);
    }

      @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtesp = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaespecialidad = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        btnaceptar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar Zona", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Codigo:");

        txtesp.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtesp.setForeground(new java.awt.Color(0, 102, 204));

        tablaespecialidad.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaespecialidad.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaespecialidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaespecialidadKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaespecialidad);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Obs.:");

        jTextField1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jTextField1.setForeground(new java.awt.Color(0, 102, 204));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtesp, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtesp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnaceptar.setText("Agregar");
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnaceptar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 224, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnaceptar)
                    .addComponent(btnsalir))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        if (modifica == 0) {
            if (!txtesp.getText().equals("")) {
                ConexionMySQLPami mysql = new ConexionMySQLPami();
                Connection cn = mysql.Conectar();
                String sql = "INSERT INTO zonas (nombre,observacion) VALUES (?,?)";
                PreparedStatement pst;
                try {
                    pst = cn.prepareStatement(sql);
                    pst.setString(1, txtesp.getText());
                    pst.setString(2, jTextField1.getText());
                    int n = pst.executeUpdate();
                    if (n > 0) {
                        cargartabla();
                        txtesp.setText("");
                        jTextField1.setText("");
                    } else {
                        JOptionPane.showMessageDialog(null, "Error al cargar la zona");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AltaEspecialidad.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            if (!txtesp.getText().equals("")) {
                ConexionMySQLPami mysql = new ConexionMySQLPami();
                Connection cn = mysql.Conectar();
                String sql = "UPDATE zonas SET nombre=?, observacion=? WHERE nombre='" + tablaespecialidad.getValueAt(tablaespecialidad.getSelectedRow(), 0).toString() + "'";
                try {
                    PreparedStatement pst = cn.prepareStatement(sql);
                    pst.setString(1, txtesp.getText());
                    pst.setString(2, jTextField1.getText());
                    int n = pst.executeUpdate();
                    if (n > 0) {
                        cargartabla();
                        txtesp.setText("");
                        jTextField1.setText("");
                    } else {
                        JOptionPane.showMessageDialog(null, "Error al modificar la zona...");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AltaMedicos.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe ingresar un codigo...");
            }
        }

    }//GEN-LAST:event_btnaceptarActionPerformed

    void dobleclick() {
        tablaespecialidad.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    ConexionMySQLPami mysql = new ConexionMySQLPami();
                    Connection cn = mysql.Conectar();
                    String id = tablaespecialidad.getValueAt(tablaespecialidad.getSelectedRow(), 0).toString();
                    String sql = "SELECT nombre,observacion FROM zonas where nombre='" + id + "'";
                    try {
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sql);
                        while (rs.next()) {
                            txtesp.setText(rs.getString(1));
                            jTextField1.setText(rs.getString(2));
                        }
                    } catch (Exception ec) {
                        JOptionPane.showMessageDialog(null, ec);
                    }
                }
            }
        });
    }

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();      
    }//GEN-LAST:event_btnsalirActionPerformed

    private void tablaespecialidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaespecialidadKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            DefaultTableModel temp = (DefaultTableModel) tablaespecialidad.getModel();
            if (tablaespecialidad.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
            } else {
                try {
                    ConexionMySQLPami mysql = new ConexionMySQLPami();
                    Connection cn = mysql.Conectar();
                    PreparedStatement pst3 = cn.prepareStatement("DELETE FROM zonas WHERE Id =" + tablaespecialidad.getValueAt(tablaespecialidad.getSelectedRow(), 0).toString() );
                    pst3.execute();                    
                    temp.removeRow(tablaespecialidad.getSelectedRow());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                }
            }
        }
    }//GEN-LAST:event_tablaespecialidadKeyPressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTable tablaespecialidad;
    private javax.swing.JTextField txtesp;
    // End of variables declaration//GEN-END:variables
}
