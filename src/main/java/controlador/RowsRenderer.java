package controlador;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class RowsRenderer extends DefaultTableCellRenderer {

    public RowsRenderer() {
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {

        JLabel celda = (JLabel) super.getTableCellRendererComponent(table, value, selected, focused, row, column);

        celda.setHorizontalAlignment(SwingConstants.CENTER);

        setBackground(Color.WHITE);
        table.setForeground(Color.BLACK);
        super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        int estado = Integer.valueOf(table.getValueAt(row, 6).toString());
        
        switch (estado) {
            case 1:
                this.setBackground(Color.decode("#fcba03"));
                this.setForeground(Color.BLACK);
                break;
            case 2:
                this.setBackground(Color.decode("#03b1fc"));
                this.setForeground(Color.BLACK);
                break;
            case 3:
                this.setBackground(Color.decode("#98FB98"));
                this.setForeground(Color.BLACK);
                break;
            case 4:
                this.setBackground(Color.decode("#fc1c03"));
                this.setForeground(Color.BLACK);
                break;
            default:
                this.setBackground(Color.WHITE);
                this.setForeground(Color.BLACK);
                break;
        }

        if (selected) {
            this.setBackground(Color.BLACK);
            this.setForeground(Color.WHITE);

        }

        return celda;

    }

}
