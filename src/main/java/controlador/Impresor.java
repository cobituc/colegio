/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
 
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
 
public class Impresor {
 
    private final static Logger LOGGER = Logger.getLogger("");
 
    
 
    /***
     * Manda un documento a imprimir a la impresora que se indique en el dialogo
     * @param pdf
     * @throws PrinterException
     * @throws IOException 
     */
    public void imprimir(File pdf) throws PrinterException, IOException {
        // Indicamos el nombre del archivo Pdf que deseamos imprimir
        PDDocument document = PDDocument.load(pdf);
 
        PrinterJob job = PrinterJob.getPrinterJob();
 
        LOGGER.log(Level.INFO, "Mostrando el dialogo de impresion");
        if (job.printDialog() == true) {            
            job.setPageable(new PDFPageable(document));
 
            LOGGER.log(Level.INFO, "Imprimiendo documento");
            job.print();
        }
    }
}