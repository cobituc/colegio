package controlador;

import java.util.ArrayList;
import javax.swing.AbstractListModel;
import modelo.Colegiado;

public class CustomListModel extends AbstractListModel {

    private ArrayList<Colegiado> lista = new ArrayList<>();

    @Override
    public int getSize() {
        return lista.size();
    }

    @Override
    public Object getElementAt(int index) {
        Colegiado c = lista.get(index);
        return c.getMatricula() + " - " + c.getNombre();
    }

    public void addColegiado(Colegiado c) {
        lista.add(c);
        this.fireIntervalAdded(this, getSize(), getSize() + 1);
    }

    public void eliminarColegiado(int index0) {
        lista.remove(index0);
        this.fireIntervalRemoved(index0, getSize(), getSize() + 1);
    }
      
    public Colegiado getColegiado(int index) {
        return lista.get(index);
    }

}
