package controlador;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import javax.swing.JTable;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableWorkbook;
import jxl.write.WritableSheet;
import jxl.write.WriteException;

public class Exportar {

    private File archivo;
    private List<JTable> tabla;
    private List<String> nom_archivo;

    public Exportar(File archivo, List<JTable> tabla, List<String> nom_archivo) throws Exception {

        this.archivo = archivo;
        this.tabla = tabla;
        this.nom_archivo = nom_archivo;
        if (nom_archivo.size() != tabla.size()) {
            throw new Exception("Error");
        }

    }

    public boolean export() {
        try {
            DataOutputStream out = new DataOutputStream(new FileOutputStream(archivo));
            WritableWorkbook w = Workbook.createWorkbook(out);
            for (int index = 0; index < tabla.size(); index++) {
                JTable table = tabla.get(index);
                WritableSheet s = w.createSheet(nom_archivo.get(index), 0);
                for (int i = 0; i < table.getColumnCount(); i++) {
                    for (int j = 0; j < table.getRowCount(); j++) {
                        Object o = table.getValueAt(j, i);
                        s.addCell(new Label(i, j, String.valueOf(o)));
                    }

                }

            }
            w.write();
            w.close();
            return true;
        } catch (IOException | WriteException e) {
            return false;
        }

    }

}
