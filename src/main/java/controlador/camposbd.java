package controlador;

public class camposbd {

    String fecha_de_registracion, concepto_del_importe_registrado, numero_de_poliza,
            entidad_aseguradora, moneda, importe_del_ingreso, importe_del_egreso,
            observaciones;

    public camposbd(String fecha_de_registracion, String concepto_del_importe_registrado,
            String numero_de_poliza, String entidad_aseguradora, String moneda, 
            String importe_del_ingreso, String importe_del_egreso, String observaciones) {
        
        this.fecha_de_registracion = fecha_de_registracion;
        this.concepto_del_importe_registrado = concepto_del_importe_registrado;
        this.numero_de_poliza = numero_de_poliza;
        this.entidad_aseguradora = entidad_aseguradora;
        this.moneda = moneda;
        this.importe_del_ingreso = importe_del_ingreso;
        this.importe_del_egreso = importe_del_egreso;
        this.observaciones = observaciones;
    }

    public String getfecha_de_registracion() {
        return fecha_de_registracion;
    }

    public void setfecha_de_registracion(String fecha_de_registracion) {
        this.fecha_de_registracion = fecha_de_registracion;
    }

    public String getconcepto_del_importe_registrado() {
        return concepto_del_importe_registrado;
    }

    public void setconcepto_del_importe_registrado(String concepto_del_importe_registrado) {
        this.concepto_del_importe_registrado = concepto_del_importe_registrado;
    }

    public String getnumero_de_poliza() {
        return numero_de_poliza;
    }

    public void setnumero_de_poliza(String numero_de_poliza) {
        this.numero_de_poliza = numero_de_poliza;
    }

    public String getentidad_aseguradora() {
        return entidad_aseguradora;
    }

    public void setentidad_aseguradora(String entidad_aseguradora) {
        this.entidad_aseguradora = entidad_aseguradora;
    }

    public String getmoneda() {
        return moneda;
    }

    public void setmoneda(String moneda) {
        this.moneda = moneda;
    }
    
     public String getimporte_del_ingreso() {
        return importe_del_ingreso;
    }

    public void setimporte_del_ingreso(String importe_del_ingreso) {
        this.importe_del_ingreso = importe_del_ingreso;
    }
    
     public String getimporte_del_egreso() {
        return importe_del_egreso;
    }

    public void setimporte_del_egreso(String importe_del_egreso) {
        this.importe_del_egreso = importe_del_egreso;
    }
    
     public String getobservaciones() {
        return observaciones;
    }

    public void setobservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
}
