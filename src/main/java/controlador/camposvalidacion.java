

package controlador;


public class camposvalidacion {
    String matricula, cod_obra_social, periodo, obra_social, importe,practicas,ordenes,id_ddjj;

    public camposvalidacion(String matricula, String cod_obra_social, String periodo, String obra_social, String importe, String practicas, String ordenes, String id_ddjj) {
        this.matricula = matricula;
        this.cod_obra_social = cod_obra_social;
        this.periodo = periodo;
        this.obra_social = obra_social;
        this.importe = importe;
        this.practicas = practicas;
        this.ordenes = ordenes;
        this.id_ddjj = id_ddjj;
    }

    public String getMatricula() {
        return matricula;
    }
        

    public String getCod_obra_social() {
        return cod_obra_social;
    }

    public String getPeriodo() {
        return periodo;
    }

    public String getObra_social() {
        return obra_social;
    }

    public String getImporte() {
        return importe;
    }

    public String getPracticas() {
        return practicas;
    }

    public String getOrdenes() {
        return ordenes;
    }

    public String getId_ddjj() {
        return id_ddjj;
    }
    
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public void setCod_obra_social(String cod_obra_social) {
        this.cod_obra_social = cod_obra_social;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public void setObra_social(String obra_social) {
        this.obra_social = obra_social;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public void setPracticas(String practicas) {
        this.practicas = practicas;
    }

    public void setOrdenes(String ordenes) {
        this.ordenes = ordenes;
    }
    public void setId_ddjj() {
        this.id_ddjj= id_ddjj;
    }
    
    
    
    
    
    
    
}
