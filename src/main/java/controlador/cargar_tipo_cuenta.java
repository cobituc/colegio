package controlador;

public class cargar_tipo_cuenta {

    int id_cuenta;
    String CuentaNombre;

    public cargar_tipo_cuenta() {

    }

    public cargar_tipo_cuenta(int id_cuenta, String CuentaNombre) {
        this.id_cuenta = id_cuenta;
        this.CuentaNombre = CuentaNombre;
    }

    public int getId_cuenta() {
        return id_cuenta;
    }

    public void setId_cuenta(int id_cuenta) {
        this.id_cuenta = id_cuenta;
    }

    public void setCuentaNombre(String CuentaNombre) {
        this.CuentaNombre = CuentaNombre;
    }

    public String getCuentaNombre() {
        return CuentaNombre;
    }

    @Override
    public String toString() {
        return CuentaNombre;
    }

    
    
}
