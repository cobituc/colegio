

package controlador;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class DatosGaficoValidadores {

    private String validador;
    private int idValidador;
    private int ordenes;
    private int periodo;

    public DatosGaficoValidadores(String validador, int idValidador, int ordenes, int periodo) {
        this.validador = validador;
        this.idValidador = idValidador;
        this.ordenes = ordenes;
        this.periodo = periodo;
    }

    public String getValidador() {
        return validador;
    }

    public void setValidador(String validador) {
        this.validador = validador;
    }

    public int getIdValidador() {
        return idValidador;
    }

    public void setIdValidador(int idValidador) {
        this.idValidador = idValidador;
    }

    public int getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(int ordenes) {
        this.ordenes = ordenes;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }
    
    public static void cargarDatos(Connection connection, ArrayList<DatosGaficoValidadores> lista, int periodo) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs;
            rs = instruccion.executeQuery("SELECT * FROM vista_totales_validador WHERE periodo=" +periodo);
            while (rs.next()) {
                
                lista.add(new DatosGaficoValidadores(rs.getString(1),rs.getInt(2),rs.getInt(3),rs.getInt(4)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
