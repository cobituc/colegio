package controlador;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageTest {

    public static BufferedImage loadImage(String pathname) {
        BufferedImage bufim = null;
        try {
            bufim = ImageIO.read(new File(pathname));
        } catch (IOException e) {
        }
        return bufim;
    }

    public static BufferedImage resize(BufferedImage bufferedImage, int newW, int newH) {
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();
        BufferedImage bufim = new BufferedImage(newW, newH, bufferedImage.getType());
        Graphics2D g = bufim.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(bufferedImage, 0, 0, newW, newH, 0, 0, w, h, null);
        g.dispose();
        return bufim;
    }

    public static BufferedImage rotate(BufferedImage bufferedImage, int angle) {
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();
        BufferedImage bufim = new BufferedImage(w, h, bufferedImage.getType());
        Graphics2D g = bufim.createGraphics();
        g.rotate(Math.toRadians(angle), w / 2, h / 2);
        g.drawImage(bufferedImage, null, 0, 0);
        return bufim;
    }

    public static void saveImage(BufferedImage bufferedImage, String pathname) {
        try {
            String format = (pathname.endsWith(".png")) ? "png" : "jpg";
            ImageIO.write(bufferedImage, format, new File(pathname));
        } catch (IOException e) {
        }
    }

    public static BufferedImage makeTranslucentImage(BufferedImage bufferedImage, float transparency) {
        BufferedImage bufim = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TRANSLUCENT);
        Graphics2D g = bufim.createGraphics();
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, transparency));
        g.drawImage(bufferedImage, null, 0, 0);
        g.dispose();
        return bufim;
    }

    public static BufferedImage makeColorTransparent(BufferedImage bufferedImage, Color color) {
        BufferedImage bufim = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = bufim.createGraphics();
        g.setComposite(AlphaComposite.Src);
        g.drawImage(bufferedImage, null, 0, 0);
        g.dispose();
        for (int i = 0; i < bufim.getHeight(); i++) {
            for (int j = 0; j < bufim.getWidth(); j++) {
                if (bufim.getRGB(j, i) == color.getRGB()) {
                    bufim.setRGB(j, i, 0x8F1C1C);
                }
            }
        }
        return bufim;
    }

    public static BufferedImage[] splitImage(BufferedImage bufferedImage, int rows, int cols) {
        int w = bufferedImage.getWidth() / cols;
        int h = bufferedImage.getHeight() / rows;
        int num = 0;
        BufferedImage imgs[] = new BufferedImage[w * h];
        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < cols; x++) {
                imgs[num] = new BufferedImage(w, h, bufferedImage.getType());
                // Tell the graphics to draw only one block of the image  
                Graphics2D g = imgs[num].createGraphics();
                g.drawImage(bufferedImage, 0, 0, w, h, w * x, h * y, w * x + w, h * y + h, null);
                g.dispose();
                num++;
            }
        }
        return imgs;
    }

    public static BufferedImage flipVertical(BufferedImage bufferedImage) {
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();
        BufferedImage bufim = new BufferedImage(w, h, bufferedImage.getColorModel().getTransparency());
        Graphics2D g = bufim.createGraphics();
        g.drawImage(bufferedImage, 0, 0, w, h, 0, h, w, 0, null);
        g.dispose();
        return bufim;
    }

    public static BufferedImage flipHorizontal(BufferedImage bufferedImage) {
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();
        BufferedImage bufim = new BufferedImage(w, h, bufferedImage.getType());
        Graphics2D g = bufim.createGraphics();
        g.drawImage(bufferedImage, 0, 0, w, h, w, 0, 0, h, null);
        g.dispose();
        return bufim;
    }
}
