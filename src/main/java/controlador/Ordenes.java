package controlador;

public class Ordenes {

    
    String numero_orden,nombre, direccion, localidad, especialidad1, codigo_postal, desdex4,desdex2, hastax4,hastax2;

    public Ordenes(String numero_orden,String nombre, String direccion, String localidad, String especialidad1, String codigo_postal, String desdex4, String desdex2, String hastax4, String hastax2) {
        this.numero_orden=numero_orden;                
        this.nombre = nombre;
        this.direccion = direccion;
        this.localidad = localidad;
        this.especialidad1 = especialidad1;
        this.codigo_postal = codigo_postal;
        this.desdex4 = desdex4;
        this.hastax4 = hastax4;
        this.desdex2 = desdex2;
        this.hastax2 = hastax2;
    }

    public String getNumero_orden() {
        return numero_orden;
    }

    public void setNumero_orden(String numero_orden) {
        this.numero_orden = numero_orden;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getEspecialidad1() {
        return especialidad1;
    }

    public void setEspecialidad1(String especialidad1) {
        this.especialidad1 = especialidad1;
    }

    public String getCodigo_postal() {
        return codigo_postal;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public String getDesdex4() {
        return desdex4;
    }

    public void setDesdex4(String desdex4) {
        this.desdex4 = desdex4;
    }

    public String getHastax4() {
        return hastax4;
    }

    public void setHastax4(String hastax4) {
        this.hastax4 = hastax4;
    }

    public String getDesdex2() {
        return desdex2;
    }

    public void setDesdex2(String desdex2) {
        this.desdex2 = desdex2;
    }

    public String getHastax2() {
        return hastax2;
    }

    public void setHastax2(String hastax2) {
        this.hastax2 = hastax2;
    }

    

}
   