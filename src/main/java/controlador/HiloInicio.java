package controlador;

import vista.Login;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JProgressBar;
import modelo.ObraSocialAfip;
import vista.Login;

public class HiloInicio extends Thread {

    JProgressBar progreso;
    SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");

    private modelo.ConexionMariaDB conexion;
    public static ArrayList<modelo.Colegiado> listaColegiados;
    public static ArrayList<modelo.Laboratorio> listaLaboratorios;
    public static ArrayList<modelo.ObraSocial> listaOS;
    public static ArrayList<modelo.Localidad> listaLocalidades;
    public static ArrayList<modelo.ObraSocialAfip> listaOSAfip;
    public static ArrayList<modelo.FormaDePago> listaFormaDePago;
    public static ArrayList<modelo.FormaDePagoDebito> listaDetalleFormaDePagoDebito;
    public static ArrayList<modelo.FormaDePagoCredito> listaDetalleFormaDePagoCredito;    
    public static ArrayList<modelo.FormaDePagoCuentaCorriente> listaDetalleFormaDePagoCuentaCorriente;
    public static ArrayList<modelo.Banco> listaBancos;

    public static int contadorpractica = 0;
    public static int contadoranalisis = 0;
    // public static int contadorobrasocial = 0;
    public static int contadorafiliado = 0;

    public HiloInicio(JProgressBar progreso1) {
        super();
        this.progreso = progreso1;
    }

    public void run() {
        int i = 0;
        while (i < 30) {
            progreso.setValue(i);
            i++;
            // pausa(10);
        }
        cargarLocalidades();
        cargarFormaDePago();

        while (i <= 50) {
            progreso.setValue(i);
            i++;
            // pausa(10);
        }
        cargarColegiados();
        cargarFormaDePagoCredito();
        cargarFormaDePagoCuentaCorriente();
        cargarFormaDePagoDebito();
        while (i <= 60) {
            progreso.setValue(i);
            i++;
            // pausa(10);
        }
        cargarObraSocialAfip();
        while (i <= 70) {
            progreso.setValue(i);
            i++;
            //  pausa(10);
        }
        cargarBancos();
        cargarObraSocial();
        while (i <= 100) {
            progreso.setValue(i);
            i++;
            // pausa(10);
        }
        new Login().setVisible(true);

    }

    public static Date aDate(String strFecha) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        Date fecha = null;
        try {
            fecha = formatoDelTexto.parse(strFecha);
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        return fecha;
    }

    void cargarColegiados() {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaColegiados = new ArrayList<modelo.Colegiado>();
        modelo.Colegiado.cargarColegiado(conexion.getConnection(), listaColegiados);
        conexion.cerrarConexion();

    }

    void cargarLocalidades() {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaLocalidades = new ArrayList<modelo.Localidad>();
        modelo.Localidad.cargarLocalidades(conexion.getConnection(), listaLocalidades);
        conexion.cerrarConexion();

    }

    void cargarFormaDePago() {
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaFormaDePago = new ArrayList<modelo.FormaDePago>();
        modelo.FormaDePago.cargarFormaDePago(conexion.getConnection(), listaFormaDePago);
        conexion.cerrarConexion();
    }

     void cargarBancos() {
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaBancos = new ArrayList<modelo.Banco>();
        modelo.Banco.cargarBanco(conexion.getConnection(), listaBancos);
        conexion.cerrarConexion();
    }

    
    void cargarFormaDePagoDebito() {
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaDetalleFormaDePagoDebito = new ArrayList<modelo.FormaDePagoDebito>();
        modelo.FormaDePagoDebito.cargarDetalleDePago(conexion.getConnection(), listaDetalleFormaDePagoDebito);
        conexion.cerrarConexion();
    }

    void cargarFormaDePagoCredito() {
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaDetalleFormaDePagoCredito = new ArrayList<modelo.FormaDePagoCredito>();
        modelo.FormaDePagoCredito.cargarDetalleDePago(conexion.getConnection(), listaDetalleFormaDePagoCredito);
        conexion.cerrarConexion();
    }

    void cargarFormaDePagoCuentaCorriente() {
        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaDetalleFormaDePagoCuentaCorriente = new ArrayList<modelo.FormaDePagoCuentaCorriente>();
        modelo.FormaDePagoCuentaCorriente.cargarDetalleDePago(conexion.getConnection(), listaDetalleFormaDePagoCuentaCorriente);
        conexion.cerrarConexion();
    }

    void cargarObraSocial() {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaOS = new ArrayList<modelo.ObraSocial>();
        modelo.ObraSocial.cargarOS(conexion.getConnection(), listaOS);
        conexion.cerrarConexion();

    }

    void cargarObraSocialAfip() {

        conexion = new modelo.ConexionMariaDB();
        conexion.EstablecerConexion();
        listaOSAfip = new ArrayList<>();
        ObraSocialAfip.cargarOSAfip(conexion.getConnection(), listaOSAfip);
        conexion.cerrarConexion();

    }

    /*void cargaractualizacion() {
        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String sSQL = "SELECT version FROM actulaizacion ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                version = (rs.getString("version"));   
                }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "No se pudo conectar al servidor... Verifique su conexión a internet");
        }
    }*/
 /*  void cargaranalisis() {

        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();
        String sSQL2 = "SELECT codigo_practica,id_practicasnbu,determinacion_practica FROM practicasnbu ";

        try {
            Statement st2 = cn.createStatement();
            ResultSet rs2 = st2.executeQuery(sSQL2);
            // Recorro y cargo las obras sociales
            while (rs2.next()) {
                analisis[contadoranalisis] = (rs2.getString("codigo_practica") + " - " + rs2.getString("determinacion_practica"));
                contadoranalisis++;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }*/

 /*void cargarobrasosial() {

        ConexionMariaDB mysql = new ConexionMariaDB();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT id_obrasocial,razonsocial_obrasocial,importeunidaddearancel_obrasocial,codigo_obrasocial FROM obrasocial ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idobrasocial[contadorobrasocial] = rs.getInt("id_obrasocial");
                nombreobrasocial[contadorobrasocial] = (rs.getString("razonsocial_obrasocial"));
                arancel[contadorobrasocial] = (rs.getDouble("importeunidaddearancel_obrasocial"));
                obrasocial[contadorobrasocial] = (rs.getString("codigo_obrasocial") + " - " + rs.getString("razonsocial_obrasocial"));
                contadorobrasocial++;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar al servidor... Verifique su conexión a internet");
        }
    }*/
    public static int fechasDiferenciaEnDias(Date fechaInicial, Date fechaFinal) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fechaInicioString = df.format(fechaInicial);
        try {
            fechaInicial = df.parse(fechaInicioString);
        } catch (ParseException ex) {
        }

        String fechaFinalString = df.format(fechaFinal);
        try {
            fechaFinal = df.parse(fechaFinalString);
        } catch (ParseException ex) {
        }
        long fechaInicialMs = fechaInicial.getTime();
        long fechaFinalMs = fechaFinal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        return ((int) dias);
    }

    public void pausa(int mlSeg) {
        try {
            // pausa para el splash
            Thread.sleep(mlSeg);
        } catch (Exception e) {
        }
    }
}
