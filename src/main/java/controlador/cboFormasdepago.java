package controlador;

public class cboFormasdepago {
    
  
    private int idFormasdepago;
    private String nombreFormasdepago;
    private double descuentoFormasdepago;
    
    public cboFormasdepago(){}
    
    public cboFormasdepago(int id, String nombre, double descuento){
        this.idFormasdepago = id;
        this.nombreFormasdepago = nombre;
        this.descuentoFormasdepago = descuento;
    }

    public int getidFormasdepago() {
        return idFormasdepago;
    }

    public void setidFormasdepago(int idFormasdepago) {
        this.idFormasdepago = idFormasdepago;
    }

    public String getnombreFormasdepago() {
        return nombreFormasdepago;
    }

    public void setnombreFormasdepago(String nombreFormasdepago) {
        this.nombreFormasdepago = nombreFormasdepago;
    }
    
    public Double getdescuentoFormasdepago() {
        return descuentoFormasdepago;
    }

    public void setnombreFormasdepago(Double descuento) {
        this.descuentoFormasdepago = descuento;
    }
    
    public String toString(){
        return this.nombreFormasdepago;
    }
    
}
