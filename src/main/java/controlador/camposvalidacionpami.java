

package controlador;

/**
 *
 * @author lucas.robles
 */
public class camposvalidacionpami {
    String id, apellido, ambulatorio, internado, ok, observada, anulada,total;

    public camposvalidacionpami(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


    public String getId() {
        return id;
    }

    public String getApellido() {
        return apellido;
    }

    public String getAmbulatorio() {
        return ambulatorio;
    }

    public String getInternado() {
        return internado;
    }

    public String getOk() {
        return ok;
    }

    public String getObservada() {
        return observada;
    }

    public String getAnulada() {
        return anulada;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setAmbulatorio(String ambulatorio) {
        this.ambulatorio = ambulatorio;
    }

    public void setInternado(String internado) {
        this.internado = internado;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public void setObservada(String observada) {
        this.observada = observada;
    }

    public void setAnulada(String anulada) {
        this.anulada = anulada;
    }

    public camposvalidacionpami(String id, String apellido, String ambulatorio, String internado, String ok, String observada, String anulada, String total) {
        this.id = id;
        this.apellido = apellido;
        this.ambulatorio = ambulatorio;
        this.internado = internado;
        this.ok = ok;
        this.observada = observada;
        this.anulada = anulada;
        this.total = total;
    }
}
