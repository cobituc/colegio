
package controlador;


public class camposorden_1 {
    String nombre1, numero_orden1, especialidad1, codigo_barra1;

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getNumero_orden1() {
        return numero_orden1;
    }

    public void setNumero_orden1(String numero_orden1) {
        this.numero_orden1 = numero_orden1;
    }

    public String getEspecialidad1() {
        return especialidad1;
    }

    public void setEspecialidad1(String especialidad1) {
        this.especialidad1 = especialidad1;
    }

    public String getCodigo_barra1() {
        return codigo_barra1;
    }

    public void setCodigo_barra1(String codigo_barra1) {
        this.codigo_barra1 = codigo_barra1;
    }

    public camposorden_1(String nombre1, String numero_orden1, String especialidad1, String codigo_barra1) {
        this.nombre1 = nombre1;
        this.numero_orden1 = numero_orden1;
        this.especialidad1 = especialidad1;
        this.codigo_barra1 = codigo_barra1;
    }

}