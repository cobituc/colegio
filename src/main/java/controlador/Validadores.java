package controlador;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;


public class Validadores {
    
    private int idValidadores;
    private String nombre;
    private String apellido;

    public Validadores(int idValidadores, String nombre, String apellido) {
        this.idValidadores = idValidadores;
        this.nombre = nombre;
        this.apellido = apellido;
    }   

    public int getIdValidadores() {
        return idValidadores;
    }

    public void setIdValidadores(int idValidadores) {
        this.idValidadores = idValidadores;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Override
    public String toString() {
        return nombre + " " + apellido;
    }
    
    public static void cargarValidador(Connection connection, ArrayList<Validadores> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs;
            rs = instruccion.executeQuery("SELECT id_usuario, nombre_usuario, apellido_usuario FROM usuarios WHERE validacion_pami = 1");
            while (rs.next()) {
                lista.add(new Validadores(rs.getInt("id_usuario"), rs.getString("nombre_usuario"), rs.getString("apellido_usuario")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public static Validadores verificarValidador(ArrayList<Validadores> lista, String validador){
        
        Validadores resultado = null;
        
        for(Validadores busqueda : lista){
            
            if ((busqueda.getNombre()+" "+busqueda.getApellido()).equals(validador)) {

                resultado = busqueda;
                break;

            }
        }
        return resultado;
    }

    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Validadores other = (Validadores) obj;
        if (this.idValidadores != other.idValidadores) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apellido, other.apellido)) {
            return false;
        }
        return true;
    }

    
}

