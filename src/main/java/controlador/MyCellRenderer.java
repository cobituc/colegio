package controlador;

import static vista.ArchivoObraSociales.isNumeric;
import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class MyCellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel celda = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        isNumeric num = new isNumeric();
        celda.setHorizontalAlignment(SwingConstants.CENTER);
        if (column == 0) {
            celda.setBackground(Color.decode("#3A5FCD"));
            celda.setForeground(Color.white);
        } else if (column < 2 && column != 0) {
            celda.setBackground(Color.decode("#FFFACD"));
            celda.setForeground(Color.black);
            celda.setHorizontalAlignment(SwingConstants.LEFT);
        } else if (column >= 2 && column <= 5) {
            celda.setBackground(Color.decode("#C1FFC1"));
            celda.setForeground(Color.black);
        } else if (column >= 6 && column <= 9) {
            celda.setBackground(Color.decode("#FFA54F"));
            celda.setForeground(Color.black);
        } else if (column == 10) {
            celda.setBackground(Color.decode("#FFB5C5"));
            celda.setForeground(Color.black);
        } else if (column == 11) {
            celda.setBackground(Color.decode("#8968CD"));
            celda.setForeground(Color.black);
        } else if (column >= 12 && column <= 16) {
            celda.setBackground(Color.decode("#BFEFFF"));
            celda.setForeground(Color.black);
        } else if (column == 17) {
            celda.setBackground(Color.decode("#FFA54F"));
            celda.setForeground(Color.black);
        } else if (column == 18) {
            celda.setBackground(Color.decode("#C1FFC1"));
            celda.setForeground(Color.black);
        } else if (column == 19) {
            celda.setBackground(Color.decode("#C1FFC1"));
            celda.setForeground(Color.black);
        } else if (column == 20) {
            celda.setBackground(Color.decode("#C1FFC1"));
            celda.setForeground(Color.black);
        } else if (column == 24) {
            celda.setBackground(Color.decode("#BFEFFF"));
            celda.setForeground(Color.black);
        } else if (column == 26) {
            celda.setBackground(Color.decode("#BFEFFF"));
            celda.setForeground(Color.black);
        } else if (column > 26) {
            celda.setBackground(Color.decode("#8968CD"));
            celda.setForeground(Color.black);
        }
       
        if (value instanceof String) {
            String valor = (String) value;
            if (column == 2 || 
                    column == 3 || 
                    column == 4 || 
                    column == 5 ||
                    column == 9 ||  
                    column == 10 || 
                    column == 12 || 
                    column == 13 ||
                    column == 14 || 
                    column == 15 || 
                    column == 16 ||
                    column == 17 ) {
                if (!valor.equals("") && !valor.equals("A") && !valor.equals("-") ) {
                    System.out.println(valor);
                    if (Double.valueOf(valor) < 4) {
                        celda.setForeground(Color.red);
                    } else {
                        celda.setForeground(Color.black);
                    }
                }

            }                
        }
       
       if(isSelected){
           this.setOpaque(true);
           this.setBackground(Color.WHITE);
           this.setForeground(Color.BLACK);
           
       }
        return celda;
    }
}
