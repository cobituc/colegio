package controlador;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author MATIAS
 */
public class MyCellRenderer3 extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
       JLabel celda = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        isNumeric num = new isNumeric();
        celda.setHorizontalAlignment(SwingConstants.CENTER);
        if (column == 0) {
            celda.setBackground(Color.decode("#3A5FCD"));
            celda.setForeground(Color.white);
        } else if (column < 2 && column != 0) {
            celda.setBackground(Color.decode("#FFFACD"));
            celda.setForeground(Color.black);
            celda.setHorizontalAlignment(SwingConstants.LEFT);
        } else if (column >= 2 && column <= 5) {
            celda.setBackground(Color.decode("#C1FFC1"));
            celda.setForeground(Color.black);
        } else if (column >= 6 && column <= 9) {
            celda.setBackground(Color.decode("#FFA54F"));
            celda.setForeground(Color.black);
        } else if (column == 10) {
            celda.setBackground(Color.decode("#FFB5C5"));
            celda.setForeground(Color.black);
        } else if (column == 11) {
            celda.setBackground(Color.decode("#8968CD"));
            celda.setForeground(Color.black);
        } else if (column >= 12 && column <= 16) {
            celda.setBackground(Color.decode("#BFEFFF"));
            celda.setForeground(Color.black);
        } else if (column == 17) {
            celda.setBackground(Color.decode("#FFA54F"));
            celda.setForeground(Color.black);
        } else if (column == 18) {
            celda.setBackground(Color.decode("#C1FFC1"));
            celda.setForeground(Color.black);
        } else if (column == 19) {
            celda.setBackground(Color.decode("#C1FFC1"));
            celda.setForeground(Color.black);
        } else if (column == 20) {
            celda.setBackground(Color.decode("#C1FFC1"));
            celda.setForeground(Color.black);
        } else if (column == 24) {
            celda.setBackground(Color.decode("#BFEFFF"));
            celda.setForeground(Color.black);
        } else if (column == 26) {
            celda.setBackground(Color.decode("#BFEFFF"));
            celda.setForeground(Color.black);
        } else if (column > 26) {
            celda.setBackground(Color.decode("#8968CD"));
            celda.setForeground(Color.black);
        }
       if(isSelected){
           this.setOpaque(true);
           this.setBackground(Color.WHITE);
           this.setForeground(Color.BLACK);
           
       }
        return celda;
    }
}
