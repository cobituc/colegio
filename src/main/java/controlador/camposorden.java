
package controlador;


public class camposorden {
    String nombre, numero_orden, especialidad, codigo_barra;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero_orden() {
        return numero_orden;
    }

    public void setNumero_orden(String numero_orden) {
        this.numero_orden = numero_orden;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getCodigo_barra() {
        return codigo_barra;
    }

    public void setCodigo_barra(String codigo_barra) {
        this.codigo_barra = codigo_barra;
    }

    public camposorden(String nombre, String numero_orden, String especialidad, String codigo_barra) {
        this.nombre = nombre;
        this.numero_orden = numero_orden;
        this.especialidad = especialidad;
        this.codigo_barra = codigo_barra;
    }

}