package controlador;

import java.awt.HeadlessException;
import java.awt.Image;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.PrinterName;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;

public class Reportes {

    public Image logo = new ImageIcon(getClass().getResource("/Imagenes/cbt.jpg")).getImage();

    public void ReporteRecepcion(int matricula, String bioquimico, int presentacion, int mes, int ordenes, int año, int mesProximo, int añoProximo, String vencimientoActual1, String VencimientoActual2, String vencimientoProximo1, String VencimientoProximo2) {
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            //JDialog viewer = new JDialog(new javax.swing.JFrame(), "Recepción", true);

//            viewer.setIconImage(new ImageIcon(getClass().getResource("/cbt.png")).getImage());
            // viewer.setSize(1024, 720);
            //viewer.setLocationRelativeTo(null);
            JasperReport reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Comprobante_recepcion_pami.jasper"));
            Map parametro = new HashMap();

            parametro.put("matricula", String.valueOf(matricula));
            parametro.put("bioquimico", bioquimico);
            parametro.put("presentacion", String.valueOf(presentacion));
            parametro.put("mes", String.valueOf(mes));
            parametro.put("ordenes", String.valueOf(ordenes));
            parametro.put("año", String.valueOf(año));
            parametro.put("mesProximo", String.valueOf(mesProximo));
            parametro.put("añoProximo", String.valueOf(añoProximo));
            parametro.put("vencimientoActual1", vencimientoActual1);
            parametro.put("vencimientoActual2", VencimientoActual2);
            parametro.put("vencimientoProximo1", vencimientoProximo1);
            parametro.put("vencimientoProximo2", VencimientoProximo2);
            parametro.put("logo", logo);

            JasperPrint j = JasperFillManager.fillReport(reporte, parametro, new JREmptyDataSource());
            //JasperViewer jv = new JasperViewer(j);
            //viewer.getContentPane().add(jv.getContentPane());
            //viewer.setVisible(true);
            JasperPrintManager.printReport(j, true);
            cn.close();

        } catch (HeadlessException | SQLException | JRException e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    public void ReporteRotulo(String bioquimico, int matricula, int ordenes, int mes, String fecha, String validador) {
        ConexionMariaDB cc = new ConexionMariaDB();
        Connection cn = cc.Conectar();
        try {
            //JDialog viewer = new JDialog(new javax.swing.JFrame(), "Recepción", true);

//            viewer.setIconImage(new ImageIcon(getClass().getResource("/cbt.png")).getImage());
            // viewer.setSize(1024, 720);
            //viewer.setLocationRelativeTo(null);
            JasperReport reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/RotuloPami.jasper"));
            Map parametro = new HashMap();

            parametro.put("bioquimico", bioquimico);
            parametro.put("matricula", String.valueOf(matricula));
            parametro.put("ordenes", String.valueOf(ordenes));            
            parametro.put("mes", String.valueOf(mes));
            parametro.put("fecha", fecha);
            parametro.put("validador", validador);

            JasperPrint j = JasperFillManager.fillReport(reporte, parametro, new JREmptyDataSource());
            //JasperViewer jv = new JasperViewer(j);
            //viewer.getContentPane().add(jv.getContentPane());
            //viewer.setVisible(true);
            //JasperPrintManager.printReport(j, true);
            PrintReportToPrinter(j);
            cn.close();

        } catch (HeadlessException | SQLException | JRException e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    private void PrintReportToPrinter(JasperPrint jasperPrint) throws JRException {
//Get the printers names
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
//Lets set the printer name based on the registered printers driver name (you can see the printer names in the services variable at debugging) 
        String selectedPrinter = "Godex G300";
//String selectedPrinter = "\\\\S-BPPRINT\\HP Color LaserJet 4700"; //examlpe to network shared printer
        System.out.println("Number of print services: " + services.length);
        PrintService selectedService = null;
//Set the printing settings
        PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
        //printRequestAttributeSet.add(MediaSizeName.ISO_A4);
        printRequestAttributeSet.add(new Copies(1));
        //if (jasperPrint.getOrientationValue() == net.sf.jasperreports.engine.type.OrientationEnum.LANDSCAPE) {
          //  printRequestAttributeSet.add(OrientationRequested.LANDSCAPE);
        //} else {
            printRequestAttributeSet.add(OrientationRequested.PORTRAIT);
        //}
        PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
        printServiceAttributeSet.add(new PrinterName(selectedPrinter, null));
        JRPrintServiceExporter exporter = new JRPrintServiceExporter();
        SimplePrintServiceExporterConfiguration configuration = new SimplePrintServiceExporterConfiguration();
        configuration.setPrintRequestAttributeSet(printRequestAttributeSet);
        configuration.setPrintServiceAttributeSet(printServiceAttributeSet);
        configuration.setDisplayPageDialog(false);
        configuration.setDisplayPrintDialog(false);
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setConfiguration(configuration);
//Iterate through available printer, and once matched with our <selectedPrinter>, go ahead and print!
        if (services != null && services.length != 0) {
            for (PrintService service : services) {
                String existingPrinter = service.getName();
                if (existingPrinter.equals(selectedPrinter)) {
                    selectedService = service;
                    break;
                }
            }
        }
        if (selectedService != null) {
            try {
//Lets the printer do its magic!
                exporter.exportReport();
            } catch (Exception e) {
                System.out.println("JasperReport Error: " + e.getMessage());
            }
        } else {
            System.out.println("JasperReport Error: Printer not found!");
        }
    }

}
