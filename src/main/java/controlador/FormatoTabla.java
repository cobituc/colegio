package controlador;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


public class FormatoTabla extends DefaultTableCellRenderer{

    public Component getTableCellRendererComponent(JTable table,
      Object value,
      boolean isSelected,
      boolean hasFocus,
      int row,
      int column)
   {
      super.getTableCellRendererComponent (table, value, isSelected, hasFocus, row, column);
      if ( table.getValueAt(row, 3).equals("SI"))
      {
         this.setOpaque(true);
         this.setBackground(Color.blue);
         this.setForeground(Color.white);
      } else {
         // Restaurar los valores por defecto
      }

      return this;
   }
    
}