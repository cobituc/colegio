/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author Fernando Lencina
 */
public class reporteEntregaValidadores {
   String  MP, bioquimico, fecha; 
   int ordenes;
    public void setMP(String MP) {
        this.MP = MP;
    }

    public void setBioquimico(String bioquimico) {
        this.bioquimico = bioquimico;
    }

    public void setOrdenes(int ordenes) {
        this.ordenes = ordenes;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMP() {
        return MP;
    }

    public String getBioquimico() {
        return bioquimico;
    }

    public int getOrdenes() {
        return ordenes;
    }    

    public String getFecha() {
        return fecha;
    }

    public reporteEntregaValidadores(String MP, String bioquimico, int ordenes, String fecha) {
        this.MP = MP;
        this.bioquimico = bioquimico;
        this.ordenes = ordenes;        
        this.fecha = fecha;
    }
}
