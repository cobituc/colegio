
package controlador;

import java.awt.Color;
import java.awt.Component;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import static javax.swing.SwingConstants.CENTER;
import javax.swing.table.DefaultTableCellRenderer;


public class FuncionesTabla extends DefaultTableCellRenderer{
    
    Color background;
    Color foreground;
    private Format formatter = new SimpleDateFormat("dd/MM/yyyy");    
    
   public static DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda, fecha;
    
   public static void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }
   
   
   public FuncionesTabla(Color background, Color foreground) {
        
    setHorizontalAlignment(CENTER);
    setHorizontalTextPosition(CENTER);
    setVerticalAlignment(CENTER);
    this.background = background;
    this.foreground = foreground;
    setOpaque(false);
  }

    public FuncionesTabla() {
    }
   
      
   public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        if( value instanceof Date) {
            value = formatter.format(value);
        }
        return super.getTableCellRendererComponent(table, value, isSelected,
                hasFocus, row, column);
    }
   
}
