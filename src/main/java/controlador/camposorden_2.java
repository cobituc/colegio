
package controlador;


public class camposorden_2{
    String nombre2, numero_orden2, especialidad2, codigo_barra2;

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getNumero_orden2() {
        return numero_orden2;
    }

    public void setNumero_orden2(String numero_orden2) {
        this.numero_orden2 = numero_orden2;
    }

    public String getEspecialidad2() {
        return especialidad2;
    }

    public void setEspecialidad2(String especialidad2) {
        this.especialidad2 = especialidad2;
    }

    public String getCodigo_barra2() {
        return codigo_barra2;
    }

    public void setCodigo_barra2(String codigo_barra2) {
        this.codigo_barra2 = codigo_barra2;
    }

    public camposorden_2(String nombre2, String numero_orden2, String especialidad2, String codigo_barra2) {
        this.nombre2 = nombre2;
        this.numero_orden2 = numero_orden2;
        this.especialidad2 = especialidad2;
        this.codigo_barra2 = codigo_barra2;
    }

}