package controlador;

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class MyTableCellEditor extends AbstractCellEditor implements TableCellEditor {

    JCheckBox component = new JCheckBox();

    // This method is called when editing is completed.
    // It must return the new value to be stored in the cell.
    public Object getCellEditorValue() {
        Boolean aux;
        if (component.isSelected()) {
            aux = true;
        } else {
            aux = false;
        }

        return aux;

    }

    // This method is called when a cell value is edited by the user
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        if (column == 4 && isSelected && table.getValueAt(row, column).equals(false)) {
            table.setValueAt(true, row, 3);
            table.setValueAt(true, row, column);
        } else if (column == 4 && isSelected && table.getValueAt(row, column).equals(true)) {
            table.setValueAt(false, row, column);
        }
        if(column == 3 && isSelected && table.getValueAt(row, column).equals(true)){
             table.setValueAt(false, row, 3);
            table.setValueAt(false, row, column);
        }

        if (value.equals("true")) {
            component.setSelected(true);

        } else {
            component.setSelected(false);

        }

        return component;

    }
}
