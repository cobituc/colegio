
package controlador;


public class DatosGrafico {

    private String periodo;
    private int cantidad;
    private int idColegiado;

    public DatosGrafico(String periodo, int cantidad, int idColegiado) {
        this.periodo = periodo;
        this.cantidad = cantidad;
        this.idColegiado = idColegiado;
    }

    

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getIdColegiado() {
        return idColegiado;
    }

    public void setIdColegiado(int idColegiado) {
        this.idColegiado = idColegiado;
    }
    
            
    
}
