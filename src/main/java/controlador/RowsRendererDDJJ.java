package controlador;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class RowsRendererDDJJ extends DefaultTableCellRenderer {

    private int columna;
    public static final DefaultTableCellRenderer DEFAULT_RENDERER = new DefaultTableCellRenderer();

    public RowsRendererDDJJ(int Colpatron) {
        this.columna = Colpatron;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        JLabel celda = (JLabel) super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        celda.setHorizontalAlignment(SwingConstants.CENTER);

        setBackground(Color.WHITE);
        table.setForeground(Color.BLACK);
        super.getTableCellRendererComponent(table, value, selected, focused, row, column);

        
        
        if (table.getValueAt(row, columna).equals("0")) {
            this.setBackground(Color.WHITE);
            this.setForeground(Color.BLACK);
        } else if (table.getValueAt(row, columna).equals("1")) {
            this.setBackground(Color.decode("#fcba03"));
            this.setForeground(Color.BLACK);
        } else if (table.getValueAt(row, columna).equals("2")) {
            this.setBackground(Color.decode("#03b1fc"));
            this.setForeground(Color.BLACK);
        } else if (table.getValueAt(row, columna).equals("3")) {
            this.setBackground(Color.decode("#98FB98"));
            this.setForeground(Color.BLACK);
        } else if (table.getValueAt(row, columna).equals("4")) {
            this.setBackground(Color.decode("#fc1c03"));
            this.setForeground(Color.BLACK);
        }

        if (selected) {
            this.setOpaque(true);
            this.setBackground(Color.BLACK);
            this.setForeground(Color.WHITE);

        }

        return this;

    }

}
