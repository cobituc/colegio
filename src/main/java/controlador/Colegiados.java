
package controlador;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Colegiados{
	private int idColegiados;
	private int matricula;
	private String nombreColegiado;

                       
	public Colegiados(int idColegiados, int matricula, String nombreColegiado) { 
		this.idColegiados = idColegiados;
		this.matricula = matricula;
		this.nombreColegiado = nombreColegiado;
	}

    public Colegiados() {
    }

    public int getIdColegiados() {
        return idColegiados;
    }

    public void setIdColegiados(int idColegiados) {
        this.idColegiados = idColegiados;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getNombreColegiado() {
        return nombreColegiado;
    }

    public void setNombreColegiado(String nombreColegiado) {
        this.nombreColegiado = nombreColegiado;
    }
        
      
        
        public static void cargarColegiado(Connection connection, ArrayList<Colegiados> lista, int matricula) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet rs;
            rs = instruccion.executeQuery("SELECT id_colegiados,matricula_colegiado,nombre_colegiado FROM colegiados where matricula_colegiado=" + matricula + " and tipo_profesional='B' and estado_colegiado='ACTIVOF'");
            while (rs.next()) {
                lista.add(new Colegiados(rs.getInt(1), rs.getInt(2), rs.getString(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
        
        public static Colegiados buscarColegiado(int matricula, ArrayList<Colegiados> lista) {
        Colegiados resultado = null;

        for (Colegiados colegiado : lista) {

            if (colegiado.getMatricula()== matricula) {

                resultado = colegiado;
                break;

            }
        }

        return resultado;
    }
        
}