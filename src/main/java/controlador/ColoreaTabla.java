package controlador;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


public class ColoreaTabla extends DefaultTableCellRenderer{
       
      public Component getTableCellRendererComponent (JTable table, Object value, boolean selected, boolean focused, int row, int column){
       
          Component cell = super.getTableCellRendererComponent(table, value, selected, focused, row, column);
         cell.setForeground(Color.BLACK);
          if( "ACTIVO".equals(value)) {
             cell.setForeground(Color.BLACK);
            }
         if ("BAJA".equals(value)) {
              cell.setForeground(Color.BLUE);
          }
        return cell;
      }
} 