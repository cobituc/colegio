/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

public class reporte_entrega {

    String especialidad, nombre,direccion,cantidad,desdex4,hastax4,institucion,localidad,codigo_postal,mes,año,sobre;

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getDesdex4() {
        return desdex4;
    }

    public void setDesdex4(String desdex4) {
        this.desdex4 = desdex4;
    }

    public String getHastax4() {
        return hastax4;
    }

    public void setHastax4(String hastax4) {
        this.hastax4 = hastax4;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCodigo_postal() {
        return codigo_postal;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public String getSobre() {
        return sobre;
    }

    public void setSobre(String sobre) {
        this.sobre = sobre;
    }

    public reporte_entrega(String especialidad, String nombre, String direccion, String cantidad, String desdex4, String hastax4, String institucion, String localidad, String codigo_postal, String mes, String año, String sobre) {
        this.especialidad = especialidad;
        this.nombre = nombre;
        this.direccion = direccion;
        this.cantidad = cantidad;
        this.desdex4 = desdex4;
        this.hastax4 = hastax4;
        this.institucion = institucion;
        this.localidad = localidad;
        this.codigo_postal = codigo_postal;
        this.mes = mes;
        this.año = año;
        this.sobre = sobre;
    }

   
   

}