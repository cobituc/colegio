
package controlador;


public class camposorden_3 {
    String nombre, numero_orden, especialidad, codigo_barra,nombre1, numero_orden1, especialidad1, codigo_barra1,nombre2, numero_orden2, especialidad2, codigo_barra2, fecha;

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFecha() {
        return fecha;
    }

   

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero_orden() {
        return numero_orden;
    }

    public void setNumero_orden(String numero_orden) {
        this.numero_orden = numero_orden;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getCodigo_barra() {
        return codigo_barra;
    }

    public void setCodigo_barra(String codigo_barra) {
        this.codigo_barra = codigo_barra;
    }

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getNumero_orden1() {
        return numero_orden1;
    }

    public void setNumero_orden1(String numero_orden1) {
        this.numero_orden1 = numero_orden1;
    }

    public String getEspecialidad1() {
        return especialidad1;
    }

    public void setEspecialidad1(String especialidad1) {
        this.especialidad1 = especialidad1;
    }

    public String getCodigo_barra1() {
        return codigo_barra1;
    }

    public void setCodigo_barra1(String codigo_barra1) {
        this.codigo_barra1 = codigo_barra1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getNumero_orden2() {
        return numero_orden2;
    }

    public void setNumero_orden2(String numero_orden2) {
        this.numero_orden2 = numero_orden2;
    }

    public String getEspecialidad2() {
        return especialidad2;
    }

    public void setEspecialidad2(String especialidad2) {
        this.especialidad2 = especialidad2;
    }

    public String getCodigo_barra2() {
        return codigo_barra2;
    }

    public void setCodigo_barra2(String codigo_barra2) {
        this.codigo_barra2 = codigo_barra2;
    }

    public camposorden_3(String nombre, String numero_orden, String especialidad, String codigo_barra, String nombre1, String numero_orden1, String especialidad1, String codigo_barra1, String nombre2, String numero_orden2, String especialidad2, String codigo_barra2,  String fecha) {
        this.nombre = nombre;
        this.numero_orden = numero_orden;
        this.especialidad = especialidad;
        this.codigo_barra = codigo_barra;
        this.nombre1 = nombre1;
        this.numero_orden1 = numero_orden1;
        this.especialidad1 = especialidad1;
        this.codigo_barra1 = codigo_barra1;
        this.nombre2 = nombre2;
        this.numero_orden2 = numero_orden2;
        this.especialidad2 = especialidad2;
        this.codigo_barra2 = codigo_barra2;
        this.fecha = fecha;
    }

    

}