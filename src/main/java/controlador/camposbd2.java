package controlador;

public class camposbd2 {

    String numero_de_orden, fecha_de_registracion, nombre_y_apellido_o_razon_social,
            domicilio_del_proponente, ubicacion_del_riesgo, entidad_aseguradora, bien_a_asegurar,
            riesgo_a_cubrir, suma_asegurada, vigencia_del_seguro, observaciones;

    public camposbd2(String numero_de_orden, String fecha_de_registracion,
            String nombre_y_apellido_o_razon_social, String domicilio_del_proponente, String ubicacion_del_riesgo,
            String entidad_aseguradora, String bien_a_asegurar, String riesgo_a_cubrir, String suma_asegurada,
            String vigencia_del_seguro, String observaciones) {

        this.numero_de_orden = numero_de_orden;
        this.fecha_de_registracion = fecha_de_registracion;
        this.nombre_y_apellido_o_razon_social = nombre_y_apellido_o_razon_social;
        this.entidad_aseguradora = entidad_aseguradora;
        this.domicilio_del_proponente = domicilio_del_proponente;
        this.ubicacion_del_riesgo = ubicacion_del_riesgo;
        this.bien_a_asegurar = bien_a_asegurar;
        this.riesgo_a_cubrir = riesgo_a_cubrir;
        this.suma_asegurada = suma_asegurada;
        this.vigencia_del_seguro = vigencia_del_seguro;
        this.observaciones = observaciones;
    }

    public String getnumero_de_orden() {
        return numero_de_orden;
    }

    public void setnumero_de_orden(String numero_de_orden) {
        this.numero_de_orden = numero_de_orden;
    }

    public String getfecha_de_registracion() {
        return fecha_de_registracion;
    }

    public void setfecha_de_registracion(String fecha_de_registracion) {
        this.fecha_de_registracion = fecha_de_registracion;
    }

    public String getnombre_y_apellido_o_razon_social() {
        return nombre_y_apellido_o_razon_social;
    }

    public void setnombre_y_apellido_o_razon_social(String nombre_y_apellido_o_razon_social) {
        this.nombre_y_apellido_o_razon_social = nombre_y_apellido_o_razon_social;
    }

    public String getentidad_aseguradora() {
        return entidad_aseguradora;
    }

    public void setentidad_aseguradora(String entidad_aseguradora) {
        this.entidad_aseguradora = entidad_aseguradora;
    }

    public String getdomicilio_del_proponente() {
        return domicilio_del_proponente;
    }

    public void setdomicilio_del_proponente(String domicilio_del_proponente) {
        this.domicilio_del_proponente = domicilio_del_proponente;
    }

    public String getubicacion_del_riesgo() {
        return ubicacion_del_riesgo;
    }

    public void setubicacion_del_riesgo(String ubicacion_del_riesgo) {
        this.ubicacion_del_riesgo = ubicacion_del_riesgo;
    }

    public String getbien_a_asegurar() {
        return bien_a_asegurar;
    }

    public void setbien_a_asegurar(String bien_a_asegurar) {
        this.bien_a_asegurar = bien_a_asegurar;
    }

    public String getriesgo_a_cubrir() {
        return riesgo_a_cubrir;
    }

    public void setriesgo_a_cubrir(String riesgo_a_cubrir) {
        this.riesgo_a_cubrir = riesgo_a_cubrir;
    }

    public String getsuma_asegurada() {
        return suma_asegurada;
    }

    public void setsuma_asegurada(String suma_asegurada) {
        this.suma_asegurada = suma_asegurada;
    }

    public String getvigencia_del_seguro() {
        return vigencia_del_seguro;
    }

    public void setvigencia_del_seguro(String vigencia_del_seguro) {
        this.vigencia_del_seguro = vigencia_del_seguro;
    }

    public String getobservaciones() {
        return observaciones;
    }

    public void setobservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

}
